<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/sportregistration/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/sportregistration/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/sportregistration/catalog/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/sportregistration/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/sportregistration/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/sportregistration/catalog/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/sportregistration/catalog/view/theme/');
define('DIR_CONFIG', 'C:/xampp/htdocs/sportregistration/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/sportregistration/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/sportregistration/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/sportregistration/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/sportregistration/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/sportregistration/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'db_sportregistration');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
