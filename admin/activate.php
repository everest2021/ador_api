<?php 
	include 'config.php';
	
	$computerkey = shell_exec('wmic DISKDRIVE GET SerialNumber');
	$encryptcomputerkey = md5($computerkey);
	$data = array('key' => $encryptcomputerkey, 'hotelname' => HOTEL_NAME, 'activate' => 1, 'check' => 0);
	$data1 = array('key' => $encryptcomputerkey, 'hotelname' => HOTEL_NAME, 'activate' => 0, 'check' => 1);
	$ch = curl_init(TEST.'test.php');
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/text'));
	
	if(isset($_POST['activate'])){
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($ch);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		header('location:activate.php?success=1');
	}
	if(isset($_POST['checknow'])){
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data1));
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		$finaldata = json_decode($output,true);
		if($finaldata['status'] == '1'){
			$notimp = DIR_ACTIVATE;
			$handle = fopen($notimp,'w');
			$data = trim($finaldata['key'],'"');
			fwrite($handle, $data);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			header('location:index.php');
		}else{
			header('location:activate.php?notactivate=1');
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body style="background-image:url(backactivate.jpg);background-repeat:no-repeat;background-size:cover;">
	<form action="activate.php" method="POST">
		<div style="position: absolute;top: 30%;left:<?php echo (isset($_GET['expire']) && $_GET['expire'] == '1') ? '32%' : '40%'?>">
			<button type="submit" name="activate" class="btn btn-lg btn-danger" style="padding: 50px 50px;font-size: 50px;"><?php echo (isset($_GET['expire']) && $_GET['expire'] == '1') ? 'Your Software is expired<br>Activate Now' : 'Activate Now' ?></button>
			<br><br>
			<center><button type="submit" name="checknow" class="btn btn-lg btn-danger" style="padding: 50px 50px;font-size: 50px;">Check Now</button></center>
		</div>
		<div style="position: absolute;top: 20%;left:36%">
			<?php if(isset($_GET['notactivate'])) {?>
				<center><h1 style="color:white">Wait activation is in process</h1></center>
			<?php } ?>
			<?php if(isset($_GET['success'])) {?>
				<center><h1 style="color:green;font-weight: bold;">Your Activation Request Sent</h1></center>
			<?php } ?>
		</div>
	</form>
</body>
</html>