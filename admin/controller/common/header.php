<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		$data['title'] = $this->document->getTitle();

		if(isset($this->request->get['route']) && $this->request->get['route'] == 'catalog/customer/add'){
			$delete = 0;
		} else {
			$delete = 1;
		}
		if(isset($this->session->data['table_id']) && $delete == 1){
			// echo '<pre>';
			// print_r($this->session->data);
			// exit;
			$table_id = $this->session->data['table_id'];
			$this->db->query("DELETE FROM `oc_running_table` WHERE `table_id` = '".$table_id."' ");
			unset($this->session->data['table_id']);
		}

		$running_table_datas = $this->db->query("SELECT * FROM oc_running_table")->rows;
		date_default_timezone_set("Asia/Kolkata");
		foreach ($running_table_datas as $rkey => $rvalue) {
			$current_date_time = date('Y-m-d H:i:s');
			$running_date_time = $rvalue['date_added'].' '.$rvalue['time_added'];
			$start_date = new DateTime($current_date_time);
			$since_start = $start_date->diff(new DateTime($running_date_time));
			$difference_hour = $since_start->h;
			$difference_min = $since_start->i;
			$del_status = 0;
			if($difference_hour > 0){
				$del_status = 1;
			} else if($difference_hour == 0 && $difference_min > 5){
				$del_status = 1;
			}
			if($del_status == 1){
				$this->db->query("DELETE FROM `oc_running_table` WHERE `table_id` = '".$rvalue['table_id']."' ");	
			}
		}

		if(isset($this->request->get['route']) && ($this->request->get['route'] == 'catalog/orderprocess/getForm' || $this->request->get['route'] == 'catalog/orderprocess' || $this->request->get['route'] == 'catalog/orderprocess/add' )){
			$data['hide'] = 1;
		} else if(isset($this->request->get['route']) && ($this->request->get['route'] == 'catalog/orderprocess1/getForm' || $this->request->get['route'] == 'catalog/orderprocess1' || $this->request->get['route'] == 'catalog/orderprocess1/add' || ($this->request->get['route'] == 'catalog/kitchen_display/getForm' || $this->request->get['route'] == 'catalog/kitchen_display' || $this->request->get['route'] == 'catalog/kitchen_display/add'))){
			$data['hide'] = 2;
		} else {
			$data['hide'] = 0;
		}
		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		if(isset($this->request->get['route'])){
		  	$current_location = explode("/", $this->request->get['route']);
		  	if($current_location[0] == "common"){
		    	$is_homepage = TRUE;
		  	} else {
		    	$is_homepage = FALSE;
		  	}
		} else {
			$is_homepage = FALSE;
		}
		$get_url = explode("&", $_SERVER['QUERY_STRING']);
		$get_route = substr($get_url[0], 6);
		$get_route = explode("/", $get_route);
		$page_name = array("shoppica2","journal_banner","journal_bgslider","journal_cp","journal_filter","journal_gallery","journal_menu","journal_product_slider","journal_product_tabs","journal_rev_slider","journal_slider");
		// array_push($page_name, "EDIT-ME");
		if(array_intersect($page_name, $get_route)){
  			$is_custom_page = TRUE;
		} else {
  			$is_custom_page = FALSE;
		}

		$route = '';
		if (isset($this->request->get['route'])) {
		  	$part = explode('/', $this->request->get['route']);
			if (isset($part[0])) {
		    	$route .= $part[0];
		  	}

		  	if (isset($part[1])) {
		    	$route .= '/' . $part[1];
		  	}
		}
		$data['route'] = $route;
		$data['in'] = 0 ;
		if (isset($this->request->get['contactno'])){
			$data['contact'] = $this->request->get['contactno'];
			$data['in'] = 1;
		}
		if (isset($this->request->get['order_no'])){
			$data['order_no'] = $this->request->get['order_no'];
			$data['in'] = 1;
		}
		if (isset($this->request->get['dayclose'])){
			$data['order_no'] = $this->request->get['dayclose'];
			$data['in'] = 1;
		}
		if ($data['route'] == 'catalog/order' || $data['route'] == 'catalog/ordertab' || $data['route'] == 'catalog/orderqwerty' || $data['route'] == 'catalog/ordertouch' || $data['route'] == 'catalog/currentsale' || $data['route'] == 'catalog/kottransfer' || $data['route'] == 'catalog/nckot' || isset($this->request->get['iframe']) || $data['route'] == 'catalog/settlement' || $data['route'] == 'catalog/werafood_itemdata' || $data['route'] == 'catalog/billview' || $data['route'] == 'catalog/order_undo'){
			$data['in'] = 1;
		}

		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$this->load->language('common/header');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_order'] = $this->language->get('text_order');
		$data['text_processing_status'] = $this->language->get('text_processing_status');
		$data['text_complete_status'] = $this->language->get('text_complete_status');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_online'] = $this->language->get('text_online');
		$data['text_approval'] = $this->language->get('text_approval');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_stock'] = $this->language->get('text_stock');
		$data['text_review'] = $this->language->get('text_review');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_front'] = $this->language->get('text_front');
		$data['text_help'] = $this->language->get('text_help');
		$data['text_homepage'] = $this->language->get('text_homepage');
		$data['text_documentation'] = $this->language->get('text_documentation');
		$data['text_support'] = $this->language->get('text_support');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
		$data['text_logout'] = $this->language->get('text_logout');

		if (!isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$data['logged'] = '';

			$data['home'] = $this->url->link('catalog/order', '', true);
		} else {
			$data['logged'] = true;

			$data['home'] = $this->url->link('catalog/order', 'token=' . $this->session->data['token'], true);
			$data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], true);

			// Orders
			$this->load->model('sale/order');

			// Processing Orders
			$data['processing_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));
			$data['processing_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_processing_status')), true);

			// Complete Orders
			$data['complete_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));
			$data['complete_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_complete_status')), true);

			// Returns
			$this->load->model('sale/return');

			$return_total = $this->model_sale_return->getTotalReturns(array('filter_return_status_id' => $this->config->get('config_return_status_id')));

			$data['return_total'] = $return_total;

			$data['return'] = $this->url->link('sale/return', 'token=' . $this->session->data['token'], true);

			// Customers
			$this->load->model('report/customer');

			$data['online_total'] = $this->model_report_customer->getTotalCustomersOnline();

			$data['online'] = $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], true);

			$this->load->model('customer/customer');

			$customer_total = $this->model_customer_customer->getTotalCustomers(array('filter_approved' => false));

			$data['customer_total'] = $customer_total;
			$data['customer_approval'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&filter_approved=0', true);

			// Products
			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProducts(array('filter_quantity' => 0));

			$data['product_total'] = $product_total;

			$data['product'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&filter_quantity=0', true);

			// Reviews
			$this->load->model('catalog/review');

			$review_total = $this->model_catalog_review->getTotalReviews(array('filter_status' => false));

			$data['review_total'] = $review_total;

			$data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&filter_status=0', true);

			// Affliate
			$this->load->model('marketing/affiliate');

			$affiliate_total = $this->model_marketing_affiliate->getTotalAffiliates(array('filter_approved' => false));

			$data['affiliate_total'] = $affiliate_total;
			$data['affiliate_approval'] = $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . '&filter_approved=1', true);

			$data['alerts'] = $customer_total + $product_total + $review_total + $return_total + $affiliate_total;

			// Online Stores
			$data['stores'] = array();

			$data['stores'][] = array(
				'name' => $this->config->get('config_name'),
				'href' => HTTP_CATALOG
			);

			$data['merchant_id'] = MERCHANT_ID;
			$data['merchant_id_cancel'] = MERCHANT_ID_CANCEL;

			$this->load->model('setting/store');

			$results = $this->model_setting_store->getStores();

			foreach ($results as $result) {
				$data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}
		}
		if(isset($this->session->data['token'])){
			$data['token'] = $this->session->data['token'];
		} else {
			$data['token'] = '';
		}
		
		if(isset($this->session->data['superadmin'])){
			$data['superadmin'] = $this->session->data['superadmin'];
		} else{
			$this->session->data['superadmin'] = '';
		}
		unset($this->session->data['msr_no']);
		
		return $this->load->view('common/header', $data);
	}
}
