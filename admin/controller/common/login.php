<?php
class ControllerCommonLogin extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('common/login');
		$this->load->model('catalog/order');
		$notimp = DIR_ACTIVATE;

		$bill_date = $this->db->query("SELECT * FROM oc_order_info WHERE bill_date > '".date('Y-m-d')."' ORDER BY order_id DESC LIMIT 1");

		if (isset($this->session->data['warning1'])) {
			$data['warning'] = $this->session->data['warning1'];
			unset($this->session->data['warning1']);
		} else {
			$data['warning'] = '';
		}

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			
			/*$this->db->query("INSERT INTO oc_data_logs SET action='Loggedin'");	*/	

			$data['STARTUP_PAGE'] = $this->model_catalog_order->get_settings('STARTUP_PAGE');
			if($data['STARTUP_PAGE'] == '1'){
				$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
			} elseif($data['STARTUP_PAGE'] == '2'){
				$this->response->redirect($this->url->link('catalog/orderqwerty', 'token=' . $this->session->data['token'], true));
			} elseif($data['STARTUP_PAGE'] == '3'){
				$this->response->redirect($this->url->link('catalog/ordertab', 'token=' . $this->session->data['token'], true));
			} elseif($data['STARTUP_PAGE'] == '4'){
				$this->response->redirect($this->url->link('catalog/ordertouch', 'token=' . $this->session->data['token'], true));
			} elseif($data['STARTUP_PAGE'] == '5'){
				$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'], true));
			} else {
				$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
			}
		} else{

			if(isset($this->session->data['token']) && isset($this->session->data['user_id'])){

				//if(file_exists($notimp) && $bill_date->num_rows == 0){
					//unset($this->session->data['user_id']);
					//unset($this->session->data['token']);
				//} else{
				/*echo "inn8";
				exit();*/

					$data['STARTUP_PAGE'] = $this->model_catalog_order->get_settings('STARTUP_PAGE');
					if($data['STARTUP_PAGE'] == '1'){
						$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
					} elseif($data['STARTUP_PAGE'] == '2'){
						$this->response->redirect($this->url->link('catalog/orderqwerty', 'token=' . $this->session->data['token'], true));
					} elseif($data['STARTUP_PAGE'] == '3'){
						$this->response->redirect($this->url->link('catalog/ordertab', 'token=' . $this->session->data['token'], true));
					} elseif($data['STARTUP_PAGE'] == '4'){
						$this->response->redirect($this->url->link('catalog/ordertouch', 'token=' . $this->session->data['token'], true));
					} elseif($data['STARTUP_PAGE'] == '5'){
						$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'], true));
					} else {
						$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
					}
				//}
			} else{
				/*echo "inn7";
				exit();*/
				$this->user->logout();
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$logout_url = 'http://localhost:8000/Swiggy/api/Logout';
			$logout = curl_init($logout_url);
			curl_setopt($logout, CURLOPT_TIMEOUT, 30);
			//curl_setopt($logout, CURLOPT_RETURNTRANSFER , 1);
			curl_exec($logout);

			$login_url = 'http://localhost:8000/Swiggy/api/Login';
			$login = curl_init($login_url);
			curl_setopt($login, CURLOPT_TIMEOUT, 30);
			//curl_setopt($login, CURLOPT_RETURNTRANSFER , 1);
			curl_exec($login);

			$this->session->data['token'] = token(32);

			$this->session->data['superadmin'] = 0;
			
			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) === 0 || strpos($this->request->post['redirect'], HTTPS_SERVER) === 0)) {
				//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
				$data['STARTUP_PAGE'] = $this->model_catalog_order->get_settings('STARTUP_PAGE');

				/*echo "inn66";
				exit();*/
				$this->db->query("INSERT INTO oc_data_logs SET action='Logged In',date='".date("Y-m-d")."',time='".date("H.i.s")."',user_id='".$this->session->data['user_id']."',user_name='".$this->user->getUserName()."',ip_address='".$_SERVER['REMOTE_ADDR']."'");	
				if($data['STARTUP_PAGE'] == '1'){ 
					$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '2'){
					$this->response->redirect($this->url->link('catalog/orderqwerty', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '3'){
					$this->response->redirect($this->url->link('catalog/ordertab', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '4'){
					$this->response->redirect($this->url->link('catalog/ordertouch', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '5'){
					$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'], true));
				} else {
				 	$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
				}
			} else {
				if($data['STARTUP_PAGE'] == '1'){
					$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '2'){
					$this->response->redirect($this->url->link('catalog/orderqwerty', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '3'){
					$this->response->redirect($this->url->link('catalog/ordertab', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '4'){
					$this->response->redirect($this->url->link('catalog/ordertouch', 'token=' . $this->session->data['token'], true));
				} elseif($data['STARTUP_PAGE'] == '5'){
					$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'], true));
				} else {
				 	$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
				}
			}
		} elseif(isset($this->request->get['user_id'])){
			$userid = $this->request->get['user_id'];
			$user_name = $this->db->query("SELECT `username` FROM `oc_user` WHERE user_id = '".$userid."'")->row;	
			$username = $user_name['username'];

			$this->session->data['superadmin'] = 1;

			if($this->user->login($username, '', true)){
				$this->session->data['token'] = token(32);
				$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_login'] = $this->language->get('text_login');
		$data['text_forgotten'] = $this->language->get('text_forgotten');

		$data['entry_username'] = $this->language->get('entry_username');
		$data['entry_password'] = $this->language->get('entry_password');

		$data['button_login'] = $this->language->get('button_login');

		if ((isset($this->session->data['token']) && !isset($this->request->get['token'])) || ((isset($this->request->get['token']) && (isset($this->session->data['token']) && ($this->request->get['token'] != $this->session->data['token']))))) {
			$this->error['warning'] = $this->language->get('error_token');
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('common/login', '', true);

		if (isset($this->request->post['username'])) {
			$data['username'] = $this->request->post['username'];
		} else {
			$data['username'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);
			unset($this->request->get['token']);

			$url = '';

			if ($this->request->get) {
				$url .= http_build_query($this->request->get);
			}

			$data['redirect'] = $this->url->link($route, $url, true);
		} else {
			$data['redirect'] = '';
		}

		if ($this->config->get('config_password')) {
			$data['forgotten'] = $this->url->link('common/forgotten', '', true);
		} else {
			$data['forgotten'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/login', $data));
	}

	protected function validate() {
		$notimp = DIR_ACTIVATE;
		$localfile = file_get_contents($notimp);
		$localdata = explode('|', $localfile);
		$bill_date = $this->db->query("SELECT * FROM oc_order_info WHERE bill_date > '".date('Y-m-d')."' ORDER BY order_id DESC LIMIT 1");
		if(file_exists($notimp)){
			if(isset($localdata[2]) && strtotime($localdata[2]) >= strtotime(date('Y-m-d'))){
				$computerkey = shell_exec('wmic DISKDRIVE GET SerialNumber');
				$encryptcomputerkey = md5($computerkey);
				$prefix = md5(1000);
				$postfix = md5(2000);
				$result = $prefix.$encryptcomputerkey.$postfix;
				$localfile = file_get_contents(DIR_ACTIVATE);
				$localdata = explode('|', $localfile);
				if($localdata[0] != $result){
					$this->error['warning'] = "<a href='activate.php'>Activate Your Product</a>";
				} else {
					if (!isset($this->request->post['username']) || !isset($this->request->post['password']) || !$this->user->login($this->request->post['username'], html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8'))) {
						$this->error['warning'] = $this->language->get('error_login');
					}
				}
			} else {
				$this->error['warning'] = "<a href='activate.php?expire=1'>Activate Your Product</a>";
			}
		} else {
			$this->error['warning'] = "<a href='activate.php'>Activate Your Product</a>";
		}
		if($bill_date->num_rows > 0){
			$this->error['warning'] = "Please change system date";
		} 
		return !$this->error;
	}
}
