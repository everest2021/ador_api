<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token']) && (!isset($this->request->get['iframe']))) {
			
			if(isset($this->request->get['route']) && ($this->request->get['route'] == 'catalog/orderprocess/getForm' || $this->request->get['route'] == 'catalog/orderprocess' || $this->request->get['route'] == 'catalog/orderprocess/add') || ($this->request->get['route'] == 'catalog/kitchen_display/getForm' || $this->request->get['route'] == 'catalog/kitchen_display' || $this->request->get['route'] == 'catalog/kitchen_display/add') ||($this->request->get['route'] == 'catalog/orderprocess1/getForm' || $this->request->get['route'] == 'catalog/orderprocess1' || $this->request->get['route'] == 'catalog/orderprocess1/add')){
				$data['hide'] = 1;
			} else {
				$data['hide'] = 0;
			}

			$data['profile'] = $this->load->controller('common/profile');
			$data['menu'] = $this->load->controller('common/menu');
			$data['stats'] = $this->load->controller('common/stats');

			return $this->load->view('common/column_left', $data);
		}
	}
}