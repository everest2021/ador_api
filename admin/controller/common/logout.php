<?php
class ControllerCommonLogout extends Controller {
	public function index() {
		$this->db->query("INSERT INTO oc_data_logs SET action='Logged Out',date='".date("Y-m-d")."',time='".date("H.i.s")."',user_id='".$this->session->data['user_id']."',user_name='".$this->user->getUserName()."',ip_address='".$_SERVER['REMOTE_ADDR']."'");
		$this->user->logout();
		unset($this->session->data['token']);
		unset($this->session->data['superadmin']);
		
		$this->response->redirect($this->url->link('common/login', '', true));
	}
}