<?php
class ControllerCatalogBomLazer extends Controller {
	private $error = array();
	// echo'<pre>';
	// print_r($controller);exit;

	public function index() {
		$this->load->language('catalog/bomlazer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/bomlazer');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/bomlazer', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		} else {
			$data['startdate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		} else {
			$data['storename'] = '';
		}	
		
		$data['final_data'] =array();
		$bom_data = array();
		$final_data = array();
		$bom_total = array();
		$bom_item = array();
		$bom_data = array();
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) && !empty($this->request->post['filter_storename'])){
			$startdate = date('Y-m-d', strtotime($this->request->post['filter_startdate']));
			$enddate =  date('Y-m-d', strtotime($this->request->post['filter_enddate']));
			$storecode = $this->request->post['filter_storename'];
			$all_data = $this->db->query("SELECT *,SUM(quantity) as total_quantity FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE po_date >= '".$startdate."' AND po_date <= '".$enddate."' AND outlet_id = '".$storecode."' GROUP BY item_id")->rows;
			$final_data = array();
			$item_id = array();
			$i =1;
			foreach ($all_data as $akey => $avalue) {
				$item_id[] = $avalue['item_id'];
			}
			$item_id = implode( ", ", $item_id);
			$bom_data_items = $this->db->query("SELECT * FROM oc_bom_items WHERE parent_item_code IN('".$item_id."') GROUP BY item_code")->rows;
			foreach($bom_data_items as $bom_data_item){
				$bom_item[$bom_data_item['item_code']] = array(
					'name'   	=> $bom_data_item['item_name'],
					'total'		=> 0
				);
			}

			foreach ($all_data as $akey => $avalue) {
				$bom_data = array();
				foreach($bom_data_items as $stock_item){
					$bomdatas = $this->db->query("SELECT * FROM oc_bom_items WHERE parent_item_code = '".$avalue['item_id']."' AND item_code = '".$stock_item['item_code']."'");
					if($bomdatas->num_rows > 0){
						foreach($bomdatas->rows as $bomdata){
							$bom_qty = $bomdata['qty'] * $avalue['total_quantity'];
							$bom_data[] = array(
								'quantity' 	=> $bom_qty
							);
							$bom_item[$bomdata['item_code']]['total'] += $bomdata['qty'] * $avalue['total_quantity'];
						}
					} else{
						$bom_data[] = array(
							'quantity' => 0
						);
					}
				}

				$final_data[]= array(
					'i' => $i,
					'item_name'	=> $avalue['item_name'],
					'bom_data'	=> $bom_data
				);
				$i ++ ;
			}
		}

		$data['final_data'] = $final_data;
		$data['bom_items'] = $bom_item;
		$data['bom_totals'] = $bom_total;
		// $total_quantity = array();
		// foreach($final_data as $fkey =>$fvalue) {
		// 	foreach ($fvalue['bom_data'] as $lkey => $result) {
		// 		$total_quantity[] = array(
		// 			'quantity' 	=> $result['quantity'],
		// 			'code'		=> $result['code']
		// 		);
		// 		if(isset($total_quantity['code'])){
		// 			$total_quantity['quantity'] += $total_quantity['quantity'];
		// 		} else{
		// 			$total_quantity['quantity'] = $total_quantity['quantity'];
		// 		}
		// 	}
		// }

		$data['stores'] = $this->db->query("SELECT * FROM oc_outlet")->rows;
		$data['raw_material'] = $this->db->query("SELECT * FROM oc_bom_items GROUP BY item_code")->rows;
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/bomlazer', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/bomlazer', $data));
	}
}