<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogCreditReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/creditreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/creditreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/creditreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['creditcust'])){
			$data['creditcust'] = $this->request->post['creditcust'];
		}
		else{
			$data['creditcust'] = '';
		}

		if(isset($this->request->post['creditid'])){
			$data['creditid'] = $this->request->post['creditid'];
		}
		else{
			$data['creditid'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';
		$creditdata = array();
		$credittotal = 0;
		$allcredits = array();
		$data['allcredits'] = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			$final_datas = array();
			
				if(empty($this->request->post['creditid'])){
					$customerdatas = $this->db->query("SELECT * FROM oc_customerinfo WHERE 1=1");
					foreach($customerdatas->rows as $customerdata){
						$debit_datas = array();
						foreach($dates as $date){
							$orderdatas = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND onac <> 0.00 AND onaccust <> '' AND onaccust = '".$customerdata['c_id']."'GROUP BY billno ASC")->rows;
							foreach($orderdatas as $orderdata){
								//$custdata = $this->db->query("SELECT customer_name,contact FROM oc_customercredit WHERE c_id = '".$orderdata['onaccust']."'")->row;
								$orderdata['onacname'] = trim($orderdata['onacname']);
								$debit_datas[$date][] = array(
									'billno'	=> $orderdata['billno'],
									'debit'		=> $orderdata['onac'],
									'name'		=> $orderdata['onacname'],
									'contact'	=> $orderdata['onaccontact']
								);
							}
						}
						$creditdatas = $this->db->query("SELECT * FROM oc_customercredit WHERE c_id = '".$customerdata['c_id']."'");
						$credit_datas = array();
						if($creditdatas->num_rows > 0){
							foreach($creditdatas->rows as $key){
								$credit_datas[] = array(
									'credit' => $key['credit'],
									'customer_name' => $key['customer_name'],
									'contact' => $key['contact'],
									'date' => $key['date']
							  	);
							}
						}
						if(!empty($debit_datas) || !empty($credit_datas)){
							$final_datas[$customerdata['c_id']] = array(
								'name' => $customerdata['name'],
								'credit_datas' => $credit_datas,
								'debit_datas' => $debit_datas,
							);
						}	
					}
					// echo '<pre>';
					// print_r($final_datas);
					// exit;
					$data['final_datas'] = $final_datas;
				} else{
					$debit_datas = array();
					foreach($dates as $date){
						$orderdatas = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND onac <> 0.00 AND onaccust <> '' AND onaccust = '".$this->request->post['creditid']."'GROUP BY billno ASC")->rows;
						foreach($orderdatas as $orderdata){
							//$custdata = $this->db->query("SELECT customer_name,contact FROM oc_customercredit WHERE c_id = '".$orderdata['onaccust']."'")->row;
							$orderdata['onacname'] = trim($orderdata['onacname']);
							$debit_datas[$date][] = array(
								'billno'	=> $orderdata['billno'],
								'debit'		=> $orderdata['onac'],
								'name'		=> $orderdata['onacname'],
								'contact'	=> $orderdata['onaccontact']
							);
						}
					}
					$creditdatas = $this->db->query("SELECT * FROM oc_customercredit WHERE c_id = '".$this->request->post['creditid']."'");
					$credit_datas = array();
					if($creditdatas->num_rows > 0){
						foreach($creditdatas->rows as $key){
							$credit_datas[] = array(
								'credit' => $key['credit'],
								'customer_name' => $key['customer_name'],
								'contact' => $key['contact'],
								'date' => $key['date']
						  	);
						}
					}

					$customer = $this->db->query("SELECT name FROM oc_customerinfo WHERE c_id = '".$this->request->post['creditid']."'");
					if($customer->num_rows > 0){
						$customername = $customer->row['name'];
					} else{
						$customername = '';
					}

					if(!empty($debit_datas) || !empty($credit_datas)){
						$final_datas[$this->request->post['creditid']] = array(
							'name' => $customername,
							'credit_datas' => $credit_datas,
							'debit_datas' => $debit_datas,
						);
					}	
					// echo '<pre>';
					// print_r($final_datas);
					// exit;
					$data['final_datas'] = $final_datas;
				}
		// echo "<pre>";
		// print_r($creditdata);
		// exit();
		}
	
		$data['billdatas'] = $creditdata;

		// echo "<pre>";
		// print_r($creditdata);
		// exit();

		$data['action'] = $this->url->link('catalog/creditreport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/creditreport', $data));
	}

	public function name(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE name LIKE '%".$this->request->get['filter_name']."%' OR contact LIKE '%".$this->request->get['filter_name']."%' ORDER BY c_id DESC LIMIT 10")->rows;
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'contact'        => $result['contact'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),

				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}