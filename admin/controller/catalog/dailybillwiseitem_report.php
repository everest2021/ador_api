<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogDailyBillwiseitemreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/billwiseitem_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/dailybillwiseitem_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		// echo'<pre>';
		// print_r($this->request->post);
		// exit;

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_billno'])){
			$data['billno'] = $this->request->post['filter_billno'];
		}
		else{
			$data['billno'] = '';
		}


		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$final_datas = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$billno =  $this->request->post['filter_billno'];
			
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			
			//$billdata = $this->db->query("SELECT * FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = 0 ")->rows;
			$sql = "SELECT * FROM `oc_order_info` WHERE  1=1";
			if (!empty($start_date)) {
				$sql .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND `order_no` = '" . $this->db->escape($billno) . "'";
			}
			
				$sql .=" AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = '0' AND `complimentary_status` = '0'";
			$billdata = $this->db->query($sql)->rows;
			// echo'<pre>';
			// print_r($sql);
			// exit;
			$b_count = 1;
			foreach($billdata as $bkey => $bvalue){

				//$item_datas = $this->db->query("SELECT * FROM `oc_order_items` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND order_id = '".$bvalue['order_id']."' AND cancelstatus = 0")->rows;
				$sql1 = "SELECT * FROM `oc_order_items` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$bvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'out_time' =>$bvalue['out_time'],
						'urbanpiper_order_id' 	=> $bvalue['urbanpiper_order_id'],
						'order_no' => $bvalue['order_no'],
						'wera_order_id' 	=> $bvalue['wera_order_id'],
						'order_from' => $bvalue['order_from'],
						'order_id' => $bvalue['order_id'],
						'food_cancel' => $bvalue['food_cancel'],
						'ftotal' => $bvalue['ftotal'],
						'gst' => $bvalue['gst'],
						'ftotalvalue' => $bvalue['ftotalvalue'],
						'liq_cancel' => $bvalue['liq_cancel'],
						'ltotal' => $bvalue['ltotal'],
						'vat' => $bvalue['vat'],
						'ltotalvalue' => $bvalue['ltotalvalue'],
						'grand_total' => $bvalue['grand_total'],
						'stax' => $bvalue['stax'],
						'pay_cash' => $bvalue['pay_cash'],
						'pay_online' => $bvalue['pay_online'],
						'packaging' 	=> $bvalue['packaging'],
						'packaging_cgst' 	=> $bvalue['packaging_cgst'],
						'packaging_sgst'	=> $bvalue['packaging_sgst'],
						'pay_card' => $bvalue['pay_card'],
						'onac' => $bvalue['onac'],
						'mealpass' => $bvalue['mealpass'],
						'pass' => $bvalue['pass'],
						't_name' => $bvalue['t_name'],
						'roundtotal' => $bvalue['roundtotal'],
						'advance_amount' => $bvalue['advance_amount'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}
			// echo'<pre>';
			// 	print_r($final_datas);
			// 	exit();
			

			//$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			
			$sql = "SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND oi.`order_no` = '" . $this->db->escape($billno) . "'";
			}
			
			$sql .=" AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'";
			$cancelbillitem = $this->db->query($sql)->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
			$nc_kot_sql = $this->db->query("SELECT  SUM(oit.`qty` * oit.`rate`)  AS nc_kot_statuss FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' AND oit.`qty` > 0 AND oit.`nc_kot_status` > 0 ");

			if($nc_kot_sql->num_rows > 0){
				$data['nc_kot_amt'] = $nc_kot_sql->row['nc_kot_statuss'];
			} 
				
		}
		// echo "<pre>";
		// print_r($data);
		// exit;
		$data['billdatas'] = $final_datas;
		//$data['filter_billno'] = $filter_billno;
		
		$data['action'] = $this->url->link('catalog/dailybillwiseitem_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/dailybillwiseitem_report', $data));
	}

	public function prints() {
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$billdatas = array();
		$nc_kot_amt = 0;
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$cancelamount = $cancelbillitem['total_amt'];

			$nc_kot_sql = $this->db->query("SELECT  SUM(oit.`qty` * oit.`rate`)  AS nc_kot_statuss FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' AND oit.`qty` > 0 AND oit.`nc_kot_status` > 0 ");

			if($nc_kot_sql->num_rows > 0){
				$nc_kot_amt = $nc_kot_sql->row['nc_kot_statuss'];
			} 
				

			$discount = 0;
			$grandtotal = 0;
			$vat = 0; 
			$gst = 0; 
			$discountvalue = 0; 
			$afterdiscount = 0; 
			$stax = 0; 
			$roundoff = 0; 
			$total = 0; 
			$advance = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Bill Wise Sales Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			  	$printer->text(str_pad("Ref No",7)."".str_pad("Food",7)."".str_pad("Bar",7)."".str_pad("Disc",7).""
				  		.str_pad("Total",7)."".str_pad("Pay By",8)."T No");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($billdata as $key => $value){
			  		$printer->setJustification(Printer::JUSTIFY_CENTER);
			  		$printer->text($key);
			  		$printer->feed(1);
			  		$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	foreach ($value as $data) {
				  		if($data['food_cancel'] == 1) {
				  			$data['ftotal'] = 0;
				  		}
				  		if($data['liq_cancel'] == 1) {
				  			$data['ltotal'] = 0;
				  		}
				  		if($data['pay_cash'] != '0') {
				  			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Card",8).$data['t_name']);
					  	}else if($data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("OnAc",8).$data['t_name']);
					  	}else if($data['mealpass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Meal Pass",8).$data['t_name']);
					  	}else if($data['pass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pass",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash+Card",8).$data['t_name']);
					  	} else if($data['pay_online'] != '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
								$total = round($data['ftotal'] + $data['ltotal']);
							} else {
								$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
							}
							if($data['urbanpiper_order_id'] != ''){
								$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad($data['order_from'],8).$data['t_name']);
							} else {
								$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Online",8).$data['t_name']);
							}
						} else{
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pending",8).$data['t_name']);
					  	}
				  		$printer->feed(1);
				  		  if($data['liq_cancel'] == 1){
				  		  	$vat = 0;
				  		  } else{
				  		  	$vat = $vat + $data['vat'];
				  		  }
				  		  if($data['food_cancel'] == 1){
				  		  	$gst = 0;
				  		  } else{
				  		  	$gst = $gst + $data['gst'];
				  		  }
				  		  $packaging = $packaging + $data['packaging'];
				  		  $stax = $stax + $data['stax'];
				  		  if($data['food_cancel'] == 1){
				  		  	$data['ftotalvalue'] = 0; 
				  		  } 
				  		  if($data['liq_cancel'] == 1){
				  		  	$data['ltotalvalue'] = 0;
				  		  }
				  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
				  		  if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
				  		  	$total = $grandtotal + $stax;
				  		  } else{
			  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
			  		  	 	$total = $grandtotal + $gst + $vat + $stax; 
				  		  }
				  		  $discountvalue = $discountvalue + $discount;
				  		  $afterdiscount = $total - $discountvalue;
				  		  $roundoff = $roundoff + $data['roundtotal'];
				  		  $advance = $advance + $data['advance_amount'];
				  	}
				}
			  	$printer->setEmphasis(false);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Bill Total :",20)."".$grandtotal);
			  	$printer->setEmphasis(false);
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("O- Chrg Amt (+) :",20)."0");
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("Vat Amt (+) :",20)."".$vat);
			  	$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("S- Chrg Amt (+) :",20)."".$stax);
	  			$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("GST Amt (+) :",20)."".$gst);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Packaging AMT (+) :",20)."". $packaging);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("SBC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("R-Off Amt (+) :",20)."".$roundoff);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Total :",20)."".($total + $roundoff));
			  	$printer->feed(1);
			  	$printer->setEmphasis(false);
			  	$printer->text(str_pad("",20)."".str_pad("Discount Amt (-) :",20)."".$discountvalue);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Cancel Bill Amt (-) :",20).$cancelamount);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Comp. Amt (-) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance. Amt (-) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("NC KOT :",20).$nc_kot_amt);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Net Amount :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance Amt (+) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Grand Total (+) :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}

	public function export(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$sql = "SELECT * FROM `oc_order_info` WHERE  1=1";
			if (!empty($start_date)) {
				$sql .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND `order_no` = '" . $this->db->escape($billno) . "'";
			}
			
				$sql .=" AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = '0' AND `complimentary_status` = '0'";
			$billdata = $this->db->query($sql)->rows;
			// echo'<pre>';
			// print_r($sql);
			// exit;
			$b_count = 1;
			foreach($billdata as $bkey => $bvalue){

				//$item_datas = $this->db->query("SELECT * FROM `oc_order_items` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND order_id = '".$bvalue['order_id']."' AND cancelstatus = 0")->rows;
				$sql1 = "SELECT * FROM `oc_order_items` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$bvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'order_no' => $bvalue['order_no'],
						'out_time' =>$bvalue['out_time'],

						'wera_order_id' 	=> $bvalue['wera_order_id'],
						'order_from' => $bvalue['order_from'],
						
						'order_id' => $bvalue['order_id'],
						'food_cancel' => $bvalue['food_cancel'],
						'ftotal' => $bvalue['ftotal'],
						'gst' => $bvalue['gst'],
						'ftotalvalue' => $bvalue['ftotalvalue'],
						'liq_cancel' => $bvalue['liq_cancel'],
						'ltotal' => $bvalue['ltotal'],
						'vat' => $bvalue['vat'],
						'ltotalvalue' => $bvalue['ltotalvalue'],
						'grand_total' => $bvalue['grand_total'],
						'stax' => $bvalue['stax'],
						'pay_cash' => $bvalue['pay_cash'],
						'pay_card' => $bvalue['pay_card'],
						'pay_online' => $bvalue['pay_online'],
						'packaging' 	=> $bvalue['packaging'],
						'packaging_cgst' 	=> $bvalue['packaging_cgst'],
						'packaging_sgst'	=> $bvalue['packaging_sgst'],
						'onac' => $bvalue['onac'],
						'mealpass' => $bvalue['mealpass'],
						'pass' => $bvalue['pass'],
						't_name' => $bvalue['t_name'],
						'roundtotal' => $bvalue['roundtotal'],
						'advance_amount' => $bvalue['advance_amount'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}
			// echo'<pre>';
			// 	print_r($final_datas);
			// 	exit();
			

			//$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			
			$sql = "SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND oi.`order_no` = '" . $this->db->escape($billno) . "'";
			}
			
				$sql .=" AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'";
			$cancelbillitem = $this->db->query($sql)->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}

			$nc_kot_sql = $this->db->query("SELECT  SUM(oit.`qty` * oit.`rate`)  AS nc_kot_statuss FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' AND oit.`qty` > 0 AND oit.`nc_kot_status` > 0 ");

			if($nc_kot_sql->num_rows > 0){
				$data['nc_kot_amt'] = $nc_kot_sql->row['nc_kot_statuss'];
			} 
				
		}
		// echo "<pre>";
		// print_r($item_datas);
		// exit;
		$data['billdatas'] = $final_datas;
		$data['action'] = $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/dailybillwiseitem_export_report_html', $data);
		
		$filename = 'BillWise Item Report.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;

	}

	public function excel(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['nc_kot_amt'] = 0;
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$sql = "SELECT * FROM `oc_order_info` WHERE  1=1";
			if (!empty($start_date)) {
				$sql .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND `order_no` = '" . $this->db->escape($billno) . "'";
			}
			
				$sql .=" AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = '0' AND `complimentary_status` = '0'";
			$billdata = $this->db->query($sql)->rows;
			// echo'<pre>';
			// print_r($sql);
			// exit;
			$b_count = 1;
			foreach($billdata as $bkey => $bvalue){

				//$item_datas = $this->db->query("SELECT * FROM `oc_order_items` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND order_id = '".$bvalue['order_id']."' AND cancelstatus = 0")->rows;
				$sql1 = "SELECT * FROM `oc_order_items` WHERE 1=1 ";
				if (!empty($start_date)) {
					$sql1 .= " AND `bill_date` >= '" . $this->db->escape($start_date) . "'";
				}
				if (!empty($end_date)) {
					$sql1 .= " AND `bill_date` <= '" . $this->db->escape($end_date) . "'";
				}

				$sql1 .= "AND order_id = '".$bvalue['order_id']."' ";

				$sql1 .=" AND cancelstatus = '0' ";
				$item_datas = $this->db->query($sql1)->rows;


				$sub_datas = array();
				$sr_no = 1;
				foreach($item_datas as $ikey => $ivalue){
					$sub_datas[] = array(
						'kot_no' => $ivalue['kot_no'],
						'name' => $ivalue['name'],
						'rate' => $ivalue['rate'],
						'qty' => $ivalue['qty'],
						'amt' => $ivalue['amt'],
						'sr_no' => $sr_no,
					);
					$sr_no ++;
				}

				$final_datas[] = array(
						'b_count' => $b_count,
						'out_time' =>$bvalue['out_time'],
						
						'order_no' => $bvalue['order_no'],
						'order_from' => $bvalue['order_from'],
						
						'order_id' => $bvalue['order_id'],
						'food_cancel' => $bvalue['food_cancel'],
						'ftotal' => $bvalue['ftotal'],
						'gst' => $bvalue['gst'],
						'ftotalvalue' => $bvalue['ftotalvalue'],
						'liq_cancel' => $bvalue['liq_cancel'],
						'ltotal' => $bvalue['ltotal'],
						'vat' => $bvalue['vat'],
						'ltotalvalue' => $bvalue['ltotalvalue'],
						'grand_total' => $bvalue['grand_total'],
						'stax' => $bvalue['stax'],
						'pay_cash' => $bvalue['pay_cash'],
						'pay_card' => $bvalue['pay_card'],
						'pay_online' => $bvalue['pay_online'],
						'packaging' 	=> $bvalue['packaging'],
						'packaging_cgst' 	=> $bvalue['packaging_cgst'],
						'packaging_sgst'	=> $bvalue['packaging_sgst'],
						'wera_order_id' 	=> $bvalue['wera_order_id'],
						'onac' => $bvalue['onac'],
						'mealpass' => $bvalue['mealpass'],
						'pass' => $bvalue['pass'],
						't_name' => $bvalue['t_name'],
						'roundtotal' => $bvalue['roundtotal'],
						'advance_amount' => $bvalue['advance_amount'],
						'sub_data' =>$sub_datas,
					);
				$b_count ++;
			}
			// echo'<pre>';
			// 	print_r($final_datas);
			// 	exit();
			

			//$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			
			$sql = "SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE 1=1 ";
			if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND oi.`order_no` = '" . $this->db->escape($billno) . "'";
			}
			
				$sql .=" AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'";
			$cancelbillitem = $this->db->query($sql)->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}

			$nc_kot_sql = $this->db->query("SELECT  SUM(oit.`qty` * oit.`rate`)  AS nc_kot_statuss FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' AND oit.`qty` > 0 AND oit.`nc_kot_status` > 0 ");

			if($nc_kot_sql->num_rows > 0){
				$data['nc_kot_amt'] = $nc_kot_sql->row['nc_kot_statuss'];
			} 
				
		}
		// echo "<pre>";
		// print_r($item_datas);
		// exit;
		$data['billdatas'] = $final_datas;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/dailybillwiseitem_report_html', $data);
		//$html = $this->load->view('catalog/captain_and_waiter_html.tpl', $data); // exit;
	 	$filename = "Billwise Item Report.html";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		
		// $filename = 'BillWiseReport.html';
		// header('Content-disposition: attachment; filename=' . $filename);
		// header('Content-type: text/html');
		// echo $html;exit;

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}