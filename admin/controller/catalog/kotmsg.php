<?php
class ControllerCatalogKotmsg extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/kotmsg');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotmsg');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/kotmsg');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotmsg');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_kotmsg->addWaiter($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['filter_id'])) {
				$url .= '&filter_id=' . $this->request->get['filter_id'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/kotmsg');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotmsg');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_kotmsg->editWaiter($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['filter_waiter_id'])) {
				$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/kotmsg');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotmsg');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_kotmsg->deleteWaiter($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_msg_code'])) {
				$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
			}

			if (isset($this->request->get['id'])) {
				$url .= '&id=' . $this->request->get['id'];
			}

			if (isset($this->request->get['filter_message'])) {
				$url .= '&filter_message=' . $this->request->get['filter_message'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_msg_code'])) {
			$filter_msg_code = $this->request->get['filter_msg_code'];
		} else {
			$filter_msg_code = null;
		}

		if (isset($this->request->get['filter_message'])) {
			$filter_message = $this->request->get['filter_message'];
		} else {
			$filter_message = null;
		}

		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'msg_code';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_msg_code'])) {
			$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['filter_message'])) {
			$url .= '&filter_message=' . $this->request->get['filter_message'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/kotmsg/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/kotmsg/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_msg_code' => $filter_msg_code,
			'filter_message' => $filter_message,
			'filter_id' => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_kotmsg->getTotalWaiter();

		$results = $this->model_catalog_kotmsg->getWaiters($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$data['waiters'][] = array(
				'filter_msg_code' => $result['msg_code'],
				'filter_message'        => $result['message'],
				'filter_id'  => $result['id'],
				'edit'        => $this->url->link('catalog/kotmsg/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_message'])) {
			$url .= '&filter_message=' . $this->request->get['filter_message'];
		}

		if (isset($this->request->get['filter_msg_code'])) {
			$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        $data['sort_message'] = $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . '&sort=message' . $url, true);
        $data['sort_msg_code'] = $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . '&sort=msg_code' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_message'])) {
			$url .= '&filter_message=' . $this->request->get['filter_message'];
		}

		if (isset($this->request->get['filter_msg_code'])) {
			$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_message'] = $filter_message;
		$data['filter_msg_code'] = $filter_msg_code;
		$data['filter_id'] = $filter_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kotmsg_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}



		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_msg_code'])) {
			$url .= '&filter_msg_code=' . $this->request->get['filter_msg_code'];
		}

		if (isset($this->request->get['filter_message'])) {
			$url .= '&filter_message=' . $this->request->get['filter_message'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/kotmsg/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/kotmsg/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/kotmsg', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$waiter_info = $this->model_catalog_kotmsg->getWaiter($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		$i_code = $this->db->query(" SELECT msg_code FROM oc_kotmsg ORDER BY msg_code DESC limit 0,1 ");
		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['msg_code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }

		if (isset($this->request->get['msg_code'])) {
			$data['msg_code'] = $this->request->get['msg_code'];
		} elseif (!empty($waiter_info)) {
			$data['msg_code'] = $waiter_info['msg_code'];
		} else {
			$data['msg_code'] = $new_code;
		}

		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} elseif (!empty($waiter_info)) {
			$data['message'] = $waiter_info['message'];
		} else {
			$data['message'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kotmsg_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/kotmsg')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['message']) < 2) || (utf8_strlen($this->request->post['message']) > 255)) {
			$this->error['message'] = 'Enter valid message';
		} 

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/waiter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_message'])) {
			$this->load->model('catalog/kotmsg');

			$filter_data = array(
				'filter_message' => $this->request->get['filter_message'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_kotmsg->getWaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'message'        => strip_tags(html_entity_decode($result['message'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['message'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}