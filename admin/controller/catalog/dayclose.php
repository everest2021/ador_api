<?php
class ControllerCatalogDayclose extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle('Day Close');
		$this->getForm();
	}

	public function new_mysql($sql) {
		$con=mysqli_connect(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		mysqli_multi_query($con,$sql);
		/*do {
		   if($result = mysqli_store_result($con)){
			   mysqli_free_result($result);
		   }
		} while(mysqli_next_result($con));
		if(mysqli_error($con)) {
		   die(mysqli_error($con));
		}*/
		// while (mysqli_next_result($link)) {
		// if (!mysqli_more_results()){
		// break;
		// }
		// }
		mysqli_close($con);
	}


	public function edit() {
		$this->load->language('catalog/table');
		$this->document->setTitle('Bill Merge');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//$this->db->query("UPDATE `oc_order_info` SET `day_close_status` = '1' WHERE bill_date = '" . $this->request->post['open_date'] . "' ");
			
			$sql = "INSERT INTO `oc_order_info_report`(`order_id`, `app_order_id`, `kot_no`, `order_no`, `merge_number`, `location`, `location_id`, `t_name`,`table_id`, `waiter_code`, `waiter`, `waiter_id`, `captain_code`, `captain`, `captain_id`, `person`, `ftotal`, `ftotal_discount`, `gst`, `ltotal`, `ltotal_discount`, `vat`, `cess`, `staxfood`, `staxliq`, `stax`, `ftotalvalue`, `fdiscountper`, `discount`, `ldiscount`, `ldiscountper`, `ltotalvalue`, `dtotalvalue`, `dchargeper`, `dcharge`, `date`, `time`, `bill_status`, `payment_status`, `cust_name`, `cust_id`, `cust_contact`, `cust_address`, `cust_email`, `gst_no`, `parcel_status`, `day_close_status`, `bill_date`, `rate_id`, `grand_total`, `advance_billno`, `advance_amount`, `roundtotal`, `total_items`, `item_quantity`, `date_added`, `time_added`, `out_time`, `nc_kot_status`, `year_close_status`, `cancel_status`, `pay_cash`, `pay_card`, `card_no`, `creditcustname`, `mealpass`, `pass`, `roomservice`,`roomno` ,`msrcard`,`msrcardno`,`msrcustname`,`msr_cust_id`, `onac`,`onaccust`,`onaccontact`,`onacname`,`total_payment`,`pay_method`,`bk_status`,`login_id`,`login_name`,`food_cancel`,`liq_cancel`,`modify_remark`,`duplicate`,`duplicate_time`,`printstatus`,`tip`,`pay_online`,`msrcardname`,`delivery_charges`,`advance_id`,`complimentary_status`,`complimentary_resion`,`new_pay_cash`,`new_pay_card`,`new_pay_online`,`new_total_payment`,`shiftclose_status`,`shift_id`,`shift_date`,`shift_time`,`payment_type`,`shift_username`,`shift_user_id`,`report_status`, `wera_order_id`, `cancel_bill_reason`, `discount_reason`, `cancel_kot_reason`, `order_from`, `order_packaging`, `packaging_cgst`, `packaging_sgst`, `packaging_cgst_percent`,`packaging_sgst_percent`, `packaging`, `urbanpiper_order_id`) VALUES";
			
			$order_datas = $this->db->query(" SELECT * FROM `oc_order_info` WHERE bill_date = '".$this->request->post['open_date']."' ")->rows;
			$inn = 0;
			foreach ($order_datas as $okey => $ovalues) {
				$is_exist = $this->db->query(" SELECT * FROM `oc_order_info_report` WHERE order_id = '".$ovalues['order_id']."' ");
				if($is_exist->num_rows == 0){

					$sql .= " ('" . $this->db->escape($ovalues['order_id']) . "','" . $this->db->escape($ovalues['app_order_id']) . "','".$this->db->escape($ovalues['kot_no'])."','" . $this->db->escape($ovalues['order_no']) . "','" . $this->db->escape($ovalues['merge_number']) . "','" . $this->db->escape($ovalues['location']) . "','" . $this->db->escape($ovalues['location_id']) ."','" . $this->db->escape($ovalues['t_name']) . "','" . $this->db->escape($ovalues['table_id']) . "',  '" . $this->db->escape($ovalues['waiter_code']) . "','" . $this->db->escape($ovalues['waiter']) . "',  '" . $this->db->escape($ovalues['waiter_id']) . "','" . $this->db->escape($ovalues['captain_code']) . "',  '" . $this->db->escape($ovalues['captain']) . "', '" . $this->db->escape($ovalues['captain_id']) . "','" . $this->db->escape($ovalues['person']) . "','" . $this->db->escape($ovalues['ftotal']) . "','" . $this->db->escape($ovalues['ftotal_discount']) . "','" . $this->db->escape($ovalues['gst']) . "','" . $this->db->escape($ovalues['ltotal']) . "','" . $this->db->escape($ovalues['ltotal_discount']) . "','" . $this->db->escape($ovalues['vat']) . "','" . $this->db->escape($ovalues['cess']) . "','" . $this->db->escape($ovalues['staxfood']) . "','" . $this->db->escape($ovalues['staxliq']) . "','" . $this->db->escape($ovalues['stax']) . "','" . $this->db->escape($ovalues['ftotalvalue']) . "','" . $this->db->escape($ovalues['fdiscountper']) . "','" . $this->db->escape($ovalues['discount']) . "','" . $this->db->escape($ovalues['ldiscount']) . "','" . $this->db->escape($ovalues['ldiscountper']) . "','" . $this->db->escape($ovalues['ltotalvalue']) . "','" . $this->db->escape($ovalues['dtotalvalue']) . "','" . $this->db->escape($ovalues['dchargeper']) . "','" . $this->db->escape($ovalues['dcharge']) . "','" . $this->db->escape($ovalues['date']) . "','" . $this->db->escape($ovalues['time']). "','".$this->db->escape($ovalues['bill_status'])."','".$this->db->escape($ovalues['payment_status'])."','" . $this->db->escape($ovalues['cust_name']) ."','" . $this->db->escape($ovalues['cust_id']) ."','" . $this->db->escape($ovalues['cust_contact']) ."','" . $this->db->escape($ovalues['cust_address']) ."','" . $this->db->escape($ovalues['cust_email']) ."','" . $this->db->escape($ovalues['gst_no']) ."','" . $this->db->escape($ovalues['parcel_status']) ."','" . $this->db->escape($ovalues['day_close_status']) ."', '".$this->db->escape($ovalues['bill_date'])."','" . $this->db->escape($ovalues['rate_id']) . "','" . $this->db->escape($ovalues['grand_total']) . "','" . $this->db->escape($ovalues['advance_billno']) . "','" . $this->db->escape($ovalues['advance_amount']) . "','" . $this->db->escape($ovalues['roundtotal']) . "','" . $this->db->escape($ovalues['total_items']) . "','" . $this->db->escape($ovalues['item_quantity']) . "','".$this->db->escape($ovalues['date_added'])."','".$this->db->escape($ovalues['time_added'])."','".$this->db->escape($ovalues['out_time'])."','" . $this->db->escape($ovalues['nc_kot_status']) . "','" . $this->db->escape($ovalues['year_close_status']) . "','" . $this->db->escape($ovalues['cancel_status']) . "','".$this->db->escape($ovalues['pay_cash'])."','".$this->db->escape($ovalues['pay_card'])."','".$this->db->escape($ovalues['card_no'])."','".$this->db->escape($ovalues['creditcustname'])."','".$this->db->escape($ovalues['mealpass'])."','".$this->db->escape($ovalues['pass'])."','".$this->db->escape($ovalues['roomservice'])."','".$this->db->escape($ovalues['roomno'])."','".$this->db->escape($ovalues['msrcard'])."','".$this->db->escape($ovalues['msrcardno'])."','".$this->db->escape($ovalues['msrcustname'])."','".$this->db->escape($ovalues['msr_cust_id'])."','".$this->db->escape($ovalues['onac'])."','".$this->db->escape($ovalues['onaccust'])."','".$this->db->escape($ovalues['onaccontact'])."','".$this->db->escape($ovalues['onacname'])."','".$this->db->escape($ovalues['total_payment'])."','".$this->db->escape($ovalues['pay_method'])."','".$this->db->escape($ovalues['bk_status'])."','" . $this->db->escape($ovalues['login_id']) . "','" . $this->db->escape($ovalues['login_name']) . "','".$this->db->escape($ovalues['food_cancel'])."','".$this->db->escape($ovalues['liq_cancel'])."','".$this->db->escape($ovalues['modify_remark'])."','".$this->db->escape($ovalues['duplicate'])."','".$this->db->escape($ovalues['duplicate_time'])."','".$this->db->escape($ovalues['printstatus'])."','".$this->db->escape($ovalues['tip'])."','".$this->db->escape($ovalues['pay_online'])."','".$this->db->escape($ovalues['msrcardname'])."','".$this->db->escape($ovalues['delivery_charges'])."','".$this->db->escape($ovalues['advance_id'])."','".$this->db->escape($ovalues['complimentary_status'])."','".$this->db->escape($ovalues['complimentary_resion'])."','".$this->db->escape($ovalues['new_pay_cash'])."','".$this->db->escape($ovalues['new_pay_card'])."','".$this->db->escape($ovalues['new_pay_online'])."','".$this->db->escape($ovalues['new_total_payment'])."','".$this->db->escape($ovalues['shiftclose_status'])."','".$this->db->escape($ovalues['shift_id'])."','".$this->db->escape($ovalues['shift_date'])."','".$this->db->escape($ovalues['shift_time'])."','".$this->db->escape($ovalues['payment_type'])."','".$this->db->escape($ovalues['shift_username'])."','".$this->db->escape($ovalues['shift_user_id'])."','".$this->db->escape($ovalues['report_status'])."','".$this->db->escape($ovalues['wera_order_id'])."','".$this->db->escape($ovalues['cancel_bill_reason'])."','".$this->db->escape($ovalues['discount_reason'])."','".$this->db->escape($ovalues['cancel_kot_reason'])."','".$this->db->escape($ovalues['order_from'])."','".$this->db->escape($ovalues['order_packaging'])."','".$this->db->escape($ovalues['packaging_cgst'])."','".$this->db->escape($ovalues['packaging_sgst'])."','".$this->db->escape($ovalues['packaging_cgst_percent'])."','".$this->db->escape($ovalues['packaging_sgst_percent'])."','".$this->db->escape($ovalues['packaging'])."','".$this->db->escape($ovalues['urbanpiper_order_id'])."'
								),";
						$inn ++;
				} 
			}
			
			if($inn > 0){
				$excute_query = rtrim($sql, ",");
				//$this->db->query($excute_query);
				$this->new_mysql($excute_query);
				// $this->log->write("Log write For the info excute");
			}
			
			$itmes_data = $this->db->query("SELECT * FROM `oc_order_items` WHERE bill_date = '".$this->request->post['open_date']."' ")->rows;
				
				$sql1 = "INSERT INTO `oc_order_items_report`(`id`,`order_id`,`billno`,`nc_kot_status`,`nc_kot_reason`,
				`code`,`name`,`qty`,`transfer_qty`,`rate`,`ismodifier`,`parent_id`,`parent`,`cancelmodifier`,`amt`,
				`new_amt`,`new_qty`,`stax`,`new_rate`,`new_discount_per`,`new_discount_value`,`subcategoryid`,`message`,`is_liq`,`kot_status`,
				`pre_qty`,`prefix`,`is_new`,`kot_no`,`reason`,`discount_per`,`discount_value`,`tax1`,`tax1_value`,`tax2`,
				`tax2_value`,`bk_status`,`cancelstatus`,`login_id`,`login_name`,`cancel_bill`,`bill_modify`,`time`,`date`,
				`printstatus` ,`captain_id`,`captain_commission`,`waiter_id`,`waiter_commission`,`complimentary_status`,`complimentary_resion`,
				`bill_date`,`kitchen_display`,`kitchen_dis_status`,`cancel_kot_reason`,`packaging_amt`) VALUES";
				$inn1 = 0;
				foreach ($itmes_data as $pkey => $pvalues) {
					$iss_exist = $this->db->query(" SELECT * FROM `oc_order_items_report` WHERE id = '".$pvalues['id']."' ");
					if($iss_exist->num_rows == 0){

						$sql1 .="('" . $this->db->escape($pvalues['id']) . "',
									'" . $this->db->escape($pvalues['order_id']) . "',
									 '" . $this->db->escape($pvalues['billno']) ."',
									 '" . $this->db->escape($pvalues['nc_kot_status']) . "',
									'" . $this->db->escape($pvalues['nc_kot_reason']) . "',
									'" . $this->db->escape($pvalues['code']) ."',
									'" . htmlspecialchars_decode($this->db->escape($pvalues['name'])) ."',
									 '" . $this->db->escape($pvalues['qty']). "', 
									'" . $this->db->escape($pvalues['transfer_qty']) . "',
									'" . $this->db->escape($pvalues['rate']) . "',
									'" . $this->db->escape($pvalues['ismodifier']). "', 
									 '" . $this->db->escape($pvalues['parent_id']) . "',
									'" . $this->db->escape($pvalues['parent']) . "',
									'" . $this->db->escape($pvalues['cancelmodifier']) . "',
									'" . $this->db->escape($pvalues['amt']) . "',
									'" .$this->db->escape($pvalues['new_amt']) . "',
									'" . $this->db->escape($pvalues['new_qty'])  . "',
									'" . $this->db->escape($pvalues['stax']) . "',
									'" . $this->db->escape($pvalues['new_rate']) . "',
									 '" . $this->db->escape($pvalues['new_discount_per']) . "',
									 '" . $this->db->escape($pvalues['new_discount_value']) . "',
									'" . $this->db->escape($pvalues['subcategoryid']) . "',
									'" . $this->db->escape($pvalues['message']) . "',
									'" . $this->db->escape($pvalues['is_liq']) . "', 
									'" .$this->db->escape($pvalues['kot_status']). "',
									'" .$this->db->escape($pvalues['pre_qty']). "',
									'" .$this->db->escape($pvalues['prefix']). "',
									'" .$this->db->escape($pvalues['is_new']). "',
									'" .$this->db->escape($pvalues['kot_no']). "',
									'" .$this->db->escape($pvalues['reason']). "',
									'" . $this->db->escape($pvalues['discount_per']) . "',
									'" . $this->db->escape($pvalues['discount_value']) . "',
									'" . $this->db->escape($pvalues['tax1']) . "',
									'" . $this->db->escape($pvalues['tax1_value']) . "',
									'" . $this->db->escape($pvalues['tax2']) . "',
									'" . $this->db->escape($pvalues['tax2_value']) . "',
									'" . $this->db->escape($pvalues['bk_status']) . "',
									'" . $this->db->escape($pvalues['cancelstatus']) . "',
									'" . $this->db->escape($pvalues['login_id']) . "',
									'".$this->db->escape($pvalues['login_name']) ."',
									'" . $this->db->escape($pvalues['cancel_bill']) . "',
									'" . $this->db->escape($pvalues['bill_modify']) . "',
									'" . $this->db->escape($pvalues['time']) . "',
									'" . $this->db->escape($pvalues['date']) . "',
									'" . $this->db->escape($pvalues['printstatus']) . "',
									'" . $this->db->escape($pvalues['captain_id']) . "',
									'" . $this->db->escape($pvalues['captain_commission'])  . "',
									'" . $this->db->escape($pvalues['waiter_id']) . "',
									'" . $this->db->escape($pvalues['waiter_commission'])  . "',
									'" . $this->db->escape($pvalues['complimentary_status']) . "',
									'" . $this->db->escape($pvalues['complimentary_resion']) . "',
									'".$this->db->escape($pvalues['bill_date']) ."',
									'" . $this->db->escape($pvalues['kitchen_display']) . "',
									'" . $this->db->escape($pvalues['kitchen_dis_status']) . "',
									'" . $this->db->escape($pvalues['cancel_kot_reason']) . "',
									'" . $this->db->escape($pvalues['packaging_amt']) . "'
								 ),";
						$inn1++;			 
					}
				}
				if($inn1 > 0){
					$excute_query1 = rtrim($sql1, ",");
					//$this->db->query($excute_query1);
					$this->new_mysql($excute_query1);

					// $this->log->write("Log write For the items");
					//$this->log->write($excute_query1);
				}

			/****************************************** For Stock Liq Start ********************************************/
			$total_purchase = 0;
			$sale_qty = 0;
			$brand_datas = $this->db->query("SELECT * FROM oc_brand")->rows;
			$store_datas = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
			$sql3 = "INSERT INTO purchase_test(item_code,store_id,item_name,purchase_size_id,purchase_size,closing_balance,invoice_date,loose_status) VALUES";
			$inn2 = 0;

			$purchase_test_query = $this->db->query("SELECT `invoice_date` FROM purchase_test ORDER BY id DESC LIMIT 1");
			$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
			if($purchase_test_query->num_rows > 0){
				if($purchase_test_query->row['invoice_date'] != ''){
					$purchase_test_date = date('Y-m-d',strtotime($purchase_test_query->row['invoice_date']));
				} 
				// else{
				// 	$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
				// }
			}
			
			foreach($store_datas as $store_data){
				foreach($brand_datas as $brand_data){
					$brand_sizes = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."'")->rows;
					foreach($brand_sizes as $brand_size){
						$loose_status =0;
						$loose_qtysss = 0;
						$bssstata = 0;
						$loose_purchase = 0 ;
						$brand_qtys_sale = 0;
						$loose_closing = 0;
						//$last_stockss = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND purchase_size_id = '".$brand_size['id']."' AND invoice_date = '".date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'))."'  AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
						$last_stockss = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND purchase_size_id = '".$brand_size['id']."' AND invoice_date = '".$purchase_test_date."'  AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
						if($last_stockss->num_rows > 0){
							$last_stock = $last_stockss->row['closing_balance'];
							$last_loose = $last_stockss->row['loose_status'];
						} else{
							$last_stock = 0;
							$last_loose = 0;
						}

						if($brand_size['loose'] == 0){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
							$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='1' GROUP BY unit_id, description_id");
						} elseif($brand_size['loose'] == 1){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='1' GROUP BY unit_id, description_id");
						}

						if($purchase_order->num_rows > 0){
							$total_purchase = $purchase_order->row['qty'];
						} else {
							$total_purchase = 0;
						}
						
						if($purchase_order_loose->num_rows > 0){
							$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
							$loose_status = $purchase_order_loose->row['qty'];
							$loose_purchase = (int)$purchase_order_loose->row['unit'];
						}

						
						$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND to_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						if($stock_transfer_from->num_rows > 0){
							$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
						} elseif($stock_transfer_to->num_rows > 0) {
							$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
						}

						$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."' AND quantity = '".$brand_size['id']."' AND is_liq = '1'");
						if($item_data->num_rows > 0){
							$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_data['id']."'");
							if($for_stores->num_rows > 0){
								foreach($for_stores->rows as $key){
									$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oit.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
									if($order_item_data->num_rows > 0){
										if($order_item_data->row['code'] != ''){
											$sale_qty = $order_item_data->row['sale_qty'];
										} else{
											$sale_qty = 0;
										}
									}
								}
							} else{
								$sale_qty = 0;
							}
						} else{
							$sale_qty = 0;
						}
						if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
							$closing_balance = $total_purchase - $sale_qty;
						} else{
							$closing_balance = $last_stock + $total_purchase - $sale_qty;
						}

						$item_datassss = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."' AND quantity = '".$brand_size['id']."'  AND is_liq = '1'");
						if($item_datassss->num_rows > 0){
							foreach($item_datassss->rows as $key){
								$order_item_datassss = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$key['item_code']."'  GROUP BY code");
								if($order_item_datassss->num_rows > 0){
									if($order_item_datassss->row['code'] != ''){
										$brand_size_datasss = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$brand_data['type_id']."' AND loose = '1' ");

										$this->log->write('-----------------------brand size query ------------------------------');
										$this->log->write("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$brand_data['type_id']."' AND loose = '1' ");
										if($brand_size_datasss->num_rows > 0){
											$brand_qtys = (int)$brand_size_datasss->row['size'];
											$brand_qtys_sale =  $brand_qtys * $order_item_datassss->row['sale_qty'] ;
										}
									} 
								}
							}
						}
						$loose_closing = $last_loose + $loose_purchase - $brand_qtys_sale;
						
						$sql3 .="('".$brand_data['brand_id']."','".$store_data['id']."','".$brand_data['brand']."',
								'".$brand_size['id']."','".$brand_size['size']."','".$closing_balance."','".$this->request->post['open_date']."','".$loose_closing."'
								),";
						
						$inn2++;
					}
				}
			}
			if($inn2 > 0){
				 $excute_query2 = rtrim($sql3, ",");
				 //$this->db->query($excute_query2);
					$this->new_mysql($excute_query2);

				$this->log->write('Excute dayclose');
				$this->log->write($excute_query2);
			}
			/****************************************** For Stock Liq End ********************************************/

			/****************************************** For Expense start ********************************************/
			//echo $this->request->post['open_date'];exit;
			$opening_bal = 0;
			$closing_bal = 0;
			$total_paid_in_amt = 0;
			$total_paid_out_amt = 0;
			$net_total = 0;
			$start_date = $this->request->post['open_date'];
			$opening_balss = "SELECT * FROM oc_day_close_expense ORDER BY id DESC LIMIT 1";
			$opening_bals = $this->db->query($opening_balss);
			$opening_bal = 0;
			if($opening_bals->num_rows > 0){
				$opening_bal = $opening_bals->row['amount'];
			}
			$sql = "SELECT * FROM oc_expense_trans WHERE `date` = '".$start_date."'";
			$expensedatas = $this->db->query($sql)->rows;
			//echo '<pre>';print_r($expensedatas);exit();
			foreach($expensedatas as $expensedata){
				if($expensedata['payment_type'] == 'PAID IN'){
					$paid_in_amt = $expensedata['amount'];
				}else{
					$paid_in_amt = 0;
				}
				if($expensedata['payment_type'] == 'PAID OUT'){
					$paid_out_amt = $expensedata['amount'];
				}else{
					$paid_out_amt = 0;
				}
				$total_paid_in_amt = $total_paid_in_amt + $paid_in_amt;
				$total_paid_out_amt = $total_paid_out_amt + $paid_out_amt;
				$net_total = $opening_bal + $total_paid_in_amt;
				$closing_bal = $net_total - $total_paid_out_amt;
			}
			
			$this->db->query("INSERT INTO oc_day_close_expense SET amount ='".$closing_bal."',date = '".$this->request->post['open_date']."'");

			/****************************************** For Expense End ********************************************/

			/****************************************** For Stock Food Start ********************************************/
			$total_purchase = 0;
			$westage_amt = 0;
			$stock_item_datas = $this->db->query("SELECT * FROM oc_stock_item")->rows;
			$store_datas = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
			$sql4 = "INSERT INTO purchase_test(item_code,store_id,item_name,purchase_size_id,purchase_size,closing_balance,invoice_date) VALUES";
			$inn3 = 0;
			$purchase_test_query = $this->db->query("SELECT `invoice_date` FROM purchase_test ORDER BY id DESC LIMIT 1");
			$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
			if($purchase_test_query->num_rows > 0){
				if($purchase_test_query->row['invoice_date'] != ''){
					$purchase_test_date = date('Y-m-d',strtotime($purchase_test_query->row['invoice_date']));
				} 
				// else{
				// 	$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
				// }
			}
			foreach($store_datas as $store_data){
				foreach($stock_item_datas as $stock_item){
					//$last_stock = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$stock_item['item_code']."' AND purchase_size_id = '".$stock_item['uom']."' AND invoice_date = '".date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'))."' AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
					$last_stock = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$stock_item['item_code']."' AND purchase_size_id = '".$stock_item['uom']."' AND invoice_date = '".$purchase_test_date."' AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
					if($last_stock->num_rows > 0){
						$last_stock = $last_stock->row['closing_balance'];
					} else{
						$last_stock = 0;
					}

					if($stock_item['item_type'] == 'Semi Finish'){
						$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					} else{
						$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					}
					
					if($purchase_order->num_rows > 0){
						$total_purchase = $purchase_order->row['qty'];
					} else {
						$total_purchase = 0;
					}
					$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

					$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND to_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

					if($stock_transfer_from->num_rows > 0){
						$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
					} elseif($stock_transfer_to->num_rows > 0) {
						$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
					}

					$stock_deducted = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_deduction WHERE description_id = '".$stock_item['id']."' AND description_code = '".$stock_item['item_code']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$qty_deducted = 0;
					if($stock_deducted->num_rows > 0){
						$total_purchase = $total_purchase - $stock_deducted->row['qty'];
					}

					$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$stock_item['item_code']."' AND store_id = '".$store_data['id']."'");
					
					if($item_data->num_rows > 0){
						$sale_qty = 0;
						foreach($item_data->rows as $key){
							$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oit.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$key['parent_item_code']."' GROUP BY code");
							if($order_item_data->num_rows > 0){
								$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
							}
						}
					} else{
						$sale_qty = 0;
					}

					//$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit = '".$stock_item['unit_name']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$westage_amt = 0;
					if($westage_datas->num_rows > 0){
						$westage_amt = $westage_datas->row['qty'];
					}

					if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
						$closing_balance = $total_purchase + $westage_amt - $sale_qty;
					} else{
						$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty;
					}
					$sql4 .="('".$stock_item['item_code']."','".$store_data['id']."','".$stock_item['item_name']."',
								'".$stock_item['uom']."','".$stock_item['unit_name']."','".$closing_balance."','".$this->request->post['open_date']."'
								),";
					$inn3++;
						
				}
			}

			if($inn3 > 0){
				$excute_query3 = rtrim($sql4, ",");
				//$this->log->write($excute_query3);
				//$this->db->query($excute_query3);
					$this->new_mysql($excute_query3);

			}
			/****************************************** For Stock Food End ********************************************/
			

			$name = DB_DATABASE;
			$user = DB_USERNAME;
			$pass = DB_PASSWORD;
			$host = DB_HOSTNAME;
			$date = date('Y-m-d');
			$date_1 = date('Y_m_d_H_i_s');
			$download_path = DIR_DOWNLOAD."db_ador_bk_".$date_1.".sql";
			$db_bkp = DATABASE_BKP;
			$command = "\"".$db_bkp.":\\xampp\\mysql\\bin\\mysqldump.exe\" --opt --skip-extended-insert --complete-insert --host=".$host." --user=".$user." --password=".$pass." ".$name." > " . $download_path;
			//echo $command;exit;
			exec($command);

			$current_data = $this->db->query("SELECT order_id FROM oc_order_info order by order_id DESC LIMIT 1");
			$trans_data = $this->db->query("SELECT order_id FROM oc_order_info_report order by order_id DESC LIMIT 1");
			if($current_data->num_rows > 0){
				if($current_data->row['order_id'] == $trans_data->row['order_id']){
					$this->db->query("DELETE FROM `oc_order_info` WHERE bill_date = '".$this->request->post['open_date']."' ");
				}
			}
			$current_items_data = $this->db->query("SELECT id FROM oc_order_items order by id DESC LIMIT 1");
			$trans_items_data = $this->db->query("SELECT id FROM oc_order_items_report order by id DESC LIMIT 1");
			if($current_items_data->num_rows > 0){
				if($current_items_data->row['id'] == $trans_items_data->row['id']){
					$this->db->query("DELETE FROM `oc_order_items` WHERE bill_date = '".$this->request->post['open_date']."' ");
				}
			}


			unset($this->session->data['warning1']);
			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			$this->response->setOutput(json_encode($json));
		} else {
			if(isset($this->error['open_tran'])){
				$json['info'] = 0;
			} elseif(isset($this->error['open_tran_1'])){
				$json['info'] = 2;
			}
			$json['action'] = $this->url->link('catalog/dayclose/edit_force', 'token=' . $this->session->data['token'], true);
			$this->response->setOutput(json_encode($json));
		}
	}

	public function edit_force() {
		$this->load->language('catalog/table');
		$this->document->setTitle('Bill Merge');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			//$this->db->query("UPDATE `oc_order_info` SET `day_close_status` = '1' WHERE bill_date = '" . $this->request->post['open_date'] . "' ");

			/****************************************** For Stock Liq Start ********************************************/
			$total_purchase = 0;
			$sale_qty = 0;
			$brand_datas = $this->db->query("SELECT * FROM oc_brand")->rows;
			$store_datas = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
			$sql3 = "INSERT INTO purchase_test(item_code,store_id,item_name,purchase_size_id,purchase_size,closing_balance,invoice_date) VALUES";
			$inn = 0;

			$purchase_test_query = $this->db->query("SELECT `invoice_date` FROM purchase_test ORDER BY id DESC LIMIT 1");
			$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
			if($purchase_test_query->num_rows > 0){
				if($purchase_test_query->row['invoice_date'] != ''){
					$purchase_test_date = date('Y-m-d',strtotime($purchase_test_query->row['invoice_date']));
				} 
				// else{
				// 	$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
				// }
			}

			foreach($store_datas as $store_data){
				foreach($brand_datas as $brand_data){
					$brand_sizes = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."'")->rows;
					foreach($brand_sizes as $brand_size){
						$loose_status =0;
						$loose_qtysss = 0;
						$bssstata = 0;
						$loose_purchase = 0 ;
						$brand_qtys_sale = 0;
						$loose_closing = 0;
						//$last_stockss = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND purchase_size_id = '".$brand_size['id']."' AND invoice_date = '".date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'))."'  AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
						$last_stockss = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND purchase_size_id = '".$brand_size['id']."' AND invoice_date = '".$purchase_test_date."'  AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
						if($last_stockss->num_rows > 0){
							$last_stock = $last_stockss->row['closing_balance'];
							$last_loose = $last_stockss->row['loose_status'];
						} else{
							$last_stock = 0;
							$last_loose = 0;
						}

						if($brand_size['loose'] == 0){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
							$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='1' GROUP BY unit_id, description_id");
						} elseif($brand_size['loose'] == 1){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='1' GROUP BY unit_id, description_id");
						}

						if($purchase_order->num_rows > 0){
							$total_purchase = $purchase_order->row['qty'];
						} else {
							$total_purchase = 0;
						}
						
						if($purchase_order_loose->num_rows > 0){
							$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
							$loose_status = $purchase_order_loose->row['qty'];
							$loose_purchase = (int)$purchase_order_loose->row['unit'];
						}

						
						$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND to_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						if($stock_transfer_from->num_rows > 0){
							$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
						} elseif($stock_transfer_to->num_rows > 0) {
							$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
						}

						$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."' AND quantity = '".$brand_size['id']."' AND is_liq = '1'");
						if($item_data->num_rows > 0){
							$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_data['id']."'");
							if($for_stores->num_rows > 0){
								foreach($for_stores->rows as $key){
									$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oit.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
									if($order_item_data->num_rows > 0){
										if($order_item_data->row['code'] != ''){
											$sale_qty = $order_item_data->row['sale_qty'];
										} else{
											$sale_qty = 0;
										}
									}
								}
							} else{
								$sale_qty = 0;
							}
						} else{
							$sale_qty = 0;
						}
						if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
							$closing_balance = $total_purchase - $sale_qty;
						} else{
							$closing_balance = $last_stock + $total_purchase - $sale_qty;
						}

						$item_datassss = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."' AND quantity = '".$brand_size['id']."'  AND is_liq = '1'");
						if($item_datassss->num_rows > 0){
							foreach($item_datassss->rows as $key){
								$order_item_datassss = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$key['item_code']."'  GROUP BY code");
								if($order_item_datassss->num_rows > 0){
									if($order_item_datassss->row['code'] != ''){
										$brand_size_datasss = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$brand_data['type_id']."' AND loose = '1' ");

										$this->log->write('-----------------------brand size query ------------------------------');
										$this->log->write("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$brand_data['type_id']."' AND loose = '1' ");
										if($brand_size_datasss->num_rows > 0){
											$brand_qtys = (int)$brand_size_datasss->row['size'];
											$brand_qtys_sale =  $brand_qtys * $order_item_datassss->row['sale_qty'] ;
										}
									} 
								}
							}
						}
						$loose_closing = $last_loose + $loose_purchase - $brand_qtys_sale;
						
						$sql3 .="('".$brand_data['brand_id']."','".$store_data['id']."','".$brand_data['brand']."',
								'".$brand_size['id']."','".$brand_size['size']."','".$closing_balance."','".$this->request->post['open_date']."','".$loose_closing."'
								),";
						
						$inn++;
					}
				}
			}


			/*foreach($store_datas as $store_data){
				foreach($brand_datas as $brand_data){
					$brand_sizes = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."'")->rows;
					foreach($brand_sizes as $brand_size){
						$last_stock = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND purchase_size_id = '".$brand_size['id']."' AND invoice_date = '".date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'))."'  AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
						if($last_stock->num_rows > 0){
							$last_stock = $last_stock->row['closing_balance'];
						} else{
							$last_stock = 0;
						}
						if($brand_size['loose'] == 0){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
							$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='0' GROUP BY unit_id, description_id");
						} elseif($brand_size['loose'] == 1){
							$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND store_id = '".$store_data['id']."' AND loose='1' GROUP BY unit_id, description_id");
						}

						if($purchase_order->num_rows > 0){
							$total_purchase = $purchase_order->row['qty'];
						} else {
							$total_purchase = 0;
						}

						if($purchase_order_loose->num_rows > 0){
							$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
						}

						$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$brand_size['id']."' AND category = 'Liquor' AND to_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

						if($stock_transfer_from->num_rows > 0){
							$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
						} elseif($stock_transfer_to->num_rows > 0) {
							$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
						}

						$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."' AND quantity = '".$brand_size['id']."' AND is_liq = '1'");
						if($item_data->num_rows > 0){
							$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_data['id']."'");
							if($for_stores->num_rows > 0){
								foreach($for_stores->rows as $key){
									$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oit.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
									if($order_item_data->num_rows > 0){
										if($order_item_data->row['code'] != ''){
											$sale_qty = $order_item_data->row['sale_qty'];
										} else{
											$sale_qty = 0;
										}
									}
								}
							} else{
								$sale_qty = 0;
							}
						} else{
							$sale_qty = 0;
						}
						if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
							$closing_balance = $total_purchase - $sale_qty;
						} else{
							$closing_balance = $last_stock + $total_purchase - $sale_qty;
						}
						$sql3 .="('".$brand_data['brand_id']."','".$store_data['id']."','".$brand_data['brand']."',
								'".$brand_size['id']."','".$brand_size['size']."','".$closing_balance."','".$this->request->post['open_date']."'
								),";

						$inn++;
						// $this->db->query("INSERT INTO purchase_test SET 
						// 				item_code = '".$brand_data['brand_id']."',
						// 				store_id = '".$store_data['id']."',
						// 				item_name = '".$brand_data['brand']."',
						// 				purchase_size_id = '".$brand_size['id']."',
						// 				purchase_size = '".$brand_size['size']."',
						// 				closing_balance = '".$closing_balance."',
						// 				invoice_date = '".$this->request->post['open_date']."'
						// 			");
					}
				}
			}*/
			if($inn > 0){
				$excute_query2 = rtrim($sql3, ",");
				$this->new_mysql($excute_query2);

				//$this->db->query($excute_query2);
			}
			/****************************************** For Stock Liq End ********************************************/

			//******************************************************//
			$opening_bal =0;
			$closing_bal =0;
			$total_paid_in_amt =0;
			$total_paid_out_amt =0;
			$net_total=0;
			$start_date = $this->request->post['open_date'];
			$opening_balss="SELECT * FROM oc_day_close_expense ORDER BY id DESC LIMIT 1";
			$opening_bals = $this->db->query($opening_balss);
			$opening_bal = 0;
			if($opening_bals->num_rows > 0){
				$opening_bal = $opening_bals->row['amount'];
			}
			$sql = "SELECT * FROM oc_expense_trans WHERE `date` = '".$start_date."'";
			$expensedatas = $this->db->query($sql)->rows;
			//echo '<pre>';print_r($expensedatas);exit();
			foreach($expensedatas as $expensedata){
				if($expensedata['payment_type']=='PAID IN'){
					$paid_in_amt=$expensedata['amount'];
				}else{
					$paid_in_amt=0;
				}
				if($expensedata['payment_type']=='PAID OUT'){
					$paid_out_amt=$expensedata['amount'];
				}else{
					$paid_out_amt=0;
				}
				$total_paid_in_amt=$total_paid_in_amt+$paid_in_amt;
				$total_paid_out_amt=$total_paid_out_amt+$paid_out_amt;
				$net_total=$opening_bal+$total_paid_in_amt;
				$closing_bal=$net_total-$total_paid_out_amt;
			}
			//echo $closing_bal;exit;
			$this->db->query("INSERT INTO oc_day_close_expense SET 
										amount ='".$closing_bal."',
										date = '".$this->request->post['open_date']."'
									");
			//*****************************************************************************//

			/****************************************** For Stock Food Start ********************************************/
			$total_purchase = 0;
			$westage_amt = 0;
			$stock_item_datas = $this->db->query("SELECT * FROM oc_stock_item")->rows;
			$store_datas = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
			$sql4 = "INSERT INTO purchase_test(item_code,store_id,item_name,purchase_size_id,purchase_size,closing_balance,invoice_date) VALUES";
			$inn1 = 0;
			$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
			if($purchase_test_query->num_rows > 0){
				if($purchase_test_query->row['invoice_date'] != ''){
					$purchase_test_date = date('Y-m-d',strtotime($purchase_test_query->row['invoice_date']));
				} 
				// else{
				// 	$purchase_test_date = date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'));
				// }
			}
			foreach($store_datas as $store_data){
				foreach($stock_item_datas as $stock_item){
					//$last_stock = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$stock_item['item_code']."' AND purchase_size_id = '".$stock_item['uom']."' AND invoice_date = '".date('Y-m-d',strtotime($this->request->post['open_date'] .' -1 day'))."' AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
					$last_stock = $this->db->query("SELECT * FROM purchase_test WHERE item_code = '".$stock_item['item_code']."' AND purchase_size_id = '".$stock_item['uom']."' AND invoice_date = '".$purchase_test_date."' AND store_id = '".$store_data['id']."' ORDER BY id DESC LIMIT 1 ");
					if($last_stock->num_rows > 0){
						$last_stock = $last_stock->row['closing_balance'];
					} else{
						$last_stock = 0;
					}

					if($stock_item['item_type'] == 'Semi Finish'){
						$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					} else{
						$purchase_order = $this->db->query("SELECT SUM(qty) as qty, description, description_id, invoice_date, unit_id, unit FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					}
					
					if($purchase_order->num_rows > 0){
						$total_purchase = $purchase_order->row['qty'];
					} else {
						$total_purchase = 0;
					}
					$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

					$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND to_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");

					if($stock_transfer_from->num_rows > 0){
						$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
					} elseif($stock_transfer_to->num_rows > 0) {
						$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
					}

					$stock_deducted = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_deduction WHERE description_id = '".$stock_item['id']."' AND description_code = '".$stock_item['item_code']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$qty_deducted = 0;
					if($stock_deducted->num_rows > 0){
						$total_purchase = $total_purchase - $stock_deducted->row['qty'];
					}

					$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$stock_item['item_code']."' AND store_id = '".$store_data['id']."'");
					
					if($item_data->num_rows > 0){
						$sale_qty = 0;
						foreach($item_data->rows as $key){
							$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oit.`bill_date` = '".$this->request->post['open_date']."' AND code = '".$key['parent_item_code']."' GROUP BY code");
							if($order_item_data->num_rows > 0){
								$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
							}
						}
					} else{
						$sale_qty = 0;
					}

					//$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit_id = '".$stock_item['uom']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$stock_item['id']."' AND invoice_date = '".$this->request->post['open_date']."' AND unit = '".$stock_item['unit_name']."' AND category = 'Food' AND from_store_id = '".$store_data['id']."' GROUP BY unit_id, description_id");
					$westage_amt = 0;
					if($westage_datas->num_rows > 0){
						$westage_amt = $westage_datas->row['qty'];
					}

					if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
						$closing_balance = $total_purchase + $westage_amt - $sale_qty;
					} else{
						$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty;
					}
					$sql4 .="('".$stock_item['item_code']."','".$store_data['id']."','".$stock_item['item_name']."',
								'".$stock_item['uom']."','".$stock_item['unit_name']."','".$closing_balance."','".$this->request->post['open_date']."'
								),";
					$inn1++;			
					// $this->db->query("INSERT INTO purchase_test SET 
					// 				item_code = '".$stock_item['item_code']."',
					// 				store_id = '".$store_data['id']."',
					// 				item_name = '".$stock_item['item_name']."',
					// 				purchase_size_id = '".$stock_item['uom']."',
					// 				purchase_size = '".$stock_item['unit_name']."',
					// 				closing_balance = '".$closing_balance."',
					// 				invoice_date = '".$this->request->post['open_date']."'
					// 			");
				}
			}
			if($inn1 > 0){
				$excute_query3 = rtrim($sql4, ",");
				$this->new_mysql($excute_query3);

				//$this->db->query($excute_query3);
			}
			/****************************************** For Stock Food End ********************************************/
			/*$name = DB_DATABASE;
			$user = DB_USERNAME;
			$pass = DB_PASSWORD;
			$host = DB_HOSTNAME;
			//Export the database and output the status to the page
			$mysqli = new mysqli($host,$user,$pass,$name); 
			$mysqli->select_db($name); 
			$mysqli->query("SET NAMES 'utf8'");
			$queryTables = $mysqli->query('SHOW TABLES'); 
			while($row = $queryTables->fetch_row()) { 
				$target_tables_all[] = $row[0]; 
			}
			
			foreach($target_tables_all as $table) {
				$result         =   $mysqli->query('SELECT * FROM '.$table);  
				$fields_amount  =   $result->field_count;  
				$rows_num=$mysqli->affected_rows;     
				$res            =   $mysqli->query('SHOW CREATE TABLE '.$table); 
				$TableMLine     =   $res->fetch_row();
				$content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine[1].";\n\n";

				for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
					while($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
						if ($st_counter%100 == 0 || $st_counter == 0 )  {
								$content .= "\nINSERT INTO ".$table." VALUES";
						}
						$content .= "\n(";
						for($j=0; $j<$fields_amount; $j++)  { 
							$row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
							if (isset($row[$j])){
								$content .= '"'.$row[$j].'"' ; 
							} else {   
								$content .= '""';
							}     
							if ($j<($fields_amount-1)) {
								$content.= ',';
							}      
						}
						$content .=")";
						//every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
						if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {   
							$content .= ";";
						} else {
							$content .= ",";
						} 
						$st_counter=$st_counter+1;
					}
				} $content .="\n\n\n";
			}
			//$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
		  // $backup_name = DIR_DOWNLOAD.'db_ador_bk.sql';
			$date = date('Y-m-d');
			$backup_name = DIR_DOWNLOAD."db_ador_bk_".$date.".sql";
			//echo $backup_name;exit;
			file_put_contents($backup_name, $content);

*/

			$name = DB_DATABASE;
			$user = DB_USERNAME;
			$pass = DB_PASSWORD;
			$host = DB_HOSTNAME;
			$date = date('Y-m-d');
			$date_1 = date('Y_m_d_H_i_s');
			$download_path = DIR_DOWNLOAD."db_ador_bk_".$date_1.".sql";
			$db_bkp = DATABASE_BKP;
			$command = "\"".$db_bkp.":\\xampp\\mysql\\bin\\mysqldump.exe\" --opt --skip-extended-insert --complete-insert --host=".$host." --user=".$user." --password=".$pass." ".$name." > " . $download_path;
			//echo $command;exit;
			exec($command);


			unset($this->session->data['warning1']);
			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			//$this->response->setOutput(json_encode($json));

			$sql = "SELECT * FROM oc_order_info WHERE `bill_status` = '0' and `pay_method` ='0' ";
			$datass = $this->db->query($sql)->rows;
			foreach($datass as $datas){
				$this->db->query("UPDATE `oc_order_info` SET  `cancel_status` = '1' WHERE `order_id`='".$datas['order_id']."' ");
				$this->db->query("UPDATE `oc_order_items` SET  `cancel_bill` = '1' ,`cancelstatus`='1' WHERE `order_id`='".$datas['order_id']."' ");
			}
			//$this->log->write("---------POST START--------");
			//$this->log->write(print_r($this->db->query("UPDATE `oc_order_info` SET  `cancel_status` = '1' WHERE `order_id`='".$datas['order_id']."' "), true));
			//$this->log->write("---------POST END---------");
			
			//****************************************************************************//
			$sql = "SELECT * FROM oc_order_info WHERE `bill_status` = '1' and `pay_method` ='0' ";
			$datass = $this->db->query($sql)->rows;
			foreach($datass as $datas){
				$this->db->query("UPDATE `oc_order_info` SET `pay_cash` ='".$datas['grand_total']."' , `pay_method` = '1' ,`payment_status` = '1' WHERE `order_id`='".$datas['order_id']."' ");
			}

			//****************************************************************************//	
			//$this->db->query("UPDATE `oc_order_info` SET `day_close_status` = '1', `bill_status` = '1', `payment_status` = '1' WHERE bill_date = '" . $this->request->post['open_date'] . "' ");


			$order_datas = $this->db->query(" SELECT * FROM `oc_order_info` WHERE bill_date = '".$this->request->post['open_date']."' ")->rows;
			$sql = "INSERT INTO `oc_order_info_report`(`order_id`,`app_order_id`,`kot_no`,`order_no`,`merge_number`, `location`,`location_id`,`t_name`,`table_id`,`waiter_code`,`waiter`,`waiter_id`,`captain_code`,`captain`,`captain_id`,`person`,`ftotal`,`ftotal_discount`,`gst`,`ltotal`,`ltotal_discount`,`vat`,`cess`,`staxfood`,`staxliq`,`stax`,`ftotalvalue`,`fdiscountper`,`discount`,`ldiscount`,`ldiscountper`,`ltotalvalue`,`dtotalvalue`,`dchargeper`,`dcharge`,`date`,`time`,`bill_status`,`payment_status`,`cust_name`,`cust_id`,`cust_contact`,`cust_address`,`cust_email`,`gst_no`,`parcel_status`,`day_close_status`,`bill_date`,`rate_id`,`grand_total`,`advance_billno`,`advance_amount`,`roundtotal`,`total_items`,`item_quantity`,`date_added`,`time_added`,`out_time`,`nc_kot_status`,`year_close_status`,`cancel_status`,`pay_cash`,`pay_card`,`card_no`,`creditcustname`,`mealpass`,`pass`,`roomservice`,`roomno` ,`msrcard`,`msrcardno`,`msrcustname`,`msr_cust_id`, `onac`,`onaccust`,`onaccontact`,`onacname`,`total_payment`,`pay_method`,`bk_status`,`login_id`,`login_name`,`food_cancel`,`liq_cancel`,`modify_remark`,`duplicate`,`duplicate_time`,`printstatus`,`tip`,`pay_online`,`msrcardname`,`delivery_charges`,`advance_id`,`complimentary_status`,`complimentary_resion`,`new_pay_cash`,`new_pay_card`,`new_pay_online`,`new_total_payment`,`shiftclose_status`,`shift_id`,`shift_date`,`shift_time`,`payment_type`,`shift_username`,`shift_user_id`,`report_status`, `wera_order_id`, `cancel_bill_reason`, `discount_reason`, `cancel_kot_reason`, `order_from`, `order_packaging`, `packaging_cgst`, `packaging_sgst`, `packaging_cgst_percent`,`packaging_sgst_percent`, `packaging`, `urbanpiper_order_id`) VALUES";
			$inn2 = 0;
			foreach ($order_datas as $okey => $ovalues) {
				$is_exist = $this->db->query(" SELECT * FROM `oc_order_info_report` WHERE order_id = '".$ovalues['order_id']."' ");
				if($is_exist->num_rows == 0){

					$sql .= " ('" . $this->db->escape($ovalues['order_id']) . "',
									'" . $this->db->escape($ovalues['app_order_id']) . "',
									'".$this->db->escape($ovalues['kot_no'])."',
									'" . $this->db->escape($ovalues['order_no']) . "',
									'" . $this->db->escape($ovalues['merge_number']) . "',
									'" . $this->db->escape($ovalues['location']) . "',
									'" . $this->db->escape($ovalues['location_id']) ."',
									'" . $this->db->escape($ovalues['t_name']) . "',
									'" . $this->db->escape($ovalues['table_id']) . "',  
									'" . $this->db->escape($ovalues['waiter_code']) . "',
									'" . $this->db->escape($ovalues['waiter']) . "',  
									'" . $this->db->escape($ovalues['waiter_id']) . "',
									'" . $this->db->escape($ovalues['captain_code']) . "',  
									'" . $this->db->escape($ovalues['captain']) . "',  
									'" . $this->db->escape($ovalues['captain_id']) . "',
									'" . $this->db->escape($ovalues['person']) . "',
									'" . $this->db->escape($ovalues['ftotal']) . "',
									'" . $this->db->escape($ovalues['ftotal_discount']) . "',
									'" . $this->db->escape($ovalues['gst']) . "',
									'" . $this->db->escape($ovalues['ltotal']) . "',
									'" . $this->db->escape($ovalues['ltotal_discount']) . "',
									'" . $this->db->escape($ovalues['vat']) . "',
									'" . $this->db->escape($ovalues['cess']) . "',
									'" . $this->db->escape($ovalues['staxfood']) . "',
									'" . $this->db->escape($ovalues['staxliq']) . "',
									'" . $this->db->escape($ovalues['stax']) . "',
									'" . $this->db->escape($ovalues['ftotalvalue']) . "',
									'" . $this->db->escape($ovalues['fdiscountper']) . "',
									'" . $this->db->escape($ovalues['discount']) . "',
									'" . $this->db->escape($ovalues['ldiscount']) . "',
									'" . $this->db->escape($ovalues['ldiscountper']) . "',
									'" . $this->db->escape($ovalues['ltotalvalue']) . "',
									'" . $this->db->escape($ovalues['dtotalvalue']) . "',
									'" . $this->db->escape($ovalues['dchargeper']) . "',
									'" . $this->db->escape($ovalues['dcharge']) . "',
									'" . $this->db->escape($ovalues['date']) . "',
									'" . $this->db->escape($ovalues['time']). "',
									'".$this->db->escape($ovalues['bill_status'])."',
									'".$this->db->escape($ovalues['payment_status'])."',
									'" . $this->db->escape($ovalues['cust_name']) ."',
									'" . $this->db->escape($ovalues['cust_id']) ."',
									'" . $this->db->escape($ovalues['cust_contact']) ."',
									'" . $this->db->escape($ovalues['cust_address']) ."',
									'" . $this->db->escape($ovalues['cust_email']) ."',
									'" . $this->db->escape($ovalues['gst_no']) ."',
									'" . $this->db->escape($ovalues['parcel_status']) ."', 
									'" . $this->db->escape($ovalues['day_close_status']) ."', 
									'".$this->db->escape($ovalues['bill_date'])."',
									'" . $this->db->escape($ovalues['rate_id']) . "',
									'" . $this->db->escape($ovalues['grand_total']) . "',
									'" . $this->db->escape($ovalues['advance_billno']) . "',
									'" . $this->db->escape($ovalues['advance_amount']) . "',
									'" . $this->db->escape($ovalues['roundtotal']) . "',
									'" . $this->db->escape($ovalues['total_items']) . "',
									'" . $this->db->escape($ovalues['item_quantity']) . "',
									'".$this->db->escape($ovalues['date_added'])."',
									'".$this->db->escape($ovalues['time_added'])."',
									'".$this->db->escape($ovalues['out_time'])."',
									'" . $this->db->escape($ovalues['nc_kot_status']) . "',
									'" . $this->db->escape($ovalues['year_close_status']) . "',
									'" . $this->db->escape($ovalues['cancel_status']) . "',
									'".$this->db->escape($ovalues['pay_cash'])."',
									'".$this->db->escape($ovalues['pay_card'])."',
									'".$this->db->escape($ovalues['card_no'])."',
									'".$this->db->escape($ovalues['creditcustname'])."',
									'".$this->db->escape($ovalues['mealpass'])."',
									'".$this->db->escape($ovalues['pass'])."',
									'".$this->db->escape($ovalues['roomservice'])."',
									'".$this->db->escape($ovalues['roomno'])."',
									'".$this->db->escape($ovalues['msrcard'])."',
									'".$this->db->escape($ovalues['msrcardno'])."',
									'".$this->db->escape($ovalues['msrcustname'])."',
									'".$this->db->escape($ovalues['msr_cust_id'])."',
									'".$this->db->escape($ovalues['onac'])."',
									'".$this->db->escape($ovalues['onaccust'])."',
									'".$this->db->escape($ovalues['onaccontact'])."',
									'".$this->db->escape($ovalues['onacname'])."',
									'".$this->db->escape($ovalues['total_payment'])."',
									'".$this->db->escape($ovalues['pay_method'])."',
									'".$this->db->escape($ovalues['bk_status'])."',
									'" . $this->db->escape($ovalues['login_id']) . "',
									'" . $this->db->escape($ovalues['login_name']) . "',
									'".$this->db->escape($ovalues['food_cancel'])."',
									'".$this->db->escape($ovalues['liq_cancel'])."',
									'".$this->db->escape($ovalues['modify_remark'])."',
									'".$this->db->escape($ovalues['duplicate'])."',
									'".$this->db->escape($ovalues['duplicate_time'])."',
									'".$this->db->escape($ovalues['printstatus'])."',
									'".$this->db->escape($ovalues['tip'])."',
									'".$this->db->escape($ovalues['pay_online'])."',
									'".$this->db->escape($ovalues['msrcardname'])."',
									'".$this->db->escape($ovalues['delivery_charges'])."',
									'".$this->db->escape($ovalues['advance_id'])."',
									'".$this->db->escape($ovalues['complimentary_status'])."',
									'".$this->db->escape($ovalues['complimentary_resion'])."',
									'".$this->db->escape($ovalues['new_pay_cash'])."',
									'".$this->db->escape($ovalues['new_pay_card'])."',
									'".$this->db->escape($ovalues['new_pay_online'])."',
									'".$this->db->escape($ovalues['new_total_payment'])."',
									'".$this->db->escape($ovalues['shiftclose_status'])."',
									'".$this->db->escape($ovalues['shift_id'])."',
									'".$this->db->escape($ovalues['shift_date'])."',
									'".$this->db->escape($ovalues['shift_time'])."',
									'".$this->db->escape($ovalues['payment_type'])."',
									'".$this->db->escape($ovalues['shift_username'])."',
									'".$this->db->escape($ovalues['shift_user_id'])."','".$this->db->escape($ovalues['report_status'])."','".$this->db->escape($ovalues['wera_order_id'])."','".$this->db->escape($ovalues['cancel_bill_reason'])."','".$this->db->escape($ovalues['discount_reason'])."','".$this->db->escape($ovalues['cancel_kot_reason'])."','".$this->db->escape($ovalues['order_from'])."','".$this->db->escape($ovalues['order_packaging'])."','".$this->db->escape($ovalues['packaging_cgst'])."','".$this->db->escape($ovalues['packaging_sgst'])."','".$this->db->escape($ovalues['packaging_cgst_percent'])."','".$this->db->escape($ovalues['packaging_sgst_percent'])."','".$this->db->escape($ovalues['packaging'])."','".$this->db->escape($ovalues['urbanpiper_order_id'])."'
								),";
						$inn2++;		
				}
			}
			if($inn2 > 0){
				$excute_query = rtrim($sql, ",");
					$this->new_mysql($excute_query);

				//$this->db->query($excute_query);
			}
			
			$itmes_data = $this->db->query("SELECT * FROM `oc_order_items` WHERE bill_date = '".$this->request->post['open_date']."' ")->rows;
				
				$sql1 = "INSERT INTO `oc_order_items_report`(`id`,`order_id`,`billno`,`nc_kot_status`,`nc_kot_reason`,
				`code`,`name`,`qty`,`transfer_qty`,`rate`,`ismodifier`,`parent_id`,`parent`,`cancelmodifier`,`amt`,
				`new_amt`,`new_qty`,`stax`,`new_rate`,`new_discount_per`,`new_discount_value`,`subcategoryid`,`message`,`is_liq`,`kot_status`,
				`pre_qty`,`prefix`,`is_new`,`kot_no`,`reason`,`discount_per`,`discount_value`,`tax1`,`tax1_value`,`tax2`,
				`tax2_value`,`bk_status`,`cancelstatus`,`login_id`,`login_name`,`cancel_bill`,`bill_modify`,`time`,`date`,
				`printstatus` ,`captain_id`,`captain_commission`,`waiter_id`,`waiter_commission`,`complimentary_status`,`complimentary_resion`,
				`bill_date`,`kitchen_display`,`kitchen_dis_status`,`cancel_kot_reason`,`packaging_amt`) VALUES";
				$inn3 = 0;
				foreach ($itmes_data as $pkey => $pvalues) {
					$iss_exist = $this->db->query(" SELECT * FROM `oc_order_items_report` WHERE id = '".$pvalues['id']."' ");
					if($iss_exist->num_rows == 0){

						$sql1 .="('".$pvalues['id']."','" . $pvalues['order_id'] . "', '" . $pvalues['billno'] ."','" . $pvalues['nc_kot_status'] . "',
									'" . $pvalues['nc_kot_reason'] . "','" . $pvalues['code'] ."','" . htmlspecialchars_decode($pvalues['name']) ."', '" . $pvalues['qty']. "', 
									'" . $pvalues['transfer_qty'] . "','" . $pvalues['rate'] . "','" . $pvalues['ismodifier']. "',  '" . $pvalues['parent_id'] . "',
									'" . $pvalues['parent'] . "','" . $pvalues['cancelmodifier'] . "','" . $pvalues['amt'] . "','" .$pvalues['new_amt'] . "','" . $pvalues['new_qty']  . "',
									'" . $pvalues['stax'] . "','" . $pvalues['new_rate'] . "', '" . $pvalues['new_discount_per'] . "','" . $pvalues['new_discount_value'] . "',
									'" . $pvalues['subcategoryid'] . "','" . $pvalues['message'] . "','" . $pvalues['is_liq'] . "', '" .$pvalues['kot_status']. "',
									'" .$pvalues['pre_qty']. "','" .$pvalues['prefix']. "','" .$pvalues['is_new']. "','" .$pvalues['kot_no']. "','" .$pvalues['reason']. "',
									'" . $pvalues['discount_per'] . "','" . $pvalues['discount_value'] . "','" . $pvalues['tax1'] . "','" . $pvalues['tax1_value'] . "',
									'" . $pvalues['tax2'] . "','" . $pvalues['tax2_value'] . "','" . $pvalues['bk_status'] . "','" . $pvalues['cancelstatus'] . "',
									'" . $pvalues['login_id'] . "','".$pvalues['login_name'] ."','" . $pvalues['cancel_bill'] . "','" . $pvalues['bill_modify'] . "',
									'" . $pvalues['time'] . "','" . $pvalues['date'] . "','" . $pvalues['printstatus'] . "','" . $pvalues['captain_id'] . "',
									'" . $pvalues['captain_commission']  . "','" . $pvalues['waiter_id'] . "','" . $pvalues['waiter_commission']  . "','" . $pvalues['complimentary_status'] . "',
									'" . $pvalues['complimentary_resion'] . "','".$pvalues['bill_date'] ."','" . $pvalues['kitchen_display'] . "','" . $pvalues['kitchen_dis_status'] . "','" . $pvalues['cancel_kot_reason'] . "',
									'" . $pvalues['packaging_amt'] . "'
								 ),";
								 $inn3++;
					}
				}
				if($inn3 > 0){
					$excute_query1 = rtrim($sql1, ",");
					$this->new_mysql($excute_query1);

					//$this->db->query($excute_query1);
				}
				// echo'<pre>';
				// print_r($valu);
				// exit;
			$this->db->query("DELETE FROM `oc_order_info` WHERE bill_date = '".$this->request->post['open_date']."' ");
			$this->db->query("DELETE FROM `oc_order_items` WHERE bill_date = '".$this->request->post['open_date']."' ");

			$json['done'] = '<img style="width:100%" src="'.HTTP_CATALOG.'system/storage/download/done.gif"> ';
			$json['info'] = 1;
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$this->response->setOutput(json_encode($json));
		}

	}

	protected function getForm() {
		$data['heading_title'] = 'Bill Merge';//$this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('catalog/dayclose/edit', 'token=' . $this->session->data['token'] . $url, true);
		$data['action1'] = $this->url->link('catalog/dayclose/edit_force', 'token=' . $this->session->data['token'] . $url, true);
		$data['cancel'] = $this->url->link('catalog/dayclose', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `bill_date` ASC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$open_date = $last_open_dates->row['bill_date'];
		} else {
			$open_date = date('Y-m-d');
		}

		//$this->log->write(print_r($this->request->get, true));
		
		if (isset($this->request->post['open_date'])) {
			$data['open_date'] = $this->request->post['open_date'];
		} else {
			$data['open_date'] = $open_date;
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/dayclose_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/dayclose')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$open_date = $this->request->post['open_date'];
		$is_exist = $this->db->query("SELECT `order_id` FROM `oc_order_info` WHERE `bill_date` = '".$open_date."' AND (`bill_status` = '0' OR `payment_status` = '0') AND `cancel_status` = '0'");
		if($is_exist->num_rows > 0 && strtotime($open_date) != strtotime(date('Y-m-d'))){
			$this->error['open_tran'] = 'Unbilled Tables Present.';
		}

		if(strtotime($open_date) == strtotime(date('Y-m-d'))){
			$this->error['open_tran_1'] = 'Warning: Cannot Close Current Date';	
		}
		// if (isset($this->request->post['order_no'])){
		// 	$this->error['order_no'] = 'Please Enter order number';
		// }
		// if (($this->request->post['table_frm']) == ($this->request->post['table_to'])) {
		// 	$this->error['table_to'] = 'both tables are same';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/dayclose')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function tests()
	{
		$json['info'] = 1;
		$this->response->setOutput(json_encode($json));
	}

	public function getorderinfo() {
		$json = array();
		$json['status'] = 0;
		if (isset($this->request->get['order_no'])) {
			$filter_data = array(
				'order_no' => $this->request->get['order_no'],
				'master_order_no' => $this->request->get['master_order_no'],
			);
			$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
			$last_open_dates = $this->db->query($last_open_date_sql);
			if($last_open_dates->num_rows > 0){
				$last_open_date = $last_open_dates->row['bill_date'];
			} else {
				$last_open_date = date('Y-m-d');
			}
			//$results = $this->model_catalog_table->getTables($filter_data);
			$results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `merge_number` FROM `oc_order_info` WHERE `order_no` = '".$filter_data['order_no']."' AND `date` = '".$last_open_date."' ")->rows;
			$in = 0;
			foreach ($results as $result) {
				$in = 1;
				if($result['merge_number'] == '0' || $result['merge_number'] == $filter_data['master_order_no']){
					$json = array(
						'order_no' => $result['order_no'],
						'ftotal'   => $result['ftotal'],
						'ltotal'   => $result['ltotal'],
						'grand_total' => $result['ftotal'] + $result['ltotal'],
						'status' => 1,
					);
				} else {
					$json['status'] = 2;
				}
			}
			if($in == 1){
				// $sort_order = array();
				// foreach ($json as $key => $value) {
				// 	$sort_order[$key] = $value['order_no'];
				// }
				// array_multisort($sort_order, SORT_ASC, $json);
			}
		}
		//$this->log->write(print_r($json, true));
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}