<?php
class ControllerCatalogStockItem extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stock_item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stock_item');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/stock_item');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stock_item');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_stock_item->addWaiter($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_item_code'])) {
				$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
			}

			if (isset($this->request->get['filter_id'])) {
				$url .= '&filter_id=' . $this->request->get['filter_id'];
			}

			if (isset($this->request->get['filter_item_name'])) {
				$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/stock_item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stock_item');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_stock_item->editWaiter($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_item_code'])) {
				$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
			}

			if (isset($this->request->get['filter_item_name'])) {
				$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
			}

			if (isset($this->request->get['filter_waiter_id'])) {
				$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/stock_item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stock_item');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_stock_item->deleteWaiter($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_item_code'])) {
				$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
			}

			if (isset($this->request->get['id'])) {
				$url .= '&id=' . $this->request->get['id'];
			}

			if (isset($this->request->get['filter_item_name'])) {
				$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	public function dayclose_items() {
		//echo "in";exit;
		
		$last_sel_datas = $this->db->query("SELECT bill_date FROM `oc_order_info` WHERE `day_close_status` = '0' LIMIT 1 ");
		if($last_sel_datas->num_rows > 0){
			$bill_date = $last_sel_datas->row['bill_date'];
		} else{
			$bill_date = date('Y-m-d');
		}
		$pre_date = date('Y-m-d', strtotime('-1 day', strtotime($bill_date)));

			$stock_items_datas = $this->db->query("SELECT * FROM `oc_stock_item` WHERE `manage_stock` = 1");
			//echo "<pre>";print_r($stock_items_datas);exit;
			if($stock_items_datas->num_rows > 0){
				foreach ($stock_items_datas->rows as $skey => $svalue) {
					$purchase_datas = $this->db->query("SELECT * FROM `purchase_test` WHERE item_code = '".$svalue['item_code']."'");
					if ($purchase_datas->num_rows == 0) {
						$this->db->query("INSERT INTO purchase_test SET 
							item_code = '" .$this->db->escape($svalue['item_code']). "',
							store_id = '" .$this->db->escape($svalue['store_id']). "',
							item_name = '" .$this->db->escape($svalue['item_name']). "',
							purchase_size_id = '" .$this->db->escape($svalue['uom']). "',
							purchase_size = '" .$this->db->escape($svalue['unit_name']). "',
							closing_balance  = '0',
							invoice_date = '".$pre_date."'
							  ");
					$this->session->data['success'] = "Stock inserted successfully";
					}
				}

			}
			$this->response->redirect($this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true));

		
	}

	protected function getList() {
		if (isset($this->request->get['filter_item_code'])) {
			$filter_item_code = $this->request->get['filter_item_code'];
		} else {
			$filter_item_code = null;
		}

		if (isset($this->request->get['filter_item_name'])) {
			$filter_item_name = $this->request->get['filter_item_name'];
		} else {
			$filter_item_name = null;
		}

		if (isset($this->request->get['filter_item_type'])) {
			$filter_item_type = $this->request->get['filter_item_type'];
		} else {
			$filter_item_type = null;
		}

		if (isset($this->request->get['filter_item_code'])) {
			$filter_item_code = $this->request->get['filter_item_code'];
		} else {
			$filter_item_code = null;
		}

		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/stock_item/add', 'token=' . $this->session->data['token'] . $url, true);

		$data['dayclose_items'] = $this->url->link('catalog/stock_item/dayclose_items', 'token=' . $this->session->data['token'] . $url, true);


		$data['delete'] = $this->url->link('catalog/stock_item/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_item_code' => $filter_item_code,
			'filter_item_name' => $filter_item_name,
			'filter_item_type' => $filter_item_type,
			'filter_id' => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_stock_item->getTotalWaiter();

		$results = $this->model_catalog_stock_item->getWaiters($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();
		$data['waiters'] = array();
		foreach ($results as $result) {
			$data['waiters'][] = array(
				'filter_item_code' => $result['item_code'],
				'filter_item_name'        => $result['item_name'],
				'filter_item_type'        => $result['item_type'],
				'filter_id'  => $result['id'],
				'edit'        => $this->url->link('catalog/stock_item/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}


		
		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
        $data['sort_item_code'] = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . '&sort=item_code' . $url, true);
        $data['sort_item_name'] = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . '&sort = item_name' . $url, true);
        $data['sort_item_type'] = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . '&sort = item_type' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_item_code'] = $filter_item_code;
		$data['filter_item_name'] = $filter_item_name;
		$data['filter_item_type'] = $filter_item_type;
		$data['filter_id'] = $filter_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stock_item_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_name'] = $this->error['item_name'];
		} else {
			$data['error_name'] = array();
		}



		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/stock_item/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/stock_item/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$waiter_info = $this->model_catalog_stock_item->getWaiter($this->request->get['id']);
		}

		$data['vendor'] = $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true);
		$data['bom'] = $this->url->link('catalog/bom', 'token=' . $this->session->data['token'] . $url, true);


		$data['token'] = $this->session->data['token'];

		$i_code = $this->db->query(" SELECT item_code FROM oc_stock_item ORDER BY item_code DESC limit 0,1 ");

		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['item_code'] + 1;
		 }else { 
		 	$new_code = 5000; 
		 }

		if (isset($this->request->get['item_code'])) {
			$data['item_code'] = $this->request->get['item_code'];
		} elseif (!empty($waiter_info)) {
			$data['item_code'] = $waiter_info['item_code'];
		} else {
			$data['item_code'] = $new_code;
		}

		if (isset($this->request->post['item_name'])) {
			$data['item_name'] = $this->request->post['item_name'];
		} elseif (!empty($waiter_info)) {
			$data['item_name'] = $waiter_info['item_name'];
		} else {
			$data['item_name'] = '';
		}

		if (isset($this->request->post['bar_code'])) {
			$data['bar_code'] = $this->request->post['bar_code'];
		} elseif (!empty($waiter_info)) {
			$data['bar_code'] = $waiter_info['bar_code'];
		} else {
			$data['bar_code'] = '';
		}

		if (isset($this->request->post['bar_code_1'])) {
			$data['bar_code_1'] = $this->request->post['bar_code_1'];
		} elseif (!empty($waiter_info)) {
			$data['bar_code_1'] = $waiter_info['bar_code_1'];
		} else {
			$data['bar_code_1'] = '';
		}


		$data['item_types']  = array(
			'Raw'  => 'Raw',
			'Finish' => 'Finish',
			'Semi Finish' => 'Semi Finish',
			
		);

		$data['manage_stocks']  = array(
			'1'  => 'Yes',
			'0' => 'No'
		);

		if (isset($this->request->post['item_type'])) {
			$data['item_type'] = $this->request->post['item_type'];
		} elseif (!empty($waiter_info)) {
			$data['item_type'] = $waiter_info['item_type'];
		} else {
			$data['item_type'] = '';
		}

		if (isset($this->request->post['manage_stock'])) {
			$data['manage_stock'] = $this->request->post['manage_stock'];
		} elseif (!empty($waiter_info)) {
			$data['manage_stock'] = $waiter_info['manage_stock'];
		} else {
			$data['manage_stock'] = '';
		}

		$item_category = $this->db->query("SELECT * FROM oc_stockcategory")->rows;
		
		$this->load->model('catalog/stockcategory');
		$stockcategory_datas = $this->model_catalog_stockcategory->getWaiters();
		
		$stockcategory_data = array();
		foreach ($stockcategory_datas as $dkey => $dvalue) {
			$stockcategory_data[$dvalue['id']] = $dvalue['name'];
		}
		$data['stockcategory_data'] = $stockcategory_data;

		$data['item_categorys']  = array(
			'Food' => 'Food',
			'Liquor'  => 'Liquor' 
		);

		if (isset($this->request->post['category'])) {
			$data['category'] = $this->request->post['category'];
		} elseif (!empty($waiter_info)) {
			$data['category'] = $waiter_info['category'];
		} else {
			$data['category'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($waiter_info)) {
			$data['category_id'] = $waiter_info['category_id'];
		} else {
			$data['category_id'] = '';
		}

		if (isset($this->request->post['item_category'])) {
			$data['item_category'] = $this->request->post['item_category'];
		} elseif (!empty($waiter_info)) {
			$data['item_category'] = $waiter_info['item_category'];
		} else {
			$data['item_category'] = '';
		}

		if (isset($this->request->post['show_in_purchase'])) {
			$data['show_in_purchase'] = $this->request->post['show_in_purchase'];
		} elseif (!empty($waiter_info)) {
			$data['show_in_purchase'] = $waiter_info['show_in_purchase'];
		} else {
			$data['show_in_purchase'] = '';
		}

		if (isset($this->request->post['unit_name'])) {
			$data['unit_name'] = $this->request->post['unit_name'];
		} elseif (!empty($waiter_info)) {
			$data['unit_name'] = $waiter_info['unit_name'];
		} else {
			$data['unit_name'] = '';
		}	

		if (isset($this->request->post['category'])) {
			$data['category'] = $this->request->post['category'];
		} elseif (!empty($waiter_info)) {
			$data['category'] = $waiter_info['category'];
		} else {
			$data['category'] = '';
		}

		if (isset($this->request->post['store_id'])) {
			$data['store_id'] = $this->request->post['store_id'];
		} elseif (!empty($waiter_info)) {
			$data['store_id'] = $waiter_info['store_id'];
		} else {
			$data['store_id'] = '';
		}

		if (isset($this->request->post['store_name'])) {
			$data['store_name'] = $this->request->post['store_name'];
		} elseif (!empty($waiter_info)) {
			$data['store_name'] = $waiter_info['store_name'];
		} else {
			$data['store_name'] = '';
		}

		$data['item_umos']  = $this->db->query("SELECT * FROM oc_unit")->rows;

		if (isset($this->request->post['uom'])) {
			$data['uom'] = $this->request->post['uom'];
		} elseif (!empty($waiter_info)) {
			$data['uom'] = $waiter_info['uom'];
		} else {
			$data['uom'] = '';
		}

		if (isset($this->request->post['purchase_rate'])) {
			$data['purchase_rate'] = $this->request->post['purchase_rate'];
		} elseif (!empty($waiter_info)) {
			$data['purchase_rate'] = $waiter_info['purchase_rate'];
		} else {
			$data['purchase_rate'] = '';
		}


		if (isset($this->request->post['reorder_level'])) {
			$data['reorder_level'] = $this->request->post['reorder_level'];
		} elseif (!empty($waiter_info)) {
			$data['reorder_level'] = $waiter_info['reorder_level'];
		} else {
			$data['reorder_level'] = '';
		}


		if (isset($this->request->post['tax'])) {
			$data['tax'] = $this->request->post['tax'];
		} elseif (!empty($waiter_info)) {
			$data['tax'] = $waiter_info['tax'];
		} else {	
			$data['tax'] = '';
		}

		if (isset($this->request->post['tax_value'])) {
			$data['tax_value'] = $this->request->post['tax_value'];
		} elseif (!empty($waiter_info)) {
			$data['tax_value'] = $waiter_info['tax_value'];
		} else {	
			$data['tax_value'] = '';
		}

		$this->load->model('catalog/tax');
		$tax_datas = $this->model_catalog_tax->getWaiters();
		// echo "<pre>";
		// print_r($tax_datas);
		// exit;
		$tax_data = array();
		$tax_data['0'] = 'All';
		foreach ($tax_datas as $dkey => $dvalue) {
			$tax_data[$dvalue['tax_value']] = $dvalue['tax_name'];
		}
		$data['tax_data'] = $tax_data;

		
		$store_datass = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;

		$store_datas = array();
		foreach ($store_datass as $skey => $svalue) {
			$store_datas[$svalue['id']] = $svalue['store_name'];
		}
		$data['store_names'] = $store_datas;

		// echo "<pre>";
		// print_r($store_names);
		// exit;


		if (isset($this->request->post['store_name'])) {
			$data['store_name'] = $this->request->post['store_name'];
		} elseif (!empty($waiter_info)) {
			$data['store_name'] = $waiter_info['store_name'];
		} else {
			$data['store_name'] = '';
		}

		

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stock_item_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/store_name')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['item_name']) < 2) || (utf8_strlen($this->request->post['item_name']) > 255 || (utf8_strlen($this->request->post['item_name']) == '') )) {
			$this->error['item_name'] = 'Enter valid Item Name';
		} 

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/waiter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/stock_item');

			$filter_data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_stock_item->getWaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}