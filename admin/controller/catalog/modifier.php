<?php
class ControllerCatalogModifier extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/modifier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/modifier');

		$this->getList();
	}
	public function add() {
		$this->load->language('catalog/modifier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/modifier');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_catalog_modifier->addItem($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/modifier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/modifier');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_catalog_modifier->editItem($this->request->get['item_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/modifier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/modifier');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_modifier->deleteItem($item_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_item_name'])) {
			$filter_item_name = $this->request->get['filter_item_name'];
		} else {
			$filter_item_name = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}


		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/modifier/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/modifier/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		$filter_data = array(
			'filter_item_id' => $filter_item_id,
			'filter_item_name' => $filter_item_name,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$item_total = $this->model_catalog_modifier->getTotalItems();

		$results = $this->model_catalog_modifier->getItems($filter_data);
		// echo "<pre>";
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$data['items'][] = array(
				'item_id' => $result['id'],
				'item_name'        => $result['modifier_name'],
				//'max_quantity' => $result['max_quantity'],
				'edit'        => $this->url->link('catalog/modifier/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_item_name'] = $this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . '&sort=item_name' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_item_name'] = $filter_item_name;
		$data['filter_item_id'] = $filter_item_id;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/modifier_list', $data));
	}

	protected function getForm() {
		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['item_id'])) {
			$data['action'] = $this->url->link('catalog/modifier/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/modifier/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $this->request->get['item_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/modifier', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_modifier->getItem($this->request->get['item_id']);
		}

		if (isset($this->request->post['modifiername'])) {
			$data['modifiername'] = $this->request->post['modifiername'];
		} elseif (!empty($item_info)) {
			$data['modifiername'] = $item_info['modifier_name'];
		} else {
			$data['modifiername'] = '';
		}

		if (isset($this->request->post['max_quantity'])) {
			$data['max_quantity'] = $this->request->post['max_quantity'];
		} elseif (!empty($item_info)) {
			$data['max_quantity'] = $item_info['max_quantity'];
		} else {
			$data['max_quantity'] = '';
		}

		if (isset($this->request->get['item_id'])){
			$data['testdatas'] = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$this->request->get['item_id']."'")->rows;
		} else{
			$data['testdatas'] = array();
		}
		// echo "<pre>";
		// print_r($this->request->get);
		// exit();

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/modifier_form', $data));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/modifier')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/modifier');

		return !$this->error;
	}

	public function autocompletename() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {

			$results = $this->db->query("SELECT * FROm oc_item WHERE item_name LIKE '%" .$this->request->get['filter_item_name']. "%' AND ismodifier = '1' LIMIT 0,15")->rows;

			//$maximum_quan =  $this->db->query("SELECT * FROm oc_modifier_items WHERE 1=1 ")->rows;

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_code' => $result['item_code'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/modifier');

			$data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_modifier->getItems($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['id'],
					'item_name'        => strip_tags(html_entity_decode($result['modifier_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}