<?php
class ControllerCatalogItem extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/item');

		$this->getList();
	}
	public function add() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		$this->load->model('catalog/item');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
		 //    print_r($this->request->post);
		 //    exit;
			$this->model_catalog_item->addItem($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');
		//echo "<pre>";print_r($this->request->post);exit;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_item->editItem($this->request->get['item_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function generate_short_name() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$items_datas = $this->db->query("SELECT `item_id`, `item_name` FROM `oc_item` WHERE 1=1 ORDER BY `item_id` ")->rows;
		foreach ($items_datas as $ikey => $ivalue) {
			$item_name = $ivalue['item_name'];
			$item_name_exp = explode(' ', $item_name);
			$short_name = '';
			foreach($item_name_exp as $akey => $avalue){
				$short_name .= substr($avalue, 0, 1);
			}
			$update_sql = "UPDATE `oc_item` SET `short_name` = '".$short_name."' WHERE `item_id` = '".$ivalue['item_id']."' ";
			// echo $update_sql;
			// echo '<br />';
			$this->db->query($update_sql);
		}

		$this->session->data['success'] = 'Short Name Updated Successfully';

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
	}


	public function sync_menu() {
		sleep(3);
		//echo "<pre>";print_r($this->request->post);exit;
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

  		$body_data = array(
  			'merchant_id' => MERCHANT_ID
  		);
		

  		$store_sql = $this->db->query("SELECT ref_id,delivery_charge,packaging_charge FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
		if($store_sql->num_rows > 0){
			$store_name = $store_sql->row['ref_id'];
			$delivery_charge = (float)$store_sql->row['delivery_charge'];
			$packaging_charge = (float)$store_sql->row['packaging_charge'];
		} else {
			$store_name = 'ador12';
			$delivery_charge = (float)0.00;
			$packaging_charge = (float)0.00;
		}


  		$category_array = array();
  		
  		$item_category_datas = $this->db->query("SELECT * FROM oc_item WHERE inapp = '1' AND is_liq = '0' AND `api_rate` > '0' GROUP BY item_sub_category_id");
  		foreach ($item_category_datas->rows as $ickey => $icvalue) {
  			$category_datas = $this->db->query("SELECT * FROM oc_subcategory WHERE `category_id` = '".$icvalue['item_sub_category_id']."' ");
	  		if($category_datas->num_rows > 0){
	  			//$in=0;
	  			foreach ($category_datas->rows as $ckey => $cvalue) {
	  				//$in++;
	  				$category_array[] = array(
	  					'ref_id' => $cvalue['category_id'],
	  					'name' => strip_tags(html_entity_decode(ucwords(strtolower($cvalue['name'])), ENT_QUOTES, 'UTF-8')),
	  					'description' => '',
	  					'sort_order' => $cvalue['sort_order_api'],
	  					'active' => true,
	  					'img_url' => '',
	  					'translations' => array()
	  				);
	  			}
	  		}
  		}
  		
  		$tax_cgst_array = array();
  		$tax_sgst_array = array();
  		$tax_array = array();
  		$item_array = array();
  		$sort_order = 0;
  		$charges_array_pickup =  array();
  		$charges_array_delivery =  array();
  		$delivery_array = array();
  		$item_codes_tax =  array();
  		$item_codes_delivery_charge =  array();
  		$item_codes_packaging_charge = array();
  		$item_datas = $this->db->query("SELECT * FROM oc_item WHERE inapp = '1' AND is_liq = '0' AND `api_rate` > '0'")->rows;
  		foreach ($item_datas as $akey => $avalue) {
  			$cgst = 0;
  			if($avalue['vat'] == '6'){
	  			$tax1 = $this->db->query("SELECT tax_value FROM oc_tax WHERE id = '".$avalue['vat']."' ");
	  			if($tax1->num_rows > 0){
	  				$cgst = $tax1->row['tax_value'] / 2;
	  			}
	  		} 


	  		if($avalue['serves'] <= 0){
	  			$serves = 2;
	  		} else {
	  			$serves = $avalue['serves'];
	  		}

	  		//if($avalue['item_stock_api'] <= 0){
	  			$current_stock = -1;
	  		// } else {
	  		// 	$current_stock = $avalue['item_stock_api'];
	  		// }
	  		// if($avalue['packaging_amt'] > 0){
	  		// 	$packaging = $avalue['packaging_amt'];
	  		// } else {
	  		// 	$packaging = 0;
	  		// }
	  		//$item_codes[] = $avalue['item_code'];

	  		if($cgst > 0){
	  			$item_codes_tax[] = $avalue['item_code'];
		  		$tax_cgst_array = array(
		  			'code' => 'CGST_P',
	      			'title' => 'CGST',
	      			'description' => '2.5% CGST on all items',
	      			'active' => true,
	      			'structure' => array ('value' => (float)$cgst),
	      			'item_ref_ids' => $item_codes_tax,
	      			//'location_ref_ids' => array(0 => $store_name),
	      			//'clear_items' => false,
	      			//'clear_locations' => false,
		  		);
		  		$tax_sgst_array = array(
		  			'code' => 'SGST_P',
	      			'title' => 'SGST',
	      			'description' => '2.5% SGST on all items',
	      			'active' => true,
	      			'structure' => array ('value' => (float)$cgst),
	      			'item_ref_ids' => $item_codes_tax,
	      			//'location_ref_ids' => array(0 => $store_name),
	      			//'clear_items' => false,
	      			//'clear_locations' => false,
		  		);
	  		}
	  		

	  		if($delivery_charge > 0 ){
	  			$item_codes_delivery_charge[] = $avalue['item_code'];
	  			$charges_array_delivery = array(
	  			'code' => 'DC_F',
		      	'title' => 'Delivery Charge',
		      	'description' => 'Fixed Delivery Charge on Order Subtotal',
		      	'active' => true,
		      	'structure' => array('applicable_on' => '','value' => $delivery_charge),
		      	'fulfillment_modes' => array (0 => 'delivery'),
      			'excluded_platforms' => array(),
      			'item_ref_ids' => 	$item_codes_delivery_charge,
      			//'location_ref_ids' => array(0 => $store_name),
      			//'clear_items' => false,
      			//'clear_locations' => false,
	  			);
	  		}

	  		if($packaging_charge > 0 ){
	  			$item_codes_packaging_charge[] = $avalue['item_code'];
	  			$charges_array_pickup = array(
	  			'code' => 'PC_F',
		      	'title' => 'Packaging Charge',
		      	'description' => 'Fixed Packing Charge per Item Quantity',
		      	'active' => true,
		      	'structure' => array('applicable_on' => '','value' => $packaging_charge),
		      	'fulfillment_modes' => array (0 => 'pickup',1 => 'delivery'),
      			'excluded_platforms' => array(),
      			'item_ref_ids' => 	$item_codes_packaging_charge,
      			//'location_ref_ids' => array(0 => $store_name),
      			//'clear_items' => false,
      			//'clear_locations' => false,
	  			);
	  		}
	  		$sort_order++;
	  		$food_type = 1;
	  		if($avalue['food_type_api'] > 0){
	  			$food_type = $avalue['food_type_api'];
	  		}

	  		if($avalue['platfome_included'] != ''){
	  			$platform_included = json_decode($avalue['platfome_included']);
	  		} else {
				$platform_included = array (0 => 'zomato', 1 => 'swiggy', 3 => 'foodpanda', 4 => 'amazon', 5 => 'ubereats');
			}

			if($avalue['fullfillmode'] != ''){
	  			$fullfillmode = json_decode($avalue['fullfillmode']);
	  		} else {
				$fullfillmode = array (0 => 'pickup');
			}

			if($avalue['item_stock_api'] == 0){
	  			$available_product = false;
	  		} else {
				$available_product =  true;
			}

	  		// if($avalue['item_sub_category_id'] == 2){
	  		// 	$food_type = 2;
	  		// }

	  		//$category_ref_array[] = $avalue['item_sub_category_id'];
	  		$item_array[] = array(
				"ref_id"=> $avalue['item_code'],
				'title' => strip_tags(html_entity_decode(ucwords(strtolower($avalue['item_name'])), ENT_QUOTES, 'UTF-8')),
				'available' => $available_product,
				'description' => '',
				'weight' => (float)$avalue['weight_for_api'],
				'sold_at_store' => true,
				'sort_order' => $sort_order,
				//'serves' => (int)'',
				'serves' => (int) $serves,
				'external_price' => (float) $avalue['api_rate'],
				'price' => (float) $avalue['api_rate'],
		      	'markup_price' => (float) '',
		     	'current_stock' => (int)$current_stock,
		      	'recommended' => true,
		      	'food_type' => $food_type,
		      	//'category_ref_ids' => $category_ref_array,
		      	'category_ref_ids' => array (0 => $avalue['item_sub_category_id']),
		      	'fulfillment_modes' => (array)$fullfillmode,
		      	'images' => array(),
		      	'img_url' => '',
				'translations' => array(),
				'tags' =>(object) array(),
				'included_platforms' => (array)$platform_included,
				//'included_platforms' =>  array ()
  			);
  		}
  		$final_item_array = array();
  		$tax_array[] = $tax_cgst_array;
  		$tax_array[] = $tax_sgst_array;
  		if(!empty($charges_array_delivery)){
  			$delivery_array[] = $charges_array_delivery;
  		}
  		if(!empty($charges_array_pickup)){
  			$delivery_array[] = $charges_array_pickup;
  		}
  		$final_item_array = array(
  			'flush_categories' => $this->request->post['flush_categories'],
  			'categories' => $category_array,
  			'flush_items' => $this->request->post['flush_items'],
  			'items' => $item_array,
  			'flush_options' => true,
  			'options' => array(),
  			'flush_taxes' => $this->request->post['flush_taxes'],
  			'taxes' => $tax_array,
  			'flush_charges' => $this->request->post['flush_charges'],
  			'charges' => $delivery_array,
  			'store_ids' =>  $store_name,
		);


		//echo "<pre>";print_r($final_item_array);exit;
		$bodys = json_encode($final_item_array,JSON_UNESCAPED_SLASHES);
		
		// $bodys = json_encode($final_item_array, JSON_PRETTY_PRINT);
		
		// echo "<pre>";print_r($bodys);exit;

		//$api_url = sprintf(ITEM_API,$store_name)
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$bodys = json_encode($final_item_array,JSON_UNESCAPED_SLASHES);
		curl_setopt_array($curl, array(
		 // CURLOPT_URL => 'https://pos-int.urbanpiper.com/external/api/v1/inventory/locations/Ador98999/',
		  CURLOPT_URL => sprintf(ITEM_API,$store_name),
		 
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => $bodys,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json',
		   	'Authorization:'.URBANPIPER_API_KEY,
		   	//'Authorization: apikey biz_adm_clients_FLDupyUkibSx:e1a2fbbb587461510fdfb63313ed76fb33c365d6',
		  ),
		));

		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    $error_msg = curl_error($curl);
		    
		}
		$datas = json_decode($response,true);
		curl_close($curl);
		if (isset($error_msg)) {
			//echo "<pre>";print_r($error_msg);
		}
		//echo "<pre>";print_r($response);

		//echo "<pre>";print_r($datas);exit;
		
		date_default_timezone_set("Asia/Kolkata");
		$error = 0;
		$success = 0;
		$json = array();
		if($datas['status'] == 'success'){
			sleep(25);
			$url = Callback_webhook;
			$final_data = array('db_name' => API_DATABASE);
			$data_json = json_encode($final_data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
			$response  = curl_exec($ch);
			if (curl_errno($ch)) {
		    	$error_msg = curl_error($$ch);
			}
			$item_results = json_decode($response,true);

			if (!empty($item_results)) {
				$this->db->query("TRUNCATE `oc_item_urbanpiper`");
				foreach ($item_results as $result_rdr) {
					$this->db->query("INSERT INTO `oc_item_urbanpiper` SET 
						`item_id` = '".$this->db->escape($result_rdr['item_id']) ."', 
						`urbanpiper_id` = '".$this->db->escape($result_rdr['urbanpiper_id']) ."', 
						`title` = '".$this->db->escape($result_rdr['title'])."',
						`category_ref_ids` = '".$this->db->escape($result_rdr['category_ref_ids'])."', 
						`food_type`= '".$this->db->escape($result_rdr['food_type'])."',
						`price`= '".$this->db->escape($result_rdr['price'])."', 
						`included_platforms`= '".$this->db->escape(serialize($result_rdr['included_platforms']))."', 
						`serves` = '".$this->db->escape($result_rdr['serves'])."', 
						`current_stock` = '".$this->db->escape($result_rdr['current_stock'])."',
						`item_onoff_status` = '".$this->db->escape($result_rdr['item_onoff_status'])."'
						 "
					);
				}
				$this->session->data['success'] = 'Items Send Successfully!';
				$success = 1;
			} 
			
			
		} elseif($datas['status'] == 'error') {
			$this->session->data['errors'] = 'Items Not Send! ' .$datas['message'];
			$json['error'] = 'Items Not Send! ' .$datas['message'];
			$error = 1;
		} else if (isset($error_msg)) {
			$this->session->data['errors'] = $error_msg;
			$json['error'] = $error_msg;
			$error = 1;
		} else {
			$this->session->data['errors'] = $response;
			$json['error'] =  $response;
			$error = 1;
		}

		
		$error_plus = 0;
		$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Item_send' ORDER BY id desc Limit 1");
		if ($check_error->num_rows > 0) {
			if($error == 1){
				$error_plus = $check_error->row['failure'] + 1;
			}
			$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
					`sucess`= '".$this->db->escape($success)."', 
					 `date`= NOW(),
					`failure` = '".$this->db->escape($error_plus)."'  
					WHERE `event` = 'Item_send' ");
		} else {
			$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
					`sucess`= '".$this->db->escape($success)."', 
					 `date`= NOW(),
					`failure` = '".$this->db->escape($error)."'  ,
					`event` = 'Item_send'
				");
		}
		
		//curl_close($ch);
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->addHeader('Content-Department: application/json');
		$this->response->setOutput(json_encode($json));

		//$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function item_onoff() {
		
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$store_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
		if($store_sql->num_rows > 0){
			$store_name = $store_sql->row['ref_id'];
		} else {
			$store_name = 'ador12';
		}

  		$body_data = array(
  			// 'merchant_id' =>  MERCHANT_ID
  			'location_ref_id' => $store_name,
  			//'option_ref_ids' =>  array()
  		);
  			
  		if(isset($this->request->get['item_code'])){
  			if($this->request->get['inapp'] == 1){
			//echo '<pre>'; print_r($this->request->get) ;exit;
	  			$item_code = $this->request->get['item_code'];
	  			$item_status = $this->request->get['item_onoff'];

	  			$body_data['item_ref_ids'][] = $item_code;
	  			$body_data['option_ref_ids'] = array();
	  			if($item_status == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}

		  		$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$item_code."' ");
	  			$sync_data = 1; 
	  		} else {
	  			$sync_data = 0; 
	  		}
	  	} else {
	  		$item_datas = $this->db->query("SELECT * FROM oc_item WHERE inapp = '1' ")->rows;
	  		foreach ($item_datas as $akey => $avalue) {
		  		$body_data['item_ref_ids'][] = $avalue['item_code'];
		  		$body_data['option_ref_ids'] = array();
	  			if($avalue['item_onoff_status'] == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}
	  			$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$avalue['item_code']."' ");
	  		}
	  		$sync_data = 1; 
	  	}
	  
	  	//$body_data['action'] = 'disable';
		//echo '<pre>'; print_r($body_data);exit;
		if($sync_data == 1){
			$body = json_encode($body_data);//echo '<pre>'; print_r($body);exit;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt_array($curl, array(
			 // CURLOPT_URL => 'https://pos-int.urbanpiper.com/hub/api/v1/items/',
			CURLOPT_URL => ITEM_ON_OFF,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>$body,
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:'.URBANPIPER_API_KEY,
			    'Content-Type: application/json',
			  ),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
			    $error_msg = curl_error($curl);
			    
			}
			$datas = json_decode($response,true);
			if (isset($error_msg)) {
				
			}
			curl_close($curl);
			$datas = json_decode($response,true);
			//echo $response;exit;
			// echo'<pre>';
			// print_r($value);
			// exit;
			if($datas['status'] == 'success'){
				$this->session->data['success'] = 'Item Status Updated Successfully...!';
			} elseif($datas['status'] == 'error') {
				$this->session->data['errors'] = $datas['message'];
			}else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
			} else {
				$this->session->data['errors'] = $response;
			}
		} else {
			$this->session->data['errors'] = 'Item is not for Sync';

		}

		

		//$this->session->data['success'] = 'Short Name Updated Successfully';

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
	}





	public function import_data() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');
		$valid = 0;
		// echo '<pre>';
		// print_r($this->request->files);
		// exit;
		if(isset($this->request->files['import_data']['name'])){
			$file_name = $this->request->files['import_data']['name'];
			$file_name_exp = explode('.', $file_name);
			// echo $file_name_exp[0];
			// echo $file_name_exp[1];
			// exit();
			 
			if($file_name_exp[1] == 'csv'){
				$valid = 1;
			}
		}
		// echo $valid;exit;
		if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
				$i=1;
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					$item_error_data = array();
					$var0=addslashes($var[0]);//item id
					
					$var1 = html_entity_decode(trim($var[1]));//item code
					// echo $var1;
					// exit();
					if($var1 == ''){
						$item_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Item code Blank',
						);
					}
					$var2=addslashes($var[2]);//department
					
					$var3=addslashes($var[3]);//Item Name
					$var4=addslashes($var[4]);//Purchase_Price
					$var5=addslashes($var[5]);//Description
					$var6=addslashes($var[6]);//Ingredient
					$var7=addslashes($var[7]);//Rate 1
					$var8=addslashes($var[8]);//Rate 2
					$var9=addslashes($var[9]);//Rate 3
					$var10=addslashes($var[10]);//Rate 4
					$var11=addslashes($var[11]);//Rate 5
					$var12=addslashes($var[12]);//Rate 6
					//$var13=addslashes($var[13]);//Item Category Id
					//$var14=addslashes($var[14]);//Item Category
					$var13=addslashes($var[13]);//MRP
					$var14=addslashes($var[14]);//Item Sub Category Id
					//$var17=addslashes($var[15]);//Item Sub Category
					$var15=addslashes($var[15]);//VAT
					$var16=addslashes($var[16]);//Service Tax
					$var17=addslashes($var[17]);//Brand
					$var18=addslashes($var[18]);//Brand Id
					$var19=addslashes($var[19]);//Height
					$var20=addslashes($var[20]);//Width
					$var21=addslashes($var[21]);//Colour

					// echo '<pre>';
					// print_r($var);
					// exit;
					$subcategory_data = $this->db->query("SELECT *  FROM `oc_subcategory` WHERE `category_id` = '".$var[14]."' ")->row;

					//echo "<pre>";print_r($subcategory_data);exit;
					$is_exist = $this->db->query("SELECT *  FROM `oc_item` WHERE `item_id` = '".$var0."' ");
					if($is_exist->num_rows == 0){
						$insert = "INSERT INTO `oc_item` SET
									`item_id` = '".$var0."', 
									`item_code` = '".$var1."',
									`department` = '".$var2."',
									`item_name` = '".$var3."',
									`purchase_price` = '".$var4."',
									`description` = '".$var5."',
									`ingredient` = '".$var6."',
									`rate_1` = '".$var7."',
									`rate_2` = '".$var8."',
									`rate_3` = '".$var9."',
									`rate_4` = '".$var10."',
									`rate_5` = '".$var11."',
									`rate_6` = '".$var12."',
									`item_category_id` = '".$subcategory_data['parent_id']."',
									`item_category` = '".$subcategory_data['parent_category']."',
									`mrp` = '".$var13."',
									`item_sub_category_id` = '".$subcategory_data['category_id']."',
									`item_sub_category` = '".$subcategory_data['name']."',
									`vat` = '".$var15."',
									`s_tax` = '".$var16."',
									`brand` = '".$var17."',
									`brand_id` = '".$var18."',
									`height` = '".$var19."',
									`width` = '".$var20."',
									`colour` = '".$var21."'";

						$this->db->query($insert);
					} else {
							$db_item_data = $is_exist->row;
							$update = "UPDATE `oc_item` SET
										`item_code` = '".$var1."',
										`department` = '".$var2."',
										`item_name` = '".$var3."',
										`purchase_price` = '".$var4."',
										`description` = '".$var5."',
										`ingredient` = '".$var6."',
										`rate_1` = '".$var7."',
										`rate_2` = '".$var8."',
										`rate_3` = '".$var9."',
										`rate_4` = '".$var10."',
										`rate_5` = '".$var11."',
										`rate_6` = '".$var12."',
										`item_category_id` = '".$subcategory_data['parent_id']."',
										`item_category` = '".$subcategory_data['parent_category']."',
										`mrp` = '".$var13."',
										`item_sub_category_id` = '".$subcategory_data['category_id']."',
										`item_sub_category` = '".$subcategory_data['name']."',
										`vat` = '".$var15."',
										`s_tax` = '".$var16."',
										`brand` = '".$var17."',
										`brand_id` = '".$var18."',
										`height` = '".$var19."',
										`width` = '".$var20."',
										`colour` = '".$var21."'
										WHERE `item_id` = '".$var0."' "; 
							$this->db->query($update);
						}
				}
				$i ++;	
			}
			fclose($file);
			
		$students = $this->db->query("SELECT * FROM sc_student")->rows;

		foreach ($students as $key => $value) {

			if($value['dob'] != '00-00-0000' AND $value['dob'] != ''){
				$dob = date('Y-m-d',strtotime($value['dob']));
			}else{
				$dob = '0000-00-00';
			}
			if($value['doa'] != '00-00-0000' AND $value['doa'] != ''){
				$doj = date('Y-m-d',strtotime($value['doa']));
				
			}else{
				$doj = '0000-00-00';
			}

			if($value['dol'] != '00-00-0000' AND $value['dol'] != ''){
				$dol = date('Y-m-d',strtotime($value['dol']));
			}else{
				$dol = '0000-00-00';
			}

			//$student_data = $this->db->query("SELECT `standard_id`, `division_id`, `medium_id` FROM `sc_student` WHERE `student_id` = '".$student_id."' ")->row;
			$standard_name = $this->db->query("SELECT `name` FROM `sc_standard` WHERE `standard_id` = '".$value['standard_id']."' ")->row['name'];
			$division_name = $this->db->query("SELECT `name` FROM `sc_division` WHERE `division_id` = '".$value['division_id']."' ")->row['name'];
			$medium_name = $this->db->query("SELECT `name` FROM `sc_medium` WHERE `medium_id` = '".$value['medium_id']."' ")->row['name'];

			$this->db1->query("INSERT INTO `students` SET 
								id = '" . $this->db->escape($value['student_uid']) . "',
								uniqueid = '" . $this->db->escape($value['student_uid']) . "',
								active = '" . $this->db->escape($value['status']) . "',
								fname = '" . $this->db->escape($value['name']) . "',
								lname = '" . $this->db->escape($value['l_name']) . "',
								mname = '" . $this->db->escape($value['m_name']) . "',
								dob = '" . $this->db->escape($dob) . "',
								DOJ = '" . $this->db->escape($doa). "',
								GR_num = '" . $this->db->escape($value['gr_no']) . "',
								mobile = '" . $this->db->escape($value['phone_no']) . "',
								religion = '" . $this->db->escape($value['religious']) . "',
								caste = '" . $this->db->escape($value['caste']) . "',
								subcaste = '" . $this->db->escape($value['sub_caste']) . "',
								birth_place = '" . $this->db->escape($value['place_birth']) . "',
								mother_tongue = '" . $this->db->escape($value['mother_tongue']) . "',
								nationality = '" . $this->db->escape($value['nationality']) . "',
								mothername = '" . $this->db->escape($value['mother_name']) . "',
								progress = '" . $this->db->escape($value['progress']) . "',
								conduct = '" . $this->db->escape($value['conduct']) . "',
								last_school = '" . $this->db->escape($value['last_school_name']) . "',
								last_medium = '" . $this->db->escape($value['medium_id']) . "',
								aadhar_no = '" . $this->db->escape($value['aadhar_no']) . "',
								taluka = '" . $this->db->escape($value['taluka']) . "',
								district = '" . $this->db->escape($value['district']) . "',
								state = '" . $this->db->escape($value['state']) . "',
								last_std_passed = '" . $this->db->escape($value['last_school_standard']) . "',
								remarks = '" . $this->db->escape($value['remarks']) . "'
								");

			$name = '';

			switch ($standard_name) {

				case 'NURSERY':
					$name = 'NURSERY';
					break;
				case 'JRKG':
					$name = 'JRKG';
					break;
				case 'SRKG':
					$name = 'SRKG';
					break;
				case '1':
					$name = '1';
					break;
				case '2':
					$name = '2';
					break;
				case '3':
					$name = '3';
					break;
				case '4':
					$name = '4';
					break;
				case '5':
					$name = '5';
					break;
				case '6':
					$name = '6';
					break;
				case '7':
					$name = '7';
					break;
				case '8':
					$name = '8';
					break;
				case '9':
					$name = '9';
					break;
				case '10':
					$name = '10';
					break;
				default:
					$name = '0';
					break;
			}

			$this->db1->query("UPDATE `current_year` SET 
								sid = '" . $this->db->escape($student_id) . "',
								year = '" . $this->db->escape("3") . "',							
								GR_num = '" . $this->db->escape($value['gr_no']) . "',
								roll_num = '" . $this->db->escape($value['roll_no']) . "',
								active = '" . $this->db->escape($value['status']) . "',
								reason = '" . $this->db->escape($value['reason_leaving']) . "',
								standard = '" . $this->db->escape($name) . "',
								division = '" . $this->db->escape($division_name) . "',
								leave_date = '" . $this->db->escape($dol) . "'
							");
		}
			// echo '<pre>';
			// print_r($final_item_error_data);
			// exit;

			$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			//echo $url;exit;
			if(empty($final_item_error_data)){
				$this->session->data['success'] = 'Items Imported Successfully';
			} else {
				$this->session->data['warning'] = 'Items Imported with following Errors';
			}
			
			$this->response->redirect($url);
		} else {
			$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['warning'] = 'Please Select the Valid CSV File';
			$this->response->redirect($url);
		}
	}

	// public function import_data() {
	// 	$this->load->language('catalog/item');

	// 	$this->document->setTitle($this->language->get('heading_title'));

	// 	$this->load->model('catalog/item');
	// 	$valid = 0;
	// 	// echo '<pre>';
	// 	// print_r($this->request->files);
	// 	// exit;
	// 	if(isset($this->request->files['import_data']['name'])){
	// 		$file_name = $this->request->files['import_data']['name'];
	// 		$file_name_exp = explode('.', $file_name);
	// 		// echo $file_name_exp[0];
	// 		// echo $file_name_exp[1];
	// 		// exit();
			 
	// 		if($file_name_exp[1] == 'csv'){
	// 			$valid = 1;
	// 		}
	// 	}
	// 	// echo $valid;exit;
	// 	if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
	// 		$file=fopen($this->request->files['import_data']['tmp_name'],"r");
	// 			$i=1;
	// 		while(($var=fgetcsv($file,1000,","))!==FALSE){
	// 			if($i != 1) {
	// 				$item_error_data = array();
	// 				$var0=addslashes($var[0]);//item id
					
	// 				$var1 = html_entity_decode(trim($var[1]));//item code
	// 				// echo $var1;
	// 				// exit();
	// 				if($var1 == ''){
	// 					$item_error_data[] = array(
	// 						'line_number' => $i,
	// 						'reason' => 'Item code Blank',
	// 					);
	// 				}
	// 				// $var2=addslashes($var[2]);//department
					
	// 				// $var3=addslashes($var[3]);//Item Name
	// 				// $var4=addslashes($var[4]);//Purchase_Price
	// 				// $var5=addslashes($var[5]);//Description
	// 				// $var6=addslashes($var[6]);//Ingredient
	// 				// $var7=addslashes($var[7]);//Rate 1
	// 				// $var8=addslashes($var[8]);//Rate 2
	// 				// $var9=addslashes($var[9]);//Rate 3
	// 				// $var10=addslashes($var[10]);//Rate 4
	// 				// $var11=addslashes($var[11]);//Rate 5
	// 				// $var12=addslashes($var[12]);//Rate 6
	// 				// //$var13=addslashes($var[13]);//Item Category Id
	// 				// //$var14=addslashes($var[14]);//Item Category
	// 				// $var13=addslashes($var[13]);//MRP
	// 				// $var14=addslashes($var[14]);//Item Sub Category Id
	// 				// //$var17=addslashes($var[15]);//Item Sub Category
	// 				// $var15=addslashes($var[15]);//VAT
	// 				// $var16=addslashes($var[16]);//Service Tax
	// 				// $var17=addslashes($var[17]);//Brand
	// 				// $var18=addslashes($var[18]);//Brand Id
	// 				// $var19=addslashes($var[19]);//Height
	// 				// $var20=addslashes($var[20]);//Width
	// 				// $var21=addslashes($var[21]);//Colour

	// 				// echo '<pre>';
	// 				// print_r($var);
	// 				// exit;
	// 				$subcategory_data = $this->db->query("SELECT *  FROM `oc_subcategory` WHERE `category_id` = '".$var[6]."' ");

	// 				//echo "<pre>";print_r($subcategory_data);exit;
	// 				if($subcategory_data->num_rows > 0){
	// 					$is_exist = $this->db->query("SELECT *  FROM `oc_item` WHERE `item_id` = '".$var0."' ");
	// 					if($is_exist->num_rows > 0){
	// 						$db_item_data = $is_exist->row;
	// 						$check_category = '0';
	// 						if($var[3] == 'veg'){
	// 							$check_category = '1';
	// 						} else if($var[3] == 'nonveg'){
	// 							$check_category = '2';
	// 						}
	// 						$fullfillmode = '["'."delivery".'"]';
	// 						$platfome_included = '["zomato","swiggy"]';
	// 						$update = "UPDATE `oc_item` SET

	// 									`api_rate`=`rate_4`,
	// 									`inc_api_rate` = `inc_rate_4`,
	// 									`item_onoff_status` = '0',
	// 									`serves`= '2',
	// 									`food_type_api`= '".$check_category."',
	// 									`item_stock_api`= '-1',
	// 									`fullfillmode`= '".$fullfillmode."',
	// 									`platfome_included`= '".$platfome_included."',
	// 									`inapp`= '1'
	// 									WHERE `item_id` = '".$var0."' AND `is_liq` = '0' "; 

							
	// 						$this->db->query($update);
	// 					} 
	// 				}
					
	// 			}
	// 			$i ++;	
	// 		}
	// 		fclose($file);
			
		
	// 		// echo '<pre>';
	// 		// print_r($final_item_error_data);
	// 		// exit;

	// 		$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
	// 		//exit;
	// 		$url = str_replace('&amp;', '&', $url);
	// 		//echo $url;exit;
	// 		if(empty($final_item_error_data)){
	// 			$this->session->data['success'] = 'Items Imported Successfully';
	// 		} else {
	// 			$this->session->data['warning'] = 'Items Imported with following Errors';
	// 		}
			
	// 		$this->response->redirect($url);
	// 	} else {
	// 		$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
	// 		//exit;
	// 		$url = str_replace('&amp;', '&', $url);
	// 		$this->session->data['warning'] = 'Please Select the Valid CSV File';
	// 		$this->response->redirect($url);
	// 	}
	// }
	public function export() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$fp = fopen(DIR_DOWNLOAD . "Item_list.csv", "w"); 
		
		$row = array();
		$row = $this->db->query("SELECT * FROM " . DB_PREFIX . "item")->rows;
		// echo '<pre>';
		// 	 print_r($row);
		// 	 exit();
		$line = "";
		$comma = "";

		$line .= $comma . '"' . str_replace('"', '""', "Item Id") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Item Code") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Department") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Item Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Purchase_Price") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Description") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Ingredient") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 1") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 2") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 3") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 4") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 5") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate 6") . '"';
		$comma = ",";
		// $line .= $comma . '"' . str_replace('"', '""', "Item Category Id") . '"';
		// $comma = ",";
		// $line .= $comma . '"' . str_replace('"', '""', "Item Category") . '"';
		// $comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "MRP") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Item Sub Category Id") . '"';
		$comma = ",";
		//$line .= $comma . '"' . str_replace('"', '""', "Item Sub Category") . '"';
		//$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "VAT") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Service Tax") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Brand") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Brand Id") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Height") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Width") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Colour") . '"';
		$comma = ",";

		$line .= "\n";
		fputs($fp, $line);
		$i ='1';
		foreach($row as $key => $value) { 
				$comma = "";
				$line = "";
				$line .= $comma . '"' . str_replace('"', '""', $value['item_id']) . '"';
				$comma = ",";
				$line .= $comma . '"' . str_replace('"', '""', $value['item_code']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['department']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['item_name']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['purchase_price']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['description']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['ingredient']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_1']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_2']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_3']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_4']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_5']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['rate_6']) . '"';
				//$line .= $comma . '"' . str_replace('"', '""', $value['item_category_id']) . '"';
				//$line .= $comma . '"' . str_replace('"', '""', $value['item_category']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['mrp']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['item_sub_category_id']) . '"';
				//$line .= $comma . '"' . str_replace('"', '""', $value['item_sub_category']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['vat']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['s_tax']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['brand']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['brand_id']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['height']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['width']) . '"';
				$line .= $comma . '"' . str_replace('"', '""', $value['colour']) . '"';
				$i++;
				$line .= "\n";
				fputs($fp, $line);
			}
			//echo $line;exit;
			fclose($fp);
			$filename = "item_list.csv";
			$file_path = DIR_DOWNLOAD . $filename;
			//$file_path = '/Library/WebServer/Documents/riskmanagement/'.$filename;
			$html = file_get_contents($file_path);
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;
		
	}

	public function import_data_online() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');
		$valid = 0;
		// echo '<pre>';
		// print_r($this->request->files);
		// exit;
		if(isset($this->request->files['import_data']['name'])){
			$file_name = $this->request->files['import_data']['name'];
			$file_name_exp = explode('.', $file_name);
			// echo $file_name_exp[0];
			// echo $file_name_exp[1];
			// exit();
			 
			if($file_name_exp[1] == 'csv'){
				$valid = 1;
			}
		}
		// echo $valid;exit;
		if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
				$i=1;
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					$item_error_data = array();
					$var0=addslashes($var[0]);//item id
					
					// exit;
					if(isset($var[7]) && ($var[7] == 1)){
						
						$subcategory_data = $this->db->query("SELECT *  FROM `oc_subcategory` WHERE `category_id` = '".$var[2]."' ");
						if($subcategory_data->num_rows > 0){
							$is_exist = $this->db->query("SELECT *  FROM `oc_item` WHERE `item_id` = '".$var0."' ");
							if($is_exist->num_rows > 0){
								$db_item_data = $is_exist->row;
								$fullfillmode = '["'."delivery".'"]';
								//$platfome_included = '["zomato"]';
								//$platfome_included = '["zomato"]';
								$platfome_included = '["zomato","swiggy"]';
								$update = "UPDATE `oc_item` SET
											`api_rate`= '".$var[4]."',
											`inc_api_rate` = '".$var[5]."',
											`item_onoff_status` = '0',
											`serves`= '2',
											`food_type_api`= '".$var[6]."',
											`item_stock_api`= '-1',
											`fullfillmode`= '".$fullfillmode."',
											`inapp` = '".$var[7]."',
											`platfome_included`= '".$platfome_included."'
											WHERE `item_id` = '".$var0."' AND `is_liq` = '0' "; 
									//echo "<pre>";print_r($update  );
								$this->db->query($update);
							} 
						} 
					}
				}
				$i ++;	
			}//exit;
			fclose($file);
			
		
			// echo '<pre>';
			// print_r($final_item_error_data);
			// exit;

			$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			//echo $url;exit;
			if(empty($final_item_error_data)){
				$this->session->data['success'] = 'Items Imported Successfully';
			} else {
				$this->session->data['warning'] = 'Items Imported with following Errors';
			}
			
			$this->response->redirect($url);
		} else {
			$url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['warning'] = 'Please Select the Valid CSV File';
			$this->response->redirect($url);
		}
	}

	public function delete() {
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');
		//echo "<pre>";print_r($this->request->post['selected']);exit;
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_item->deleteItem($item_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_item_name'])) {
			$filter_item_name = $this->request->get['filter_item_name'];
		} else {
			$filter_item_name = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}

		if (isset($this->request->get['filter_item_code'])) {
			$filter_item_code = $this->request->get['filter_item_code'];
		} else {
			$filter_item_code = null;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}

		if (isset($this->request->get['filter_barcode'])) {
			$filter_barcode = $this->request->get['filter_barcode'];
		} else {
			$filter_barcode = null;
		}

		if (isset($this->request->get['filter_item_category'])) {
			$filter_item_category = $this->request->get['filter_item_category'];
		} else {
			$filter_category = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$data['filter_status'] = $this->request->get['filter_status'];
		}
		else{
			$filter_status = '0';
			$data['filter_status'] = 'In-Active';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $filter_status;
		}

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/item/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/item/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['restore'] = $this->url->link('catalog/item/import_data', 'token=' . $this->session->data['token'] . $url, true);
		$data['import_data_online_func'] = $this->url->link('catalog/item/import_data_online', 'token=' . $this->session->data['token'] . $url, true);
		$data['generate_short_name'] = $this->url->link('catalog/item/generate_short_name', 'token=' . $this->session->data['token'] . $url, true);

		$data['sync_menu'] = $this->url->link('catalog/item/sync_menu', 'token=' . $this->session->data['token'] . $url, true);
		$data['item_onoff'] = $this->url->link('catalog/item/item_onoff', 'token=' . $this->session->data['token'] . $url, true);

		$data['urban_piper_item'] = $this->url->link('catalog/urbanpiperitem_show', 'token=' . $this->session->data['token'] , true);
		
		$data['items'] = array();

		$data['status'] =array(
				'1'  =>"Active",
				'0'  =>'In-Active'
		);


		


		$filter_data = array(
			'filter_status' => $filter_status,
			'filter_item_name' => $filter_item_name,
			'filter_item_id' => $filter_item_id,
			'filter_item_code' => $filter_item_code,
			'filter_department' => $filter_department,
			'filter_barcode' => $filter_barcode,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$item_total = $this->model_catalog_item->getTotalItems($filter_data);
		//$item_total = $this->model_catalog_item->getTotalItems();

		$results = $this->model_catalog_item->getItems($filter_data);
		// echo "<pre>";
		// print_r($filter_data);
		// exit();

		foreach ($results as $result) {
			$data['items'][] = array(
				'item_id' => $result['item_id'],
				'item_name'        => $result['item_name'],
				'item_code'        => $result['item_code'],
				'department'        => $result['department'],
				'rate_1'        => $result['rate_1'],
				'rate_2'        => $result['rate_2'],
				'rate_3'        => $result['rate_3'],
				'rate_4'        => $result['rate_4'],
				'rate_5'        => $result['rate_5'],
				'rate_6'        => $result['rate_6'],
				'barcode'        => $result['barcode'],
				'item_onoff_status'        => $result['item_onoff_status'],

				'item_category'        => $result['item_category'],
				'edit'        => $this->url->link('catalog/item/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $result['item_id'] . $url, true),
				'item_onoff_update'       => $this->url->link('catalog/item/item_onoff', 'token=' . $this->session->data['token'] . '&item_code=' . $result['item_code'].'&item_onoff=' . $result['item_onoff_status'].'&inapp=' . $result['inapp'] . $url, true)
			);
		}

		$data['show_item_send'] = 0;
		$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Item_send' ORDER BY id desc Limit 1");
		if ($check_error->num_rows > 0) {
			if($check_error->row['failure'] == 3){
				$data['show_item_send'] = 1;
			}
		}


		
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['errors'])) {
			$data['errors'] = $this->session->data['errors'];

			unset($this->session->data['errors']);
		} else {
			$data['errors'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $filter_status;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_item_name'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=item_name' . $url, true);
		$data['sort_item_code'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=item_code' . $url, true);
		$data['sort_department'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=department' . $url, true);
		$data['sort_item_category'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=item_category' . $url, true);
		$data['sort_barcode'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=barcode' . $url, true);
		$data['sort_rate_1'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_1' . $url, true);
		$data['sort_rate_2'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_2' . $url, true);
		$data['sort_rate_3'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_3' . $url, true);
		$data['sort_rate_4'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_4' . $url, true);
		$data['sort_rate_5'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_5' . $url, true);
		$data['sort_rate_6'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . '&sort=rate_6' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $filter_status;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_item_name'] = $filter_item_name;
		$data['filter_item_id'] = $filter_item_id;
		$data['filter_item_code'] = $filter_item_code;
		$data['filter_barcode'] = $filter_barcode;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/item_list', $data));
	}

	protected function getForm() {
		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}

		if (isset($this->error['item_code'])) {
			$data['error_item_code'] = $this->error['item_code'];
		} else {
			$data['error_item_code'] = '';
		}

		if (isset($this->error['department'])) {
			$data['error_department'] = $this->error['department'];
		} else {
			$data['error_department'] = '';
		}

		if (isset($this->error['item_category_id'])) {
			$data['error_item_category_id'] = $this->error['item_category_id'];
		} else {
			$data['error_item_category_id'] = '';
		}

		if (isset($this->error['item_sub_category_id'])) {
			$data['error_item_sub_category_id'] = $this->error['item_sub_category_id'];
		} else {
			$data['error_item_sub_category_id'] = '';
		}

		if (isset($this->error['gst_error'])) {
			$data['gst_error'] = $this->error['gst_error'];
		} else {
			$data['gst_error'] = '';
		}

		if (isset($this->error['error_item_stock_api'])) {
			$data['error_item_stock_api'] = $this->error['error_item_stock_api'];
		} else {
			$data['error_item_stock_api'] = '';
		}

		if (isset($this->error['error_food_type_api'])) {
			$data['error_food_type_api'] = $this->error['error_food_type_api'];
		} else {
			$data['error_food_type_api'] = '';
		}

		

		if (isset($this->error['error_serves'])) {
			$data['error_serves'] = $this->error['error_serves'];
		} else {
			$data['error_serves'] = '';
		}

		if (isset($this->error['error_api_rate'])) {
			$data['error_api_rate'] = $this->error['error_api_rate'];
		} else {
			$data['error_api_rate'] = '';
		}

		if (isset($this->error['error_fullfillmode'])) {
			$data['error_fullfillmode'] = $this->error['error_fullfillmode'];
		} else {
			$data['error_fullfillmode'] = '';
		}

		if (isset($this->error['error_platfome_included'])) {
			$data['error_platfome_included'] = $this->error['error_platfome_included'];
		} else {
			$data['error_platfome_included'] = '';
		}

		if (isset($this->error['filename'])) {
			$data['error_filename'] = $this->error['filename'];
		} else {
			$data['error_filename'] = '';
		}

		if (isset($this->error['mask'])) {
			$data['error_mask'] = $this->error['mask'];
		} else {
			$data['error_mask'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['item_id'])) {
			$data['action'] = $this->url->link('catalog/item/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/item/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $this->request->get['item_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/item', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['stock_item'] = $this->url->link('catalog/stock_item', 'token=' . $this->session->data['token'] . $url, true);
		$data['bom'] = $this->url->link('catalog/bom', 'token=' . $this->session->data['token'] . $url, true);

		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_item->getItem($this->request->get['item_id']);
		}

		$data['fullfillment_mode_array']['delivery'] = 'delivery';
		$data['fullfillment_mode_array']['pickup'] = 'pickup';
		

		$data['included_platforms_array']['zomato'] = 'zomato';
		$data['included_platforms_array']['swiggy'] = 'swiggy';
		$data['included_platforms_array']['foodpanda'] = 'foodpanda';
		$data['included_platforms_array']['amazon'] = 'amazon';
		$data['included_platforms_array']['ubereats'] = 'ubereats';

		if (isset($this->request->post['fullfillmode'])) {
			$data['fullfillmode'] = $this->request->post['fullfillmode'];
		} elseif (!empty($item_info['fullfillmode'])) {
			$data['fullfillmode'] = json_decode($item_info['fullfillmode']);
		} else {
			$data['fullfillmode'] = array();
		}


		if (isset($this->request->post['platfome_included'])) {
			$data['platfome_included'] = $this->request->post['platfome_included'];
		} elseif (!empty($item_info['platfome_included'])) {
			$data['platfome_included'] = json_decode($item_info['platfome_included']);
		} else {
			$data['platfome_included'] = array();
		}

		if (isset($this->request->post['weight_for_api'])) {
			$data['weight_for_api'] = $this->request->post['weight_for_api'];
		} elseif (!empty($item_info['weight_for_api'])) {
			$data['weight_for_api'] = json_decode($item_info['weight_for_api']);
		} else {
			$data['weight_for_api'] = array();
		}


		$data['token'] = $this->session->data['token'];
		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
			$data['photo_source'] = $this->request->post['photo_source'];
		} elseif (!empty($item_info)) {
			$data['photo'] = $item_info['photo'];
			$data['photo_source'] = $item_info['photo_source'];
		} else {	
			$data['photo'] = '';
			$data['photo_source'] = '';
		}

		if (isset($this->request->get['item_id'])) {
			$data['item_id'] = $this->request->get['item_id'];
		} elseif (!empty($item_info)) {
			$data['item_id'] = $item_info['item_id'];
		} else {
			$data['item_id'] = 0;
		}

		$i_code = $this->db->query("SELECT item_code FROM oc_item ORDER BY CAST(item_code AS UNSIGNED) DESC LIMIT 1");
		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['item_code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }
		 
		// print_r($i_code);
		// exit();
		if (isset($this->request->post['item_code'])) {
			$data['item_code'] = $this->request->post['item_code'];
		} elseif (!empty($item_info)) {
			$data['item_code'] = $item_info['item_code'];
		} else {
			$data['item_code'] = $new_code ;
		}

		if (isset($this->request->post['activate_item'])) {
			$data['activate_item'] = $this->request->post['activate_item'];
		} elseif (!empty($item_info)) {
			$data['activate_item'] = $item_info['activate_item'];
		} else {
			$data['activate_item'] = '';
		}

		if (isset($this->request->post['is_create_stock'])) {
			$data['is_create_stock'] = $this->request->post['is_create_stock'];
		} elseif (!empty($item_info)) {
			$data['is_create_stock'] = $item_info['is_create_stock'];
		} else {
			$data['is_create_stock'] = '';
		}

		if (isset($this->request->post['create_stock_item'])) {
			$data['create_stock_item'] = $this->request->post['create_stock_item'];
		} elseif (!empty($item_info)) {
			$data['create_stock_item'] = $item_info['create_stock_item'];
		} else {
			$data['create_stock_item'] = '';
		}

		if (isset($this->request->post['no_discount'])) {
			$data['no_discount'] = $this->request->post['no_discount'];
		} elseif (!empty($item_info)) {
			$data['no_discount'] = $item_info['no_discount'];
		} else {
			$data['no_discount'] = '';
		}

		if (isset($this->request->post['stock_register'])) {
			$data['stock_register'] = $this->request->post['stock_register'];
		} elseif (!empty($item_info)) {
			$data['stock_register'] = $item_info['stock_register'];
		} else {
			$data['stock_register'] = '';
		}

		if (isset($this->request->post['decimal_mesurement'])) {
			$data['decimal_mesurement'] = $this->request->post['decimal_mesurement'];
		} elseif (!empty($item_info)) {
			$data['decimal_mesurement'] = $item_info['decimal_mesurement'];
		} else {
			$data['decimal_mesurement'] = '';
		}

		if (isset($this->request->post['inapp'])) {
			$data['inapp'] = $this->request->post['inapp'];
		} elseif (!empty($item_info)) {
			$data['inapp'] = $item_info['inapp'];
		} else {
			$data['inapp'] = '';
		}

		if (isset($this->request->post['ismodifier'])) {
			$data['ismodifier'] = $this->request->post['ismodifier'];
		} elseif (!empty($item_info)) {
			$data['ismodifier'] = $item_info['ismodifier'];
		} else {
			$data['ismodifier'] = '';
		}

		if (isset($this->request->post['department'])) {
			$data['department'] = $this->request->post['department'];
		} elseif (!empty($item_info)) {
			$data['department'] = $item_info['department'];
		} else {
			$data['department'] = '';
		}

		if (isset($this->request->post['kitchen_dispaly'])) {
			$data['kitchen_dispaly'] = $this->request->post['kitchen_dispaly'];
		} elseif (!empty($item_info)) {
			$data['kitchen_dispaly'] = $item_info['kitchen_dispaly'];
		} else {
			$data['kitchen_dispaly'] = '';
		}

		if (isset($this->request->post['item_name'])) {
			$data['item_name'] = $this->request->post['item_name'];
		} elseif (!empty($item_info)) {
			$data['item_name'] = $item_info['item_name'];
		} else {
			$data['item_name'] = '';
		}

		if (isset($this->request->post['purchase_price'])) {
			$data['purchase_price'] = $this->request->post['purchase_price'];
		} elseif (!empty($item_info)) {
			$data['purchase_price'] = $item_info['purchase_price'];
		} else {
			$data['purchase_price'] = '';
		}

		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (!empty($item_info)) {
			$data['description'] = $item_info['description'];
		} else {
			$data['description'] = '';
		}

		if (isset($this->request->post['ingredient'])) {
			$data['ingredient'] = $this->request->post['ingredient'];
		} elseif (!empty($item_info)) {
			$data['ingredient'] = $item_info['ingredient'];
		} else {
			$data['ingredient'] = '';
		}

		if (isset($this->request->post['rate_1'])) {
			$data['rate_1'] = $this->request->post['rate_1'];
		} elseif (!empty($item_info)) {
			$data['rate_1'] = $item_info['rate_1'];
		} else {
			$data['rate_1'] = '';
		}

		if (isset($this->request->post['rate_2'])) {
			$data['rate_2'] = $this->request->post['rate_2'];
		} elseif (!empty($item_info)) {
			$data['rate_2'] = $item_info['rate_2'];
		} else {
			$data['rate_2'] = '';
		}

		if (isset($this->request->post['rate_3'])) {
			$data['rate_3'] = $this->request->post['rate_3'];
		} elseif (!empty($item_info)) {
			$data['rate_3'] = $item_info['rate_3'];
		} else {
			$data['rate_3'] = '';
		}

		if (isset($this->request->post['rate_4'])) {
			$data['rate_4'] = $this->request->post['rate_4'];
		} elseif (!empty($item_info)) {
			$data['rate_4'] = $item_info['rate_4'];
		} else {
			$data['rate_4'] = '';
		}

		if (isset($this->request->post['rate_5'])) {
			$data['rate_5'] = $this->request->post['rate_5'];
		} elseif (!empty($item_info)) {
			$data['rate_5'] = $item_info['rate_5'];
		} else {
			$data['rate_5'] = '';
		}

		if (isset($this->request->post['rate_6'])) {
			$data['rate_6'] = $this->request->post['rate_6'];
		} elseif (!empty($item_info)) {
			$data['rate_6'] = $item_info['rate_6'];
		} else {
			$data['rate_6'] = '';
		}

		if (isset($this->request->post['inc_rate_1'])) {
			$data['inc_rate_1'] = $this->request->post['inc_rate_1'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_1'] = $item_info['inc_rate_1'];
		} else {
			$data['inc_rate_1'] = '';
		}



		if (isset($this->request->post['inc_rate_2'])) {
			$data['inc_rate_2'] = $this->request->post['inc_rate_2'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_2'] = $item_info['inc_rate_2'];
		} else {
			$data['inc_rate_2'] = '';
		}

		if (isset($this->request->post['inc_rate_3'])) {
			$data['inc_rate_3'] = $this->request->post['inc_rate_3'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_3'] = $item_info['inc_rate_3'];
		} else {
			$data['inc_rate_3'] = '';
		}

		if (isset($this->request->post['inc_rate_4'])) {
			$data['inc_rate_4'] = $this->request->post['inc_rate_4'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_4'] = $item_info['inc_rate_4'];
		} else {
			$data['inc_rate_4'] = '';
		}

		if (isset($this->request->post['inc_rate_5'])) {
			$data['inc_rate_5'] = $this->request->post['inc_rate_5'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_5'] = $item_info['inc_rate_5'];
		} else {
			$data['inc_rate_5'] = '';
		}

		if (isset($this->request->post['inc_rate_6'])) {
			$data['inc_rate_6'] = $this->request->post['inc_rate_6'];
		} elseif (!empty($item_info)) {
			$data['inc_rate_6'] = $item_info['inc_rate_6'];
		} else {
			$data['inc_rate_6'] = '';
		}


		if (isset($this->request->post['inc_api_rate'])) {
			$data['inc_api_rate'] = $this->request->post['inc_api_rate'];
		} elseif (!empty($item_info)) {
			$data['inc_api_rate'] = $item_info['inc_api_rate'];
		} else {
			$data['inc_api_rate'] = '';
		}

		if (isset($this->request->post['api_rate'])) {
			$data['api_rate'] = $this->request->post['api_rate'];
		} elseif (!empty($item_info)) {
			$data['api_rate'] = $item_info['api_rate'];
		} else {
			$data['api_rate'] = '';
		}

		if (isset($this->request->post['short_name'])) {
			$data['short_name'] = $this->request->post['short_name'];
		} elseif (!empty($item_info)) {
			$data['short_name'] = $item_info['short_name'];
		} else {
			$data['short_name'] = '';
		}

		if (isset($this->request->post['uom'])) {
			$data['uom'] = $this->request->post['uom'];
		} elseif (!empty($item_info)) {
			$data['uom'] = $item_info['uom'];
		} else {
			$data['uom'] = '';
		}

		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} elseif (!empty($item_info)) {
			$data['company'] = $item_info['company'];
		} else {
			$data['company'] = '';
		}

		if (isset($this->request->post['item_category'])) {
			$data['item_category'] = $this->request->post['item_category'];
		} elseif (!empty($item_info)) {
			$data['item_category'] = $item_info['item_category'];
		} else {
			$data['item_category'] = '';
		}

		if (isset($this->request->post['item_category_id'])) {
			$data['item_category_id'] = $this->request->post['item_category_id'];
		} elseif (!empty($item_info)) {
			$data['item_category_id'] = $item_info['item_category_id'];
		} else {
			$data['item_category_id'] = '1';
		}

		if($data['item_category_id'] == ''){
			$data['item_sub_categorys'] = array();
		} else {
			$item_sub_categorys = $this->db->query("SELECT * FROM oc_subcategory WHERE parent_id = '" . (int)$data['item_category_id']. "' ")->rows;
			$data['item_sub_categorys'] = $item_sub_categorys;			
		}
		 
		if (isset($this->request->post['mrp'])) {
			$data['mrp'] = $this->request->post['mrp'];
		} elseif (!empty($item_info)) {
			$data['mrp'] = $item_info['mrp'];
		} else {
			$data['mrp'] = '';
		}

		if (isset($this->request->post['modifier_group'])) {
			$data['modifier_group'] = $this->request->post['modifier_group'];
		} elseif (!empty($item_info)) {
			$data['modifier_group'] = $item_info['modifier_group'];
		} else {
			$data['modifier_group'] = '';
		}

		if (isset($this->request->post['modifier_qty'])) {
			$data['modifier_qty'] = $this->request->post['modifier_qty'];
		} elseif (!empty($item_info)) {
			$data['modifier_qty'] = $item_info['modifier_qty'];
		} else {
			$data['modifier_qty'] = '';
		}

		if (isset($this->request->post['max_item'])) {
			$data['max_item'] = $this->request->post['max_item'];
		} elseif (!empty($item_info)) {
			$data['max_item'] = $item_info['max_item'];
		} else {
			$data['max_item'] = '';
		}

		if (isset($this->request->post['item_sub_category'])) {
			$data['item_sub_category'] = $this->request->post['item_sub_category'];
		} elseif (!empty($item_info)) {
			$data['item_sub_category'] = $item_info['item_sub_category'];
		} else {
			$data['item_sub_category'] = '';
		}

		if (isset($this->request->post['item_sub_category_id'])) {
			$data['item_sub_category_id'] = $this->request->post['item_sub_category_id'];
		} elseif (!empty($item_info)) {
			$data['item_sub_category_id'] = $item_info['item_sub_category_id'];
		} else {
			$data['item_sub_category_id'] = '';
		}

		// if($data['item_sub_category_id'] == ''){
		// 	$data['brands'] = array();
		// } else {
		// 	$brands = $this->db->query("SELECT * FROM oc_brand WHERE subcategory_id = '" . (int)$data['item_sub_category_id']. "' ")->rows;
		// 	$data['brands'] = $brands;			
		// }

		$data['taxs'] = array();

		$data['taxs'] = $this->db->query( "SELECT * FROM oc_tax")->rows;

		$data['tax_percent'] = '';

		if (isset($this->request->post['vat'])) {
			$data['vat'] = $this->request->post['vat'];
		} elseif (!empty($item_info['vat'])) {
			$data['vat'] = $item_info['vat'];
			if($data['vat'] != '99'){
				$calculate = $this->db->query("SELECT tax_value FROM oc_tax WHERE id='".$data['vat']."'")->row['tax_value'];
				$data['tax_percent'] = $calculate;
			}
		} else {
			$data['vat'] = '';
		}

		if (isset($this->request->post['tax2'])) {
			$data['tax2'] = $this->request->post['tax2'];
		} elseif (!empty($item_info)) {
			$data['tax2'] = $item_info['tax2'];
		} else {
			$data['tax2'] = '';
		}

		if (isset($this->request->post['kotmsg_cat'])) {
			$data['kotmsg_cat'] = $this->request->post['kotmsg_cat'];
		} elseif (!empty($item_info)) {
			$data['kotmsg_cat'] = $item_info['kotmsg_cat_id'];
		} else {
			$data['kotmsg_cat'] = '';
		}

		if (isset($this->request->post['s_tax'])) {
			$data['s_tax'] = $this->request->post['s_tax'];
		} elseif (!empty($item_info)) {
			$data['s_tax'] = $item_info['s_tax'];
		} else {
			$data['s_tax'] = '';
		}

		if (isset($this->request->post['brand'])) {
			$data['brand'] = $this->request->post['brand'];
		} elseif (!empty($item_info)) {
			$data['brand'] = $item_info['brand'];
		} else {
			$data['brand'] = '';
		}

		if (isset($this->request->post['brand_id'])) {
			$data['brand_id'] = $this->request->post['brand_id'];
		} elseif (!empty($item_info)) {
			$data['brand_id'] = $item_info['brand_id'];
		} else {
			$data['brand_id'] = '';
		}

		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($item_info)) {
			$data['height'] = $item_info['height'];
		} else {
			$data['height'] = '';
		}

		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($item_info)) {
			$data['width'] = $item_info['width'];
		} else {
			$data['width'] = '';
		}

		if (isset($this->request->post['birthday_point'])) {
			$data['birthday_point'] = $this->request->post['birthday_point'];
		} elseif (!empty($item_info)) {
			$data['birthday_point'] = $item_info['birthday_point'];
		} else {
			$data['birthday_point'] = '';
		}
		if (isset($this->request->post['normal_point'])) {
			$data['normal_point'] = $this->request->post['normal_point'];
		} elseif (!empty($item_info)) {
			$data['normal_point'] = $item_info['normal_point'];
		} else {
			$data['normal_point'] = '';
		}

		if (isset($this->request->post['quantity'])) {
			$data['quantity'] = $this->request->post['quantity'];
		} elseif (!empty($item_info)) {
			$quantity_id = $item_info['quantity'];
			$quantity_data = $this->db->query("SELECT * from oc_brand_type WHERE id = '".$quantity_id."'");
			if($quantity_data->num_rows > 0){
				$data['quantity'] = $quantity_data->row;
			} else{
				$data['quantity'] = '';
			}
		} else {
			$data['quantity'] = '';
		}

		if($data['quantity'] == ''){
			if($data['brand_id'] != 0){
				$brand_data = $this->db->query("SELECT * FROM oc_brand WHERE brand_id = '".$data['brand_id']."'")->row;
				$type_data = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '" .$brand_data['type_id']. "' ")->rows;
				$data['quantitys'] = $type_data;
			} else{
				$data['quantitys'] = array();
			}
		} else {
			$quantity = array();
			if(isset($data['quantity']['type_id'])){
				$quantity = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '" . (int)$data['quantity']['type_id']. "' ")->rows;
			}
			$data['quantitys'] = $quantity;	
		}

		if (isset($this->request->post['colour'])) {
			$data['colour'] = $this->request->post['colour'];
		} elseif (!empty($item_info)) {
			$data['colour'] = $item_info['colour'];
		} else {
			$data['colour'] = '';
		}

		if (isset($this->request->post['xattr_get(filename, name)'])) {
			$data['hot_key'] = $this->request->post['hot_key'];
		} elseif (!empty($item_info)) {
			$data['hot_key'] = $item_info['hot_key'];
		} else {
			$data['hot_key'] = '';
		}

		if (isset($this->request->post['captain_rate_per'])) {
			$data['captain_rate_per'] = $this->request->post['captain_rate_per'];
		} elseif (!empty($item_info)) {
			$data['captain_rate_per'] = $item_info['captain_rate_per'];
		} else {
			$data['captain_rate_per'] = '';
		}

		if (isset($this->request->post['captain_rate_value'])) {
			$data['captain_rate_value'] = $this->request->post['captain_rate_value'];
		} elseif (!empty($item_info)) {
			$data['captain_rate_value'] = $item_info['captain_rate_value'];
		} else {
			$data['captain_rate_value'] = '';
		}

		if (isset($this->request->post['captain_rate_value'])) {
			$data['captain_rate_value'] = $this->request->post['captain_rate_value'];
		} elseif (!empty($item_info)) {
			$data['captain_rate_value'] = $item_info['captain_rate_value'];
		} else {
			$data['captain_rate_value'] = '';
		}

		if (isset($this->request->post['with_qty'])) {
			$data['with_qty'] = $this->request->post['with_qty'];
		} elseif (!empty($item_info)) {
			$data['with_qty'] = $item_info['with_qty'];
		} else {
			$data['with_qty'] = '';
		}

		if (isset($this->request->post['gst_incu'])) {
			$data['gst_incu'] = $this->request->post['gst_incu'];
		} elseif (!empty($item_info)) {
			$data['gst_incu'] = $item_info['gst_incu'];
		} else {
			$data['gst_incu'] = '';
		}

		if (isset($this->request->post['barcode'])) {
			$data['barcode'] = $this->request->post['barcode'];
		} elseif (!empty($item_info)) {
			$data['barcode'] = $item_info['barcode'];
		} else {
			$data['barcode'] = '';
		}

		if (isset($this->request->post['stweward_rate_per'])) {
			$data['stweward_rate_per'] = $this->request->post['stweward_rate_per'];
		} elseif (!empty($item_info)) {
			$data['stweward_rate_per'] = $item_info['stweward_rate_per'];
		} else {
			$data['stweward_rate_per'] = '';
		}

		if (isset($this->request->post['packaging_charge'])) {
			$data['packaging_charge'] = $this->request->post['packaging_charge'];
		} elseif (!empty($item_info)) {
			$data['packaging_charge'] = $item_info['packaging_amt'];
		} else {
			$data['packaging_charge'] = '';
		}

		if (isset($this->request->post['serves'])) {
			$data['serves'] = $this->request->post['serves'];
		} elseif (!empty($item_info)) {
			$data['serves'] = $item_info['serves'];
		} else {
			$data['serves'] = '1';
		}

		if (isset($this->request->post['item_stock_api'])) {
			$data['item_stock_api'] = $this->request->post['item_stock_api'];
		} elseif (!empty($item_info)) {
			$data['item_stock_api'] = $item_info['item_stock_api'];
		} else {
			$data['item_stock_api'] = '1';
		}


		if (isset($this->request->post['stweward_rate_value'])) {
			$data['stweward_rate_value'] = $this->request->post['stweward_rate_value'];
		} elseif (!empty($item_info)) {
			$data['stweward_rate_value'] = $item_info['stweward_rate_value'];
		} else {
			$data['stweward_rate_value'] = '';
		}

		if (isset($this->request->post['is_liq'])) {
			$data['is_liq'] = $this->request->post['is_liq'];
		} elseif (!empty($item_info)) {
			$data['is_liq'] = $item_info['is_liq'];
		} else {
			$data['is_liq'] = '';
		}

		$results = $this->model_catalog_item->getDepartment();
		$data['departments'] = array();
		foreach ($results as $dvalue) {
			$data['departments'][$dvalue['name']]= $dvalue['name'];
		}
		$results = $this->model_catalog_item->getCategory();
		  

		$data['item_categorys'] = array();
		foreach ($results as $dvalue) {
			$data['item_categorys'][$dvalue['category_id']]= $dvalue['category'];
		}

		$data['food_type_api_array'] = array(
			'0' => 'Please Select',
			'1' => 'VEG ',
			'2' => 'Non-vegetarian',
			'3' => 'Eggetarian',
			'4' => 'Not specified',
		);

		$data['Item_avilable_api'] = array(
			'' => 'Please Select',
			'0' => 'No',
			'-1' => 'Yes',
		);

		if (isset($this->request->post['food_type_api'])) {
			$data['food_type_api'] = $this->request->post['food_type_api'];
		} elseif (!empty($item_info)) {
			$data['food_type_api'] = $item_info['food_type_api'];
		} else {
			$data['food_type_api'] = '';
		}

		$data['kitchen_dispalys'] = array(
			'' => 'Please Select',
			'0' => 'No',
			'1' => 'Yes',
		);
		//$data['item_umos']  = $this->db->query("SELECT * FROM oc_unit")->rows;
		$data['uoms'] = array(
			'' => 'Please Select',
			'1' => '1',
			'2' => '2',
		);
		$data['companys'] = array(
			'' => 'Please Select',
			'1' => '1',
			'2' => '2',
		);
		
		// $data['modifier_groups'] = array(
		// 	'' => 'Please Select',
		// 	'1' => '1',
		// 	'2' => '2',
		// );
		$data['modifier_groups'] = $this->db->query("SELECT * FROM oc_modifier")->rows;
		
		$data['vats'] = array(
			//'' => 'Please Select',
			'5' => '5',
		);
		$data['s_taxs'] = array(
			//'' => 'Please Select',
		);
		
		// $data['quantitys'] = array(
		// 	'' => 'Please Select',
		// 	'1' => '1',
		// 	'2' => '2',
		// );

		$data['captain_rate_values'] = array(
			'' => 'Please Select',
			'1' => 'Rs',
			'2' => '%',
		);
		$data['stweward_rate_values'] = array(
			'' => 'Please Select',
			'1' => 'Rs',
			'2' => '%',
		);

		$data['is_liqs'] = array(
			'' => 'Please Select',
			'1' => 'Yes',
			'0' => 'No',
		);

		$data['kotmsg_cats'] = $this->db->query("SELECT * FROM oc_kotmsg_cat")->rows;
		$data['brands'] = $this->db->query("SELECT * FROM oc_brand")->rows;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/item_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/item')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$datas = $this->request->post;

		if ($this->request->post['department'] == 'Please Select') {
			//$this->error['department'] = 'Please Select Department';
		}

		if ($this->request->post['item_category_id'] == 'Please Select') {
			$this->error['item_category_id'] = 'Please Select Category';
		}  

		if ($this->request->post['item_sub_category_id'] == '' || $this->request->post['item_sub_category_id'] == 'Please Select') {
			$this->error['item_sub_category_id'] = 'Please Select Sub Category';
		} 

		if ((utf8_strlen($datas['item_name']) < 2) || (utf8_strlen($datas['item_name']) > 255)) {
			$this->error['item_name'] = 'Please Enter Item Name';
		} else {
			if(isset($this->request->get['item_id'])){
				$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_name` = '".$datas['item_name']."' AND `item_id` <> '".$this->request->get['item_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['item_name'] = 'Item Name Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_name` = '".$datas['item_name']."' ");
				if($is_exist->num_rows > 0){
					$this->error['item_name'] = 'Item Name Already Exists';
				}
			}
		}

		if ($this->request->post['item_code'] == '') {
			$this->error['item_code'] = 'Please Select Item code';
		} else {
			if(isset($this->request->get['item_id'])){
				$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_code` = '".$datas['item_code']."' AND `item_id` <> '".$this->request->get['item_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['item_code'] = 'Item Code Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_code` = '".$datas['item_code']."' ");
				if($is_exist->num_rows > 0){
					$this->error['item_code'] = 'Item Code Already Exists';
				}
			}
		}


		if (isset($this->request->post['inapp']) && $this->request->post['inapp'] == '1') {
			if ($this->request->post['vat'] == '99') {
				$this->error['gst_error'] = 'Please Select GST';
			}  

			if ($this->request->post['item_stock_api'] == '') {
				$this->error['error_item_stock_api'] = 'Please Select Stock';
			} 


			if ($this->request->post['api_rate'] <= '0' || $this->request->post['api_rate'] == '') {
				$this->error['error_api_rate'] = 'Please Add Rate';
			}  

			if ($this->request->post['serves'] == '0' || $this->request->post['serves'] == '') {
				$this->error['error_serves'] = 'Please Add Serves';
			} 

			if ($this->request->post['food_type_api'] == '0' || $this->request->post['food_type_api'] == '') {
				$this->error['error_food_type_api'] = 'Please select food type';
			} 

			if (empty($this->request->post['fullfillmode'])) {
				$this->error['error_fullfillmode'] = 'Please select atleast one Fulfillment modes';
			} 

			if (empty($this->request->post['platfome_included'])) {
				$this->error['error_platfome_included'] = 'Please select atleast one Included Platform';
			}  
		} 

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/item')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		foreach ($this->request->post['selected'] as $item_id) {
			$item = $this->db->query("SELECT  `item_code`  FROM oc_item WHERE `item_id` = '".$item_id."' ")->row['item_code'];
			$items = $this->db->query("SELECT * FROM oc_order_items WHERE `code` = '".$item."'");
			if ($items->num_rows > 0) {
				$this->error['warning'] = "You Cannot Delete this Item";
			}
		}

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/item');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/item')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	


	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItems($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecode() {
		$json = array();

		if (isset($this->request->get['filter_item_code'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_item_code' => $this->request->get['filter_item_code'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItems($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_code'        => strip_tags(html_entity_decode($result['item_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_code'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletebarcode() {
		$json = array();

		if (isset($this->request->get['filter_barcode'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_barcode' => $this->request->get['filter_barcode'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItems($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_barcode'        => strip_tags(html_entity_decode($result['item_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_barcode'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	// public function autocomplete() {
	// 	$json = array();
	// 	if (isset($this->request->get['filter_name'])) {
	// 		$this->load->model('catalog/customer');
	// 		$data = array(
	// 			'filter_name' => $this->request->get['filter_name'],
	// 			'start'       => 0,
	// 			'limit'       => 20
	// 		);
	// 		$results = $this->model_catalog_customer->getCustomers($data);
	// 		// echo "<pre>";
	// 		// print_r($results);
	// 		// exit();
	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'c_id' => $result['c_id'],
	// 				'name'    => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}		
	// 	}
	// 	$sort_order = array();
	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['name'];
	// 	}
	// 	array_multisort($sort_order, SORT_ASC, $json);
	// 	$this->response->setOutput(json_encode($json));
	// }

	


	public function getsubcategory() {
		$json = array();
		$item_categorys = array();
		if (isset($this->request->get['filter_category_id'])) {
			$this->load->model('catalog/subcategory');
			$data = array(
				'filter_category_idf' => $this->request->get['filter_category_id'],
			);
			$results = $this->model_catalog_subcategory->getSubCategorys($data);

			//echo '<pre>';
			//print_r($results);
			//exit();

			if(isset($this->request->get['list'])){
				$item_categorys[] = array(
					'subcategory_id' => '',
					'subcategory' => 'All'
				);
			} else {
				$item_categorys[] = array(
					'subcategory_id' => '',
					'subcategory' => 'Please Select'
				);
			} 
			foreach ($results as $result) {
				$item_categorys[] = array(
					'subcategory_id' => $result['category_id'],
					'subcategory' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$json['item_categorys'] = $item_categorys;
		$this->response->setOutput(json_encode($json));
	}

	public function gettaxs() {
		$json = array();
		$brands = array();
		if (isset($this->request->get['filter_tax_id'])) {
			$result = $this->db->query("SELECT * FROM oc_tax WHERE id ='".$this->request->get['filter_tax_id']."'")->row;
			$brands = array(
				'tax_value' => $result['tax_value'],
			);
		}
		$json['taxs'] = $brands;
		$this->response->setOutput(json_encode($json));
	}

	// public function getbrands() {
	// 	$json = array();
	// 	$brands = array();
	// 	if (isset($this->request->get['filter_subcategory_id'])) {
	// 		$this->load->model('catalog/tax');
	// 		$data = array(
	// 			'filter_subcategory_id' => $this->request->get['filter_subcategory_id'],
	// 		);
	// 		$results = $this->model_catalog_brand->getBrands($data);

	// 		// echo '<pre>';
	// 		// print_r($results);
	// 		// exit();

	// 		if(isset($this->request->get['list'])){
	// 			$brands[] = array(
	// 				'brand_id' => '',
	// 				'brand' => 'All'
	// 			);
	// 		} else {
	// 			$brands[] = array(
	// 				'brand_id' => '',
	// 				'brand' => 'Please Select'
	// 			);
	// 		} 
	// 		foreach ($results as $result) {
	// 			$brands[] = array(
	// 				'brand_id' => $result['brand_id'],
	// 				'brand' => strip_tags(html_entity_decode($result['brand'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$json['brands'] = $brands;
	// 	$this->response->setOutput(json_encode($json));
	// }

	 public function getBrandQuantity() {
	 	$json = array();
	 	if(isset($this->request->get['brand_id'])){
	 		$typeData = $this->db->query("SELECT type_id FROM oc_brand WHERE brand_id = '".$this->request->get['brand_id']."'")->row;
	 		$type_id = $typeData['type_id'];
	 		$type_quantitys = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$type_id."'")->rows;
	 		$json[] = array(
					'qty_id' => '',
					'qty' => 'Please Select'
				);
	 		foreach($type_quantitys as $key){
	 			$json[] = array(
	 						'qty_id' => $key['id'],
	 						'qty'    => $key['size']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	 }
}