<?php
class ControllerCatalogExpensetrans extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/expensetrans');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expensetrans');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/expensetrans');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expensetrans');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		// 	echo '<pre>';
		// print_r($this->request->post);
		// exit;

			$this->model_catalog_expensetrans->addAccexpense($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_account_name'])) {
				$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
			}

			if (isset($this->request->get['parent_id'])) {
				$url .= '&parent_id=' . $this->request->get['parent_id'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/expensetrans');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expensetrans');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_expensetrans->editAccexpense($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_account_name'])) {
				$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
			}

			if (isset($this->request->get['filter_parent'])) {
				$url .= '&filter_parent=' . $this->request->get['filter_parent'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/expensetrans');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/expensetrans');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_expensetrans->deleteAccexpense($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_account_name'])) {
				$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
			}

			if (isset($this->request->get['filter_acc_exp_type'])) {
				$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = '';
		}

		if (isset($this->request->get['filter_account_name'])) {
			$filter_account_name = $this->request->get['filter_account_name'];
		} else {
			$filter_account_name = '';
		}

		
		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = '';
		}		

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_account_name'])) {
			$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
		}
		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/expensetrans/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/expensetrans/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_account_name' => $filter_account_name,
			'filter_id' => $filter_id,
			'filter_id' => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$table_total = $this->model_catalog_expensetrans->getTotalAccexpense($filter_data);

		$results = $this->model_catalog_expensetrans->getAccexpenses($filter_data);

		$data['tables'] = array();
		foreach ($results as $result) {
			$data['tables'][] = array(
				'id' => $result['id'],
				'payment_type'   => $result['payment_type'],
				'type'        => $result['type'],
				'account_name' => $result['account_name'],
				'edit'        => $this->url->link('catalog/expensetrans/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		 
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_account_name'])) {
			$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['name'] = $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['id'] = $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . '&sort=id' . $url, true);
		$data['sort_parent_category'] = $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . '&sort = parent_category' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_account_name'])) {
			$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
		}
		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}
		
		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $table_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($table_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($table_total - $this->config->get('config_limit_admin'))) ? $table_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $table_total, ceil($table_total / $this->config->get('config_limit_admin')));

		$data['filter_id'] = $filter_id;
		$data['filter_account_name'] = $filter_account_name;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/expensetrans_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_account_name'])) {
			$url .= '&filter_account_name=' . $this->request->get['filter_account_name'];
		}

		if (isset($this->request->get['filter_acc_exp_type'])) {
			$url .= '&filter_acc_exp_type=' . $this->request->get['filter_acc_exp_type'];
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$category_array = $this->model_catalog_expensetrans->getCategory();
		
		$data['payment_types']  = array('PAID IN' =>'PAID IN',
		'PAID OUT' =>'PAID OUT'
		);

		$data['account_types']  = array('Supplier' =>'Supplier',
		'Employee' =>'Employee',
		'Other' =>'Other');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/expensetrans/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/expensetrans/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/expensetrans', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_expensetrans->getAccexpense($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		

		if (isset($this->request->post['payment_type'])) {
			$data['payment_type'] = $this->request->post['payment_type'];
		} elseif (!empty($category_info)) {
			$data['payment_type'] = $category_info['payment_type'];
		} else {
			$data['payment_type'] = '';
		}

		if (isset($this->request->post['type'])) {
			$data['type'] = $this->request->post['type'];
		} elseif (!empty($category_info)) {
			$data['type'] = $category_info['type'];
		} else {
			$data['type'] = '';
		}

		if (isset($this->request->post['account_name'])) {
			$data['account_name'] = $this->request->post['account_name'];
		} elseif (!empty($category_info)) {
			$data['account_name'] = $category_info['account_name'];
		} else {
			$data['account_name'] = '';
		}

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} elseif (!empty($category_info)) {
			$data['amount'] = $category_info['amount'];
		} else {
			$data['amount'] = '';
		}

		if (isset($this->request->post['remarks'])) {
			$data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$data['remarks'] = $category_info['remarks'];
		} else {
			$data['remarks'] = '';
		}
		
		$data['account_names'] = $this->db->query("SELECT * FROM oc_expense_account")->rows;
		//echo "<pre>";print_r($data['filter_account_name']);exit;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/expensetrans_from', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/expensetrans')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $datas = $this->request->post;
		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
		// 	$this->error['name'] = 'Please Enter Name';
		// } else {
		// 	if(isset($this->request->get['id'])){
		// 		$is_exist = $this->db->query("SELECT `id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' AND `id` <> '".$this->request->get['id']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Category Name Already Exists';
		// 		}
		// 	} else {
		// 		$is_exist = $this->db->query("SELECT `id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Category Name Already Exists';
		// 		}
		// 	}
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/subcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/subcategory');

		return !$this->error;
	}

		

	public function autocompletecname() {
		$json = array();

		if (isset($this->request->get['filter_account_name'])) {

			$this->load->model('catalog/expensetrans');

			$filter_data = array(
				'filter_account_name' => $this->request->get['filter_account_name'],
				'start'       => 0,
				'limit'       => 20
			);

			//echo $this->request->get['account_name'];exit;

			$results = $this->model_catalog_expensetrans->getAccexpenses($filter_data);
			//echo "<pre>";print_r($results);exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'account_name'        => strip_tags(html_entity_decode($result['account_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['account_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function types() {
		$json = array();
		$acc_type = array();
		if (isset($this->request->get['types'])) {
			$data = array(
				'types' => $this->request->get['types'],
			);
			//$results = $this->model_catalog_subcategory->getSubCategorys($data);
			$results = $this->db->query("SELECT * FROM oc_expense_account WHERE `exp_acc_type` = '".$this->request->get['types']."'")->rows;
			//echo '<pre>';
			//print_r($results);
			//exit();

			if(isset($this->request->get['list'])){
				$acc_type[] = array(
					'expense_trans_id' => '',
					'exp_acc_name' => 'All'
				);
			} else {
				$acc_type[] = array(
					'expense_trans_id' => '',
					'exp_acc_name' => 'Please Select'
				);
			} 
			foreach ($results as $result) {
				$acc_type[] = array(
					'expense_trans_id' => $result['expense_id'],
					'exp_acc_name' => strip_tags(html_entity_decode($result['exp_acc_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$json['types'] = $acc_type;
		$this->response->setOutput(json_encode($json));
	}

	
}