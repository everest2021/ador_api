<?php
class ControllerCatalogKotTransfer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kottransfer');
		$this->getList();
	}

	public function add() {
		$this->log->write("----------------------Kot Transfer Start-------------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());

		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kottransfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
			$order_items = array();

			$orderid = $this->request->post['orderid'];
			$po_datas = $this->request->post['po_datas'];
			if(isset($this->request->post['orderidtransfer'])){
				$orderidtransfer = $this->request->post['orderidtransfer'];
			} else {
				$orderidtransfer = '';

			}

			// echo'<pre>';
			// print_r($this->request->get);
			// exit;
			// $this->log->write(print_r("postr id", true));

			 //$this->log->write(print_r($po_datas, true));
			// $this->log->write(print_r($this->request->post, true));

			$ischecked = 0;
			foreach ($po_datas as $po_data) {
				if(isset($po_data['checkedtotransfer'])){
					$ischecked = 2;
					if($po_data['parent_id'] == '0' && $po_data['parent'] == '0'){
						$ischecked = 1;
						$codes = $this->db->query("SELECT * FROM `oc_order_items` WHERE id='".$po_data['checkedtotransfer']."'")->rows;
						foreach ($codes as $code) {	
							if($orderidtransfer != ''){
								$transfercode = $this->db->query("SELECT * FROM `oc_order_items` WHERE code='".$code['code']."' AND rate = '".$code['rate']."' AND message = '".$code['message']."' AND order_id='".$orderidtransfer."' AND cancelstatus = '".$code['cancelstatus']."'");
								if($transfercode->num_rows > 0){
									$result = $transfercode->row;
									if($po_data['qty'] < $po_data['pre_qty']){
										$quantity = $po_data['pre_qty'] - $po_data['qty'];
										$result['qty'] = $result['qty'] + $quantity;
										$result['transfer_qty'] = $result['transfer_qty'] + $quantity;
										if($result['nc_kot_status'] == 0){
											$result['amt'] = $result['qty'] * $result['rate'];
										} else {
											$result['amt'] = 0;
										}
										$this->db->query("UPDATE `oc_order_items` SET qty = '".$result['qty']."',amt = '".$result['amt']."', reason = '".$po_data['reason']."', transfer_qty = '".$result['transfer_qty']."', subcategoryid = '".$result['subcategoryid']."', ismodifier = '".$result['ismodifier']."', parent = '".$result['parent']."' WHERE id='".$result['id']."'");
										$this->db->query("UPDATE `oc_order_items` SET qty = '".$po_data['qty']."',amt = '".$po_data['amt']."', subcategoryid = '".$result['subcategoryid']."', ismodifier = '".$result['ismodifier']."', parent = '".$result['parent']."' WHERE id='".$po_data['checkedtotransfer']."'   ");
									}

									if($po_data['qty'] == $po_data['pre_qty']){
										$quantity = $po_data['qty'];
										$result['qty'] = $result['qty'] + $quantity;
										$result['transfer_qty'] = $result['transfer_qty'] + $quantity;
										if($result['nc_kot_status'] == 0){
											$result['amt'] = $result['qty'] * $result['rate']; 
										} else {
											$result['amt'] = 0;
										}
										//echo'innn';
										$this->db->query("UPDATE `oc_order_items` SET qty = '".$result['qty']."',amt = '".$result['amt']."', reason = '".$po_data['reason']."', transfer_qty = '".$result['transfer_qty']."', subcategoryid = '".$result['subcategoryid']."', ismodifier = '".$result['ismodifier']."', parent = '".$result['parent']."' WHERE id='".$result['id']."'   ");					
										$this->db->query("DELETE FROM `oc_order_items` WHERE id ='".$po_data['checkedtotransfer']."'");
									}
								} else {
									if($po_data['qty'] < $po_data['pre_qty']){
										$quantity = $po_data['pre_qty'] - $po_data['qty'];
										if($po_data['nc_kot_status'] == 0){
											$amt = $quantity * $po_data['rate']; 
										} else {
											$amt = 0;
										}
										$this->db->query("INSERT INTO oc_order_items SET 
											order_id='" . $this->db->escape($orderidtransfer) . "',
											code='" . $this->db->escape($po_data['code']) . "',
											name='" . $this->db->escape($po_data['name']) . "',
											qty='" . $this->db->escape($quantity) . "',
											transfer_qty='" . $this->db->escape($quantity) . "',
											rate='" . $this->db->escape($po_data['rate']) . "',
											amt='" . $this->db->escape($amt) . "',
											message='" . $this->db->escape($po_data['message']) . "',
											is_liq='" . $this->db->escape($po_data['is_liq']) . "',
											kot_status='" . $this->db->escape($po_data['kot_status']) . "',
											pre_qty='" . $this->db->escape($po_data['pre_qty']) . "',
											tax1='" . $this->db->escape($code['tax1']) . "',
											tax2='" . $this->db->escape($code['tax2']) . "',
											subcategoryid='" . $this->db->escape($code['subcategoryid']) . "',
											ismodifier='" . $this->db->escape($code['ismodifier']) . "',
											bill_date='" . $this->db->escape($code['bill_date']) . "',
											parent='" . $this->db->escape($code['parent']) . "'
										");


										$this->db->query("UPDATE `oc_order_items` SET qty = '".$po_data['qty']."',amt = '".$po_data['amt']."', subcategoryid = '".$code['subcategoryid']."', ismodifier = '".$code['ismodifier']."', parent = '".$code['parent']."' WHERE id='".$po_data['checkedtotransfer']."' AND cancelstatus = '".$code['cancelstatus']."'");
									

									}

									if($po_data['qty'] == $po_data['pre_qty']){
										$quantity = $po_data['qty'];
										if($po_data['nc_kot_status'] == 0){
											$amt = $quantity * $po_data['rate']; 
										} else {
											$amt = 0;
										}
										 $this->db->query("INSERT INTO oc_order_items SET 
											order_id='" . $this->db->escape($orderidtransfer) . "',
											code='" . $this->db->escape($po_data['code']) . "',
											name='" . $this->db->escape($po_data['name']) . "',
											qty='" . $this->db->escape($quantity) . "',
											transfer_qty='" . $this->db->escape($quantity) . "',
											rate='" . $this->db->escape($po_data['rate']) . "',
											amt='" . $this->db->escape($amt) . "',
											message='" . $this->db->escape($po_data['message']) . "',
											is_liq='" . $this->db->escape($po_data['is_liq']) . "',
											kot_status='" . $this->db->escape($po_data['kot_status']) . "',
											tax1='" . $this->db->escape($code['tax1']) . "',
											tax2='" . $this->db->escape($code['tax2']) . "',
											subcategoryid='" . $this->db->escape($code['subcategoryid']) . "',
											ismodifier='" . $this->db->escape($code['ismodifier']) . "',
											parent='" . $this->db->escape($code['parent']) . "',
											bill_date='" . $this->db->escape($code['bill_date']) . "',
											time='" . $this->db->escape($code['time']) . "',
											date='" . $this->db->escape($code['date']) . "'
										");

										$orderitemid = $this->db->getLastId();
										if($code['ismodifier'] == '1' && $code['cancelstatus'] == '0'){
											$autoincrementparentid = $orderitemid;
										} else{
											$autoincrementparentid = 0;
										}

										$this->db->query("DELETE FROM `oc_order_items` WHERE id ='".$po_data['checkedtotransfer']."'");
									}
								}
							}

							if($orderidtransfer == ''){
								if($po_data['qty'] < $po_data['pre_qty']){
									 //echo "7\n";
									$quantity = $po_data['pre_qty'] - $po_data['qty'];
									if($po_data['nc_kot_status'] == 0){
										$amt = $quantity * $po_data['rate']; 
									} else {
										$amt = 0;
									}
									$order_items[] = array(
										'code'			=>$code['code'],
										'name'			=>$code['name'],
										'qty'			=>$quantity,
										'transfer_qty' 	=>$quantity,
										'rate'			=>$code['rate'],
										'amt'			=>$amt,
										'message'		=>$code['message'],
										'is_liq'		=>$code['is_liq'],
										'kot_status'	=>$code['kot_status'],
										'pre_qty'		=>$code['pre_qty'],
										'is_new'		=>$code['is_new'],
										'reason'		=>$code['reason'],
										'tax1'			=>$code['tax1'],
										'tax2'			=>$code['tax2'],
										'subcategoryid' =>$code['subcategoryid'],
										'ismodifier' 	=>$code['ismodifier'],
										'parent' 		=>$code['parent'],
										'bill_date' 	=>$code['bill_date'],
										'date'			=>$code['date'],
										'time'			=>$code['time'],
										'login_id'		=>$code['login_id'],
										'login_name'	=>$code['login_name'],
										'stax' 			=>$code['stax']
									);
									//$this->db->query("UPDATE `oc_order_items` SET qty = '".$po_data['qty']."',amt = '".$po_data['amt']."', subcategoryid = '".$po_data['subcategoryid']."', ismodifier = '".$po_data['ismodifier']."', parent = '".$po_data['parent']."' WHERE id='".$po_data['checkedtotransfer']."'  AND cancelstatus = '".$po_data['cancelstatus']."'");	
									$this->db->query("UPDATE `oc_order_items` SET qty = '".$po_data['qty']."',amt = '".$po_data['amt']."', parent = '".$po_data['parent']."' WHERE id='".$po_data['checkedtotransfer']."'");	
									
								}
								
								if($po_data['qty'] == $po_data['pre_qty']){
									//echo "8\n";
									$order_items[] = array(
										'code'			=>$code['code'],
										'name'			=>$code['name'],
										'qty'			=>$code['qty'],
										'transfer_qty' 	=>$code['qty'],
										'rate'			=>$code['rate'],
										'amt'			=>$code['amt'],
										'message'		=>$code['message'],
										'is_liq'		=>$code['is_liq'],
										'kot_status'	=>$code['kot_status'],
										'pre_qty'		=>$code['pre_qty'],
										'is_new'		=>$code['is_new'],
										'reason'		=>$code['reason'],
										'tax1'			=>$code['tax1'],
										'tax2'			=>$code['tax2'],
										'subcategoryid'	=>$code['subcategoryid'],
										'ismodifier'	=>$code['ismodifier'],
										'parent'		=>$code['parent'],
										'bill_date' 	=>$code['bill_date'],
										'date'			=>$code['date'],
										'time'			=>$code['time'],
										'login_id'		=>$code['login_id'],
										'login_name'	=>$code['login_name'],
										'stax' 			=>$code['stax']

									);	
									$this->db->query("DELETE FROM `oc_order_items` WHERE id='".$po_data['checkedtotransfer']."'");
								}
							}
						}
					}
				}	
			}
		
		
			if(!empty($order_items)){
				$totalitemscount = count($order_items);
				$newftotal = 0;
				$newltotal = 0;
				$newgst = 0;
				$newvat = 0;
				$newgrand_total = 0;
				$totalquantitycount = 0;
				foreach ($order_items as $orderitem) {
					if($orderitem['is_liq'] == '0'){
						$newftotal = $newftotal + $orderitem['amt'];
						$newtaxfood1 = $orderitem['tax1'];
					}
					if($orderitem['is_liq'] == '1'){
						$newltotal = $newltotal + $orderitem['amt'];
						$newtaxliq1 = $orderitem['tax1'];
					}
					$newgrand_total = $newgrand_total + $orderitem['amt'];
					$totalquantitycount = $totalquantitycount + $orderitem['qty']; 
				}
				if($newftotal > 0){
					if($newtaxfood1 > 0){
						$newgst = $newftotal * ($newtaxfood1/100);
					}
				}
				if($newltotal > 0){
					if($newtaxliq1 > 0){
						$newvat = $newltotal * ($newtaxliq1/100);
					}
				}
				$newgrand_totalval = $newgrand_total + $newgst + $newvat;
				$from_order_info = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$orderid."'")->row;
				$neworderid = $this->db->query("SELECT order_id, kot_no FROM oc_order_info ORDER BY order_id DESC, kot_no DESC LIMIT 1")->row;
				//$orderidval = $neworderid['order_id'] + 1 ;
				$kotnoval = $neworderid['kot_no'] + 1 ;
				$this->db->query("INSERT INTO oc_order_info SET
						kot_no   = '".$kotnoval."',
						location = '".$this->request->post['location']."',
						location_id = '".$this->request->post['location_id']."',
						t_name = '".$this->request->post['transfer_table']."',
						table_id = '".$this->request->post['transfer_table']."',
						waiter_code = '".$from_order_info['waiter_code']."',
						waiter = '".$from_order_info['waiter']."',
						waiter_id = '".$from_order_info['waiter_id']."',
						captain_code = '".$from_order_info['captain_code']."',
						captain = '".$from_order_info['captain']."',
						captain_id = '".$from_order_info['captain_id']."',
						person = '".$from_order_info['person']."',
						ftotal = '".$newftotal."',
						ftotal_discount = '".$newftotal."',
						gst = '".$newgst."',
						ltotal = '".$newltotal."',
						ltotal_discount = '".$newltotal."',
						vat = '".$newvat."',
						date = '".$from_order_info['date']."',
						time = '".$from_order_info['time']."',
						bill_date = '".$from_order_info['bill_date']."',
						rate_id = '".$from_order_info['rate_id']."',
						grand_total = '".$newgrand_totalval."',
						total_items = '".$totalitemscount."',
						item_quantity = '".$totalquantitycount."',
						date_added = '".$from_order_info['date_added']."',
						login_id = '".$from_order_info['login_id']."',
						login_name = '".$from_order_info['login_name']."',
						time_added = '".$from_order_info['time_added']."'");

				$orderinfoid = $this->db->getLastId();

				$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date = date('Y-m-d');
				}

				$itemskotfood = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
				if($itemskotfood->num_rows > 0){
					$itemskotfoodval = $itemskotfood->row['kot_no'] + 1; 
				} else{
					$itemskotfoodval = 1 ;
				}

				$itemskotliq = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 1 order by oit.`kot_no` DESC LIMIT 1");
				if($itemskotliq->num_rows > 0){
					$itemskotliqval = $itemskotliq->row['kot_no'] + 1;
				} else{
					$itemskotliqval = 1;
				}

				foreach ($order_items as $orderitem) {
					if($orderitem['is_liq'] == '0'){
						$this->db->query("INSERT INTO oc_order_items SET 
							order_id='" .$orderinfoid. "',
							code='" .$orderitem['code']. "',
							name='" .$orderitem['name']. "',
							qty='" .$orderitem['qty']. "',
							transfer_qty='" . $orderitem['transfer_qty']. "',
							rate='" . $orderitem['rate']. "',
							amt='" . $orderitem['amt']. "',
							message='" . $orderitem['message']. "',
							is_liq='" . $orderitem['is_liq']. "',
							kot_status='" . $orderitem['kot_status']. "',
							pre_qty='" . $orderitem['pre_qty']. "',
							is_new='" . $orderitem['is_new']. "',
							kot_no='" . $itemskotfoodval. "',
							reason='" . $orderitem['reason']. "',
							tax1='" .$orderitem['tax1']. "',
							tax2='" .$orderitem['tax2']. "',
							subcategoryid='" .$orderitem['subcategoryid']. "',
							ismodifier='" .$orderitem['ismodifier']. "',
							parent='" .$orderitem['parent']. "',
							bill_date='" . $orderitem['bill_date']. "',
							date='" .$orderitem['date']. "',
							time='" .$orderitem['time']. "',
							login_id='" .$orderitem['login_id']. "',
							login_name='" .$orderitem['login_name']. "',
							stax='" .$orderitem['stax']. "'

							");
					}
					if($orderitem['is_liq'] == '1'){
							$this->db->query("INSERT INTO oc_order_items SET 
							order_id='" .$orderinfoid. "',
							code='" .$orderitem['code']. "',
							name='" .$orderitem['name']. "',
							qty='" .$orderitem['qty']. "',
							transfer_qty='" . $orderitem['transfer_qty']. "',
							rate='" . $orderitem['rate']. "',
							amt='" . $orderitem['amt']. "',
							message='" . $orderitem['message']. "',
							is_liq='" . $orderitem['is_liq']. "',
							kot_status='" . $orderitem['kot_status']. "',
							pre_qty='" . $orderitem['pre_qty']. "',
							is_new='" . $orderitem['is_new']. "',
							kot_no='" . $itemskotliqval. "',
							reason='" . $orderitem['reason']. "',
							tax1='" .$orderitem['tax1']. "',
							tax2='" .$orderitem['tax2']. "',
							subcategoryid='" .$orderitem['subcategoryid']. "',
							ismodifier='" .$orderitem['ismodifier']. "',
							parent='" .$orderitem['parent']. "',
							bill_date='" . $orderitem['bill_date']. "',
							date='" .$orderitem['date']. "',
							time='" .$orderitem['time']. "',
							login_id='" .$orderitem['login_id']. "',
							login_name='" .$orderitem['login_name']. "',
							stax='" .$orderitem['stax']. "'
							");
					}
				}
			}
			$deletedata = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$orderid."'");
			$in = 0; 
			if($deletedata->num_rows == 0){
				$in = 1;
				$this->db->query("DELETE FROM oc_order_info WHERE order_id = '".$orderid."'");
			}
			if($orderid != '' && $orderid != 1 && $in == 0 ){
				$this->updateorderinfo($orderid);
					//$this->log->write(print_r('in infooooo', true));
			}
			if($orderidtransfer != ''){
				$this->updateorderinfo($orderidtransfer);
					//$this->log->write(print_r('inssss', true));

			}
			if($ischecked == 1){
				$json['done'] = '<script>parent.closeIFrame();</script>';
				$json['info'] = 1;
				$this->response->setOutput(json_encode($json));
			} elseif($ischecked == 0){
				$json['done'] = '<script>parent.closeIFrame();</script>';
				$json['info'] = 0;
				$this->response->setOutput(json_encode($json));
			} elseif($ischecked == 2){
				$json['info'] = 2;
				$this->response->setOutput(json_encode($json));
			}
		
		}
		$this->log->write("---------------------------Kot transfer end------------------------------");

	}

	public function updateorderinfo($orderid){
		$this->log->write("---------------------------Update orderinfo Start------------------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());


		$this->load->model('catalog/order');
		$ftotal = 0;
		$ltotal = 0;
		$gst = 0;
		$vat = 0;
		$grand_total = 0;
		$ftotalvalue = 0;
		$ftotal_discount = 0;
		$ltotalvalue = 0;
		$ltotal_discount = 0;
		$gsttaxamt = 0;
		$staxfood = 0;
		$staxliq = 0;
		$vattaxamt = 0;
		$totalamtfood = 0;
		$totalamtliq = 0;

		$delete = $this->db->query("SELECT * FROM oc_order_items WHERE (qty = '0' OR qty = '0.00') AND cancelstatus = '0'");
		$deleteinfo = $this->db->query("SELECT * FROM oc_order_info WHERE (total_items = '0' OR total_items = '0.00') AND cancel_status = '0' ");
		if($deleteinfo->num_rows > 0){
			$this->db->query("DELETE  FROM oc_order_info WHERE (total_items = '0' OR total_items = '0.00') AND cancel_status = '0' ");
		}
		if($delete->num_rows > 0){
			$this->db->query("DELETE  FROM oc_order_items WHERE (qty = '0' OR qty = '0.00') AND cancelstatus = '0' ");
		}
		$totalitems = $this->db->query("SELECT count(*) as id, SUM(qty) as quantity FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND ismodifier = '1' AND cancelstatus = '0'")->row;

		$fromtotable = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '0' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		$fromtotableliquir = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '1' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		// $this->log->write(print_r("orderid" , true));
		// $this->log->write(print_r($orderid , true));
		
		$order_fromtotable = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id` = '".$orderid."'")->row;
		// $this->log->write(print_r("order from to table" , true));
		// $this->log->write(print_r($order_fromtotable['location_id'] , true));
		
		$tax_food1 = 0;
		foreach ($fromtotable as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
					$valtax2 = $taxes->row['tax2'];
				} else{
					$vattax = '0';
					$valtax2 = '0';
				}
				$taxfood1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxfood1->num_rows > 0){
					$tax_food1 = $taxfood1->row['tax_value'];
				} else{
					$tax_food1 = '0';
				}
				$taxfood2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$valtax2."'");
				if($taxfood2->num_rows > 0){
					$tax_food2 = $taxfood2->row['tax_value'];
				} else{
					$tax_food2 = '0';
				}
			}
			$ftotal = $ftotal + $value['amt'];
		}
		$servicechargefood = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$service_charge = $this->db->query("SELECT `service_charge` FROM oc_location WHERE `location_id` = '".$order_fromtotable['location_id']."'")->row['service_charge'];
		//$this->log->write(print_r($order_fromtotable['location_id'] , true));
		
		$staxfood = 0;
		foreach ($fromtotable as $key => $value) {
			if($order_fromtotable['fdiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['fdiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);		
					}else{
						$staxfood_1 = 0;	
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
		  		$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
		  		$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['fdiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}elseif($order_fromtotable['discount'] > 0){
				$discount_per = (($order_fromtotable['discount'])/$ftotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);		
					} else {
						$staxfood_1 = 0;	
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
				$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}else{
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $value['amt'] * ($servicechargefood/100);		
					} else {
						$staxfood_1 = 0;	
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($value['amt'] + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
				$ftotal_discount = $ftotal_discount + $value['amt']; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_f = $staxfood;
		$gst = $gsttaxamt;
		
		$tax_liq1 = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
				} else{
					$vattax = '0';
				}
				$taxliq1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxliq1->num_rows > 0){
					$tax_liq1 = $taxliq1->row['tax_value'];
				} else{
					$tax_liq1 = '0';
				}
			}
			$ltotal = $ltotal + $value['amt'];
		}

		$servicechargeliq = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		$staxliq = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($order_fromtotable['ldiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['ldiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					} else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['ldiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} elseif($order_fromtotable['ldiscount'] > 0){
				$discount_per = ($order_fromtotable['ldiscount']/$ltotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} else{
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $value['amt'] * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($value['amt'] + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotal_discount = $ltotal_discount + $value['amt']; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_l = $staxliq;
		$vat = $vattaxamt;
		$stax = $stax_l + $stax_f;
		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
			$grand_total = ($ftotal + $ltotal + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		} else{
			$grand_total = ($ftotal + $ltotal + $gst + $vat + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		}
		$grand_totals = number_format($grand_total,2,'.','');
		$grand_total_explode = explode('.', $grand_total);
		$roundtotal = 0;
		if(isset($grand_total_explode[1])){
			if($grand_total_explode[1] >= 50){
				$roundtotals = 100 - $grand_total_explode[1];
				$roundtotal = ($roundtotals / 100); 
			} else {
				$roundtotal = -($grand_total_explode[1] / 100);
			}
		}
		$dtotalvalue = 0;
		if($order_fromtotable['dchargeper'] > 0){
			$dtotalvalue = $grand_total * ($order_fromtotable['dchargeper'] / 100);
		} elseif($order_fromtotable['dcharge'] > 0){
			$dtotalvalue = $order_fromtotable['dcharge'];
		}
		$totalitemandqty = $this->db->query("SELECT SUM(qty) as totalqty, COUNT(*) as totalitem FROM oc_order_items WHERE order_id = '".$orderid."' AND cancelstatus = '0' AND ismodifier = '1'")->row;
		//$totalvalue = ($order_fromtotable['ltotalvalue'] + $order_fromtotable['ftotalvalue'] + $order_fromtotable['discount'] + $order_fromtotable['ldiscount']);
		//$grand_total = $grand_total - $totalvalue;
		$this->db->query("UPDATE `oc_order_info` 
									SET `ftotal` = '".$ftotal."', 
									`ltotal` = '".$ltotal."', 
									`gst` = '".$gst."', 
									`vat` = '".$vat."', 
									`grand_total` = '".$grand_total."', 
									`dtotalvalue` = '".$dtotalvalue."', 
									`ftotalvalue` = '".$ftotalvalue."', 
									`ftotal_discount` = '".$ftotal_discount."', 
									`ltotalvalue` = '".$ltotalvalue."', 
									`ltotal_discount` = '".$ltotal_discount."', 
									`staxfood` = '".$stax_f."', 
									`staxliq` = '".$stax_l."', 
									`stax` = '".$stax."',
									`roundtotal` = '".$roundtotal."',
									`total_items` = '".$totalitemandqty['totalitem']."',
									`item_quantity` = '".$totalitemandqty['totalqty']."',
									`login_id` = '".$this->user->getId()."',
									`login_name` = '".$this->user->getUserName()."',
									`cancel_status` = '0'
									WHERE `order_id` = '".$orderid."'");


	 //   	$test = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '0'")->rows;
		// $testmain = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '1'")->rows;
		// foreach($testmain as $testmaindata){
		// 	echo "10\n";
		// 	foreach($test as $testdata){
		// 		if($testdata['parent_id'] == $testmaindata['id']){
		// 			$this->log->write("in");
		// 			$testdata['qty'] = $testmaindata['qty'];
		// 			$testdata['amt'] = $testdata['qty'] * $testdata['rate'];
		// 			$this->db->query("UPDATE oc_order_items SET qty = '".$testdata['qty']."', amt = '".$testdata['amt']."' WHERE parent_id ='".$testmaindata['id']."' AND id='".$testdata['id']."'");
		// 			$this->log->write("UPDATE oc_order_items SET qty = '".$testdata['qty']."', amt = '".$testdata['amt']."' WHERE parent_id ='".$testmaindata['id']."' AND id='".$testdata['id']."'");
		// 		}
		// 	}
		// }
		$this->log->write("---------------------------Update orderinfo end----------------------------");

	}

	public function getList() {

		$this->log->write("---------------------------Open Kot Transfer-----------------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());


		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kottransfer');
		
		$data['loc'] = '';
		$data['locid'] = '';
		$data['rate_id'] = '';
		$data['cust_name'] = '';
		$data['cust_id'] = '';
		$data['cust_contact'] = '';
		$data['cust_address'] = '';
		$data['cust_email'] = '';
		$data['table'] = '';
		$data['table_id'] = '';
		$data['waiter'] = '';
		$data['waiter_id'] = '';
		$data['captain'] = '';
		$data['captain_id'] = '';
		$data['person'] = 0;
		$data['ftotal'] = 0;
		$data['gst'] = 0;
		$data['ltotal'] = 0;
		$data['vat'] = 0;
		$data['cess'] = '';
		$data['stax'] = '';
		$data['discount'] = '';
		$data['ldiscount'] = '';
		$data['grand_total'] = '';
		$data['total_items'] = '';
		$data['item_quantity'] = '';
		$data['date_added'] = '';
		$data['time_added'] = '';

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$query = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id`='".$order_id."'")->rows;
			$query1 = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id`='".$order_id."'")->rows;
			$data['orderitems'] = $query1;
			$data['loc'] = $query[0]['location'];
			$data['locid'] = $query[0]['location_id'];
			$data['rate_id'] = $query[0]['rate_id'];
			$data['table'] = $query[0]['t_name'];
			$data['table_id'] = $query[0]['table_id'];
			$data['waiter'] = $query[0]['waiter'];
			$data['waiter_id'] = $query[0]['waiter_id'];
			$data['captain'] = $query[0]['captain'];
			$data['captain_id'] = $query[0]['captain_id'];
			$data['person'] = $query[0]['person'];
			$data['ftotal'] = $query[0]['ftotal'];
			$data['gst'] = $query[0]['gst'];
			$data['ltotal'] = $query[0]['ltotal'];
			$data['vat'] = $query[0]['vat'];
			$data['cess'] = $query[0]['cess'];
			$data['stax'] = $query[0]['stax'];
			$data['discount'] = $query[0]['discount'];
			$data['ldiscount'] = $query[0]['ldiscount'];
			$data['grand_total'] = $query[0]['grand_total'];
			$data['total_items'] = $query[0]['total_items'];
			$data['item_quantity'] = $query[0]['item_quantity'];
			$data['date_added'] = $query[0]['date_added'];
			$data['time_added'] = $query[0]['time_added'];
			$data['cust_name'] = $query[0]['cust_name'];
			$data['cust_address'] = $query[0]['cust_address'];
			$data['cust_id'] = $query[0]['cust_id'];
			$data['cust_contact'] = $query[0]['cust_contact'];
			$data['cust_email'] = $query[0]['cust_email'];
			// echo '<pre>';
			// print_r($query[0]['location']);
			// print_r($query);
			// print_r($query1);
			// exit;
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		$data['display_type'] = type;
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kottransfer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		if(isset($this->request->get['order_id'])) {
			$data['printb'] = $this->url->link('catalog/kottransfer/printsb', 'token=' . $this->session->data['token'] .'&order_id=' . $this->request->get['order_id'], true);
		} else {
			$data['printb'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		}
		$data['delete'] = $this->url->link('catalog/kottransfer/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['orders'] = array();

		$locations =  $this->db->query("SELECT  * FROM " . DB_PREFIX . "location")->rows;
		
		foreach ($locations as $lkey => $loc) {
			if($data['locid'] != ''){
				$data['sel'] = $data['locid'];
			}
				elseif($lkey == 0)
				{
					$data['sel'] = $loc['location_id'];
				}
				$data['locations'][] = array(
						'select' => $data['sel'],
						'name'	=> $loc['location'],
						'id'	=>	$loc['location_id']
					);
			
		}
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		// if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		// } 

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kottransfer_form', $data));

		if (isset($this->request->get['table_running'])) {
			$table_old = $this->request->get['table_running'];
		} else {
			$table_old = '';
		}

		$this->load->model('catalog/order');
		if($this->model_catalog_order->get_settings('SKIPTABLE') == 0) {
			date_default_timezone_set("Asia/Kolkata");
			$sql = "INSERT INTO `oc_running_table` SET `table_id` = '".$table_old."', `date_added` = '".date('Y-m-d')."', `time_added` = '".date('H:i:s')."' ";
			$this->db->query($sql);
			$this->session->data['table_id'] = $table_old;
			// $data['status'] = 1;
		}
		$this->log->write("----------------------------Close Kot----------------------------------------");

	}

	public function tableinfo() {
		$tid = $this->request->get['tid'];
		$lid = $this->request->get['lid'];
		$tab_index = $this->request->get['tab_index'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$display_type = type;

		$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND cancel_status <> '1' ORDER BY order_id DESC LIMIT 1 ");
		$data['html'] = '';
		$data['status'] = 0;
		if($tableinfo->num_rows>0) {
			if($tableinfo->row['bill_status'] == 0){
				$order =	$tableinfo->row['order_id'];
				$order_no =	$tableinfo->row['order_no'];
				$data['order'] = $order;
				$tableitems = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
				$data['i']=1;
				foreach ($tableinfo->row as $tkey => $tinfo) {
					$data[$tkey] = $tinfo;
				}
				$html = '<tr>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>';
					$html .= "<td style='width:0%;padding-top: 2px;padding-bottom: 2px;'></td>";
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Code</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">Name</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>';
					$html .= '<td style="width: 20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>';
					$html .= '<td style="width: 20%;padding-top: 2px;padding-bottom: 2px;">Reason</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;display:none;">Delete</td>';
				$html .= '</tr>';
				foreach ($tableitems as $lkey => $items) {
					$html .= '<tr>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= ''.$data['i'].'<input type="hidden" name="po_datas['.$data['i'].'][nc_kot_status]" value="'.$items['nc_kot_status'].'" id="nc_kot_status'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][kot_status]" value="'.$items['kot_status'].'" id="kot_status'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][pre_qty]" value="'.$items['qty'].'" id="pre_qty'.$data['i'].'" />';
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">'; 
							$html .= '<input style="padding: 0px 1px;" type="checkbox" name="po_datas['.$data['i'].'][checkedtotransfer]" value="'.$items['id'].'"/>';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" class="inputs code form-control readonly" name="po_datas['.$data['i'].'][code]" value="'.$items['code'].'" id="code_'.$data['i'].'" />';
							$html .= '<input style="padding: 0px 1px;" type="hidden" class="inputs parent form-control readonly" name="po_datas['.$data['i'].'][parent]" value="'.$items['parent'].'" id="parent_'.$data['i'].'" />';
							$html .= '<input style="padding: 0px 1px;" type="hidden" class="inputs parent_id form-control readonly" name="po_datas['.$data['i'].'][parent_id]" value="'.$items['parent_id'].'" id="parent_id_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" class="inputs names form-control readonly" name="po_datas['.$data['i'].'][name]" value="'.$items['name'].'" id="name_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						if($display_type == '1'){
							$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control" id="qty_'.$data['i'].'" />';
						}
						else{
							$html .= '<input style="padding: 0px 1px;" onclick="select_qty('.$data['i'].')" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control screenquantity" id="qty_'.$data['i'].'" />';
						}
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						if($display_type == '1'){
							$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control readonly" id="rate_'.$data['i'].'" />';
						}
						else{
							$html .= '<input style="padding: 0px 1px;" onclick="select_rate('.$data['i'].')" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control readonly" id="rate_'.$data['i'].'" />';
						}
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;background-color: #f2f2f2;" type="text" readonly="readonly" name="po_datas['.$data['i'].'][amt]" value="'.$items['amt'].'" class="inputs form-control" id="amt_'.$data['i'].'" />';
						$html .= '</td>';		
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" name="po_datas['.$data['i'].'][message]" value="'.$items['message'].'" class="inputs lst form-control readonly" id="message_'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][is_liq]" value="'.$items['is_liq'].'" class="inputs lst" id="is_liq_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" name="po_datas['.$data['i'].'][reason]" value="'.$items['reason'].'" class="inputs reason form-control" id="reason_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;display: none;">';
							$html .= '-';
						$html .= '</td>';
					$html .= '</tr>';
					$data['i'] ++;
				}
				$html .= '<input type="hidden" name="orderid" value="'.$order.'"  id="orderid" />';
				$html .= '<input type="hidden" name="order_no" value="'.$order_no.'"  id="orderno" />';
				$data['html'] = $html;
				$data['status'] = 1;
			} else {
				$data['status'] = 0;
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function tableinfotransfer() {
		$this->log->write("---------------------------table info for kot transfer start-------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());

		$tid = $this->request->get['tid'];
		$lid = $this->request->get['lid'];
		$tab_index = $this->request->get['tab_index'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$display_type = type;

		$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND cancel_status <> '1' ORDER BY order_id DESC LIMIT 1 ");
		$data['html'] = '';
		$data['status'] = 0;

		$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$tid."' ";
		$running_datas = $this->db->query($running_sql);
		if($running_datas->num_rows > 0){
			$data['status'] = 2;

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
			
			
		}

		if($tableinfo->num_rows>0) {
			if($tableinfo->row['bill_status'] == 0){
				$order =	$tableinfo->row['order_id'];
				$order_no =	$tableinfo->row['order_no'];
				$data['order'] = $order;
				$tableitems = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
				$data['i']=1;
				foreach ($tableinfo->row as $tkey => $tinfo) {
					$data[$tkey] = $tinfo;
				}
				$html = '<tr>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Code</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">Name</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>';
					$html .= '<td style="width: 10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>';
					$html .= '<td style="width: 20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>';
					$html .= '<td style="padding-top: 2px;padding-bottom: 2px;display:none;">Delete</td>';
				$html .= '</tr>';
				foreach ($tableitems as $lkey => $items) {
					$html .= '<tr>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= ''.$data['i'].'<input type="hidden" name="po_datas_transfer['.$data['i'].'][nc_kot_status]" value="'.$items['nc_kot_status'].'" id="nc_kot_status'.$data['i'].'" /><input type="hidden" name="po_datas_transfer['.$data['i'].'][kot_status]" value="'.$items['kot_status'].'" id="to_kot_status'.$data['i'].'" /><input type="hidden" name="po_datas_transfer['.$data['i'].'][pre_qty]" value="'.$items['qty'].'" id="to_pre_qty'.$data['i'].'" />';
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" class="inputs code form-control readonly" name="po_datas_transfer['.$data['i'].'][code]" value="'.$items['code'].'" id="to_code_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" class="inputs names form-control readonly" name="po_datas_transfer['.$data['i'].'][name]" value="'.$items['name'].'" id="to_name_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						if($display_type == '1'){
							$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas_transfer['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control readonly" id="to_qty_'.$data['i'].'" />';
						}
						else{
							$html .= '<input style="padding: 0px 1px;" onclick="select_qty('.$data['i'].')" name="po_datas_transfer['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control screenquantity readonly" id="to_qty_'.$data['i'].'" />';
						}
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
						if($display_type == '1'){
							$html .= '<input style="padding: 0px 1px;" type="number" name="po_datas_transfer['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control readonly" id="to_rate_'.$data['i'].'" />';
						}
						else{
							$html .= '<input style="padding: 0px 1px;" onclick="select_rate('.$data['i'].')" name="po_datas_transfer['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control readonly" id="to_rate_'.$data['i'].'" />';
						}
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;background-color: #f2f2f2;" type="text" readonly="readonly" name="po_datas_transfer['.$data['i'].'][amt]" value="'.$items['amt'].'" class="inputs form-control" id="to_amt_'.$data['i'].'" />';
						$html .= '</td>';		
						$html .= '<td style="padding-top: 2px;padding-bottom: 2px;">';
							$html .= '<input style="padding: 0px 1px;" type="text" name="po_datas_transfer['.$data['i'].'][message]" value="'.$items['message'].'" class="inputs lst form-control readonly" id="message_'.$data['i'].'" /><input type="hidden" name="po_datas_transfer['.$data['i'].'][is_liq]" value="'.$items['is_liq'].'" class="inputs lst" id="to_is_liq_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
						$html .= '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;display: none;">';
							$html .= '-';
						$html .= '</td>';
					$html .= '</tr>';
					$data['i'] ++;
				}
				$html .= '<input type="hidden" name="orderidtransfer" value="'.$order.'"  id="orderidtransfer" />';
				$html .= '<input type="hidden" name="order_no" value="'.$order_no.'"  id="orderno" />';
				$data['html'] = $html;
				$data['status'] = 1;
			} else {
				$data['status'] = 0;
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
		$this->log->write("---------------------------table info for kot transfer end-----------------------------");

	}
	
	public function findtable() {
		$this->log->write("---------------------------Find table For kot transfer start---------------------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());
		$this->log->write('find Table From : ' .$this->request->get['filter_tname']);
		

			$json = array();
		if (isset($this->request->get['filter_tname'])) {
			$this->load->model('catalog/order');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_tname'],
			);
			//$results = $this->model_catalog_order->gettables($filter_data);
			$results = $this->model_catalog_order->gettables_data($filter_data);
			if(isset($results['location_id'])){
				// $location_datas = $this->db->query("SELECT `rate_id` FROM `oc_location` WHERE `location_id` = '".$results['location_id']."' ");
				// $rate_id = 1;
				// if($location_datas->num_rows > 0){
				// 	$location_data = $location_datas->row;
				// 	$rate_id = $location_data['rate_id'];
				// }
				// $json['table_id'] = $results['table_id'];
				$json['table_id'] = $filter_data['filter_name'];
				$json['loc_id'] = $results['location_id'];
				$json['rate_id'] = $results['rate_id'];
				$json['loc_name'] = strip_tags(html_entity_decode($results['location'], ENT_QUOTES, 'UTF-8'));
				$json['name'] = strip_tags(html_entity_decode($filter_data['filter_name'], ENT_QUOTES, 'UTF-8'));
				$json['status'] = 1;
			} else {
				$json['status'] = 0;
			}
		}
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		// array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		$this->log->write("-------------------Find table For kot transfer End-----------------------");

	}

	public function orderdatas() {
			$json = array();
		if (isset($this->request->get['tbl'])) {
			$this->load->model('catalog/order');

			$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
			$last_open_dates = $this->db->query($last_open_date_sql);
			if($last_open_dates->num_rows > 0){
				$last_open_date = $last_open_dates->row['bill_date'];
			} else {
				$last_open_date = date('Y-m-d');
			}
		 	$order_info_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '1'  AND payment_status = '0' AND `table_id` = '".$this->request->get['tbl']."' ORDER BY `order_id` ");
			$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$this->request->get['tbl']."' ";
			$running_datas = $this->db->query($running_sql);
		 	if(($order_info_datas->num_rows > 0) || $running_datas->num_rows > 0){
				$json['status'] = 1;
			} else {
				$json['status'] = 0;
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}