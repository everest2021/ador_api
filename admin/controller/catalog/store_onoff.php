<?php

require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStoreonoff extends Controller {

	public function index() {
		$this->load->model('catalog/settlement');
		$this->load->model('catalog/werapending_order');
		$this->getList();
	}

	public function store_status(){
		sleep(10);

		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');



		// if(isset($this->request->post['reason'])){

			if(isset($this->request->post['store'])){
				$store = $this->request->post['store'];
			} else{
				$store = array();
			}


			if(!empty($store)){
				if(isset($this->request->post['store_status'])){
					$store_status = $this->request->post['store_status'];
				} else{
					$store_status = 'disable';
				}

				
				$stor = array();
				$store_insert = '';
				if($store){
					
					foreach ($store as $key => $value) {
						if($value == 'all'){
							$stor[] = array("zomato","swiggy","foodpanda","amazon","ubereats");
						} else {
							$stor[] .= $value;
						}
						$store_insert .= $value.',';
					}
				}

				//$newrow = rtrim($stor,',');
				//$store_of = '["'.$newrow.'"]';

				$store_inserts = rtrim($store_insert,',');

				$store_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
				if($store_sql->num_rows > 0){
					$store_name = $store_sql->row['ref_id'];
				} else {
					$store_name = 'ador12';
				}

				$body_data = array(
					"location_ref_id" => $store_name,
				    "platforms" => $stor,
				    "action" => $store_status
				 );

				

				$body = json_encode($body_data);

				//$body = json_encode($body_data, JSON_PRETTY_PRINT);echo "<pre>";print_r($body);exit;

				//echo "<pre>";print_r($body);exit;
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt_array($curl, array(
				  CURLOPT_URL => PLATFORMON_OFF,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'POST',
				  CURLOPT_POSTFIELDS => $body,
				  CURLOPT_HTTPHEADER => array(
				   	'Authorization:'.URBANPIPER_API_KEY,
				    'Content-Type: application/json',
				  ),
				));

				$response = curl_exec($curl);
				$datas = json_decode($response,true);
				curl_close($curl);
				//echo "<pre>";print_r($datas);exit;
				$error = 0;
				if($datas['status'] == 'success'){
					//echo "<pre>";print_r($json_data);exit;
					$json['success'] = 'platforms Updated Succesfully...!';
					$this->db->query("DELETE FROM `oc_pipedream_store` WHERE 1 = 1");
					$this->db->query("INSERT INTO `oc_pipedream_store` SET 
								`location_ref_id` = '".$store_name."',
								`platforms` = '".$this->db->escape($store_inserts)."',
								`action` = '".$this->db->escape($store_status)."'
					");
					$error = 0;
				} elseif($datas['status'] == 'error') {
					$json['errors'] = $datas['message'];
					$error = 1;
				}else if (isset($error_msg)) {
					$json['errors'] = $error_msg;
					$error = 1;
				} else {
					$json['errors'] = $response;
					$error = 1;
				}
				$error_plus = 0;
				$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Store_on_off' ORDER BY id desc Limit 1");
				if ($check_error->num_rows > 0) {
					if($error == 1){
						//if($check_error->row['failure']){
							$error_plus = $check_error->row['failure'] + 1;
						//}
					}
					$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '0', 
							 `date`= NOW(),
							`failure` = '".$this->db->escape($error_plus)."'  
							WHERE `event` = 'Store_on_off' ");
				} else {
					$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '0', 
							 `date`= NOW(),
							`failure` = '".$this->db->escape($error)."'  ,
							`event` = 'Store_on_off'
						");
				}
			} else {
				$json['errors'] = 'Please Select Atlease One Store';
			}

			
			//echo $response;
			
		//}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$this->response->setOutput(json_encode($json));
	}


	

	public function getlist(){
		$url = '';
		
		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		} else{
			$data['order_id'] = '';
		}
		$data['final_datas'] = array();
		$final_data = array();
		
		//echo $status['status'];exit;
	/*	$stores = array(
			'0' => 'All',
			'1' => 'Zomato',
			'2' => 'Swiggy',
			'3' => 'Uber',

		);*/
		$item_info = $this->model_catalog_werapending_order->getstores();


		$stores = array(
			//'all' => 'All',
			'zomato' => 'Zomato',
			'swiggy' => 'Swiggy',
			'foodpanda' => 'Foodpanda',
			'amazon' => 'Amazon',
			'ubereats' => 'Ubereats',
			

		);

		if (!empty($item_info)) {
			$stores_post = explode(',',$item_info['platforms']);;
			foreach ($stores_post as $key => $value) {
				$data['stores_post'][]= ucwords($value);
			}
		} else {
			$data['stores_post'] = array();
		}

		//echo "<pre>";print_r($data['stores_post']);exit;



		if (!empty($item_info)) {
			$data['store_status'] =$item_info['action'];
		} else {
			$data['store_status'] = '';
		}
		
		$data['stores'] = $stores;

		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['final_datas'] = $final_data;

		$data['action_comp'] = $this->url->link('catalog/complimentary/complimentary_direct', 'token=' . $this->session->data['token'] . $url, true);

		$data['action'] = $this->url->link('catalog/settlement/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/store_onoff', $data));
	}

	
}