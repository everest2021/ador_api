<?php
class ControllerCatalogcurrentsale extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/currentsale');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/currentsale');

		// $cancelamountkot = 0;
		// $cancelqtykot = 0;
		// $cancelamountbot = 0;
		// $cancelqtytbot = 0;
		// $cancelkotbot = array();
		// $totalcancelkotbot = array();
		$bill_dates =  $this->db->query("SELECT bill_date FROM oc_order_info WHERE day_close_status = '0' order by bill_date desc LIMIT 1");
		if($bill_dates->num_rows > 0){
			$bill_datee = $bill_dates->row['bill_date'];
			$bill_date = date('Y-m-d', strtotime($bill_datee));
		} else {
			$bill_date = date('Y-m-d');
		}

		$data['billdatas'] = $this->model_catalog_currentsale->getInfo($bill_date);
		$data['getTotalAmount'] = $this->model_catalog_currentsale->getTotalAmount($bill_date);
		$data['tableinfos'] = $this->model_catalog_currentsale->getTableInfo($bill_date);
		$data['currentamounts'] = $this->model_catalog_currentsale->currentAmount($bill_date);
		$data['lastbill'] = $this->model_catalog_currentsale->lastBill($bill_date);
		$data['cancelamount'] = $this->model_catalog_currentsale->cancelbillitem($bill_date);

		$data['fooddata'] = $this->db->query("SELECT SUM(qty) as cancelqty_food, SUM(amt) as cancelamt_food FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '1' AND is_liq = '0' AND oit.`ismodifier` = '1' AND oit.`date` = '".$bill_date."'")->row;

		$data['liqdata'] = $this->db->query("SELECT SUM(qty) as cancelqty_liq, SUM(amt) as cancelamt_liq FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '1' AND is_liq = '1' AND oit.`ismodifier` = '1' AND oi.`date` = '".$bill_date."'")->row;

		if($this->model_catalog_order->get_settings('SETTLEMENT') == '1'){
			$data['unsettlebill'] = $this->model_catalog_currentsale->unsettleAmount($bill_date);
		}else{
			$data['unsettlebill'] = 0;
		}
		$data['settleamts'] = $this->model_catalog_currentsale->settleAmount($bill_date);
		$data['cancelbillitem'] = $this->model_catalog_currentsale->cancelbillitem($bill_date);

		// $cancelkotbot = $this->model_catalog_currentsale->cancelkotbot();
		// foreach ($cancelkotbot as $key) {
		// 	if($key['cancelstatus'] == '1' AND $key['is_liq'] == '0'){
		// 		$cancelamountkot = $cancelamountkot + $key['amt'];
		// 		$cancelqtytkot = $cancelqtykot + $key['qty'];
		// 	}

		// 	if($key['cancelstatus'] == '1' AND $key['is_liq'] == '1'){
		// 		$cancelamountbot = $cancelamountbot + $key['amt'];
		// 		$cancelqtytbot = $cancelqtybot + $key['qty'];
		// 	}
			
		// 	$totalcancelkotbot = array(
		// 							'cancelamountkot' => $cancelamountkot,
		// 							'cancelamountbot' => $cancelamountbot,
		// 							'cancelqtykot' => $cancelqtykot,
		// 							'cancelqtybot' => $cancelqtybot
		// 						);
		// }

		// $data['cancelkotbot'] = $totalcancelkotbot;

		$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$bill_date."'");
		if($advance->num_rows > 0){
			$data['advancetotal'] = $advance->row['advancetotal'];
		} else{
			$data['advancetotal'] = 0;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/currentsale_form', $data));
	}
}