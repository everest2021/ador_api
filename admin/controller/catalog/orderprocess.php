<?php
class ControllerCatalogOrderProcess extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/orderprocess');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/orderprocess');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/orderprocess');
		$this->document->setTitle($this->language->get('heading_title'));
		// echo '<pre>';
		// print_r($this->request->server);
		// exit;
		if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->get);
			// exit;
			date_default_timezone_set("Asia/Kolkata");
			$order_in_process_date = date('Y-m-d');
			$order_in_process_time = date('H:i:s');
			$order_no = $this->request->get['filter_order_no'];
			$sql_insert = "INSERT INTO `oc_orderprocess` SET `order_no` = '".$order_no."', `order_in_process_date` = '".$order_in_process_date."', `order_in_process_time` = '".$order_in_process_time."', `status` = '1' ";
			$this->db->query($sql_insert);
			$this->session->data['success'] = 'Order Added Successfully';

			$url = '';

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function update_status() {
		$this->load->language('catalog/orderprocess');
		$this->document->setTitle($this->language->get('heading_title'));
		$id = $this->request->get['id'];
		$status = $this->request->get['status'];
		date_default_timezone_set("Asia/Kolkata");
		if($status == 2){
			$order_ready_date = date('Y-m-d');
			$order_ready_time = date('H:i:s');
			$sql_update = "UPDATE `oc_orderprocess` SET `status` = '".$status."', `order_ready_date` = '".$order_ready_date."', `order_ready_time` = '".$order_ready_time."' WHERE `id` = '".$id."' ";
			$this->db->query($sql_update);
			$this->session->data['success'] = 'Order Status Updated to Ready Successfully';
		} elseif($status == 3) {
			$order_taken_date = date('Y-m-d');
			$order_taken_time = date('H:i:s');
			$sql_update = "UPDATE `oc_orderprocess` SET `status` = '".$status."', `order_taken_date` = '".$order_taken_date."', `order_taken_time` = '".$order_taken_time."' WHERE `id` = '".$id."' ";
			$this->db->query($sql_update);
			$this->session->data['success'] = 'Order Status Updated to Taken Successfully';
		} elseif($status == 4) {
			$sql_update = "DELETE FROM `oc_orderprocess` WHERE `id` = '".$id."' ";
			$this->db->query($sql_update);
			$this->session->data['success'] = 'Order Taken Successfully';
		}
		$url = '';

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . $url, true));
	}

	protected function getList() {
		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = null;
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = null;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->error['filter_order_no'])) {
			$data['error_filter_order_no'] = $this->error['filter_order_no'];
		} else {
			$data['error_filter_order_no'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . $this->request->get['filter_order_no'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/orderprocess/add', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_order_no' => $filter_order_no,
			'sort' => $sort,
			'order' => $order,
		);
		date_default_timezone_set("Asia/Kolkata");
		$sport_total = 0;
		//$results = $this->model_catalog_orderprocess->getOrderprocesss($filter_data);
		
		if($sort == 1){
			if($order == 'ASC'){
				$inprocess_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '1' ORDER BY `id` ASC ");	
			} else {
				$inprocess_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '1' ORDER BY `id` DESC ");	
			}
		} else {
			$inprocess_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '1' ORDER BY `id` DESC ");
		}
		
		$inprocess_datas = array();
		if($inprocess_orders->num_rows > 0){
			foreach($inprocess_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=2' . $url, true);
				$current_date_time = date('Y-m-d H:i:s');
				$compare_date_time = $ivalue['order_in_process_date'].' '.$ivalue['order_in_process_time'];
				$start_date = new DateTime($compare_date_time);
				$since_start = $start_date->diff(new DateTime($current_date_time));
				// echo '<pre>';
				// print_r($since_start);
				// exit;
				if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 5){
					$color = 'orange';
					$class = '';
				} elseif($since_start->d == 0 && $since_start->h == 0 && $since_start->i >= 5 && $since_start->i < 10){
					$color = 'red';
					$class = '';
				} else {
					$color = 'red';
					$class = 'blink_me';
				}
				$inprocess_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'order_date' => $ivalue['order_in_process_date'],
					'order_time' => $ivalue['order_in_process_time'],
					'order_date_time' => $ivalue['order_in_process_date'].' '.$ivalue['order_in_process_time'],
					'color' => $color,
					'href' => $href,
				);
			}
		}
		// echo '<pre>';
		// print_r($inprocess_datas);
		// exit;
		$data['inprocess_datas'] = $inprocess_datas;

		if($sort == 2){
			if($order == 'ASC'){
				$ready_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '2' ORDER BY `id` ASC ");
			} else {
				$ready_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '2' ORDER BY `id` DESC ");
			}
		} else {
			$ready_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '2' ORDER BY `id` DESC ");
		}
		
		$ready_datas = array();
		if($ready_orders->num_rows > 0){
			foreach($ready_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=3' . $url, true);
				$current_date_time = date('Y-m-d H:i:s');
				$compare_date_time = $ivalue['order_ready_date'].' '.$ivalue['order_ready_time'];
				$start_date = new DateTime($compare_date_time);
				$since_start = $start_date->diff(new DateTime($current_date_time));
				// echo '<pre>';
				// print_r($since_start);
				// exit;
				if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 5){
					$color = 'orange';
					$class = '';
				} elseif($since_start->d == 0 && $since_start->h == 0 && $since_start->i >= 5 && $since_start->i < 10){
					$color = 'red';
					$class = '';
				} else {
					$color = 'red';
					$class = 'blink_me';
				}
				$ready_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'order_date' => $ivalue['order_ready_date'],
					'order_time' => $ivalue['order_ready_time'],
					'order_date_time' => $ivalue['order_ready_date'].' '.$ivalue['order_ready_time'],
					'href' => $href,
					'color' => $color,
				);
			}
		}
		$data['ready_datas'] = $ready_datas;

		if($sort == 3){
			if($order == 'ASC'){
				$taken_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '3' ORDER BY `id` ASC ");
			} else {
				$taken_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '3' ORDER BY `id` DESC ");
			}
		} else {
			$taken_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '3' ORDER BY `id` DESC ");
		}
		
		$taken_datas = array();
		if($taken_orders->num_rows > 0){
			foreach($taken_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=4' . $url, true);
				$current_date_time = date('Y-m-d H:i:s');
				$compare_date_time = $ivalue['order_taken_date'].' '.$ivalue['order_taken_time'];
				$start_date = new DateTime($compare_date_time);
				$since_start = $start_date->diff(new DateTime($current_date_time));
				// echo '<pre>';
				// print_r($since_start);
				// exit;
				if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 5){
					$color = 'orange';
					$class = '';
				} elseif($since_start->d == 0 && $since_start->h == 0 && $since_start->i >= 5 && $since_start->i < 10){
					$color = 'red';
					$class = '';
				} else {
					$color = 'red';
					$class = 'blink_me';
				}
				$taken_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'order_date' => $ivalue['order_taken_date'],
					'order_time' => $ivalue['order_taken_time'],
					'order_date_time' => $ivalue['order_taken_date'].' '.$ivalue['order_taken_time'],
					'href' => $href,
					'color' => $color,
				);
			}
		}
		$data['taken_datas'] = $taken_datas;


		$data['token'] = $this->session->data['token'];
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . $this->request->get['filter_order_no'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        
        $data['sort_in_process'] = $this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . '&sort=1' . $url, true);
        $data['sort_ready'] = $this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . '&sort=2' . $url, true);
        $data['sort_taken'] = $this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . '&sort=3' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));
		$data['filter_order_no'] = $filter_order_no;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/orderprocess_list', $data));
	}

	public function getForm() {
		$this->load->language('catalog/orderprocess');
		$this->document->setTitle($this->language->get('heading_title_1'));
		$this->load->model('catalog/orderprocess');

		$data['heading_title'] = $this->language->get('heading_title_1');

		
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['token'] = $this->session->data['token'];

		$inprocess_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '1' ORDER BY `id` DESC ");
		$inprocess_datas = array();
		if($inprocess_orders->num_rows > 0){
			foreach($inprocess_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=2', true);
				$inprocess_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'href' => $href,
				);
			}
		}
		$data['inprocess_datas'] = $inprocess_datas;

		$ready_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '2' ORDER BY `id` DESC ");
		$ready_datas = array();
		if($ready_orders->num_rows > 0){
			foreach($ready_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=3', true);
				$ready_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'href' => $href,
				);
			}
		}
		$data['ready_datas'] = $ready_datas;

		$refresh_url = $this->url->link('catalog/orderprocess/getForm', 'token=' . $this->session->data['token'], true);
		$refresh_url = str_replace('&amp;', '&', $refresh_url);
		$refresh_url = str_replace('&amp;', '&', $refresh_url);
		$data['refresh_url'] = $refresh_url;


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/orderprocess_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/orderprocess')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$datas = $this->request->get;
		if(isset($this->request->get['filter_order_no'])){
			$is_exist = $this->db->query("SELECT `id` FROM `oc_orderprocess` WHERE `order_no` = '".$datas['filter_order_no']."' AND `status` = '1' ");
			if($is_exist->num_rows > 0){
				$this->error['filter_order_no'] = 'Order Number Already Exists';
			}
		}
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/orderprocess')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_c_name'])) {
			$this->load->model('catalog/orderprocess');

			$filter_data = array(
				'filter_c_name' => $this->request->get['filter_c_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_orderprocess->getOrderprocesss($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'orderprocess_id' => $result['orderprocess_id'],
					'c_name'        => strip_tags(html_entity_decode($result['c_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['c_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}