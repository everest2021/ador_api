<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogExpenseReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/expensereport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/expensereport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/expensereport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y'); 
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_type'])){
			$data['filter_type'] = $this->request->post['filter_type'];
		}
		else{
			$data['filter_type'] = '';
		}

		if(isset($this->request->post['filter_paidtype'])){
			$data['filter_paidtype'] = $this->request->post['filter_paidtype'];
		}
		else{
			$data['filter_paidtype'] = '';
		}
		$expenseAllDatas = array();
		$opening_bal =0;
		$closing_bal =0;
		$total_paid_in_amt =0;
		$total_paid_out_amt =0;
		$net_total=0;
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);

			$start_date_open_bal= date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $start_date) ) ));
			//echo $start_date_open_bal;exit;
			$opening_bals="SELECT * FROM oc_day_close_expense WHERE `date` ='".$start_date_open_bal."' LIMIT 1";
			$opening_balss = $this->db->query($opening_bals);
			if($opening_balss->num_rows > 0){
				$opening_bal =$opening_balss->row['amount'];
			}else{
				$opening_bal=0;
			}
			//echo $opening_bal;exit;
			$sql = "SELECT * FROM oc_expense_trans WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."'";
			if($this->request->post['filter_type'] != ''){
				$sql .= " AND `type` = '".$this->request->post['filter_type']."'";
			}
			if($this->request->post['filter_paidtype'] != ''){
				$sql .= " AND `payment_type` = '".$this->request->post['filter_paidtype']."'";
			}
			$expensedatas = $this->db->query($sql)->rows;
			/*echo '<pre>';print_r($expensedatas);exit();*/
			foreach($expensedatas as $expensedata){
				if($expensedata['payment_type']=='PAID IN'){
					$paid_in_amt=$expensedata['amount'];
				}else{
					$paid_in_amt=0;
				}
				if($expensedata['payment_type']=='PAID OUT'){
					$paid_out_amt=$expensedata['amount'];
				}else{
					$paid_out_amt=0;
				}
				$total_paid_in_amt=$total_paid_in_amt+$paid_in_amt;
				$total_paid_out_amt=$total_paid_out_amt+$paid_out_amt;
				$net_total=$opening_bal+$total_paid_in_amt;
				$closing_bal=$net_total-$total_paid_out_amt;
				$expenseAllDatas[]=array(
					'paid_in_amt' =>$paid_in_amt,
					'paid_out_amt' =>$paid_out_amt,
					'date' =>$expensedata['date'],
					'exp_no' =>$expensedata['exp_no'],
					'account_name' =>$expensedata['account_name'],
					'remarks' =>$expensedata['remarks'],
					'total_paid_in' =>$total_paid_in_amt,
					'total_paid_out' =>$total_paid_out_amt,
					'net_total' =>$net_total,
					'closing_total' =>$closing_bal,
				);
			}
		}

		$data['expensedatas'] = $expenseAllDatas;
		/*echo '<pre>';
		print_r($expenseAllDatas);
		exit();*/
		$type=array(
			'Supplier' => 'SUPPLIER',
			'Employee' => 'EMPLOYEE',
			'Other' => 'OTHER',
			);
		$paidtype=array(
			'PAID IN' => 'PAID IN',
			'PAID OUT' => 'PAID OUT',
			);
		$data['type'] =$type ;
		$data['opening_bal'] =$opening_bal ;
		//echo $data['opening_bal'];exit;
		$data['paidtype'] =$paidtype ;
		$data['action'] = $this->url->link('catalog/expensereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/expensereport', $data));
	}




	public function prints(){

		$this->load->language('catalog/expensereport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/expensereport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$startdate = $this->request->get['filter_startdate'];
		}
		else{
			$startdate = date('m/d/Y'); 
		}

		if(isset($this->request->get['filter_enddate'])){
			$enddate = $this->request->get['filter_enddate'];
		}
		else{
			$enddate = date('m/d/Y');
		}

		if(isset($this->request->get['filter_type'])){
			$filter_type = $this->request->get['filter_type'];
		}
		else{
			$filter_type = '';
		}

		if(isset($this->request->get['filter_paidtype'])){
			$filter_paidtype = $this->request->get['filter_paidtype'];
		}
		else{
			$filter_paidtype = '';
		}
		$expenseAllDatas = array();
		$opening_bal =0;
		$closing_bal =0;
		$total_paid_in_amt =0;
		$total_paid_out_amt =0;
		$net_total=0;
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);

			$start_date_open_bal= date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $start_date) ) ));
			//echo $start_date_open_bal;exit;
			$opening_bals="SELECT * FROM oc_day_close_expense WHERE `date` ='".$start_date_open_bal."' LIMIT 1";
			$opening_balss = $this->db->query($opening_bals);
			if($opening_balss->num_rows > 0){
				$opening_bal =$opening_balss->row['amount'];
			}else{
				$opening_bal=0;
			}
			//echo $opening_bal;exit;
			$sql = "SELECT * FROM oc_expense_trans WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."'";
			if($filter_type != ''){
				$sql .= " AND `type` = '".$filter_type."'";
			}
			if($filter_paidtype != ''){
				$sql .= " AND `payment_type` = '".$filter_paidtype."'";
			}
			$expensedatas = $this->db->query($sql)->rows;
			/*echo '<pre>';print_r($expensedatas);exit();*/
			foreach($expensedatas as $expensedata){
				if($expensedata['payment_type']=='PAID IN'){
					$paid_in_amt=$expensedata['amount'];
				}else{
					$paid_in_amt=0;
				}
				if($expensedata['payment_type']=='PAID OUT'){
					$paid_out_amt=$expensedata['amount'];
				}else{
					$paid_out_amt=0;
				}
				$total_paid_in_amt=$total_paid_in_amt+$paid_in_amt;
				$total_paid_out_amt=$total_paid_out_amt+$paid_out_amt;
				$net_total=$opening_bal+$total_paid_in_amt;
				$closing_bal=$net_total-$total_paid_out_amt;
				$expenseAllDatas[]=array(
					'paid_in_amt' =>$paid_in_amt,
					'paid_out_amt' =>$paid_out_amt,
					'date' =>$expensedata['date'],
					'exp_no' =>$expensedata['exp_no'],
					'account_name' =>$expensedata['account_name'],
					'remarks' =>$expensedata['remarks'],
					'total_paid_in' =>$total_paid_in_amt,
					'total_paid_out' =>$total_paid_out_amt,
					'net_total' =>$net_total,
					'closing_total' =>$closing_bal,
				);
			}

		$data['expensedatas'] = $expenseAllDatas;


		/************************************** Second last part End **************************************************/
			$this->load->model('catalog/order');
			try {
			   	if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
			 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
			 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
			 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
			 	} else {
			 		$connector = '';
			 	}
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Expens Report");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$start_date,30)."To :".$end_date);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("Date",11)."".str_pad("Exp.no",7)."".str_pad("Account",11)."".str_pad("Paid In",8)."".str_pad("Paid Out",8));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$usertotal = 0;
			  	
			  	
			  	foreach($expenseAllDatas as $expensedata) { 
					// $printer->text(str_pad($value['name'],30).$value['amount']);
					// $printer->feed(1);
				
				$printer->text(str_pad($expensedata['date'],11)."".str_pad($expensedata['exp_no'],7)."".str_pad($expensedata['account_name'],12)."".str_pad($expensedata['paid_in_amt'],8)."".str_pad($expensedata['paid_out_amt'],8));
				$printer->feed(1);
				
			  	}
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("Opening bal ",30).  $opening_bal);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Paid IN",30). $total_paid_in_amt);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Net Total ",30). $net_total);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Paid OUT ",30).$total_paid_out_amt);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Closing ",30). $closing_bal);
			  	$printer->feed(1);
			  	$printer->feed(1);
			  	$printer->feed(1);

			  	
			  	$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}