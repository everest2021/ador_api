<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
date_default_timezone_set('Asia/Kolkata');
class ControllerCatalogItemwisereport extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('catalog/itemwisereport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/itemwisereport');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_itemname'])){
			$filter_itemname = $this->request->get['filter_itemname'];
		} else {
			$filter_itemname = '';
		}

		if(isset($this->request->get['filter_itemid'])){
			$filter_itemid = $this->request->get['filter_itemid'];
		} else {
			$filter_itemid = '';
		}

		if(isset($this->request->get['store'])){
			$store_id = $this->request->get['store'];
		}
		else{
			$store_id = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}
		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}
		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_itemname' => $filter_itemname,
			'filter_itemid' => $filter_itemid,
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			'store_id'			=> $store_id
			);
		
		$all_data ='';
		$final_data = array();
		$item_data ='';
		$data['final_data'] = '';
		$stores = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
		
			$sql ="SELECT *,SUM(`quantity`) as `total_quantity` FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE 1=1 ";
			if (!empty($filter_data['filter_itemid'])) {
				$sql .= " AND (poi.`item_id`) = '" . $this->db->escape($filter_itemid) . "'";
			}
			if (!empty($filter_data['filter_startdate'])) {
				$sql .= " AND DATE(po.`po_date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_startdate))) . "'";
			}
			if (!empty($filter_data['filter_enddate'])) {
				$sql .= " AND DATE(po.`po_date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_enddate))) . "'";
			}
				
			$sql .= " GROUP BY poi.`item_id`,`notes`";
			$all_data = $this->db->query($sql)->rows;
			$final_data = array();
			$i =1;
			foreach ($all_data as $akey => $avalue) {
				$href = $this->url->link('catalog/itemwisereport/recipe', 'token=' . $this->session->data['token'].'&filter_quantity='. $avalue['total_quantity'] . '&filter_item_id=' . $avalue['item_id'] . $url, true);
				$stores = $this->db->query("SELECT * FROM oc_outlet")->rows;
				$storewise = array();
				foreach($stores as $store){
					$storedatas = $this->db->query("SELECT * FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE outlet_id = '".$store['outlet_id']."' AND item_id = '".$avalue['item_id']."' AND notes = '".$avalue['notes']."'");
					if($storedatas->num_rows > 0){
						foreach($storedatas->rows as $storedata){
							$storewise[] = array(
								'quantity' => $storedata['quantity']
							);
						}
					} else{
						$storewise[] = array(
							'quantity' => 0
						);
					}
				}
	
				$final_data[]= array(
					'i' => $i,
					'item_name'	=> $avalue['item_name'],
					'storewise'	=> $storewise,
					'quantity' => $avalue['total_quantity'],
					'notes' => $avalue['notes'],
					'href' => $href,
				);
				$i ++ ;
			}
		}
		$data['final_data'] = $final_data;
		// echo "<pre>";
		// print_r($final_data);
		// exit();
		$data['stores'] = $stores;
		$data['storecount'] = $this->db->query("SELECT COUNT(*) as total FROM oc_outlet")->row;
		$data['filter_itemname'] = $filter_itemname;
		$data['filter_itemid'] = $filter_itemid;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		$data['store_id'] = $store_id;
		$data['recipestores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
		
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/itemwisereport', $data));
	}
	public function name(){
		$json = array();

		if (isset($this->request->get['filter_itemname'])) {
			$sql = "SELECT * FROM `oc_purchase_order_items` WHERE 1=1 ";
			if(!empty($this->request->get['filter_itemname'])){
				$sql .= " AND `item_name` LIKE '%".$this->request->get['filter_itemname']."%'";
			}
			$sql .=" GROUP BY`item_id` DESC LIMIT 0,10";
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints(){
		$this->load->model('catalog/order');
		if(isset($this->request->get['filter_item_id'])){
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = '';
		}

		if(isset($this->request->get['filter_quantity'])){
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = '';
		}

		if(isset($this->request->get['store'])){
			$store = $this->request->get['store'];
		} else {
			$store = '';
		}

		$bomdata = array();

		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}

		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}

		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		$data['cancel'] = $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['print'] = $this->url->link('catalog/itemwisereport/prints', 'token=' . $this->session->data['token'] . $url, true);
		
		$itemDetails = $this->db->query("SELECT `item_name`,`item_id`, `item_code`,`rate_1` FROM oc_item WHERE item_id = '".$filter_item_id."'")->row;

		$bomdetails = $this->db->query("SELECT qty, item_name, unit, id, item_code, unit_id, store_id FROM oc_bom_items WHERE parent_item_code = '".$itemDetails['item_code']."' AND store_id = '".$store."'")->rows;

		$notes_data = $this->db->query("SELECT `notes` FROM `oc_purchase_order_items` WHERE item_id = '".$filter_item_id."'")->row;

		// echo'<pre>';
		// print_r($notes_data);
		// exit;
		$itemdata = array(
			'item_name' => $itemDetails['item_name'],
			'item_code' => $itemDetails['item_code'],
			'purchase_price' => $itemDetails['rate_1'],
			'description' => $notes_data['notes'],
			'quantity'	=> $filter_quantity,
		);

		foreach($bomdetails as $key){
			$stock_item_type = $this->db->query("SELECT * FROM oc_stock_item WHERE item_code = '".$key['item_code']."'")->row;
			$this->load->model('catalog/purchaseentry');
			$availableqty = $this->model_catalog_purchaseentry->availableQuantity($key['unit_id'], $key['id'], 0, $key['store_id'], $stock_item_type['item_type'], $key['item_code']);
			$bomdata[] = array(
				'item_name' => $key['item_name'],
				'qty'		=> $key['qty'] * $filter_quantity,
				'unit'		=> $key['unit'],
				'avq'		=> $availableqty
			);
		}

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Recipe");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text($itemdata['item_name']);
		   	if($itemdata['description'] != ''){
		    		$printer->feed(1);
		    		$printer->text("Notes :".$itemdata['description']);
			}
			$printer->feed(1);
			$printer->text(str_pad("Item Code",26)."".str_pad("Rate", 10)."Qty");
		   	$printer->feed(1);
		   	$printer->text(str_pad($itemdata['item_code'],26)."".str_pad($itemdata['purchase_price'], 10).$itemdata['quantity']);
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("Item",26)."".str_pad("Qty", 10)."Units");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $total_items_normal = 0;
			$total_quantity_normal = 0;
		    foreach($bomdata as $nkey => $nvalue){
	    	  	$nvalue['item_name'] = utf8_substr(html_entity_decode($nvalue['item_name'], ENT_QUOTES, 'UTF-8'), 0, 25);
				//$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0, 4);
				//$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
				//$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 4);
		    	//$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
		    	//$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
		    	//$printer->feed(1);
		    	$printer->text("".str_pad($nvalue['item_name'],26)."".str_pad($nvalue['qty'],10).$nvalue['unit']);
		    	$printer->feed(1);
		    	$total_items_normal ++ ;
		    	$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	    	}
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(2);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->recipe();
	}

	public function recipe(){

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		if(isset($this->request->get['filter_item_id'])){
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = '';
		}

		if(isset($this->request->get['filter_quantity'])){
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = '';
		}

		if(isset($this->request->get['store'])){
			$store = $this->request->get['store'];
		} else {
			$store = '';
		}

		$bomdata = array();

		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}

		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}

		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		$data['cancel'] = $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['print'] = $this->url->link('catalog/itemwisereport/prints', 'token=' . $this->session->data['token'] . $url, true);
		
		$itemDetails = $this->db->query("SELECT `item_name`,`item_id`, `item_code`,`rate_1` FROM oc_item WHERE item_id = '".$filter_item_id."'")->row;

		$bomdetails = $this->db->query("SELECT qty, item_name, unit, id, item_code, unit_id, store_id FROM oc_bom_items WHERE parent_item_code = '".$itemDetails['item_code']."' AND store_id = '".$store."'")->rows;

		$notes_data = $this->db->query("SELECT `notes` FROM `oc_purchase_order_items` WHERE item_id = '".$filter_item_id."'")->row;

		// echo'<pre>';
		// print_r($notes_data);
		// exit;
		$itemdata = array(
			'item_id' 	=> $itemDetails['item_id'],
			'item_name' => $itemDetails['item_name'],
			'item_code' => $itemDetails['item_code'],
			'purchase_price' => $itemDetails['rate_1'],
			'description' => $notes_data['notes'],
			'quantity'	=> $filter_quantity,
		);

		foreach($bomdetails as $key){
			$stock_item_type = $this->db->query("SELECT * FROM oc_stock_item WHERE item_code = '".$key['item_code']."'")->row;
			$this->load->model('catalog/purchaseentry');
			$availableqty = $this->model_catalog_purchaseentry->availableQuantity($key['unit_id'], $key['id'], 0, $key['store_id'], $stock_item_type['item_type'], $key['item_code']);
			$bomdata[] = array(
				'item_name' => $key['item_name'],
				'qty'		=> $key['qty'] * $filter_quantity,
				'unit'		=> $key['unit'],
				'avq'		=> $availableqty
			);
		}

		$data['token'] = $this->session->data['token'];
		$data['itemDetails'] = $itemdata;
		$data['bomdetails'] = $bomdata;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/itemwisedetails', $data));
	}

	public function export() {
		//echo "in";exit;
		$data['title'] = 'Item Wise Report'; 

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}
		$this->load->language('catalog/itemwisereport');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_itemname'])){
			$filter_itemname = $this->request->get['filter_itemname'];
		} else {
			$filter_itemname = '';
		}

		if(isset($this->request->get['filter_itemid'])){
			$filter_itemid = $this->request->get['filter_itemid'];
		} else {
			$filter_itemid = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}


		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}
		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_itemname' => $filter_itemname,
			'filter_itemid' => $filter_itemid,
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			);
		$all_data ='';
		$final_data = array();
		$item_data ='';
		$data['final_data'] = '';
		$stores = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
		
			$sql ="SELECT *,SUM(`quantity`) as `total_quantity` FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE 1=1 ";
			if (!empty($filter_data['filter_itemid'])) {
				$sql .= " AND (poi.`item_id`) = '" . $this->db->escape($filter_itemid) . "'";
			}
			if (!empty($filter_data['filter_startdate'])) {
				$sql .= " AND DATE(po.`po_date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($filter_startdate))) . "'";
			}
			if (!empty($filter_data['filter_enddate'])) {
				$sql .= " AND DATE(po.`po_date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($filter_enddate))) . "'";
			}
				
			$sql .= " GROUP BY poi.`item_id`,`notes`";
			$all_data = $this->db->query($sql)->rows;
			$final_data = array();
			$i =1;
			foreach ($all_data as $akey => $avalue) {
				$href = $this->url->link('catalog/itemwisereport/recipe', 'token=' . $this->session->data['token'].'&filter_quantity='. $avalue['total_quantity'] . '&filter_item_id=' . $avalue['item_id'] . $url, true);
				$stores = $this->db->query("SELECT * FROM oc_outlet")->rows;
				$storewise = array();
				foreach($stores as $store){
					$storedatas = $this->db->query("SELECT * FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE outlet_id = '".$store['outlet_id']."' AND item_id = '".$avalue['item_id']."' AND notes = '".$avalue['notes']."'");
					if($storedatas->num_rows > 0){
						foreach($storedatas->rows as $storedata){
							$storewise[] = array(
								'quantity' => $storedata['quantity']
							);
						}
					} else{
						$storewise[] = array(
							'quantity' => 0
						);
					}
				}
	
				$final_data[]= array(
					'i' => $i,
					'item_name'	=> $avalue['item_name'],
					'storewise'	=> $storewise,
					'quantity' => $avalue['total_quantity'],
					'notes' => $avalue['notes'],
					'href' => $href,
				);
				$i ++ ;
			}
		}
		$data['final_data'] = $final_data;
		// echo'<pre>';
		// print_r($data['final_data']);
		// exit;

		$data['stores'] = $stores;
		$data['storecount'] = $this->db->query("SELECT COUNT(*) as total FROM oc_outlet")->row;
		$data['filter_itemname'] = $filter_itemname;
		$data['filter_itemid'] = $filter_itemid;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		
		$html = $this->load->view('catalog/itemwisereport_html.tpl', $data); // exit;
	 	$filename = "Item Wise Report.html";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		$this->response->setOutput($this->load->view('catalog/itemwisereport', $data));
	}

}