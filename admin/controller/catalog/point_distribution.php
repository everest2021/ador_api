<?php
class ControllerCatalogPointDistribution extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/brand');

		$this->document->setTitle($this->language->get('Point Distribution'));

		$this->load->model('catalog/point_distribution');
		//$this->load->model('catalog/item');

		$this->getList();
	}
	public function add() {
		//echo '<pre>';print_r($_POST);exit;
		//$this->load->language('catalog/point_distribution');
		$this->load->model('catalog/point_distribution');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('catalog/student');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_catalog_point_distribution->addBarcode($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));

			}

			if (isset($this->request->get['filter_standard'])) {
				$url .= '&filter_standard=' . urlencode(html_entity_decode($this->request->get['filter_standard'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . urlencode(html_entity_decode($this->request->get['filter_division'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_rollno'])) {
				$url .= '&filter_rollno=' . urlencode(html_entity_decode($this->request->get['filter_rollno'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_medium'])) {
				$url .= '&filter_medium=' . urlencode(html_entity_decode($this->request->get['filter_medium'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/point_distribution', 'token=' . $this->session->data['token'] . $url, true));

		}

		$this->getForm();
	}
	public function ajax(){
		$this->load->model('catalog/point_distribution');
		//$this->load->model('catalog/item');
		$filter_data = array();
		$results = $this->model_catalog_point_distribution->getBrands($filter_data);
	}
	protected function getList() {
		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} else {
			$filter_brand = null;
		}

		if (isset($this->request->get['filter_brand_id'])) {
			$filter_brand_id = $this->request->get['filter_brand_id'];
		} else {
			$filter_brand_id = null;
		}

		if (isset($this->request->get['filter_subcategory'])) {
			$filter_subcategory = $this->request->get['filter_subcategory'];
		} else {
			$filter_subcategory = null;
		}

		if (isset($this->request->get['filter_subcategory_id'])) {
			$filter_subcategory_id = $this->request->get['filter_subcategory_id'];
		} else {
			$filter_subcategory_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Point Distribution'),
			'href' => $this->url->link('catalog/point_distribution', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array();

		//$brand_total = $this->model_catalog_brand->getTotalBrand($filter_data);
		

		$data['tables'] = array();
		$data['token'] = $this->session->data['token'];
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['brand'] = $this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . '&sort=brand' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (!isset($this->request->get['student_id'])) {
			$data['action'] = $this->url->link('catalog/point_distribution/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/point_distribution/edit', 'token=' . $this->session->data['token'] . '&student_id=' . $this->request->get['student_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('catalog/point_distribution', 'token=' . $this->session->data['token'] . $url, true);
		$pagination = new Pagination();
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		$data['filter_brand'] = $filter_brand;
		$data['filter_brand_id'] = $filter_brand_id;
		$data['filter_subcategory'] = $filter_subcategory;
		$data['filter_subcategory_id'] = $filter_subcategory_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/point_distribution', $data));
	}

	

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/brand');

		return !$this->error;
	}

	// public function upload() {
	// 	$this->load->language('catalog/brand');

	// 	$json = array();

	// 	// Check user has permission
	// 	if (!$this->user->hasPermission('modify', 'catalog/brand')) {
	// 		$json['error'] = $this->language->get('error_permission');
	// 	}

	// 	if (!$json) {
	// 		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
	// 			// Sanitize the filename
	// 			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

	// 			// Validate the filename length
	// 			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
	// 				$json['error'] = $this->language->get('error_filename');
	// 			}

	// 			// Allowed file extension types
	// 			$allowed = array();

	// 			$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

	// 			$filetypes = explode("\n", $extension_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Allowed file mime types
	// 			$allowed = array();

	// 			$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

	// 			$filetypes = explode("\n", $mime_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array($this->request->files['file']['type'], $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Check to see if any PHP files are trying to be uploaded
	// 			$content = file_get_contents($this->request->files['file']['tmp_name']);

	// 			if (preg_match('/\<\?php/i', $content)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Return any upload error
	// 			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
	// 				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
	// 			}
	// 		} else {
	// 			$json['error'] = $this->language->get('error_upload');
	// 		}
	// 	}

	// 	if (!$json) {
	// 		$file = $filename . '.' . token(32);

	// 		move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

	// 		$json['filename'] = $file;
	// 		$json['mask'] = $filename;

	// 		$json['success'] = $this->language->get('text_upload');
	// 	}

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function autocompletebname() {
		$json = array();
		if (isset($this->request->get['filter_brand'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_brand' => $this->request->get['filter_brand'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'brand'    => strip_tags(html_entity_decode($result['brand'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['brand'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletesubcategory() {
		$json = array();
		if (isset($this->request->get['filter_subcategory'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_subcategory' => $this->request->get['filter_subcategory'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'subcategory'    => strip_tags(html_entity_decode($result['subcategory'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['subcategory'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

}