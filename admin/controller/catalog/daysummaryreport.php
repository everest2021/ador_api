<?php
require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
//require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');

require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');
class ControllerCatalogdaysummaryreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_category'])){
			$filter_category = $this->request->post['filter_category'];
		} else {
			$filter_category = '99';
		}

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		$onlineorder = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			/************************************** User Wise Start *************************************************/
			$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, oi.`bill_date`, is_liq, onac FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oi.`login_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderdata) {
				$orderdataval = array();
				$orderitemval = array();
				$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows;
				$netsale = 0;
				$pay_cash = 0;
				$msr =0;
				$pay_card = 0;
				$pay_online = 0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$duplicatebill = 0;
				$duplicatecount = 0;
				$advanceamount = 0;
				$cancelkot = 0;
				$cancelamountkot = 0;
				$cancelbot = 0;
				$cancelamountbot = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$cancelledbill = 0;
				$cancelledbillcount = 0;
				$complimentarybill = 0;
				$complimentarycount = 0;
				foreach ($orderdatatest as $key) {
					//$netsale =  round($netsale  + $key['grand_total'] );

					$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
					$pay_cash = $pay_cash + $key['pay_cash'];
					$msr =$msr + $key['msrcard'];
					$pay_card = $pay_card + $key['pay_card'];
					$pay_online = $pay_online + $key['pay_online'];
					$onac = $onac + $key['onac'];
					$mealpass = $mealpass + $key['mealpass'];
					$pass = $pass + $key['pass'];

					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingbill ++;
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountbillcount ++;
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					}

					if($key['duplicate'] == '1'){
						$duplicatecount ++;
						$duplicatebill = $duplicatebill + $key['grand_total'];
					}

					$advanceamount = $advanceamount + $key['advance_amount'];
				}

				$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info_report`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY `order_id`";
				$orderdatabill = $this->db->query($sql)->rows;
				//echo "<pre>";print_r($orderdatabill);exit;
				foreach ($orderdatabill as $key => $value) {
					if($value['cancel_status'] == '1'){
						$cancelledbillcount ++;
						$cancelledbill = $cancelledbill + $value['grand_total'];
					}

					if($value['complimentary_status'] == '1'){
						$complimentarycount ++;
						$complimentarybill = $complimentarybill + $value['grand_total'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";
				$orderdatatest = $this->db->query($sql)->rows; 
				foreach ($orderdatatest as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalnc ++;
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					}
				}

				$orderdataval = array(
					'netsale' => $netsale,
					'total_cash' => $pay_cash,
					'msr' => $msr,
					'total_card' => $pay_card,
					'total_pay_online' => $pay_online,
					'onac' => $onac,
					'total_mealpass' => $mealpass,
					'total_pass' => $pass,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'discountbillcount' => $discountbillcount,
					'discountamount' => $discountamount,
					'duplicate' => $duplicatebill,
					'duplicatecount' => $duplicatecount,
					'advanceamount' => $advanceamount,
					'cancelkot' => $cancelkot,
					'cancelamountkot' => $cancelamountkot,
					'cancelbot' => $cancelbot,
					'cancelamountbot' => $cancelamountbot,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'cancelledcount' => $cancelledbillcount,
					'cancelledbill' => $cancelledbill,
					'complimentarycount' => $complimentarycount,
					'complimentarybill' => $complimentarybill,
					'login_id' => $orderdata['login_id'],
					'login_name' => $orderdata['login_name'],
				);
				$orderdataarray[$orderdata['login_id']] = array(
					'orderdata' => $orderdataval,
				);
			}
			/************************************** User Wise End *************************************************/

			/************************************** Location Wise Start *************************************************/

			$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oit.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY location";
			$orderdatalocationwises = $this->db->query($sql)->rows; 
			$totalnetsale = 0;
			$totalgst = 0;
			$totalvat = 0;
			$totalroundoff = 0;
			$totalscharge = 0;
			$totaldiscount = 0;
			$nettotalamt = 0;
			$advance = 0;
			$totalpackaging_sgst = 0;
			$totalpackaging_cgst = 0;
			$totalpackaging = 0;

			foreach($orderdatalocationwises as $orderdata){
				$sql = "SELECT *, oi.stax as order_stax FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";
				//echo $sql;exit;
				$orderdatatestloc = $this->db->query($sql)->rows; 
				$netsale = 0;
				$vat = 0;
				$gst = 0;
				$roundoff = 0;
				$scharge = 0;
				$discount = 0;
				$nettotal = 0;
				$advance = 0;
				$packaging = 0;
				$packaging_cgst = 0;
				$packaging_sgst = 0;
				foreach ($orderdatatestloc as $key) {
					if($key['is_liq'] == '1' && $filter_category == '1'){
						$netsale = $netsale + $key['amt'];
						$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxliq'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					}elseif($key['is_liq'] == '0' && $filter_category == '0'){
						$netsale = $netsale + $key['amt'];
						$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxfood'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					} else{
						$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
						$vat = $vat + $key['vat'];
						$gst = $gst + $key['gst'];
						$roundoff = $roundoff + $key['roundtotal'];
						$scharge = $scharge + $key['order_stax'];
						$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$nettotal = $nettotal + $key['grand_total'] + ($key['roundtotal']);
						$advance = $advance + $key['advance_amount'];
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

					}
				}
				$orderlocdatas[] = array(
					'location' => $key['location'],
					'netsale' => $netsale,
				);
				$totalnetsale = $totalnetsale + $netsale;
				$totalgst = $totalgst + $gst;
				$totalvat = $totalvat + $vat;
				$totalroundoff = $totalroundoff + $roundoff;
				$totalscharge = $totalscharge + $scharge;
				$totaldiscount = $totaldiscount + $discount;
				$nettotalamt = $nettotalamt + $nettotal + $advance;

				$totalpackaging = $totalpackaging + $packaging;
				$totalpackaging_cgst = $totalpackaging_cgst + $packaging_cgst;
				$totalpackaging_sgst = $totalpackaging_sgst + $packaging_sgst;
			}

			$orderlocdatasamt = array(
				'totalnetsale' => $totalnetsale,
				'totalgst' => $totalgst,
				'totalvat' => $totalvat,
				'totalroundoff' => $totalroundoff,
				'totalscharge' => $totalscharge,
				'totaldiscount' => $totaldiscount,
				'nettotalamt' => $nettotalamt,
				'advance' => $advance,
				'totalpackaging' => $totalpackaging,
				'totalpackaging_cgst' => $totalpackaging_cgst,
				'totalpackaging_sgst' => $totalpackaging_sgst 
			);

			/************************************** Location Wise End *************************************************/

			/************************************** Payment Summary Start *************************************************/
			$totalcash = 0;
			$totalcard = 0;
			$totalpayonline =0;
			$onac = 0;
			$mealpass = 0;
			$pass = 0;
			$room = 0;
			$msr = 0;
			$total = 0;
			$cardtip = 0;
			$totalpaysumm = 0;
			$onac = 0;
			$advanceamt = 0;
			$tip_amount = 0;
			$persons = 0;
			$sql = "SELECT pay_cash,msrcard ,pay_card,pay_online, mealpass, pass, advance_amount, oit.`bill_date`, is_liq, onac, tip FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oit.`order_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderpayment) {
				$totalcash = $totalcash + $orderpayment['pay_cash'];
				$totalcard = $totalcard + $orderpayment['pay_card'];
				$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
				$onac = $onac + $orderpayment['onac'];
				$mealpass = $mealpass + $orderpayment['mealpass'];
				$pass = $pass + $orderpayment['pass'];
				$advanceamt = $advance + $orderpayment['advance_amount'];
				$tip_amount = round($tip_amount) + round($orderpayment['tip']);
				$room = 0.00;
				$msr =$msr + $orderpayment['msrcard'];
				$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
				$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$orderpaymentamt = array(
				'totalcash' => $totalcash,
				'totalcard' => $totalcard,
				'totalpayonline' => $totalpayonline,
				'onac' => $onac,
				'mealpass' => $mealpass,
				'pass' => $pass,
				'room' => $room,
				'msr' => $msr,
				'total' => $total,
				'tip_amount' => $tip_amount,
				'advanceamt' => $advanceamt,
				'advance' => $advancetotal,
				'totalpaysumm' => $totalpaysumm
			);

			/************************************** Payment Summary End *************************************************/

			/************************************** Online Payment Summary Start *************************************************/
			$onlineorder = array();
			$onlineorderamt = array();
			$swiggy_total = 0;
			$zomato_total = 0;
			$online_total = 0;
			$uber_total = 0;
			$g_tot = 0;
			$sql = "SELECT grand_total,order_from FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON  oi.`order_id` = oit.`order_id` WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`urbanpiper_order_id` <> ''  ";
			$sql .= " GROUP BY oit.`order_id`";	
			$online_orders = $this->db->query($sql)->rows;
			
			foreach ($online_orders as $online_order) {
				if($online_order['order_from'] == 'swiggy'){
					$swiggy_total = $swiggy_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'zomato'){
					$zomato_total = $zomato_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Online'){
					$online_total = $online_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Uber'){
					$uber_total = $uber_total + $online_order['grand_total'];
				}

				
			}
			$g_tot =  $swiggy_total + $zomato_total + $online_total + $uber_total;

			
			$onlineorder = array(
				'swiggy_total' => $swiggy_total,
				'zomato_total' => $zomato_total,
				'online_total' => $online_total,
				'uber_total'   => $uber_total,
				'g_tot' 	   => $g_tot
			);


			/************************************** Online Payment Summary *************************************************/



			/************************************** Tax Summary End *************************************************/
			// echo "<pre>";
			// print_r($orderdatauserwises);
			// exit();
			//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items_report` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
			$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`discount_value`) as discount_value, SUM(oit.`tax1_value`) as tax1_value, SUM(oit.`tax2_value`) as tax2_value, oit.`is_liq`, oit.`tax1`, oit.`tax2` FROM `oc_order_items_report` oit LEFT JOIN `oc_order_info_report` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY tax1, is_liq";
			//echo $sql;exit;
			$tests = $this->db->query($sql)->rows; 
			foreach($tests as $test){
				if($test['is_liq'] == '0'){
					if(isset($testfoods[$test['tax1'] + $test['tax2']]) ){
						if(isset($testfoods[$test['tax1'] + $test['tax2']]['tax1_value']) && $test['tax1_value'] > 0 ){
							
							$testfoods[$test['tax1'] + $test['tax2']] = array(
								'tax1' => $test['tax1'] + $test['tax2'],
								'amt' => $testfoods[$test['tax1'] + $test['tax2']]['amt'] + $test['amt'] - $test['discount_value'],
								'tax1_value' => $testfoods[$test['tax1'] + $test['tax2']]['tax1_value'] + $test['tax2_value'] + $test['tax1_value']
							);
						}
					} else {
						$testfoods[$test['tax1'] + $test['tax2']] = array(
							'tax1' => $test['tax1'] + $test['tax2'],
							'amt' => $test['amt'] - $test['discount_value'],
							'tax1_value' => $test['tax1_value']  + $test['tax2_value']
						);
					}
				} else{
					$testliqs[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				}
			}
			$foodnetamt = 0;
			$taxvalfood = 0;
			$liqnetamt = 0;
			$taxvalliq = 0;

			foreach($testfoods as $tkey => $tvalue){
				$foodarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$foodnetamt = $foodnetamt + $tvalue['amt'];
				$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
			}

			foreach($testliqs as $tkey => $tvalue){
				$liqarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$liqnetamt = $liqnetamt + $tvalue['amt'];
				$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
			}

			$ordertaxamt = array(
				'foodtax' => $foodarray,
				'liqtax' => $liqarray,
				'foodnetamt' => $foodnetamt,
				'taxvalfood' => $taxvalfood,
				'liqnetamt' => $liqnetamt,
				'taxvalliq' => $taxvalliq
			);
			
			// echo "<pre>";
			// print_r($ordertaxamt);
			// exit();
			/************************************** Tax Summary End *************************************************/

			/************************************** Category Start **************************************************/
			$foodamt = '0';
			$liqamt = '0';
			$disamt_food = 0;
			$foodtotaldis = 0;
			$liqtotaldis = 0;
			$disamt_liq = 0;

			if($filter_category == '0'){
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$foodamt = $this->db->query("SELECT SUM(oit.`amt`) as total_food,SUM(oit.`discount_value`) as discount_value, FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
				$disamt_food = $foodamt['discount_value'];
			}
			if($filter_category == '1'){
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$liqamt = $liqamt['total_liq'];
				$disamt_liq = $liqamt['discount_value'];
			}
			if($filter_category == '99'){
				$foodamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_food FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as ldiscount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				// echo'<pre>';
				// print_r($foodamt['fdiscount_value']);
				// exit;
				$foodtotaldis = $foodamt['discount_value'];
				$liqtotaldis = $liqamt['ldiscount_value'];
				$foodamt = $foodamt['total_food'];
				$liqamt = $liqamt['total_liq'];

				
				
			}

			/************************************** Category End **************************************************/

			/************************************** Sub Category Start **************************************************/

			$sql = "SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY subcategoryid";
			//echo $sql;exit;
			$subcategoryamts = $this->db->query($sql)->rows; 
			// echo'<pre>';
			// print_r($subcategoryamts);
			// exit;
			$totalamt = 0;
			$discountamt = 0;
			$subcategory = array();
			foreach($subcategoryamts as $key){
				$subcategoryname = $this->db->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'")->row;
				$subcategory[] = array(
					'name' => $subcategoryname['name'],
					'amount' =>  $key['amt']
				);
				$totalamt = $totalamt + $key['amt'];
				$discountamt = $discountamt + $key['discount_value'];
			}
			
			$catsub = array(
				
				'foodcat' => $foodamt,
				'liqcat' => $liqamt,
				'cat_fdis' => $foodtotaldis,
				'cat_ldis' => $liqtotaldis,
				'subcat' => $subcategory,
				'disamt_food' => $disamt_food,
				'disamt_liq' => $disamt_liq,
				'totalamt' => $totalamt,
				'discountamt' => $discountamt,
			);

			// echo'<pre>';
			// print_r($catsub);
			// exit;

			/************************************** Sub Category End **************************************************/

			/************************************** Second last part Start **************************************************/
			
			$sql = "SELECT billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY billno DESC LIMIT 1";
			$lastbillno = $this->db->query($sql); 

			if($lastbillno->num_rows > 0){
					$lastbillno = $lastbillno->row['billno'];			
			} else{
				$lastbillno = 0;			
			}
			// echo'<pre>';
			// print_r($lastbillno);
			// exit;

			$lastkotno = 0;
			$lastbotno = 0;

			if($filter_category == '0'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` LEFT JOIN `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];
				} else{
					$lastkotno = 0;
				}
			}
			if($filter_category == '1'){
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];
				} else{
					$lastbotno = 0;				
				}
			}
			if($filter_category == '99'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];			
				} else{
					$lastkotno = 0;			
				}
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];			
				} else{
					$lastbotno = 0;			
				}
			}

			$sql = "SELECT *,oi.`order_id` FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
			$orderdatalast = $this->db->query($sql)->rows; 
			$totalpendingamount = 0;
			$totalpendingbill = 0;
			$duplicate = 0;
			$duplicatecount = 0;
			$totalamountnc = 0;
			$totalnc = 0;
			$discountamount = 0;
			$discountbillcount = 0;
			$advanceamount = 0;
			$billmodify = 0;
			$qty = 0;
			$cancelamountkot = 0;
			$cancelamountbot = 0;
			$cancelkot = 0;
			$cancelbot = 0;
			foreach ($orderdatalast as $key) {
				if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
					$totalpendingamount = $totalpendingamount + $key['grand_total'];
					$totalpendingbill ++;
				}

				if($key['duplicate'] == '1'){
					$duplicate = $duplicate + $key['duplicate'];
					$duplicatecount ++;
				}

				if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
					$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					$discountbillcount ++;
				}

				if($key['advance_amount'] != '0'){
					$advanceamount = $advanceamount + $key['advance_amount'];
				}
			}

			$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`)  WHERE  oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY oi.`order_id`";	
			$orderitemlast = $this->db->query($sql)->rows;
			foreach ($orderitemlast as $key) {
				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
					$cancelamountkot = $cancelamountkot + $key['amt'];
					$cancelkot ++;
				}

				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
					$cancelamountbot = $cancelamountbot + $key['amt'];
					$cancelbot ++;
				}

				if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
					$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					$totalnc ++;
				}
			}
			
			$cancelledbillcount = 0;
			$cancelledbill = 0;
			$sql = "SELECT count(*) as row_count,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1'  AND `cancel_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " ORDER BY oit.`order_id` ";
			//echo $sql;exit;
			$orderdatabill = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatabill);exit;
			if ($orderdatabill->num_rows > 0) {
				if ($orderdatabill->row['grand_total'] > '0') {
					$cancelledbillcount = $orderdatabill->row['row_count'];
					//echo $cancelledbillcount;exit;
				}
				$cancelledbill = $orderdatabill->row['grand_total'];
			}
			

			$complimentarycount = 0;
			$complimentarybill = 0;
			$sql = "SELECT count(*) as row_count ,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatacomp = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatacomp);exit;
			if ($orderdatacomp->num_rows > 0) {
				if ($orderdatacomp->row['grand_total'] > 0) {
					$complimentarycount = $orderdatacomp->row['row_count'];
				}
				$complimentarybill = $orderdatacomp->row['grand_total'];
			}

			$orderitemlastval = array(
				'cancelamountkot' => $cancelamountkot,
				'cancelamountbot' => $cancelamountbot,
				'cancelkot' => $cancelkot,
				'cancelbot' => $cancelbot,
				'cancelledbill' => $cancelledbill,
				'cancelledcount' => $cancelledbillcount,
				'complimentarybill' => $complimentarybill,
				'complimentarycount' => $complimentarycount,
			);

			$pendingtable = 0;
			$pending_table = $this->db->query("SELECT count(*) as pending_table FROM oc_order_info_report WHERE  bill_status = '0'AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($pending_table->num_rows > 0) {
				$pendingtable = $pending_table->row['pending_table'];
			}
			
			$bill_printed = 0;
			$bill_count = $this->db->query("SELECT count(*) as bill_printed FROM oc_order_info_report WHERE  bill_status = '1' AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($bill_count->num_rows > 0) {
				$bill_printed = $bill_count->row['bill_printed'];
			}

			$paid_out = 0;
			$paid_outs = $this->db->query("SELECT SUM(amount) as paid_out FROM oc_expense_trans WHERE  payment_type = 'PAID OUT' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_outs->num_rows > 0) {
				$paid_out = $paid_outs->row['paid_out'];
			}

			$paid_in = 0;
			$paid_inss = $this->db->query("SELECT SUM(amount) as paid_in FROM oc_expense_trans WHERE  payment_type = 'PAID IN' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_inss->num_rows > 0) {
				$paid_in = $paid_inss->row['paid_in'];
			}


			$orderdatalastval = array(
				'lastbill' => $lastbillno,
				'lastkot' => $lastkotno,
				'lastbot' => $lastbotno,
				'paid_out' => $paid_out,
				'paid_in' => $paid_in,
				'pendingbill' => $totalpendingbill,
				'pendingtable' => $pendingtable,
				'bill_printed' => $bill_printed,
				'pendingamount' => $totalpendingamount,
				'duplicate' => $duplicate,
				'duplicatecount' => $duplicatecount,
				'totalnc' => $totalnc,
				'totalamountnc' => $totalamountnc,
				'billmodify' => $billmodify,
				'discountamount' => $discountamount,
				'discountbillcount' => $discountbillcount,
				'advanceamount' => $advanceamount
			);

			$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info_report oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql1 .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$bill_modifiy_data = $this->db->query($sql1);
			$data['NumberOfbillmodify'] = 0;
			$data['totalbillamount'] = 0;
			if($bill_modifiy_data->num_rows > 0){
				$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
				$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
				
			}
			$orderdatalastarray = array(
				'orderdatalast' => $orderdatalastval,
				'orderitemlast' => $orderitemlastval
			);
			/************************************** Second last part End **************************************************/
			$tip = $this->db->query("SELECT SUM(tip) as tip FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($tip->num_rows > 0){
				$data['tip'] = $tip->row['tip'];
			} else{
				$data['tip'] = 0;
			}

			$persons = $this->db->query("SELECT SUM(person) as person , SUM(grand_total) as g_total FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			if($persons->num_rows > 0){
				$data['person'] = $persons->row['person'];
				if($persons->row['person'] > 0){
					$per = $persons->row['person'];
					$data['average_grand_total'] =  $persons->row['person'] / $persons->row['g_total'];
				} else {
					$per = 0;
					$data['average_grand_total'] =  0;

				}

			} else{
				$data['person'] = 0;
				$data['average_grand_total'] = 0;
			}


			$pending_datas = $this->db->query("SELECT sum(grand_total) as pending_amt , count(*) as pendingcount FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND (`bill_status` = '0' OR `bill_status` = '1') AND `pay_method` = '0' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			if($pending_datas->num_rows > 0){
				$data['pendingtable_amt'] = $pending_datas->row['pending_amt'];
				$data['pendingtablecount'] = $pending_datas->row['pendingcount'];

			} else{
				$data['pendingtable_amt'] = 0;
				$data['pendingtablecount'] = 0;
			}


		}
		//echo '<pre>';print_r($orderdataarray);exit;
		$data['orderdataarrays'] = $orderdataarray; // user wise
		$data['orderlocdatas'] = $orderlocdatas; // location wise
		$data['orderlocdatasamt'] = $orderlocdatasamt; // all total amt location wise
		$data['orderpaymentamt'] = $orderpaymentamt; // all payment details
		$data['onlineorderamt'] = $onlineorder; // all payment online details
		$data['ordertaxamt'] = $ordertaxamt; // all tax details
		$data['catsubs'] = $catsub; //category and subcategory
		$data['orderdatalastarray'] = $orderdatalastarray;
		// echo "<pre>";
		// print_r($orderdatalastarray);
		// exit();


		$forprintarray = array(
			'orderdataarray' => $orderdataarray,
			'orderlocdatas' => $orderlocdatas,
			'orderlocdatasamt' => $orderlocdatasamt,
			'orderpaymentamt' => $orderpaymentamt,
			'ordertaxamt' => $ordertaxamt,
		);

		$data['forprintarray'] = json_encode($forprintarray);

		$data['categorys'] = array(
								//'0' => 'Food',
								//'1' => 'Liquor'
							);
		$data['filter_category'] = $filter_category;
		$data['token'] = $this->session->data['token'];
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		$data['action'] = $this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
	}

	public function prints(){

		if(isset($this->request->get['filter_category'])){
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '99';
		}

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			/************************************** User Wise Start *************************************************/
			$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, oi.`bill_date`, is_liq, onac FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oi.`login_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderdata) {
				$orderdataval = array();
				$orderitemval = array();
				$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows;
				$netsale = 0;
				$pay_cash = 0;
				$msr =0;
				$pay_card = 0;
				$pay_online = 0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$duplicatebill = 0;
				$duplicatecount = 0;
				$advanceamount = 0;
				$cancelkot = 0;
				$cancelamountkot = 0;
				$cancelbot = 0;
				$cancelamountbot = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$cancelledbill = 0;
				$cancelledbillcount = 0;
				$complimentarybill = 0;
				$complimentarycount = 0;
				foreach ($orderdatatest as $key) {
					//$netsale =  round($netsale  + $key['grand_total'] );

					$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
					$pay_cash = $pay_cash + $key['pay_cash'];
					$msr =$msr + $key['msrcard'];
					$pay_card = $pay_card + $key['pay_card'];
					$pay_online = $pay_online + $key['pay_online'];
					$onac = $onac + $key['onac'];
					$mealpass = $mealpass + $key['mealpass'];
					$pass = $pass + $key['pass'];

					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingbill ++;
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountbillcount ++;
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					}

					if($key['duplicate'] == '1'){
						$duplicatecount ++;
						$duplicatebill = $duplicatebill + $key['grand_total'];
					}

					$advanceamount = $advanceamount + $key['advance_amount'];
				}

				$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info_report`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY `order_id`";
				$orderdatabill = $this->db->query($sql)->rows;
				//echo "<pre>";print_r($orderdatabill);exit;
				foreach ($orderdatabill as $key => $value) {
					if($value['cancel_status'] == '1'){
						$cancelledbillcount ++;
						$cancelledbill = $cancelledbill + $value['grand_total'];
					}

					if($value['complimentary_status'] == '1'){
						$complimentarycount ++;
						$complimentarybill = $complimentarybill + $value['grand_total'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";
				$orderdatatest = $this->db->query($sql)->rows; 
				foreach ($orderdatatest as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalnc ++;
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					}
				}

				$orderdataval = array(
					'netsale' => $netsale,
					'total_cash' => $pay_cash,
					'msr' => $msr,
					'total_card' => $pay_card,
					'total_pay_online' => $pay_online,
					'onac' => $onac,
					'total_mealpass' => $mealpass,
					'total_pass' => $pass,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'discountbillcount' => $discountbillcount,
					'discountamount' => $discountamount,
					'duplicate' => $duplicatebill,
					'duplicatecount' => $duplicatecount,
					'advanceamount' => $advanceamount,
					'cancelkot' => $cancelkot,
					'cancelamountkot' => $cancelamountkot,
					'cancelbot' => $cancelbot,
					'cancelamountbot' => $cancelamountbot,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'cancelledcount' => $cancelledbillcount,
					'cancelledbill' => $cancelledbill,
					'complimentarycount' => $complimentarycount,
					'complimentarybill' => $complimentarybill,
					'login_id' => $orderdata['login_id'],
					'login_name' => $orderdata['login_name'],
				);
				$orderdataarray[$orderdata['login_id']] = array(
					'orderdata' => $orderdataval,
				);
			}
			/************************************** User Wise End *************************************************/

			/************************************** Location Wise Start *************************************************/

			$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oit.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY location";
			$orderdatalocationwises = $this->db->query($sql)->rows; 
			$totalnetsale = 0;
			$totalgst = 0;
			$totalvat = 0;
			$totalroundoff = 0;
			$totalscharge = 0;
			$totaldiscount = 0;
			$nettotalamt = 0;
			$advance = 0;
			$totalpackaging_sgst = 0;
			$totalpackaging_cgst = 0;
			$totalpackaging = 0;

			foreach($orderdatalocationwises as $orderdata){
				$sql = "SELECT *, oi.stax as order_stax FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";
				//echo $sql;exit;
				$orderdatatestloc = $this->db->query($sql)->rows; 
				$netsale = 0;
				$vat = 0;
				$gst = 0;
				$roundoff = 0;
				$scharge = 0;
				$discount = 0;
				$nettotal = 0;
				$advance = 0;
				$packaging = 0;
				$packaging_cgst = 0;
				$packaging_sgst = 0;
				foreach ($orderdatatestloc as $key) {
					if($key['is_liq'] == '1' && $filter_category == '1'){
						$netsale = $netsale + $key['amt'];
						$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxliq'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					}elseif($key['is_liq'] == '0' && $filter_category == '0'){
						$netsale = $netsale + $key['amt'];
						$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxfood'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					} else{
						$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
						$vat = $vat + $key['vat'];
						$gst = $gst + $key['gst'];
						$roundoff = $roundoff + $key['roundtotal'];
						$scharge = $scharge + $key['order_stax'];
						$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$nettotal = $nettotal + $key['grand_total'] + ($key['roundtotal']);
						$advance = $advance + $key['advance_amount'];
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

					}
				}
				$orderlocdatas[] = array(
					'location' => $key['location'],
					'netsale' => $netsale,
				);
				$totalnetsale = $totalnetsale + $netsale;
				$totalgst = $totalgst + $gst;
				$totalvat = $totalvat + $vat;
				$totalroundoff = $totalroundoff + $roundoff;
				$totalscharge = $totalscharge + $scharge;
				$totaldiscount = $totaldiscount + $discount;
				$nettotalamt = $nettotalamt + $nettotal + $advance;

				$totalpackaging = $totalpackaging + $packaging;
				$totalpackaging_cgst = $totalpackaging_cgst + $packaging_cgst;
				$totalpackaging_sgst = $totalpackaging_sgst + $packaging_sgst;
			}

			$orderlocdatasamt = array(
				'totalnetsale' => $totalnetsale,
				'totalgst' => $totalgst,
				'totalvat' => $totalvat,
				'totalroundoff' => $totalroundoff,
				'totalscharge' => $totalscharge,
				'totaldiscount' => $totaldiscount,
				'nettotalamt' => $nettotalamt,
				'advance' => $advance,
				'totalpackaging' => $totalpackaging,
				'totalpackaging_cgst' => $totalpackaging_cgst,
				'totalpackaging_sgst' => $totalpackaging_sgst 
			);

			/************************************** Location Wise End *************************************************/

			/************************************** Payment Summary Start *************************************************/
			$totalcash = 0;
			$totalcard = 0;
			$totalpayonline =0;
			$onac = 0;
			$mealpass = 0;
			$pass = 0;
			$room = 0;
			$msr = 0;
			$total = 0;
			$cardtip = 0;
			$totalpaysumm = 0;
			$onac = 0;
			$advanceamt = 0;
			$tip_amount = 0;
			$persons = 0;
			$sql = "SELECT pay_cash,msrcard ,pay_card,pay_online, mealpass, pass, advance_amount, oit.`bill_date`, is_liq, onac, tip FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oit.`order_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderpayment) {
				$totalcash = $totalcash + $orderpayment['pay_cash'];
				$totalcard = $totalcard + $orderpayment['pay_card'];
				$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
				$onac = $onac + $orderpayment['onac'];
				$mealpass = $mealpass + $orderpayment['mealpass'];
				$pass = $pass + $orderpayment['pass'];
				$advanceamt = $advance + $orderpayment['advance_amount'];
				$tip_amount = round($tip_amount) + round($orderpayment['tip']);
				$room = 0.00;
				$msr =$msr + $orderpayment['msrcard'];
				$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
				$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$orderpaymentamt = array(
				'totalcash' => $totalcash,
				'totalcard' => $totalcard,
				'totalpayonline' => $totalpayonline,
				'onac' => $onac,
				'mealpass' => $mealpass,
				'pass' => $pass,
				'room' => $room,
				'msr' => $msr,
				'total' => $total,
				'tip_amount' => $tip_amount,
				'advanceamt' => $advanceamt,
				'advance' => $advancetotal,
				'totalpaysumm' => $totalpaysumm
			);

			/************************************** Payment Summary End *************************************************/

			/************************************** Online Payment Summary Start *************************************************/
			$onlineorder = array();
			$onlineorderamt = array();
			$swiggy_total = 0;
			$zomato_total = 0;
			$online_total = 0;
			$uber_total = 0;
			$g_tot = 0;
			$sql = "SELECT grand_total,order_from FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON  oi.`order_id` = oit.`order_id` WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`urbanpiper_order_id` <> ''  ";
			$sql .= " GROUP BY oit.`order_id`";	
			$online_orders = $this->db->query($sql)->rows;
			
			foreach ($online_orders as $online_order) {
				if($online_order['order_from'] == 'swiggy'){
					$swiggy_total = $swiggy_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'zomato'){
					$zomato_total = $zomato_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Online'){
					$online_total = $online_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Uber'){
					$uber_total = $uber_total + $online_order['grand_total'];
				}

				
			}
			$g_tot =  $swiggy_total + $zomato_total + $online_total + $uber_total;

			
			$onlineorder = array(
				'swiggy_total' => $swiggy_total,
				'zomato_total' => $zomato_total,
				'online_total' => $online_total,
				'uber_total'   => $uber_total,
				'g_tot' 	   => $g_tot
			);


			/************************************** Online Payment Summary *************************************************/


			/************************************** Tax Summary End *************************************************/
			// echo "<pre>";
			// print_r($orderdatauserwises);
			// exit();
			//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items_report` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
			$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`discount_value`) as discount_value, SUM(oit.`tax1_value`) as tax1_value, oit.`is_liq`, oit.`tax1` FROM `oc_order_items_report` oit LEFT JOIN `oc_order_info_report` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY tax1, is_liq";
			//echo $sql;exit;
			$tests = $this->db->query($sql)->rows; 
			foreach($tests as $test){
				if($test['is_liq'] == '0'){
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				} else{
					$testliqs[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				}
			}
			$foodnetamt = 0;
			$taxvalfood = 0;
			$liqnetamt = 0;
			$taxvalliq = 0;

			foreach($testfoods as $tkey => $tvalue){
				$foodarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$foodnetamt = $foodnetamt + $tvalue['amt'];
				$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
			}

			foreach($testliqs as $tkey => $tvalue){
				$liqarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$liqnetamt = $liqnetamt + $tvalue['amt'];
				$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
			}

			$ordertaxamt = array(
				'foodtax' => $foodarray,
				'liqtax' => $liqarray,
				'foodnetamt' => $foodnetamt,
				'taxvalfood' => $taxvalfood,
				'liqnetamt' => $liqnetamt,
				'taxvalliq' => $taxvalliq
			);
			
			// echo "<pre>";
			// print_r($ordertaxamt);
			// exit();
			/************************************** Tax Summary End *************************************************/

			/************************************** Category Start **************************************************/
			$foodamt = '0';
			$liqamt = '0';
			$disamt_food = 0;
			$foodtotaldis = 0;
			$liqtotaldis = 0;
			$disamt_liq = 0;

			if($filter_category == '0'){
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$foodamt = $this->db->query("SELECT SUM(oit.`amt`) as total_food,SUM(oit.`discount_value`) as discount_value, FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
				$disamt_food = $foodamt['discount_value'];
			}
			if($filter_category == '1'){
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$liqamt = $liqamt['total_liq'];
				$disamt_liq = $liqamt['discount_value'];
			}
			if($filter_category == '99'){
				$foodamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_food FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as ldiscount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				// echo'<pre>';
				// print_r($foodamt['fdiscount_value']);
				// exit;
				$foodtotaldis = $foodamt['discount_value'];
				$liqtotaldis = $liqamt['ldiscount_value'];
				$foodamt = $foodamt['total_food'];
				$liqamt = $liqamt['total_liq'];

				
				
			}

			/************************************** Category End **************************************************/

			/************************************** Sub Category Start **************************************************/

			$sql = "SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY subcategoryid";
			//echo $sql;exit;
			$subcategoryamts = $this->db->query($sql)->rows; 
			// echo'<pre>';
			// print_r($subcategoryamts);
			// exit;
			$totalamt = 0;
			$discountamt = 0;
			$subcategory = array();
			foreach($subcategoryamts as $key){
				$subcategoryname = $this->db->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'")->row;
				$subcategory[] = array(
					'name' => $subcategoryname['name'],
					'amount' =>  $key['amt']
				);
				$totalamt = $totalamt + $key['amt'];
				$discountamt = $discountamt + $key['discount_value'];
			}
			
			$catsub = array(
				
				'foodcat' => $foodamt,
				'liqcat' => $liqamt,
				'cat_fdis' => $foodtotaldis,
				'cat_ldis' => $liqtotaldis,
				'subcat' => $subcategory,
				'disamt_food' => $disamt_food,
				'disamt_liq' => $disamt_liq,
				'totalamt' => $totalamt,
				'discountamt' => $discountamt,
			);

			// echo'<pre>';
			// print_r($catsub);
			// exit;

			/************************************** Sub Category End **************************************************/

			/************************************** Second last part Start **************************************************/
			
			$sql = "SELECT billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY billno DESC LIMIT 1";
			$lastbillno = $this->db->query($sql); 

			if($lastbillno->num_rows > 0){
					$lastbillno = $lastbillno->row['billno'];			
			} else{
				$lastbillno = 0;			
			}
			// echo'<pre>';
			// print_r($lastbillno);
			// exit;

			$lastkotno = 0;
			$lastbotno = 0;

			if($filter_category == '0'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` LEFT JOIN `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];
				} else{
					$lastkotno = 0;
				}
			}
			if($filter_category == '1'){
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];
				} else{
					$lastbotno = 0;				
				}
			}
			if($filter_category == '99'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];			
				} else{
					$lastkotno = 0;			
				}
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];			
				} else{
					$lastbotno = 0;			
				}
			}

			$sql = "SELECT *,oi.`order_id` FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
			$orderdatalast = $this->db->query($sql)->rows; 
			$totalpendingamount = 0;
			$totalpendingbill = 0;
			$duplicate = 0;
			$duplicatecount = 0;
			$totalamountnc = 0;
			$totalnc = 0;
			$discountamount = 0;
			$discountbillcount = 0;
			$advanceamount = 0;
			$billmodify = 0;
			$qty = 0;
			$cancelamountkot = 0;
			$cancelamountbot = 0;
			$cancelkot = 0;
			$cancelbot = 0;
			foreach ($orderdatalast as $key) {
				if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
					$totalpendingamount = $totalpendingamount + $key['grand_total'];
					$totalpendingbill ++;
				}

				if($key['duplicate'] == '1'){
					$duplicate = $duplicate + $key['duplicate'];
					$duplicatecount ++;
				}

				if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
					$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					$discountbillcount ++;
				}

				if($key['advance_amount'] != '0'){
					$advanceamount = $advanceamount + $key['advance_amount'];
				}
			}

			$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`)  WHERE  oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY oi.`order_id`";	
			$orderitemlast = $this->db->query($sql)->rows;
			foreach ($orderitemlast as $key) {
				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
					$cancelamountkot = $cancelamountkot + $key['amt'];
					$cancelkot ++;
				}

				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
					$cancelamountbot = $cancelamountbot + $key['amt'];
					$cancelbot ++;
				}

				if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
					$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					$totalnc ++;
				}
			}
			
			$cancelledbillcount = 0;
			$cancelledbill = 0;
			$sql = "SELECT count(*) as row_count,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1'  AND `cancel_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " ORDER BY oit.`order_id` ";
			//echo $sql;exit;
			$orderdatabill = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatabill);exit;
			if ($orderdatabill->num_rows > 0) {
				if ($orderdatabill->row['grand_total'] > '0') {
					$cancelledbillcount = $orderdatabill->row['row_count'];
					//echo $cancelledbillcount;exit;
				}
				$cancelledbill = $orderdatabill->row['grand_total'];
			}
			

			$complimentarycount = 0;
			$complimentarybill = 0;
			$sql = "SELECT count(*) as row_count ,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatacomp = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatacomp);exit;
			if ($orderdatacomp->num_rows > 0) {
				if ($orderdatacomp->row['grand_total'] > 0) {
					$complimentarycount = $orderdatacomp->row['row_count'];
				}
				$complimentarybill = $orderdatacomp->row['grand_total'];
			}

			$orderitemlastval = array(
				'cancelamountkot' => $cancelamountkot,
				'cancelamountbot' => $cancelamountbot,
				'cancelkot' => $cancelkot,
				'cancelbot' => $cancelbot,
				'cancelledbill' => $cancelledbill,
				'cancelledcount' => $cancelledbillcount,
				'complimentarybill' => $complimentarybill,
				'complimentarycount' => $complimentarycount,
			);

			$pendingtable = 0;
			$pending_table = $this->db->query("SELECT count(*) as pending_table FROM oc_order_info_report WHERE  bill_status = '0'AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($pending_table->num_rows > 0) {
				$pendingtable = $pending_table->row['pending_table'];
			}
			
			$bill_printed = 0;
			$bill_count = $this->db->query("SELECT count(*) as bill_printed FROM oc_order_info_report WHERE  bill_status = '1' AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($bill_count->num_rows > 0) {
				$bill_printed = $bill_count->row['bill_printed'];
			}

			$paid_out = 0;
			$paid_outs = $this->db->query("SELECT SUM(amount) as paid_out FROM oc_expense_trans WHERE  payment_type = 'PAID OUT' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_outs->num_rows > 0) {
				$paid_out = $paid_outs->row['paid_out'];
			}

			$paid_in = 0;
			$paid_inss = $this->db->query("SELECT SUM(amount) as paid_in FROM oc_expense_trans WHERE  payment_type = 'PAID IN' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_inss->num_rows > 0) {
				$paid_in = $paid_inss->row['paid_in'];
			}


			$orderdatalastval = array(
				'lastbill' => $lastbillno,
				'lastkot' => $lastkotno,
				'lastbot' => $lastbotno,
				'paid_out' => $paid_out,
				'paid_in' => $paid_in,
				'pendingbill' => $totalpendingbill,
				'pendingtable' => $pendingtable,
				'bill_printed' => $bill_printed,
				'pendingamount' => $totalpendingamount,
				'duplicate' => $duplicate,
				'duplicatecount' => $duplicatecount,
				'totalnc' => $totalnc,
				'totalamountnc' => $totalamountnc,
				'billmodify' => $billmodify,
				'discountamount' => $discountamount,
				'discountbillcount' => $discountbillcount,
				'advanceamount' => $advanceamount
			);

			$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info_report oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql1 .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$bill_modifiy_data = $this->db->query($sql1);
			$data['NumberOfbillmodify'] = 0;
			$data['totalbillamount'] = 0;
			if($bill_modifiy_data->num_rows > 0){
				$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
				$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
				
			}
			$orderdatalastarray = array(
				'orderdatalast' => $orderdatalastval,
				'orderitemlast' => $orderitemlastval
			);
			/************************************** Second last part End **************************************************/
			$tip = $this->db->query("SELECT SUM(tip) as tip FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($tip->num_rows > 0){
				$data['tip'] = $tip->row['tip'];
			} else{
				$data['tip'] = 0;
			}

			$persons = $this->db->query("SELECT SUM(person) as person , SUM(grand_total) as g_total FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			// if($persons->num_rows > 0){
			// 	$data['person'] = $persons->row['person'];
			// 	$data['average_grand_total'] = $persons->row['g_total']/ $persons->row['person'];

			// } else{
			// 	$data['person'] = 0;
			// 	$data['average_grand_total'] = 0;
			// }

			if($persons->num_rows > 0){
				$data['person'] = $persons->row['person'];
				if($persons->row['person'] > 0){
					$per = $persons->row['person'];
					$data['average_grand_total'] =  $persons->row['person'] / $persons->row['g_total'];
				} else {
					$per = 0;
					$data['average_grand_total'] =  0;

				}

			} else{
				$data['person'] = 0;
				$data['average_grand_total'] = 0;
			}

			$pending_datas = $this->db->query("SELECT sum(grand_total) as pending_amt , count(*) as pendingcount FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND (`bill_status` = '0' OR `bill_status` = '1') AND `pay_method` = '0' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			if($pending_datas->num_rows > 0){
				$data['pendingtable_amt'] = $pending_datas->row['pending_amt'];
				$data['pendingtablecount'] = $pending_datas->row['pendingcount'];

			} else{
				$data['pendingtable_amt'] = 0;
				$data['pendingtablecount'] = 0;
			}

			/************************************** Second last part End **************************************************/
			$this->load->model('catalog/order');
			try {
			   	if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
			 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
			 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
			 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
			 	} else {
			 		$connector = '';
			 	}
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Day Summary Report");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$start_date,30)."To :".$end_date);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Login Summary ",30)."Amount ");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$usertotal = 0;
			  	foreach($orderdataarray as $key => $value){
					$printer->text(str_pad("User :".$value['orderdata']['login_id'],10)."".str_pad($value['orderdata']['login_name'],10)."".str_pad("T.Sale", 10)."".$value['orderdata']['netsale']);
					$printer->feed(1);
					$printer->text("------------------------------------------------");
			  		$printer->feed(1);
					$printer->text(str_pad("Usettle Bill Amount ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Cash ",30)."".$value['orderdata']['total_cash']);
					$printer->feed(1);
					$printer->text(str_pad("Credit Card ",30)."".$value['orderdata']['total_card']);
					$printer->feed(1);
					$printer->text(str_pad("Online ",30)."".$value['orderdata']['total_pay_online']);
					$printer->feed(1);
					$printer->text(str_pad("On A/c ",30).$value['orderdata']['onac']);
					$printer->feed(1);
					$printer->text(str_pad("Meal Pass ",30)."".$value['orderdata']['total_mealpass']);
					$printer->feed(1);
					$printer->text(str_pad("Pass ",30)."".$value['orderdata']['total_pass']);
					$printer->feed(1);
					$printer->text(str_pad("MSR ",30)."".$value['orderdata']['msr']);
					$printer->feed(1);
					$printer->text(str_pad("Pending table :",20)."".str_pad($value['orderdata']['pendingbill'],10)."".$value['orderdata']['pendingamount']);
					$printer->feed(1);
					$printer->text(str_pad("Compliment bill ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Cancel bill ",20)."".str_pad($value['orderdata']['cancelledcount'],10).$value['orderdata']['cancelledbill']);
					$printer->feed(1);
					$printer->text(str_pad("Canceled Kot :",20)."".str_pad($value['orderdata']['cancelkot'],10)."".$value['orderdata']['cancelamountkot']);
					$printer->feed(1);
					$printer->text(str_pad("Canceled Bot :",20)."".str_pad($value['orderdata']['cancelbot'],10)."".$value['orderdata']['cancelamountbot']);
					$printer->feed(1);
					$printer->text(str_pad("NC KOT :",20)."".str_pad($value['orderdata']['totalnc'],10)."".$value['orderdata']['totalamountnc']);
					$printer->feed(1);
					$printer->text(str_pad("DUPLICATE BILL ",20)."".str_pad($value['orderdata']['duplicatecount'],10).$value['orderdata']['duplicate']);
					$printer->feed(1);
					$printer->text(str_pad("Disc Before Bill :",20)."".str_pad($value['orderdata']['discountbillcount'],10)."".$value['orderdata']['discountamount']);
					$printer->feed(1);
					$printer->text(str_pad("DUPLICATE KOT ",20)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Disc After Bill ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Advance Amount (-)",30).$value['orderdata']['advanceamount']);
					$printer->feed(1);
					$usertotal = $usertotal + $value['orderdata']['total_cash'] + $value['orderdata']['total_card'] + $value['orderdata']['total_pay_online'] + $value['orderdata']['pendingamount'] + $value['orderdata']['onac'] + $value['orderdata']['advanceamount'] + $value['orderdata']['msr'];
					$printer->text("------------------------------------------------");
			  		$printer->feed(1);
				}
				$printer->text(str_pad("All User Totals ",30).$usertotal);
				$printer->feed(1);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Table Group ",30)."Amount ");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach($orderlocdatas as $locdata){
			  		$printer->text(str_pad($locdata['location'],30).$locdata['netsale']);
			  		$printer->feed(1);
			  	}
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Sub Total ",30).$orderlocdatasamt['totalnetsale']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("KKC Amt(+) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("SBC Amt(+) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("S-Chrg Amt(+) ",30).$orderlocdatasamt['totalscharge']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Vat(+) ",30).$orderlocdatasamt['totalvat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Gst(+) ",30).$orderlocdatasamt['totalgst']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Packaging ",30).$orderlocdatasamt['totalpackaging']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Packaging CGST ",30).$orderlocdatasamt['totalpackaging_cgst']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Packaging SGST ",30).$orderlocdatasamt['totalpackaging_sgst']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("R-off Amt(+) ",30).$orderlocdatasamt['totalroundoff']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Discount Amt(-) ",30). $orderlocdatasamt['totaldiscount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("P-Discount Amt(-) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancel Bill Amt(-) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance. Amt(-) ",30).$orderlocdatasamt['advance']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("Net Total ",30).$orderlocdatasamt['nettotalamt']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text("Payment Summary");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cash ",30).$orderpaymentamt['totalcash']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Credit Card ",30).$orderpaymentamt['totalcard']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Online ",30).$orderpaymentamt['totalpayonline']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("On A/c ",30).$orderpaymentamt['onac']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Meal Pass ",30).$orderpaymentamt['mealpass']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Pass ",30).$orderpaymentamt['pass']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("MSR ",30).$orderpaymentamt['msr']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Room Service ",30).$orderpaymentamt['room']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("MSR Card ",30).$orderpaymentamt['msr']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance Amount (-) ",30).$orderpaymentamt['advanceamt']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total ",30).$orderpaymentamt['total']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Card tip ",30).$orderpaymentamt['tip_amount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance Amt(+) ",30).$orderpaymentamt['advance']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Grand Total",30).$orderpaymentamt['totalpaysumm']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);

			  	$printer->text("Online Payment Summary");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Swiggy ",30).$onlineorder['swiggy_total']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Zomato ",30).$onlineorder['zomato_total']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Online ",30).$onlineorder['online_total']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Uber ",30).$onlineorder['uber_total']);
			  	$printer->feed(1);
			  	
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Grand Total",30).$onlineorder['g_tot']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);

			  	$printer->text(str_pad("Tax Details:",20)."".str_pad("Net Amt",10)."".str_pad("Tax Amt", 10));
			  	$printer->feed(1);
			  	foreach($ordertaxamt['liqtax'] as $value){
			  		$printer->text(str_pad("Vat :",10)."".str_pad($value['tax1']." % On",10)."".str_pad($value['amt']." is", 10)."".$value['tax1_value']);
			  		$printer->feed(1);
			  	}
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Vat :",20)."".str_pad($ordertaxamt['liqnetamt'],10)."".$ordertaxamt['taxvalliq']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach($ordertaxamt['foodtax'] as $value){
			  		$printer->text(str_pad("Gst:",10)."".str_pad($value['tax1']." % On",10)."".str_pad($value['amt']." is", 10)."".$value['tax1_value']);
			  		$printer->feed(1);
			  	}
			  	$printer->feed(1);
			  	$printer->text(str_pad("CSGST ",30).$ordertaxamt['taxvalfood']/2);
			  	$printer->feed(1);
			  	$printer->text(str_pad("SSGST ",30).$ordertaxamt['taxvalfood']/2);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Gst :",20)."".str_pad($ordertaxamt['foodnetamt'],10)."".$ordertaxamt['taxvalfood']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text("Category");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Food ",30).$catsub['foodcat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Liquor ",30).$catsub['liqcat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total ",30).$catsub['foodcat'] + $catsub['liqcat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Discount ",30).$catsub['cat_fdis'] + $catsub['cat_ldis']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Category Net Total ",30).($catsub['foodcat'] + $catsub['liqcat']) - ($catsub['cat_fdis'] + $catsub['cat_ldis']));
			  	$printer->feed(1);
			  	$printer->text("Sub Category");
			  	$printer->feed(1);
			  	foreach($catsub['subcat'] as $key => $value) { 
					$printer->text(str_pad($value['name'],30).$value['amount']);
					$printer->feed(1);
				}
				$printer->text(str_pad("Total ",30).$catsub['totalamt']);
				$printer->feed(1);
				$printer->text(str_pad("Discount ",30).$catsub['discountamt']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Sub Category Net Total ",30).($catsub['totalamt'] - $catsub['discountamt']));
			  	$printer->feed(2);
			  	$printer->text(str_pad("Last Bill No ",30).$orderdatalastarray['orderdatalast']['lastbill']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Bill Printed ",30).$orderdatalastarray['orderdatalast']['bill_printed']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Last Kot No ",30).$orderdatalastarray['orderdatalast']['lastkot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Last Bot No ",30).$orderdatalastarray['orderdatalast']['lastbot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Unsettle Bill Amt ",20)."".str_pad($data['pendingtablecount'],10)."".$data['pendingtable_amt']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Duplicate Bill ",20)."".str_pad($orderdatalastarray['orderdatalast']['duplicatecount'],10)."".$orderdatalastarray['orderdatalast']['duplicate']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Pending Table ",20)."".str_pad($data['pendingtablecount'],10)."".$data['pendingtable_amt'],10);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Compliment Bill ",20)."".str_pad($orderdatalastarray['orderitemlast']['complimentarycount'],10)."".$orderdatalastarray['orderitemlast']['complimentarybill']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Bill ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelledcount'],10)."".$orderdatalastarray['orderitemlast']['cancelledbill']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Kot ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelkot'],10)."".$orderdatalastarray['orderitemlast']['cancelamountkot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Bot ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelbot'],10)."".$orderdatalastarray['orderitemlast']['cancelamountbot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("NC Kot ",20)."".str_pad($orderdatalastarray['orderdatalast']['totalnc'],10)."".$orderdatalastarray['orderdatalast']['totalamountnc']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Disc Before Bill ",20)."".str_pad("0.00",10)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Disc after Bill ",20)."".str_pad($orderdatalastarray['orderdatalast']['discountbillcount'],10)."".$orderdatalastarray['orderdatalast']['discountamount']);
			  	$printer->feed(2);
			  	$printer->text("CASH DROVEREST");
			  	$printer->feed(1);
			  	$printer->text("OPENING BALANCE");
			  	$printer->feed(1);
			  	$printer->text(str_pad("CASH SALE ",20)."".str_pad("0.00",10).$orderpaymentamt['totalcash']);
			  	$printer->feed(1);
			  	$printer->text("PAID IN ");
			  	$printer->feed(1);
			  	$printer->text("PAID OUT ");
			  	$printer->feed(1);
			  	$printer->text(str_pad("TOTAL BALANCE IN DROVER ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("TIP ",30).$data['tip']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("NO. OF PERSONS",30).$data['person']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Average Grand Total",30).$data['average_grand_total']);
			  	$printer->feed(2);
			  	$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}

	}
	public function pdfprints(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');
		$html ='';
		$url = '';

		

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_category'])){
			$filter_category = $this->request->post['filter_category'];
		} else {
			$filter_category = '99';
		}

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			/************************************** User Wise Start *************************************************/
			$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, oi.`bill_date`, is_liq, onac FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oi.`login_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderdata) {
				$orderdataval = array();
				$orderitemval = array();
				$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows;
				$netsale = 0;
				$pay_cash = 0;
				$msr =0;
				$pay_card = 0;
				$pay_online = 0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$duplicatebill = 0;
				$duplicatecount = 0;
				$advanceamount = 0;
				$cancelkot = 0;
				$cancelamountkot = 0;
				$cancelbot = 0;
				$cancelamountbot = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$cancelledbill = 0;
				$cancelledbillcount = 0;
				$complimentarybill = 0;
				$complimentarycount = 0;
				foreach ($orderdatatest as $key) {
					//$netsale =  round($netsale  + $key['grand_total'] );

					$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
					$pay_cash = $pay_cash + $key['pay_cash'];
					$msr =$msr + $key['msrcard'];
					$pay_card = $pay_card + $key['pay_card'];
					$pay_online = $pay_online + $key['pay_online'];
					$onac = $onac + $key['onac'];
					$mealpass = $mealpass + $key['mealpass'];
					$pass = $pass + $key['pass'];

					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingbill ++;
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountbillcount ++;
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					}

					if($key['duplicate'] == '1'){
						$duplicatecount ++;
						$duplicatebill = $duplicatebill + $key['grand_total'];
					}

					$advanceamount = $advanceamount + $key['advance_amount'];
				}

				$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info_report`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY `order_id`";
				$orderdatabill = $this->db->query($sql)->rows;
				//echo "<pre>";print_r($orderdatabill);exit;
				foreach ($orderdatabill as $key => $value) {
					if($value['cancel_status'] == '1'){
						$cancelledbillcount ++;
						$cancelledbill = $cancelledbill + $value['grand_total'];
					}

					if($value['complimentary_status'] == '1'){
						$complimentarycount ++;
						$complimentarybill = $complimentarybill + $value['grand_total'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";
				$orderdatatest = $this->db->query($sql)->rows; 
				foreach ($orderdatatest as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalnc ++;
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					}
				}

				$orderdataval = array(
					'netsale' => $netsale,
					'total_cash' => $pay_cash,
					'msr' => $msr,
					'total_card' => $pay_card,
					'total_pay_online' => $pay_online,
					'onac' => $onac,
					'total_mealpass' => $mealpass,
					'total_pass' => $pass,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'discountbillcount' => $discountbillcount,
					'discountamount' => $discountamount,
					'duplicate' => $duplicatebill,
					'duplicatecount' => $duplicatecount,
					'advanceamount' => $advanceamount,
					'cancelkot' => $cancelkot,
					'cancelamountkot' => $cancelamountkot,
					'cancelbot' => $cancelbot,
					'cancelamountbot' => $cancelamountbot,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'cancelledcount' => $cancelledbillcount,
					'cancelledbill' => $cancelledbill,
					'complimentarycount' => $complimentarycount,
					'complimentarybill' => $complimentarybill,
					'login_id' => $orderdata['login_id'],
					'login_name' => $orderdata['login_name'],
				);
				$orderdataarray[$orderdata['login_id']] = array(
					'orderdata' => $orderdataval,
				);
			}
			/************************************** User Wise End *************************************************/

			/************************************** Location Wise Start *************************************************/

			$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oit.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY location";
			$orderdatalocationwises = $this->db->query($sql)->rows; 
			$totalnetsale = 0;
			$totalgst = 0;
			$totalvat = 0;
			$totalroundoff = 0;
			$totalscharge = 0;
			$totaldiscount = 0;
			$nettotalamt = 0;
			$advance = 0;
			$totalpackaging_sgst = 0;
			$totalpackaging_cgst = 0;
			$totalpackaging = 0;

			foreach($orderdatalocationwises as $orderdata){
				$sql = "SELECT *, oi.stax as order_stax FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";
				//echo $sql;exit;
				$orderdatatestloc = $this->db->query($sql)->rows; 
				$netsale = 0;
				$vat = 0;
				$gst = 0;
				$roundoff = 0;
				$scharge = 0;
				$discount = 0;
				$nettotal = 0;
				$advance = 0;
				$packaging = 0;
				$packaging_cgst = 0;
				$packaging_sgst = 0;
				foreach ($orderdatatestloc as $key) {
					if($key['is_liq'] == '1' && $filter_category == '1'){
						$netsale = $netsale + $key['amt'];
						$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxliq'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					}elseif($key['is_liq'] == '0' && $filter_category == '0'){
						$netsale = $netsale + $key['amt'];
						$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxfood'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					} else{
						$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
						$vat = $vat + $key['vat'];
						$gst = $gst + $key['gst'];
						$roundoff = $roundoff + $key['roundtotal'];
						$scharge = $scharge + $key['order_stax'];
						$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$nettotal = $nettotal + $key['grand_total'] + ($key['roundtotal']);
						$advance = $advance + $key['advance_amount'];
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

					}
				}
				$orderlocdatas[] = array(
					'location' => $key['location'],
					'netsale' => $netsale,
				);
				$totalnetsale = $totalnetsale + $netsale;
				$totalgst = $totalgst + $gst;
				$totalvat = $totalvat + $vat;
				$totalroundoff = $totalroundoff + $roundoff;
				$totalscharge = $totalscharge + $scharge;
				$totaldiscount = $totaldiscount + $discount;
				$nettotalamt = $nettotalamt + $nettotal + $advance;

				$totalpackaging = $totalpackaging + $packaging;
				$totalpackaging_cgst = $totalpackaging_cgst + $packaging_cgst;
				$totalpackaging_sgst = $totalpackaging_sgst + $packaging_sgst;
			}

			$orderlocdatasamt = array(
				'totalnetsale' => $totalnetsale,
				'totalgst' => $totalgst,
				'totalvat' => $totalvat,
				'totalroundoff' => $totalroundoff,
				'totalscharge' => $totalscharge,
				'totaldiscount' => $totaldiscount,
				'nettotalamt' => $nettotalamt,
				'advance' => $advance,
				'totalpackaging' => $totalpackaging,
				'totalpackaging_cgst' => $totalpackaging_cgst,
				'totalpackaging_sgst' => $totalpackaging_sgst 
			);

			/************************************** Location Wise End *************************************************/

			/************************************** Payment Summary Start *************************************************/
			$totalcash = 0;
			$totalcard = 0;
			$totalpayonline =0;
			$onac = 0;
			$mealpass = 0;
			$pass = 0;
			$room = 0;
			$msr = 0;
			$total = 0;
			$cardtip = 0;
			$totalpaysumm = 0;
			$onac = 0;
			$advanceamt = 0;
			$tip_amount = 0;
			$persons = 0;
			$sql = "SELECT pay_cash,msrcard ,pay_card,pay_online, mealpass, pass, advance_amount, oit.`bill_date`, is_liq, onac, tip FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oit.`order_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderpayment) {
				$totalcash = $totalcash + $orderpayment['pay_cash'];
				$totalcard = $totalcard + $orderpayment['pay_card'];
				$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
				$onac = $onac + $orderpayment['onac'];
				$mealpass = $mealpass + $orderpayment['mealpass'];
				$pass = $pass + $orderpayment['pass'];
				$advanceamt = $advance + $orderpayment['advance_amount'];
				$tip_amount = round($tip_amount) + round($orderpayment['tip']);
				$room = 0.00;
				$msr =$msr + $orderpayment['msrcard'];
				$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
				$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$orderpaymentamt = array(
				'totalcash' => $totalcash,
				'totalcard' => $totalcard,
				'totalpayonline' => $totalpayonline,
				'onac' => $onac,
				'mealpass' => $mealpass,
				'pass' => $pass,
				'room' => $room,
				'msr' => $msr,
				'total' => $total,
				'tip_amount' => $tip_amount,
				'advanceamt' => $advanceamt,
				'advance' => $advancetotal,
				'totalpaysumm' => $totalpaysumm
			);

			/************************************** Payment Summary End *************************************************/

			/************************************** Online Payment Summary Start *************************************************/
			$onlineorder = array();
			$onlineorderamt = array();
			$swiggy_total = 0;
			$zomato_total = 0;
			$online_total = 0;
			$uber_total = 0;
			$g_tot = 0;
			$sql = "SELECT grand_total,order_from FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON  oi.`order_id` = oit.`order_id` WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`urbanpiper_order_id` <> ''  ";
			$sql .= " GROUP BY oit.`order_id`";	
			$online_orders = $this->db->query($sql)->rows;
			
			foreach ($online_orders as $online_order) {
				if($online_order['order_from'] == 'swiggy'){
					$swiggy_total = $swiggy_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'zomato'){
					$zomato_total = $zomato_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Online'){
					$online_total = $online_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Uber'){
					$uber_total = $uber_total + $online_order['grand_total'];
				}

				
			}
			$g_tot =  $swiggy_total + $zomato_total + $online_total + $uber_total;

			
			$onlineorder = array(
				'swiggy_total' => $swiggy_total,
				'zomato_total' => $zomato_total,
				'online_total' => $online_total,
				'uber_total'   => $uber_total,
				'g_tot' 	   => $g_tot
			);


			/************************************** Online Payment Summary *************************************************/


			/************************************** Tax Summary End *************************************************/
			// echo "<pre>";
			// print_r($orderdatauserwises);
			// exit();
			//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items_report` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
			$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`discount_value`) as discount_value, SUM(oit.`tax1_value`) as tax1_value, SUM(oit.`tax2_value`) as tax2_value, oit.`is_liq`, oit.`tax1`, oit.`tax2` FROM `oc_order_items_report` oit LEFT JOIN `oc_order_info_report` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY tax1, is_liq";
			//echo $sql;exit;
			$tests = $this->db->query($sql)->rows; 
			foreach($tests as $test){
				if($test['is_liq'] == '0'){
					if(isset($testfoods[$test['tax1'] + $test['tax2']]) ){
						if(isset($testfoods[$test['tax1'] + $test['tax2']]['tax1_value']) && $test['tax1_value'] > 0 ){
							
							$testfoods[$test['tax1'] + $test['tax2']] = array(
								'tax1' => $test['tax1'] + $test['tax2'],
								'amt' => $testfoods[$test['tax1'] + $test['tax2']]['amt'] + $test['amt'] - $test['discount_value'],
								'tax1_value' => $testfoods[$test['tax1'] + $test['tax2']]['tax1_value'] + $test['tax2_value'] + $test['tax1_value']
							);
						}
					} else {
						$testfoods[$test['tax1'] + $test['tax2']] = array(
							'tax1' => $test['tax1'] + $test['tax2'],
							'amt' => $test['amt'] - $test['discount_value'],
							'tax1_value' => $test['tax1_value']  + $test['tax2_value']
						);
					}
				} else{
					$testliqs[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				}
			}
			$foodnetamt = 0;
			$taxvalfood = 0;
			$liqnetamt = 0;
			$taxvalliq = 0;

			foreach($testfoods as $tkey => $tvalue){
				$foodarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$foodnetamt = $foodnetamt + $tvalue['amt'];
				$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
			}

			foreach($testliqs as $tkey => $tvalue){
				$liqarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$liqnetamt = $liqnetamt + $tvalue['amt'];
				$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
			}

			$ordertaxamt = array(
				'foodtax' => $foodarray,
				'liqtax' => $liqarray,
				'foodnetamt' => $foodnetamt,
				'taxvalfood' => $taxvalfood,
				'liqnetamt' => $liqnetamt,
				'taxvalliq' => $taxvalliq
			);
			
			// echo "<pre>";
			// print_r($ordertaxamt);
			// exit();
			/************************************** Tax Summary End *************************************************/

			/************************************** Category Start **************************************************/
			$foodamt = '0';
			$liqamt = '0';
			$disamt_food = 0;
			$foodtotaldis = 0;
			$liqtotaldis = 0;
			$disamt_liq = 0;

			if($filter_category == '0'){
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$foodamt = $this->db->query("SELECT SUM(oit.`amt`) as total_food,SUM(oit.`discount_value`) as discount_value, FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
				$disamt_food = $foodamt['discount_value'];
			}
			if($filter_category == '1'){
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$liqamt = $liqamt['total_liq'];
				$disamt_liq = $liqamt['discount_value'];
			}
			if($filter_category == '99'){
				$foodamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_food FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as ldiscount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				// echo'<pre>';
				// print_r($foodamt['fdiscount_value']);
				// exit;
				$foodtotaldis = $foodamt['discount_value'];
				$liqtotaldis = $liqamt['ldiscount_value'];
				$foodamt = $foodamt['total_food'];
				$liqamt = $liqamt['total_liq'];

				
				
			}

			/************************************** Category End **************************************************/

			/************************************** Sub Category Start **************************************************/

			$sql = "SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY subcategoryid";
			//echo $sql;exit;
			$subcategoryamts = $this->db->query($sql)->rows; 
			// echo'<pre>';
			// print_r($subcategoryamts);
			// exit;
			$totalamt = 0;
			$discountamt = 0;
			$subcategory = array();
			foreach($subcategoryamts as $key){
				$subcategoryname = $this->db->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'")->row;
				$subcategory[] = array(
					'name' => $subcategoryname['name'],
					'amount' =>  $key['amt']
				);
				$totalamt = $totalamt + $key['amt'];
				$discountamt = $discountamt + $key['discount_value'];
			}
			
			$catsub = array(
				
				'foodcat' => $foodamt,
				'liqcat' => $liqamt,
				'cat_fdis' => $foodtotaldis,
				'cat_ldis' => $liqtotaldis,
				'subcat' => $subcategory,
				'disamt_food' => $disamt_food,
				'disamt_liq' => $disamt_liq,
				'totalamt' => $totalamt,
				'discountamt' => $discountamt,
			);

			// echo'<pre>';
			// print_r($catsub);
			// exit;

			/************************************** Sub Category End **************************************************/

			/************************************** Second last part Start **************************************************/
			
			$sql = "SELECT billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY billno DESC LIMIT 1";
			$lastbillno = $this->db->query($sql); 

			if($lastbillno->num_rows > 0){
					$lastbillno = $lastbillno->row['billno'];			
			} else{
				$lastbillno = 0;			
			}
			// echo'<pre>';
			// print_r($lastbillno);
			// exit;

			$lastkotno = 0;
			$lastbotno = 0;

			if($filter_category == '0'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` LEFT JOIN `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];
				} else{
					$lastkotno = 0;
				}
			}
			if($filter_category == '1'){
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];
				} else{
					$lastbotno = 0;				
				}
			}
			if($filter_category == '99'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];			
				} else{
					$lastkotno = 0;			
				}
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];			
				} else{
					$lastbotno = 0;			
				}
			}

			$sql = "SELECT *,oi.`order_id` FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
			$orderdatalast = $this->db->query($sql)->rows; 
			$totalpendingamount = 0;
			$totalpendingbill = 0;
			$duplicate = 0;
			$duplicatecount = 0;
			$totalamountnc = 0;
			$totalnc = 0;
			$discountamount = 0;
			$discountbillcount = 0;
			$advanceamount = 0;
			$billmodify = 0;
			$qty = 0;
			$cancelamountkot = 0;
			$cancelamountbot = 0;
			$cancelkot = 0;
			$cancelbot = 0;
			foreach ($orderdatalast as $key) {
				if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
					$totalpendingamount = $totalpendingamount + $key['grand_total'];
					$totalpendingbill ++;
				}

				if($key['duplicate'] == '1'){
					$duplicate = $duplicate + $key['duplicate'];
					$duplicatecount ++;
				}

				if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
					$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					$discountbillcount ++;
				}

				if($key['advance_amount'] != '0'){
					$advanceamount = $advanceamount + $key['advance_amount'];
				}
			}

			$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`)  WHERE  oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY oi.`order_id`";	
			$orderitemlast = $this->db->query($sql)->rows;
			foreach ($orderitemlast as $key) {
				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
					$cancelamountkot = $cancelamountkot + $key['amt'];
					$cancelkot ++;
				}

				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
					$cancelamountbot = $cancelamountbot + $key['amt'];
					$cancelbot ++;
				}

				if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
					$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					$totalnc ++;
				}
			}
			
			$cancelledbillcount = 0;
			$cancelledbill = 0;
			$sql = "SELECT count(*) as row_count,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1'  AND `cancel_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " ORDER BY oit.`order_id` ";
			//echo $sql;exit;
			$orderdatabill = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatabill);exit;
			if ($orderdatabill->num_rows > 0) {
				if ($orderdatabill->row['grand_total'] > '0') {
					$cancelledbillcount = $orderdatabill->row['row_count'];
					//echo $cancelledbillcount;exit;
				}
				$cancelledbill = $orderdatabill->row['grand_total'];
			}
			

			$complimentarycount = 0;
			$complimentarybill = 0;
			$sql = "SELECT count(*) as row_count ,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatacomp = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatacomp);exit;
			if ($orderdatacomp->num_rows > 0) {
				if ($orderdatacomp->row['grand_total'] > 0) {
					$complimentarycount = $orderdatacomp->row['row_count'];
				}
				$complimentarybill = $orderdatacomp->row['grand_total'];
			}

			$orderitemlastval = array(
				'cancelamountkot' => $cancelamountkot,
				'cancelamountbot' => $cancelamountbot,
				'cancelkot' => $cancelkot,
				'cancelbot' => $cancelbot,
				'cancelledbill' => $cancelledbill,
				'cancelledcount' => $cancelledbillcount,
				'complimentarybill' => $complimentarybill,
				'complimentarycount' => $complimentarycount,
			);

			$pendingtable = 0;
			$pending_table = $this->db->query("SELECT count(*) as pending_table FROM oc_order_info_report WHERE  bill_status = '0'AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($pending_table->num_rows > 0) {
				$pendingtable = $pending_table->row['pending_table'];
			}
			
			$bill_printed = 0;
			$bill_count = $this->db->query("SELECT count(*) as bill_printed FROM oc_order_info_report WHERE  bill_status = '1' AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($bill_count->num_rows > 0) {
				$bill_printed = $bill_count->row['bill_printed'];
			}

			$paid_out = 0;
			$paid_outs = $this->db->query("SELECT SUM(amount) as paid_out FROM oc_expense_trans WHERE  payment_type = 'PAID OUT' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_outs->num_rows > 0) {
				$paid_out = $paid_outs->row['paid_out'];
			}

			$paid_in = 0;
			$paid_inss = $this->db->query("SELECT SUM(amount) as paid_in FROM oc_expense_trans WHERE  payment_type = 'PAID IN' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_inss->num_rows > 0) {
				$paid_in = $paid_inss->row['paid_in'];
			}


			$orderdatalastval = array(
				'lastbill' => $lastbillno,
				'lastkot' => $lastkotno,
				'lastbot' => $lastbotno,
				'paid_out' => $paid_out,
				'paid_in' => $paid_in,
				'pendingbill' => $totalpendingbill,
				'pendingtable' => $pendingtable,
				'bill_printed' => $bill_printed,
				'pendingamount' => $totalpendingamount,
				'duplicate' => $duplicate,
				'duplicatecount' => $duplicatecount,
				'totalnc' => $totalnc,
				'totalamountnc' => $totalamountnc,
				'billmodify' => $billmodify,
				'discountamount' => $discountamount,
				'discountbillcount' => $discountbillcount,
				'advanceamount' => $advanceamount
			);

			$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info_report oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql1 .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$bill_modifiy_data = $this->db->query($sql1);
			$data['NumberOfbillmodify'] = 0;
			$data['totalbillamount'] = 0;
			if($bill_modifiy_data->num_rows > 0){
				$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
				$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
				
			}
			$orderdatalastarray = array(
				'orderdatalast' => $orderdatalastval,
				'orderitemlast' => $orderitemlastval
			);
			/************************************** Second last part End **************************************************/
			$tip = $this->db->query("SELECT SUM(tip) as tip FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($tip->num_rows > 0){
				$data['tip'] = $tip->row['tip'];
			} else{
				$data['tip'] = 0;
			}

			$persons = $this->db->query("SELECT SUM(person) as person , SUM(grand_total) as g_total FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			// if($persons->num_rows > 0){
			// 	$data['person'] = $persons->row['person'];
			// 	$data['average_grand_total'] = $persons->row['g_total']/ $persons->row['person'];

			// } else{
			// 	$data['person'] = 0;
			// 	$data['average_grand_total'] = 0;
			// }

			if($persons->num_rows > 0){
				$data['person'] = $persons->row['person'];
				if($persons->row['person'] > 0){
					$per = $persons->row['person'];
					$data['average_grand_total'] =  $persons->row['person'] / $persons->row['g_total'];
				} else {
					$per = 0;
					$data['average_grand_total'] =  0;

				}

			} else{
				$data['person'] = 0;
				$data['average_grand_total'] = 0;
			}

			$pending_datas = $this->db->query("SELECT sum(grand_total) as pending_amt , count(*) as pendingcount FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND (`bill_status` = '0' OR `bill_status` = '1') AND `pay_method` = '0' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			if($pending_datas->num_rows > 0){
				$data['pendingtable_amt'] = $pending_datas->row['pending_amt'];
				$data['pendingtablecount'] = $pending_datas->row['pendingcount'];

			} else{
				$data['pendingtable_amt'] = 0;
				$data['pendingtablecount'] = 0;
			}

		}
		
		$data['orderdataarrays'] = $orderdataarray; // user wise
		$data['orderlocdatas'] = $orderlocdatas; // location wise
		$data['orderlocdatasamt'] = $orderlocdatasamt; // all total amt location wise
		$data['orderpaymentamt'] = $orderpaymentamt; // all payment details
		$data['ordertaxamt'] = $ordertaxamt; // all tax details
		$data['onlineorderamt'] = $onlineorder; // all payment online details

		$data['catsubs'] = $catsub; //category and subcategory
		$data['orderdatalastarray'] = $orderdatalastarray;
		// echo "<pre>";
		// print_r($orderdatalastarray);
		// exit();

		$forprintarray = array(
			'orderdataarray' => $orderdataarray,
			'orderlocdatas' => $orderlocdatas,
			'orderlocdatasamt' => $orderlocdatasamt,
			'orderpaymentamt' => $orderpaymentamt,
			'ordertaxamt' => $ordertaxamt,
		);

		$data['forprintarray'] = json_encode($forprintarray);

		$data['categorys'] = array(
								'0' => 'Food',
								'1' => 'Liquor'
							);
		$data['filter_category'] = $filter_category;
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		//$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
	
			/************************************** Second last part End **************************************************/
		// echo '<pre>';
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/order_invoice_html', $data);
		
		//echo $html;exit;

		$filename = 'DaysummaryReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');

		$filename = 'DaysummaryReport';
		define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $dompdf->stream($filename);
		echo $html;exit;
	}

	public function newbtn(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');

		$cat_datas = $this->db->query("SELECT item_category_id FROM oc_item")->rows;
		foreach ($cat_datas as $skey => $svalue) {
			$cat_name = $this->db->query("SELECT category_id,category FROM oc_category WHERE category_id = '".$svalue['item_category_id']."' ")->row;
			$sql_update = "UPDATE `oc_item` SET `department` = '".$cat_name['category']."' WHERE `item_category_id` = '".$cat_name['category_id']."' ";
			$this->db->query($sql_update);
		}
		// echo'<pre>';
		// print_r($cat_name);
		// exit;
	}

	public function sendmail(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');
		$html ='';
		$url = '';

		

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_category'])){
			$filter_category = $this->request->post['filter_category'];
		} else {
			$filter_category = '99';
		}

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			/************************************** User Wise Start *************************************************/
			$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, oi.`bill_date`, is_liq, onac FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oi.`login_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderdata) {
				$orderdataval = array();
				$orderitemval = array();
				$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows;
				$netsale = 0;
				$pay_cash = 0;
				$msr =0;
				$pay_card = 0;
				$pay_online = 0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$duplicatebill = 0;
				$duplicatecount = 0;
				$advanceamount = 0;
				$cancelkot = 0;
				$cancelamountkot = 0;
				$cancelbot = 0;
				$cancelamountbot = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$cancelledbill = 0;
				$cancelledbillcount = 0;
				$complimentarybill = 0;
				$complimentarycount = 0;
				foreach ($orderdatatest as $key) {
					//$netsale =  round($netsale  + $key['grand_total'] );

					$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
					$pay_cash = $pay_cash + $key['pay_cash'];
					$msr =$msr + $key['msrcard'];
					$pay_card = $pay_card + $key['pay_card'];
					$pay_online = $pay_online + $key['pay_online'];
					$onac = $onac + $key['onac'];
					$mealpass = $mealpass + $key['mealpass'];
					$pass = $pass + $key['pass'];

					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingbill ++;
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountbillcount ++;
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					}

					if($key['duplicate'] == '1'){
						$duplicatecount ++;
						$duplicatebill = $duplicatebill + $key['grand_total'];
					}

					$advanceamount = $advanceamount + $key['advance_amount'];
				}

				$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info_report`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY `order_id`";
				$orderdatabill = $this->db->query($sql)->rows;
				//echo "<pre>";print_r($orderdatabill);exit;
				foreach ($orderdatabill as $key => $value) {
					if($value['cancel_status'] == '1'){
						$cancelledbillcount ++;
						$cancelledbill = $cancelledbill + $value['grand_total'];
					}

					if($value['complimentary_status'] == '1'){
						$complimentarycount ++;
						$complimentarybill = $complimentarybill + $value['grand_total'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";
				$orderdatatest = $this->db->query($sql)->rows; 
				foreach ($orderdatatest as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalnc ++;
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					}
				}

				$orderdataval = array(
					'netsale' => $netsale,
					'total_cash' => $pay_cash,
					'msr' => $msr,
					'total_card' => $pay_card,
					'total_pay_online' => $pay_online,
					'onac' => $onac,
					'total_mealpass' => $mealpass,
					'total_pass' => $pass,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'discountbillcount' => $discountbillcount,
					'discountamount' => $discountamount,
					'duplicate' => $duplicatebill,
					'duplicatecount' => $duplicatecount,
					'advanceamount' => $advanceamount,
					'cancelkot' => $cancelkot,
					'cancelamountkot' => $cancelamountkot,
					'cancelbot' => $cancelbot,
					'cancelamountbot' => $cancelamountbot,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'cancelledcount' => $cancelledbillcount,
					'cancelledbill' => $cancelledbill,
					'complimentarycount' => $complimentarycount,
					'complimentarybill' => $complimentarybill,
					'login_id' => $orderdata['login_id'],
					'login_name' => $orderdata['login_name'],
				);
				$orderdataarray[$orderdata['login_id']] = array(
					'orderdata' => $orderdataval,
				);
			}
			/************************************** User Wise End *************************************************/

				/************************************** Location Wise Start *************************************************/

			$sql = "SELECT * FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oit.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY location";
			$orderdatalocationwises = $this->db->query($sql)->rows; 
			$totalnetsale = 0;
			$totalgst = 0;
			$totalvat = 0;
			$totalroundoff = 0;
			$totalscharge = 0;
			$totaldiscount = 0;
			$nettotalamt = 0;
			$advance = 0;
			$totalpackaging_sgst = 0;
			$totalpackaging_cgst = 0;
			$totalpackaging = 0;

			foreach($orderdatalocationwises as $orderdata){
				$sql = "SELECT *, oi.stax as order_stax FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";
				//echo $sql;exit;
				$orderdatatestloc = $this->db->query($sql)->rows; 
				$netsale = 0;
				$vat = 0;
				$gst = 0;
				$roundoff = 0;
				$scharge = 0;
				$discount = 0;
				$nettotal = 0;
				$advance = 0;
				$packaging = 0;
				$packaging_cgst = 0;
				$packaging_sgst = 0;
				foreach ($orderdatatestloc as $key) {
					if($key['is_liq'] == '1' && $filter_category == '1'){
						$netsale = $netsale + $key['amt'];
						$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxliq'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					}elseif($key['is_liq'] == '0' && $filter_category == '0'){
						$netsale = $netsale + $key['amt'];
						$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxfood'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
						}
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

						$advance = $advance + $key['advance_amount'];
					} else{
						$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
						$vat = $vat + $key['vat'];
						$gst = $gst + $key['gst'];
						$roundoff = $roundoff + $key['roundtotal'];
						$scharge = $scharge + $key['order_stax'];
						$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$nettotal = $nettotal + $key['grand_total'] + ($key['roundtotal']);
						$advance = $advance + $key['advance_amount'];
						$packaging = $packaging + $key['packaging'];
						$packaging_cgst = $packaging_cgst + $key['packaging_cgst'];
						$packaging_sgst = $packaging_sgst + $key['packaging_sgst'];

					}
				}
				$orderlocdatas[] = array(
					'location' => $key['location'],
					'netsale' => $netsale,
				);
				$totalnetsale = $totalnetsale + $netsale;
				$totalgst = $totalgst + $gst;
				$totalvat = $totalvat + $vat;
				$totalroundoff = $totalroundoff + $roundoff;
				$totalscharge = $totalscharge + $scharge;
				$totaldiscount = $totaldiscount + $discount;
				$nettotalamt = $nettotalamt + $nettotal + $advance;

				$totalpackaging = $totalpackaging + $packaging;
				$totalpackaging_cgst = $totalpackaging_cgst + $packaging_cgst;
				$totalpackaging_sgst = $totalpackaging_sgst + $packaging_sgst;
			}

			$orderlocdatasamt = array(
				'totalnetsale' => $totalnetsale,
				'totalgst' => $totalgst,
				'totalvat' => $totalvat,
				'totalroundoff' => $totalroundoff,
				'totalscharge' => $totalscharge,
				'totaldiscount' => $totaldiscount,
				'nettotalamt' => $nettotalamt,
				'advance' => $advance,
				'totalpackaging' => $totalpackaging,
				'totalpackaging_cgst' => $totalpackaging_cgst,
				'totalpackaging_sgst' => $totalpackaging_sgst 
			);

			/************************************** Location Wise End *************************************************/

			/************************************** Payment Summary Start *************************************************/
			$totalcash = 0;
			$totalcard = 0;
			$totalpayonline =0;
			$onac = 0;
			$mealpass = 0;
			$pass = 0;
			$room = 0;
			$msr = 0;
			$total = 0;
			$cardtip = 0;
			$totalpaysumm = 0;
			$onac = 0;
			$advanceamt = 0;
			$tip_amount = 0;
			$persons = 0;
			$sql = "SELECT pay_cash,msrcard ,pay_card,pay_online, mealpass, pass, advance_amount, oit.`bill_date`, is_liq, onac, tip FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oit.`order_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderpayment) {
				$totalcash = $totalcash + $orderpayment['pay_cash'];
				$totalcard = $totalcard + $orderpayment['pay_card'];
				$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
				$onac = $onac + $orderpayment['onac'];
				$mealpass = $mealpass + $orderpayment['mealpass'];
				$pass = $pass + $orderpayment['pass'];
				$advanceamt = $advance + $orderpayment['advance_amount'];
				$tip_amount = round($tip_amount) + round($orderpayment['tip']);
				$room = 0.00;
				$msr =$msr + $orderpayment['msrcard'];
				$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
				$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$orderpaymentamt = array(
				'totalcash' => $totalcash,
				'totalcard' => $totalcard,
				'totalpayonline' => $totalpayonline,
				'onac' => $onac,
				'mealpass' => $mealpass,
				'pass' => $pass,
				'room' => $room,
				'msr' => $msr,
				'total' => $total,
				'tip_amount' => $tip_amount,
				'advanceamt' => $advanceamt,
				'advance' => $advancetotal,
				'totalpaysumm' => $totalpaysumm
			);

			/************************************** Payment Summary End *************************************************/

			/************************************** Online Payment Summary Start *************************************************/
			$onlineorder = array();
			$onlineorderamt = array();
			$swiggy_total = 0;
			$zomato_total = 0;
			$online_total = 0;
			$uber_total = 0;
			$g_tot = 0;
			$sql = "SELECT grand_total,order_from FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON  oi.`order_id` = oit.`order_id` WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`urbanpiper_order_id` <> ''  ";
			$sql .= " GROUP BY oit.`order_id`";	
			$online_orders = $this->db->query($sql)->rows;
			
			foreach ($online_orders as $online_order) {
				if($online_order['order_from'] == 'swiggy'){
					$swiggy_total = $swiggy_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'zomato'){
					$zomato_total = $zomato_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Online'){
					$online_total = $online_total + $online_order['grand_total'];
				} elseif($online_order['order_from'] == 'Uber'){
					$uber_total = $uber_total + $online_order['grand_total'];
				}

				
			}
			$g_tot =  $swiggy_total + $zomato_total + $online_total + $uber_total;

			
			$onlineorder = array(
				'swiggy_total' => $swiggy_total,
				'zomato_total' => $zomato_total,
				'online_total' => $online_total,
				'uber_total'   => $uber_total,
				'g_tot' 	   => $g_tot
			);


			/************************************** Online Payment Summary *************************************************/

			/************************************** Tax Summary End *************************************************/
			// echo "<pre>";
			// print_r($orderdatauserwises);
			// exit();
			//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items_report` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
			$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`discount_value`) as discount_value, SUM(oit.`tax1_value`) as tax1_value, oit.`is_liq`, oit.`tax1` FROM `oc_order_items_report` oit LEFT JOIN `oc_order_info_report` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY tax1, is_liq";
			//echo $sql;exit;
			$tests = $this->db->query($sql)->rows; 
			foreach($tests as $test){
				if($test['is_liq'] == '0'){
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				} else{
					$testliqs[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'] - $test['discount_value'],
						'tax1_value' => $test['tax1_value']
					);
				}
			}
			$foodnetamt = 0;
			$taxvalfood = 0;
			$liqnetamt = 0;
			$taxvalliq = 0;

			foreach($testfoods as $tkey => $tvalue){
				$foodarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$foodnetamt = $foodnetamt + $tvalue['amt'];
				$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
			}

			foreach($testliqs as $tkey => $tvalue){
				$liqarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$liqnetamt = $liqnetamt + $tvalue['amt'];
				$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
			}

			$ordertaxamt = array(
				'foodtax' => $foodarray,
				'liqtax' => $liqarray,
				'foodnetamt' => $foodnetamt,
				'taxvalfood' => $taxvalfood,
				'liqnetamt' => $liqnetamt,
				'taxvalliq' => $taxvalliq
			);
			
			// echo "<pre>";
			// print_r($ordertaxamt);
			// exit();
			/************************************** Tax Summary End *************************************************/

			/************************************** Category Start **************************************************/
			$foodamt = '0';
			$liqamt = '0';
			$disamt_food = 0;
			$foodtotaldis = 0;
			$liqtotaldis = 0;
			$disamt_liq = 0;

			if($filter_category == '0'){
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$foodamt = $this->db->query("SELECT SUM(oit.`amt`) as total_food,SUM(oit.`discount_value`) as discount_value, FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
				$disamt_food = $foodamt['discount_value'];
			}
			if($filter_category == '1'){
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				$liqamt = $liqamt['total_liq'];
				$disamt_liq = $liqamt['discount_value'];
			}
			if($filter_category == '99'){
				$foodamt = $this->db->query("SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_food FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ")->row;
				//$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				
				$liqamt = $this->db->query("SELECT SUM(oit.`discount_value`) as ldiscount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info_report oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ")->row;
				//$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info_report WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ")->row;
				// echo'<pre>';
				// print_r($foodamt['fdiscount_value']);
				// exit;
				$foodtotaldis = $foodamt['discount_value'];
				$liqtotaldis = $liqamt['ldiscount_value'];
				$foodamt = $foodamt['total_food'];
				$liqamt = $liqamt['total_liq'];

				
				
			}

			/************************************** Category End **************************************************/

			/************************************** Sub Category Start **************************************************/

			$sql = "SELECT SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY subcategoryid";
			//echo $sql;exit;
			$subcategoryamts = $this->db->query($sql)->rows; 
			// echo'<pre>';
			// print_r($subcategoryamts);
			// exit;
			$totalamt = 0;
			$discountamt = 0;
			$subcategory = array();
			foreach($subcategoryamts as $key){
				$subcategoryname = $this->db->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'")->row;
				$subcategory[] = array(
					'name' => $subcategoryname['name'],
					'amount' =>  $key['amt']
				);
				$totalamt = $totalamt + $key['amt'];
				$discountamt = $discountamt + $key['discount_value'];
			}
			
			$catsub = array(
				
				'foodcat' => $foodamt,
				'liqcat' => $liqamt,
				'cat_fdis' => $foodtotaldis,
				'cat_ldis' => $liqtotaldis,
				'subcat' => $subcategory,
				'disamt_food' => $disamt_food,
				'disamt_liq' => $disamt_liq,
				'totalamt' => $totalamt,
				'discountamt' => $discountamt,
			);

			// echo'<pre>';
			// print_r($catsub);
			// exit;

			/************************************** Sub Category End **************************************************/

			/************************************** Second last part Start **************************************************/
			
			$sql = "SELECT billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY billno DESC LIMIT 1";
			$lastbillno = $this->db->query($sql); 

			if($lastbillno->num_rows > 0){
					$lastbillno = $lastbillno->row['billno'];			
			} else{
				$lastbillno = 0;			
			}
			// echo'<pre>';
			// print_r($lastbillno);
			// exit;

			$lastkotno = 0;
			$lastbotno = 0;

			if($filter_category == '0'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` LEFT JOIN `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];
				} else{
					$lastkotno = 0;
				}
			}
			if($filter_category == '1'){
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];
				} else{
					$lastbotno = 0;				
				}
			}
			if($filter_category == '99'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items_report` oit WHERE oit.`bill_date` >= '".$start_date."' AND oit.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];			
				} else{
					$lastkotno = 0;			
				}
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];			
				} else{
					$lastbotno = 0;			
				}
			}

			$sql = "SELECT *,oi.`order_id` FROM oc_order_info_report oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oi.`complimentary_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
			$orderdatalast = $this->db->query($sql)->rows; 
			$totalpendingamount = 0;
			$totalpendingbill = 0;
			$duplicate = 0;
			$duplicatecount = 0;
			$totalamountnc = 0;
			$totalnc = 0;
			$discountamount = 0;
			$discountbillcount = 0;
			$advanceamount = 0;
			$billmodify = 0;
			$qty = 0;
			$cancelamountkot = 0;
			$cancelamountbot = 0;
			$cancelkot = 0;
			$cancelbot = 0;
			foreach ($orderdatalast as $key) {
				if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
					$totalpendingamount = $totalpendingamount + $key['grand_total'];
					$totalpendingbill ++;
				}

				if($key['duplicate'] == '1'){
					$duplicate = $duplicate + $key['duplicate'];
					$duplicatecount ++;
				}

				if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
					$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					$discountbillcount ++;
				}

				if($key['advance_amount'] != '0'){
					$advanceamount = $advanceamount + $key['advance_amount'];
				}
			}

			$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON (oi.`order_id` = oit.`order_id`)  WHERE  oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY oi.`order_id`";	
			$orderitemlast = $this->db->query($sql)->rows;
			foreach ($orderitemlast as $key) {
				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
					$cancelamountkot = $cancelamountkot + $key['amt'];
					$cancelkot ++;
				}

				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
					$cancelamountbot = $cancelamountbot + $key['amt'];
					$cancelbot ++;
				}

				if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
					$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					$totalnc ++;
				}
			}
			
			$cancelledbillcount = 0;
			$cancelledbill = 0;
			$sql = "SELECT count(*) as row_count,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1'  AND `cancel_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " ORDER BY oit.`order_id` ";
			//echo $sql;exit;
			$orderdatabill = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatabill);exit;
			if ($orderdatabill->num_rows > 0) {
				if ($orderdatabill->row['grand_total'] > '0') {
					$cancelledbillcount = $orderdatabill->row['row_count'];
					//echo $cancelledbillcount;exit;
				}
				$cancelledbill = $orderdatabill->row['grand_total'];
			}
			

			$complimentarycount = 0;
			$complimentarybill = 0;
			$sql = "SELECT count(*) as row_count ,SUM(oi.`grand_total`) as grand_total FROM `oc_order_info_report` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatacomp = $this->db->query($sql);
			//echo "<pre>";print_r($orderdatacomp);exit;
			if ($orderdatacomp->num_rows > 0) {
				if ($orderdatacomp->row['grand_total'] > 0) {
					$complimentarycount = $orderdatacomp->row['row_count'];
				}
				$complimentarybill = $orderdatacomp->row['grand_total'];
			}

			$orderitemlastval = array(
				'cancelamountkot' => $cancelamountkot,
				'cancelamountbot' => $cancelamountbot,
				'cancelkot' => $cancelkot,
				'cancelbot' => $cancelbot,
				'cancelledbill' => $cancelledbill,
				'cancelledcount' => $cancelledbillcount,
				'complimentarybill' => $complimentarybill,
				'complimentarycount' => $complimentarycount,
			);

			$pendingtable = 0;
			$pending_table = $this->db->query("SELECT count(*) as pending_table FROM oc_order_info_report WHERE  bill_status = '0'AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($pending_table->num_rows > 0) {
				$pendingtable = $pending_table->row['pending_table'];
			}
			
			$bill_printed = 0;
			$bill_count = $this->db->query("SELECT count(*) as bill_printed FROM oc_order_info_report WHERE  bill_status = '1' AND cancel_status = '0' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ");
			if ($bill_count->num_rows > 0) {
				$bill_printed = $bill_count->row['bill_printed'];
			}

			$paid_out = 0;
			$paid_outs = $this->db->query("SELECT SUM(amount) as paid_out FROM oc_expense_trans WHERE  payment_type = 'PAID OUT' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_outs->num_rows > 0) {
				$paid_out = $paid_outs->row['paid_out'];
			}

			$paid_in = 0;
			$paid_inss = $this->db->query("SELECT SUM(amount) as paid_in FROM oc_expense_trans WHERE  payment_type = 'PAID IN' AND `date` >= '".$start_date."' AND `date`<= '".$end_date."' ");
			if ($paid_inss->num_rows > 0) {
				$paid_in = $paid_inss->row['paid_in'];
			}


			$orderdatalastval = array(
				'lastbill' => $lastbillno,
				'lastkot' => $lastkotno,
				'lastbot' => $lastbotno,
				'paid_out' => $paid_out,
				'paid_in' => $paid_in,
				'pendingbill' => $totalpendingbill,
				'pendingtable' => $pendingtable,
				'bill_printed' => $bill_printed,
				'pendingamount' => $totalpendingamount,
				'duplicate' => $duplicate,
				'duplicatecount' => $duplicatecount,
				'totalnc' => $totalnc,
				'totalamountnc' => $totalamountnc,
				'billmodify' => $billmodify,
				'discountamount' => $discountamount,
				'discountbillcount' => $discountbillcount,
				'advanceamount' => $advanceamount
			);

			$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info_report oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql1 .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$bill_modifiy_data = $this->db->query($sql1);
			$data['NumberOfbillmodify'] = 0;
			$data['totalbillamount'] = 0;
			if($bill_modifiy_data->num_rows > 0){
				$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
				$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
				
			}
			$orderdatalastarray = array(
				'orderdatalast' => $orderdatalastval,
				'orderitemlast' => $orderitemlastval
			);
			/************************************** Second last part End **************************************************/
			$tip = $this->db->query("SELECT SUM(tip) as tip FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($tip->num_rows > 0){
				$data['tip'] = $tip->row['tip'];
			} else{
				$data['tip'] = 0;
			}

			$persons = $this->db->query("SELECT SUM(person) as person , SUM(grand_total) as g_total FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			// if($persons->num_rows > 0){
			// 	$data['person'] = $persons->row['person'];
			// 	$data['average_grand_total'] = $persons->row['g_total']/ $persons->row['person'];

			// } else{
			// 	$data['person'] = 0;
			// 	$data['average_grand_total'] = 0;
			// }

			if($persons->num_rows > 0){
				$data['person'] = $persons->row['person'];
				if($persons->row['person'] > 0){
					$per = $persons->row['person'];
					$data['average_grand_total'] =  $persons->row['person'] / $persons->row['g_total'];
				} else {
					$per = 0;
					$data['average_grand_total'] =  0;

				}

			} else{
				$data['person'] = 0;
				$data['average_grand_total'] = 0;
			}

			$pending_datas = $this->db->query("SELECT sum(grand_total) as pending_amt , count(*) as pendingcount FROM `oc_order_info_report` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND (`bill_status` = '0' OR `bill_status` = '1') AND `pay_method` = '0' AND `cancel_status` = '0'  AND `complimentary_status` = '0' ");
			if($pending_datas->num_rows > 0){
				$data['pendingtable_amt'] = $pending_datas->row['pending_amt'];
				$data['pendingtablecount'] = $pending_datas->row['pendingcount'];

			} else{
				$data['pendingtable_amt'] = 0;
				$data['pendingtablecount'] = 0;
			}

		}
		
		$data['orderdataarrays'] = $orderdataarray; // user wise
		$data['orderlocdatas'] = $orderlocdatas; // location wise
		$data['orderlocdatasamt'] = $orderlocdatasamt; // all total amt location wise
		$data['orderpaymentamt'] = $orderpaymentamt; // all payment details
		$data['ordertaxamt'] = $ordertaxamt; // all tax details
		$data['catsubs'] = $catsub; //category and subcategory
		$data['orderdatalastarray'] = $orderdatalastarray;
		$data['onlineorderamt'] = $onlineorder; // all payment online details


		$forprintarray = array(
			'orderdataarray' => $orderdataarray,
			'orderlocdatas' => $orderlocdatas,
			'orderlocdatasamt' => $orderlocdatasamt,
			'orderpaymentamt' => $orderpaymentamt,
			'ordertaxamt' => $ordertaxamt,
		);

		$data['forprintarray'] = json_encode($forprintarray);

		$data['categorys'] = array(
								'0' => 'Food',
								'1' => 'Liquor'
							);
		$data['filter_category'] = $filter_category;
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		//$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
	
			/************************************** Second last part End **************************************************/
		// echo '<pre>';
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/order_invoice_html', $data);
		
		//echo $html;exit;

		$filename = 'DaysummaryReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');


		$filename = 'Day Summary Report';
		define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $output = $dompdf->output();
	    $date = date('Y-m-d');
	    $backup_name = DIR_DOWNLOAD."DaysummaryReport.pdf";
	    file_put_contents($backup_name, $output);

	    $filename = "DaysummaryReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;

	 	$to_emails = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT');
	 	$password = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT_PASSWORD');
	 	$username = $this->model_catalog_order->get_settings('EMAIL_USERNAME');

		
		$mail = new PHPMailer();
		$message = "Day Summary Report";
		$subject = "Day Summary Report";


		// $to_emails = 'myadav00349@gmail.com';
		// $from_email = 'report@loyalrisk.in';
		// $password = '2020loyalrisk';

		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
			$to_emails_array[]= $to_emails;
			$to_emails_array[]= $to_emails;
		} else {
			$to_emails_array[] = $to_emails; 
			$to_emails_array[]= $to_emails;
		}

		$date = date('Y-m-d');
		$filename = "DaysummaryReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;
		
		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = $username;//$this->config->get('config_smtp_username');
		$mail->Password = $password;//$this->config->get('config_smtp_password');
		// $mail->Username = 'report@loyalrisk.in';//$this->config->get('config_smtp_username');
		// $mail->Password = '2020loyalrisk';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($username, 'Hotel');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			//$to_name = 'Aaron Fargose';
			$mail->AddAddress($evalue);
		}
		
		if($mail->Send()) {
			//echo"send";
			$this->session->data['success'] = 'Mail Sent';
		} else {
			//echo"not send";
			$this->session->data['success'] = 'Mail Not Sent';
		}
	 	//echo 'Done';exit;
	 	//$this->getList();
		$this->response->redirect($this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'], true));




	}
}
	