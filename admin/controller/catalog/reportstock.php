<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');
class ControllerCatalogReportStock extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/reportstock');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportstock');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/reportstock');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_type'])){
			$data['type'] = $this->request->post['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->post['filter_departmentvalue'])){
			$data['departmentvalue'] = $this->request->post['filter_departmentvalue'];
		}
		else{
			$data['departmentvalue'] = '';
		}

		if(isset($this->request->post['filter_subcategory'])){
			$data['subcategory'] = $this->request->post['filter_subcategory'];
		}
		else{
			$data['subcategory'] = '';
		}

		if(isset($this->request->post['filter_category'])){
			$data['category'] = $this->request->post['filter_category'];
		}
		else{
			$data['category'] = '';
		}


		if(isset($this->request->post['filter_tablegroup'])){
			$data['tablegroup'] = $this->request->post['filter_tablegroup'];
		}
		else{
			$data['tablegroup'] = '';
		}

		$departments = $this->db->query("SELECT DISTINCT department FROM oc_item WHERE 1=1")->rows;
		// $inc = 0;

		// foreach ($departments as $dept) {
		// 	$array[] = array(
		// 		$inc => $dept['department'],
		// 	);	
		// 	$inc ++;	
		// }

		$data['departmentlist'] = $departments;

		$data['itemdatas'] = array();
		$itemdata = array();
		$department = array();
		$data['totals'] = array();
		$totalamount = array();
		$cancelamount = array();
		$modifiers = array();
		$data['modifiers'] = array();
		$data['cancelamount'] = array();

		$data['types'] = array(
							'liq'    => 'Liq',
							'food'    => 'Food'
						);

		$data['subcategorys'] = $this->db->query("SELECT * FROM oc_subcategory WHERE 1=1")->rows;
		$data['categorys'] = $this->db->query("SELECT * FROM oc_category WHERE 1=1")->rows;
		$data['tablegroups'] = $this->db->query("SELECT * FROM oc_location WHERE 1=1")->rows;

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) || isset($this->request->post['filter_departmentvalue']) || isset($this->request->post['filter_type']) || isset($this->request->post['filter_subcategory']) || isset($this->request->post['filter_category']) || isset($this->request->post['filter_tablegroup'])){

			$this->load->model('catalog/reportstock');
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$departmentvalue = $this->request->post['filter_departmentvalue'];
			$subcategory = $this->request->post['filter_subcategory'];
			$category = $this->request->post['filter_category'];
			$type = $this->request->post['filter_type'];
			$tablegroup = $this->request->post['filter_tablegroup'];

			$filter_data = array(
				'start_date'      => $start_date,
				'end_date'		  => $end_date,
				'departmentvalue' => $departmentvalue,
				'subcategory'     => $subcategory,
				'category'     	  => $category,
				'type'            => $type,
				'tablegroup'	  => $tablegroup
			);

			$department_itemdata = $this->model_catalog_reportstock->department_itemdata($filter_data);
			foreach ($department_itemdata as $xyz) {
				$department[$xyz['department']] = $this->model_catalog_reportstock->itemdata($start_date,$end_date,$xyz,$filter_data);
			//echo "<pre>";print_r($department);
			}

			//exit;

			//exit;
			$totalamount = $this->model_catalog_reportstock->totalamount($start_date,$end_date);
			$cancelamount = $this->model_catalog_reportstock->cancelamount($start_date,$end_date);
			$modifiers = $this->model_catalog_reportstock->modifier($start_date,$end_date);

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
		}

		$data['itemdatas'] = $itemdata;
		$data['departments'] = $department;
		//$data['departmentsm'] = $department;
		$data['totals'] = $totalamount;
		$data['modifiers'] = $modifiers;
		$data['cancelamount'] = $cancelamount;

		$data['token'] = $this->session->data['token'];
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}


		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['action'] = $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/reportstock_form', $data));
	}

	public function itemwisereport(){
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$departments = array();
		$totals = array();
		$cancelamount = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['filter_departmentvalue']) || isset($this->request->get['filter_type']) || isset($this->request->get['filter_subcategory']) || isset($this->request->get['filter_category']) || isset($this->request->get['filter_tablegroup'])){

			$this->load->model('catalog/reportstock');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$departmentvalue = isset($this->request->get['filter_departmentvalue']) ? $this->request->get['filter_departmentvalue'] : '';
			$subcategory =  isset($this->request->get['filter_subcategory']) ? $this->request->get['filter_subcategory'] : '' ;
			$category = isset($this->request->get['filter_category']) ? $this->request->get['filter_category'] : '';
			$type = isset($this->request->get['filter_type']) ? $this->request->get['filter_type'] : '';
			$tablegroup = isset($this->request->get['filter_tablegroup']) ? $this->request->get['filter_tablegroup'] : '';

			$filter_data = array(
					'start_date'      => $start_date,
					'end_date'		  => $end_date,
					'departmentvalue' => $departmentvalue,
					'subcategory'     => $subcategory,
					'category'     	  => $category,
					'type'            => $type,
					'tablegroup'	  => $tablegroup
				);

			$department_itemdata = $this->model_catalog_reportstock->department_itemdata($filter_data);

			foreach ($department_itemdata as $xyz) {
				$departments[$xyz['department']] = $this->model_catalog_reportstock->itemdata($start_date,$end_date,$xyz,$filter_data);
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$totalamount = $this->model_catalog_reportstock->totalamount($start_date,$end_date);
			$cancelamount = $this->model_catalog_reportstock->cancelamount($start_date,$end_date);
			$modifiers = $this->model_catalog_reportstock->modifier($start_date,$end_date);

			$qtytotal = 0; 
			$amttotal = 0;
			$qtytotalm = 0; 
			$amttotalm = 0; 
			$vat = 0; 
			$gst = 0; 
			$stax = 0; 
			$grandtotal = 0; 
			$discount = 0; 
			$cancelamt = 0; 
			$roundoff = 0;
			$advance = 0;
			$packaging = 0;
			$packaging_cgst = 0;
			$packaging_sgst = 0;

		if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
	 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
	 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
	 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
	 	} else {
	 		$connector = '';
	 	}
		try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Item Wise Sales Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("Product Name",20)."".str_pad("Quantity",15)."Amount");
			  	$printer->feed(1);
			  	foreach($departments as $dept => $value ){
			  		$departmentqty = 0; $departmentamt = 0;
			  		if($value != array()){
			  			$printer->text("------------------------------------------------");
						$printer->setJustification(Printer::JUSTIFY_CENTER);
			  			$printer->text($dept);
			  			$printer->feed(1);
			  			$printer->text("------------------------------------------------");
			  		}
			  		foreach($value as $key) {
			  			$key['name'] = utf8_substr(html_entity_decode($key['name'], ENT_QUOTES, 'UTF-8'), 0, 19);
			  			if($key['cancel_bill'] == 1){
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  				$printer->text(str_pad('0',20)."".str_pad('0',20)."".'0');
			  			} else{
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  				$printer->text(str_pad($key['name'],20)."".str_pad($key['quantity'],15)."".$key['amount']);
			  			}
			  			$printer->feed(1);
			  			$qtytotal = $qtytotal + $key['quantity']; 
			  			$amttotal = $amttotal + $key['amount'];
			  			$departmentqty = $departmentqty + $key['quantity']; 
			  			$departmentamt = $departmentamt + $key['amount'];
			  		}
			  		if($value != array()) {
		  				$printer->text(str_pad("Total",20)."".str_pad($departmentqty,15).$departmentamt);
		  				$printer->feed(1);
		  			}
			  	}
			  	if($modifiers != []){
				  	$printer->setJustification(Printer::JUSTIFY_CENTER);
				  	$printer->text("Modifiers Start");
				  	$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	$printer->feed(1);
				  	$qtytotalm = 0; $amttotalm = 0;
				  	foreach($modifiers as $modifier) {
				  		$modifier['name'] = utf8_substr(html_entity_decode($modifier['name'], ENT_QUOTES, 'UTF-8'), 0, 19);
					  	$printer->text(str_pad($modifier['name'],20)."".str_pad($modifier['quantity'],15)."".$modifier['amount']);
					  	$printer->feed(1);
			  			$qtytotalm = $qtytotalm + $modifier['quantity'];
			  			$amttotalm = $amttotalm + $modifier['amount'];
				  	}
				  	$printer->text(str_pad("Total",20)."".str_pad($qtytotalm,15).$amttotalm);
				  	$printer->feed(1);
				  	$printer->setJustification(Printer::JUSTIFY_CENTER);
				  	$printer->text("Modifiers End");
				  	$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	$printer->feed(1);
			  	}
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	if($qtytotal != '0' && $amttotal != '0' ) {
			  		$printer->text(str_pad("Total",20)."".str_pad($qtytotal,15)."".$amttotal);
			  		$printer->feed(1);
			  		$printer->text("------------------------------------------------");
			  		$printer->setEmphasis(true);
			  		$printer->feed(1);
			  		$printer->text(str_pad("",20)."".str_pad("Gross Total",15)."".$amttotal);
			  		$printer->feed(1);
			  		$printer->setEmphasis(false);
			  		$printer->text("------------------------------------------------");
			  		$printer->feed(1);
			  		if($departmentvalue  == '' && $subcategory == '' && $category == '' && $type  == '' && $tablegroup == '') {
			  			$printer->text(str_pad("",20)."".str_pad("P- Chrg Amt (+) :",15)."0");
			  			$printer->feed(1);
			  			$printer->text(str_pad("",20)."".str_pad("O- Chrg Amt (+) :",15)."0");
			  			$printer->feed(1);
			  			foreach($totalamount as $total) {
				  			if($total['liq_cancel'] == 1){
					  			$vat = 0;
					  		}else{
					  			$vat = $vat + $total['vat'];
					  		}
					  		if($total['food_cancel'] == 1){
					  			$gst = 0;
					  		} else{
					  			$gst = $gst + $total['gst'];
					  		}
					  		$packaging = $packaging + $total['packaging'];
					  		$packaging_cgst = $packaging_cgst + $total['packaging_cgst'];
					  		$packaging_sgst = $packaging_sgst + $total['packaging_sgst'];

					  		$stax = $stax + $total['stax'];
					  		if($total['food_cancel'] == 1){
					  			$total['ftotalvalue'] = 0;
					  		}
					  		if($total['liq_cancel'] == 1){
					  			$total['ltotalvalue'] = 0;
					  		}
					  		$discount = $discount + $total['ftotalvalue'] + $total['ltotalvalue'];
					  		$grandtotal = $grandtotal + $total['grand_total'];
					  		if($total['liq_cancel'] == 1 || $total['food_cancel'] == 1){
					  			$total['roundtotal'] = 0;
					  		}
					  		$roundoff = $roundoff + $total['roundtotal'];
					  		$advance = $advance + $total['advance_amount'];
				  		}
			  			$printer->text(str_pad("",20)."".str_pad("S- Chrg Amt (+) :",15)."".$stax);
			  			$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Vat Amt (+) :",15)."".$vat);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("GST Amt (+) :",15)."".$gst);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Packaging AMT (+) :",15)."".$packaging);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Packaging CGST (+) :",15)."".$packaging_cgst);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Packaging SGST (+) :",15)."".$packaging_sgst);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("R-Off Amt (+) :",15)."".$roundoff/2);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Discount Amt (-) :",15)."".$discount);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("P- Discount Amt (-) :",15)."0");
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Cancel Bill Amt (-) :",15).$cancelamount);
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Comp. Amt (-) :",15)."0");
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Advance. Amt (-) :",15).$advance);
					  	$printer->feed(1);
					  	$printer->setEmphasis(true);
					  	if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
					  		$printer->text(str_pad("",20)."".str_pad("Net Amount :",15)."".(($amttotal + $stax + $roundoff - $advance) - $discount));
					  	} else{
					  		$printer->text(str_pad("",20)."".str_pad("Net Amount :",15)."".(($amttotal + $stax + $vat + $gst + $roundoff - $advance) - $discount));
					  	}
					  	$printer->feed(1);
					  	$printer->text(str_pad("",20)."".str_pad("Advance Amt (+) :",20).$advancetotal);
					  	$printer->feed(1);
					  	$printer->text("------------------------------------------------");
					  	$printer->feed(1);
					  	if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
					  		$printer->text(str_pad("",20)."".str_pad("Grand Total (+) :",15)."".(($amttotal + $stax + $roundoff) - $discount));
					  	} else{
					  		$printer->text(str_pad("",20)."".str_pad("Grand Total (+) :",15)."".(($amttotal + $stax + $vat + $gst + $roundoff) - $discount));
					  	}
					  	$printer->feed(1);
					  	$printer->text("------------------------------------------------");
					  	$printer->feed(1);
			  		}
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("End of Report");
			  	$printer->feed(1);
			  	}
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}

	public function pdfprints(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/reportstock');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/reportstock');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_type'])){
			$data['type'] = $this->request->get['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->get['filter_departmentvalue'])){
			$data['departmentvalue'] = $this->request->get['filter_departmentvalue'];
		}
		else{
			$data['departmentvalue'] = '';
		}

		if(isset($this->request->get['filter_subcategory'])){
			$data['subcategory'] = $this->request->get['filter_subcategory'];
		}
		else{
			$data['subcategory'] = '';
		}

		if(isset($this->request->get['filter_category'])){
			$data['category'] = $this->request->get['filter_category'];
		}
		else{
			$data['category'] = '';
		}


		if(isset($this->request->get['filter_tablegroup'])){
			$data['tablegroup'] = $this->request->get['filter_tablegroup'];
		}
		else{
			$data['tablegroup'] = '';
		}

		$departments = $this->db->query("SELECT DISTINCT department FROM oc_item WHERE 1=1")->rows;
		// $inc = 0;

		// foreach ($departments as $dept) {
		// 	$array[] = array(
		// 		$inc => $dept['department'],
		// 	);	
		// 	$inc ++;	
		// }

		$data['departmentlist'] = $departments;

		$data['itemdatas'] = array();
		$itemdata = array();
		$department = array();
		$data['totals'] = array();
		$totalamount = array();
		$cancelamount = array();
		$modifiers = array();
		$data['modifiers'] = array();
		$data['cancelamount'] = array();

		$data['types'] = array(
							'liq'    => 'Liq',
							'food'    => 'Food'
						);

		$data['subcategorys'] = $this->db->query("SELECT * FROM oc_subcategory WHERE 1=1")->rows;
		$data['categorys'] = $this->db->query("SELECT * FROM oc_category WHERE 1=1")->rows;
		$data['tablegroups'] = $this->db->query("SELECT * FROM oc_location WHERE 1=1")->rows;

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['filter_departmentvalue']) || isset($this->request->get['filter_type']) || isset($this->request->get['filter_subcategory']) || isset($this->request->get['filter_category']) || isset($this->request->get['filter_tablegroup'])){

			//echo "<pre>";print_r($_GET);exit;
			$this->load->model('catalog/reportstock');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			if (isset($this->request->get['filter_departmentvalue'])) {
				//echo "in";exit;
				$departmentvalue = $this->request->get['filter_departmentvalue'];
			}else{
				$departmentvalue = '';
			}
			if (isset($this->request->get['filter_subcategory'])) {
				$subcategory = $this->request->get['filter_subcategory'];
			}else{
				$subcategory = '';
			}
			if (isset($this->request->get['filter_category'])) {
				$category = $this->request->get['filter_category'];
			}else{
				$category = '';
			}
			if (isset($this->request->get['filter_type'])) {
				$type = $this->request->get['filter_type'];
			}else{
				$type = '';
			}
			if (isset($this->request->get['filter_tablegroup'])) {
				$tablegroup = $this->request->get['filter_tablegroup'];
			}else{
				$tablegroup = '';
			}

			$filter_data = array(
					'start_date'      => $start_date,
					'end_date'		  => $end_date,
					'departmentvalue' => $departmentvalue,
					'subcategory'     => $subcategory,
					'category'     	  => $category,
					'type'            => $type,
					'tablegroup'	  => $tablegroup
				);

			$department_itemdata = $this->model_catalog_reportstock->department_itemdata($filter_data);
			//echo "<pre>";print_r($department_itemdata);exit;

			foreach ($department_itemdata as $xyz) {
				$department[$xyz['department']] = $this->model_catalog_reportstock->itemdata($start_date,$end_date,$xyz,$filter_data);
				//$departmentm[$xyz['department']] = $this->model_catalog_reportstock->itemdatam($start_date,$end_date,$xyz,$filter_data);
			}

			$totalamount = $this->model_catalog_reportstock->totalamount($start_date,$end_date);
			//echo "<pre>";print_r($totalamount);exit;

			$cancelamount = $this->model_catalog_reportstock->cancelamount($start_date,$end_date);
			$modifiers = $this->model_catalog_reportstock->modifier($start_date,$end_date);

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
		}
		//echo "<pre>";print_r($itemdata);exit;
		$data['itemdatas'] = $itemdata;
		$data['departments'] = $department;
		//$data['departmentsm'] = $department;
		$data['totals'] = $totalamount;
		$data['modifiers'] = $modifiers;
		$data['cancelamount'] = $cancelamount;
		//echo "<pre>";print_r($data);exit;
		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['action'] = $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
		//echo "<pre>";print_r($data);exit;
		$html = $this->load->view('sale/itemwise_html', $data);
		//echo "<pre>";print_r($html);exit;
		
		/*$filename = 'reportstock.html';*/
		$filename = 'reportstock.xls';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/xls');
		echo $html;exit;
	}

	public function sendmail(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/reportstock');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/reportstock');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_type'])){
			$data['type'] = $this->request->get['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->get['filter_departmentvalue'])){
			$data['departmentvalue'] = $this->request->get['filter_departmentvalue'];
		}
		else{
			$data['departmentvalue'] = '';
		}

		if(isset($this->request->get['filter_subcategory'])){
			$data['subcategory'] = $this->request->get['filter_subcategory'];
		}
		else{
			$data['subcategory'] = '';
		}

		if(isset($this->request->get['filter_category'])){
			$data['category'] = $this->request->get['filter_category'];
		}
		else{
			$data['category'] = '';
		}


		if(isset($this->request->get['filter_tablegroup'])){
			$data['tablegroup'] = $this->request->get['filter_tablegroup'];
		}
		else{
			$data['tablegroup'] = '';
		}

		$departments = $this->db->query("SELECT DISTINCT department FROM oc_item WHERE 1=1")->rows;
		// $inc = 0;

		// foreach ($departments as $dept) {
		// 	$array[] = array(
		// 		$inc => $dept['department'],
		// 	);	
		// 	$inc ++;	
		// }

		$data['departmentlist'] = $departments;

		$data['itemdatas'] = array();
		$itemdata = array();
		$department = array();
		$data['totals'] = array();
		$totalamount = array();
		$cancelamount = array();
		$modifiers = array();
		$data['modifiers'] = array();
		$data['cancelamount'] = array();

		$data['types'] = array(
							'liq'    => 'Liq',
							'food'    => 'Food'
						);

		$data['subcategorys'] = $this->db->query("SELECT * FROM oc_subcategory WHERE 1=1")->rows;
		$data['categorys'] = $this->db->query("SELECT * FROM oc_category WHERE 1=1")->rows;
		$data['tablegroups'] = $this->db->query("SELECT * FROM oc_location WHERE 1=1")->rows;

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['filter_departmentvalue']) || isset($this->request->get['filter_type']) || isset($this->request->get['filter_subcategory']) || isset($this->request->get['filter_category']) || isset($this->request->get['filter_tablegroup'])){

			//echo "<pre>";print_r($_GET);exit;
			$this->load->model('catalog/reportstock');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			if (isset($this->request->get['filter_departmentvalue'])) {
				//echo "in";exit;
				$departmentvalue = $this->request->get['filter_departmentvalue'];
			}else{
				$departmentvalue = '';
			}
			if (isset($this->request->get['filter_subcategory'])) {
				$subcategory = $this->request->get['filter_subcategory'];
			}else{
				$subcategory = '';
			}
			if (isset($this->request->get['filter_category'])) {
				$category = $this->request->get['filter_category'];
			}else{
				$category = '';
			}
			if (isset($this->request->get['filter_type'])) {
				$type = $this->request->get['filter_type'];
			}else{
				$type = '';
			}
			if (isset($this->request->get['filter_tablegroup'])) {
				$tablegroup = $this->request->get['filter_tablegroup'];
			}else{
				$tablegroup = '';
			}

			$filter_data = array(
					'start_date'      => $start_date,
					'end_date'		  => $end_date,
					'departmentvalue' => $departmentvalue,
					'subcategory'     => $subcategory,
					'category'     	  => $category,
					'type'            => $type,
					'tablegroup'	  => $tablegroup
				);

			$department_itemdata = $this->model_catalog_reportstock->department_itemdata($filter_data);
			//echo "<pre>";print_r($department_itemdata);exit;

			foreach ($department_itemdata as $xyz) {
				$department[$xyz['department']] = $this->model_catalog_reportstock->itemdata($start_date,$end_date,$xyz,$filter_data);
				//$departmentm[$xyz['department']] = $this->model_catalog_reportstock->itemdatam($start_date,$end_date,$xyz,$filter_data);
			}

			$totalamount = $this->model_catalog_reportstock->totalamount($start_date,$end_date);
			//echo "<pre>";print_r($totalamount);exit;

			$cancelamount = $this->model_catalog_reportstock->cancelamount($start_date,$end_date);
			$modifiers = $this->model_catalog_reportstock->modifier($start_date,$end_date);

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
		}
		//echo "<pre>";print_r($itemdata);exit;
		$data['itemdatas'] = $itemdata;
		$data['departments'] = $department;
		//$data['departmentsm'] = $department;
		$data['totals'] = $totalamount;
		$data['modifiers'] = $modifiers;
		$data['cancelamount'] = $cancelamount;
		//echo "<pre>";print_r($data);exit;
		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['action'] = $this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
		//echo "<pre>";print_r($data);exit;
		$html = $this->load->view('sale/itemwise_html', $data);
		$filename = 'ItemWiseReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');


		$filename = 'Item Wise Report';
		define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $output = $dompdf->output();
	    $date = date('Y-m-d');
	    $backup_name = DIR_DOWNLOAD."ItemWiseReport.pdf";
	    file_put_contents($backup_name, $output);

	    $filename = "ItemWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;

		
	 	$to_emails = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT');
	 	$password = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT_PASSWORD');
	 	$username = $this->model_catalog_order->get_settings('EMAIL_USERNAME');


		$mail = new PHPMailer();
		$message = "Item Wise Report";
		$subject = "Item Wise Report";


		// $to_emails = 'myadav00349@gmail.com';
		// $from_email = 'report@loyalrisk.in';
		// $password = '2020loyalrisk';

		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
			$to_emails_array[]= $to_emails;
			$to_emails_array[]= $to_emails;
		} else {
			$to_emails_array[] = $to_emails; 
			$to_emails_array[]= $to_emails;
		}

		$date = date('Y-m-d');
		$filename = "ItemWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;
		
		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = $username;//$this->config->get('config_smtp_username');
		$mail->Password = $password;//$this->config->get('config_smtp_password');
		// $mail->Username = 'report@loyalrisk.in';//$this->config->get('config_smtp_username');
		// $mail->Password = '2020loyalrisk';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($username, 'Hotel');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			//$to_name = 'Aaron Fargose';
			$mail->AddAddress($evalue);
		}
		
		if($mail->Send()) {
			//echo"send";
			$this->session->data['success'] = 'Mail Sent';
		} else {
			//echo"not send";
			$this->session->data['success'] = 'Mail Not Sent';
		}
	 	//echo 'Done';exit;
	 	//$this->getList();
		$this->response->redirect($this->url->link('catalog/reportstock', 'token=' . $this->session->data['token'], true));

	}


}