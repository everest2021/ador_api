<?php 
class ControllerCommonHeader extends Controller {
	protected function index() {

    	//$this->load->model('transaction/receipt');
		//$this->model_transaction_receipt->checkgymids();
		$this->data['title'] = $this->document->getTitle(); 
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}

		//if(!$this->user->isLogged()){
			//$expdate = '2015-02-25';
			//$date1 = new DateTime('now');
			//$date1 = new DateTime('2015-01-20');
			//$date2 = new DateTime($expdate);
			//$interval = $date1->diff($date2);
			//if($interval->invert == 0){
				//$this->session->data['warning'] = 'Kindly Update The Subscription within '. $interval->days .' Days';
			//} elseif($interval->invert == 1){
				//$this->session->data['warning'] = 'Kindly Update The Subscription';
			//}
		//}

		if (isset($this->session->data['warning'])) {
    		$this->data['error_warning1'] = $this->session->data['warning'];
    		unset($this->session->data['warning']);
		} else {
			$this->data['error_warning1'] = '';
		}
		
		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		
		$this->language->load('common/header');

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
		$this->data['text_attribute'] = $this->language->get('text_attribute');
		$this->data['text_attribute_group'] = $this->language->get('text_attribute_group');
		$this->data['text_backup'] = $this->language->get('text_backup');
		$this->data['text_banner'] = $this->language->get('text_banner');
		$this->data['text_catalog'] = $this->language->get('text_catalog');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_coupon'] = $this->language->get('text_coupon');
		$this->data['text_currency'] = $this->language->get('text_currency');			
		$this->data['text_customer'] = $this->language->get('text_customer');
		$this->data['text_customer_group'] = $this->language->get('text_customer_group');
		$this->data['text_customer_field'] = $this->language->get('text_customer_field');
		$this->data['text_customer_ban_ip'] = $this->language->get('text_customer_ban_ip');
		$this->data['text_custom_field'] = $this->language->get('text_custom_field');
		$this->data['text_sale'] = $this->language->get('text_sale');
		$this->data['text_design'] = $this->language->get('text_design');
		$this->data['text_documentation'] = $this->language->get('text_documentation');
		$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_error_log'] = $this->language->get('text_error_log');
		$this->data['text_extension'] = $this->language->get('text_extension');
		$this->data['text_feed'] = $this->language->get('text_feed');
		$this->data['text_filter'] = $this->language->get('text_filter');
		$this->data['text_front'] = $this->language->get('text_front');
		$this->data['text_geo_zone'] = $this->language->get('text_geo_zone');
		$this->data['text_dashboard'] = $this->language->get('text_dashboard');
		$this->data['text_help'] = $this->language->get('text_help');
		$this->data['text_information'] = $this->language->get('text_information');
		$this->data['text_language'] = $this->language->get('text_language');
		$this->data['text_layout'] = $this->language->get('text_layout');
		$this->data['text_localisation'] = $this->language->get('text_localisation');
		$this->data['text_logout'] = $this->language->get('text_logout');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_manager'] = $this->language->get('text_manager');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_module'] = $this->language->get('text_module');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_order_status'] = $this->language->get('text_order_status');
		$this->data['text_opencart'] = $this->language->get('text_opencart');
		$this->data['text_payment'] = $this->language->get('text_payment');
		$this->data['text_product'] = $this->language->get('text_product'); 
		$this->data['text_profile'] = $this->language->get('text_profile');
		$this->data['text_reports'] = $this->language->get('text_reports');
		$this->data['text_report_sale_order'] = $this->language->get('text_report_sale_order');
		$this->data['text_report_sale_tax'] = $this->language->get('text_report_sale_tax');
		$this->data['text_report_sale_shipping'] = $this->language->get('text_report_sale_shipping');
		$this->data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$this->data['text_report_sale_coupon'] = $this->language->get('text_report_sale_coupon');
		$this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
		$this->data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
		$this->data['text_report_customer_online'] = $this->language->get('text_report_customer_online');
		$this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
		$this->data['text_report_customer_reward'] = $this->language->get('text_report_customer_reward');
		$this->data['text_report_customer_credit'] = $this->language->get('text_report_customer_credit');
		$this->data['text_report_affiliate_commission'] = $this->language->get('text_report_affiliate_commission');
		$this->data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
		$this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
		$this->data['text_review'] = $this->language->get('text_review');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_return_action'] = $this->language->get('text_return_action');
		$this->data['text_return_reason'] = $this->language->get('text_return_reason');
		$this->data['text_return_status'] = $this->language->get('text_return_status');
		$this->data['text_support'] = $this->language->get('text_support');
		$this->data['text_shipping'] = $this->language->get('text_shipping');
		$this->data['text_setting'] = $this->language->get('text_setting');
		$this->data['text_stock_status'] = $this->language->get('text_stock_status');
		$this->data['text_system'] = $this->language->get('text_system');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_tax_rate'] = $this->language->get('text_tax_rate');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_user'] = $this->language->get('text_user');
		$this->data['text_user_group'] = $this->language->get('text_user_group');
		$this->data['text_users'] = $this->language->get('text_users');
		$this->data['text_voucher'] = $this->language->get('text_voucher');
		$this->data['text_voucher_theme'] = $this->language->get('text_voucher_theme');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_length_class'] = $this->language->get('text_length_class');
		$this->data['text_zone'] = $this->language->get('text_zone');
        $this->data['text_openbay_extension'] = $this->language->get('text_openbay_extension');
        $this->data['text_openbay_dashboard'] = $this->language->get('text_openbay_dashboard');
        $this->data['text_openbay_orders'] = $this->language->get('text_openbay_orders');
        $this->data['text_openbay_items'] = $this->language->get('text_openbay_items');
        $this->data['text_openbay_ebay'] = $this->language->get('text_openbay_ebay');
        $this->data['text_openbay_amazon'] = $this->language->get('text_openbay_amazon');
        $this->data['text_openbay_amazonus'] = $this->language->get('text_openbay_amazonus');
        $this->data['text_openbay_play'] = $this->language->get('text_openbay_play');
        $this->data['text_openbay_settings'] = $this->language->get('text_openbay_settings');
        $this->data['text_openbay_links'] = $this->language->get('text_openbay_links');
        $this->data['text_openbay_report_price'] = $this->language->get('text_openbay_report_price');
        $this->data['text_openbay_order_import'] = $this->language->get('text_openbay_order_import');
		
		$this->data['text_paypal_express'] = $this->language->get('text_paypal_manage');
		$this->data['text_paypal_express_search'] = $this->language->get('text_paypal_search');
		$this->data['text_recurring_profile'] = $this->language->get('text_recurring_profile');

		if (!$this->user->isLogged() || !isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$this->data['logged'] = '';
			
			$this->data['home'] = $this->url->link('common/login', '', 'SSL');
		} else {
			$this->data['logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
            
            $this->data['category'] = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], 'SSL');
            $this->data['designation'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['division'] = $this->url->link('catalog/division', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['location'] = $this->url->link('catalog/location', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['venue'] = $this->url->link('catalog/venue', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['employee'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['schedule'] = $this->url->link('catalog/schedule', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['requirement'] = $this->url->link('catalog/requirement', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attendance'] = $this->url->link('transaction/daily', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');
			//$this->load->model('catalog/contractor');
			//$sql_filter_date = $this->model_catalog_contractor->gettransactiondate(5);
			
			//if(isset($sql_filter_date['date']) && $sql_filter_date['date'] != '' && $sql_filter_date['date'] != '0000-00-00'){
				//$this->data['contractor'] = $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . '&filter_date=' . $sql_filter_date['date'].'&filter_cont_name='.$sql_filter_date['venue'], 'SSL');
			//} else {
				$this->data['contractor'] = $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'], 'SSL');
			//}
			$this->data['tote_promotion'] = $this->url->link('tote/promotion', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['tote_allocation'] = $this->url->link('tote/allocation', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['tote_auto_allocation'] = $this->url->link('tote/allocation/auto_allocate', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['allocation'] = $this->url->link('catalog/allocation', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attendance_calander'] = $this->url->link('attendance/attendance', 'token=' . $this->session->data['token'], 'SSL');	
			$this->data['attendance_bonus'] = $this->url->link('attendance/bonus', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['threereport'] = $this->url->link('report/3areport', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['contractor_report'] = $this->url->link('report/contractor', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['daily_report'] = $this->url->link('report/daily', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['monthly_summary'] = $this->url->link('report/pfsummary', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['monthly_employee'] = $this->url->link('report/pfemployee', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['monthly_summary_venue'] = $this->url->link('report/monthlysummaryvenue', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['empdet'] = $this->url->link('report/empdet', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['bonusworking'] = $this->url->link('report/bonusworking', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['removefinal'] = $this->url->link('report/removefinal', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['pp_express_status'] = $this->config->get('pp_express_status');
            $this->data['home'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['affiliate'] = $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attribute'] = $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attribute_group'] = $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['backup'] = $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['error_log'] = $this->url->link('tool/error_log', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['import'] = $this->url->link('tool/import', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['user'] = $this->url->link('user/user', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['user_group'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['receipt'] = $this->url->link('transaction/receipt', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['particular'] = $this->url->link('catalog/particular', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['detail'] = $this->url->link('report/detail', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['summary'] = $this->url->link('report/summary', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['member'] = $this->url->link('catalog/member', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['category_report'] = $this->url->link('report/category_report', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['offline_list'] = $this->url->link('transaction/attendance', 'token=' . $this->session->data['token'].'&date='.date('Y-m-d'), 'SSL');
			$this->data['rec_no'] = $this->url->link('report/receipt', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['rec_man'] = $this->url->link('transaction/receipt_manual', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['collection'] = $this->url->link('report/collection', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['cancel_receipt'] = $this->url->link('transaction/receipt/modify_receipt', 'token=' . $this->session->data['token'], 'SSL');
			//$this->load->model('tote/allocation');
			//$this->data['racedet'] = $this->model_tote_allocation->getracedet_ocbc(date('Y-m-d'));
			//if(isset($this->data['racedet']) && $this->data['racedet']){
				//$this->data['daily_report_ocbc'] = $this->url->link('report/daily/checklist_daily', 'token=' . $this->session->data['token'] . '&venue=' . $this->data['racedet']['venue_id'].'&date='.$this->data['racedet']['dateofrace'], 'SSL');
			//} else {
				$this->data['daily_report_ocbc'] = $this->url->link('report/daily/checklist_daily', 'token=' . $this->session->data['token'], 'SSL');
			//}
		}
		
		$this->template = 'common/header.tpl';
		
		$this->render();
	}
}
?>