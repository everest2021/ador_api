<?php
class ControllerCatalogsettlement extends Controller {

	public function index() {
		$this->load->model('catalog/settlement');
		$this->getList();
	}

	public function add() {
		$this->log->write("Settlement Start");
		$this->log->write('User Name : ' .$this->user->getUserName());
		


		$ischecked = 0;

		if(!empty($this->request->get['orderidmodify'])){
			$orderidmodify = $this->request->get['orderidmodify'];
		} else{
			$orderidmodify = '';
		}


		// echo $orderidmodify;
		// exit();

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->load->model('catalog/settlement');

			$settledata = $this->request->post;
		//echo'<pre>';print_r($settledata); exit;

		$this->log->write("Order id :" .$settledata['order_id']);

			if($settledata['payment_type']=='0'){
				$payment_type = '';
			}else{
				$payment_type = $settledata['payment_type'];
			}


			// echo "<pre>";
			// print_r($settledata['payment_type']);
			// exit;
			if(isset($settledata['compl_status'])){
				if($settledata['compl_status'] == 1){
					if($settledata['complimentary_reason'] != ''){
						$ischecked = 3;
						$this->session->data['compl_status'] = $settledata['compl_status'];
						$this->session->data['complimentary_reason'] = $settledata['complimentary_reason'];
					}else {
						$ischecked = 4;
					}
				}
			}

			$total_amount = (float)$settledata['cash'] + (float)$settledata['credit'] +(float)$settledata['online'] + (float)$settledata['mealpass'] + (float)$settledata['pass'] + (float)$settledata['roomservice'] + (float)$settledata['msrcard'] + (float)$settledata['deductamount']; 
			if($settledata['grand_total'] == $total_amount){
				//$customercredits = $this->db->query("SELECT * FROM oc_customercredit WHERE c_id = '".$settledata['c_id']."'");
				// if($customercredits->num_rows > 0){
				// 	$creditval = $customercredits->row['balance'] - $settledata['deductamount'];
				// 	$deductamount = $customercredits->row['deducted'] + $settledata['deductamount'];
				// 	$this->db->query("UPDATE oc_customercredit SET balance = '".$creditval."', deducted = '".$deductamount."' WHERE c_id = '".$settledata['c_id']."'");
				// }

				
				if($orderidmodify != ''){
					if($settledata['remark'] != ''){
						$ischecked = 1;
						// echo'<pre>';
						// print_r($settledata);
						// exit;
						$this->db->query("UPDATE oc_order_info SET
											pay_method = '1',
											payment_status = '1',
											pay_cash = '".$settledata['cash']."',
											pay_card = '".$settledata['credit']."',
											pay_online = '".$settledata['online']."',
											payment_type = '".$payment_type."',
											new_pay_cash = '".$settledata['cash']."',
											new_pay_card = '".$settledata['credit']."',
											new_pay_online = '".$settledata['online']."', 
											new_total_payment  = '".$total_amount."',
											card_no = '".$settledata['cardno']."',
											creditcustname  = '".$settledata['creditcustname']."',
											mealpass  = '".$settledata['mealpass']."',
											pass  = '".$settledata['pass']."',
											roomservice  = '".$settledata['roomservice']."',
											roomno  = '".$settledata['roomno']."',
											msrcard  = '".$settledata['msrcard']."',
											msrcardno  = '".$settledata['msrcardno']."',
											msrcustname  = '".$settledata['msrcustname']."',
											msr_cust_id  = '".$settledata['msr_cust_id']."',
											onac  = '".$settledata['deductamount']."',
											onaccust  = '".$settledata['c_id']."',
											onaccontact  = '".$settledata['onaccontact']."',
											onacname  = '".$settledata['onacname']."',
											total_payment  = '".$total_amount."',
											tip  = '".$settledata['tip']."',
											modify_remark = '".$settledata['remark']."'
											WHERE order_id = '".$settledata['order_id']."' ");
						$advance_billno = $this->db->query("SELECT advance_billno FROM oc_order_info WHERE order_id = '".$settledata['order_id']."' ")->row;
						if(isset($advance_billno['advance_billno'])){
							$this->db->query("UPDATE oc_advance SET status = '1' WHERE advance_billno = '".$advance_billno['advance_billno']."'");
						}
					} else{
						$ischecked = 0;
					}
				
				} else{
					$ischecked = 2;
					if($settledata['order_id'] == 0 ){
						/*echo'<pre>';print_r($settledata); exit;*/
						$this->session->data['cash'] = $settledata['cash'];
						$this->session->data['credit'] = $settledata['credit'];
						$this->session->data['online'] = $settledata['online'];
						$this->session->data['onac'] = $settledata['deductamount'];
						$this->session->data['onaccust'] = $settledata['c_id'];
						$this->session->data['onaccontact'] = $settledata['onaccontact'];
						$this->session->data['onacname'] = $settledata['onacname'];
						$this->session->data['payment_type'] = $settledata['payment_type'];
						

					} else {
						//echo'inn2';exit;
						if($settledata['payment_type']=='0'){
							$payment_type = '';
						}else{
							$payment_type = $settledata['payment_type'];
						}
						$ischecked = 1;
						$this->db->query("UPDATE oc_order_info SET
											pay_method = '1',
											payment_status = '1',
											pay_cash = '".$settledata['cash']."',
											pay_card = '".$settledata['credit']."',
											pay_online = '".$settledata['online']."',
											payment_type = '".$payment_type."',
											card_no = '".$settledata['cardno']."',
											creditcustname  = '".$settledata['creditcustname']."',
											mealpass  = '".$settledata['mealpass']."',
											pass  = '".$settledata['pass']."',
											roomservice  = '".$settledata['roomservice']."',
											roomno  = '".$settledata['roomno']."',
											msrcard  = '".$settledata['msrcard']."',
											msrcardno  = '".$settledata['msrcardno']."',
											msrcustname  = '".$settledata['msrcustname']."',
											msr_cust_id  = '".$settledata['msr_cust_id']."',
											onac  = '".$settledata['deductamount']."',
											onaccust  = '".$settledata['c_id']."',
											onaccontact  = '".$settledata['onaccontact']."',
											onacname  = '".$settledata['onacname']."',
											total_payment  = '".$total_amount."',
											tip  = '".$settledata['tip']."'
											WHERE order_id = '".$settledata['order_id']."' ");

						$advance_billno = $this->db->query("SELECT advance_billno FROM oc_order_info WHERE order_id = '".$settledata['order_id']."' ")->row;
						if(isset($advance_billno['advance_billno'])){
							$this->db->query("UPDATE oc_advance SET status = '1' WHERE advance_billno = '".$advance_billno['advance_billno']."'");
						}
					}
				}
			}

			unset($this->session->data['cust_id']);
			unset($this->session->data['msr_no']);
			unset($this->session->data['msr_name']);

			if($ischecked == 1){
				$json['done'] = '<script>parent.closeIFrame();</script>';
				$json['info'] = 1;
				$this->response->setOutput(json_encode($json));
			} elseif($ischecked == 2){
				$json['done'] = '<script>parent.closepaymode();</script>';
				//	$json['done'] = '<script>JavaScript:eval(window.close(););</script>';
				//JavaScript:eval(window.close(););
				$json['info'] = 2;
				$this->response->setOutput(json_encode($json));
			} elseif($ischecked == 3){
				$json['done'] = '<script>parent.closepaymode();</script>';
				//	$json['done'] = '<script>JavaScript:eval(window.close(););</script>';
				//JavaScript:eval(window.close(););
				$json['info'] = 3;
				$this->response->setOutput(json_encode($json));
			} elseif($ischecked == 4){
				$json['info'] = 4;
				$this->response->setOutput(json_encode($json));
			} else {
				$json['info'] = 0;
				$this->response->setOutput(json_encode($json));
			}
		}
		$this->log->write("Settlement end");

			unset($this->session->data['cust_id']);
			unset($this->session->data['msr_no']);
			unset($this->session->data['msr_name']);
	}

	public function getlist(){
		$url = '';
		if(isset($this->request->get['orderidmodify'])){
			$data['orderidmodify'] = $this->request->get['orderidmodify'];
		} else{
			$data['orderidmodify'] = '';
		}
		// echo'<pre>';
		// print_r($this->session->data);
		// exit;

		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		} else{
			$data['order_id'] = '';
		}

		if (!empty($this->request->get['orderidmodify'])) {
			$url = '&orderidmodify=' . $this->request->get['orderidmodify'];
		}
		$this->load->model('catalog/msr_recharge');
		$this->load->model('catalog/order');


		//$cust_id = $this->session->data['c_id'];
		//$msr_no = $this->session->data['msr_no'];
		if(isset($this->session->data['msr_no'])){
			$msr_no = $this->session->data['msr_no'];
		} else{
			$msr_no = '';
		}

		if(isset($this->session->data['cust_id'])){
			$cust_id = $this->session->data['cust_id'];
		} else{
			$cust_id = '';
		}
		// echo $msr_no;
		// exit;
		$results = array();
		$resultzz = $this->db->query("SELECT * FROM oc_customerinfo WHERE msr_no = '" .$msr_no. "'");
		if($resultzz->num_rows > 0){
			$results = $resultzz->row;
		}
		$msr_bal = $this->model_catalog_msr_recharge->pre_bal($msr_no,$cust_id);
		$data['msr_bals'] = $msr_bal;
		$data['msr_nos'] = $msr_no;

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		if(isset($this->request->get['grand_total'])){
			$grand_total = $this->request->get['grand_total'];
		} else{
			$grand_total = '';
		}

		if(isset($this->request->get['table_id'])){
			$table_id = $this->request->get['table_id'];
		} else{
			$table_id = '';
		}

		$order_id = $this->request->get['order_id'];
		// $grand_total = $this->request->get['grand_total'];
		// $table_id = $this->request->get['table_id'];

		$data['billdata'] = array();
		//$billdata = array();
		$credit = 0;
		if($order_id != 1 && $grand_total != 0 ){
			$billdata= array(
				'bill_date' => date('Y-m-d'),
				'grand_total' => $grand_total,
				'order_id' => $order_id,
				'order_no' => $order_id,
				't_name' => $table_id

			);
		} else{

			$order_id = $this->request->get['order_id'];
			$billdata = $this->db->query("SELECT * FROM oc_order_info WHERE pay_method = '0' AND order_id = '".$order_id."'")->row;

			$credit = 0;
			if($billdata['cust_id'] != 0){
				$credit = $this->db->query("SELECT SUM(credit) as credit FROM oc_customercredit WHERE c_id = '".$billdata['cust_id']."'");
				if($credit->num_rows > 0){
					$credit = $credit->row['credit'];
					$deductedcredit = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$billdata['cust_id']."'");
					if($deductedcredit->num_rows > 0){
						$totalcredit = $credit - $deductedcredit->row['onac'];
						$credit = $totalcredit;
					}
				} else{
					$credit = 0;
				}
			}
		}


		// if (isset($this->request->get['c_id'])) {
		// 	$credit = $this->db->query("SELECT SUM(credit) as credit FROM oc_customercredit WHERE c_id = '".$this->request->get['c_id']."'");
		// 	if($credit->num_rows > 0){
		// 		$credit = $credit->row['credit'];
		// 		$deductedcredit = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$this->request->get['c_id']."'");
		// 		if($deductedcredit->num_rows > 0){
		// 			$totalcredit = $credit - $deductedcredit->row['onac'];
		// 			$credit = $totalcredit;
		// 		}
		// 	} else{
		// 		$credit = 0;
		// 	}
		// 	$json = array(
		// 		'credit' => $credit
		// 	);
		// }

		$sql = "SELECT * FROM " . DB_PREFIX . "onlinepayment WHERE 1=1 ORDER BY id ASC ";

		$online_type = $this->db->query($sql)->rows;

		// echo "<pre>";
		// print_r($billdata);
		// exit;
		$data['complimentary_status'] = $this->model_catalog_order->get_settings('COMPLIMENTARY_STATUS');

		$data['online_type'] = $online_type;


		$data['msr_data'] = $results;
		$data['onaccount'] = $credit;
		$data['credit'] = $credit;
		$data['billdata'] = $billdata;
		$data['action_comp'] = $this->url->link('catalog/complimentary/complimentary_direct', 'token=' . $this->session->data['token'] . $url, true);

		$data['action'] = $this->url->link('catalog/settlement/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/settlement', $data));
	}

	public function autocomplete(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE name LIKE '%".$this->request->get['filter_name']."%' OR contact LIKE '%".$this->request->get['filter_name']."%' ORDER BY c_id DESC LIMIT 0,5")->rows;
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'contact'        => $result['contact'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function credit(){
		$json = array();

		if (isset($this->request->get['c_id'])) {
			$credit = $this->db->query("SELECT SUM(credit) as credit FROM oc_customercredit WHERE c_id = '".$this->request->get['c_id']."'");
			if($credit->num_rows > 0){
				$credit = $credit->row['credit'];
				$deductedcredit = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$this->request->get['c_id']."'");
				if($deductedcredit->num_rows > 0){
					$totalcredit = $credit - $deductedcredit->row['onac'];
					$credit = $totalcredit;
				}
			} else{
				$credit = 0;
			}
			$json = array(
				'credit' => $credit
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}