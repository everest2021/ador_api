<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogreportdaywise extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/reportdaywise');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportdaywise');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportdaywise', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$date_array = array();
			$datacash = array();
			$datacredit = array();
			$dataonac = array();
			$datamealpass = array();
			$datapass = array();
			foreach($dates as $date){
				$datacash = $this->db->query("SELECT count(*) as cashcount , SUM(vat) as cashvat, SUM(gst) as cashgst, SUM(stax) as cashstax, SUM(grand_total) as grandtotalcash, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pay_cash <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datacredit = $this->db->query("SELECT count(*) as creditcount , SUM(vat) as creditvat, SUM(gst) as creditgst, SUM(stax) as creditstax, SUM(grand_total) as grandtotalcredit, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pay_card <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$dataonac = $this->db->query("SELECT count(*) as onaccount , SUM(vat) as onacvat, SUM(gst) as onacgst, SUM(stax) as onacstax, SUM(grand_total) as grandtotalonac, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE onac <> '0.00' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datamealpass = $this->db->query("SELECT count(*) as mealpass , SUM(vat) as mealvat, SUM(gst) as mealcgst, SUM(stax) as mealstax, SUM(grand_total) as grandtotalmeal, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE mealpass <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datapass = $this->db->query("SELECT count(*) as pass , SUM(vat) as passvat, SUM(gst) as passcgst, SUM(stax) as passstax, SUM(grand_total) as grandtotalpass, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pass <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$dataadvance = $this->db->query("SELECT SUM(advance_amount) as total_advance FROM `oc_order_info_report` WHERE pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$totalcount = $datacash[0]['cashcount'] + $datacredit[0]['creditcount'] + $dataonac[0]['onaccount'] + $datamealpass[0]['mealpass'] + $datapass[0]['pass'];

				$cashvat =  $datacash[0]['cashvat'];
				$cashgst =  $datacash[0]['cashgst'];
				$cashstax =  $datacash[0]['cashstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalcash = ($datacash[0]['grandtotalcash'] + $datacash[0]['total_round']);
				} else{
					$totalcash = ($datacash[0]['grandtotalcash'] + $datacash[0]['total_round']) - ($cashvat + $cashgst) - ($cashstax) ;
				}

				$creditvat =  $datacredit[0]['creditvat'];
				$creditgst =  $datacredit[0]['creditgst'];
				$creditstax =  $datacredit[0]['creditstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalcredit = ($datacredit[0]['grandtotalcredit'] + $datacredit[0]['total_round']);
				} else{
					$totalcredit = ($datacredit[0]['grandtotalcredit'] + $datacredit[0]['total_round']) - ($creditvat + $creditgst) - ($creditstax);
				}

				$onacvat =  $dataonac[0]['onacvat'];
				$onacgst =  $dataonac[0]['onacgst'];
				$onacstax =  $dataonac[0]['onacstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalonac = ($dataonac[0]['grandtotalonac'] + $dataonac[0]['total_round']);
				} else{
					$totalonac = ($dataonac[0]['grandtotalonac'] + $dataonac[0]['total_round']) - ($onacvat + $onacgst) - ($onacstax);
				}

				$mealvat =  $datamealpass[0]['mealvat'];
				$mealcgst =  $datamealpass[0]['mealcgst'];
				$mealstax =  $datamealpass[0]['mealstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalmeal = ($datamealpass[0]['grandtotalmeal'] + $datamealpass[0]['total_round']);
				} else{
					$totalmeal = ($datamealpass[0]['grandtotalmeal'] + $datamealpass[0]['total_round']) - ($mealvat + $mealcgst) - ($mealstax);
				}

				$passvat =  $datapass[0]['passvat'];
				$passcgst =  $datapass[0]['passcgst'];
				$passstax =  $datapass[0]['passstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalpass = ($datapass[0]['grandtotalpass'] + $datapass[0]['total_round']);
				} else{
					$totalpass = ($datapass[0]['grandtotalpass'] + $datapass[0]['total_round']) - ($passvat + $passcgst) - ($passstax);
				}

				$totalvat = $cashvat + $creditvat + $onacvat + $mealvat + $passvat; 
				$totalgst = $cashgst + $creditgst + $onacgst + $mealcgst + $passcgst;
				$totalstax = $cashstax + $creditstax + $onacstax + $mealstax + $passstax;
				$totalnetsale = $totalcash + $totalcredit + $totalonac + $totalmeal + $totalpass;

				$date_array[] = array(
					'totalcount' => $totalcount,
					'totalvat' => $totalvat,
					'totalgst' => $totalgst,
					'totalstax' => $totalstax,
					'grandtotalcash' => $totalcash,
					'grandtotalcredit' => $totalcredit,
					'grandtotalonac' => $totalonac,
					'grandtotalmeal' => $totalmeal,
					'grandtotalpass' => $totalpass,
					'totaladvance' => $dataadvance[0]['total_advance'],
					'total' => $totalnetsale + $dataadvance[0]['total_advance'],
					'date' => date('d-m-Y', strtotime($date)),
				);
			}
			$final_data[] = array(
				'date_array' => $date_array,
			);
		}

		$data['finaldatas'] = $final_data;

		$data['token'] = $this->session->data['token'];

		$data['action'] = $this->url->link('catalog/reportdaywise', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/reportdaywise_form', $data));
	}


	public function prints(){
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$finaldatas = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$date_array = array();
			$datacash = array();
			$datacredit = array();
			$dataonac = array();
			$datamealpass = array();
			$datapass = array();
			foreach($dates as $date){
				$datacash = $this->db->query("SELECT count(*) as cashcount , SUM(vat) as cashvat, SUM(gst) as cashgst, SUM(stax) as cashstax, SUM(grand_total) as grandtotalcash, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pay_cash <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datacredit = $this->db->query("SELECT count(*) as creditcount , SUM(vat) as creditvat, SUM(gst) as creditgst, SUM(stax) as creditstax, SUM(grand_total) as grandtotalcredit, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pay_card <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$dataonac = $this->db->query("SELECT count(*) as onaccount , SUM(vat) as onacvat, SUM(gst) as onacgst, SUM(stax) as onacstax, SUM(grand_total) as grandtotalonac, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE onac <> '0.00' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datamealpass = $this->db->query("SELECT count(*) as mealpass , SUM(vat) as mealvat, SUM(gst) as mealcgst, SUM(stax) as mealstax, SUM(grand_total) as grandtotalmeal, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE mealpass <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$datapass = $this->db->query("SELECT count(*) as pass , SUM(vat) as passvat, SUM(gst) as passcgst, SUM(stax) as passstax, SUM(grand_total) as grandtotalpass, SUM(roundtotal) as total_round FROM `oc_order_info_report` WHERE pass <> '0' AND pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$dataadvance = $this->db->query("SELECT SUM(advance_amount) as total_advance FROM `oc_order_info_report` WHERE pay_method = '1' AND `bill_date` = '".$date."' AND food_cancel <> '1' AND liq_cancel <> '1'")->rows;

				$totalcount = $datacash[0]['cashcount'] + $datacredit[0]['creditcount'] + $dataonac[0]['onaccount'] + $datamealpass[0]['mealpass'] + $datapass[0]['pass'];

				$cashvat =  $datacash[0]['cashvat'];
				$cashgst =  $datacash[0]['cashgst'];
				$cashstax =  $datacash[0]['cashstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalcash = ($datacash[0]['grandtotalcash'] + $datacash[0]['total_round']);
				} else{
					$totalcash = ($datacash[0]['grandtotalcash'] + $datacash[0]['total_round']) - ($cashvat + $cashgst) - ($cashstax) ;
				}

				$creditvat =  $datacredit[0]['creditvat'];
				$creditgst =  $datacredit[0]['creditgst'];
				$creditstax =  $datacredit[0]['creditstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalcredit = ($datacredit[0]['grandtotalcredit'] + $datacredit[0]['total_round']);
				} else{
					$totalcredit = ($datacredit[0]['grandtotalcredit'] + $datacredit[0]['total_round']) - ($creditvat + $creditgst) - ($creditstax);
				}

				$onacvat =  $dataonac[0]['onacvat'];
				$onacgst =  $dataonac[0]['onacgst'];
				$onacstax =  $dataonac[0]['onacstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalonac = ($dataonac[0]['grandtotalonac'] + $dataonac[0]['total_round']);
				} else{
					$totalonac = ($dataonac[0]['grandtotalonac'] + $dataonac[0]['total_round']) - ($onacvat + $onacgst) - ($onacstax);
				}

				$mealvat =  $datamealpass[0]['mealvat'];
				$mealcgst =  $datamealpass[0]['mealcgst'];
				$mealstax =  $datamealpass[0]['mealstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalmeal = ($datamealpass[0]['grandtotalmeal'] + $datamealpass[0]['total_round']);
				} else{
					$totalmeal = ($datamealpass[0]['grandtotalmeal'] + $datamealpass[0]['total_round']) - ($mealvat + $mealcgst) - ($mealstax);
				}

				$passvat =  $datapass[0]['passvat'];
				$passcgst =  $datapass[0]['passcgst'];
				$passstax =  $datapass[0]['passstax'];
				if($this->model_catalog_order->get_settings('INCLUSIVE') == '1'){
					$totalpass = ($datapass[0]['grandtotalpass'] + $datapass[0]['total_round']);
				} else{
					$totalpass = ($datapass[0]['grandtotalpass'] + $datapass[0]['total_round']) - ($passvat + $passcgst) - ($passstax);
				}

				$totalvat = $cashvat + $creditvat + $onacvat + $mealvat + $passvat; 
				$totalgst = $cashgst + $creditgst + $onacgst + $mealcgst + $passcgst;
				$totalstax = $cashstax + $creditstax + $onacstax + $mealstax + $passstax;
				$totalnetsale = $totalcash + $totalcredit + $totalonac + $totalmeal + $totalpass;

				$date_array[] = array(
					'totalcount' => $totalcount,
					'totalvat' => $totalvat,
					'totalgst' => $totalgst,
					'totalstax' => $totalstax,
					'grandtotalcash' => $totalcash,
					'grandtotalcredit' => $totalcredit,
					'grandtotalonac' => $totalonac,
					'grandtotalmeal' => $totalmeal,
					'grandtotalpass' => $totalpass,
					'totaladvance' => $dataadvance[0]['total_advance'],
					'total' => $totalnetsale + $dataadvance[0]['total_advance'],
					'date' => date('d-m-Y', strtotime($date)),
				);
			}
			$final_data[] = array(
				'date_array' => $date_array,
			);

			$total = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}

			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),35)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Daily Sales Summary Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			    foreach($finaldatas as $finaldata) {
				  	$printer->text(str_pad("DATE",11)."".str_pad("CASH",7)."".str_pad("CARD",5)."".str_pad("ONACC",6)."".str_pad("MEAL PASS",7)."".str_pad("PASS",5)."".str_pad("ADVANCE",5)."".str_pad("N.SALE",7)."".str_pad("VAT",5)."".str_pad("GST",5)."".str_pad("SC",5)."T.BILL");
				  	$printer->feed(1);
				  	$printer->text("------------------------------------------------");
				  	$printer->feed(1);
			  		foreach($finaldata['date_array'] as $data) {
			  			$printer->text(str_pad($data['date'],11)."".str_pad($data['grandtotalcash'],7)."".str_pad($data['grandtotalcredit'],5)."".str_pad($data['grandtotalonac'],7)."".str_pad($data['grandtotalmeal'],7)."".str_pad($data['grandtotalpass'],7)."".str_pad($data['totaladvance'],7)."".str_pad($data['total'] ,7)."".str_pad($data['totalvat'],5)."".str_pad($data['totalgst'],5)."".str_pad($data['totalstax'],5)."".$data['totalcount']);
			  			$printer->feed(1);
			  			$printer->text("------------------------------------------------");
			  			$printer->feed(1);
			  		}
				 	$printer->feed(1);	
				}
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}		
	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}