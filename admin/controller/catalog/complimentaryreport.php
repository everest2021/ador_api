<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogComplimentaryreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/complimentaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/complimentaryreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/complimentaryreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['export'] = $this->url->link('catalog/complimentaryreport/export', 'token=' . $this->session->data['token'] . $url, true);


		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$date_array = array();
			$datacash = array();
			$datacash = $this->db->query("SELECT *,SUM(qty) as qty ,SUM(amt) as amt FROM `oc_order_items_report` oit LEFT join `oc_order_info_report` oi  ON (oit.`order_id` = oi.`order_id`) WHERE oi.`pay_method` = '1' AND oi.`bill_date` >= '".$start_date."'AND oi.`bill_date` <= '".$end_date."' AND oi.`food_cancel` <> '1' AND oi.`liq_cancel` <> '1' AND oit.`cancelstatus` <> '1' AND oit.`complimentary_status` <> '0' GROUP BY oit.date,oit.order_id,oit.code")->rows;
				foreach($datacash as $dkey => $dvalue){
					$date_array[] = array(
						'bill_date' => $dvalue['bill_date'],
						'table_no' => $dvalue['table_id'],
						'ref_no' => $dvalue['order_id'],
						'item_name' => $dvalue['name'],
						'qty' => $dvalue['qty'],
						'rate' => $dvalue['rate'],
						'amt' => $dvalue['amt'],
						'grand_total' => $dvalue['grand_total'],
						'resion' => $dvalue['complimentary_resion'],
						
					);
				}
			$final_data[] = array(
				'date_array' => $date_array,
			);
		}

		$data['finaldatas'] = $final_data;

		$data['token'] = $this->session->data['token'];

		$data['action'] = $this->url->link('catalog/complimentaryreport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/complimentaryreport_form', $data));
	}


	public function export(){
		//echo'innnnn';exit;
		$this->load->model('catalog/order');
		$this->load->language('catalog/complimentaryreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/complimentaryreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();
		
		$startdate = strtotime($data['startdate']);
		$enddate =  strtotime($data['enddate']);
		$start_date = date('Y-m-d', $startdate);
		$end_date = date('Y-m-d', $enddate);
		$dates = $this->GetDays($start_date,$end_date);
		$date_array = array();
		$datacash = array();
			$datacash = $this->db->query("SELECT *,SUM(qty) as qty ,SUM(amt) as amt FROM `oc_order_items_report` oit LEFT join `oc_order_info_report` oi  ON (oit.`order_id` = oi.`order_id`) WHERE oi.`pay_method` = '1' AND oi.`bill_date` >= '".$start_date."'AND oi.`bill_date` <= '".$end_date."' AND oi.`food_cancel` <> '1' AND oi.`liq_cancel` <> '1' AND oit.`cancelstatus` <> '1' AND oit.`complimentary_status` <> '0' GROUP BY oit.date,oit.order_id,oit.code")->rows;
			foreach($datacash as $dkey => $dvalue){
				$date_array[] = array(
					'bill_date' => $dvalue['bill_date'],
					'table_no' => $dvalue['table_id'],
					'ref_no' => $dvalue['order_id'],
					'item_name' => $dvalue['name'],
					'qty' => $dvalue['qty'],
					'rate' => $dvalue['rate'],
					'amt' => $dvalue['amt'],
					'grand_total' => $dvalue['grand_total'],
					'resion' => $dvalue['complimentary_resion'],
					
				);
			}

		$data['token'] = $this->session->data['token'];

		
		$fp = fopen(DIR_DOWNLOAD . "Complimentaryreport.csv", "w"); 
		$row = array();
		$row = $date_array;
		$line = "";
		$comma = "";
		
		//$line .= $comma . '"' . str_replace('"', '""', "Sr. No") . '"';
		//$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Ref No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Table No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Bill Date") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Item Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Quantity") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Amount") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Grand Total") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Reason") . '"';
		$comma = ",";
		
		
		$comma = ",";

		$line .= "\n";
		fputs($fp, $line);
		$i ='1';
		foreach($row as $key => $value) { 
			// echo'<pre>';
			// print_r($value);
			// exit();	
			$comma = "";
			$line = "";
			//$line .= $comma . '"' . str_replace('"', '""', $i) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['ref_no'])) . '"';
			$comma = ",";
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['table_no'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['bill_date'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['item_name'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['qty'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['rate'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['amt'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['grand_total'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['resion'])) . '"';
			
			$i++;
			$line .= "\n";
			//echo $line;exit;
			fputs($fp, $line);
		}
		//echo $line;exit;
		fclose($fp);
		$filename = "Complimentaryreport.csv";
		$file_path = DIR_DOWNLOAD . $filename;
		//$file_path = '/Library/WebServer/Documents/riskmanagement/'.$filename;
		$html = file_get_contents($file_path);
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;
		exit;
	}


	public function prints(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/complimentaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		date_default_timezone_set("Asia/Kolkata");
		$finaldatas = array();

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/complimentaryreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;

		//if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($data['startdate']);
			$enddate =  strtotime($data['enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$date_array = array();
			$datacash = array();
			//foreach($dates as $date){
			$datacash = $this->db->query("SELECT *,SUM(qty) as qty ,SUM(amt) as amt FROM `oc_order_items_report` oit LEFT join `oc_order_info_report` oi  ON (oit.`order_id` = oi.`order_id`) WHERE oi.`pay_method` = '1' AND oi.`bill_date` >= '".$start_date."'AND oi.`bill_date` <= '".$end_date."' AND oi.`food_cancel` <> '1' AND oi.`liq_cancel` <> '1' AND oit.`cancelstatus` <> '1' AND oit.`complimentary_status` <> '0' GROUP BY oit.date,oit.order_id,oit.code")->rows;
				foreach($datacash as $dkey => $dvalue){
					$date_array[] = array(
						'bill_date' => $dvalue['bill_date'],
						'table_no' => $dvalue['table_id'],
						'ref_no' => $dvalue['order_id'],
						'item_name' => $dvalue['name'],
						'qty' => $dvalue['qty'],
						'rate' => $dvalue['rate'],
						'amt' => $dvalue['amt'],
						'grand_total' => $dvalue['grand_total'],
						'resion' => $dvalue['complimentary_resion'],
						
					);
				}
		// 		$final_data[] = array(
		// 			'date_array' => $date_array,
		// 		);
		// //}

		// $data['finaldatas'] = $final_data;

		// echo'<pre>';
		// print_r($data['finaldatas']);
		// exit;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}

			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),35)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Complimentary Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$start_date,30)."To :".$end_date);
			  	$printer->feed(2);
			 //  	echo'<pre>';
				// print_r($date_array);
				// exit;
			    //foreach($data['finaldatas'] as $finaldata) {
				  	$printer->text(str_pad("REF.NO",7)."".str_pad("TBL.NO",7)."".str_pad("B.DATE",7)."".str_pad("I.NAME",7)."".str_pad("QTY",5)."".str_pad("RATE",6)."".str_pad("AMT",7)."".str_pad("G.TOTAL",7)."".str_pad("RESION",7));
				  	$printer->feed(1);
				  	$printer->text("------------------------------------------------");
				  	$printer->feed(1);
			  		foreach($date_array as $data) {
			  			$printer->text(str_pad($data['ref_no'],11)."".str_pad($data['table_no'],7)."".str_pad($data['bill_date'],5)."".str_pad($data['item_name'],7)."".str_pad($data['qty'],7)."".str_pad($data['rate'],7)."".str_pad($data['amt'],7)."".str_pad($data['grand_total'] ,7)."".str_pad($data['resion'],5));
			  			$printer->feed(1);
			  			$printer->text("------------------------------------------------");
			  			$printer->feed(1);
			  		}
				 	//$printer->feed(1);	
				//}
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}