<?php
class ControllerCatalogPurchaseoutward extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/purchaseoutward');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseoutward');

		$this->getList();
	}
	public function add() {
		$this->load->language('catalog/purchaseoutward');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseoutward');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateForm())) {
			// echo "<pre>";
			// print_r($this->request->post);
			// //echo(date('h:i:s:a', strtotime($this->request->post['delivery_time'])));
			// exit();

			$this->model_catalog_purchaseoutward->addItem($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/purchaseoutward');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseoutward');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			$this->model_catalog_purchaseoutward->editItem($this->request->get['item_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/purchaseoutward');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseoutward');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_purchaseoutward->deleteItem($item_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_customername'])) {
			$filter_customername = $this->request->get['filter_customername'];
		} else {
			$filter_customername = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['order_id'])) {
			$data['order_id'] = $this->request->get['order_id'];
		} else {
			$data['order_id'] = '';
		}

		// echo $filter_status;
		// exit();

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/purchaseoutward/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/purchaseoutward/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		$filter_data = array(
			'filter_date' => $filter_date,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$item_total = $this->model_catalog_purchaseoutward->getTotalItems($filter_data);

		$results = $this->model_catalog_purchaseoutward->getItems($filter_data);
		// echo "<pre>";
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$storename = $this->db->query("SELECT outlet_name FROM oc_outlet WHERE outlet_id = '".$result['outlet_id']."'")->row;
			$data['items'][] = array(
				'item_id'  			=> $result['id'],
				'ow_number' 		=> $result['ow_prefix'].$result['ow_number'],
				'date' 				=> $result['ow_date'],
				'store'				=> $storename['outlet_name']
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['advance'])) {
			$data['error_advance'] = $this->error['advance'];
		} else {
			$data['error_advance'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_ow_date'] = $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . '&sort=ow_date' . $url, true);
		$data['sort_ow_number'] = $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . '&sort=ow_number' . $url, true);
		$data['sort_outlet_id'] = $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . '&sort=sort_outlet_id' . $url, true);
		
		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_date'] = $filter_date;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['status'] = ['0' => 'Pending', '1' => 'Done'];
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/purchaseoutward_list', $data));
	}

	protected function getForm() {
		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['advance'])) {
			$data['error_advance'] = $this->error['advance'];
		} else {
			$data['error_advance'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['item_id'])) {
			$data['action'] = $this->url->link('catalog/purchaseoutward/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/purchaseoutward/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $this->request->get['item_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/purchaseoutward', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_purchaseoutward->getItem($this->request->get['item_id']);
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($item_info)) {
			$data['date'] = $item_info['ow_date'];
		} else {
			$data['date'] = '';
		}

		if (isset($this->request->post['store'])) {
			$data['store'] = $this->request->post['store'];
		} elseif (!empty($item_info)) {
			$data['store'] = $item_info['store'];
		} else {
			$data['store'] = '';
		}

		if (isset($this->request->get['item_id'])){
			$data['testdatas'] = $this->db->query("SELECT * FROM oc_purchase_order_items WHERE po_id = '".$this->request->get['item_id']."'")->rows;
		} else{
			$data['testdatas'] = array();
		}

		$data['stores'] = $this->db->query("SELECT * FROM oc_outlet WHERE status = 1")->rows;

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/purchaseoutward_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/purchaseoutward')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['pos'] == ''){
			$this->error['warning'] = 'Please enter items';
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/purchaseoutward')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/purchaseoutward');

		return !$this->error;
	}

	public function autocompleteitemname() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {

			$results = $this->db->query("SELECT * FROM oc_item WHERE item_name LIKE '%" .$this->request->get['filter_item_name']. "%' AND is_liq = '0' LIMIT 0,5")->rows;

			foreach ($results as $result) {

				$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['vat']."'");
				if($tax1->num_rows > 0){
					$taxvalue1 = $tax1->row['tax_value'];
				} else{
					$taxvalue1 = '0';
				}

				$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['tax2']."'");
				if($tax2->num_rows > 0){
					$taxvalue2 = $tax2->row['tax_value'];
				} else{
					$taxvalue2 = '0';
				}

				$json[] = array(
					'item_id' => $result['item_id'],
					'item_code' => $result['item_code'],
					'rate' => $result['rate_1'],
					'tax1'       	=> $taxvalue1,
					'tax2'       	=> $taxvalue2,
					'subcategoryid' => $result['item_sub_category_id'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function getPos() {
		$json = array();

		if (isset($this->request->get['store_id'])) {

			$results = $this->db->query("SELECT * FROM oc_purchase_order WHERE outlet_id = '".$this->request->get['store_id']."' AND status = 0")->rows;

			$jsons[] = array(
							'id' => '',
							'po_number' => 'Please Select'
						);

			foreach ($results as $result) {
				$jsons[] = array(
					'id' => $result['id'],
					'po_number' => $result['po_prefix'].$result['po_number']
				);
			}
		}

		$sort_order = array();

		foreach ($jsons as $key => $value) {
			$sort_order[$key] = $value['po_number'];
		}
		//array_multisort($sort_order, SORT_ASC, $jsons);
		$json['po_data'] = $jsons;
		$this->response->setOutput(json_encode($json));
	}

	public function getPosData() {
		$json = array();

		if (isset($this->request->get['pos_id'])) {

			$results = $this->db->query("SELECT item_id, item_name, quantity, notes, po_prefix, po_number FROM oc_purchase_order po LEFT JOIN oc_purchase_order_items pi ON(po.`id`= pi.`po_id`) WHERE po.`id` = '".$this->request->get['pos_id']."' AND status = 0")->rows;

			foreach ($results as $result) {
				$json[] = array(
					'item_id' 	=> $result['item_id'],
					'item_name' => $result['item_name'],
					'quantity' 	=> $result['quantity'],
					'notes' 	=> $result['notes'],
					'po_number' => $result['po_prefix'].$result['po_number']
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['po_number'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}