<?php
class ControllerCatalogSportGroup extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/sport_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sport_group');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/sport_group');
		/*echo '<pre>';
		print_r($this->request->post);
		exit;
*/
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sport_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_sport_group->addSportGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_participant_type'])) {
				$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/sport_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sport_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_sport_group->editSportGroup($this->request->get['sport_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_participant_type'])) {
				$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/sport_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/sport_group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $sport_group_id) {
				$this->model_catalog_sport_group->deleteSportGroup($sport_group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_participant_type'])) {
				$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = null;
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$filter_sport_type = $this->request->get['filter_sport_type'];
		} else {
			$filter_sport_type = null;
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$filter_participant_type = $this->request->get['filter_participant_type'];
		} else {
			$filter_participant_type = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/sport_group/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/sport_group/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['sport_groups'] = array();

		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_sport_type' => $filter_sport_type,
			'filter_participant_type' => $filter_participant_type,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_group_total = $this->model_catalog_sport_group->getTotalSportGroups();

		$results = $this->model_catalog_sport_group->getSportGroups($filter_data);

		foreach ($results as $result) {
			$data['sport_groups'][] = array(
				'sport_group_id' => $result['sport_group_id'],
				'sport_group'        => $result['sport_group'],
				'sport_type'        => $result['sport_type'],
				'participant_type'        => $result['participation'],
				'edit'        => $this->url->link('catalog/sport_group/edit', 'token=' . $this->session->data['token'] . '&sport_group_id=' . $result['sport_group_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['sport_types'] = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);

		$data['part_types'] = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_sport_type'] = $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . '&sort=sport_type' . $url, true);
		$data['sort_participant_type'] = $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . '&sort=participant_type' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_group_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_group_total - $this->config->get('config_limit_admin'))) ? $sport_group_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_group_total, ceil($sport_group_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_name_id'] = $filter_name_id;
		$data['filter_sport_type'] = $filter_sport_type;
		$data['filter_participant_type'] = $filter_participant_type;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/sport_group_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['sport_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['sport_type_id'])) {
			$data['error_sport_type_id'] = $this->error['sport_type_id'];
		} else {
			$data['error_sport_type_id'] = '';
		}

		if (isset($this->error['participation_id'])) {
			$data['error_participation_id'] = $this->error['participation_id'];
		} else {
			$data['error_participation_id'] = '';
		}
		
		if (isset($this->error['sports_data'])) {
			$data['error_sports_data'] = $this->error['sports_data'];
		} else {
			$data['error_sports_data'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_sport_type'])) {
			$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
		}

		if (isset($this->request->get['filter_participant_type'])) {
			$url .= '&filter_participant_type=' . $this->request->get['filter_participant_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['sport_group_id'])) {
			$data['action'] = $this->url->link('catalog/sport_group/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/sport_group/edit', 'token=' . $this->session->data['token'] . '&sport_group_id=' . $this->request->get['sport_group_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/sport_group', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['sport_group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$sport_group_info = $this->model_catalog_sport_group->getSportGroup($this->request->get['sport_group_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['sport_group_id'])) {
			$data['sport_group_id'] = $this->request->get['sport_group_id'];
		} elseif (!empty($sport_group_info)) {
			$data['sport_group_id'] = $sport_group_info['sport_group_id'];
		} else {
			$data['sport_group_id'] = 0;
		}

		if (isset($this->request->post['sport_group'])) {
			$data['sport_group'] = $this->request->post['sport_group'];
		} elseif (!empty($sport_group_info)) {
			$data['sport_group'] = $sport_group_info['sport_group'];
		} else {
			$data['sport_group'] = '';
		}

		if (isset($this->request->post['sport_type_id'])) {
			$data['sport_type_id'] = $this->request->post['sport_type_id'];
		} elseif (!empty($sport_group_info)) {
			$data['sport_type_id'] = $sport_group_info['sport_type_id'];
		} else {
			$data['sport_type_id'] = '1';
		}

		//echo $data['part_re'];
		//exit;
		$data['sport_types'] = array(
			'' => 'Please Select',
			'1' => 'कला',
			'2' => 'क्रिडा',
		);

		$data['part_types'] = array(
			'' => 'Please Select',
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);

		if (isset($this->request->post['participation_id'])) {
			$data['participation_id'] = $this->request->post['participation_id'];
		} elseif (!empty($sport_group_info)) {
			$data['participation_id'] = $sport_group_info['participation_id'];
		} else {
			$data['participation_id'] = '1';
		}


		if (isset($this->request->post['birthdate_date_from'])) {
			$data['from_date'] = $this->request->post['birthdate_date_from'];
		} elseif (!empty($sport_group_info)) {
			if($sport_group_info['birthdate_date_from'] != '0000-00-00'){
				$data['from_date'] = date('d-m-Y', strtotime($sport_group_info['birthdate_date_from']));
			} else {
				$data['from_date'] = '';
			}
		} else {
			$data['from_date'] = '';//'01-01-1950';
		}

		
		if (isset($this->request->post['birthdate_date_to'])) {
			$data['to_date'] = $this->request->post['birthdate_date_to'];
		} elseif (!empty($sport_group_info)) {
			if($sport_group_info['birthdate_date_to'] != '0000-00-00'){
				$data['to_date'] = date('d-m-Y', strtotime($sport_group_info['birthdate_date_to']));
			} else {
				$data['to_date'] = '';
			}
		} else {
			$data['to_date'] = '';//'31-12-2017';
		}
		
		if (isset($this->request->post['sport_datas'])) {
			$data['sport_datas'] = $this->request->post['sport_datas'];
		} elseif (!empty($sport_group_info)) {
			$sport_groups = $this->db->query("SELECT * FROM `oc_sport_group_sports` WHERE `sport_group_id` = '".$data['sport_group_id']."' ")->rows;
			$sport_datas = array();
			foreach($sport_groups as $skey => $svalue){
				$sport_datas[] = $svalue['sport_id'];
			}
			$data['sport_datas'] = $sport_datas;
		} else {
			$data['sport_datas'] = array();
		}

		$sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '".$data['sport_type_id']."' AND `participation_id` = '".$data['participation_id']."' ")->rows;
		// echo '<pre>';
		// print_r($sportss);
		// exit;
		$sports = array();
		foreach($sportss as $kkey => $kvalue){
			$sports[$kvalue['sport_id']] = $kvalue['name'];
		}
		$data['sports'] = $sports;

		$kala_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '1' ")->rows;
		foreach($kala_sportss as $kkey => $kvalue){
			$kala_sports[$kvalue['sport_id']] = $kvalue['name'];
		}
		$data['kala_sports'] = $kala_sports;

		$krida_sportss = $this->db->query("SELECT * FROM `oc_sport` WHERE `sport_type_id` = '2' ")->rows;
		foreach($krida_sportss as $kkey => $kvalue){
			$krida_sports[$kvalue['sport_id']] = $kvalue['name'];
		}
		$data['krida_sports'] = $krida_sports;

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/sport_group_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/sport_group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['sport_group']) < 2) || (utf8_strlen($this->request->post['sport_group']) > 255)) {
			$this->error['name'] = 'Please Enter Sport Group Name';
		}

		if ($this->request->post['sport_type_id'] == '') {
			$this->error['sport_type_id'] = 'Please Select Sport Type';
		}

		if ($this->request->post['participation_id'] == '') {
			$this->error['participation_id'] = 'Please Select Participant Type';
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/sport_group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		// foreach ($this->request->post['selected'] as $sport_group_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsBySportGroupId($sport_group_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/sport_group');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/sport_group')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/sport_group');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_sport_group->getSportGroups($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'sport_group_id' => $result['sport_group_id'],
					'sport_group'        => strip_tags(html_entity_decode($result['sport_group'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['sport_group'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getsports() {
		$json = array();
		$sport_datas = array();
		if (isset($this->request->get['filter_sport_type']) && isset($this->request->get['filter_participant_type'])) {
			$data['filter_sport_type'] = $this->request->get['filter_sport_type'];
			$data['filter_participant_type'] = $this->request->get['filter_participant_type'];
			
			$filter_sport_type = $this->request->get['filter_sport_type'];
			$filter_participant_type = $this->request->get['filter_participant_type'];
			
			$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
			if($filter_sport_type){
				$sql .= " AND `sport_type_id` = '".$filter_sport_type."' ";
			}
			if($filter_participant_type){
				$sql .= " AND `participation_id` = '".$filter_participant_type."' ";
			}
			$sql .= " ORDER BY `sport_type` ";
			//$this->log->write($sql);
			$sportss = $this->db->query($sql)->rows;
			foreach($sportss as $skey => $svalue){
				$sport_datas[$svalue['sport_id']] = $svalue['name'];
			}
			//$class = 'even';
            $html = '';
            //$html .= '<div class="well well-sm" style="height: 350px; overflow: auto;">';
            foreach ($sport_datas as $skey => $svalue) {
            	//$class = ($class == 'even' ? 'odd' : 'even');
            	$html .= '<div class="checkbox">';
            		$html .= '<label>';
	              		$html .= '<input class="checkboxclass" type="checkbox" name="sport_datas['.$skey.']" value="'.$svalue.'" />';
	              		$html .= $svalue;
            		$html .= '</label>';
            	$html .= '</div>';
            }
            //$html .= '</div>';
		}
		$json['html'] = $html;
		$this->response->setOutput(json_encode($json));
	}
}