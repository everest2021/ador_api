<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogAdvanceReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/advancereport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/advancereport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_status'])){
			$data['filter_status'] = $this->request->post['filter_status'];
		}
		else{
			$data['filter_status'] = '';
		}

		$advanceAllDatas = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			// foreach($dates as $date){ 
			// 	$sql = "SELECT * FROM oc_advance oa LEFT JOIN oc_advance_item oai ON (oa.`id` = oai.`advance_id`) WHERE booking_date = '".$date."'";
			// 	if($this->request->post['filter_status'] != ''){
			// 		$sql .= "AND status = '".$this->request->post['filter_status']."'";
			// 	}
			// 	$advancedata[$date] = $this->db->query($sql)->rows;
			// 	// $advancemain = $this->db->query("SELECT * FROM oc_advance WHERE booking_date = '".$date."' AND status = '".$this->request->post['filter_status']."'")->rows;
			// 	// foreach($advancemain as $key){
			// 		// $advanceitem = $this->db->query("SELECT * FROM oc_advance_item WHERE advance_id = '".$key['id']."'")->rows;
			// 	// }
			// }

			$sql = "SELECT * FROM oc_advance WHERE booking_date >= '".$start_date."' AND booking_date <= '".$end_date."'";
			if($this->request->post['filter_status'] != ''){
				$sql .= "AND status = '".$this->request->post['filter_status']."'";
			}
			$advancedatas = $this->db->query($sql)->rows;
			foreach($advancedatas as $advancedata){
				$advanceitems[] = $this->db->query("SELECT * FROM oc_advance_item WHERE advance_id = '".$advancedata['id']."'")->rows;
				foreach($advanceitems as $advanceitem){
					$advanceAllDatas[$advancedata['id']] = [
								'advancedata' => $advancedata,
								'advanceitem' => $advanceitem
							  ];
				}
			}
		}

		$data['advancedatas'] = $advanceAllDatas;
		// echo "<pre>";
		// print_r($advanceAllDatas);
		// exit();

		$data['status'] = ['0' => 'Pending', '1' => 'Done'];
		$data['action'] = $this->url->link('catalog/advancereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/advancereport', $data));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}