<?php
//require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
//use Dompdf\Dompdf;
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogstockinvoice extends Controller {
	private $error = array();

	public function index() {
		//$this->load->language('catalog/daysummaryreport');
		//$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('catalog/daysummaryreport');
		$this->pdfprints();
	}

	public function prints(){

		if(isset($this->request->get['filter_category'])){
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '99';
		}

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			/************************************** User Wise Start *************************************************/
			$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, bill_date, is_liq, onac FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oi.`login_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderdata) {
				$orderdataval = array();
				$orderitemval = array();
				$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows;
				$netsale = 0;
				$pay_cash = 0;
				$pay_card = 0;
				$pay_online = 0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$duplicatebill = 0;
				$duplicatecount = 0;
				$advanceamount = 0;
				$cancelkot = 0;
				$cancelamountkot = 0;
				$cancelbot = 0;
				$cancelamountbot = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$cancelledbill = 0;
				$cancelledbillcount = 0;
				$complimentarybill = 0;
				$complimentarycount = 0;
				foreach ($orderdatatest as $key) {
					$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
					$pay_cash = $pay_cash + $key['pay_cash'];
					$pay_card = $pay_card + $key['pay_card'];
					$pay_online = $pay_online + $key['pay_online'];
					$onac = $onac + $key['onac'];
					$mealpass = $mealpass + $key['mealpass'];
					$pass = $pass + $key['pass'];

					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingbill ++;
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountbillcount ++;
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					}

					if($key['duplicate'] == '1'){
						$duplicatecount ++;
						$duplicatebill = $duplicatebill + $key['grand_total'];
					}

					$advanceamount = $advanceamount + $key['advance_amount'];
				}

				$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY `order_id`";
				$orderdatabill = $this->db->query($sql)->rows;
				//echo "<pre>";print_r($orderdatabill);exit;
				foreach ($orderdatabill as $key => $value) {
					if($value['cancel_status'] == '1'){
						$cancelledbillcount ++;
						$cancelledbill = $cancelledbill + $value['grand_total'];
					}

					if($value['complimentary_status'] == '1'){
						$complimentarycount ++;
						$complimentarybill = $complimentarybill + $value['grand_total'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";	
				$orderdatatest = $this->db->query($sql)->rows; 
				foreach ($orderdatatest as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalnc ++;
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					}
				}
				$orderdataval = array(
					'netsale' => $netsale,
					'total_cash' => $pay_cash,
					'total_card' => $pay_card,
					'total_pay_online' => $pay_online,
					'onac' => $onac,
					'total_mealpass' => $mealpass,
					'total_pass' => $pass,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'discountbillcount' => $discountbillcount,
					'discountamount' => $discountamount,
					'duplicate' => $duplicatebill,
					'duplicatecount' => $duplicatecount,
					'advanceamount' => $advanceamount,
					'cancelkot' => $cancelkot,
					'cancelamountkot' => $cancelamountkot,
					'cancelbot' => $cancelbot,
					'cancelamountbot' => $cancelamountbot,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'cancelledcount' => $cancelledbillcount,
					'cancelledbill' => $cancelledbill,
					'complimentarycount' => $complimentarycount,
					'complimentarybill' => $complimentarybill,
					'login_id' => $orderdata['login_id'],
					'login_name' => $orderdata['login_name'],
				);
				$orderdataarray[$orderdata['login_id']] = array(
					'orderdata' => $orderdataval,
				);
			}
			/************************************** User Wise End *************************************************/

			/************************************** Location Wise Start *************************************************/

			$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY location";
			$orderdatalocationwises = $this->db->query($sql)->rows; 
			$totalnetsale = 0;
			$totalgst = 0;
			$totalvat = 0;
			$totalroundoff = 0;
			$totalscharge = 0;
			$totaldiscount = 0;
			$nettotalamt = 0;
			$advance = 0;
			foreach($orderdatalocationwises as $orderdata){
				$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY oi.`order_id`";
				//echo $sql;exit;
				$orderdatatestloc = $this->db->query($sql)->rows; 
				$netsale = 0;
				$vat = 0;
				$gst = 0;
				$roundoff = 0;
				$scharge = 0;
				$discount = 0;
				$nettotal = 0;
				$advance = 0;
				foreach ($orderdatatestloc as $key) {
					if($key['is_liq'] == '1' && $filter_category == '1'){
						$netsale = $netsale + $key['amt'];
						$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxliq'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
						}
						$advance = $advance + $key['advance_amount'];
					}elseif($key['is_liq'] == '0' && $filter_category == '0'){
						$netsale = $netsale + $key['amt'];
						$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
						$scharge = $scharge + $key['staxfood'];
						$discount = $discount + $key['discount_value'];
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
						} else{
							$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
						}
						$advance = $advance + $key['advance_amount'];
					} else{
						$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
						$vat = $vat + $key['vat'];
						$gst = $gst + $key['gst'];
						$roundoff = $roundoff + $key['roundtotal'];
						$scharge = $scharge + $key['stax'];
						$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$nettotal = $nettotal + $key['grand_total'] + $key['roundtotal'];
						$advance = $advance + $key['advance_amount'];
					}
				}
				$orderlocdatas[] = array(
					'location' => $key['location'],
					'netsale' => $netsale,
				);
				$totalnetsale = $totalnetsale + $netsale;
				$totalgst = $totalgst + $gst;
				$totalvat = $totalvat + $vat;
				$totalroundoff = $totalroundoff + $roundoff;
				$totalscharge = $totalscharge + $scharge;
				$totaldiscount = $totaldiscount + $discount;
				$nettotalamt = $nettotalamt + $nettotal + $advance;
			}
			$orderlocdatasamt = array(
				'totalnetsale' => $totalnetsale,
				'totalgst' => $totalgst,
				'totalvat' => $totalvat,
				'totalroundoff' => $totalroundoff,
				'totalscharge' => $totalscharge,
				'totaldiscount' => $totaldiscount,
				'nettotalamt' => $nettotalamt,
				'advance' => $advance
			);

			/************************************** Location Wise End *************************************************/

			/************************************** Payment Summary Start *************************************************/
			$totalcash = 0;
			$totalcard = 0;
			$totalpayonline =0;
			$onac = 0;
			$mealpass = 0;
			$pass = 0;
			$room = 0;
			$msr = 0;
			$total = 0;
			$cardtip = 0;
			$totalpaysumm = 0;
			$onac = 0;
			$advanceamt = 0;
			$tip_amount = 0;
			$sql = "SELECT pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, bill_date, is_liq, onac, tip FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";
			}	
			$sql .= " GROUP BY oit.`order_id`";	
			$orderdatauserwises = $this->db->query($sql)->rows;
			foreach ($orderdatauserwises as $orderpayment) {
				$totalcash = $totalcash + $orderpayment['pay_cash'];
				$totalcard = $totalcard + $orderpayment['pay_card'];
				$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
				$onac = $onac + $orderpayment['onac'];
				$mealpass = $mealpass + $orderpayment['mealpass'];
				$pass = $pass + $orderpayment['pass'];
				$advanceamt = $advance + $orderpayment['advance_amount'];
				$tip_amount = $tip_amount + $orderpayment['tip'];
				$room = 0.00;
				$msr = 0.00;
				$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
				$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
			}

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}

			$orderpaymentamt = array(
				'totalcash' => $totalcash,
				'totalcard' => $totalcard,
				'totalpayonline' => $totalpayonline,
				'onac' => $onac,
				'mealpass' => $mealpass,
				'pass' => $pass,
				'room' => $room,
				'msr' => $msr,
				'total' => $total,
				'tip_amount' => $tip_amount,
				'advanceamt' => $advanceamt,
				'advance' => $advancetotal,
				'totalpaysumm' => $totalpaysumm
			);

			/************************************** Payment Summary End *************************************************/

			/************************************** Tax Summary End *************************************************/
			// echo "<pre>";
			// print_r($orderdatauserwises);
			// exit();
				//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
			$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`tax1_value`) as tax1_value, oit.`is_liq`, oit.`tax1` FROM `oc_order_items` oit LEFT JOIN `oc_order_info` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY tax1, is_liq";
			//echo $sql;exit;
			$tests = $this->db->query($sql)->rows; 
			foreach($tests as $test){
				if($test['is_liq'] == '0'){
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'],
						'tax1_value' => $test['tax1_value']
					);
				} else{
					$testliqs[] = array(
						'tax1' => $test['tax1'],
						'amt' => $test['amt'],
						'tax1_value' => $test['tax1_value']
					);
				}
			}
			$foodnetamt = 0;
			$taxvalfood = 0;
			$liqnetamt = 0;
			$taxvalliq = 0;

			foreach($testfoods as $tkey => $tvalue){
				$foodarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$foodnetamt = $foodnetamt + $tvalue['amt'];
				$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
			}

			foreach($testliqs as $tkey => $tvalue){
				$liqarray[] = array(
					'tax1' => $tvalue['tax1'],
					'amt' => $tvalue['amt'],
					'tax1_value' => $tvalue['tax1_value'],
				);
				$liqnetamt = $liqnetamt + $tvalue['amt'];
				$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
			}

			$ordertaxamt = array(
				'foodtax' => $foodarray,
				'liqtax' => $liqarray,
				'foodnetamt' => $foodnetamt,
				'taxvalfood' => $taxvalfood,
				'liqnetamt' => $liqnetamt,
				'taxvalliq' => $taxvalliq
			);
			
			// echo "<pre>";
			// print_r($ordertaxamt);
			// exit();
			/************************************** Tax Summary End *************************************************/

			/************************************** Category Start **************************************************/
			$foodamt = '0';
			$liqamt = '0';

			if($filter_category == '0'){
				$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
			}
			if($filter_category == '1'){
				$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ")->row;
				$liqamt = $liqamt['total_liq'];
			}
			if($filter_category == '99'){
				$foodamt = $this->db->query("SELECT SUM(ftotal) as total_food FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ")->row;
				$liqamt = $this->db->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ")->row;
				$foodamt = $foodamt['total_food'];
				$liqamt = $liqamt['total_liq'];
			}

			/************************************** Category End **************************************************/

			/************************************** Sub Category Start **************************************************/

			$sql = "SELECT SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " GROUP BY subcategoryid";
			//echo $sql;exit;
			$subcategoryamts = $this->db->query($sql)->rows; 
			$totalamt = 0;
			$subcategory = array();
			foreach($subcategoryamts as $key){
				$subcategoryname = $this->db->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'")->row;
				$subcategory[] = array(
					'name' => $subcategoryname['name'],
					'amount' =>  $key['amt']
				);
				$totalamt = $totalamt + $key['amt'];
			}
			
			$catsub = array(
				'foodcat' => $foodamt,
				'liqcat' => $liqamt,
				'subcat' => $subcategory,
				'totalamt' => $totalamt
			);

			/************************************** Sub Category End **************************************************/

			/************************************** Second last part Start **************************************************/
			
			$sql = "SELECT billno FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY billno DESC LIMIT 1";
			$lastbillno = $this->db->query($sql); 

			if($lastbillno->num_rows > 0){
					$lastbillno = $lastbillno->row['billno'];			
			} else{
				$lastbillno = 0;			
			}
			// echo'<pre>';
			// print_r($lastbillno);
			// exit;

			$lastkotno = 0;
			$lastbotno = 0;

			if($filter_category == '0'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];
				} else{
					$lastkotno = 0;
				}
			}
			if($filter_category == '1'){
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];
				} else{
					$lastbotno = 0;				
				}
			}
			if($filter_category == '99'){
				$lastkotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1");
				$lastbotno = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1");
				if($lastkotno->num_rows > 0){
					$lastkotno = $lastkotno->row['kot_no'];			
				} else{
					$lastkotno = 0;			
				}
				if($lastbotno->num_rows > 0){
					$lastbotno = $lastbotno->row['kot_no'];			
				} else{
					$lastbotno = 0;			
				}
			}

			$sql = "SELECT *,oi.`order_id` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
			$orderdatalast = $this->db->query($sql)->rows; 
			$totalpendingamount = 0;
			$totalpendingbill = 0;
			$duplicate = 0;
			$duplicatecount = 0;
			$totalamountnc = 0;
			$totalnc = 0;
			$discountamount = 0;
			$discountbillcount = 0;
			$advanceamount = 0;
			$billmodify = 0;
			$qty = 0;
			$cancelamountkot = 0;
			$cancelamountbot = 0;
			$cancelkot = 0;
			$cancelbot = 0;
			foreach ($orderdatalast as $key) {
				if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
					$totalpendingamount = $totalpendingamount + $key['grand_total'];
					$totalpendingbill ++;
				}

				if($key['duplicate'] == '1'){
					$duplicate = $duplicate + $key['duplicate'];
					$duplicatecount ++;
				}

				if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
					$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
					$discountbillcount ++;
				}

				if($key['advance_amount'] != '0'){
					$advanceamount = $advanceamount + $key['advance_amount'];
				}
			}

			$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`)  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			$sql .= " ORDER BY oi.`order_id`";	
			$orderitemlast = $this->db->query($sql)->rows;
			foreach ($orderitemlast as $key) {
				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
					$cancelamountkot = $cancelamountkot + $key['amt'];
					$cancelkot ++;
				}

				if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
					$cancelamountbot = $cancelamountbot + $key['amt'];
					$cancelbot ++;
				}

				if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
					$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
					$totalnc ++;
				}
			}
			
			$cancelledbillcount = 0;
			$cancelledbill = 0;
			$sql = "SELECT SUM(oi.`grand_total`) as grand_total FROM `oc_order_info` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatabill = $this->db->query($sql);
			if ($orderdatabill->num_rows > 0) {
				$cancelledbillcount = $orderdatabill->num_rows;
				$cancelledbill = $orderdatabill->row['grand_total'];
			}

			$complimentarycount = 0;
			$complimentarybill = 0;
			$sql = "SELECT SUM(oi.`grand_total`) as grand_total FROM `oc_order_info` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
			if($filter_category != '99'){
				$sql .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$orderdatacomp = $this->db->query($sql);
			if ($orderdatacomp->num_rows > 0) {
				$complimentarycount = $orderdatacomp->num_rows;
				$complimentarybill = $orderdatacomp->row['grand_total'];
			}

			$orderitemlastval = array(
				'cancelamountkot' => $cancelamountkot,
				'cancelamountbot' => $cancelamountbot,
				'cancelkot' => $cancelkot,
				'cancelbot' => $cancelbot,
				'cancelledbill' => $cancelledbill,
				'cancelledcount' => $cancelledbillcount,
				'complimentarybill' => $complimentarybill,
				'complimentarycount' => $complimentarycount,
			);
			$orderdatalastval = array(
				'lastbill' => $lastbillno,
				'lastkot' => $lastkotno,
				'lastbot' => $lastbotno,
				'pendingbill' => $totalpendingbill,
				'pendingamount' => $totalpendingamount,
				'duplicate' => $duplicate,
				'duplicatecount' => $duplicatecount,
				'totalnc' => $totalnc,
				'totalamountnc' => $totalamountnc,
				'billmodify' => $billmodify,
				'discountamount' => $discountamount,
				'discountbillcount' => $discountbillcount,
				'advanceamount' => $advanceamount
			);

			$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
			if($filter_category != '99'){
				$sql1 .= " AND is_liq = '".$filter_category."'";	
			}
			//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
			$bill_modifiy_data = $this->db->query($sql1);
			$data['NumberOfbillmodify'] = 0;
			$data['totalbillamount'] = 0;
			if($bill_modifiy_data->num_rows > 0){
				$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
				$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
				
			}
			$orderdatalastarray = array(
				'orderdatalast' => $orderdatalastval,
				'orderitemlast' => $orderitemlastval
			);
			/************************************** Second last part End **************************************************/
			$tip = $this->db->query("SELECT SUM(tip) as tip FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($tip->num_rows > 0){
				$data['tip'] = $tip->row['tip'];
			} else{
				$data['tip'] = 0;
			}

			$persons = $this->db->query("SELECT SUM(person) as person FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ");
			if($persons->num_rows > 0){
				$data['person'] = $persons->row['person'];
			} else{
				$data['person'] = 0;
			}
		
			/************************************** Second last part End **************************************************/
			$this->load->model('catalog/order');
			try {
			   	if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
			 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
			 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
			 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
			 	} else {
			 		$connector = '';
			 	}
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Day Summary Report");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$start_date,30)."To :".$end_date);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Login Summary ",30)."Amount ");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$usertotal = 0;
			  	foreach($orderdataarray as $key => $value){
					$printer->text(str_pad("User :".$value['orderdata']['login_id'],10)."".str_pad($value['orderdata']['login_name'],10)."".str_pad("T.Sale", 10)."".$value['orderdata']['netsale']);
					$printer->feed(1);
					$printer->text("------------------------------------------------");
			  		$printer->feed(1);
					$printer->text(str_pad("Usettle Bill Amount ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Cash ",30)."".$value['orderdata']['total_cash']);
					$printer->feed(1);
					$printer->text(str_pad("Credit Card ",30)."".$value['orderdata']['total_card']);
					$printer->feed(1);
					$printer->text(str_pad("Online ",30)."".$value['orderdata']['total_pay_online']);
					$printer->feed(1);
					$printer->text(str_pad("On A/c ",30).$value['orderdata']['onac']);
					$printer->feed(1);
					$printer->text(str_pad("Meal Pass ",30)."".$value['orderdata']['total_mealpass']);
					$printer->feed(1);
					$printer->text(str_pad("Pass ",30)."".$value['orderdata']['total_pass']);
					$printer->feed(1);
					$printer->text(str_pad("Pending table :",20)."".str_pad($value['orderdata']['pendingbill'],10)."".$value['orderdata']['pendingamount']);
					$printer->feed(1);
					$printer->text(str_pad("Compliment bill ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Cancel bill ",20)."".str_pad($value['orderdata']['cancelledcount'],10).$value['orderdata']['cancelledbill']);
					$printer->feed(1);
					$printer->text(str_pad("Canceled Kot :",20)."".str_pad($value['orderdata']['cancelkot'],10)."".$value['orderdata']['cancelamountkot']);
					$printer->feed(1);
					$printer->text(str_pad("Canceled Bot :",20)."".str_pad($value['orderdata']['cancelbot'],10)."".$value['orderdata']['cancelamountbot']);
					$printer->feed(1);
					$printer->text(str_pad("NC KOT :",20)."".str_pad($value['orderdata']['totalnc'],10)."".$value['orderdata']['totalamountnc']);
					$printer->feed(1);
					$printer->text(str_pad("DUPLICATE BILL ",20)."".str_pad($value['orderdata']['duplicatecount'],10).$value['orderdata']['duplicate']);
					$printer->feed(1);
					$printer->text(str_pad("Disc Before Bill :",20)."".str_pad($value['orderdata']['discountbillcount'],10)."".$value['orderdata']['discountamount']);
					$printer->feed(1);
					$printer->text(str_pad("DUPLICATE KOT ",20)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Disc After Bill ",30)."0.00");
					$printer->feed(1);
					$printer->text(str_pad("Advance Amount (-)",30).$value['orderdata']['advanceamount']);
					$printer->feed(1);
					$usertotal = $usertotal + $value['orderdata']['total_cash'] + 
									 			  $value['orderdata']['total_card'] + $value['orderdata']['total_pay_online'] + 
									 			  $value['orderdata']['pendingamount'] + $value['orderdata']['onac'] + $value['orderdata']['advanceamount'];
					$printer->text("------------------------------------------------");
			  		$printer->feed(1);
				}
				$printer->text(str_pad("All User Totals ",30).$usertotal);
				$printer->feed(1);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Table Group ",30)."Amount ");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach($orderlocdatas as $locdata){
			  		$printer->text(str_pad($locdata['location'],30).$locdata['netsale']);
			  		$printer->feed(1);
			  	}
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Sub Total ",30).$orderlocdatasamt['totalnetsale']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("KKC Amt(+) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("SBC Amt(+) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("S-Chrg Amt(+) ",30).$orderlocdatasamt['totalscharge']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Vat(+) ",30).$orderlocdatasamt['totalvat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Gst(+) ",30).$orderlocdatasamt['totalgst']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("R-off Amt(+) ",30).$orderlocdatasamt['totalroundoff']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Discount Amt(-) ",30). $orderlocdatasamt['totaldiscount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("P-Discount Amt(-) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancel Bill Amt(-) ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance. Amt(-) ",30).$orderlocdatasamt['advance']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("Net Total ",30).$orderlocdatasamt['nettotalamt']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text("Payment Summary");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cash ",30).$orderpaymentamt['totalcash']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Credit Card ",30).$orderpaymentamt['totalcard']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Online ",30).$orderpaymentamt['totalpayonline']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("On A/c ",30).$orderpaymentamt['onac']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Meal Pass ",30).$orderpaymentamt['mealpass']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Pass ",30).$orderpaymentamt['pass']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Room Service ",30).$orderpaymentamt['room']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("MSR Card ",30).$orderpaymentamt['msr']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance Amount (-) ",30).$orderpaymentamt['advanceamt']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total ",30).$orderpaymentamt['total']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Card tip ",30).$orderpaymentamt['tip_amount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Advance Amt(+) ",30).$orderpaymentamt['advance']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Grand Total",30).$orderpaymentamt['totalpaysumm']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Tax Details:",20)."".str_pad("Net Amt",10)."".str_pad("Tax Amt", 10));
			  	$printer->feed(1);
			  	foreach($ordertaxamt['liqtax'] as $value){
			  		$printer->text(str_pad("Vat :",10)."".str_pad($value['tax1']." % On",10)."".str_pad($value['amt']." is", 10)."".$value['tax1_value']);
			  		$printer->feed(1);
			  	}
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Vat :",20)."".str_pad($ordertaxamt['liqnetamt'],10)."".$ordertaxamt['taxvalliq']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach($ordertaxamt['foodtax'] as $value){
			  		$printer->text(str_pad("Gst:",10)."".str_pad($value['tax1']." % On",10)."".str_pad($value['amt']." is", 10)."".$value['tax1_value']);
			  		$printer->feed(1);
			  	}
			  	$printer->feed(1);
			  	$printer->text(str_pad("CSGST ",30).$ordertaxamt['taxvalfood']/2);
			  	$printer->feed(1);
			  	$printer->text(str_pad("SSGST ",30).$ordertaxamt['taxvalfood']/2);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Total Gst :",20)."".str_pad($ordertaxamt['foodnetamt'],10)."".$ordertaxamt['taxvalfood']);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text("Category");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Food ",30).$catsub['foodcat']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Liquor ",30).$catsub['liqcat']);
			  	$printer->feed(1);
			  	$printer->text("Sub Category");
			  	$printer->feed(1);
			  	foreach($catsub['subcat'] as $key => $value) { 
					$printer->text(str_pad($value['name'],30).$value['amount']);
					$printer->feed(1);
				}
				$printer->text(str_pad("Total ",30).$catsub['totalamt']);
			  	$printer->feed(2);
			  	$printer->text(str_pad("Last Bill No ",30).$orderdatalastarray['orderdatalast']['lastbill']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Last Kot No ",30).$orderdatalastarray['orderdatalast']['lastkot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Last Bot No ",30).$orderdatalastarray['orderdatalast']['lastbot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Unsettle Bill Amt ",20)."".str_pad($orderdatalastarray['orderdatalast']['pendingbill'],10)."".$orderdatalastarray['orderdatalast']['pendingamount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Duplicate Bill ",20)."".str_pad($orderdatalastarray['orderdatalast']['duplicatecount'],10)."".$orderdatalastarray['orderdatalast']['duplicate']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Pending Table ",20)."".str_pad($orderdatalastarray['orderdatalast']['pendingbill'],10)."".$orderdatalastarray['orderdatalast']['pendingamount']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Compliment Bill ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Bill ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelledcount'],10)."".$orderdatalastarray['orderitemlast']['cancelledbill']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Kot ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelkot'],10)."".$orderdatalastarray['orderitemlast']['cancelamountkot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Cancelled Bot ",20)."".str_pad($orderdatalastarray['orderitemlast']['cancelbot'],10)."".$orderdatalastarray['orderitemlast']['cancelamountbot']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("NC Kot ",20)."".str_pad($orderdatalastarray['orderdatalast']['totalnc'],10)."".$orderdatalastarray['orderdatalast']['totalamountnc']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("Disc Before Bill ",20)."".str_pad("0.00",10)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Disc after Bill ",20)."".str_pad($orderdatalastarray['orderdatalast']['discountbillcount'],10)."".$orderdatalastarray['orderdatalast']['discountamount']);
			  	$printer->feed(2);
			  	$printer->text("CASH DROVEREST");
			  	$printer->feed(1);
			  	$printer->text("OPENING BALANCE");
			  	$printer->feed(1);
			  	$printer->text(str_pad("CASH SALE ",20)."".str_pad("0.00",10).$orderpaymentamt['totalcash']);
			  	$printer->feed(1);
			  	$printer->text("PAID IN ");
			  	$printer->feed(1);
			  	$printer->text("PAID OUT ");
			  	$printer->feed(1);
			  	$printer->text(str_pad("TOTAL BALANCE IN DROVER ",30)."0.00");
			  	$printer->feed(1);
			  	$printer->text(str_pad("TIP ",30).$data['tip']);
			  	$printer->feed(1);
			  	$printer->text(str_pad("NO. OF PERSONS",30).$data['person']);
			  	$printer->feed(2);
			  	$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}

	}
	public function pdfprints(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/daysummaryreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/daysummaryreport');
		$html ='';
		$url = '';

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		} else {
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_category'])){
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '99';
		}

		if(isset($this->request->get['filter_hotelvalue'])){
			$data['hotelvalue'] = $this->request->get['filter_hotelvalue'];
		}
		else{
			$data['hotelvalue'] = '';
		}

		$orderdataarraysz = array();
		$orderlocdatasz = array();
		$orderlocdatasamtz = array();
		$orderpaymentamtz = array(); 
		$ordertaxamtz = array(); 
		$catsubsz = array();


		$orderdatalastarrayz = array();

		$orderdataarray = array();
		$orderlocdatas = array();
		$orderlocdatasamt = array();
		$orderpaymentamt = array(); 
		$ordertaxamt = array(); 
		$testfoods = array();
		$testliqs = array();
		$foodarray = array();
		$liqarray = array();
		$catsub = array();
		$orderdatalastarray = array();
		$orderdatalastval = array();
		$orderitemlastval = array();
		$final_data = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['filter_hotelvalue'])){
			$this->load->model('catalog/daysummaryreport');
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$hotelvalue = $this->request->get['filter_hotelvalue'];

			$hotellists = $this->db->query("SELECT * FROM oc_hotel_list WHERE name = '".$hotelvalue."'" )->rows;
			
			foreach ($hotellists as $hotellist) {
				$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $hotellist['db']);

				/************************************** User Wise Start *************************************************/
				$sql = "SELECT oi.`login_id`, oi.`login_name`, pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, bill_date, is_liq, onac FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0'";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";
				}	
				$sql .= " GROUP BY oi.`login_id`";	
				$orderdatauserwises = $this->query($sql,$conn)->rows;
				// echo'<pre>';
				// print_r($orderdatauserwises);
				// exit;
				foreach ($orderdatauserwises as $orderdata) {
					$orderdataval = array();
					$orderitemval = array();
					$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0'";
					if($filter_category != '99'){
						$sql .= " AND is_liq = '".$filter_category."'";	
					}
					$sql .= " GROUP BY oi.`order_id`";	
					$orderdatatest = $this->query($sql,$conn)->rows;
					// echo"<pre>";
					// print_r($orderdatatest);
					// exit;
					$netsale = 0;
					$pay_cash = 0;
					$pay_card = 0;
					$pay_online = 0;
					$onac = 0;
					$mealpass = 0;
					$pass = 0;
					$totalpendingamount = 0;
					$totalpendingbill = 0;
					$discountamount = 0;
					$discountbillcount = 0;
					$duplicatebill = 0;
					$duplicatecount = 0;
					$advanceamount = 0;
					$cancelkot = 0;
					$cancelamountkot = 0;
					$cancelbot = 0;
					$cancelamountbot = 0;
					$totalamountnc = 0;
					$totalnc = 0;
					$cancelledbill = 0;
					$cancelledbillcount = 0;
					$complimentarybill = 0;
					$complimentarycount = 0;
					foreach ($orderdatatest as $key) {
						$netsale =  $netsale  + $key['ftotal'] + $key['ltotal'];
						$pay_cash = $pay_cash + $key['pay_cash'];
						$pay_card = $pay_card + $key['pay_card'];
						$pay_online = $pay_online + $key['pay_online'];
						$onac = $onac + $key['onac'];
						$mealpass = $mealpass + $key['mealpass'];
						$pass = $pass + $key['pass'];

						if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
							$totalpendingbill ++;
							$totalpendingamount = $totalpendingamount + $key['grand_total'];
						}

						if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
							$discountbillcount ++;
							$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
						}

						if($key['duplicate'] == '1'){
							$duplicatecount ++;
							$duplicatebill = $duplicatebill + $key['grand_total'];
						}

						$advanceamount = $advanceamount + $key['advance_amount'];
					}

					$sql = "SELECT cancel_status,`complimentary_status`,`grand_total` FROM `oc_order_info`  WHERE `login_id` = '".$orderdata['login_id']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' ";
					if($filter_category != '99'){
						$sql .= " AND is_liq = '".$filter_category."'";	
					}
					$sql .= " ORDER BY `order_id`";
					$orderdatabill = $this->query($sql,$conn)->rows;
					// echo "<pre>";
					// print_r($orderdatabill);
					// exit;
					foreach ($orderdatabill as $key => $value) {
						if($value['cancel_status'] == '1'){
							$cancelledbillcount ++;
							$cancelledbill = $cancelledbill + $value['grand_total'];
						}

						if($value['complimentary_status'] == '1'){
							$complimentarycount ++;
							$complimentarybill = $complimentarybill + $value['grand_total'];
						}
					}

					$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status`, `grand_total` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`login_id` = oi.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE oi.`login_id` = '".$orderdata['login_id']."' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND ismodifier = '1' ";
					if($filter_category != '99'){
						$sql .= " AND is_liq = '".$filter_category."'";	
					}
					$sql .= " ORDER BY oi.`order_id`";
					$orderdatatest = $this->query($sql,$conn)->rows;
					// echo"<pre>";
					// print_r($orderdatatest);
					// exit; 
					foreach ($orderdatatest as $key) {
						if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
							$cancelamountkot = $cancelamountkot + $key['amt'];
							$cancelkot ++;
						}

						if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
							$cancelamountbot = $cancelamountbot + $key['amt'];
							$cancelbot ++;
						}

						if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
							$totalnc ++;
							$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
						}
					}
					$orderdataval = array(
						'netsale' => $netsale,
						'total_cash' => $pay_cash,
						'total_card' => $pay_card,
						'total_pay_online' => $pay_online,
						'onac' => $onac,
						'total_mealpass' => $mealpass,
						'total_pass' => $pass,
						'pendingbill' => $totalpendingbill,
						'pendingamount' => $totalpendingamount,
						'discountbillcount' => $discountbillcount,
						'discountamount' => $discountamount,
						'duplicate' => $duplicatebill,
						'duplicatecount' => $duplicatecount,
						'advanceamount' => $advanceamount,
						'cancelkot' => $cancelkot,
						'cancelamountkot' => $cancelamountkot,
						'cancelbot' => $cancelbot,
						'cancelamountbot' => $cancelamountbot,
						'totalnc' => $totalnc,
						'totalamountnc' => $totalamountnc,
						'cancelledcount' => $cancelledbillcount,
						'cancelledbill' => $cancelledbill,
						'complimentarycount' => $complimentarycount,
						'complimentarybill' => $complimentarybill,
						'login_id' => $orderdata['login_id'],
						'login_name' => $orderdata['login_name'],
					);
					$orderdataarray[$orderdata['login_id']] = array(
						'orderdata' => $orderdataval,
					);
				}
				/************************************** User Wise End *************************************************/

				/************************************** Location Wise Start *************************************************/

				$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' AND `bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0'  AND oit.`complimentary_status` = '0'";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY location";
				$orderdatalocationwises = $this->query($sql,$conn)->rows; 
				// echo"<pre>";
				// print_r($orderdatalocationwises);
				// exit; 
				$totalnetsale = 0;
				$totalgst = 0;
				$totalvat = 0;
				$totalroundoff = 0;
				$totalscharge = 0;
				$totaldiscount = 0;
				$nettotalamt = 0;
				$advance = 0;
				foreach($orderdatalocationwises as $orderdata){
					$sql = "SELECT * FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE location = '".$orderdata['location']."' AND `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' AND oi.`bill_status` = '1'  AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0'";
					if($filter_category != '99'){
						$sql .= " AND is_liq = '".$filter_category."'";	
					}
					$sql .= " GROUP BY oi.`order_id`";
					//echo $sql;exit;
					$orderdatatestloc = $this->query($sql,$conn)->rows;
					// echo"<pre>";
					// print_r($orderdatatestloc);
					// exit;  
					$netsale = 0;
					$vat = 0;
					$gst = 0;
					$roundoff = 0;
					$scharge = 0;
					$discount = 0;
					$nettotal = 0;
					$advance = 0;
					foreach ($orderdatatestloc as $key) {
						if($key['is_liq'] == '1' && $filter_category == '1'){
							$netsale = $netsale + $key['amt'];
							$vat = $vat + ($key['tax1_value'] + $key['tax2_value']);
							$scharge = $scharge + $key['staxliq'];
							$discount = $discount + $key['discount_value'];
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
							} else{
								$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxliq']) - $key['discount_value'];
							}
							$advance = $advance + $key['advance_amount'];
						}elseif($key['is_liq'] == '0' && $filter_category == '0'){
							$netsale = $netsale + $key['amt'];
							$gst = $gst + ($key['tax1_value'] + $key['tax2_value']);
							$scharge = $scharge + $key['staxfood'];
							$discount = $discount + $key['discount_value'];
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$nettotal = $nettotal + $key['amt'] - $key['discount_value'];
							} else{
								$nettotal = $nettotal + ($key['amt'] + $key['tax1_value'] + $key['tax2_value'] + $key['staxfood']) - $key['discount_value'];
							}
							$advance = $advance + $key['advance_amount'];
						} else{
							$netsale = $netsale + $key['ftotal'] + $key['ltotal'];
							$vat = $vat + $key['vat'];
							$gst = $gst + $key['gst'];
							$roundoff = $roundoff + $key['roundtotal'];
							$scharge = $scharge + $key['stax'];
							$discount = $discount + $key['ftotalvalue'] + $key['ltotalvalue'];
							$nettotal = $nettotal + $key['grand_total'] + $key['roundtotal'];
							$advance = $advance + $key['advance_amount'];
						}
					}
					$orderlocdatas[] = array(
						'location' => $key['location'],
						'netsale' => $netsale,
					);
					$totalnetsale = $totalnetsale + $netsale;
					$totalgst = $totalgst + $gst;
					$totalvat = $totalvat + $vat;
					$totalroundoff = $totalroundoff + $roundoff;
					$totalscharge = $totalscharge + $scharge;
					$totaldiscount = $totaldiscount + $discount;
					$nettotalamt = $nettotalamt + $nettotal + $advance;
				}
				$orderlocdatasamt = array(
					'totalnetsale' => $totalnetsale,
					'totalgst' => $totalgst,
					'totalvat' => $totalvat,
					'totalroundoff' => $totalroundoff,
					'totalscharge' => $totalscharge,
					'totaldiscount' => $totaldiscount,
					'nettotalamt' => $nettotalamt,
					'advance' => $advance
				);
				// echo"<pre>";
				// print_r($orderlocdatasamt);
				// exit;  
				/************************************** Location Wise End *************************************************/

				/************************************** Payment Summary Start *************************************************/
				$totalcash = 0;
				$totalcard = 0;
				$totalpayonline =0;
				$onac = 0;
				$mealpass = 0;
				$pass = 0;
				$room = 0;
				$msr = 0;
				$total = 0;
				$cardtip = 0;
				$totalpaysumm = 0;
				$onac = 0;
				$advanceamt = 0;
				$tip_amount = 0;
				$persons = 0;
				$sql = "SELECT pay_cash, pay_card,pay_online, mealpass, pass, advance_amount, bill_date, is_liq, onac, tip FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`login_id` = oit.`login_id` AND oi.`order_id` = oit.`order_id`) WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0'";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";
				}	
				$sql .= " GROUP BY oit.`order_id`";	
				$orderdatauserwises = $this->query($sql,$conn)->rows;
				// echo"<pre>";
				// print_r($orderdatauserwises );
				// exit; 
				foreach ($orderdatauserwises as $orderpayment) {
					$totalcash = $totalcash + $orderpayment['pay_cash'];
					$totalcard = $totalcard + $orderpayment['pay_card'];
					$totalpayonline = $totalpayonline + $orderpayment['pay_online'];
					$onac = $onac + $orderpayment['onac'];
					$mealpass = $mealpass + $orderpayment['mealpass'];
					$pass = $pass + $orderpayment['pass'];
					$advanceamt = $advance + $orderpayment['advance_amount'];
					$tip_amount = $tip_amount + $orderpayment['tip'];
					$room = 0.00;
					$msr = 0.00;
					$total =        ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr) - $advanceamt;
					$totalpaysumm = ($totalcash + $totalcard + $totalpayonline + $onac + $mealpass + $pass + $room + $msr + $tip_amount) - $advanceamt;
				}

				$advance = $this->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'",$conn);
				if($advance->num_rows > 0){
					$advancetotal = $advance->row['advancetotal'];
				} else{
					$advancetotal = 0;
				}

				$orderpaymentamt = array(
					'totalcash' => $totalcash,
					'totalcard' => $totalcard,
					'totalpayonline' => $totalpayonline,
					'onac' => $onac,
					'mealpass' => $mealpass,
					'pass' => $pass,
					'room' => $room,
					'msr' => $msr,
					'total' => $total,
					'tip_amount' => $tip_amount,
					'advanceamt' => $advanceamt,
					'advance' => $advancetotal,
					'totalpaysumm' => $totalpaysumm
				);

				/************************************** Payment Summary End *************************************************/

				/************************************** Tax Summary End *************************************************/
				// echo "<pre>";
				// print_r($orderdatauserwises);
				// exit();
					//$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items` WHERE cancelstatus = '0' GROUP BY tax1")->rows;
				$sql = "SELECT SUM(oit.`amt`) as amt, SUM(oit.`tax1_value`) as tax1_value, oit.`is_liq`, oit.`tax1` FROM `oc_order_items` oit LEFT JOIN `oc_order_info` oi ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`complimentary_status` = '0'";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY tax1, is_liq";
				//echo $sql;exit;
				$tests = $this->query($sql,$conn)->rows; 
				// echo "<pre>";
				// print_r($tests);
				// exit();
				foreach($tests as $test){
					if($test['is_liq'] == '0'){
						$testfoods[] = array(
							'tax1' => $test['tax1'],
							'amt' => $test['amt'],
							'tax1_value' => $test['tax1_value']
						);
					} else{
						$testliqs[] = array(
							'tax1' => $test['tax1'],
							'amt' => $test['amt'],
							'tax1_value' => $test['tax1_value']
						);
					}
				}
				$foodnetamt = 0;
				$taxvalfood = 0;
				$liqnetamt = 0;
				$taxvalliq = 0;

				foreach($testfoods as $tkey => $tvalue){
					$foodarray[] = array(
						'tax1' => $tvalue['tax1'],
						'amt' => $tvalue['amt'],
						'tax1_value' => $tvalue['tax1_value'],
					);
					$foodnetamt = $foodnetamt + $tvalue['amt'];
					$taxvalfood = $taxvalfood + $tvalue['tax1_value'];
				}

				foreach($testliqs as $tkey => $tvalue){
					$liqarray[] = array(
						'tax1' => $tvalue['tax1'],
						'amt' => $tvalue['amt'],
						'tax1_value' => $tvalue['tax1_value'],
					);
					$liqnetamt = $liqnetamt + $tvalue['amt'];
					$taxvalliq = $taxvalliq + $tvalue['tax1_value'];
				}

				$ordertaxamt = array(
					'foodtax' => $foodarray,
					'liqtax' => $liqarray,
					'foodnetamt' => $foodnetamt,
					'taxvalfood' => $taxvalfood,
					'liqnetamt' => $liqnetamt,
					'taxvalliq' => $taxvalliq
				);
				
				// echo "<pre>";
				// print_r($ordertaxamt);
				// exit();
				/************************************** Tax Summary End *************************************************/

				/************************************** Category Start **************************************************/
				$foodamt = '0';
				$liqamt = '0';
				$disamt_food = 0;
				$foodtotaldis = 0;
				$liqtotaldis = 0;
				$disamt_liq = 0;

				if($filter_category == '0'){
					// $foodamt = $this->query("SELECT SUM(ftotal) as total_food FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND oi.`complimentary_status` = '0'",$conn)->row;
					// $foodamt = $foodamt['total_food'];

					$foodamt = $this->query("SELECT SUM(oit.`amt`) as total_food,SUM(oit.`discount_value`) as discount_value, FROM oc_order_info oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ",$conn)->row;
				$foodamt = $foodamt['total_food'];
				$disamt_food = $foodamt['discount_value'];
				}
				if($filter_category == '1'){
					// $liqamt = $this->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND oi.`complimentary_status` = '0'",$conn)->row;
					//

					$liqamt = $this->query("SELECT SUM(oit.`discount_value`) as discount_value,SUM(oit.`amt`) as total_liq FROM oc_order_info oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ",$conn)->row;
					 $liqamt = $liqamt['total_liq'];
					 $disamt_liq = $liqamt['discount_value'];


				}
				if($filter_category == '99'){
					// $foodamt = $this->query("SELECT SUM(ftotal) as total_food FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ",$conn)->row;

					$foodamt = $this->query("SELECT  SUM(oit.`discount_value`) as discount_value, SUM(oit.`amt`) as total_food FROM oc_order_info oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '0' ",$conn)->row;

					// $liqamt = $this->query("SELECT SUM(ltotal) as total_liq FROM oc_order_info WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ",$conn)->row;

					$liqamt = $this->query("SELECT  SUM(oit.`discount_value`) as ldiscount_value, SUM(oit.`amt`) as total_liq FROM oc_order_info oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' AND `is_liq` = '1' ",$conn)->row;
					$foodtotaldis = $foodamt['discount_value'];
					$liqtotaldis = $liqamt['ldiscount_value'];
					$foodamt = $foodamt['total_food'];
					$liqamt = $liqamt['total_liq'];
				}

				/************************************** Category End **************************************************/

				/************************************** Sub Category Start **************************************************/

				$sql = "SELECT SUM(oit.`discount_value`) as discount_value,SUM(oit.`amt`) as amt, subcategoryid FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " GROUP BY subcategoryid";
				//echo $sql;exit;
				$subcategoryamts = $this->query($sql,$conn)->rows; 
				$totalamt = 0;
				$discountamt = 0;
				$subcategory = array();
				foreach($subcategoryamts as $key){
					$subcategoryname = $this->query("SELECT name FROM oc_subcategory WHERE category_id = '".$key['subcategoryid']."'",$conn)->row;
					$subcategory[] = array(
						'name' => $subcategoryname['name'],
						'amount' =>  $key['amt']
					);
					$totalamt = $totalamt + $key['amt'];
					$discountamt = $discountamt + $key['discount_value'];
				}
				
				$catsub = array(
					'foodcat' => $foodamt,
					'liqcat' => $liqamt,
					'cat_fdis' => $foodtotaldis,
					'cat_ldis' => $liqtotaldis,
					'subcat' => $subcategory,
					'disamt_food' => $disamt_food,
					'disamt_liq' => $disamt_liq,
					'totalamt' => $totalamt,
					'discountamt' => $discountamt,
				);

				/************************************** Sub Category End **************************************************/

				/************************************** Second last part Start **************************************************/
				
				$sql = "SELECT billno FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' ";

				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY billno DESC LIMIT 1";
				$lastbillno = $this->query($sql,$conn); 

				if($lastbillno->num_rows > 0){
						$lastbillno = $lastbillno->row['billno'];			
				} else{
					$lastbillno = 0;			
				}
				// echo'<pre>';
				// print_r($lastbillno);
				// exit;

				$lastkotno = 0;
				$lastbotno = 0;

				if($filter_category == '0'){
					$lastkotno = $this->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1",$conn);
					if($lastkotno->num_rows > 0){
						$lastkotno = $lastkotno->row['kot_no'];
					} else{
						$lastkotno = 0;
					}
				}
				if($filter_category == '1'){
					$lastbotno = $this->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1",$conn);
					if($lastbotno->num_rows > 0){
						$lastbotno = $lastbotno->row['kot_no'];
					} else{
						$lastbotno = 0;				
					}
				}
				if($filter_category == '99'){
					$lastkotno = $this->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '0' ORDER BY oit.`kot_no` DESC LIMIT 1",$conn);
					$lastbotno = $this->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`)  WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND is_liq = '1' ORDER BY oit.`kot_no` DESC LIMIT 1",$conn);
					if($lastkotno->num_rows > 0){
						$lastkotno = $lastkotno->row['kot_no'];			
					} else{
						$lastkotno = 0;			
					}
					if($lastbotno->num_rows > 0){
						$lastbotno = $lastbotno->row['kot_no'];			
					} else{
						$lastbotno = 0;			
					}
				}

				$sql = "SELECT *,oi.`order_id` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`) WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_status` = '1' AND oi.`pay_method` = '1' AND oi.`cancel_status` = '0' AND oi.`complimentary_status` = '0' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= "  GROUP BY oi.`order_id` ORDER BY oi.`order_id`";
				$orderdatalast = $this->query($sql,$conn)->rows; 
				$totalpendingamount = 0;
				$totalpendingbill = 0;
				$duplicate = 0;
				$duplicatecount = 0;
				$totalamountnc = 0;
				$totalnc = 0;
				$discountamount = 0;
				$discountbillcount = 0;
				$advanceamount = 0;
				$billmodify = 0;
				$qty = 0;
				$cancelamountkot = 0;
				$cancelamountbot = 0;
				$cancelkot = 0;
				$cancelbot = 0;
				foreach ($orderdatalast as $key) {
					if($key['pay_method'] == '0' AND $key['bill_status'] == '1'){
						$totalpendingamount = $totalpendingamount + $key['grand_total'];
						$totalpendingbill ++;
					}

					if($key['duplicate'] == '1'){
						$duplicate = $duplicate + $key['duplicate'];
						$duplicatecount ++;
					}

					if(($key['ftotalvalue'] != '0.00' || $key['ltotalvalue'] != '0.00') || ($key['ftotalvalue'] != '0.00' && $key['ltotalvalue'] != '0.00')){
						$discountamount = $discountamount + $key['ftotalvalue'] + $key['ltotalvalue'];
						$discountbillcount ++;
					}

					if($key['advance_amount'] != '0'){
						$advanceamount = $advanceamount + $key['advance_amount'];
					}
				}

				$sql = "SELECT rate, qty, amt, cancelstatus, is_liq, oit.`nc_kot_status` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON (oi.`order_id` = oit.`order_id`)  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."' ";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				$sql .= " ORDER BY oi.`order_id`";	
				$orderitemlast = $this->query($sql,$conn)->rows;
				foreach ($orderitemlast as $key) {
					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '0'){
						$cancelamountkot = $cancelamountkot + $key['amt'];
						$cancelkot ++;
					}

					if($key['cancelstatus'] == '1' AND $key['qty'] > 0 AND $key['is_liq'] == '1'){
						$cancelamountbot = $cancelamountbot + $key['amt'];
						$cancelbot ++;
					}

					if($key['nc_kot_status'] == '1' AND $key['qty'] > 0){
						$totalamountnc = $totalamountnc + ($key['rate'] * $key['qty']);
						$totalnc ++;
					}
				}
				
				$cancelledbillcount = 0;
				$cancelledbill = 0;
				$sql = "SELECT SUM(oi.`grand_total`) as grand_total FROM `oc_order_info` oi  WHERE  `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = 1";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				//$sql .= " ORDER BY oit.`order_id` ";
				$orderdatabill = $this->query($sql,$conn);
				if ($orderdatabill->num_rows > 0) {
					$cancelledbillcount = $orderdatabill->num_rows;
					$cancelledbill = $orderdatabill->row['grand_total'];
				}
				

				$complimentarycount = 0;
				$complimentarybill = 0;
				$sql = "SELECT SUM(oi.`grand_total`) as grand_total FROM `oc_order_info` oi WHERE `bill_date` >= '".$start_date."' AND `bill_date`<= '".$end_date."'  AND `bill_status` = '1' AND `pay_method` = '1' AND oi.`complimentary_status` = 1";
				if($filter_category != '99'){
					$sql .= " AND is_liq = '".$filter_category."'";	
				}
				//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
				$orderdatacomp = $this->query($sql,$conn);
				if ($orderdatacomp->num_rows > 0) {
					$complimentarycount = $orderdatacomp->num_rows;
					$complimentarybill = $orderdatacomp->row['grand_total'];
				}

				$orderitemlastval = array(
					'cancelamountkot' => $cancelamountkot,
					'cancelamountbot' => $cancelamountbot,
					'cancelkot' => $cancelkot,
					'cancelbot' => $cancelbot,
					'cancelledbill' => $cancelledbill,
					'cancelledcount' => $cancelledbillcount,
					'complimentarybill' => $complimentarybill,
					'complimentarycount' => $complimentarycount,
				);
				$orderdatalastval = array(
					'lastbill' => $lastbillno,
					'lastkot' => $lastkotno,
					'lastbot' => $lastbotno,
					'pendingbill' => $totalpendingbill,
					'pendingamount' => $totalpendingamount,
					'duplicate' => $duplicate,
					'duplicatecount' => $duplicatecount,
					'totalnc' => $totalnc,
					'totalamountnc' => $totalamountnc,
					'billmodify' => $billmodify,
					'discountamount' => $discountamount,
					'discountbillcount' => $discountbillcount,
					'advanceamount' => $advanceamount
				);

				$sql1 = "SELECT SUM(oi.`grand_total`) as grand_total FROM oc_order_info oi  WHERE bill_date >= '".$start_date."' AND bill_date <= '".$end_date."' AND oi.`bill_modify` = '1'  AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ";
				if($filter_category != '99'){
					$sql1 .= " AND is_liq = '".$filter_category."'";	
				}
				//$sql .= " GROUP BY oit.`order_id` ORDER BY oit.`order_id` ";
				$bill_modifiy_data = $this->query($sql1,$conn);
				$data['NumberOfbillmodify'] = 0;
				$data['totalbillamount'] = 0;
				if($bill_modifiy_data->num_rows > 0){
					$data['NumberOfbillmodify'] = $bill_modifiy_data->num_rows;
					$data['totalbillamount'] = $bill_modifiy_data->row['grand_total'];
					
				}
				$orderdatalastarray = array(
					'orderdatalast' => $orderdatalastval,
					'orderitemlast' => $orderitemlastval
				);
				/************************************** Second last part End **************************************************/
				$tip = $this->query("SELECT SUM(tip) as tip FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' ",$conn);
				if($tip->num_rows > 0){
					$data['tip'] = $tip->row['tip'];
				} else{
					$data['tip'] = 0;
				}

				$persons = $this->query("SELECT SUM(person) as person FROM `oc_order_info` WHERE `bill_date` >= '".$start_date."' AND `bill_date` <= '".$end_date."' AND `bill_status` = '1' AND `pay_method` = '1' AND `cancel_status` = '0' AND `complimentary_status` = '0' ",$conn);
				if($persons->num_rows > 0){
					$data['person'] = $persons->row['person'];
				} else{
					$data['person'] = 0;
				}

				//$final_data = array(
					$orderdataarraysz = $orderdataarray; // user wise
					$orderlocdatasz = $orderlocdatas; // location wise
					$orderlocdatasamtz = $orderlocdatasamt; // all total amt location wise
					$orderpaymentamtz = $orderpaymentamt; // all payment details
					$ordertaxamtz = $ordertaxamt; // all tax details
					$catsubsz = $catsub; //category and subcategory
					$orderdatalastarrayz = $orderdatalastarray;
				//);
		
				$conn->close();
			}
		}
		
		
		$data['orderdataarrays'] = $orderdataarraysz; // user wise
		$data['orderlocdatas'] = $orderlocdatasz; // location wise
		$data['orderlocdatasamt'] = $orderlocdatasamtz; // all total amt location wise
		$data['orderpaymentamt'] = $orderpaymentamtz; // all payment details
		$data['ordertaxamt'] = $ordertaxamtz; // all tax details
		$data['catsubs'] = $catsub; //category and subcategory
		$data['orderdatalastarray'] = $orderdatalastarray;
		// echo "<pre>";
		// print_r($orderlocdatasz);
		// exit();

		$forprintarray = array(
			'orderdataarray' => $orderdataarray,
			'orderlocdatas' => $orderlocdatas,
			'orderlocdatasamt' => $orderlocdatasamt,
			'orderpaymentamt' => $orderpaymentamt,
			'ordertaxamt' => $ordertaxamt,
		);

		$data['forprintarray'] = json_encode($forprintarray);

		
		$hotel_datas = $this->db->query("SELECT * FROM oc_hotel_list WHERE 1=1 " )->rows;
		$data['hotel_list'] = $hotel_datas;

		$data['categorys'] = array(
								'0' => 'Food',
								'1' => 'Liquor'
							);
		$data['filter_category'] = $filter_category;
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/daysummaryreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//$this->response->setOutput($this->load->view('catalog/daysummaryreport', $data));
	
			/************************************** Second last part End **************************************************/
		// echo '<pre>';
		// print_r($data);
		// exit;
		$html = $this->load->view('catalog/stock_invoice', $data);
		echo $html;exit;
		$filename = 'stock_invoice.tpl';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}
}
	