<?php
class ControllerCatalogItembulk extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/itembulk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$this->getList();
	}
	
	public function edit() {
		$this->load->language('catalog/itembulk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$in = 0;
			foreach($this->request->post['item_datas'] as $ikey => $ivalue){
				if($ivalue['item_sub_category_id'] != ''){
					if (!isset($ivalue['stock_register'])) {
						$ivalue['stock_register'] = '';
					}
					if (!isset($ivalue['ismodifier'])) {
						$ivalue['ismodifier'] = '';
					}
					$sql = "UPDATE `oc_item` SET 
							`activate_item` = '".$this->db->escape($ivalue['activate_item'])."',
							`stock_register` = '".$this->db->escape($ivalue['stock_register'])."',
							
							`item_name` = '".$this->db->escape($ivalue['item_name'])."',
							`department` = '".$this->db->escape($ivalue['department'])."',
							`item_category` = '".$this->db->escape($ivalue['item_category'])."',
							`item_category_id` = '".$this->db->escape($ivalue['item_category_id'])."',
							`item_sub_category` = '".$this->db->escape($ivalue['item_sub_category'])."',
							`item_sub_category_id` = '".$this->db->escape($ivalue['item_sub_category_id'])."',
							`rate_1` = '".$this->db->escape($ivalue['rate_1'])."',
							`rate_2` = '".$this->db->escape($ivalue['rate_2'])."',
							`rate_3` = '".$this->db->escape($ivalue['rate_3'])."',
							`rate_4` = '".$this->db->escape($ivalue['rate_4'])."',
							`rate_5` = '".$this->db->escape($ivalue['rate_5'])."',
							`rate_6` = '".$this->db->escape($ivalue['rate_6'])."',
							`brand_id` = '".$this->db->escape($ivalue['brand_id'])."',
							`brand` = '".$this->db->escape($ivalue['brand'])."',
							`quantity` = '".$this->db->escape($ivalue['quantity'])."',
							`ismodifier` = '".$this->db->escape($ivalue['ismodifier'])."',
							`modifier_group` = '".$this->db->escape($ivalue['modifier_group'])."',
							`vat` = '".$this->db->escape($ivalue['vat'])."',
							`s_tax` = '".$this->db->escape($ivalue['s_tax'])."'
							WHERE `item_id` = '".$ivalue['item_id']."'";
					//echo $sql;exit;
					$this->db->query($sql);
				} else{
					$in = 1;
				}
			}

			if($in == 0){
				$this->session->data['success'] = $this->language->get('text_success');
			} else{
				$this->session->data['warning'] = 'Some of data not have subcategory data will not be inserted';
			}

			$url = '';

			if (isset($this->request->get['filter_item_name'])) {
				$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
			}

			if (isset($this->request->get['filter_item_id'])) {
				$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
			}
			
			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_item_code'])) {
				$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
			}

			if (isset($this->request->get['filter_barcode'])) {
				$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
			}

			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . $this->request->get['filter_category'];
			}

			if (isset($this->request->get['filter_sub_category'])) {
				$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	
	protected function getList() {
		if (isset($this->request->get['filter_item_name'])) {
			$filter_item_name = $this->request->get['filter_item_name'];
		} else {
			$filter_item_name = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}

		if (isset($this->request->get['filter_item_code'])) {
			$filter_item_code = $this->request->get['filter_item_code'];
		} else {
			$filter_item_code = null;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}

		if (isset($this->request->get['filter_barcode'])) {
			$filter_barcode = $this->request->get['filter_barcode'];
		} else {
			$filter_barcode = null;
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}

		if (isset($this->request->get['filter_sub_category'])) {
			$filter_sub_category = $this->request->get['filter_sub_category'];
		} else {
			$filter_sub_category = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->error['item_sub_category_id'])) {
			$data['error_item_sub_category_id'] = $this->error['item_sub_category_id'];
		} else {
			$data['error_item_sub_category_id'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if (isset($this->request->get['filter_sub_category'])) {
			$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('catalog/itembulk/edit', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		$filter_data = array(
			'filter_item_name' => $filter_item_name,
			'filter_item_id' => $filter_item_id,
			'filter_item_code' => $filter_item_code,
			'filter_department' => $filter_department,
			'filter_barcode' => $filter_barcode,
			'filter_category' => $filter_category,
			'filter_sub_category' => $filter_sub_category,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		// $data['vats'] = array(
		// 	'' => 'None',
		// 	'5' => 'GST 5%',
		// 	'18' => 'GST 18%',
		// );
		$data['vats'] = $this->db->query( "SELECT * FROM oc_tax")->rows;

		$data['s_taxs'] = array(
			'' => 'None',
		);

		$brands = $this->db->query("SELECT * FROM oc_brand WHERE 1=1 ")->rows;
		$data['brands'] = $brands;

		$modifier_groups = $this->db->query("SELECT * FROM oc_modifier")->rows;
		$data['modifier_groups'] = $modifier_groups;

		$results = $this->model_catalog_item->getDepartment();
		$data['departments'] = array();
		$data['departments'][''] = 'None';
		foreach ($results as $dvalue) {
			$data['departments'][$dvalue['name']]= $dvalue['name'];
		}

		$results = $this->model_catalog_item->getCategory();
		$data['item_categorys'] = array();
		$data['item_categorys'][''] = 'None';
		foreach ($results as $dvalue) {
			$data['item_categorys'][$dvalue['category_id']]= $dvalue['category'];
		}

		$data['item_categorys1'] = array();
		$data['item_categorys1'][''] = 'All';
		foreach ($results as $dvalue) {
			$data['item_categorys1'][$dvalue['category_id']]= $dvalue['category'];
		}

		$results = $this->model_catalog_item->getSubCategory();
		$data['item_sub_categorys'] = array();
		$data['item_sub_categorys'][''] = 'None';
		foreach ($results as $dvalue) {
			$data['item_sub_categorys'][$dvalue['category_id']]= $dvalue['name'];
		}

		$data['item_sub_categorys1'] = array();
		$data['item_sub_categorys1'][''] = 'All';
		foreach ($results as $dvalue) {
			$data['item_sub_categorys1'][$dvalue['category_id']]= $dvalue['name'];
		}

		$item_total = $this->model_catalog_item->getTotalItems();
		$results = $this->model_catalog_item->getItems($filter_data);

		foreach ($results as $result) {
			$data['items'][] = array(
				'item_id' 				=> $result['item_id'],
				'item_code'        		=> $result['item_code'],
				'item_name'       	 	=> $result['item_name'],
				'department'        	=> $result['department'],
				'item_category'        	=> $result['item_category'],
				'item_category_id'      => $result['item_category_id'],
				'item_sub_category'     => $result['item_sub_category'],
				'item_sub_category_id'  => $result['item_sub_category_id'],
				'rate_1'        		=> $result['rate_1'],
				'rate_2'        		=> $result['rate_2'],
				'rate_3'       			=> $result['rate_3'],
				'rate_4'        		=> $result['rate_4'],
				'rate_5'        		=> $result['rate_5'],
				'rate_6'        		=> $result['rate_6'],
				'brand'        			=> $result['brand'],
				'brand_id'        		=> $result['brand_id'],
				'quantity'        		=> $result['quantity'],
				'ismodifier'        	=> $result['ismodifier'],
				'modifier_group'        => $result['modifier_group'],
				'vat'        			=> $result['vat'],
				's_tax'        			=> $result['s_tax'],
				'activate_item'			=> $result['activate_item'],
				'stock_register'		=> $result['stock_register'],
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if (isset($this->request->get['filter_sub_category'])) {
			$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_item_name'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=item_name' . $url, true);
		$data['sort_item_code'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=item_code' . $url, true);
		$data['sort_department'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=department' . $url, true);
		$data['sort_item_category'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=item_category' . $url, true);
		$data['sort_item_sub_category'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=item_sub_category' . $url, true);
		$data['sort_rate_1'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_1' . $url, true);
		$data['sort_rate_2'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_2' . $url, true);
		$data['sort_rate_3'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_3' . $url, true);
		$data['sort_rate_4'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_4' . $url, true);
		$data['sort_rate_5'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_5' . $url, true);
		$data['sort_rate_6'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=rate_6' . $url, true);
		$data['sort_brand'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=brand' . $url, true);
		$data['sort_ismodifier'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=ismodifier' . $url, true);
		$data['sort_activate_item'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=activate_item' . $url, true);
		$data['sort_stock_register'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=stock_register' . $url, true);
		
		$data['sort_modifier_group'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=modifier_group' . $url, true);
		$data['sort_quantity'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=quantity' . $url, true);
		$data['sort_vat'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=vat' . $url, true);
		$data['sort_s_tax'] = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . '&sort=s_tax' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_item_name'])) {
			$url .= '&filter_item_name=' . $this->request->get['filter_item_name'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_item_code'])) {
			$url .= '&filter_item_code=' . $this->request->get['filter_item_code'];
		}

		if (isset($this->request->get['filter_barcode'])) {
			$url .= '&filter_barcode=' . $this->request->get['filter_barcode'];
		}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if (isset($this->request->get['filter_sub_category'])) {
			$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/itembulk', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_item_name'] = $filter_item_name;
		$data['filter_item_id'] = $filter_item_id;
		$data['filter_item_code'] = $filter_item_code;
		$data['filter_barcode'] = $filter_barcode;
		$data['filter_category'] = $filter_category;
		$data['filter_sub_category'] = $filter_sub_category;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/itembulk_list', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/item')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$datas = $this->request->post;
		// if ((utf8_strlen($datas['item_name']) < 2) || (utf8_strlen($datas['item_name']) > 255)) {
		// 	$this->error['item_name'] = 'Please Enter Itembulk Name';
		// } else {
		// 	if(isset($this->request->get['item_id'])){
		// 		$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_name` = '".$datas['item_name']."' AND `item_id` <> '".$this->request->get['item_id']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['item_name'] = 'Itembulk Name Already Exists';
		// 		}
		// 	} else {
		// 		$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_name` = '".$datas['item_name']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['item_name'] = 'Itembulk Name Already Exists';
		// 		}
		// 	}
		// }

		// if ($this->request->post['item_code'] == '') {
		// 	$this->error['item_code'] = 'Please Select Itembulk code';
		// } else {
		// 	if(isset($this->request->get['item_id'])){
		// 		$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_code` = '".$datas['item_code']."' AND `item_id` <> '".$this->request->get['item_id']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['item_code'] = 'Itembulk Code Already Exists';
		// 		}
		// 	} else {
		// 		$is_exist = $this->db->query("SELECT `item_id` FROM `oc_item` WHERE `item_code` = '".$datas['item_code']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['item_code'] = 'Itembulk Code Already Exists';
		// 		}
		// 	}
		// }
		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItembulks($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecode() {
		$json = array();

		if (isset($this->request->get['filter_item_code'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_item_code' => $this->request->get['filter_item_code'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItembulks($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_code'        => strip_tags(html_entity_decode($result['item_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_code'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletebarcode() {
		$json = array();

		if (isset($this->request->get['filter_barcode'])) {
			$this->load->model('catalog/item');

			$data = array(
				'filter_barcode' => $this->request->get['filter_barcode'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_item->getItembulks($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_barcode'        => strip_tags(html_entity_decode($result['item_code'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_barcode'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	// public function autocomplete() {
	// 	$json = array();
	// 	if (isset($this->request->get['filter_name'])) {
	// 		$this->load->model('catalog/customer');
	// 		$data = array(
	// 			'filter_name' => $this->request->get['filter_name'],
	// 			'start'       => 0,
	// 			'limit'       => 20
	// 		);
	// 		$results = $this->model_catalog_customer->getCustomers($data);
	// 		// echo "<pre>";
	// 		// print_r($results);
	// 		// exit();
	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'c_id' => $result['c_id'],
	// 				'name'    => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}		
	// 	}
	// 	$sort_order = array();
	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['name'];
	// 	}
	// 	array_multisort($sort_order, SORT_ASC, $json);
	// 	$this->response->setOutput(json_encode($json));
	// }

	


	public function getsubcategory() {
		$json = array();
		$item_categorys = array();
		if (isset($this->request->get['filter_category_id'])) {
			$this->load->model('catalog/subcategory');
			$data = array(
				'filter_category_idf' => $this->request->get['filter_category_id'],
			);
			$results = $this->model_catalog_subcategory->getSubCategorys($data);

			//echo '<pre>';
			//print_r($results);
			//exit();

			if(isset($this->request->get['list'])){
				$item_categorys[] = array(
					'subcategory_id' => '',
					'subcategory' => 'All'
				);
			} else {
				$item_categorys[] = array(
					'subcategory_id' => '',
					'subcategory' => 'Please Select'
				);
			} 
			foreach ($results as $result) {
				$item_categorys[] = array(
					'subcategory_id' => $result['category_id'],
					'subcategory' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$json['item_categorys'] = $item_categorys;
		$this->response->setOutput(json_encode($json));
	}

	public function getbrands() {
		$json = array();
		$brands = array();
		if (isset($this->request->get['filter_subcategory_id'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_subcategory_id' => $this->request->get['filter_subcategory_id'],
			);
			$results = $this->model_catalog_brand->getBrands($data);

			// echo '<pre>';
			// print_r($results);
			// exit();

			if(isset($this->request->get['list'])){
				$brands[] = array(
					'brand_id' => '',
					'brand' => 'All'
				);
			} else {
				$brands[] = array(
					'brand_id' => '',
					'brand' => 'Please Select'
				);
			} 
			foreach ($results as $result) {
				$brands[] = array(
					'brand_id' => $result['brand_id'],
					'brand' => strip_tags(html_entity_decode($result['brand'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$json['brands'] = $brands;
		$this->response->setOutput(json_encode($json));
	}
}