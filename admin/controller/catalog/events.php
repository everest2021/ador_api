<?php
class ControllerCatalogEvents extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/events');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/events');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/events');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/events');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
			$this->model_catalog_events->addvenue($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/events');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/events');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_events->editvenue($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }
			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/events');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/events');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_events->deletevenue($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();

	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		// if (isset($this->request->get['filter_department_id'])) {

		// 	$filter_department_id = $this->request->get['filter_department_id'];

		// } else {

		// 	$filter_department_id = null;

		// }

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/events/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/events/delete', 'token=' . $this->session->data['token'] . $url, true);

		$filter_data = array(
			'filter_name' => $filter_name,
			//'filter_department_id' => $filter_department_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$venue_total = $this->model_catalog_events->getTotalvenue();

		$results = $this->model_catalog_events->getvenues($filter_data);
		foreach ($results as $result) {
			$data['venues'][] = array(
				'id'		=>$result['id'],
				'event_name' => $result['event_name'],
				'event_type'        => $result['event_type'],
				'edit'        => $this->url->link('catalog/events/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_capacity'] = $this->language->get('column_capacity');
		$data['column_rate'] = $this->language->get('column_rate');
		$data['column_action'] = $this->language->get('column_action');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {

			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];

		}

		$pagination = new Pagination();
		$pagination->total = $venue_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/venue', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($venue_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($venue_total - $this->config->get('config_limit_admin'))) ? $venue_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $venue_total, ceil($venue_total / $this->config->get('config_limit_admin')));
		$data['filter_name'] = $filter_name;

		// $data['filter_department_id'] = $filter_department_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/events_list', $data));
	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_rate'] = $this->language->get('entry_rate');
		$data['entry_capacity'] = $this->language->get('entry_capacity');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');
		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['rate'])) {
			$data['error_rate'] = $this->error['rate'];
		} else {
			$data['error_rate'] = array();
		}

		if (isset($this->error['capacity'])) {
			$data['error_capacity'] = $this->error['capacity'];
		} else {
			$data['error_capacity'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];

		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/events', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/events/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/events/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/events', 'token=' . $this->session->data['token'] . $url, true);
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_events->getvenue($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		
		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
			$data['photo_source'] = $this->request->post['photo_source'];
		} elseif (!empty($item_info)) {
			$data['photo'] = $item_info['photo'];
			$data['photo_source'] = $item_info['photo_source'];
		} else {	
			$data['photo'] = '';
			$data['photo_source'] = '';
		}
		if (isset($this->request->post['photo1'])) {
			$data['photo1'] = $this->request->post['photo1'];
			$data['photo_source1'] = $this->request->post['photo_source1'];
		} elseif (!empty($item_info)) {
			$data['photo1'] = $item_info['photo1'];
			$data['photo_source1'] = $item_info['photo_source1'];
		} else {	
			$data['photo1'] = '';
			$data['photo_source1'] = '';
		}
		if (isset($this->request->post['photo2'])) {
			$data['photo2'] = $this->request->post['photo2'];
			$data['photo_source2'] = $this->request->post['photo_source2'];
		} elseif (!empty($item_info)) {
			$data['photo2'] = $item_info['photo2'];
			$data['photo_source2'] = $item_info['photo_source2'];
		} else {	
			$data['photo2'] = '';
			$data['photo_source2'] = '';
		}
		if (isset($this->request->post['photo3'])) {
			$data['photo3'] = $this->request->post['photo3'];
			$data['photo_source3'] = $this->request->post['photo_source3'];
		} elseif (!empty($item_info)) {
			$data['photo3'] = $item_info['photo3'];
			$data['photo_source3'] = $item_info['photo_source3'];
		} else {	
			$data['photo3'] = '';
			$data['photo_source3'] = '';
		}
		if (isset($this->request->post['photo4'])) {
			$data['photo4'] = $this->request->post['photo4'];
			$data['photo_source4'] = $this->request->post['photo_source4'];
		} elseif (!empty($item_info)) {
			$data['photo4'] = $item_info['photo4'];
			$data['photo_source4'] = $item_info['photo_source4'];
		} else {	
			$data['photo4'] = '';
			$data['photo_source4'] = '';
		}
		if (isset($this->request->post['event_name'])) {
			$data['event_name'] = $this->request->post['event_name'];
		} elseif (!empty($venue_info)) {
			$data['event_name'] = $venue_info['event_name'];
		} else {
			$data['event_name'] = '';
		}

		if (isset($this->request->post['event_type'])) {
			$data['event_type'] = $this->request->post['event_type'];
		} elseif (!empty($venue_info)) {
			$data['event_type'] = $venue_info['event_type'];
		} else {
			$data['event_type'] = '';
		}
		$data['event_types'] = array('1' => 'Brithday','2' => 'other' );
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/events_form', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/venue')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
		// 	$this->error['name'] = 'Please Enter  Name';
		// }
		// if ((utf8_strlen($this->request->post['rate']) < 2) || (utf8_strlen($this->request->post['rate']) > 255)) {
		// 	$this->error['rate'] = 'Please Enter  rate';
		// }
		// if ((utf8_strlen($this->request->post['capacity']) < 2) || (utf8_strlen($this->request->post['capacity']) > 255)) {
		// 	$this->error['capacity'] = 'Please Enter  capacity';
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		// if (!$this->user->hasPermission('modify', 'catalog/events')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');

		// }
		// $this->load->model('catalog/events');
		// return !$this->error;
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/events');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20

			);
			$results = $this->model_catalog_events->getvenues($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'event_name'        => strip_tags(html_entity_decode($result['event_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['event_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

}