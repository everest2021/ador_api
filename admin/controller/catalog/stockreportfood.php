<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStockReportFood extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockreportfood');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/stockreportfood');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['item_name'])){
			$data['item_name'] = $this->request->post['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->post['store'])){
			$data['store_id'] = $this->request->post['store'];
			$store_id = $this->request->post['store'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		if(isset($this->request->post['item_id'])){
			$data['item_id'] = $this->request->post['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = array();
		$final_data = array();
		if(!empty($this->request->post['store'])){
			$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1";

			if(!empty($this->request->post['item_id'])){
				$sql .= " AND id = '".$this->request->post['item_id']."'";
			}

			if(!empty($this->request->post['store'])){
				$sql .= " AND store_id = '".$this->request->post['store']."'";
			}

			$stock_data = $this->db->query($sql)->rows;
			foreach ($stock_data as $skey => $svalue) {
				$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($svalue['uom'], $svalue['id'],  0, $store_id, $svalue['item_type'], $svalue['item_code']);
				$final_data[] = array(
					'item_name'	=> $svalue['item_name'],
					'avail_quantity' => $avail_quantity,
				);
			}
		}
		$data['final_data'] = $final_data;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
			// echo'<pre>';
			// print_r($avail_quantity);
			// exit;
		$data['action'] = $this->url->link('catalog/stockreportfood', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockreportfood', $data));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	 public function autocomplete() {
	 	$json = array();
	 	if(isset($this->request->get['filter_name'])){
	 		$results = $this->db->query("SELECT id, item_name FROM oc_stock_item WHERE item_name LIKE '%".$this->request->get['filter_name']."%' LIMIT 0,10")->rows;
	 		foreach($results as $key){
	 			$json[] = array(
	 						'id'	=> $key['id'],
	 						'name'  => $key['item_name']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	}
	public function export() {
		$this->load->language('catalog/stockreportfood');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['store'])){
			$data['store_id'] = $this->request->get['store'];
			$store_id = $this->request->get['store'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		if(isset($this->request->get['item_id'])){
			$data['item_id'] = $this->request->get['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		$this->load->model('catalog/purchaseentry');
		$data['final_data'] = array();
		$final_data = array();
		/*echo '<pre>';
		print_r($this->request->get);
		exit;*/
		//if(!empty($this->request->get['store'])){
			$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1";

			if(!empty($this->request->get['item_id'])){
				$sql .= " AND id = '".$this->request->get['item_id']."'";
			}
			if(!empty($this->request->get['store'])){
				$sql .= " AND store_id = '".$this->request->get['store']."'";
			}
			$stock_data = $this->db->query($sql)->rows;
			foreach ($stock_data as $skey => $svalue) {
				$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($svalue['uom'], $svalue['id'],  0, $store_id, $svalue['item_type'], $svalue['item_code']);
				$final_data[] = array(
					'item_name'	=> $svalue['item_name'],
					'avail_quantity' => $avail_quantity,
				);
			}
		//}
		$data['final_data'] = $final_data;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
		$data['action'] = $this->url->link('catalog/stockreportfood', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['base'] = HTTPS_SERVER;
		$html = $this->load->view('catalog/stockreportfood_html', $data);
		$filename = 'stockreportfood.xls';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;
	}
}