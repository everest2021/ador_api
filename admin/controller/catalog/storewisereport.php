<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class ControllerCatalogStorewisereport extends Controller {
	private $error = array();
	// echo'<pre>';
	// print_r($controller);exit;

	public function index() {
		$this->load->language('catalog/storewisereport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/storewisereport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/storewisereport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		} else {
			$data['startdate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		} else {
			if($this->user->getuserstore() != 0){
				$data['storename'] = $this->user->getuserstore();
			} else {
				$data['storename'] = '';
			}
		}	
		
		$data['final_data'] =array();
		$final_data = array();
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'])){
			//echo'aaa';exit;
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->post['filter_storename'];
				// echo'<pre>';
				// print_r($this->request->post);
				// exit;

			$sql = "SELECT * FROM `oc_purchase_order` WHERE `po_date` >= '".$start_date."' AND `po_date` <= '".$end_date."' ";	
			if($storecode != ''){
				$sql .= " AND outlet_id = '".$storecode."'";
			}
			$sql .=" GROUP BY `outlet_id`";
			//echo $sql;exit;
			$store_data = $this->db->query($sql)->rows;
			foreach ($store_data as $skey => $svalue) {
				$item_data = array();
				$i =1;
				$out_name =$this->db->query("SELECT `outlet_name` FROM `oc_outlet` WHERE `outlet_id` = '".$svalue['outlet_id']."' ")->row;
				$all_data =$this->db->query("SELECT * FROM `oc_purchase_order` po LEFT JOIN `oc_purchase_order_items` poi ON(po.`id` = poi.`po_id`) WHERE po.`outlet_id` = '".$svalue['outlet_id']."' ")->rows;			
				foreach ($all_data as $akey => $avalue) {
					$itemdata = $this->db->query("SELECT rate_1, item_code FROM oc_item WHERE item_id = '".$avalue['item_id']."'")->row;
					$item_data[] =array(
						'i' =>$i,
						'po_number' => $avalue['po_prefix'].'-'.$avalue['po_number'],
						'date'		=> $avalue['po_date'],
						'item_code'	=> $itemdata['item_code'],
						'item_name' => $avalue['item_name'],
						'quantity' 	=> $avalue['quantity'],
						'rate' 		=> $itemdata['rate_1'],
						'notes' 	=> $avalue['notes'],
					);
					$i ++;
				}

				$final_data[] = array(
					'outlet_name' =>$out_name['outlet_name'],
					'item_data' => $item_data,
				);
			}

		}
		$data['final_data'] = $final_data;

		$sql = "SELECT * FROM `oc_outlet` WHERE 1=1 ";
		if($this->user->getuserstore() != 0){
			$sql .="AND outlet_id = '".$this->user->getuserstore()."'";
		}
		$outlet_datas = $this->db->query($sql)->rows;
		if($this->user->getuserstore() == 0){
		$outlet_data[''] = 'All';
		}
		foreach ($outlet_datas as $dkey => $dvalue) {

			$outlet_data[$dvalue['outlet_id']] = $dvalue['outlet_name'];
		}
		$data['outlet_data'] = $outlet_data;
		
				// echo'<pre>';
				// print_r($data['final_data']);
				// exit;

		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/storewisereport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/storewisereport', $data));
	}
}