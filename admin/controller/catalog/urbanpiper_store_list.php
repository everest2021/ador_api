<?php
class Controllercatalogurbanpiperstorelist extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/urbanpier_store');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->getList();
		$this->getForm();
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_store_nme_lbl'] = $this->language->get('entry_store_nme');
		$data['entry_store_add_lbl'] = $this->language->get('entry_store_add');
		$data['entry_store_zipcode_lbl'] = $this->language->get('entry_store_zipcode');
		$data['entry_store_city_lbl'] = $this->language->get('entry_store_city');
		
		$data['entry_store_contact_phone_lbl'] = $this->language->get('entry_store_contact_phone');
		$data['entry_store_notification_no_lbl'] = $this->language->get('entry_store_notification_no');

		$data['entry_store_notification_email_lbl'] = $this->language->get('entry_store_notification_email_no');
		$data['entry_store_ref_id_lbl'] = $this->language->get('entry_store_ref_id');
		$data['entry_minimum_pick_time_lbl'] = $this->language->get('entry_minimum_pick_time');
		$data['entry_minimum_delivery_time_lbl'] = $this->language->get('entry_minimum_delivery_time');
		$data['entry_minimum_order_value_lbl'] = $this->language->get('entry_minimum_order_value');
		$data['entry_geo_longitude_lbl'] = $this->language->get('entry_geo_longitude');
		$data['entry_geo_latitude_lbl'] = $this->language->get('entry_geo_latitude');
		$data['entry_ordering_enabled_lbl'] = $this->language->get('entry_ordering_enabled');
		$data['entry_ordering_timing_lbl'] = $this->language->get('entry_ordering_timing');

		$data['entry_ordering_start_timing_lbl'] = $this->language->get('entry_ordering_start_timing');
		$data['entry_ordering_end_timing_lbl'] = $this->language->get('entry_ordering_end_timing');

		$data['entry_packaging_charge_lbl'] = $this->language->get('entry_packaging_charge');

		$data['days_name'] = array(
			'sunday' => 'sunday',
		    'monday' => 'monday',
		    'tuesday' => 'tuesday',
		    'wednesday' => 'wednesday',
		    'thursday' => 'thursday',
		    'friday' => 'friday',
		    'saturday' => 'saturday'
		);



		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_ean'] = $this->language->get('entry_ean');
		$data['entry_jan'] = $this->language->get('entry_jan');
		$data['entry_isbn'] = $this->language->get('entry_isbn');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_shipping'] = $this->language->get('entry_shipping');
		$data['entry_date_available'] = $this->language->get('entry_date_available');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length_class'] = $this->language->get('entry_length_class');
		$data['entry_length'] = $this->language->get('entry_length');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_additional_image'] = $this->language->get('entry_additional_image');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_recurring'] = $this->language->get('entry_recurring');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_sku'] = $this->language->get('help_sku');
		$data['help_upc'] = $this->language->get('help_upc');
		$data['help_ean'] = $this->language->get('help_ean');
		$data['help_jan'] = $this->language->get('help_jan');
		$data['help_isbn'] = $this->language->get('help_isbn');
		$data['help_mpn'] = $this->language->get('help_mpn');
		$data['help_minimum'] = $this->language->get('help_minimum');
		$data['help_manufacturer'] = $this->language->get('help_manufacturer');
		$data['help_stock_status'] = $this->language->get('help_stock_status');
		$data['help_points'] = $this->language->get('help_points');
		$data['help_category'] = $this->language->get('help_category');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_download'] = $this->language->get('help_download');
		$data['help_related'] = $this->language->get('help_related');
		$data['help_tag'] = $this->language->get('help_tag');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
		$data['button_option_add'] = $this->language->get('button_option_add');
		$data['button_option_value_add'] = $this->language->get('button_option_value_add');
		$data['button_discount_add'] = $this->language->get('button_discount_add');
		$data['button_special_add'] = $this->language->get('button_special_add');
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_recurring_add'] = $this->language->get('button_recurring_add');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_recurring'] = $this->language->get('tab_recurring');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['tab_openbay'] = $this->language->get('tab_openbay');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['error_entry_store_ref_id'])) {
			$data['error_entry_store_ref_id'] = $this->error['error_entry_store_ref_id'];
		} else {
			$data['error_entry_store_ref_id'] = array();
		}

		if (isset($this->error['error_entry_store_nme'])) {
			$data['error_entry_store_nme'] = $this->error['error_entry_store_nme'];
		} else {
			$data['error_entry_store_nme'] = array();
		}

		if (isset($this->error['error_entry_store_add'])) {
			$data['error_entry_store_add'] = $this->error['error_entry_store_add'];
		} else {
			$data['error_entry_store_add'] = array();
		}


		if (isset($this->error['error_entry_store_city'])) {
			$data['error_entry_store_city'] = $this->error['error_entry_store_city'];
		} else {
			$data['error_entry_store_city'] = array();
		}


		if (isset($this->error['error_entry_store_zipcode'])) {
			$data['error_entry_store_zipcode'] = $this->error['error_entry_store_zipcode'];
		} else {
			$data['error_entry_store_zipcode'] = array();
		}

		if (isset($this->error['error_entry_store_contact_phone_lbl'])) {
			$data['error_entry_store_contact_phone_lbl'] = $this->error['error_entry_store_contact_phone_lbl'];
		} else {
			$data['error_entry_store_contact_phone_lbl'] = array();
		}

		if (isset($this->error['error_entry_store_nme'])) {
			$data['error_entry_store_nme'] = $this->error['error_entry_store_nme'];
		} else {
			$data['error_entry_store_nme'] = array();
		}
		if (isset($this->error['error_entry_store_notification_no'])) {
			$data['error_entry_store_notification_no'] = $this->error['error_entry_store_notification_no'];
		} else {
			$data['error_entry_store_notification_no'] = array();
		}

		if (isset($this->error['error_notification_emails'])) {
			$data['error_notification_emails'] = $this->error['error_notification_emails'];
		} else {
			$data['error_notification_emails'] = array();
		}
		
		if (isset($this->error['error_entry_minimum_pick_time'])) {
			$data['error_entry_minimum_pick_time'] = $this->error['error_entry_minimum_pick_time'];
		} else {
			$data['error_entry_minimum_pick_time'] = array();
		}
		if (isset($this->error['error_entry_minimum_delivery_time'])) {
			$data['error_entry_minimum_delivery_time'] = $this->error['error_entry_minimum_delivery_time'];
		} else {
			$data['error_entry_minimum_delivery_time'] = array();
		}
		if (isset($this->error['error_entry_minimum_order_value'])) {
			$data['error_entry_minimum_order_value'] = $this->error['error_entry_minimum_order_value'];
		} else {
			$data['error_entry_minimum_order_value'] = array();
		}

		if (isset($this->error['error_entry_packaging_charge'])) {
			$data['error_entry_packaging_charge'] = $this->error['error_entry_packaging_charge'];
		} else {
			$data['error_entry_packaging_charge'] = array();
		}


		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Live Store',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Live Store',
			'href' => $this->url->link('catalog/urbanpier_store', 'token=' . $this->session->data['token'] . $url, true)
		);

		
		$data['action'] = $this->url->link('catalog/urbanpier_store/add', 'token=' . $this->session->data['token'], true);
		

		$data['cancel'] = $this->url->link('catalog/webhook', 'token=' . $this->session->data['token'] . $url, true);
		$data['platform_datas_array'] = array(
			'zomato' => 'zomato',
		    'swiggy' => 'swiggy',
		    'ubereats' => 'ubereats',
		    'scootsy' => 'scootsy',
		    'dunzo' => 'dunzo',
		    'dotpe' => 'dotpe',
		    'foodpanda' => 'foodpanda',
		    'amazon' => 'amazon',
		    'swiggystore' => 'swiggystore',
		    'zomatomarket' => 'zomatomarket',
				    
		);
			sleep(5);
		//if (isset($this->session->data['store_api_status']) && ($this->session->data['store_api_status'] != '')) {
			$url = Callback_Store_LIVE;
			$final_data = array('db_name' => API_DATABASE);
			$data_json = json_encode($final_data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
			$response  = curl_exec($ch);
			if (curl_errno($ch)) {
		    	$error_msg = curl_error($ch);
			}
			$item_results = json_decode($response,true);
			$product_info =  array();
			if (!empty($item_results['array'])) {
				$pproduct_info = unserialize($item_results['array']);
				if(isset($pproduct_info[0] )){
					$product_info = $pproduct_info[0];
				}
			} 
				// $this->db->query("DELETE FROM `oc_allwebhook_response` WHERE `api_for` = 'Store_Api'");
				// 	$this->db->query("INSERT INTO `oc_allwebhook_response` SET 
				// 		`api_for` = 'Store_Api',  
				// 		`array` = '".$this->db->escape(serialize($item_results['array'])) ."' "
				// 	);


				
				//unset($this->session->data['item_api_status']);
			
		//} 
		// $product_info =  array();

		// $allwebhook_response_query = $this->db->query("SELECT DISTINCT `array` FROM " . DB_PREFIX . "allwebhook_response WHERE  `api_for` = 'Store_Api' ORDER BY id DESC LIMIT 1");
		
		// if($allwebhook_response_query->num_rows > 0){
		// 	$pproduct_info = unserialize(unserialize($allwebhook_response_query->row['array']));
		// } 
		// if(isset($pproduct_info[0] )){
		// 	$product_info = $pproduct_info[0];
		// }
		

		//echo "<pre>";print_r($product_info);exit;
		

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		

		if (!empty($product_info['ref_id'])) {
			$data['entry_store_ref_id'] = $product_info['ref_id'];
		} else {
			$data['entry_store_ref_id'] = '';
		}

		if (!empty($product_info['active'])) {
			$data['entry_ordering_enabled'] = $product_info['active'];
		} else {
			$data['entry_ordering_enabled'] = '';
		}

		if (!empty($product_info['name'])) {
			$data['entry_store_nme'] = $product_info['name'];
		} else {
			$data['entry_store_nme'] = '';
		}

		if (!empty($product_info['address'])) {
			$data['entry_store_add'] = $product_info['address'];
		} else {
			$data['entry_store_add'] = '';
		}

		if (!empty($product_info['city'])) {
			$data['entry_store_city'] = $product_info['city'];
		} else {
			$data['entry_store_city'] = '';
		}

		if (!empty($product_info['zip_codes'])) {
			$data['entry_store_zipcode'] = $product_info['zip_codes'][0];
		} else {
			$data['entry_store_zipcode'] = '';
		}

		if (!empty($product_info['contact_phone'])) {
			$data['entry_store_contact_phone'] = $product_info['contact_phone'];
		} else {
			$data['entry_store_contact_phone'] = '';
		}
		//echo "<pre>";print_r($product_info);exit;

		if (!empty($product_info['notification_phones'])) {
			$notification_phones = '';
			foreach($product_info['notification_phones'] as $notikey => $notivalue){
				if($notivalue != ''){
					$notification_phones .= $notivalue.',' ;
				}
				$final_notification = '';
				if($notification_phones != ''){
					$final_notification = rtrim($notification_phones ,',');
				}
			}
			$data['entry_store_notification_no'] = $final_notification;
		} else {
			$data['entry_store_notification_no'] = '';
		}

		if (!empty($product_info['notification_emails'])) {
			$notification_email = '';
			foreach($product_info['notification_emails'] as $notimkey => $notimvalue){
				if($notimvalue != ''){
					$notification_email .= $notimvalue.',' ;
				}
				$final_notification_emails = '';
				if($notification_email != ''){
					$final_notification_emails = rtrim($notification_email ,',');
				}
			}
			$data['notification_emails'] = $final_notification_emails;
		} else {
			$data['notification_emails'] = '';
		}



		if (!empty($product_info)) {
			$data['entry_minimum_pick_time'] = $product_info['min_pickup_time'];
		} else {
			$data['entry_minimum_pick_time'] = 900;
		}

		// if (!empty($product_info)) {
		// 	$data['entry_packaging_charge'] = $product_info['delivery_charge'];
		// } else {
		// 	$data['entry_packaging_charge'] = 10;
		// }

		

		if (!empty($product_info)) {
			$data['entry_minimum_delivery_time'] = $product_info['min_delivery_time'];
		} else {
			$data['entry_minimum_delivery_time'] = 1800;
		}

		if (isset($this->request->post['entry_minimum_order_value'])) {
			$data['entry_minimum_order_value'] = $this->request->post['entry_minimum_order_value'];
		} elseif (!empty($product_info)) {
			$data['entry_minimum_order_value'] = $product_info['min_order_value'];
		} else {
			$data['entry_minimum_order_value'] = 200;
		}

		if (!empty($product_info)) {
			$data['entry_geo_longitude'] = $product_info['geo_longitude'];
		} else {
			$data['entry_geo_longitude'] = '';
		}

		if (!empty($product_info)) {
			$data['entry_geo_latitude'] = $product_info['geo_latitude'];
		} else {
			$data['entry_geo_latitude'] = '';
		}

		if (!empty($product_info)) {
			$data['store_all'] = $product_info['timings'];
		} else {
			$data['store_all'] = array();
		}

		if (!empty($product_info)) {
			$data['platform_datas'] = $product_info['platform_data'];;
		} else {
			$data['platform_datas'] = array();
		}
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/urbanpier_store_list', $data));
	}

	public function item_onoff() {
		
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$store_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
		if($store_sql->num_rows > 0){
			$store_name = $store_sql->row['ref_id'];
		} else {
			$store_name = 'ador12';
		}

  		$body_data = array(
  			// 'merchant_id' =>  MERCHANT_ID
  			'location_ref_id' => $store_name,
  			//'option_ref_ids' =>  array()
  		);
  			
  		if(isset($this->request->get['item_code'])){
  			if($this->request->get['inapp'] == 1){
			//echo '<pre>'; print_r($this->request->get) ;exit;
	  			$item_code = $this->request->get['item_code'];
	  			$item_status = $this->request->get['item_onoff'];

	  			$body_data['item_ref_ids'][] = $item_code;
	  			$body_data['option_ref_ids'] = array();
	  			if($item_status == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}

		  		$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$item_code."' ");
		  		$this->db->query("UPDATE oc_item_urbanpiper SET item_onoff_status = '".$item_onoff_status."' WHERE item_id = '".$item_code."' ");
	  			$sync_data = 1; 
	  		} else {
	  			$sync_data = 0; 
	  		}
	  	} else {
	  		$item_datas = $this->db->query("SELECT `item_id`,`item_onoff_status` FROM oc_item_urbanpiper WHERE 1 = 1 ")->rows;
	  		foreach ($item_datas as $akey => $avalue) {
		  		$body_data['item_ref_ids'][] = $avalue['item_id'];
		  		$body_data['option_ref_ids'] = array();
	  			if($avalue['item_onoff_status'] == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}
	  			$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$avalue['item_id']."' ");
	  			$this->db->query("UPDATE oc_item_urbanpiper SET item_onoff_status = '".$item_onoff_status."' WHERE item_id = '".$avalue['item_id']."' ");
	  		}
	  		$sync_data = 1; 
	  	}
	  
	  	//$body_data['action'] = 'disable';
		//echo '<pre>'; print_r($body_data);exit;
		if($sync_data == 1){
			$body = json_encode($body_data);//echo '<pre>'; print_r($body);exit;
			$curl = curl_init();

			curl_setopt_array($curl, array(
			 // CURLOPT_URL => 'https://pos-int.urbanpiper.com/hub/api/v1/items/',
			CURLOPT_URL => ITEM_ON_OFF,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>$body,
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:'.URBANPIPER_API_KEY,
			    'Content-Type: application/json',
			  ),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
			    $error_msg = curl_error($curl);
			    
			}
			$datas = json_decode($response,true);
			if (isset($error_msg)) {
				
			}
			curl_close($curl);
			$datas = json_decode($response,true);
			//echo $response;exit;
			// echo'<pre>';
			// print_r($value);
			// exit;
			if($datas['status'] == 'success'){
				$this->session->data['success'] = 'Item Status Updated Successfully...!';
			} elseif($datas['status'] == 'error') {
				$this->session->data['errors'] = $response['error'];
			}else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
			} else {
				$this->session->data['errors'] = $response;
			}
		} else {
			$this->session->data['errors'] = 'Item is not for Sync';

		}

		

		//$this->session->data['success'] = 'Short Name Updated Successfully';

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->redirect($this->url->link('catalog/urbanpiper_store_list', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/urbanpiper_store_list');

			$data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_urbanpiperitem_show->getItems($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_name'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecode() {
		$json = array();

		if (isset($this->request->get['filter_item_code'])) {
			$this->load->model('catalog/urbanpiper_store_list');

			$data = array(
				'filter_item_code' => $this->request->get['filter_item_code'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_urbanpiperitem_show->getItems($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					//'item_id' => $result['item_code'],
					'item_code'        => strip_tags(html_entity_decode($result['item_id'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_code'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_store_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (($this->request->post['webhook_url'] == ' ')) {
			$this->error['error_webhook_url'] = 'Please Add Webhook Url';
		}

		if (($this->request->post['webhook_event'] == ' ')) {
			$this->error['error_webhook_event'] = 'Please Add Webhook Event';
		}

		if (($this->request->post['retries_mode'] == ' ')) {
			$this->error['error_retries_mode'] = 'Please Select Retries Mode';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_store_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_store_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	
}
