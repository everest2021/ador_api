<?php

class ControllerCatalogTournament extends Controller {

	private $error = array();
	public function index() {

		$this->load->language('catalog/tournament');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/tournament');
		$this->getList();
	}

	public function add() {

		$this->load->language('catalog/tournament');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/tournament');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			/*echo '<pre>';

		print_r($this->request->post);

		exit;*/





			$this->model_catalog_tournament->addactivity($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['sort'])) {

				$url .= '&sort=' . $this->request->get['sort'];

			}



			if (isset($this->request->get['order'])) {

				$url .= '&order=' . $this->request->get['order'];

			}



			if (isset($this->request->get['page'])) {

				$url .= '&page=' . $this->request->get['page'];

			}



			$this->response->redirect($this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true));

		}



		$this->getForm();

	}



	public function edit() {

		$this->load->language('catalog/tournament');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/tournament');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_catalog_tournament->editactivity($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_sport_type'])) {

				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];

			}



			if (isset($this->request->get['sort'])) {

				$url .= '&sort=' . $this->request->get['sort'];

			}



			if (isset($this->request->get['order'])) {

				$url .= '&order=' . $this->request->get['order'];

			}



			if (isset($this->request->get['page'])) {

				$url .= '&page=' . $this->request->get['page'];

			}



			$this->response->redirect($this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true));

		}



		$this->getForm();

	}



	public function delete() {

		$this->load->language('catalog/tournament');



		$this->document->setTitle($this->language->get('heading_title'));



		$this->load->model('catalog/tournament');



		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $id) {

				$this->model_catalog_tournament->deleteactivity($id);

			}



			$this->session->data['success'] = $this->language->get('text_success');



			$url = '';



			if (isset($this->request->get['filter_name'])) {

				$url .= '&filter_name=' . $this->request->get['filter_name'];

			}



			// if (isset($this->request->get['filter_department_id'])) {

			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];

			// }



			if (isset($this->request->get['filter_sport_type'])) {

				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];

			}



			if (isset($this->request->get['sort'])) {

				$url .= '&sort=' . $this->request->get['sort'];

			}



			if (isset($this->request->get['order'])) {

				$url .= '&order=' . $this->request->get['order'];

			}



			if (isset($this->request->get['page'])) {

				$url .= '&page=' . $this->request->get['page'];

			}



			$this->response->redirect($this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true));

		}



		$this->getList();

	}



	protected function getList() {

		if (isset($this->request->get['filter_name'])) {

			$filter_name = $this->request->get['filter_name'];

		} else {

			$filter_name = null;

		}



		// if (isset($this->request->get['filter_department_id'])) {

		// 	$filter_department_id = $this->request->get['filter_department_id'];

		// } else {

		// 	$filter_department_id = null;

		// }



		if (isset($this->request->get['sort'])) {

			$sort = $this->request->get['sort'];

		} else {

			$sort = 'name';

		}



		if (isset($this->request->get['order'])) {

			$order = $this->request->get['order'];

		} else {

			$order = 'ASC';

		}



		if (isset($this->request->get['page'])) {

			$page = $this->request->get['page'];

		} else {

			$page = 1;

		}



		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}



		// if (isset($this->request->get['filter_department_id'])) {

		// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];

		// }



		if (isset($this->request->get['sort'])) {

			$url .= '&sort=' . $this->request->get['sort'];

		}



		if (isset($this->request->get['order'])) {

			$url .= '&order=' . $this->request->get['order'];

		}



		if (isset($this->request->get['page'])) {

			$url .= '&page=' . $this->request->get['page'];

		}



		$data['breadcrumbs'] = array();



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)

		);



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('heading_title'),

			'href' => $this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true)

		);



		$data['add'] = $this->url->link('catalog/tournament/add', 'token=' . $this->session->data['token'] . $url, true);

		$data['delete'] = $this->url->link('catalog/tournament/delete', 'token=' . $this->session->data['token'] . $url, true);



		

		$filter_data = array(

			'filter_name' => $filter_name,

			//'filter_department_id' => $filter_department_id,

			'sort'  => $sort,

			'order' => $order,

			'start' => ($page - 1) * $this->config->get('config_limit_admin'),

			'limit' => $this->config->get('config_limit_admin')

		);



		$activity_total = $this->model_catalog_tournament->getTotalactivity();



		$results = $this->model_catalog_tournament->getactivitys($filter_data);

		//echo '<pre>';

		//print_r($results);

		//exit();

		$data['activitys'] = array();

		foreach ($results as $result) {

			$data['activitys'][] = array(

				'id'		=>$result['id'],

				'discription'        => $result['discription'],

				'customer_name'        => $result['customer_name'],

				'edit'        => $this->url->link('catalog/tournament/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)

			);

		}



		$data['token'] = $this->session->data['token'];



		

		$data['heading_title'] = $this->language->get('heading_title');



		$data['text_list'] = $this->language->get('text_list');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['text_confirm'] = $this->language->get('text_confirm');



		$data['column_name'] = $this->language->get('column_name');

		//$data['column_capacity'] = $this->language->get('column_capacity');

		$data['column_rate'] = $this->language->get('column_rate');

		$data['column_action'] = $this->language->get('column_action');



		$data['button_add'] = $this->language->get('button_add');

		$data['button_edit'] = $this->language->get('button_edit');

		$data['button_delete'] = $this->language->get('button_delete');



		if (isset($this->error['warning'])) {

			$data['error_warning'] = $this->error['warning'];

		} else {

			$data['error_warning'] = '';

		}



		if (isset($this->session->data['success'])) {

			$data['success'] = $this->session->data['success'];



			unset($this->session->data['success']);

		} else {

			$data['success'] = '';

		}



		if (isset($this->request->post['selected'])) {

			$data['selected'] = (array)$this->request->post['selected'];

		} else {

			$data['selected'] = array();

		}



		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}



		// if (isset($this->request->get['filter_department_id'])) {

		// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];

		// }



		



		if ($order == 'ASC') {

			$url .= '&order=DESC';

		} else {

			$url .= '&order=ASC';

		}



		if (isset($this->request->get['page'])) {

			$url .= '&page=' . $this->request->get['page'];

		}

		$data['sort_name'] = $this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);

		

		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}



		// if (isset($this->request->get['filter_department_id'])) {

		// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];

		// }





		if (isset($this->request->get['sort'])) {

			$url .= '&sort=' . $this->request->get['sort'];

		}



		if (isset($this->request->get['order'])) {

			$url .= '&order=' . $this->request->get['order'];

		}



		$pagination = new Pagination();

		$pagination->total = $activity_total;

		$pagination->page = $page;

		$pagination->limit = $this->config->get('config_limit_admin');

		$pagination->url = $this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);



		$data['pagination'] = $pagination->render();



		$data['results'] = sprintf($this->language->get('text_pagination'), ($activity_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($activity_total - $this->config->get('config_limit_admin'))) ? $activity_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $activity_total, ceil($activity_total / $this->config->get('config_limit_admin')));



		$data['filter_name'] = $filter_name;

		// $data['filter_department_id'] = $filter_department_id;

		



		$data['sort'] = $sort;

		$data['order'] = $order;



		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');



		$this->response->setOutput($this->load->view('catalog/tournament_list', $data));

	}



	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');



		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$data['text_loading'] = $this->language->get('text_loading');



		$data['entry_name'] = $this->language->get('entry_name');

		$data['entry_rate'] = $this->language->get('entry_rate');

		// $data['entry_capacity'] = $this->language->get('entry_capacity');

		$data['entry_filename'] = $this->language->get('entry_filename');

		$data['entry_mask'] = $this->language->get('entry_mask');



		$data['help_filename'] = $this->language->get('help_filename');

		$data['help_mask'] = $this->language->get('help_mask');



		$data['button_save'] = $this->language->get('button_save');

		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['button_upload'] = $this->language->get('button_upload');



		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {

			$data['error_name'] = $this->error['name'];

		} else {

			$data['error_name'] = array();

		}

		if (isset($this->error['rate'])) {

			$data['error_rate'] = $this->error['rate'];

		} else {

			$data['error_rate'] = array();

		}

		// if (isset($this->error['capacity'])) {

		// 	$data['error_capacity'] = $this->error['capacity'];

		// } else {

		// 	$data['error_capacity'] = array();

		// }



		$url = '';



		if (isset($this->request->get['filter_name'])) {

			$url .= '&filter_name=' . $this->request->get['filter_name'];

		}



		// if (isset($this->request->get['filter_department_id'])) {

		// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];

		// }



		if (isset($this->request->get['sort'])) {

			$url .= '&sort=' . $this->request->get['sort'];

		}



		if (isset($this->request->get['order'])) {

			$url .= '&order=' . $this->request->get['order'];

		}



		if (isset($this->request->get['page'])) {

			$url .= '&page=' . $this->request->get['page'];

		}



		$data['breadcrumbs'] = array();



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/tournament', 'token=' . $this->session->data['token'], true)

		);



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('heading_title'),

			'href' => $this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true)

		);



		if (!isset($this->request->get['id'])) {

			$data['action'] = $this->url->link('catalog/tournament/add', 'token=' . $this->session->data['token'] . $url, true);

		} else {

			$data['action'] = $this->url->link('catalog/tournament/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);

		}



		$data['cancel'] = $this->url->link('catalog/tournament', 'token=' . $this->session->data['token'] . $url, true);



		$this->load->model('localisation/language');



		$data['languages'] = $this->model_localisation_language->getLanguages();



		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {

			$activity_info = $this->model_catalog_tournament->getactivity($this->request->get['id']);

		}



		$data['token'] = $this->session->data['token'];



		// if (isset($this->request->get['department_id'])) {

		// 	$data['department_id'] = $this->request->get['department_id'];

		// } elseif (!empty($venue_info)) {

		// 	$data['department_id'] = $venue_info['department_id'];

		// } else {

		// 	$data['department_id'] = '';

		// }



		if (isset($this->request->post['tournament_name'])) {
			$data['tournament_name'] = $this->request->post['tournament_name'];
		} elseif (!empty($activity_info)) {
			$data['tournament_name'] = $activity_info['tournament_name'];
		} else {
			$data['tournament_name'] = '';
		}

		if (isset($this->request->post['from_date'])) {
			$data['from_date'] = $this->request->post['from_date'];
		} elseif (!empty($activity_info)) {
			$data['from_date'] = $activity_info['from_date'];
		} else {
			$data['from_date'] = '';
		}

		if (isset($this->request->post['to_date'])) {
			$data['to_date'] = $this->request->post['to_date'];
		} elseif (!empty($activity_info)) {
			$data['to_date'] = $activity_info['to_date'];
		} else {
			$data['to_date'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($activity_info)) {
			$data['status'] = $activity_info['status'];
		} else {
			$data['status'] = '';
		}

		if (isset($this->request->post['type'])) {
			$data['type'] = $this->request->post['type'];
		} elseif (!empty($activity_info)) {
			$data['type'] = $activity_info['type'];
		} else {
			$data['type'] = '';
		}

		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
			$data['photo_source'] = $this->request->post['photo_source'];
		} elseif (!empty($activity_info)) {
			$data['photo'] = $activity_info['photo'];
			$data['photo_source'] = $activity_info['photo_source'];
		} else {	
			$data['photo'] = '';
			$data['photo_source'] = '';
		}

		if (isset($this->request->post['sports'])) {
			$data['sports'] = $this->request->post['sports'];
		} elseif (!empty($activity_info)) {
			$data['sports'] = $activity_info['sports'];
		} else {
			$data['sports'] = '';
		}

		if (isset($this->request->post['rate'])) {
			$data['rate'] = $this->request->post['rate'];
		} elseif (!empty($activity_info)) {
			$data['rate'] = $activity_info['rate'];
		} else {
			$data['rate'] = '';
		}

		// if (isset($this->request->post['capacity'])) {

		// 	$data['capacity'] = $this->request->post['capacity'];

		// } elseif (!empty($venue_info)) {

		// 	$data['capacity'] = $venue_info['capacity'];

		// } else {

		// 	$data['capacity'] = '';

		// }

		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');



		$this->response->setOutput($this->load->view('catalog/tournament_form', $data));

	}

	public function upload() {
		$this->load->language('catalog/item');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/item')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}



	protected function validateForm() {

		if (!$this->user->hasPermission('modify', 'catalog/activity')) {

			$this->error['warning'] = $this->language->get('error_permission');

		}



		$datas = $this->request->post;

		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {

		// 	$this->error['name'] = 'Please Enter  Name';

		// }

		// if ((utf8_strlen($this->request->post['rate']) < 2) || (utf8_strlen($this->request->post['rate']) > 255)) {

		// 	$this->error['rate'] = 'Please Enter  rate';

		// }

		// if ((utf8_strlen($this->request->post['capacity']) < 2) || (utf8_strlen($this->request->post['capacity']) > 255)) {

		// 	$this->error['capacity'] = 'Please Enter  capacity';

		// }

		// // else {

		// 	if(isset($this->request->get['department_id'])){

		// 		$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `name` = '".$datas['name']."' AND `department_id` <> '".$this->request->get['department_id']."' ");

		// 		if($is_exist->num_rows > 0){

		// 			$this->error['name'] = 'Department Name Already Exists';

		// 		}

		// 	} else {

		// 		$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `name` = '".$datas['name']."' ");

		// 		if($is_exist->num_rows > 0){

		// 			$this->error['name'] = 'Department Name Already Exists';

		// 		}

		// 	}

		// }



		return !$this->error;

	}



	protected function validateDelete() {

		// if (!$this->user->hasPermission('modify', 'catalog/tournament')) {

		// 	$this->error['warning'] = $this->language->get('error_permission');

		// }



		$this->load->model('catalog/tournament');



		return !$this->error;

	}



	

	public function autocomplete() {

		$json = array();



		if (isset($this->request->get['filter_name'])) {

			$this->load->model('catalog/tournament');



			$filter_data = array(

				'filter_name' => $this->request->get['filter_name'],

				'start'       => 0,

				'limit'       => 20

			);



			$results = $this->model_catalog_tournament->getactivitys($filter_data);



			foreach ($results as $result) {

				$json[] = array(

					'id' => $result['id'],

					'customer_name'        => strip_tags(html_entity_decode($result['customer_name'], ENT_QUOTES, 'UTF-8'))

				);

			}

		}



		$sort_order = array();



		foreach ($json as $key => $value) {

			$sort_order[$key] = $value['customer_name'];

		}



		array_multisort($sort_order, SORT_ASC, $json);



		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));

	}

}