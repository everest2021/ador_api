<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogPurchaseentryFood extends Controller {
	private $error = array();
	
	public function index() {

		$this->load->language('catalog/purchaseentryfood');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseentry');

		$this->getForm();
	}

	public function add() {
		$this->load->language('catalog/purchaseentryfood');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseentry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$id = $this->model_catalog_purchaseentry->addpurchaseentry($this->request->post);
			$this->prints($id);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['year'])) {
				$url .= '&year=' . $this->request->get['year'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//$url .= '&id='.$id;.'&submit_fun=1'
			$this->response->redirect($this->url->link('catalog/purchaseentryfood/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/purchaseentryfood');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseentry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$id = $this->model_catalog_purchaseentry->editpurchaseentry($this->request->post, $this->request->get['id']);
			$this->prints($id);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['year'])) {
				$url .= '&year=' . $this->request->get['year'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$url .= '&id='.$id;

			$this->response->redirect($this->url->link('catalog/purchaseentryfood/getForm', 'token=' . $this->session->data['token'] . $url.'&submit_fun=1', true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/purchaseentryfood');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseentry');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $po_numbers) {
				$po_numbers_exp = explode('/', $po_numbers);
				
				$po_number = $po_numbers_exp[0];
				$year = $po_numbers_exp[1];
				$pre_po = $po_numbers_exp[2];
				$this->model_catalog_purchaseentry->deletepurchaseentry($po_number, $year , $pre_po);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/purchaseentryfood', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function getForm() {
		$this->load->language('catalog/purchaseentryfood');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/purchaseentry');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['po_datas'])) {
			$data['error_po_datas'] = $this->error['po_datas'];
		} else {
			$data['error_po_datas'] = '';
		}

		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_number'])) {
			$url .= '&filter_number=' . $this->request->get['filter_number'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_mname'])) {
			$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('catalog/purchaseentryfood', 'token=' . $this->session->data['token'] . $url, true);

		$purchaseentry_info = array();
		if(isset($this->request->get['supplier_id']) && isset($this->request->get['filter_invoice_no']) && isset($this->request->get['category']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$purchaseentry_info = $this->model_catalog_purchaseentry->getPurchaseentryby_supp_invoice($this->request->get['supplier_id'], $this->request->get['filter_invoice_no'], $this->request->get['category']);
		} elseif (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$purchaseentry_info = $this->model_catalog_purchaseentry->getPurchaseentry($this->request->get['id']);
		}

		// echo '<pre>';
		// print_r($purchaseentry_info);
		// exit;

		if(isset($purchaseentry_info[0]['id'])){
			$data['edit'] = 0;
		} else {
			$data['edit'] = 1;
		}

		$data['redirect_url'] =  $this->url->link('catalog/purchaseentryfood/add', 'token=' . $this->session->data['token'], true);
		
		if (!isset($this->request->get['id']) && !isset($this->request->get['supplier_id'])) {
			$data['action'] = $this->url->link('catalog/purchaseentryfood/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['action1'] = $this->url->link('catalog/purchaseentryfood/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			if(isset($this->request->get['id'])){
				$data['action'] = $this->url->link('catalog/purchaseentryfood/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
				$data['action1'] = $this->url->link('catalog/purchaseentryfood/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
			} else {
				if(isset($purchaseentry_info[0]['id'])){
					$id = $purchaseentry_info[0]['id'];
					$data['action'] = $this->url->link('catalog/purchaseentryfood/edit', 'token=' . $this->session->data['token'] . '&id=' . $id . $url, true);
					$data['action1'] = $this->url->link('catalog/purchaseentryfood/edit', 'token=' . $this->session->data['token'] . '&id=' . $id . $url, true);
				} else {
					$data['action'] = $this->url->link('catalog/purchaseentryfood/add', 'token=' . $this->session->data['token'] . $url, true);
					$data['action1'] = $this->url->link('catalog/purchaseentryfood/add', 'token=' . $this->session->data['token'] . $url, true);	
				}
			}
		}

		$data['token'] = $this->session->data['token'];

		// if (isset($this->request->post['po_number'])) {
		// 	$data['po_number'] = $this->request->post['po_number'];
		// } elseif (isset($purchaseentry_info[0]['po_number'])) {
		// 	if($purchaseentry_info[0]['po_number'] != '0'){
		// 		$data['po_number'] = $purchaseentry_info[0]['po_number'].'/'.$purchaseentry_info[0]['po_prefix'];
		// 	} else {
		// 		$po_numbers = $this->db->query("SELECT `po_number` FROM `oc_purchaseentry` WHERE `year` = '".$data['year']."' ORDER BY `po_number` DESC LIMIT 1");
		// 		if($po_numbers->num_rows > 0){
		// 			$po_number = $po_numbers->row['po_number'] + 1;
		// 		} else {
		// 			$po_number = 1;
		// 		}

		// 		if(date('n') <= 3){
		// 			$prev_year = date('y',strtotime("-1 year"));
		// 			$current_year = date('y');
		// 			$po_prefix = $prev_year.'-'.$current_year;
		// 		} else {
		// 			$current_year = date('y');
		// 			$next_year = date('y',strtotime("+1 year"));
		// 			$po_prefix = $current_year.'-'.$next_year;
		// 		}

		// 		$data['po_number'] = $po_number.'/0/'.$po_prefix;//$po_number.'/'.date('Y');	
		// 	}
		// } else {
		// 	$po_numbers = $this->db->query("SELECT `po_number` FROM `oc_purchaseentry` WHERE `year` = '".$data['year']."' ORDER BY `po_number` DESC LIMIT 1");
		// 	if($po_numbers->num_rows > 0){
		// 		$po_number = $po_numbers->row['po_number'] + 1;
		// 	} else {
		// 		$po_number = 1;
		// 	}
		// 	if(date('n') <= 3){
		// 		$prev_year = date('y',strtotime("-1 year"));
		// 		$current_year = date('y');
		// 		$po_prefix = $prev_year.'-'.$current_year;
		// 	} else {
		// 		$current_year = date('y');
		// 		$next_year = date('y',strtotime("+1 year"));
		// 		$po_prefix = $current_year.'-'.$next_year;
		// 	}

		// 	$data['po_number'] = $po_number.'/0/'.$po_prefix;
		// }
		
		if (isset($this->request->post['id'])) {
			$data['id'] = $this->request->post['id'];
		} elseif (isset($purchaseentry_info[0]['id'])) {
			$data['id'] = $purchaseentry_info[0]['id'];
		} else {
			$data['id'] = '0';
		}	

		if (isset($this->request->post['filter_invoice_no'])) {
			$data['filter_invoice_no'] = $this->request->post['filter_invoice_no'];
		}  elseif(isset($this->request->get['filter_invoice_no'])){
			$data['filter_invoice_no'] = $this->request->get['filter_invoice_no'];
		} else {
			$data['filter_invoice_no'] = '';
		}

		$suppliers_datas = $this->db->query("SELECT `supplier_id`, `supplier` FROM `oc_supplier` ORDER BY `supplier` ")->rows;
		$data['suppliers'] = array();
		foreach($suppliers_datas as $skey => $svalue){
			$data['suppliers'][$svalue['supplier_id']] = $svalue['supplier'];
		}
		
		if (isset($this->request->post['supplier_id'])) {
			$data['supplier_name'] = $this->request->post['supplier_name'];
			$data['supplier_id'] = $this->request->post['supplier_id'];
		} elseif (isset($purchaseentry_info[0]['supplier_name'])) {
			$data['supplier_name'] = $purchaseentry_info[0]['supplier_name'];
			$data['supplier_id'] = $purchaseentry_info[0]['supplier_id'];
		} elseif(isset($this->request->get['supplier_id'])){
			$data['supplier_name'] = $this->request->get['supplier_name'];
			$data['supplier_id'] = $this->request->get['supplier_id'];
		} else {
			$data['supplier_name'] = '';
			$data['supplier_id'] = '0';
		}

		if (isset($this->request->post['invoice_date'])) {
			$data['invoice_date'] = $this->request->post['invoice_date'];
		} elseif (isset($purchaseentry_info[0]['invoice_date'])) {
			if($purchaseentry_info[0]['invoice_date'] != '0000-00-00' && $purchaseentry_info[0]['invoice_date'] != '1970-01-01'){
				$data['invoice_date'] = date('d-m-Y', strtotime($purchaseentry_info[0]['invoice_date']));
			} else {
				$data['invoice_date'] = '';
			}
		} else {
			$data['invoice_date'] = date('d-m-Y');
		}

		if (isset($this->request->post['invoice_no'])) {
			$data['invoice_no'] = $this->request->post['invoice_no'];
		} elseif (isset($purchaseentry_info[0]['invoice_no'])) {
			$data['invoice_no'] = $purchaseentry_info[0]['invoice_no'];
		} else {
			$invoice_no = $this->db->query("SELECT invoice_no FROM oc_purchase_transaction ORDER BY invoice_no DESC LIMIT 1");
			if($invoice_no->num_rows > 0){
				$data['invoice_no'] = $invoice_no->row['invoice_no'] + 1;
			} else{
				$data['invoice_no'] = 1;
			}
		}

		$stores_datas = $this->db->query("SELECT `id`, `store_name` FROM `oc_store_name` WHERE store_type = 'Food' ORDER BY `store_name` ")->rows;
		$data['stores'] = array();
		foreach($stores_datas as $skey => $svalue){
			$data['stores'][$svalue['id']] = $svalue['store_name'];
		}

		if (isset($this->request->post['store_id'])) {
			$data['store_name'] = $this->request->post['store_name'];
			$data['store_id'] = $this->request->post['store_id'];
		} elseif (isset($purchaseentry_info[0]['store_name'])) {
			$data['store_name'] = $purchaseentry_info[0]['store_name'];
			$data['store_id'] = $purchaseentry_info[0]['store_id'];
		} else {
			$data['store_name'] = '';
			$data['store_id'] = '0';
		}

		if (isset($this->request->post['update_master'])) {
			$data['update_master'] = $this->request->post['update_master'];
		} elseif (isset($purchaseentry_info[0]['update_master'])) {
			$data['update_master'] = $purchaseentry_info[0]['update_master'];
		} else {
			$data['update_master'] = '0';
		}

		if (isset($this->request->post['narration'])) {
			$data['narration'] = $this->request->post['narration'];
		} elseif (isset($purchaseentry_info[0]['narration'])) {
			$data['narration'] = $purchaseentry_info[0]['narration'];
		} else {
			$data['narration'] = '';
		}

		if (isset($this->request->post['category'])) {
			$data['category'] = $this->request->post['category'];
		} elseif (isset($purchaseentry_info[0]['category'])) {
			$data['category'] = $purchaseentry_info[0]['category'];
		} else {
			$data['category'] = '';
		}

		$data['categories'] = array(
			'Food' => 'Food',
			'Liquor' => 'Liquor',
		);

		if (isset($this->request->post['tp_no'])) {
			$data['tp_no'] = $this->request->post['tp_no'];
		} elseif (isset($purchaseentry_info[0]['tp_no'])) {
			$data['tp_no'] = $purchaseentry_info[0]['tp_no'];
		} else {
			$data['tp_no'] = '';
		}

		if (isset($this->request->post['batch_no'])) {
			$data['batch_no'] = $this->request->post['batch_no'];
		} elseif (isset($purchaseentry_info[0]['batch_no'])) {
			$data['batch_no'] = $purchaseentry_info[0]['batch_no'];
		} else {
			$data['batch_no'] = '';
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (isset($purchaseentry_info[0]['total_qty'])) {
			$data['total_qty'] = $purchaseentry_info[0]['total_qty'];
		} else {
			$data['total_qty'] = '';
		}

		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (isset($purchaseentry_info[0]['total_amt'])) {
			$data['total_amt'] = $purchaseentry_info[0]['total_amt'];
		} else {
			$data['total_amt'] = '';
		}

		if (isset($this->request->post['total_cash_discount'])) {
			$data['total_cash_discount'] = $this->request->post['total_cash_discount'];
		} elseif (isset($purchaseentry_info[0]['total_cash_discount'])) {
			$data['total_cash_discount'] = $purchaseentry_info[0]['total_cash_discount'];
		} else {
			$data['total_cash_discount'] = '';
		}

		if (isset($this->request->post['total_tax'])) {
			$data['total_tax'] = $this->request->post['total_tax'];
		} elseif (isset($purchaseentry_info[0]['total_tax'])) {
			$data['total_tax'] = $purchaseentry_info[0]['total_tax'];
		} else {
			$data['total_tax'] = '';
		}

		if (isset($this->request->post['total_add_charges'])) {
			$data['total_add_charges'] = $this->request->post['total_add_charges'];
		} elseif (isset($purchaseentry_info[0]['total_add_charges'])) {
			$data['total_add_charges'] = $purchaseentry_info[0]['total_add_charges'];
		} else {
			$data['total_add_charges'] = '';
		}

		if (isset($this->request->post['total_pay'])) {
			$data['total_pay'] = $this->request->post['total_pay'];
		} elseif (isset($purchaseentry_info[0]['total_pay'])) {
			$data['total_pay'] = $purchaseentry_info[0]['total_pay'];
		} else {
			$data['total_pay'] = '';
		}

		$units = array();
		if (isset($this->request->post['po_datas'])) {
			$data['po_datas'] = $this->request->post['po_datas'];
		} elseif(isset($purchaseentry_info[0]['id'])){
			$total = 0;
			$purchaseentry_item_info = $this->db->query("SELECT * FROM `oc_purchase_items_transaction` WHERE `p_id` = '".$purchaseentry_info[0]['id']."' ")->rows;
			foreach ($purchaseentry_item_info as $tkey => $tvalue) {
				$data['po_datas'][$tkey]['id'] = $tvalue['id'];
				$data['po_datas'][$tkey]['p_id'] = $tvalue['p_id'];
				$data['po_datas'][$tkey]['description'] = $tvalue['description'];
				$data['po_datas'][$tkey]['description_id'] = $tvalue['description_id'];
				$data['po_datas'][$tkey]['description_code'] = $tvalue['description_code'];
				$data['po_datas'][$tkey]['description_barcode'] = $tvalue['description_barcode'];
				$data['po_datas'][$tkey]['description_code_search'] = $tvalue['description_code_search'];
				$data['po_datas'][$tkey]['item_category'] = $tvalue['item_category'];
				$data['po_datas'][$tkey]['qty'] = $tvalue['qty'];

				// $availableQty = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$tvalue['unit_id']."' AND item_code = '".$tvalue['description_code']."' ORDER BY id DESC LIMIT 1")->row;

				// $purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$tvalue['description_id']."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$tvalue['unit_id']."' AND category = 'Food' GROUP BY unit_id, description_id");
			
				// if($purchase_order->num_rows > 0){
				// 	$avq = $availableQty['closing_balance'] + $purchase_order->row['qty'];
				// } else{
				// 	$avq = $availableQty['closing_balance'];
				// }
				$stock_type = $this->db->query("SELECT item_type FROM oc_stock_item WHERE item_code = '".$tvalue['description_code']."'")->row;
				$avq = $this->model_catalog_purchaseentry->availableQuantity($tvalue['unit_id'],$tvalue['description_id'],'0', $purchaseentry_info[0]['store_id'], $stock_type['item_type'], $tvalue['description_code']);

				$data['po_datas'][$tkey]['avq'] = $avq;
				$data['po_datas'][$tkey]['type_id'] = $tvalue['type'];

				$unitss = $this->db->query("SELECT * FROM `oc_unit`")->rows;
				$units = array();
				foreach($unitss as $ukey => $uvalue){
					$units[$uvalue['unit_id']] = $uvalue['unit'];
				}
				// echo '<pre>';
				// print_r($units);
				// exit;

				$data['po_datas'][$tkey]['units'] = $units;
				
				$data['po_datas'][$tkey]['unit_id'] = $tvalue['unit_id'];
				$data['po_datas'][$tkey]['unit'] = $tvalue['unit'];
				$data['po_datas'][$tkey]['rate'] = $tvalue['rate'];
				$data['po_datas'][$tkey]['free_qty'] = $tvalue['free_qty'];
				$data['po_datas'][$tkey]['discount_percent'] = $tvalue['discount_percent'];
				$data['po_datas'][$tkey]['discount_amount'] = $tvalue['discount_amount'];
				$data['po_datas'][$tkey]['tax_name'] = $tvalue['tax_name'];
				$data['po_datas'][$tkey]['tax_percent'] = $tvalue['tax_percent'];
				$data['po_datas'][$tkey]['tax_value'] = $tvalue['tax_value'];
				$data['po_datas'][$tkey]['amount'] = $tvalue['amount'];
				if($tvalue['exp_date'] != '' && $tvalue['exp_date'] != '0000-00-00'){
					$exp_date = date('d-m-Y', strtotime($tvalue['exp_date']));
				} else {
					$exp_date = '';
				}
				$data['po_datas'][$tkey]['exp_date'] = $exp_date;
			}
		} else {	
			$data['po_datas'] = array();
		}
		$data['units'] = $units;
		$unitfood = $this->db->query("SELECT * FROM oc_unit")->rows;
		$data['units1'] = json_encode($unitfood);

		$taxess = $this->db->query("SELECT * FROM `oc_tax` ")->rows;
		$taxes = array();
		foreach($taxess as $ukey => $uvalue){
			$taxes[$uvalue['tax_value']] = $uvalue['tax_name'];
		}
		$data['taxes'] = $taxes;

		if(isset($this->request->get['submit_fun'])){
			$data['submit_fun'] = '1';
		} else {
			$data['submit_fun'] = '0';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/purchaseentryfood_form', $data));
	}

	public function prints($id){
		// echo'inn';
		// exit;
		$this->load->model('catalog/order');
		$transfer = $this->db->query("SELECT * FROM oc_purchase_transaction WHERE id = '".$id."'")->row;
		$transfer_items = $this->db->query("SELECT *, SUM(qty) as qty FROM oc_purchase_items_transaction WHERE p_id = '".$id."' GROUP BY unit_id, description_id")->rows;
		foreach($transfer_items as $transfer_item){
			$transfer_data[] = array(
				'name'			=> $transfer_item['description'],
				'qty'         	=> $transfer_item['qty'],
				'rate'          => $transfer_item['rate'],
				'amt'         	=> $transfer_item['amount']
			);
		}
		// echo'<pre>';
		// print_r($transfer_data);
		// exit;
		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->text("Purchase Entery Food");
		    $printer->feed(1);
		    $printer->text("Issue Slip");
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->feed(1);
		   	$printer->text("Supplier :".$transfer['supplier_name']);
		   	$printer->feed(1);
		   	$printer->text("Store Name :".$transfer['store_name']);
		   	$printer->feed(1);
		   	$printer->text("Slip No :".$transfer['invoice_no']);
		   	$printer->feed(1);
		   	$printer->text("Date:".$transfer['invoice_date']);
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("Description",16)."".str_pad("Qty", 10)."".str_pad("Rate", 10)."Amount");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $total_items_normal = 0;
			$total_quantity_normal = 0;
			$total_amt_normal = 0;
		    foreach($transfer_data as $nkey => $nvalue){
	    	  	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 16);
				$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$printer->text("".str_pad($nvalue['name'],16)."".str_pad($nvalue['qty'],10)."".str_pad($nvalue['rate'],10)."".$nvalue['amt']);
		    	$printer->feed(1);
		    	$total_items_normal ++ ;
		    	$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
		    	$total_amt_normal = $total_amt_normal + $nvalue['amt'];
	    	}
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		    $printer->text("T Qty : ".$total_quantity_normal."   T Item : ".$total_items_normal."  T AMT: ".$total_amt_normal."");
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(2);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getForm();

		// if(isset($this->request->get['purchase_id'])){
		// 	$this->load->model('catalog/purchaseentry');
		// 	$this->model_catalog_purchaseentry->printBarcode($this->request->get['purchase_id'], 'Food');
		// }
		// $this->response->redirect($this->url->link('catalog/purchaseentryfood', 'token=' . $this->session->data['token']));
	}



	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/purchaseentry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach($this->request->post['po_datas'] as $pkeys => $pvalues){
			if($pvalues['qty'] == '0'){
				$this->error['po_datas'][$pkeys]['qty'] = 'Please Enter the Quantity';			
			}		
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 64)) {
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if (utf8_strlen($this->request->post['keyword']) > 0) {
		// 	$this->load->model('catalog/url_alias');

		// 	$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

		// 	if ($url_alias_info && isset($this->request->get['manufacturer_id']) && $url_alias_info['query'] != 'manufacturer_id=' . $this->request->get['manufacturer_id']) {
		// 		$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
		// 	}

		// 	if ($url_alias_info && !isset($this->request->get['manufacturer_id'])) {
		// 		$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
		// 	}
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/purchaseentry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// foreach ($this->request->post['selected'] as $manufacturer_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsByManufacturerId($manufacturer_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }
		return !$this->error;
	}

	public function autocomplete_supplier() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);
			$sql = "SELECT * FROM `oc_supplier` WHERE 1=1 ";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND `supplier` LIKE '%".$this->db->escape($filter_data['filter_name'])."%' ";
			}
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			//$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'supplier_id' => $result['supplier_id'],
					'supplier'            => strip_tags(html_entity_decode($result['supplier'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['supplier'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/manufacturer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'manufacturer_id' => $result['manufacturer_id'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_description(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);
			$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND (`item_name` LIKE '%".$this->db->escape($filter_data['filter_name'])."%') ";
			}
			$sql .= " AND item_category = 'Food'";
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				if($result['uom'] != 'Please Select'){
					$units[$result['uom']]['unit_id'] = $result['uom'];
					$units[$result['uom']]['unit_name'] = $result['unit_name'];
				} else{
					$sql = "SELECT * FROM `oc_unit` WHERE 1=1 ";
					$unitss = $this->db->query($sql)->rows;
					$units = array();
					foreach($unitss as $ukey => $uvalue){
						$units[$uvalue['unit_id']]['unit_id'] = $uvalue['unit_id'];
						$units[$uvalue['unit_id']]['unit_name'] = $uvalue['unit'];
					}
				}
				$json[] = array(
					'id' 			=> $result['id'],
					'item_code' 	=> $result['item_code'],
					'stock_type'	=> $result['item_type'],
					'bar_code' 		=> $result['bar_code'],
					'tax' 			=> $result['tax'],
					'tax_value' 	=> $result['tax_value'],
					'item_category' => $result['item_category'],
					'uom' 			=> $result['uom'],
					'unit_name' 	=> $result['unit_name'],
					'units' 		=> $units,
					'item_name'     => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
					'rate'          => $result['purchase_rate']
				);
			}
			$sort_order = array();
			foreach ($json as $key => $value) {
				$sort_order[$key] = $value['item_name'];
			}
			array_multisort($sort_order, SORT_ASC, $json);
		}
		if (isset($this->request->get['filter_name_1'])) {
			// echo'<pre>';
			// print_r($this->request->get);
			// exit;
			$filter_data = array(
				'filter_name_1' => $this->request->get['filter_name_1'],
				'start'       => 0,
				'limit'       => 5
			);
			if($filter_data['filter_name_1'] != ''){
				$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1";
					$sql .= " AND (`item_code` = '".$this->db->escape($filter_data['filter_name_1'])."' OR `bar_code` = '".$this->db->escape($filter_data['filter_name_1'])."') ";

					$sql .= " AND item_category = 'Food'";
					$result = $this->db->query($sql)->row;
			}
			
			//$this->log->write($sql);
			
			
			if(isset($result['id'])){
				if($result['uom'] != 'Please Select'){
					$units[$result['uom']]['unit_id'] = $result['uom'];
					$units[$result['uom']]['unit_name'] = $result['unit_name'];
				} else{
					$sql = "SELECT * FROM `oc_unit` WHERE 1=1 ";
					$unitss = $this->db->query($sql)->rows;
					$units = array();
					foreach($unitss as $ukey => $uvalue){
						$units[$uvalue['unit_id']]['unit_id'] = $uvalue['unit_id'];
						$units[$uvalue['unit_id']]['unit_name'] = $uvalue['unit'];
					}
				}
				$json = array(
					'status'	 	=> 1,
					'id' 			=> $result['id'],
					'item_code' 	=> $result['item_code'],
					'stock_type'	=> $result['item_type'],
					'bar_code' 		=> $result['bar_code'],
					'tax' 			=> $result['tax'],
					'tax_value' 	=> $result['tax_value'],
					'item_category' => $result['item_category'],
					'uom' 			=> $result['uom'],
					'units' 		=> $units,
					'unit_name' 	=> $result['unit_name'],
					'item_name'     => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
					'rate'          => $result['purchase_rate']
				);
			} else {
				$json = array(
					'status' => 0
				);
			}


		}
		// echo'<pre>';
		// 	print_r($json);
		// 	exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function autocomplete_units(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_item_type' => $this->request->get['filter_item_type'],
				'start'       => 0,
				'limit'       => 5
			);
			$sql = "SELECT * FROM `oc_unit` WHERE 1=1 ";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND `unit` LIKE '%".$this->db->escape($filter_data['filter_name'])."%' ";
			}
			if($filter_data['filter_item_type'] != ''){
				$sql .= " AND `type` = '".$this->db->escape($filter_data['filter_item_type'])."' ";
			}
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'unit_id' 			=> $result['unit_id'],
					'unit'     => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['unit'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getAvailableQuantity(){
		$this->load->model('catalog/purchaseentry');
		$json = array();
		if (isset($this->request->get['unit_id']) && isset($this->request->get['description_id']) && isset($this->request->get['description_code']) && isset($this->request->get['store_id']) && isset($this->request->get['stock_type'])) {
			$avq = $this->model_catalog_purchaseentry->availableQuantity($this->request->get['unit_id'],$this->request->get['description_id'],'0', $this->request->get['store_id'], $this->request->get['stock_type'], $this->request->get['description_code']);
			$json = array(
				'avq' 			=> $avq
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_taxes(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);
			$sql = "SELECT * FROM `oc_tax` WHERE 1=1 ";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND `tax_name` LIKE '%".$this->db->escape($filter_data['filter_name'])."%' ";
			}
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' 			=> $result['id'],
					'tax_value' 			=> $result['tax_value'],
					'tax_name'     => strip_tags(html_entity_decode($result['tax_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['tax_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_store(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);
			$sql = "SELECT * FROM `oc_store_name` WHERE 1=1 ";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND `store_name` LIKE '%".$this->db->escape($filter_data['filter_name'])."%' ";
			}
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'id' 			=> $result['id'],
					'store_name'     => strip_tags(html_entity_decode($result['store_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['store_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
}