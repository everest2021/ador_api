<?php
class ControllerCatalogOrderapp extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/orderapp');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/orderapp');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/orderapp');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/orderapp');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
			 print_r($this->request->post);
			exit;*/

			$this->model_catalog_orderapp->addBrand($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/brand');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_brand->editBrand($this->request->get['brand_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_subcategory_id'])) {
				$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/brand');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $brand_id) {
				$this->model_catalog_brand->deleteBrand($brand_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}


	protected function getList() {

		//echo $this->request->get['order_status'];exit;

		if (isset($this->request->get['order_status'])) {
			$order_status = $this->request->get['order_status'];
		} else {
			$order_status = '0';
		}

		// if (isset($this->request->get['filter_brand_id'])) {
		// 	$filter_brand_id = $this->request->get['filter_brand_id'];
		// } else {
		// 	$filter_brand_id = null;
		// }

		// if (isset($this->request->get['filter_subcategory'])) {
		// 	$filter_subcategory = $this->request->get['filter_subcategory'];
		// } else {
		// 	$filter_subcategory = null;
		// }

		// if (isset($this->request->get['filter_subcategory_id'])) {
		// 	$filter_subcategory_id = $this->request->get['filter_subcategory_id'];
		// } else {
		// 	$filter_subcategory_id = null;
		// }

		// if (isset($this->request->get['sort'])) {
		// 	$sort = $this->request->get['sort'];
		// } else {
		// 	$sort = 'name';
		// }

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
		// if (isset($this->request->get['filter_brand_id'])) {
		// 	$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		// }
		// if (isset($this->request->get['filter_subcategory'])) {
		// 	$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		// }
		// if (isset($this->request->get['filter_subcategory_id'])) {
		// 	$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		// }
		// if (isset($this->request->get['sort'])) {
		// 	$url .= '&sort=' . $this->request->get['sort'];
		// }
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/orderapp/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/orderapp/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(

			'order_status' => $order_status,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$data['statuss'] = array(	
			'999' => 'Pending',
			'1' => 'Kot',
			'3' => 'Bill',
			'2' => 'Cancel',
			//'5' => 'Delivery Boy',
		);
		//echo $order_status;exit;

		$orderarray = array();
		if (!empty($order_status)) {
			if($order_status == '999'){
				$cutumer = $this->db->query("SELECT * FROM  oc_order_app WHERE `status` = '0' ")->rows;
			} else {
				$cutumer = $this->db->query("SELECT * FROM  oc_order_app WHERE `status` = '".$order_status."' ")->rows;
			}
		} else {
			$cutumer = $this->db->query("SELECT * FROM  oc_order_app WHERE `status` = '0' OR `status` = '1' OR `status` = '2' OR `status` = '3' OR `status` = '4' ")->rows;
		}
		// echo "<pre>";
		// print_r($cutumer);
		// exit;
		foreach ($cutumer as $key) {
			$itemarray = array();
			$item_info = $this->db->query("SELECT * FROM  oc_app_item  WHERE order_id = '".$key['order_id']."' LIMIT 2 ")->rows;
			//echo "<pre>";print_r($item_info);exit;
			//echo "SELECT `order_id`,`table_id`,`app_order_id` FROM  oc_order_info  WHERE app_order_id = '".$key['order_id']."'";exit;
			$order_data = $this->db->query("SELECT `order_id`,`table_id`,`app_order_id` FROM  oc_order_info  WHERE app_order_id = '".$key['order_id']."'")->row;
			//echo "<pre>";print_r($order_data);exit;
			$kot_no = "";
			$bill_no = "";
			$table_id = "";
			if (isset($order_data['order_id'])) {
				$kot = $this->db->query("SELECT `billno`,`kot_no` FROM  oc_order_items WHERE order_id = '".$order_data['order_id']."'")->row;
				 //echo "<pre>";print_r($kot);exit;
				if (!empty($kot['kot_no'])) {
					//echo "in";exit;
					$kot_no = $kot['kot_no'];
				}
				
				if (!empty($kot['billno'])) {
					$bill_no = $kot['billno'];
				}

				if (!empty($order_data['table_id'])) {
					$table_id = $order_data['table_id'];
				}
			}

			foreach($item_info as $test2){
				$itemarray[] = array(
					'total'		=> $test2['total'],
					'quantity' => $test2['quantity'],
					'item_name' => $test2['item_name']
				);
			}
			
			$orderarray[] = array(
				'order_no' => $key['order_id'],
				'cus_name' => $key['cus_name'],
				'delivery_address' => $key['delivery_address'],
				'contact' => $key['contact'],
				'kot_no'  => $kot_no,
				'bill_no'  => $bill_no,
				'table_id'  => $table_id,
				'itemarray' => $itemarray,
				 'except'        => $this->url->link('catalog/orderapp/except', 'token='. $this->session->data['token'] .'&order_id='.$key['order_id'] , true),
				'cancle'        => $this->url->link('catalog/orderapp/orderapp_cancel', 'token='. $this->session->data['token'] .'&order_id='.$key['order_id'] , true),
				'view'        => $this->url->link('catalog/orderapp/getform', 'token='. $this->session->data['token'] .'&order_id='.$key['order_id'] , true)
			);
		}
		$data['orderarray'] = $orderarray;

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
		// if (isset($this->request->get['filter_brand'])) {
		// 	$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		// }
		// if (isset($this->request->get['filter_brand_id'])) {
		// 	$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		// }
		// if (isset($this->request->get['filter_subcategory'])) {
		// 	$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		// }
		// if (isset($this->request->get['filter_subcategory_id'])) {
		// 	$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		// }
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['brand'] = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . '&sort=brand' . $url, true);
		$url = '';

		if (isset($this->request->get['order_status'])) {
			$url .= '&order_status=' . $this->request->get['order_status'];
		}
		// if (isset($this->request->get['filter_brand'])) {
		// 	$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		// }
		// if (isset($this->request->get['filter_brand_id'])) {
		// 	$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		// }
		// if (isset($this->request->get['filter_subcategory'])) {
		// 	$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		// }
		// if (isset($this->request->get['filter_subcategory_id'])) {
		// 	$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		// }
		// if (isset($this->request->get['sort'])) {
		// 	$url .= '&sort=' . $this->request->get['sort'];
		// }
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$brand_total = 1;
		$pagination = new Pagination();
		$pagination->total = $brand_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($brand_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($brand_total - $this->config->get('config_limit_admin'))) ? $brand_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $brand_total, ceil($brand_total / $this->config->get('config_limit_admin')));

		// $data['filter_brand'] = $filter_brand;
		// $data['filter_brand_id'] = $filter_brand_id;
		// $data['filter_subcategory'] = $filter_subcategory;
		// $data['filter_subcategory_id'] = $filter_subcategory_id;
		//$data['sort'] = $sort;
		$data['order'] = $order;

		// = $this->db->query("SELECT * FROM oc_app_item WHERE `order_id` = 11")->rows;
		
		//$data['tests'] = $this->db->query("SELECT oi.`cus_name`,oi.`delivery_address`,oi.`contact`,oit.`quantity`,oit.`item_name`,oit.`order_id` FROM `oc_order_app` oi LEFT JOIN `oc_app_item` oit ON (oit.`order_id` = oi.`order_id`) ORDER BY oit.`order_id` DESC")->rows;
		//$data['except'] = $this->url->link('catalog/orderapp/except', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true);
		//$data['except'] = "";
		//$data['action'] = $this->url->link('catalog/orderapp/getlist', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url, true);
		//echo $data['action'];exit;
		$data['order_status'] = $order_status;
		// echo "<pre>";
		// print_r($orderarray);
		// exit();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/orderapp_list', $data));
	}
	public function reason() {
		$this->db->query("UPDATE oc_order_app SET `status` = '2', `cancel_reson` = '" . $this->request->post['cancel_reson'] . "' WHERE order_id = '" . $this->request->get['order_id'] . "'");
	 	$this->response->redirect($this->url->link('catalog/orderapp', 'token='. $this->session->data['token']));
	}

	public function cancel() {
		$data['reason'] = $this->url->link('catalog/orderapp/reason', 'token='. $this->session->data['token'] .'&order_id='.$this->request->get['order_id'] , true);

		$data['order_id'] = $this->request->get['order_id'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/orderapp_cancel',$data));
	}

	public function getForm(){
		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		$data['success'] = '';

		$cutumer = $this->db->query("SELECT * FROM  oc_order_app WHERE order_id = '".$this->request->get['order_id']."' ")->row;
		// echo "<pre>";
		// print_r($cutumer);exit;
		$cut_detail = array(
			'order_no' => $cutumer['order_id'],	
			'cus_name' => $cutumer['cus_name'],
			'delivery_address' => $cutumer['delivery_address'],
			'contact' => $cutumer['contact'],
			'exp_delivery_time' => $cutumer['exp_delivery_time'],
			'status' => $cutumer['status'],
			// 'except'        => $this->url->link('catalog/orderapp/except', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true),
			//'cancel'        => $this->url->link('catalog/orderapp/cancle', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true),
		);

		$item_info = $this->db->query("SELECT * FROM  oc_app_item  WHERE order_id = '".$this->request->get['order_id']."' ")->rows;
		$itemarray = array();
		foreach($item_info as $test2){
			$itemarray[] = array(
				'total' => $test2['total'],
				'quantity' => $test2['quantity'],
				'item_name' => $test2['item_name']
			);
		}

		$data['cut_detail'] = $cut_detail;
		$data['item_detail'] = $itemarray;
		$data['except'] = $this->url->link('catalog/orderapp/except', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true); 
		$data['cancel'] = $this->url->link('catalog/orderapp/cancel', 'token='. $this->session->data['token'] .'&order_id='.$cutumer['order_id'], true);
		$data['back'] = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'], true);

		$data['timee'] = array(
			'5' => 5,
			'10' => 10,
			'15' => 15,
			'30' => 30, 
			'45' => 45
		);
		//echo "in";exit;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/orderapp_form',$data));
	 }

	public function except() {
		date_default_timezone_set("Asia/Kolkata");
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		if(isset($this->request->post['exp_delivery_time'])){
			//echo "in";
			$this->db->query("UPDATE oc_order_app SET `exp_delivery_time` = '".$this->request->post['exp_delivery_time']."', `status` = '1'  WHERE order_id = '" . $this->request->get['order_id'] . "'");
		}

		$between = $this->db->query("SELECT `table_to`,`table_from`, `location`, `location_id`, `rate_id` FROM oc_location WHERE is_app = 1")->row;
		$order_info = $this->db->query("SELECT *  FROM oc_order_info WHERE `t_name` BETWEEN '".$between['table_from']."' AND '".$between['table_to']."' AND `payment_status` = '0' ORDER BY t_name DESC LIMIT 1")->row;
		$app_order_cust = $this->db->query("SELECT * FROM oc_order_app WHERE order_id = '". $this->request->get['order_id'] . "'")->row;
		//$app = $this->db->query("SELECT `location`,`location_id` FROM oc_location WHERE `is_app` = 1")->row;
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}
		$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
		if($kotno2->num_rows > 0){
			$kot_no2 = $kotno2->row['kot_no'];
			$kotno = $kot_no2 + 1;
		} else{
			$kotno = 1;
		}
		$date = date("Y-m-d");
		$time = date("h:i:sa");
		//echo $date;echo $time ;exit;
		if (!empty($order_info['t_name'])) {
			$t_name = $order_info['t_name'] + 1;	
			$order_info = $this->db->query("INSERT INTO  oc_order_info SET 
				app_order_id = '".$app_order_cust['order_id']."',
				kot_no = '".$kotno."',
				cust_name = '".$app_order_cust['cus_name']."',
				cust_email = '".$app_order_cust['cus_email']."',
				cust_contact = '".$app_order_cust['contact']."',
				cust_id = '".$app_order_cust['customer_id']."',
				cust_address = '".$app_order_cust['delivery_address']."',
				t_name = '".$t_name."',
				table_id = '".$t_name."',
				location = '".$between['location']."',
				location_id = '".$between['location_id']."',
				rate_id = '".$between['rate_id']."',
				date_added = '".$date."',
				bill_date = '".$last_open_date."',
				date = '".$date."',
				time_added = '".$time."',
				time = '".$time."',
				login_id = '" . $this->db->escape($this->user->getId()) . "',
				login_name = '" . $this->db->escape($this->user->getUserName()) . "' ");
			$id = $this->db->getLastId();
		} else {
			$t_name = $between['table_from'];	
			$order_info = $this->db->query("INSERT INTO  oc_order_info SET 
				app_order_id = '".$app_order_cust['order_id']."',
				kot_no = '".$kotno."',
				cust_name = '".$app_order_cust['cus_name']."',
				cust_email = '".$app_order_cust['cus_email']."',
				cust_contact = '".$app_order_cust['contact']."',
				cust_id = '".$app_order_cust['customer_id']."',
				cust_address = '".$app_order_cust['delivery_address']."',
				t_name = '".$t_name."',
				table_id = '".$t_name."',
				location = '".$between['location']."',
				location_id = '".$between['location_id']."',
				rate_id = '".$between['rate_id']."',
				date_added = '".$date."',
				bill_date = '".$last_open_date."',
				date = '".$date."',
				time_added = '".$time."',
				time = '".$time."', 
				login_id = '" . $this->db->escape($this->user->getId()) . "',
				login_name = '" . $this->db->escape($this->user->getUserName()) . "' ");
			$id = $this->db->getLastId();						
		}
		
		$app_order_item = $this->db->query("SELECT * FROM oc_app_item WHERE order_id = '". $this->request->get['order_id'] . "'")->rows;
		foreach ($app_order_item as $key) {
			$iteminfo = $this->db->query("SELECT `is_liq`,`vat`,`item_code`, `item_sub_category_id` FROM oc_item WHERE `item_id` = '".$key['product_id'] . "'")->row;
			$tax = $this->db->query("SELECT `tax_value`  FROM oc_tax WHERE `id` = '". $iteminfo['vat'] . "'")->row;
			$tax1_value = $key['total'] * $tax['tax_value']/100;
			$order_item_info = $this->db->query("INSERT INTO  oc_order_items SET 
				order_id = '". $id . "',
				ismodifier = '1',
				kot_status = '1',
				is_new = '1',
				printstatus = '1',
				kot_no = '".$kotno."',
				name = '".$key['item_name']."',
				pre_qty = '".$key['quantity']."',
				qty = '".$key['quantity']."',
				rate = '".$key['rate']."',
				amt = '".$key['total']."',
				code = '".$iteminfo['item_code']."',
				is_liq = '".$iteminfo['is_liq']."',
				subcategoryid = '".$iteminfo['item_sub_category_id']."',
				date = '".$date."',
				time = '".$time."',
				tax1 = '".$tax['tax_value']."',
				tax1_value = '".$tax1_value."',
				login_id = '" . $this->db->escape($this->user->getId()) . "',
				login_name = '" . $this->db->escape($this->user->getUserName()) . "' ");
		}

		$ftotal = 0;
		$ltotal = 0;
		$gst = 0;
		$vat = 0;
		$t_item = 0;
		$t_quty = 0; 
		$testdata = $this->db->query("SELECT * FROM oc_order_items WHERE `order_id` ='".$id."' ")->rows;
		foreach ($testdata as $datass) {
			if($datass['is_liq'] == '0'){
				$ftotal = $ftotal + $datass['amt'];
				$gst = $gst + $datass['tax1_value'];
			}
			if($datass['is_liq'] == '1'){
				$ltotal = $ltotal + $datass['amt'];
				$vat = $vat + $datass['tax1_value'];
			}
			$t_item ++;
			$t_quty = $t_quty +  $datass['qty'];
		}
		$g_total =  $ftotal + $ltotal + $gst + $vat;
		$this->db->query("UPDATE oc_order_info  SET  ftotal = '".$ftotal."',
			ftotal = '".$ftotal."',
			gst = '".$gst."',
			ltotal = '".$ltotal."',
			vat = '".$vat."',
			grand_total =  '".$g_total."',
			item_quantity =  '".$t_quty."',
			total_items =  '".$t_item."' WHERE order_id = '".$id."'");

		$this->response->redirect($this->url->link('catalog/orderapp', 'token='. $this->session->data['token']));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['brand']) < 2) || (utf8_strlen($this->request->post['brand']) > 255)) {
			$this->error['brand'] = 'Please Enter brand';
		} else {
			if(isset($this->request->get['brand_id'])){
				$is_exist = $this->db->query("SELECT `brand_id` FROM `oc_brand` WHERE `brand` = '".$datas['brand']."' AND `brand_id` <> '".$this->request->get['brand_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['brand'] = 'Brand Name Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `brand_id` FROM `oc_brand` WHERE `brand` = '".$datas['brand']."' ");
				if($is_exist->num_rows > 0){
					$this->error['brand'] = 'Brand Name Already Exists';
				}
			}
		}


		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/brand');

		return !$this->error;
	}

	// public function upload() {
	// 	$this->load->language('catalog/brand');

	// 	$json = array();

	// 	// Check user has permission
	// 	if (!$this->user->hasPermission('modify', 'catalog/brand')) {
	// 		$json['error'] = $this->language->get('error_permission');
	// 	}

	// 	if (!$json) {
	// 		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
	// 			// Sanitize the filename
	// 			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

	// 			// Validate the filename length
	// 			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
	// 				$json['error'] = $this->language->get('error_filename');
	// 			}

	// 			// Allowed file extension types
	// 			$allowed = array();

	// 			$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

	// 			$filetypes = explode("\n", $extension_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Allowed file mime types
	// 			$allowed = array();

	// 			$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

	// 			$filetypes = explode("\n", $mime_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array($this->request->files['file']['type'], $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Check to see if any PHP files are trying to be uploaded
	// 			$content = file_get_contents($this->request->files['file']['tmp_name']);

	// 			if (preg_match('/\<\?php/i', $content)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Return any upload error
	// 			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
	// 				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
	// 			}
	// 		} else {
	// 			$json['error'] = $this->language->get('error_upload');
	// 		}
	// 	}

	// 	if (!$json) {
	// 		$file = $filename . '.' . token(32);

	// 		move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

	// 		$json['filename'] = $file;
	// 		$json['mask'] = $filename;

	// 		$json['success'] = $this->language->get('text_upload');
	// 	}

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function autocompletebname() {
		$json = array();
		if (isset($this->request->get['filter_brand'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_brand' => $this->request->get['filter_brand'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'brand'    => strip_tags(html_entity_decode($result['brand'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['brand'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletesubcategory() {
		$json = array();
		if (isset($this->request->get['filter_subcategory'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_subcategory' => $this->request->get['filter_subcategory'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'subcategory'    => strip_tags(html_entity_decode($result['subcategory'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['subcategory'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

}