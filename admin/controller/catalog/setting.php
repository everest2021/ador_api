<?php
class ControllerCatalogSetting extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/setting');
		$this->getForm();
	}

	public function add() {
		$this->load->language('catalog/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		// 	echo '<pre>';
		// print_r($this->request->post);
		// exit;
			$this->model_catalog_setting->addsetting($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/setting', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_setting->editsetting($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }
			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/setting', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/setting');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_setting->deletesetting($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/setting', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();

	}

	

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_rate'] = $this->language->get('entry_rate');
		$data['entry_capacity'] = $this->language->get('entry_capacity');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');
		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}


		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];

		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/setting', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/setting/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/setting/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/setting', 'token=' . $this->session->data['token'] . $url, true);
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$setting_info = $this->model_catalog_setting->getsetting($this->request->get);
			// echo'<pre>';
			// print_r($setting_info);
			// exit;
		}

		$startup_datas =array(
			'1' => 'Order',
			'2' => 'Order Keyboard',
			'3' => 'Order Tab',
			'4' => 'Order Touch',
			'5' => 'Order Process'
			);
		$data['startup_data'] = $startup_datas;


		$popup = array(
			'0' => 'NO',
			'1' => 'YES'
		);

		$printer_model = array(
			'0' => '48 columns',
			'1' => '45 Columns'
		);

		$data['popup'] = $popup;
		$data['printer_model'] = $printer_model;
		// echo'<pre>';
		// print_r($startup_datas);
		// exit; 

		$data['token'] = $this->session->data['token'];

		$this->load->model('catalog/order');
		
		$setting_value = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
		if (isset($this->request->post['SETTLEMENT_ON'])) {
			$data['SETTLEMENT_ON'] = $this->request->post['SETTLEMENT_ON'];
		} else {
			$data['SETTLEMENT_ON'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('INCLUSIVE');
		if (isset($this->request->post['INCLUSIVE'])) {
			$data['INCLUSIVE'] = $this->request->post['INCLUSIVE'];
		} else {
			$data['INCLUSIVE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SKIPTABLE');
		if (isset($this->request->post['SKIPTABLE'])) {
			$data['SKIPTABLE'] = $this->request->post['SKIPTABLE'];
		} else {
			$data['SKIPTABLE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SKIPCATEGORY');
		if (isset($this->request->post['SKIPCATEGORY'])) {
			$data['SKIPCATEGORY'] = $this->request->post['SKIPCATEGORY'];
		} else {
			$data['SKIPCATEGORY'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SKIPSUBCATEGORY');
		if (isset($this->request->post['SKIPSUBCATEGORY'])) {
			$data['SKIPSUBCATEGORY'] = $this->request->post['SKIPSUBCATEGORY'];
		} else {
			$data['SKIPSUBCATEGORY'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('NOCATEGORY');
		if (isset($this->request->post['NOCATEGORY'])) {
			$data['NOCATEGORY'] = $this->request->post['NOCATEGORY'];
		} else {
			$data['NOCATEGORY'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('CATEGORY');
		if (isset($this->request->post['CATEGORY'])) {
			$data['CATEGORY'] = $this->request->post['CATEGORY'];
		} else {
			$data['CATEGORY'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('PRINTER_NAME');
		if (isset($this->request->post['PRINTER_NAME'])) {
			$data['PRINTER_NAME'] = $this->request->post['PRINTER_NAME'];
		} else {
			$data['PRINTER_NAME'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('WAITER');
		if (isset($this->request->post['WAITER'])) 
		{
			$data['WAITER'] = $this->request->post['WAITER'];
		} else 
		{
			$data['WAITER'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('CAPTAIN');
		if (isset($this->request->post['CAPTAIN'])) {
			$data['CAPTAIN'] = $this->request->post['CAPTAIN'];
		} else {
			$data['CAPTAIN'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('PERSONS');
		if (isset($this->request->post['PERSONS'])) {
			$data['PERSONS'] = $this->request->post['PERSONS'];
		} else {
			$data['PERSONS'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('PERSONS_COMPULSARY');
		if (isset($this->request->post['PERSONS_COMPULSARY'])) {
			$data['PERSONS_COMPULSARY'] = $this->request->post['PERSONS_COMPULSARY'];
		} else {
			$data['PERSONS_COMPULSARY'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('LOCAL_PRINT');
		if (isset($this->request->post['LOCAL_PRINT'])) {
			$data['LOCAL_PRINT'] = $this->request->post['LOCAL_PRINT'];
		} else {
			$data['LOCAL_PRINT'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('COMPLIMENTARY_STATUS');
		if (isset($this->request->post['COMPLIMENTARY_STATUS'])) {
			$data['COMPLIMENTARY_STATUS'] = $this->request->post['COMPLIMENTARY_STATUS'];
		} else {
			$data['COMPLIMENTARY_STATUS'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SETTLEMENT');
		if (isset($this->request->post['SETTLEMENT'])) {
			$data['SETTLEMENT'] = $this->request->post['SETTLEMENT'];
		} else {
			$data['SETTLEMENT'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
		if (isset($this->request->post['KOT_RATE_AMT'])) {
			$data['KOT_RATE_AMT'] = $this->request->post['KOT_RATE_AMT'];
		} else {
			$data['KOT_RATE_AMT'] = $setting_value;
		}


		$setting_value = $this->model_catalog_order->get_settings('KOT_BILL_CANCEL_REASON');
		if (isset($this->request->post['KOT_BILL_CANCEL_REASON'])) {
			$data['KOT_BILL_CANCEL_REASON'] = $this->request->post['KOT_BILL_CANCEL_REASON'];
		} else {
			$data['KOT_BILL_CANCEL_REASON'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		if (isset($this->request->post['SERVICE_CHARGE_FOOD'])) {
			$data['SERVICE_CHARGE_FOOD'] = $this->request->post['SERVICE_CHARGE_FOOD'];
		} else {
			$data['SERVICE_CHARGE_FOOD'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		if (isset($this->request->post['SERVICE_CHARGE_LIQ'])) {
			$data['SERVICE_CHARGE_LIQ'] = $this->request->post['SERVICE_CHARGE_LIQ'];
		} else {
			$data['SERVICE_CHARGE_LIQ'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('PRINTER_TYPE');
		if (isset($this->request->post['PRINTER_TYPE'])) {
			$data['PRINTER_TYPE'] = $this->request->post['PRINTER_TYPE'];
		} else {
			$data['PRINTER_TYPE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('BARCODE_PRINTER');
		if (isset($this->request->post['BARCODE_PRINTER'])) {
			$data['BARCODE_PRINTER'] = $this->request->post['BARCODE_PRINTER'];
		} else {
			$data['BARCODE_PRINTER'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('HOTEL_NAME');
		if (isset($this->request->post['HOTEL_NAME'])) {
			$data['HOTEL_NAME'] = $this->request->post['HOTEL_NAME'];
		} else {
			$data['HOTEL_NAME'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('HOTEL_ADD');
		if (isset($this->request->post['HOTEL_ADD'])) {
			$data['HOTEL_ADD'] = $this->request->post['HOTEL_ADD'];
		} else {
			$data['HOTEL_ADD'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('GST_NO');
		if (isset($this->request->post['GST_NO'])) {
			$data['GST_NO'] = $this->request->post['GST_NO'];
		} else {
			$data['GST_NO'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('TEXT1');
		if (isset($this->request->post['TEXT1'])) {
			$data['TEXT1'] = $this->request->post['TEXT1'];
		} else {
			$data['TEXT1'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('TEXT2');
		if (isset($this->request->post['TEXT2'])) {
			$data['TEXT2'] = $this->request->post['TEXT2'];
		} else {
			$data['TEXT2'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('TEXT3');
		if (isset($this->request->post['TEXT3'])) {
			$data['TEXT3'] = $this->request->post['TEXT3'];
		} else {
			$data['TEXT3'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('BAR_NAME');
		if (isset($this->request->post['BAR_NAME'])) {
			$data['BAR_NAME'] = $this->request->post['BAR_NAME'];
		} else {
			$data['BAR_NAME'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('BAR_ADD');
		if (isset($this->request->post['BAR_ADD'])) {
			$data['BAR_ADD'] = $this->request->post['BAR_ADD'];
		} else {
			$data['BAR_ADD'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('ADVANCE_NOTE');
		if (isset($this->request->post['ADVANCE_NOTE'])) {
			$data['ADVANCE_NOTE'] = $this->request->post['ADVANCE_NOTE'];
		} else {
			$data['ADVANCE_NOTE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SMS_LINK_1');
		if (isset($this->request->post['SMS_LINK_1'])) {
			$data['SMS_LINK_1'] = $this->request->post['SMS_LINK_1'];
		} else {
			$data['SMS_LINK_1'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SMS_LINK_2');
		if (isset($this->request->post['SMS_LINK_2'])) {
			$data['SMS_LINK_2'] = $this->request->post['SMS_LINK_2'];
		} else {
			$data['SMS_LINK_2'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('SMS_LINK_3');
		if (isset($this->request->post['SMS_LINK_3'])) {
			$data['SMS_LINK_3'] = $this->request->post['SMS_LINK_3'];
		} else {
			$data['SMS_LINK_3'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
		if (isset($this->request->post['CONTACT_NUMBER'])) {
			$data['CONTACT_NUMBER'] = $this->request->post['CONTACT_NUMBER'];
		} else {
			$data['CONTACT_NUMBER'] = $setting_value;
		}



		$setting_value = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT');
		if (isset($this->request->post['EMAIL_TO_REPORT'])) {
			$data['EMAIL_TO_REPORT'] = $this->request->post['EMAIL_TO_REPORT'];
		} else {
			$data['EMAIL_TO_REPORT'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT_PASSWORD');
		if (isset($this->request->post['EMAIL_TO_REPORT_PASSWORD'])) {
			$data['EMAIL_TO_REPORT_PASSWORD'] = $this->request->post['EMAIL_TO_REPORT_PASSWORD'];
		} else {
			$data['EMAIL_TO_REPORT_PASSWORD'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('EMAIL_USERNAME');
		if (isset($this->request->post['EMAIL_USERNAME'])) {
			$data['EMAIL_USERNAME'] = $this->request->post['EMAIL_USERNAME'];
		} else {
			$data['EMAIL_USERNAME'] = $setting_value;
		}

		
		$setting_value = $this->model_catalog_order->get_settings('STARTUP_PAGE');
		if (isset($this->request->post['STARTUP_PAGE'])) {
			$data['STARTUP_PAGE'] = $this->request->post['STARTUP_PAGE'];
		} else {
			$data['STARTUP_PAGE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('DAYCLOSE_POPUP');
		if (isset($this->request->post['DAYCLOSE_POPUP'])) {
			$data['DAYCLOSE_POPUP'] = $this->request->post['DAYCLOSE_POPUP'];
		} else {
			$data['DAYCLOSE_POPUP'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('PRINTER_MODEL');
		if (isset($this->request->post['PRINTER_MODEL'])) {
			$data['PRINTER_MODEL'] = $this->request->post['PRINTER_MODEL'];
		} else {
			$data['PRINTER_MODEL'] = $setting_value;
		}
		
		$setting_value = $this->model_catalog_order->get_settings('CHECK_KOT_GRAND_TOTAL');
		if (isset($this->request->post['CHECK_KOT_GRAND_TOTAL'])) {
			$data['CHECK_KOT_GRAND_TOTAL'] = $this->request->post['CHECK_KOT_GRAND_TOTAL'];
		} else {
			$data['CHECK_KOT_GRAND_TOTAL'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('TENDERING_ENABLE');
		if (isset($this->request->post['TENDERING_ENABLE'])) {
			$data['TENDERING_ENABLE'] = $this->request->post['TENDERING_ENABLE'];
		} else {
			$data['TENDERING_ENABLE'] = $setting_value;
		}
		$setting_value = $this->model_catalog_order->get_settings('RATE_CHANGE');
		if (isset($this->request->post['RATE_CHANGE'])) 
		{
			$data['RATE_CHANGE'] = $this->request->post['RATE_CHANGE'];
		} else 
		{
			$data['RATE_CHANGE'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('VAT_SHOW');
		if (isset($this->request->post['VAT_SHOW'])) {
			$data['VAT_SHOW'] = $this->request->post['VAT_SHOW'];
		} else {
			$data['VAT_SHOW'] = $setting_value;
		}


		$setting_value = $this->model_catalog_order->get_settings('SCRG_SHOW');
		if (isset($this->request->post['SCRG_SHOW'])) {
			$data['SCRG_SHOW'] = $this->request->post['SCRG_SHOW'];
		} else {
			$data['SCRG_SHOW'] = $setting_value;
		}

		$setting_value = $this->model_catalog_order->get_settings('IS_LOGO');
		if (isset($this->request->post['IS_LOGO'])) {
			$data['IS_LOGO'] = $this->request->post['IS_LOGO'];
		} else {
			$data['IS_LOGO'] = $setting_value;
		}

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/setting', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $datas = $this->request->post;
		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
		// 	$this->error['name'] = 'Please Enter  Name';
		// }
		// if ((utf8_strlen($this->request->post['rate']) < 2) || (utf8_strlen($this->request->post['rate']) > 255)) {
		// 	$this->error['rate'] = 'Please Enter  rate';
		// }
		// if ((utf8_strlen($this->request->post['capacity']) < 2) || (utf8_strlen($this->request->post['capacity']) > 255)) {
		// 	$this->error['capacity'] = 'Please Enter  capacity';
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/setting')) {
			$this->error['warning'] = $this->language->get('error_permission');

		}
		$this->load->model('catalog/setting');
		return !$this->error;
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/setting');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20

			);
			//$results = $this->model_catalog_setting->getsettings($filter_data);
			$results = $this->db->query("SELECT * FROM settings_ador ")->rows;
			// echo'<pre>';
			// print_r($results);exit;
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'key'        => strip_tags(html_entity_decode($result['key'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['key'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

}