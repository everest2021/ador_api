<?php
class ControllerCatalogSubCategory extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/subcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/subcategory');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/subcategory');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/subcategory');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_subcategory->addSubCategory($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['parent_id'])) {
				$url .= '&parent_id=' . $this->request->get['parent_id'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/subcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/subcategory');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_subcategory->editSubCategory($this->request->get['category_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_parent'])) {
				$url .= '&filter_parent=' . $this->request->get['filter_parent'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/subcategory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/subcategory');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $category_id) {
				$this->model_catalog_subcategory->deleteSubCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_parent'])) {
			$filter_parent = $this->request->get['filter_parent'];
		} else {
			$filter_parent = null;
		}

		if (isset($this->request->get['filter_category_id'])) {
			$filter_category_id = $this->request->get['filter_category_id'];
		} else {
			$filter_category_id = null;
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_parent_id'])) {
			$filter_parent_id = $this->request->get['filter_parent_id'];
		} else {
			$filter_parent_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'category_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}
		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}
		if (isset($this->request->get['filter_parent'])) {
			$url .= '&filter_parent=' . $this->request->get['filter_parent'];
		}
		if (isset($this->request->get['filter_parent_id'])) {
			$url .= '&filter_parent_id=' . $this->request->get['filter_parent_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/subcategory/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/subcategory/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['send_timing'] = $this->url->link('catalog/stockcategory/send_timing', 'token=' . $this->session->data['token'] , true);
		
		$filter_data = array(
			'filter_parent' => $filter_parent,
			'filter_parent_id' => $filter_parent_id,
			'filter_name' => $filter_name,
			'filter_category_id' => $filter_category_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$table_total = $this->model_catalog_subcategory->getTotalSubCategory($filter_data);

		$results = $this->model_catalog_subcategory->getSubCategorys($filter_data);

		$data['tables'] = array();
		foreach ($results as $result) {
			$data['tables'][] = array(
				'category_id' => $result['category_id'],
				'parent_id'   => $result['parent_id'],
				'name'        => $result['name'],
				'parent_category'        => $result['parent_category'],
				'sort_order_api'        => $result['sort_order_api'],
				'edit'        => $this->url->link('catalog/subcategory/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, true)
			);
		 
		}

		$data['show_category_time'] = 0;
		$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Category_Timing_SEND' ORDER BY id desc Limit 1");
		if ($check_error->num_rows > 0) {
			if($check_error->row['failure'] == 3){
				$data['show_category_time'] = 1;
			}
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}
		if (isset($this->request->get['filter_parent'])) {
			$url .= '&filter_parent=' . $this->request->get['filter_parent'];
		}
		if (isset($this->request->get['filter_parent_id'])) {
			$url .= '&filter_parent_id=' . $this->request->get['filter_parent_id'];
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['name'] = $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['category_id'] = $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . '&sort=category_id' . $url, true);
		$data['sort_parent_category'] = $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . '&sort = parent_category' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}
		if (isset($this->request->get['filter_parent'])) {
			$url .= '&filter_parent=' . $this->request->get['filter_parent'];
		}
		if (isset($this->request->get['filter_parent_id'])) {
			$url .= '&filter_parent_id=' . $this->request->get['filter_parent_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $table_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($table_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($table_total - $this->config->get('config_limit_admin'))) ? $table_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $table_total, ceil($table_total / $this->config->get('config_limit_admin')));

		$data['filter_parent'] = $filter_parent;
		$data['filter_parent_id'] = $filter_parent_id;
		$data['filter_name'] = $filter_name;
		$data['filter_category_id'] = $filter_category_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/subcategory_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['days_name'] = array(
			'sunday' => 'sunday',
		    'monday' => 'monday',
		    'tuesday' => 'tuesday',
		    'wednesday' => 'wednesday',
		    'thursday' => 'thursday',
		    'friday' => 'friday',
		    'saturday' => 'saturday'
		);

		

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$category_array = $this->model_catalog_subcategory->getCategory();
		
		$data['categorys'] = array();
		foreach ($category_array as $dvalue) {
			$data['categorys'][$dvalue['category_id']]= $dvalue['category'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['category_id'])) {
			$data['action'] = $this->url->link('catalog/subcategory/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/subcategory/edit', 'token=' . $this->session->data['token'] . '&category_id=' . $this->request->get['category_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/subcategory', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		$store_info = array();
		if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_subcategory->getSubCategory($this->request->get['category_id']);
			$store_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "urbanpiper_category_time WHERE  1 = '1' AND category_id = '".$this->request->get['category_id']."' ");
            if($store_query->num_rows > 0){
                $store_info = $store_query->rows;
            } 
		}
		$data['valid'] = 0;
		if(isset($this->request->get['category_id'])){
			$data['valid'] = 1;
		}



		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['category_id'])) {
			$data['category_id'] = $this->request->get['category_id'];
		} elseif (!empty($category_info)) {
			$data['category_id'] = $category_info['category_id'];
		} 


		if (isset($this->request->post['store_all']) && !empty($this->request->post['store_all'])) {
            $data['store_all'] = $this->request->post['store_all'];
        } else if (!empty($store_info)) {
            $data['store_all'] = $store_info;
        } else {
            $data['store_all'] = array();
        }

		if (isset($this->request->get['parent_id'])) {
			$data['parent_id'] = $this->request->get['parent_id'];
		} elseif (!empty($category_info)) {
			$data['parent_id'] = $category_info['parent_id'];
		} else {
			$data['parent_id'] = '1';
		}

		if (isset($this->request->post['parent'])) {
		 	$data['parent'] = $this->request->post['parent'];
		} elseif (!empty($category_info)) {
		 	$data['parent'] = $category_info['parent_category'];
		} else {
		 	$data['parent'] = 'Root category';

		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($category_info)) {
			$data['name'] = $category_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['title_for_time'])) {
			$data['title_for_time'] = $this->request->post['title_for_time'];
		} elseif (!empty($category_info)) {
			$data['title_for_time'] = $category_info['title_for_time'];
		} else {
			$data['title_for_time'] = '';
		}

		if (isset($this->request->post['sort_order_api'])) {
			$data['sort_order_api'] = $this->request->post['sort_order_api'];
		} elseif (!empty($category_info)) {
			$data['sort_order_api'] = $category_info['sort_order_api'];
		} else {
			$data['sort_order_api'] = '';
		}

		



		if (isset($this->request->post['colour'])) {
			$data['colour'] = $this->request->post['colour'];
		} elseif (!empty($category_info)) {
			$data['colour'] = $category_info['colour'];
		} else {
			$data['colour'] = '';
		}

		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
			$data['photo_source'] = $this->request->post['photo_source'];
		} elseif (!empty($category_info)) {
			$data['photo'] = $category_info['photo'];
			$data['photo_source'] = $category_info['photo_source'];
		} else {	
			$data['photo'] = '';
			$data['photo_source'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/subcategory_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/subcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Please Enter Name';
		} else {
			if(isset($this->request->get['category_id'])){
				$is_exist = $this->db->query("SELECT `category_id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' AND `category_id` <> '".$this->request->get['category_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['name'] = 'Category Name Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `category_id` FROM `oc_subcategory` WHERE `name` = '".$datas['name']."' ");
				if($is_exist->num_rows > 0){
					$this->error['name'] = 'Category Name Already Exists';
				}
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/subcategory')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/subcategory');

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/subcategory');
		$json = array();
		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/subcategory')) {
			$json['error'] = $this->language->get('error_permission');
		}
		$file_show_path = HTTP_CATALOG.'system/storage/download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function gettiming(){
        $json = array();
        if (isset($this->request->post) && $this->request->post['day_open_store'] != '') {
            $store_sql = $this->db->query("SELECT day FROM `oc_urbanpiper_category_time` WHERE day='".$this->request->post['day_open_store']."'  AND  category_id = '".$this->request->post['category_id']."'");
            if ($store_sql->num_rows > 0) {
                $this->db->query("UPDATE `oc_urbanpiper_category_time` SET
                    day = '" . $this->request->post['day_open_store'] . "',
                    start_time = '".$this->request->post['store_intime']."',
                     category_id = '".$this->request->post['category_id']."',
                    end_time = '".$this->request->post['store_outtime']."' WHERE day ='".$store_sql->row['day']."' AND  category_id = '".$this->request->post['category_id']."'
                ");
            } else {
                $this->db->query("INSERT INTO `oc_urbanpiper_category_time` SET
                    day = '" . $this->request->post['day_open_store'] . "',
                    start_time = '".$this->request->post['store_intime']."',
                     category_id = '".$this->request->post['category_id']."',
                    end_time = '".$this->request->post['store_outtime']."'
                ");
            }

            $horse_to_tr = $this->db->query("SELECT * FROM `oc_urbanpiper_category_time` WHERE category_id = '".$this->request->post['category_id']."'");
            $html = '';
            foreach ($horse_to_tr->rows as $hkey => $hvalue) {
                $html .= '<tr >';
                    $html .= '<td class="text-center">'.$hvalue['day'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['start_time'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['end_time'].'</td>';
                    $html .= '<td class="text-center" >';
                        $html .= '<a type="button" class="btn btn-danger"  onclick="delete_store_day('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
                    $html .= '</td>';
                $html .= '</tr>';

            }
            $json['html'] = $html;
        } 
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function deleterecord(){
        $json = array();
        if (isset($this->request->get) && $this->request->get['id'] != '') {
            $this->db->query("DELETE FROM `oc_urbanpiper_category_time` WHERE id ='".$this->request->get['id']."' AND  category_id = '".$this->request->post['category_id']."'");
            $urbanpiper_store = $this->db->query("SELECT * FROM `oc_urbanpiper_category_time` WHERE category_id = '".$this->request->post['category_id']."'");
            $html = '';
            if($urbanpiper_store->num_rows > 0){
                foreach ($urbanpiper_store->rows as $hkey => $hvalue) {
                    $html .= '<tr >';
                        $html .= '<td class="text-center">'.$hvalue['day'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['start_time'].'</td>';
                        $html .= '<td class="text-center">'.$hvalue['end_time'].'</td>';
                        $html .= '<td class="text-center" >';
                            $html .= '<a type="button" class="btn btn-danger"  onclick="delete_store_day('.$hvalue['id'].')" ><i class="fa fa-minus-circle"></i></a>';
                        $html .= '</td>';
                    $html .= '</tr>';

                }
            } else {
                $html .= '<tr>';
                    $html .= '<td class="text-center" colspan="4">No Result</td>';
                $html .= '</tr>';
            }
            
            $json['html'] = $html;
        } 
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }	

	public function autocompletecname() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/subcategory');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_subcategory->getSubCategorys($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletepname() {
		$json = array();

		if (isset($this->request->get['filter_parent'])) {
			$this->load->model('catalog/subcategory');

			$filter_data = array(
				'filter_parent' => $this->request->get['filter_parent'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_subcategory->getSubCategorys($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'parent_category'        => strip_tags(html_entity_decode($result['parent_category'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['parent_category'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function send_timing() {
		$this->load->language('catalog/subcategory');
		$this->load->model('catalog/subcategory');
		$subcategory_sql = $this->db->query("SELECT `category_id`,`title_for_time` FROM `oc_subcategory` WHERE `title_for_time` != ''");

		if($subcategory_sql->num_rows > 0){
			$title_name = 'Best Time Best Food';
			if($subcategory_sql->num_rows > 0){
				$title_name = $subcategory_sql->row['title_for_time'];
			}
			$urbanpiper_category_time =  $this->db->query("SELECT * FROM `oc_urbanpiper_category_time` WHERE category_id = '".$subcategory_sql->row['category_id']."' ");
			if($urbanpiper_category_time->num_rows > 0){
				foreach ($urbanpiper_category_time as $ukey => $uvalue) {
					$day_slot[] =  array(
						'day' => $uvalue['day'],
						'slots' => array(
							'start_time'=>  $uvalue['start_time'],
							'end_time'=>  $uvalue['end_time'],
						 )
					);
				}
				$my_array = array(
					'title' => $title_name,
					'category_ref_ids' => $subcategory_sql->row['category_id'],
					'day_slots' => $day_slot
				);
			}

			$bodys = json_encode($my_array);

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array($curl, array(
		  	CURLOPT_URL => CATEGORY_TIMING_WISE,
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => '',
		 	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 0,
		  	CURLOPT_FOLLOWLOCATION => true,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => 'POST',
		  	CURLOPT_POSTFIELDS => $bodys,
			CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json',
	   			'Authorization:'.URBANPIPER_API_KEY,
			  ),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
			    $error_msg = curl_error($curl);
			    
			}
			$datas = json_decode($response,true);
			curl_close($curl);
			if (isset($error_msg)) {
				//echo "<pre>";print_r($error_msg);
			}
			//echo "<pre>";print_r($response);exit;

			if($datas['status'] == 'success'){
				$this->session->data['success'] = 'Category Timing Send Successfully!';
				$success = 1;
			} elseif($datas['status'] == 'error') {
				$this->session->data['errors'] = 'Category Timing Not Send! ' .$datas['message'];
				$error = 1;
			} else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
				$error = 1;
			} else {
				$this->session->data['errors'] = $response;
				$error = 1;
			}

			$error_plus = 0;
			$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Category_Timing_SEND' ORDER BY id desc Limit 1");
			if ($check_error->num_rows > 0) {
				if($error == 1){
					if($check_error->row['failure']){
						$error_plus = $check_error->row['failure'] + 1;
					}
				}
				$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
						`sucess`= '".$this->db->escape($success)."', 
						 `date`= NOW(),
						`failure` = '".$this->db->escape($error_plus)."'  
						WHERE `event` = 'Category_Timing_SEND' ");
			} else {
				$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
						`sucess`= '".$this->db->escape($success)."', 
						 `date`= NOW(),
						`failure` = '".$this->db->escape($error)."'  ,
						`event` = 'Category_Timing_SEND'
					");
			}
		}
	}
}