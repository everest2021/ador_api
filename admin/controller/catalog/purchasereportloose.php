<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class ControllerCatalogPurchasereportloose extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/purchasereportloose');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/purchasereportloose');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/purchasereportloose', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		}
		else{
			$data['storename'] = '';
		}

		$orderdatas  = array();
		$data['orderdatas'] = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) && isset($this->request->post['filter_storename'])){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->post['filter_storename'];

			$sql = "SELECT * FROM oc_purchase_loose_transaction WHERE `invoice_date` >= '".$start_date."' AND `invoice_date`<= '".$end_date."'";

			if($this->request->post['filter_storename'] != ''){
				$sql .= " AND store_id = '".$storecode."'";
			}

			$orders = $this->db->query($sql)->rows;
			foreach ($orders as $order) {
				$all_order_datas = $this->db->query("SELECT description, SUM(amount) as amount, SUM(qty) as qty, unit, unit_id, description_id FROM oc_purchase_loose_items_transaction WHERE p_id = '".$order['id']."' GROUP BY description_id, unit_id")->rows;
				foreach($all_order_datas as $key){
					$orderdatas[] = array(
										'description' 	=> $key['description'],
										'qty'         	=> $key['qty'],
										'unit'  		=> $key['unit'],
										'amount'      	=> $key['amount']
									);
				}
			}
		}

		$data['orderdatas'] = $orderdatas;
		$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;

		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/purchasereportloose', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/purchasereportloose', $data));
	}
}