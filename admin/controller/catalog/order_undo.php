<?php
class ControllerCatalogOrderundo extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kottransfer');
		$this->getList();
	}

	public function order_update() {
		$data['token'] = $this->session->data['token'];
		if(isset($this->request->get['orderid'])){
			$order_id = $this->request->get['orderid'];
		} else {
			$order_id = '';
		}
		

		if($order_id != ''){
			$this->db->query("UPDATE `oc_order_info` SET cancel_status = '0'  WHERE order_id = '".$order_id."'");
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$json['info'] = 1;
		$this->response->setOutput(json_encode($json));

		//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'], true));
	}

	
	public function getList() {

		$this->log->write("---------------------------Open Kot Transfer-----------------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());


		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kottransfer');
		
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		
		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		$data['display_type'] = type;
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kottransfer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		if(isset($this->request->get['order_id'])) {
			$data['printb'] = $this->url->link('catalog/kottransfer/printsb', 'token=' . $this->session->data['token'] .'&order_id=' . $this->request->get['order_id'], true);
		} else {
			$data['printb'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		}
		$data['delete'] = $this->url->link('catalog/kottransfer/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['orders'] = array();

		$data['order_undo_list'] = array();

		$order_datasss =  $this->db->query("SELECT oi.`table_id`, oi.`order_id`, oi.`grand_total`, oi.`bill_date`, oi.`time`,oi.`total_items`,oi.`item_quantity` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit  on(oi.`order_id` =  oit.`order_id`) WHERE oi.`cancel_status` = '1' AND oit.`cancelstatus` = '0' GROUP BY oit.`order_id` ");
		if($order_datasss->num_rows > 0){
			$data['order_undo_list'] = $order_datasss->rows;
		}

		$running_tbldtatas =  $this->db->query("SELECT oi.`table_id` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit  on(oi.`order_id` =  oit.`order_id`) WHERE oi.`cancel_status` = '0' AND oit.`cancelstatus` = '0' AND payment_status = '0' GROUP BY oit.`order_id` ");
		if($running_tbldtatas->num_rows > 0){
			
			foreach ($running_tbldtatas->rows as $rkey => $rvalue) {
				foreach ($data['order_undo_list'] as $okey => $ovalue) {
					if($rvalue['table_id'] == $ovalue['table_id']){
						$data['order_undo_list'][$okey]['tbl_status'] = 1;
					} else {
						$data['order_undo_list'][$okey]['tbl_status'] = 0;
					}
				}

			 }
		}
		
		// echo'<pre>';
		// print_r($data['order_undo_list']);
		// exit;
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		// if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/kottransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		// } 

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/order_undo_form', $data));

	}

	
}