<?php
class ControllerCatalogKitchenDisplay extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/kitchen_display');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/kitchen_display');
		$this->getList();
	}
	//SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

	public function query($sql,$conn) {
		$query = $conn->query($sql);

		if (!$conn->errno) {
			if ($query instanceof \mysqli_result) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new \stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				$query->close();

				return $result;
			} else {
				return true;
			}
		} else {
			throw new \Exception('Error: ' . $conn->error  . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		}
	}

	public function add() {
		$this->load->language('catalog/orderprocess');
		$this->document->setTitle($this->language->get('heading_title'));
		if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateForm()) {
			date_default_timezone_set("Asia/Kolkata");
			$order_in_process_date = date('Y-m-d');
			$order_in_process_time = date('H:i:s');
			$order_no = $this->request->get['filter_order_no'];
			$sql_insert = "INSERT INTO `oc_orderprocess` SET `order_no` = '".$order_no."', `order_in_process_date` = '".$order_in_process_date."', `order_in_process_time` = '".$order_in_process_time."', `status` = '1' ";
			$this->db->query($sql_insert);
			$this->session->data['success'] = 'Order Added Successfully';

			$url = '';

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/orderprocess', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	public function update_status() {
		$this->load->language('catalog/kitchen_display');
		$this->document->setTitle($this->language->get('heading_title'));
		$order_id = $this->request->get['order_id'];
		$status = $this->request->get['status'];
		$db_name = $this->request->get['db_name'];
		date_default_timezone_set("Asia/Kolkata");
		if($status == 1){
			$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $db_name);
			$order_ready_date = date('Y-m-d');
			$order_ready_time = date('H:i:s');
			$sql_update = "UPDATE `oc_order_items` SET `kitchen_dis_status` = '".$status."'  WHERE `id` = '".$order_id."' ";
			$this->query($sql_update,$conn);
			$this->session->data['success'] = 'Order In Process';
		} elseif($status == 2) {
			$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $db_name);
			$order_ready_date = date('Y-m-d');
			$order_ready_time = date('H:i:s');
			$sql_update = "UPDATE `oc_order_items` SET `kitchen_dis_status` = '".$status."'  WHERE `id` = '".$order_id."' ";
			$this->query($sql_update,$conn);
			$this->session->data['success'] = 'Order Completed';
		}
		$url = '';

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->response->redirect($this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . $url, true));
	}

	
	protected function getList() {
		if (isset($this->request->get['filter_order_no'])) {
			$filter_order_no = $this->request->get['filter_order_no'];
		} else {
			$filter_order_no = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = null;
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = null;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->error['filter_order_no'])) {
			$data['error_filter_order_no'] = $this->error['filter_order_no'];
		} else {
			$data['error_filter_order_no'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . $this->request->get['filter_order_no'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/kitchen_display/add', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_order_no' => $filter_order_no,
			'sort' => $sort,
			'order' => $order,
		);
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$final_data = array();
		$total_itemz = 0;
		$pending_itemz = 0;
		$kitchen_dbs = $this->db->query("SELECT * FROM oc_kitchen_db " )->rows;

		foreach ($kitchen_dbs as $kitchen_db) {
			

			$inprocess_datas = array();
			$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $kitchen_db['database_name']);

			$sport_total = 0;
			$data['summary_datas'] = array();
			$summary_datas = array();
			$kot_datas = $this->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON(oi.`order_id` = oit.`order_id`) WHERE oit.`bill_date` = '".date('Y-m-d')."'  AND oit.`kot_status` = '1' AND oit.`cancelstatus` = '0' AND oit.`cancel_bill` = '0' AND kitchen_display = '1' AND (`kitchen_dis_status` = '0' OR `kitchen_dis_status` = '1') ", $conn);
			// echo'<pre>';
			// print_r($kot_datas);
			$inprocess_items = $this->query("SELECT count(*) as pending_items FROM `oc_order_items` WHERE `bill_date` = '".date('Y-m-d')."' AND `kot_status` = '1' AND `cancelstatus` = '0' AND `cancel_bill` = '0' AND `kitchen_dis_status` = '0' AND kitchen_display = '1' ",$conn)->row;
			$total_items = $this->query("SELECT count(*) as total_item FROM `oc_order_items` WHERE `bill_date` = '".date('Y-m-d')."'  AND `kot_status` = '1' AND `cancelstatus` = '0' AND `cancel_bill` = '0' AND (`kitchen_dis_status` = '0' OR `kitchen_dis_status` = '1') AND kitchen_display = '1' ",$conn)->row;
			
			//$inprocess_datas = array();
			if($kot_datas->num_rows > 0){
				foreach($kot_datas->rows as $ikey => $ivalue){
					$href_inprocess = $this->url->link('catalog/kitchen_display/update_status', 'token=' . $this->session->data['token'] . '&order_id=' . $ivalue['id'] . '&db_name=' . $kitchen_db['database_name'] . '&status=1' . $url, true);
					$href_complete = $this->url->link('catalog/kitchen_display/update_status', 'token=' . $this->session->data['token'] . '&order_id=' . $ivalue['id'] . '&db_name=' . $kitchen_db['database_name'] . '&status=2' . $url, true);
					$inprocess_datas[$ivalue['location']][] = array(
						'qty' => $ivalue['qty'],
						'name' => $ivalue['name'],
						'msg' => $ivalue['message'],
						'table_id' => $ivalue['table_id'],
						'kot_no' => $ivalue['kot_no'],
						'location' => $ivalue['location'],
						'href_inprocess' => $href_inprocess,
						'href_complete' => $href_complete,
						'kitchen_dis_status' => $ivalue['kitchen_dis_status'],
					);
					
				}
			}

			$total_itemz = $total_itemz + $total_items['total_item'] ;
			$pending_itemz = $pending_itemz+ $inprocess_items['pending_items'] ;

			$final_data[] = array(
				'inprocess_datas' => $inprocess_datas,
			);
			$conn->close();
		}
		//exit();
		// echo '<pre>';
		// print_r($final_data);
		// exit;
		$data['inprocess_datas'] = $final_data;
		$data['total_items'] = $total_itemz ;
		$data['pending_items'] = $pending_itemz;

		//**************************************************30% screen Item summary ***********************************//
		$final_dataz =array();
		$new_datas =array();
		$summary_datas =array();
		$summary_datass =array();
		$kitchen_dbz = $this->db->query("SELECT * FROM oc_kitchen_db " )->rows;
		
		$quantity = 0;
		$inn = 0;
		foreach ($kitchen_dbz as $kitchen_dz) {
			$summary_datass = array();
			$con = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $kitchen_dz['database_name']);
			$total_items = $this->query("SELECT *,SUM(qty) as qtyy FROM  `oc_order_items`  WHERE `bill_date` = '".date('Y-m-d')."' AND `cancelstatus` = '0' AND `cancel_bill` = '0'  AND (`kitchen_dis_status` = '0' OR `kitchen_dis_status` = '1') AND `kitchen_display` = '1' GROUP BY `code`,`message` ",$con)->rows;
			//$quantity = 0;
			//$new_datas = array();
			
			// echo '<pre>';
			// print_r($total_items);
			// exit;

			if($inn == 0){
				foreach($total_items as $jkey => $jvalue){
					if($jvalue['message'] != ''){
						$custom_key = $jvalue['code'].'_'.$jvalue['message'];
					} else {
						$custom_key = $jvalue['code'];
					}
					$new_datas[$custom_key] = array(
						'qtyy' => $jvalue['qtyy'],
						'name' => $jvalue['name'],
						'message' => $jvalue['message'],
						'code' => $jvalue['code'],
					);
				}
			} else {
				foreach($total_items as $tkey => $tvalue){
				 	if($tvalue['message'] != ''){
						$custom_key = $tvalue['code'].'_'.$tvalue['message'];
					} else {
						$custom_key = $tvalue['code'];
					}
				 	$summary_datass[$custom_key] = array(
						'qtyy' => $tvalue['qtyy'],
						'name' => $tvalue['name'],
						'message' => $tvalue['message'],
						'code' => $tvalue['code'],
					);
				}
			}
			
			foreach ($summary_datass as $zkey => $zvalue) {
				foreach ($new_datas as $skey => $svalue) {
					if($skey == $zkey){
						$quantity = $zvalue['qtyy'] + $svalue['qtyy'];
						$new_datas[$skey] = array(
							'name' =>$svalue['name'],
							'message' => $svalue['message'],
							'qtyy' => $quantity,
						);
					} else {
						$new_datas[$zkey] = array(
							'name' =>$zvalue['name'],
							'message' => $zvalue['message'],
							'qtyy' => $zvalue['qtyy'],
						);
					}
				}
			}

			// echo'<pre>';
			// print_r($new_datas);
			//exit;

			$con->close();
			$inn++;
		}
		//exit;
		$final_datz = array(
			'new_datas' => $new_datas,
		);
		// echo'<pre>';
		// print_r($final_datz);
		// exit;
		
		$data['summary_datas'] = $final_datz;
		
		$data['token'] = $this->session->data['token'];
		$data['STARTUP_PAGE'] = $this->model_catalog_order->get_settings('STARTUP_PAGE');
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_order_no'])) {
			$url .= '&filter_order_no=' . $this->request->get['filter_order_no'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        
        $data['sort_in_process'] = $this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . '&sort=1' . $url, true);
        $data['sort_ready'] = $this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . '&sort=2' . $url, true);
        $data['sort_taken'] = $this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . '&sort=3' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/kitchen_display', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));
		$data['filter_order_no'] = $filter_order_no;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kitchen_display', $data));
	}

	public function getForm() {
		$this->load->language('catalog/kitchen_display');
		$this->document->setTitle($this->language->get('heading_title_1'));
		$this->load->model('catalog/kitchen_display');

		$data['heading_title'] = $this->language->get('heading_title_1');

		
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['token'] = $this->session->data['token'];

		$inprocess_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '1' ORDER BY `id` DESC ");
		$inprocess_datas = array();
		if($inprocess_orders->num_rows > 0){
			foreach($inprocess_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/kitchen_display/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=2', true);
				$inprocess_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'href' => $href,
				);
			}
		}
		$data['inprocess_datas'] = $inprocess_datas;

		$ready_orders = $this->db->query("SELECT * FROM `oc_orderprocess` WHERE `status` = '2' ORDER BY `id` DESC ");
		$ready_datas = array();
		if($ready_orders->num_rows > 0){
			foreach($ready_orders->rows as $ikey => $ivalue){
				$href = $this->url->link('catalog/orderprocess/update_status', 'token=' . $this->session->data['token'] . '&id=' . $ivalue['id'] . '&status=3', true);
				$ready_datas[] = array(
					'order_no' => $ivalue['order_no'],
					'href' => $href,
				);
			}
		}
		$data['ready_datas'] = $ready_datas;

		$refresh_url = $this->url->link('catalog/orderprocess/getForm', 'token=' . $this->session->data['token'], true);
		$refresh_url = str_replace('&amp;', '&', $refresh_url);
		$refresh_url = str_replace('&amp;', '&', $refresh_url);
		$data['refresh_url'] = $refresh_url;


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kitchen_display_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/kitchen_display')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$datas = $this->request->get;
		if(isset($this->request->get['filter_order_no'])){
			$is_exist = $this->db->query("SELECT `id` FROM `oc_orderprocess` WHERE `order_no` = '".$datas['filter_order_no']."' AND `status` = '1' ");
			if($is_exist->num_rows > 0){
				$this->error['filter_order_no'] = 'Order Number Already Exists';
			}
		}
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/kitchen_display')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_c_name'])) {
			$this->load->model('catalog/kitchen_display');

			$filter_data = array(
				'filter_c_name' => $this->request->get['filter_c_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_orderprocess->getOrderprocesss($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'orderprocess_id' => $result['orderprocess_id'],
					'c_name'        => strip_tags(html_entity_decode($result['c_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['c_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}