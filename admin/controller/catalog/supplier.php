<?php
class ControllerCatalogSupplier extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/supplier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/supplier');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/supplier');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/supplier');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_supplier->addWaiter($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_supplier_id'])) {
				$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
			}

			if (isset($this->request->get['filter_city'])) {
				$url .= '&filter_city=' . $this->request->get['filter_city'];
			}

			if (isset($this->request->get['filter_supplier'])) {
				$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/supplier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/supplier');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_supplier->editWaiter($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			
			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_supplier_id'])) {
				$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
			}

			if (isset($this->request->get['filter_city'])) {
				$url .= '&filter_city=' . $this->request->get['filter_city'];
			}

			if (isset($this->request->get['filter_supplier'])) {
				$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/supplier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/supplier');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_supplier->deleteWaiter($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_supplier_id'])) {
				$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
			}

			if (isset($this->request->get['filter_city'])) {
				$url .= '&filter_city=' . $this->request->get['filter_city'];
			}

			if (isset($this->request->get['filter_supplier'])) {
				$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = null;
		}

		if (isset($this->request->get['filter_supplier'])) {
			$filter_supplier = $this->request->get['filter_supplier'];
		} else {
			$filter_supplier = null;
		}

		if (isset($this->request->get['filter_city'])) {
			$filter_city = $this->request->get['filter_city'];
		} else {
			$filter_city = null;
		}

		if (isset($this->request->get['filter_supplier_id'])) {
			$filter_supplier_id = $this->request->get['filter_supplier_id'];
		} else {
			$filter_supplier_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_supplier_id'])) {
			$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
		}

		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/supplier/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/supplier/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_code' => $filter_code,
			'filter_supplier' => $filter_supplier,
			'filter_city' => $filter_city,
			'filter_supplier_id' => $filter_supplier_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_supplier->getTotalWaiter();

		$results = $this->model_catalog_supplier->getWaiters($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$data['waiters'][] = array(
				'filter_code' => $result['code'],
				'filter_supplier'        => $result['supplier'],
				'filter_city'        => $result['city'],
				'filter_supplier_id'  => $result['supplier_id'],
				'edit'        => $this->url->link('catalog/supplier/edit', 'token=' . $this->session->data['token'] . '&supplier_id=' . $result['supplier_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		if (isset($this->request->get['filter_supplier_id'])) {
			$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
        $data['sort_code'] = $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . '&sort=code' . $url, true);
        $data['sort_supplier'] = $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . '&sort=supplier' . $url, true);
        $data['sort_city'] = $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . '&sort=city' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}
		
		if (isset($this->request->get['filter_supplier_id'])) {
			$url .= '&filter_supplier_id=' . $this->request->get['filter_supplier_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/store_name', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_code'] = $filter_code;
		$data['filter_supplier'] = $filter_supplier;
		$data['filter_city'] = $filter_city;
		$data['filter_supplier_id'] = $filter_supplier_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/supplier_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_name'] = $this->error['item_name'];
		} else {
			$data['error_name'] = array();
		}



		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = array();
		}

		

		$url = '';

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['supplier_id'])) {
			$data['action'] = $this->url->link('catalog/supplier/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/supplier/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['supplier_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/supplier', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['supplier_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$waiter_info = $this->model_catalog_supplier->getWaiter($this->request->get['supplier_id']);
		}
		// echo "<pre>";
		// print_r($waiter_info);
		// exit;

		$data['token'] = $this->session->data['token'];

		$i_code = $this->db->query(" SELECT code FROM oc_supplier ORDER BY code DESC limit 0,1 ");

		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }

		if (isset($this->request->get['code'])) {
			$data['code'] = $this->request->get['code'];
		} elseif (!empty($waiter_info)) {
			$data['code'] = $waiter_info['code'];
		} else {
			$data['code'] = $new_code;
		}

		if (isset($this->request->get['supplier'])) {
			$data['supplier'] = $this->request->get['supplier'];
		} elseif (!empty($waiter_info)) {
			$data['supplier'] = $waiter_info['supplier'];
		} else {
			$data['supplier'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($waiter_info)) {
			$data['address'] = $waiter_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} elseif (!empty($waiter_info)) {
			$data['city'] = $waiter_info['city'];
		} else {
			$data['city'] = '';
		}

		if (isset($this->request->post['vat_no'])) {
			$data['vat_no'] = $this->request->post['vat_no'];
		} elseif (!empty($waiter_info)) {
			$data['vat_no'] = $waiter_info['vat_no'];
		} else {
			$data['vat_no'] = '';
		}

		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($waiter_info)) {
			$data['gst_no'] = $waiter_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}

		if (isset($this->request->post['pinno'])) {
			$data['pinno'] = $this->request->post['pinno'];
		} elseif (!empty($waiter_info)) {
			$data['pinno'] = $waiter_info['pinno'];
		} else {
			$data['pinno'] = '';
		}

		if (isset($this->request->post['contactno'])) {
			$data['contactno'] = $this->request->post['contactno'];
		} elseif (!empty($waiter_info)) {
			$data['contactno'] = $waiter_info['contactno'];
		} else {
			$data['contactno'] = '';
		}

		if (isset($this->request->post['mobileno'])) {
			$data['mobileno'] = $this->request->post['mobileno'];
		} elseif (!empty($waiter_info)) {
			$data['mobileno'] = $waiter_info['mobileno'];
		} else {
			$data['mobileno'] = '';
		}


		// $data['item_types']  = array('Finish' => 'Finish',
		// 'Semi Finish' => 'Semi Finish',
		// 'Raw'  => 'Raw' );

		// if (isset($this->request->post['item_type'])) {
		// 	$data['item_type'] = $this->request->post['item_type'];
		// } elseif (!empty($waiter_info)) {
		// 	$data['item_type'] = $waiter_info['item_type'];
		// } else {
		// 	$data['item_type'] = '';
		// }

		// echo "<pre>";
		// print_r( $item_category );
		// exit();

		// echo "<pre>";
		// print_r($stockcategory_datas);
		// exit;
		

	

		
	

		

		

		

		
		
		// echo "<pre>";
		// print_r($store_names);
		// exit;


		

		

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/supplier_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/store_name')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['supplier']) < 2) || (utf8_strlen($this->request->post['supplier']) > 255)) {
			$this->error['supplier'] = 'Enter valid Item Name';
		} 

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/waiter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_supplier'])) {
			$this->load->model('catalog/supplier');

			$filter_data = array(
				'filter_supplier' => $this->request->get['filter_supplier'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_supplier->getWaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'supplier_id' => $result['supplier_id'],
					'supplier'        => strip_tags(html_entity_decode($result['supplier'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['supplier'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}