<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogcurrentstockreportlsb extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/currentstockreport_lsb');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/currentstockreport_lsb');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/currentstockreport_lsb', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['type'])){
			$data['type'] = $this->request->post['type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->post['store'])){
			$data['store'] = $this->request->post['store'];
		}
		else{
			$data['store'] = '';
		}

		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();
		$brandsize = array();
		$data_array = array();
		$loose = array();

		if(isset($this->request->post['type'] )  && $this->request->post['type'] != '' && isset($this->request->post['store']) && $this->request->post['store'] != '' ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$data_array = array();
			$datacash = array();
			$datacredit = array();
			$dataonac = array();
			$datamealpass = array();
			$datapass = array();
			$loose = array();
			$data['loosezz'] = array();
			$sql1 = "SELECT * FROM `oc_brand_type` WHERE loose = '0' ";
			if($data['type'] != ''){
				$sql1 .= "AND type_id = '".$data['type']."'";
			}
			$brandsizess = $this->db->query($sql1)->rows;

			foreach ($brandsizess as $tkey => $svalue) {
				$brandsize[] = array(
					'brand_size' => $svalue['size']
				);
			}

			$sql = "SELECT * FROM `oc_brand` WHERE 1=1 ";
			if(isset($data['type'])){
				$sql .= " AND type_id = '".$data['type']."'";
			}
			
			$brands_datas = $this->db->query($sql)->rows;
			foreach($brands_datas as $key => $value){
				$pre_qtys = 0;
				$qtys = 0;
				$loose_qty = 0;
				$total_pre_qty = 0;
				$availabe_qty = 0;
				$brand_types = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$value['type_id']."' AND loose = '0' ")->rows;
				foreach ($brand_types as $tkey => $tvalue) {
					$sale_qty = 0;
					$total_purchase = 0;
					$westage_amt = 0;
					$startdate = $start_date;
					$pre_date = date('Y-m-d', strtotime($start_date . "-1 day"));

					$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$tvalue['id']."' AND item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ORDER BY id DESC LIMIT 1");

					if($last_stock->num_rows > 0){
						$last_stock = $last_stock->row['closing_balance'];
					} else{
						$last_stock = 0;
					}

					$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."' GROUP BY unit_id, description_id");
		
					$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."'   GROUP BY unit_id, description_id");
		
					if($purchase_order->num_rows > 0){
						$total_purchase = $purchase_order->row['qty'];
					} else {
						$total_purchase = 0;
					}

					if($purchase_order_loose->num_rows > 0){
						$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
					}
					
					$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");

					$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND to_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
				

					if($stock_transfer_from->num_rows > 0){
						$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
					} elseif($stock_transfer_to->num_rows > 0) {
						$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
					}

					if($data['type'] == '3'){
						$is_liq = '0';
					} else {
						$is_liq = '1';
					}
					
					$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."' AND quantity = '".$tvalue['id']."' AND is_liq = '".$is_liq."'");
					if($item_data->num_rows > 0){
						$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$data['store']."'");
						if($for_stores->num_rows > 0){
							foreach($for_stores->rows as $key){
								$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '".$is_liq."' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
								if($order_item_data->num_rows > 0){
									if($order_item_data->row['code'] != ''){
										$sale_qty = $order_item_data->row['sale_qty'];
									} else{
										$sale_qty = 0;
									}
								}
							}
						} else{
							$sale_qty = 0;
						}
					} else{
						$sale_qty = 0;
					}
					

					$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
					if($westage_datas->num_rows > 0){
						$westage_amt = $westage_datas->row['qty'];
					}

		
					if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
						$closing_balance = $total_purchase + $westage_amt - $sale_qty ;
					} else{
						$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty ;
					}

					$data_array[$value['brand']][] = array(
						'brand_size' => $tvalue['size'],
						'qty' => $closing_balance,
						
					);
				}

				//----------------------------loose avalabe qty------------------------------------------------------------------------------//

				$last_stock = $this->db->query("SELECT closing_balance,purchase_size,loose_status FROM purchase_test WHERE  item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ")->rows;
				
				foreach ($last_stock as $pkey => $pvalue) {
					$total_pre_qty = $total_pre_qty + $pvalue['loose_status'];
				}

				$purchase_datass = $this->db->query("SELECT * FROM `oc_purchase_loose_items_transaction` plit LEFT JOIN `oc_purchase_loose_transaction` plt ON (plit.`p_id` = plt.`id`)  WHERE  plit.type = '".$value['type_id']."' AND plit.description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."'  ")->rows;
				foreach ($purchase_datass as $pkey => $pvalue) {
					if (strpos($pvalue['unit'], 'ML') !== false) {
						$qtysss = str_replace("ML","",$pvalue['unit']);
					} elseif (strpos($pvalue['unit'], 'Ltr') !== false) {
						$qtydd = str_replace("Ltr","",$pvalue['unit']);
						$qtysss = $qtydd * 1000;
					}
					$loose_qty = $qtysss * $pvalue['qty'];
					$qtys = $qtys + $loose_qty;
				}

				$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."'  AND is_liq = '".$is_liq."'");
				$sale_qty = 0;
				$brand_qtys_sale = 0;
				foreach($item_data->rows as $key){
					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '".$is_liq."' AND oi.`bill_date` = '".$startdate."' AND code = '".$key['item_code']."'  GROUP BY code");
					
					if($order_item_data->num_rows > 0){
						if($order_item_data->row['code'] != ''){
							$brand_size = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$value['type_id']."' AND loose = '1' ");
							if($brand_size->num_rows > 0){
								if (strpos($brand_size->row['size'], 'ML') !== false) {
									$brand_qtys = (int)$brand_size->row['size'];
									$brand_qtys_sale = $brand_qtys * $order_item_data->row['sale_qty'];
								} 
							}
							$sale_qty =  $sale_qty + $brand_qtys_sale;
						} else{
							$sale_qty = 0;
						}
					}
				}

				$availabe_qty = $total_pre_qty + $qtys -  $sale_qty;
				$loose[$value['brand']] = $availabe_qty;
			} 

		}
		
		$data['loosezz'] = $loose;
		$data['brandsizezz'] = $brandsize;
		
		$data['finaldatas'] = $data_array;

		$data['token'] = $this->session->data['token'];
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		$data['typess'] = $this->db->query("SELECT * FROM oc_brand GROUP by type_id")->rows;


		$data['action'] = $this->url->link('catalog/currentstockreport_lsb', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/currentstockreport_lsb', $data));
	}


	public function excelprint(){

		$this->load->model('catalog/order');
		$this->load->language('catalog/currentstockreport_lsb');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/currentstockreport_lsb', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['type'])){
			$data['type'] = $this->request->get['type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->get['store'])){
			$data['store'] = $this->request->get['store'];
		}
		else{
			$data['store'] = '';
		}

		$datacash = array();
		$datacredit = array();
		$data['datacashs'] = array();
		$data['datacredits'] = array();
		$hotellists = array();
		$final_data = array();
		$data['finaldatas'] = array();
		$brandsize = array();
		$data_array = array();
		$loose = array();

		if(isset($this->request->get['type'] )  && $this->request->get['type'] != '' && isset($this->request->get['store']) && $this->request->get['store'] != '' ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$data_array = array();
			$datacash = array();
			$datacredit = array();
			$dataonac = array();
			$datamealpass = array();
			$datapass = array();
			$loose = array();
			$data['loosezz'] = array();
			$sql1 = "SELECT * FROM `oc_brand_type` WHERE loose = '0' ";
			if($data['type'] != ''){
				$sql1 .= "AND type_id = '".$data['type']."'";
			}
			$brandsizess = $this->db->query($sql1)->rows;

			foreach ($brandsizess as $tkey => $svalue) {
				$brandsize[] = array(
					'brand_size' => $svalue['size']
				);
			}

			$sql = "SELECT * FROM `oc_brand` WHERE 1=1 ";
			if(isset($data['type'])){
				$sql .= " AND type_id = '".$data['type']."'";
			}
			
			$brands_datas = $this->db->query($sql)->rows;
			foreach($brands_datas as $key => $value){
				$pre_qtys = 0;
				$qtys = 0;
				$loose_qty = 0;
				$total_pre_qty = 0;
				$availabe_qty = 0;
				$brand_types = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$value['type_id']."' AND loose = '0' ")->rows;
				foreach ($brand_types as $tkey => $tvalue) {
					$sale_qty = 0;
					$total_purchase = 0;
					$westage_amt = 0;
					$startdate = $start_date;
					$pre_date = date('Y-m-d', strtotime($start_date . "-1 day"));

					$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$tvalue['id']."' AND item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ORDER BY id DESC LIMIT 1");

					if($last_stock->num_rows > 0){
						$last_stock = $last_stock->row['closing_balance'];
					} else{
						$last_stock = 0;
					}

					$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."' GROUP BY unit_id, description_id");
		
					$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."'   GROUP BY unit_id, description_id");
		
					if($purchase_order->num_rows > 0){
						$total_purchase = $purchase_order->row['qty'];
					} else {
						$total_purchase = 0;
					}

					if($purchase_order_loose->num_rows > 0){
						$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
					}
					
					$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");

					$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND to_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
				

					if($stock_transfer_from->num_rows > 0){
						$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
					} elseif($stock_transfer_to->num_rows > 0) {
						$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
					}

					if($data['type'] == '3'){
						$is_liq = '0';
					} else {
						$is_liq = '1';
					}
					
					$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."' AND quantity = '".$tvalue['id']."' AND is_liq = '".$is_liq."'");
					if($item_data->num_rows > 0){
						$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$data['store']."'");
						if($for_stores->num_rows > 0){
							foreach($for_stores->rows as $key){
								$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '".$is_liq."' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
								if($order_item_data->num_rows > 0){
									if($order_item_data->row['code'] != ''){
										$sale_qty = $order_item_data->row['sale_qty'];
									} else{
										$sale_qty = 0;
									}
								}
							}
						} else{
							$sale_qty = 0;
						}
					} else{
						$sale_qty = 0;
					}
					

					$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
					if($westage_datas->num_rows > 0){
						$westage_amt = $westage_datas->row['qty'];
					}

		
					if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
						$closing_balance = $total_purchase + $westage_amt - $sale_qty ;
					} else{
						$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty ;
					}

					$data_array[$value['brand']][] = array(
						'brand_size' => $tvalue['size'],
						'qty' => $closing_balance,
						
					);
				}

				//----------------------------loose avalabe qty------------------------------------------------------------------------------//

				$last_stock = $this->db->query("SELECT closing_balance,purchase_size,loose_status FROM purchase_test WHERE  item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ")->rows;
				
				foreach ($last_stock as $pkey => $pvalue) {
					$total_pre_qty = $total_pre_qty + $pvalue['loose_status'];
				}

				$purchase_datass = $this->db->query("SELECT * FROM `oc_purchase_loose_items_transaction` plit LEFT JOIN `oc_purchase_loose_transaction` plt ON (plit.`p_id` = plt.`id`)  WHERE  plit.type = '".$value['type_id']."' AND plit.description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."'  ")->rows;
				foreach ($purchase_datass as $pkey => $pvalue) {
					if (strpos($pvalue['unit'], 'ML') !== false) {
						$qtysss = str_replace("ML","",$pvalue['unit']);
					} elseif (strpos($pvalue['unit'], 'Ltr') !== false) {
						$qtydd = str_replace("Ltr","",$pvalue['unit']);
						$qtysss = $qtydd * 1000;
					}
					$loose_qty = $qtysss * $pvalue['qty'];
					$qtys = $qtys + $loose_qty;
				}

				$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."'  AND is_liq = '".$is_liq."'");
				$sale_qty = 0;
				$brand_qtys_sale = 0;
				foreach($item_data->rows as $key){
					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '".$is_liq."' AND oi.`bill_date` = '".$startdate."' AND code = '".$key['item_code']."'  GROUP BY code");
					
					if($order_item_data->num_rows > 0){
						if($order_item_data->row['code'] != ''){
							$brand_size = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$value['type_id']."' AND loose = '1' ");
							if($brand_size->num_rows > 0){
								if (strpos($brand_size->row['size'], 'ML') !== false) {
									$brand_qtys = (int)$brand_size->row['size'];
									$brand_qtys_sale = $brand_qtys * $order_item_data->row['sale_qty'];
								} 
							}
							$sale_qty =  $sale_qty + $brand_qtys_sale;
						} else{
							$sale_qty = 0;
						}
					}
				}

				$availabe_qty = $total_pre_qty + $qtys -  $sale_qty;
				$loose[$value['brand']] = $availabe_qty;
			} 

		}
		
		$data['loosezz'] = $loose;
		$data['brandsizezz'] = $brandsize;
		
		$data['finaldatas'] = $data_array;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];


		$html = $this->load->view('sale/currentstockreportlsb_report_html', $data);
		//$html = $this->load->view('catalog/captain_and_waiter_html.tpl', $data); // exit;
	 	$filename = "Current Stock LBS Report";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;		

	}


	public function prints(){
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$finaldatas = array();

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['type'])){
			$data['type'] = $this->request->get['type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->get['store'])){
			$data['store'] = $this->request->get['store'];
		}
		else{
			$data['store'] = '';
		}


		if(isset($this->request->get['type'] )  && $this->request->get['type'] != '' && isset($this->request->get['store']) && $this->request->get['store'] != '' ){

			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			$data_array = array();
			$datacash = array();
			$datacredit = array();
			$dataonac = array();
			$datamealpass = array();
			$datapass = array();
			$loose = array();
			$data['loosezz'] = array();
			$sql1 = "SELECT * FROM `oc_brand_type` WHERE loose = '0' ";
			if($data['type'] != ''){
				$sql1 .= "AND type_id = '".$data['type']."'";
			}
			$brandsizess = $this->db->query($sql1)->rows;

			foreach ($brandsizess as $tkey => $svalue) {
				$brandsize[] = array(
					'brand_size' => $svalue['size']
				);
			}

			$sql = "SELECT * FROM `oc_brand` WHERE 1=1 ";
			if(isset($data['type'])){
				$sql .= " AND type_id = '".$data['type']."'";
			}
			
			$brands_datas = $this->db->query($sql)->rows;
			foreach($brands_datas as $key => $value){
				$pre_qtys = 0;
				$qtys = 0;
				$loose_qty = 0;
				$total_pre_qty = 0;
				$availabe_qty = 0;
				$brand_types = $this->db->query("SELECT * FROM `oc_brand_type` WHERE type_id = '".$value['type_id']."' AND loose = '0' ")->rows;
				foreach ($brand_types as $tkey => $tvalue) {
					$sale_qty = 0;
					$total_purchase = 0;
					$westage_amt = 0;
					$startdate = $start_date;
					$pre_date = date('Y-m-d', strtotime($start_date . "-1 day"));

					$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$tvalue['id']."' AND item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ORDER BY id DESC LIMIT 1");

					if($last_stock->num_rows > 0){
						$last_stock = $last_stock->row['closing_balance'];
					} else{
						$last_stock = 0;
					}

					$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."' GROUP BY unit_id, description_id");
		
					$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND store_id = '".$data['store']."'   GROUP BY unit_id, description_id");
		
					if($purchase_order->num_rows > 0){
						$total_purchase = $purchase_order->row['qty'];
					} else {
						$total_purchase = 0;
					}

					if($purchase_order_loose->num_rows > 0){
						$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
					}
					
					$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");

					$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND to_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
				

					if($stock_transfer_from->num_rows > 0){
						$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
					} elseif($stock_transfer_to->num_rows > 0) {
						$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
					}
					
					$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."' AND quantity = '".$tvalue['id']."' AND is_liq = '1'");
					if($item_data->num_rows > 0){
						$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$data['store']."'");
						if($for_stores->num_rows > 0){
							foreach($for_stores->rows as $key){
								$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
								if($order_item_data->num_rows > 0){
									if($order_item_data->row['code'] != ''){
										$sale_qty = $order_item_data->row['sale_qty'];
									} else{
										$sale_qty = 0;
									}
								}
							}
						} else{
							$sale_qty = 0;
						}
					} else{
						$sale_qty = 0;
					}
					

					$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$value['brand_id']."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$tvalue['id']."' AND category = '".$value['type']."' AND from_store_id = '".$data['store']."' GROUP BY unit_id, description_id");
					if($westage_datas->num_rows > 0){
						$westage_amt = $westage_datas->row['qty'];
					}

		
					if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
						$closing_balance = $total_purchase + $westage_amt - $sale_qty ;
					} else{
						$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty ;
					}

					$data_array[$value['brand']][] = array(
						'brand_size' => $tvalue['size'],
						'qty' => $closing_balance,
						
					);
				}

				//----------------------------loose avalabe qty------------------------------------------------------------------------------//

				$last_stock = $this->db->query("SELECT closing_balance,purchase_size,loose_status FROM purchase_test WHERE  item_code = '".$value['brand_id']."' AND store_id = '".$data['store']."' AND invoice_date = '".$pre_date."' ")->rows;
				
				foreach ($last_stock as $pkey => $pvalue) {
					$total_pre_qty = $total_pre_qty + $pvalue['loose_status'];
				}

				$purchase_datass = $this->db->query("SELECT * FROM `oc_purchase_loose_items_transaction` plit LEFT JOIN `oc_purchase_loose_transaction` plt ON (plit.`p_id` = plt.`id`)  WHERE  plit.type = '".$value['type_id']."' AND plit.description_id = '".$value['brand_id']."' AND invoice_date = '".$startdate."'  ")->rows;
				foreach ($purchase_datass as $pkey => $pvalue) {
					if (strpos($pvalue['unit'], 'ML') !== false) {
						$qtysss = str_replace("ML","",$pvalue['unit']);
					} elseif (strpos($pvalue['unit'], 'Ltr') !== false) {
						$qtydd = str_replace("Ltr","",$pvalue['unit']);
						$qtysss = $qtydd * 1000;
					}
					$loose_qty = $qtysss * $pvalue['qty'];
					$qtys = $qtys + $loose_qty;
				}

				$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$value['brand_id']."'  AND is_liq = '1'");
				$sale_qty = 0;
				$brand_qtys_sale = 0;
				foreach($item_data->rows as $key){
					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$key['item_code']."'  GROUP BY code");
					
					if($order_item_data->num_rows > 0){
						if($order_item_data->row['code'] != ''){
							$brand_size = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$value['type_id']."' AND loose = '1' ");
							if($brand_size->num_rows > 0){
								if (strpos($brand_size->row['size'], 'ML') !== false) {
									$brand_qtys = (int)$brand_size->row['size'];
									$brand_qtys_sale = $brand_qtys * $order_item_data->row['sale_qty'];
								} 
							}
							$sale_qty =  $sale_qty + $brand_qtys_sale;
						} else{
							$sale_qty = 0;
						}
					}
				}

				$availabe_qty = $total_pre_qty + $qtys -  $sale_qty;
				$loose[$value['brand']] = $availabe_qty;
			} 

		
			$total = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}

			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),35)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Current Stock LBS Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	//$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			    foreach($brandsize as $key => $value){
				  	$printer->text(str_pad("DATE",11)."T.BILL");
				  	$printer->feed(1);
				  	$printer->text("------------------------------------------------");
				  	
				}
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}		
	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}