<?php

class ControllerCatalogTurfBooking extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/turfbooking');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/orderapp');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/turfbooking');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/orderapp');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		     print_r($this->request->post);
		    exit;*/

			$this->model_catalog_orderapp->addBrand($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/turfbooking');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_brand->editBrand($this->request->get['brand_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_subcategory_id'])) {
				$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/turfbooking');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/brand');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $brand_id) {
				$this->model_catalog_brand->deleteBrand($brand_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_brand'])) {
				$url .= '&filter_brand=' . $this->request->get['filter_brand'];
			}

			if (isset($this->request->get['filter_brand_id'])) {
				$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}


	protected function getList() {

		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} else {
			$filter_brand = null;
		}

		if (isset($this->request->get['booking_status'])) {
			$booking_status = $this->request->get['booking_status'];
		} else {
			$booking_status = 0;
		}
		// echo $booking_status;
		// exit();
		if (isset($this->request->get['filter_brand_id'])) {
			$filter_brand_id = $this->request->get['filter_brand_id'];
		} else {
			$filter_brand_id = null;
		}

		if (isset($this->request->get['filter_subcategory'])) {
			$filter_subcategory = $this->request->get['filter_subcategory'];
		} else {
			$filter_subcategory = null;
		}

		if (isset($this->request->get['filter_subcategory_id'])) {
			$filter_subcategory_id = $this->request->get['filter_subcategory_id'];
		} else {
			$filter_subcategory_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['booking_status'])) {
			$url .= '&booking_status=' . $this->request->get['booking_status'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/orderapp/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/orderapp/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'booking_status' => $booking_status,
			'filter_brand' => $filter_brand,
			'filter_brand_id' => $filter_brand_id,
			'filter_subcategory' => $filter_subcategory,
			'filter_subcategory_id' => $filter_subcategory_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$brand_total = $this->model_catalog_orderapp->getTotalBrand($filter_data);

		$results = $this->model_catalog_orderapp->getBrands($filter_data);

		$data['tables'] = array();
		foreach ($results as $result) {
			$data['tables'][] = array(
				'brand_id' => $result['brand_id'],
				'brand'        => $result['brand'],
				'subcategory'        => $result['subcategory'],
				'edit'        => $this->url->link('catalog/orderapp/edit', 'token=' . $this->session->data['token'] . '&brand_id=' . $result['brand_id'] . $url, true)
			);
			// echo '<pre>';
		 //   print_r($data['tables']);
		 //   exit();
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['brand'] = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . '&sort=brand' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}
		if (isset($this->request->get['filter_subcategory'])) {
			$url .= '&filter_subcategory=' . $this->request->get['filter_subcategory'];
		}
		if (isset($this->request->get['filter_subcategory_id'])) {
			$url .= '&filter_subcategory_id=' . $this->request->get['filter_subcategory_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $brand_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/orderapp', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($brand_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($brand_total - $this->config->get('config_limit_admin'))) ? $brand_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $brand_total, ceil($brand_total / $this->config->get('config_limit_admin')));

		$data['filter_brand'] = $filter_brand;
		$data['filter_brand_id'] = $filter_brand_id;
		$data['filter_subcategory'] = $filter_subcategory;
		$data['filter_subcategory_id'] = $filter_subcategory_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		// = $this->db->query("SELECT * FROM oc_app_item WHERE `order_id` = 11")->rows;
		
		//$data['tests'] = $this->db->query("SELECT oi.`cus_name`,oi.`delivery_address`,oi.`contact`,oit.`quantity`,oit.`item_name`,oit.`order_id` FROM `oc_order_app` oi LEFT JOIN `oc_app_item` oit ON (oit.`order_id` = oi.`order_id`) ORDER BY oit.`order_id` DESC")->rows;
		//$data['except'] = $this->url->link('catalog/orderapp/except', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true);
		//$data['except'] = "";
		$venue_datas = $this->db->query("SELECT * FROM oc_venue ")->rows;
		$venue_data = array();
		$venue_data['0'] = 'Please Select';
		foreach ($venue_datas as $dkey => $dvalue) {
			$venue_data[$dvalue['venue_id']] = $dvalue['venue_name'];
		}
		$data['venue_data'] = $venue_data;
		
		$meal_datas = $this->db->query("SELECT * FROM oc_meal ")->rows;
		$meal_data['0'] = 'Please Select';
		foreach ($meal_datas as $dkey => $dvalue) {
			$meal_data[$dvalue['id']] = $dvalue['name'];
		}
		$data['meal_data'] = $meal_data;

		//$data['meal_types'] = $this->db->query("SELECT * FROM oc_meal ")->rows;
		$data['activitys'] = $this->db->query("SELECT * FROM oc_activity ")->rows;
		//echo "<pre>";print_r($data['activitys']);exit;



		$bookingdatas = array();
		$sql = "SELECT * FROM oc_turf WHERE 1=1";

		if($booking_status == '0'){
			$sql .= " AND booking_status = 0";
		}

		if($booking_status == '1'){
			$sql .= " AND booking_status = 1";
		}

		if($booking_status == '2'){
			$sql .= " AND booking_status = 2";
		}

		$sql .= " GROUP BY turf_id";
		//$sql .= " ORDER BY id DESC";
		//echo $sql;exit;
		$booking = $this->db->query($sql)->rows;
		//echo "<pre>";print_r($booking);exit;
		foreach ($booking as $keys => $values) {
			$timedatas = array();
			$datas = $this->db->query("SELECT * FROM oc_turf  WHERE turf_id = '".$values['turf_id']."' ")->rows;
			foreach ($datas as $key => $value) {
				$time = explode('_',$value['time']);
				$date = $time['1'].'-'.$time['2'].'-'.$value['year'];

				$timedatas[$date][] = array('time' => $time['0'] );
			}

			$customerdata = $this->db->query("SELECT name,contact FROM oc_customerinfo WHERE c_id = '".$values['user_id']."'");
			if($customerdata->num_rows > 0){
				$customername = $customerdata->row['name'];
				$customercontact = $customerdata->row['contact'];  
			} else{
				$customername = '';
				$customercontact = '';  
			}

			if ($value['booking_status'] == 1) {
				$status = "Accepted";
			}elseif ($value['booking_status'] == 2) {
				$status = "Canceled";
			}else{
				$status = "Pending";
			}
			$display = $this->db->query("SELECT * FROM oc_events WHERE event_name = '".$values['type_name']."' ")->row;
			$bookingdatas[$values['turf_id']] = array(
				'customername' => $customername,
				'customercontact' => $customercontact,
				'timedatas' => $timedatas,
				'adults' => $values['adults'],
				'turf_id' => $values['turf_id'],
				'narration' => $values['narration'],
				'kids' => $value['kids'],
				'status' => $status,
				'display' => $display,
				'except' => $this->url->link('catalog/turfbooking/except', 'token='. $this->session->data['token'] .'&id='.$values['turf_id'] , true),
				'cancle' => $this->url->link('catalog/turfbooking/cancle', 'token='. $this->session->data['token'] .'&id='.$values['turf_id'] , true)
			);

			 //echo "<pre>";print_r($bookingdatas);exit;
			 //$data = 
			 //$finaldata = array('' => , );
		}
		//echo "<pre>";print_r($bookingdatas);exit;

		//exit;


		// foreach ($booking  as $key ) {

		// 	/*$ch = curl_init('funshalla.in/hotelform1/hotel_api/meal_items_api.php?meal_id='.$key['meal_id']);
		// 	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 	$output = curl_exec($ch);
		// 	$meal_items = json_decode($output,true);*/
		// 	$meal_final_data = array();
		// 	$meal_final_data = $this->meal_items_data($key['meal_id']);

		// 	if ($key['status'] == 1) {
		// 		$status = "Accepted";
		// 	}elseif ($key['status'] == 2) {
		// 		$status = "Canceled";
		// 	}else{
		// 		$status = "Pending";
		// 	}
		// 	$customerdata = $this->db->query("SELECT name,contact FROM oc_customerinfo WHERE c_id = '".$key['user_id']."'");
		// 	if($customerdata->num_rows > 0){
		// 		$customername = $customerdata->row['name'];
		// 		$customercontact = $customerdata->row['contact'];  
		// 	} else{
		// 		$customername = '';
		// 		$customercontact = '';  
		// 	}

		// 	//echo  $key['type_name'];exit;
		// 	$display = $this->db->query("SELECT * FROM oc_events WHERE event_name = '".$key['type_name']."' ")->row;
		// 	//echo "<pre>";print_r($display);exit;
		// 	$bookingdatas[] = array(
		// 							'id'     => $key['id'],
		// 							'user_id' => $key['user_id'] ,
		// 							//'date_from' => $key['date_from'] ,
		// 							//'date_to' => $key['date_to'],
		// 							//'time_from' => $key['time_from'],
		// 							//'time_to' => $key['time_to'] ,
		// 							'adults' => $key['adults'],
		// 							'narration' => $key['narration'],
		// 							'kids' => $key['kids'],
		// 							'meal' => $key['meal'],
		// 							'meal_id' => $key['meal_id'],
		// 							'meal_items' => $meal_final_data,
		// 							'venue' => $key['venue'],
		// 							'venue_id' => $key['venue_id'],
		// 							'activity' => $key['activity'],
		// 							'activity_id' => $key['activity_id'],
		// 							'customername' => $customername,
		// 							'customercontact' => $customercontact,
		// 							'males' => $key['males'] ,
		// 							'females' => $key['females'] ,
		// 							'meal' => $key['meal'],
		// 							'type' => $key['type'] ,
		// 							'statusid' => $key['status'],
		// 							'status' => $status,
		// 							'display' => $display['display'],
		// 							'event_type' => $display['event_type'],
		// 							'except' => $this->url->link('catalog/bookingapp/except', 'token='. $this->session->data['token'] .'&id='.$key['id'] , true),
		// 							'cancle' => $this->url->link('catalog/bookingapp/cancle', 'token='. $this->session->data['token'] .'&id='.$key['id'] , true)
		// 						);
		// }

		//echo "<pre>";print_r($bookingdatas);exit;
		$data['statuss'] = array(
								'0' => 'Pending',
								'1' => 'Accept',
								'2' => 'Cancel'
							);
		$data['bookingdata'] = $bookingdatas;
		// echo "<pre>";
		// print_r($bookingdatas);
		// exit();
		$data['booking_status'] = $booking_status;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/turfbooking_list', $data));
	}

	public function meal_items_data($id){
		$meal_final_data = array();
		$combo_selecteds = $this->db->query("SELECT * FROM oc_meal_sub_main WHERE booking_id = '".$id."' GROUP BY meal_sub_main")->rows;
		foreach($combo_selecteds as $combo_selected){
			$meal_items = array();
			$meal_item_datas = $this->db->query("SELECT * FROM oc_meal_sub_main_items WHERE booking_id = '".$id."' AND sub_main_meal = '".$combo_selected['meal_sub_main']."'")->rows;
			foreach ($meal_item_datas as $value) {
				$meal_items[$combo_selected['meal_sub_main']][] = array(
					'modifier_item_id' 	=> $value['modifier_item_id'],
					'item_name' 		=> $value['item_name']
				);
			}
			$meal_final_data[] = array(
				'meal_sub_main' 		=> $combo_selected['meal_sub_main'],
				'meal_sub_main_name' 	=> $combo_selected['meal_sub_main_name'],
				'meal_items'			=> $meal_items
			);
		}
		return $meal_final_data;
	}

	public function selected_meal_items($id, $meal_id){
		$selected_meal_items = array();
		$ch = curl_init('http://funshalla.in/hotelform1/hotel_api/meal_items_api.php?meal_id='.$meal_id);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		$meal_items = json_decode($output,true);
		$combo_selecteds = $this->db->query("SELECT * FROM oc_meal_sub_main WHERE booking_id = '".$id."' GROUP BY meal_sub_main")->rows;
		foreach($combo_selecteds as $combo_selected){
			$meal_item_datas = $this->db->query("SELECT * FROM oc_meal_sub_main_items WHERE booking_id = '".$id."' AND sub_main_meal = '".$combo_selected['meal_sub_main']."'")->rows;
			foreach ($meal_item_datas as $value) {
				$selected_meal_items[] = $value['modifier_item_id'];
			}
		}
		// $html = '<table class="table table-bordered">';
		// foreach($meal_items as $key => $value){
		// 	$html .= '<tr>';
		// 		$html .= '<td>'.$value['modifier_name'].'</td>';
		// 	$html .= '</tr>';
		// 	foreach($value['modifier_items'] as $keys => $values){
		// 		$html .= '<tr>';
		// 		if(in_array($values['id'], $selected_meal_items)){	
		// 			$html .= '<td><input type="checkbox" value='.$values['id'].' name="modifieritem['.$value['modifier_id'].']['.$values['modifier_item'].']" style="display:inline;float:left" checked/>&nbsp;&nbsp;'.$values['modifier_item'].'</td>';
		// 		} else{
		// 			$html .= '<td><input type="checkbox" value='.$values['id'].' name="modifieritem['.$value['modifier_id'].']['.$values['modifier_item'].']" style="display:inline;float:left"/>&nbsp;&nbsp;'.$values['modifier_item'].'</td>';
		// 		}
		// 		$html .= '</tr>';
		// 	}
		// 	$html .= '<tr><td><span style="color: red;" id="error_meal_items_'.$value['modifier_id'].'" class="error_meal_items"></span></td></tr>';
		// }
		// $html .= '</table>';

		$html = '';
		foreach($meal_items as $key => $value){
			$html .= '<div class="dropdown" style="float:center">';
				$html .= '<button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">'.$value['modifier_name'].' - (Max Qty : '.$value['max_quantity'].')<span class="caret"></span></button>';
				$html .= '<ul class="dropdown-menu dropdown-menu-form" id="dropdown-menu role="menu" style="min-width: 247px;margin: 2px 160px 0;padding: 5px 10px;height: auto;max-height: 150px;overflow-x: hidden;">';
					foreach($value['modifier_items'] as $keys => $values){
						if(in_array($values['id'], $selected_meal_items)){	
							$html .= '<li><input type="checkbox" value='.$values['id'].' name="modifieritem['.$value['modifier_id'].']['.$values['modifier_item'].']" style="display:inline;float:left" checked/>&nbsp;&nbsp;'.$values['modifier_item'].'</li>';
						} else{
							$html .= '<li><input type="checkbox" value='.$values['id'].' name="modifieritem['.$value['modifier_id'].']['.$values['modifier_item'].']" style="display:inline;float:left"/>&nbsp;&nbsp;'.$values['modifier_item'].'</li>';
						}
					}
				$html .= '</ul>';
			$html .= '</div>';
			$html .= '<span style="color: red;" id="error_meal_items_'.$value['modifier_id'].'" class="error_meal_items"></span>';
			$html .= '<br>';
		}
		$html .= '<span style="color: red;" class="error_meal_items1"></span>';
		return $html;


		// html += '<div class="dropdown" id="dropdown_'+k+'" style="float:center">';
  //                                           				html += '<button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">'+json[i].modifier_name+' - (Max Qty : '+json[i].max_quantity+')<span class="caret"></span></button>';
  //                                           				html +='<input type="hidden" id="maxqty_'+k+'" class="maxqty" value="'+json[i].max_quantity+'">';
		// 													html += '<ul class="dropdown-menu dropdown-menu-form" id="dropdown-menu_'+k+'" role="menu" style="min-width: 247px;margin: 2px 210px 0;padding: 5px 10px;height: auto;max-height: 150px;overflow-x: hidden;">';
		// 	                                            		for(var j in json[i].modifier_items){
		//                                         					html += '<li><input type="checkbox" value="'+json[i].modifier_items[j].id+'" name="modifieritem['+json[i].modifier_id+']['+json[i].modifier_items[j].modifier_item+']" id="modifieritems_'+k+'_'+l+'" style="float:left"/> '+json[i].modifier_items[j].modifier_item+'</li>';
		// 	                                            			l++;
		// 	                                            		}
  //                                           				html += '</ul>';
	 //                                            		html += '</div>';
	 //                                            		html += '<span style="color: red;" id="error_meal_items_'+json[i].modifier_id+'" class="error_meal_items"></span>';
	 //                                            		html += '<br>';
	 //                                            		k++;
	 //                                            	}
	 //                                            	html += '<span style="color: red;" class="error_meal_items1"></span>';
	 //                                            	$('#mealitems').append(html);

	}

	public function selected_meal_items_ajax(){
		$json = array();
		if(isset($this->request->get['meal_id']) && isset($this->request->get['id'])){
			$json['html'] = $this->selected_meal_items($this->request->get['id'],$this->request->get['meal_id']);
		}
		$this->response->setOutput(json_encode($json));
	}


	public function meal() {

		$json = array();
		//if (isset($this->request->get['filter_brand'])) {
			//$this->load->model('catalog/brand');
			$data = array(
				'id' => $this->request->get['venue_id']
				
			);
			//$results = $this->model_catalog_brand->getBrands($data);
			$results = $this->db->query("SELECT * FROM oc_meal WHERE venue_id = '".$data['id']."' ")->rows;
			//echo "<pre>";print_r($results);exit;
			// echo "<pre>"; 
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				//echo  $result['id'];exit;
				$json[] = array(
					'id' => $result['id'],
					'name'    => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		//}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));

	}

	public function except() {
		//echo "in";exit;
		$this->load->language('catalog/turfbooking');
		if(isset($this->request->get['id'])){
			//echo "in";
			$this->db->query("UPDATE oc_turf SET `booking_status` = 1  WHERE turf_id = '" . $this->request->get['id'] . "'");

			$mail = $this->db->query ("SELECT * FROM oc_turf WHERE turf_id = '" . $this->request->get['id'] . "'")->row;
			$grand_total = $this->db->query ("SELECT sum(grand_total)as grand_total FROM oc_turf WHERE turf_id = '" . $this->request->get['id'] . "'")->row;

			//echo "<pre>";print_r($mail);exit;
	   		$email2 = $this->db->query("SELECT `email2`,`name` FROM `oc_customerinfo` WHERE `c_id` = '".$mail['user_id']."' ")->row;
			//echo "<pre>";print_r($email2);exit;
			$time = substr($mail['time_from'],0,5);
			$time = date("h:i:a",strtotime($time));

			$timestamp = strtotime($mail['date_from']);
   
      		$date = date('d-M-Y', $timestamp);

      		//echo $date;exit;
      		
	   		$message = 'Hello Customer ,
				Your Turf booking for the date '.$date.' is booked.
				for time  from  '.$time.'

				Number Of Adults Are '.$mail['adults'].',
				Order Total Rs '.$grand_total['grand_total'].'.
				
				In order to confirm your order please make online payment through below link within 24 hours
				http://www.funshalla.in/hotelform1/paymentform_turf.php?turf_id='.$this->request->get['id'].'
				Regards,'.'
				Funshalla';

				

			$to_email = $email2['email2'];
			$from_email = 'bookings@funshalla.com';
			$to_name = 'Funshalla';
			$subject = 'Turf Order Details';	
			
			$url='http://125.99.122.186/testprinting/accept_mail.php?message='.urlencode($message).'&to_email='.urlencode($to_email).'&from_email='.urlencode($from_email).'&to_name='.urlencode($to_name).'&subject='.urlencode($subject);
			file($url);

			
			$to_email = $email2['email2'];
			$from_email = 'bookings@funshalla.com';
			$to_name = 'Funshalla';
			$subject = 'Turf Order Details';

			$message1 = 'Dear Sir,

				You have accepted a booking for date '.$date.' is blocked.
				for time from '.$time.'
				Number Of Adults '.$mail['adults'].',
				Order Total Rs'.$grand_total['grand_total'].'.

				Regards,
				Team Funshalla';
							//echo $message1;exit;	
			
			// 		echo 	$message1;exit;
			 $url2='http://125.99.122.186/testprinting/accept_mail.php?message='.urlencode($message1).'&to_email='.urlencode($to_email).'&from_email='.urlencode($from_email).'&to_name='.urlencode($to_name).'&subject='.urlencode($subject);

			// //$url='http://125.99.122.186/testprinting/accept_mail.php?narration='.urlencode($mail['narration']).'&kid='.urlencode($mail['kids']).'&adult='.urlencode($mail['adults']).'&type='.urlencode($mail['type_name']).'&venu='.urlencode($mail['venue']).'&activity='.urlencode($mail['activity']).'&meal='.urlencode($mail['meal']).'&grandtotal='.urlencode($mail['grand_total']).'&email='.$email2['email2'].'&name='.$email2['name'].'&date='.$mail['date_from'].'&time='.$time;
			// echo $url;exit;
			 file($url2);

		}

		$this->session->data['success'] = $this->language->get('text_success');
		$this->response->redirect($this->url->link('catalog/turfbooking', 'token=' . $this->session->data['token'] , true));
	}

	public function cancle() {
		//echo "in";exit;
		$this->load->language('catalog/turfbooking');
		if(isset($this->request->get['id'])){
			//echo "in";
			$this->db->query("UPDATE oc_turf SET `booking_status` = 2  WHERE turf_id = '" . $this->request->get['id'] . "'");
		}
		$this->session->data['success'] = $this->language->get('text_cancle');
		$this->response->redirect($this->url->link('catalog/turfbooking', 'token=' . $this->session->data['token'] , true));	
	}

	public function view(){
		$json = array();
		//echo $this->request->get['id'];exit;
		if (isset($this->request->get['id'])) {
			$bookingdata = $this->db->query("SELECT * FROM oc_turf WHERE turf_id = '".$this->request->get['id']."'")->row;
			
			$customerdata = $this->db->query("SELECT name,contact FROM oc_customerinfo WHERE c_id = '".$bookingdata['user_id']."'");
			if($customerdata->num_rows > 0){
				$customername = $customerdata->row['name'];
				$customercontact = $customerdata->row['contact'];  
			} else{
				$customername = '';
				$customercontact = '';  
			}
			if($bookingdata['activity_id'] != ''){
				$activity_id = explode(',', $bookingdata['activity_id']);
			} else {
				$activity_id = array();
			}

			$category_datass = $this->db->query("SELECT * FROM oc_turf_category WHERE turf_id = '".$this->request->get['id']."'")->rows;
			//echo "<pre>";print_r($category_datass);exit;
			$html_1 = '';
			foreach ($category_datass as $key => $value) {
				 $category= $value['akey'];
				 //echo $category;exit;
				 $item = $value['avalue'];
				 //echo $item;exit;
				
				$html_1.='<div class="form-group">';
					$html_1.='<label class="col-sm-3 control-label">'.$category.'</label>';
					$html_1.='<div class="col-sm-9">';
						$html_1.='<input type="text" readonly="readonly" value = "'.$item.'" class="form-control" />';
					$html_1.='</div>';
				$html_1.='</div>';
			}


			
			$meal_datas = array();
			$meal_exist = 0;
			if($bookingdata['venue_id'] != ''){
				$meals_datass = $this->db->query("SELECT * FROM `oc_meal`");
				if($meals_datass->num_rows > 0){
					$meal_exist = 1;
					foreach($meals_datass->rows as $mkey => $mvalue){
						$meal_datas[$mvalue['id']] = $mvalue['name'];
					}
				}
			}

			if($bookingdata['type'] == 'Event' || $bookingdata['type'] == '1'){
				$is_hidden = 0;
			} else {
				$is_hidden = 1;
			}

			$selected_meal_items = $this->selected_meal_items($this->request->get['id'],$bookingdata['meal_id']);

			$timedatas = array();
			$datas = $this->db->query("SELECT * FROM oc_turf  WHERE turf_id = '".$this->request->get['id']."' ")->rows;
			foreach ($datas as $key => $value) {
				$time = explode('_',$value['time']);
				$date = $time['1'].'-'.$time['2'].'-'.$value['year'];

				$timedatas[$date][] = array('time' => $time['0'] );
			}

			//echo "<pre>";print_r($timedatas);exit;

			$html = '';
			foreach ($timedatas as $key => $value) {
						$html.='<div class="form-group">';
							$html.='<label class="col-sm-3 control-label" style="">Date</label>';
							$html.='<div class="col-sm-9" style="">';
								$html.='<input type="text" id="" readonly name="" value = "'.$key.'" class="form-control" />';
							$html.='</div>';
						$html.='</div>';
						foreach ($value as $keys => $values) {
							$html.='<div class="form-group">';
								$html.='<label class="col-sm-3 control-label" style="">Time</label>';
								$html.='<div class="col-sm-9"  style="">';
									$html.='<input type="text" id="" readonly name="" value ="'.$values['time'].'"  class="form-control" />';
								$html.='</div>';
							$html.='</div>';
						}
			}

			$html = $html;

			$json = array(
				'adults' => $bookingdata['adults'],
				'narration' => $bookingdata['narration'],
				'kids' => $bookingdata['kids'],
				'meal' => $bookingdata['meal'],
				'meal_id' => $bookingdata['meal_id'],
				'selected_meal_items' => $selected_meal_items,
				'meal_exist' => $meal_exist,
				'meal_datas' => $meal_datas,
				'venue' => $bookingdata['venue'],
				'venue_id' => $bookingdata['venue_id'],
				'activity' => $bookingdata['activity'],
				'activity_id' => $activity_id,
				'customername' => $customername,
				'customercontact' => $customercontact,
				'type' => $bookingdata['type'],
				'is_hidden' => $is_hidden,
				'html' => $html,
				'html_1' =>$html_1
				//'date_from' => date('m/d/Y',strtotime($bookingdata['date_from'])),
				//'time_from' => $bookingdata['time_from'],
				//'time_to' => $bookingdata['time_to']
			);
		}

		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function updatedata(){

		$data = array();

		if(isset($this->request->get['id'])){
			$bookingid = $this->request->get['id'];
		} else{
			$bookingid = '';
		}


		$grand_total = 0;
		if($bookingid != ''){
			$data = $this->request->post;

			if (!isset($data['venue_id'])) {
				$venue_id = '';
			}else{
				$venue_id = $data['venue_id'];
				$venue_datas=$this->db->query("SELECT * FROM `oc_venue` WHERE `venue_id` = '".$data['venue_id']."' ");
				$nvalue = $venue_datas->row;
				$grand_total = $grand_total + $nvalue['rate'] * $data['adults'];
				$grand_total = $grand_total + $nvalue['rate_child'] * $data['kids'];
			}

			if (!isset($data['venue'])) {
				$venue = '';
			}else{
				$venue = $data['venue'];
			}

			if (!isset($data['meal'])) {
				$meal = '';
			}else{
				$meal = $data['meal'];
			}

			$qty_items = 0;
			foreach($data['modifieritem'] as $key => $value){
				$check_key = $this->db->query("SELECT * FROM oc_modifier WHERE id = '".$key."'")->row;
				foreach($value as $keys => $valuess){
					$modifier_datas = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$check_key['id']."' AND id = '".$valuess."'")->rows;
					foreach($modifier_datas as $modifier_data){
						$item_price = $this->db->query("SELECT * FROM oc_item WHERE item_id = '".$modifier_data['modifier_item_id']."'")->row;
						$qty_items = $qty_items + $item_price['rate_1'];
					}
				}
			}

			if (!isset($data['meal_id'])) {
				$meal_id = '';
			}else{
				$meal_id = $data['meal_id'];
				$meal_datas=$this->db->query("SELECT * FROM `oc_meal` WHERE `id` = '".$data['meal_id']."' ");
				$nvalue = $meal_datas->row;
				$grand_total = $grand_total + ($nvalue['rate'] + $qty_items) * $data['adults'];
				$grand_total = $grand_total + ($nvalue['rate_child'] + $qty_items) * $data['kids'];
			}

			//echo "<pre>";print_r($data);exit;
			//$act_id = implode(" ", $data['activity']);
			if (isset($data['activity'])) {
				foreach ($data['activity'] as $key => $value) {
					$activity_id = implode(",", $data['activity']);
					$activity_datas = $this->db->query("SELECT * FROM `oc_activity` WHERE `id` = '".$value."' ");
					$nvalue = $activity_datas->row;
					$activity_string .= $nvalue['name'].', ';
					$grand_total = $grand_total + $nvalue['rate'];
				}
			}else{
				$activity_id = '';
				$activity_string = '';
			}

			//echo $activity_string;exit;

			// echo "<pre>";print_r($data);exit;
			$date_from = date('Y-m-d', strtotime($data['date_from']));
			$this->db->query("UPDATE oc_booking_app SET
								adults = '".$data['adults']."',
								narration = '".$data['narration']."',
								kids= '".$data['kids']."',
								type = '".$data['type']."',
								venue_id = '".$data['venue_id']."',
								venue = '".$data['venue']."',
								meal_id = '".$data['meal_id']."',
								meal = '".$data['meal']."',
								activity = '".$activity_string."',
								activity_id = '".$activity_id."',
								grand_total = '".$grand_total."',
								date_from = '".$date_from."',
								time_from = '".$data['time_from_local']."',
								time_to = '".$data['time_to_local']."'
								WHERE id='".$bookingid."'
							");
			$this->db->query("DELETE FROM oc_meal_sub_main WHERE booking_id = '".$bookingid."'");
			$this->db->query("DELETE FROM oc_meal_sub_main_items WHERE booking_id = '".$bookingid."'");
			foreach($data['modifieritem'] as $key => $value){
				$check_key = $this->db->query("SELECT * FROM oc_modifier WHERE id = '".$key."'")->row;
				$this->db->query(" INSERT INTO  `oc_meal_sub_main` SET 
								booking_id = '".$bookingid."',
								meal_id = '".$data['meal_id']."',
								date = '".$date_from."',
								time_from = '".$data['time_from_local']."',
								time_to = '".$data['time_to_local']."',
								meal_name = '".$data['meal']."',
								meal_sub_main = '".$key."',
								meal_sub_main_name = '".$check_key['modifier_name']."'
								");
				foreach($value as $keys => $valuess){
					$modifier_datas = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$check_key['id']."' AND id = '".$valuess."'")->rows;
					foreach($modifier_datas as $modifier_data){
						$item_price = $this->db->query("SELECT * FROM oc_item WHERE item_id = '".$modifier_data['modifier_item_id']."'")->row;
						$this->db->query(" INSERT INTO  `oc_meal_sub_main_items` SET 
									booking_id = '".$bookingid."',
									meal_id = '".$data['meal_id']."',
									date = '".$date_from."',
									time_from = '".$data['time_from_local']."',
									time_to = '".$data['time_to_local']."',
									sub_main_meal = '".$key."',
									modifier_item_id = '".$valuess."',
									item_id = '".$item_price['item_id']."',
									item_code = '".$item_price['item_code']."',
									item_name = '".$item_price['item_name']."',
									rate = '".$item_price['rate_1']."'
 								");
					}
				}
			}
		}
		$this->response->redirect($this->url->link('catalog/bookingapp', 'token=' . $this->session->data['token'] , true));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['subcategory_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['brand'])) {
			$data['error_brand'] = $this->error['brand'];
		} else {
			$data['error_brand'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}

		if (isset($this->request->get['filter_brand_id'])) {
			$url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$category = $this->model_catalog_brand->getCategory();
		
		 $data['categorys'] = array();
		 foreach ($category as $dvalue) {
		 	$data['categorys'][$dvalue['category_id']]= $dvalue['name'];
		}

		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['brand_id'])) {
			$data['action'] = $this->url->link('catalog/brand/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/brand/edit', 'token=' . $this->session->data['token'] . '&brand_id=' . $this->request->get['brand_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/brand', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['brand_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$brand_info = $this->model_catalog_brand->getBrand($this->request->get['brand_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['brand'])) {
			$data['brand'] = $this->request->get['brand'];
		} elseif (!empty($brand_info)) {
			$data['brand'] = $brand_info['brand'];
		} else {
			$data['brand'] = '';
		}

		if (isset($this->request->post['subcategory'])) {
			$data['subcategory'] = $this->request->post['subcategory'];
		} elseif (!empty($brand_info)) {
			$data['subcategory'] = $brand_info['subcategory'];
		} else {
			$data['subcategory'] = '';
		}

		if (isset($this->request->post['subcategory_id'])) {
			$data['subcategory_id'] = $this->request->post['subcategory_id'];
		} elseif (!empty($brand_info)) {
			$data['subcategory_id'] = $brand_info['subcategory_id'];
		} else {
			$data['subcategory_id'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($brand_info)) {
			$data['category_id'] = $brand_info['category_id'];
		} else {
			$data['category_id'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/brand_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['brand']) < 2) || (utf8_strlen($this->request->post['brand']) > 255)) {
			$this->error['brand'] = 'Please Enter brand';
		} else {
			if(isset($this->request->get['brand_id'])){
				$is_exist = $this->db->query("SELECT `brand_id` FROM `oc_brand` WHERE `brand` = '".$datas['brand']."' AND `brand_id` <> '".$this->request->get['brand_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['brand'] = 'Brand Name Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `brand_id` FROM `oc_brand` WHERE `brand` = '".$datas['brand']."' ");
				if($is_exist->num_rows > 0){
					$this->error['brand'] = 'Brand Name Already Exists';
				}
			}
		}


		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/brand');

		return !$this->error;
	}

	// public function upload() {
	// 	$this->load->language('catalog/brand');

	// 	$json = array();

	// 	// Check user has permission
	// 	if (!$this->user->hasPermission('modify', 'catalog/brand')) {
	// 		$json['error'] = $this->language->get('error_permission');
	// 	}

	// 	if (!$json) {
	// 		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
	// 			// Sanitize the filename
	// 			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

	// 			// Validate the filename length
	// 			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
	// 				$json['error'] = $this->language->get('error_filename');
	// 			}

	// 			// Allowed file extension types
	// 			$allowed = array();

	// 			$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

	// 			$filetypes = explode("\n", $extension_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Allowed file mime types
	// 			$allowed = array();

	// 			$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

	// 			$filetypes = explode("\n", $mime_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array($this->request->files['file']['type'], $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Check to see if any PHP files are trying to be uploaded
	// 			$content = file_get_contents($this->request->files['file']['tmp_name']);

	// 			if (preg_match('/\<\?php/i', $content)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Return any upload error
	// 			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
	// 				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
	// 			}
	// 		} else {
	// 			$json['error'] = $this->language->get('error_upload');
	// 		}
	// 	}

	// 	if (!$json) {
	// 		$file = $filename . '.' . token(32);

	// 		move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

	// 		$json['filename'] = $file;
	// 		$json['mask'] = $filename;

	// 		$json['success'] = $this->language->get('text_upload');
	// 	}

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function autocompletebname() {
		$json = array();
		if (isset($this->request->get['filter_brand'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_brand' => $this->request->get['filter_brand'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'brand'    => strip_tags(html_entity_decode($result['brand'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['brand'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletesubcategory() {
		$json = array();
		if (isset($this->request->get['filter_subcategory'])) {
			$this->load->model('catalog/brand');
			$data = array(
				'filter_subcategory' => $this->request->get['filter_subcategory'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_brand->getBrands($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'brand_id' => $result['brand_id'],
					'subcategory'    => strip_tags(html_entity_decode($result['subcategory'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['subcategory'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

}