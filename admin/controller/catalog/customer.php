<?php
class ControllerCatalogCustomer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customer');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/customer');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customer');
		//$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_customer->addCustomer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}


			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}


			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}
	public function add1(){
		$this->load->language('catalog/customer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/customer');
		$json['info'] = 0;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			if($this->request->post['c_id'] != '' && $this->request->post['c_id'] != 0){
				$this->model_catalog_customer->editCustomer($this->request->post['c_id'], $this->request->post);
				$customer_id = $this->request->post['c_id'];
				$msr_no = $this->request->post['msr_no'];
			} else{
				$customer_id = $this->model_catalog_customer->addCustomer($this->request->post);
			}
			$this->session->data['c_id'] = $customer_id;
			$this->session->data['msr_no'] = $this->request->post['msr_no'];
			$this->session->data['cust_id'] = $this->request->post['c_id'];
			// echo'<pre>';
			// print_r($customer_id);
			// exit;
			$json['done'] = '<script>parent.closeparceldetail();</script>';
			//$this->log->write(print_r($customer_data, true));
			$json['info'] = 1;
			//$json['done'] = '';
		} else{
			if(isset($this->error['contact']) && $this->error['contact'] != ''){
				$json['info'] = $this->error['contact'];
			} else if(isset($this->error['name']) && $this->error['name'] != ''){
				$json['info'] = $this->error['name'];
			} else if(isset($this->error['warning']) && $this->error['warning'] != ''){
				$json['info'] = $this->error['warning'];
			}
			
		}	
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
		$this->load->language('catalog/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customer');
		//$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//echo('innn');exit;
			$this->model_catalog_customer->editCustomer($this->request->get['c_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}

			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}


			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customer');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $c_id) {
				$this->model_catalog_customer->deleteCustomer($c_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';


			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_c_id'])) {
				$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
			}

			if (isset($this->request->get['filter_contact'])) {
				$url .= '&filter_contact=' . $this->request->get['filter_contact'];
			}

			if (isset($this->request->get['filter_email'])) {
				$url .= '&filter_email=' . $this->request->get['filter_email'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_c_id'])) {
			$filter_c_id = $this->request->get['filter_c_id'];
		} else {
			$filter_c_id = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_contact'])) {
			$filter_contact = $this->request->get['filter_contact'];
		} else {
			$filter_contact = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}
		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/customer/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/customer/delete', 'token=' . $this->session->data['token'] . $url, true);
		//$data['delete'] = $this->url->link('catalog/customer/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_c_id' => $filter_c_id,
			'filter_email' => $filter_email,
			'filter_contact' => $filter_contact,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$customer_total = $this->model_catalog_customer->getTotalCustomer($filter_data);

		$results = $this->model_catalog_customer->getCustomers($filter_data);

		$data['customers'] = array();
		foreach ($results as $result) {
			$data['customers'][] = array(
				'c_id' => $result['c_id'],
				'name'        => $result['name'],
				'contact'        => $result['contact'],
				'email'        => $result['email'],
				'edit'        => $this->url->link('catalog/customer/edit', 'token=' . $this->session->data['token'] . '&c_id=' . $result['c_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';

		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}

		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['name'] = $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['contact'] = $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . '&sort=contact' . $url, true);
		$data['email'] = $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . '&sort=email' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

		$data['filter_email'] = $filter_email;
		$data['filter_c_id'] = $filter_c_id;
		$data['filter_contact'] = $filter_contact;
		$data['filter_name'] = $filter_name;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customer_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['contact'])) {
			$data['error_contact'] = $this->error['contact'];
		} else {
			$data['error_contact'] = '';
			// echo $data['error_contact'];
			// exit();
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';

		}
		// echo $data['error_name'];
		// 	exit();

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_c_id'])) {
			$url .= '&filter_c_id=' . $this->request->get['filter_c_id'];
		}

		if (isset($this->request->get['filter_contact'])) {
			$url .= '&filter_contact=' . $this->request->get['filter_contact'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['c_id'])) {
			$data['action'] = $this->url->link('catalog/customer/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['action1'] = $this->url->link('catalog/customer/add1', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/customer/edit', 'token=' . $this->session->data['token'] . '&c_id=' . $this->request->get['c_id'] . $url, true);
			$data['action1'] = $this->url->link('catalog/customer/edit', 'token=' . $this->session->data['token'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/customer', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['c_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_catalog_customer->getCustomer($this->request->get['c_id']);
			// echo "<pre>";
			// print_r($customer_info);
			// exit();
		}

		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($customer_info)) {
			$data['name'] = $customer_info['name'];
		} else {
			$data['name'] = '';
		}

          $data['flag'] = 0;

		if (isset($this->request->post['contact'])) {
			$data['contact'] = $this->request->post['contact'];
		} elseif (!empty($customer_info)) {
			$data['contact'] = $customer_info['contact'];
		} else {
			$data['contact'] = '';
		}

		if(isset($this->request->get['contactno'])){
			$data['contact'] = $this->request->get['contactno'] ;
			$data['flag'] = 1;
		}
		if(isset($this->request->get['iframe'])){
			$data['contact'] = "" ;
			$data['flag'] = 1;
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($customer_info)) {
			$data['email'] = $customer_info['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($customer_info)) {
			$data['address'] = $customer_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['email2'])) {
			$data['email2'] = $this->request->post['email2'];
		} elseif (!empty($customer_info)) {
			$data['email2'] = $customer_info['email2'];
		} else {
			$data['email2'] = '';
		}

		$i_code = $this->db->query(" SELECT code FROM oc_customerinfo ORDER BY code DESC limit 0,1 ");
		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }

		if (isset($this->request->post['code'])) {
			$data['code'] = $this->request->post['code'];
		} elseif (!empty($customer_info)) {
			$data['code'] = $customer_info['code'];
		} else {
			$data['code'] = $new_code;
		}

		if (isset($this->request->post['mobile_no'])) {
			$data['mobile_no'] = $this->request->post['mobile_no'];
		} elseif (!empty($customer_info)) {
			$data['mobile_no'] = $customer_info['mobile_no'];
		} else {
			$data['mobile_no'] = '';
		}

		if (isset($this->request->post['alternate_no'])) {
			$data['alternate_no'] = $this->request->post['alternate_no'];
		} elseif (!empty($customer_info)) {
			$data['alternate_no'] = $customer_info['alternate_no'];
		} else {
			$data['alternate_no'] = '';
		}

		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($customer_info)) {
			$data['gst_no'] = $customer_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}

		if (isset($this->request->post['msr_no'])) {
			$data['msr_no'] = $this->request->post['msr_no'];
		} elseif (!empty($customer_info)) {
			$data['msr_no'] = $customer_info['msr_no'];
		} else {
			$data['msr_no'] = '';
		}


		if (isset($this->request->post['discount'])) {
			$data['discount'] = $this->request->post['discount'];
		} elseif (!empty($customer_info)) {
			$data['discount'] = $customer_info['discount'];
		} else {
			$data['discount'] = '';
		}

		if (isset($this->request->post['landmark'])) {
			$data['landmark'] = $this->request->post['landmark'];
		} elseif (!empty($customer_info)) {
			$data['landmark'] = $customer_info['landmark'];
		} else {
			$data['landmark'] = '';
		}

		if (isset($this->request->post['area_1'])) {
			$data['area_1'] = $this->request->post['area_1'];
		} elseif (!empty($customer_info)) {
			$data['area_1'] = $customer_info['area_1'];
		} else {
			$data['area_1'] = '';
		}

		if (isset($this->request->post['area_2'])) {
			$data['area_2'] = $this->request->post['area_2'];
		} elseif (!empty($customer_info)) {
			$data['area_2'] = $customer_info['area_2'];
		} else {
			$data['area_2'] = '';
		}

		if (isset($this->request->post['dob'])) {
			$data['dob'] = date('Y-m-d',strtotime($this->request->post['dob']));
		} elseif (!empty($customer_info)) {
			$data['dob'] =date('Y-m-d',strtotime( $customer_info['dob']));
		} else {
			$data['dob'] = '';
		}

		if (isset($this->request->post['anniversary'])) {
			$data['anniversary'] = date('Y-m-d',strtotime($this->request->post['anniversary']));
		} elseif (!empty($customer_info)) {
			$data['anniversary'] = date('Y-m-d',strtotime($customer_info['anniversary']));
		} else {
			$data['anniversary'] = '';
		}

		if (isset($this->request->post['passport'])) {
			$data['passport'] = $this->request->post['passport'];
		} elseif (!empty($customer_info)) {
			$data['passport'] = $customer_info['passport'];
		} else {
			$data['passport'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customer_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((strlen($this->request->post['name']) < 2) || (strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Please Enter  Name';
		}

		$datas = $this->request->post;
		if ((strlen($this->request->post['contact']) < 10) || (strlen($this->request->post['contact']) > 10)) {
			$this->error['contact'] = 'Please Enter Valid Number';
		} else {
			if(isset($this->request->get['c_id'])){
				$is_exist = $this->db->query("SELECT `c_id` FROM `oc_customerinfo` WHERE `contact` = '".$datas['contact']."' AND `c_id` <> '".$this->request->get['c_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['contact'] = 'Contact Number Already Exists';
				}
			} elseif(isset($this->request->post['c_id'])){
				$is_exist = $this->db->query("SELECT `c_id` FROM `oc_customerinfo` WHERE `contact` = '".$datas['contact']."' AND `c_id` <> '".$this->request->post['c_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['contact'] = 'Contact Number Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `c_id` FROM `oc_customerinfo` WHERE `contact` = '".$datas['contact']."' ");
				if($is_exist->num_rows > 0){
					$this->error['contact'] = 'Contact Number Already Exists';
				}
			}
		}


		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/customer');

		return !$this->error;
	}

	// public function upload() {
	// 	$this->load->language('catalog/customer');

	// 	$json = array();

	// 	// Check user has permission
	// 	if (!$this->user->hasPermission('modify', 'catalog/customer')) {
	// 		$json['error'] = $this->language->get('error_permission');
	// 	}

	// 	if (!$json) {
	// 		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
	// 			// Sanitize the filename
	// 			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

	// 			// Validate the filename length
	// 			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
	// 				$json['error'] = $this->language->get('error_filename');
	// 			}

	// 			// Allowed file extension types
	// 			$allowed = array();

	// 			$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

	// 			$filetypes = explode("\n", $extension_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Allowed file mime types
	// 			$allowed = array();

	// 			$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

	// 			$filetypes = explode("\n", $mime_allowed);

	// 			foreach ($filetypes as $filetype) {
	// 				$allowed[] = trim($filetype);
	// 			}

	// 			if (!in_array($this->request->files['file']['type'], $allowed)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Check to see if any PHP files are trying to be uploaded
	// 			$content = file_get_contents($this->request->files['file']['tmp_name']);

	// 			if (preg_match('/\<\?php/i', $content)) {
	// 				$json['error'] = $this->language->get('error_filetype');
	// 			}

	// 			// Return any upload error
	// 			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
	// 				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
	// 			}
	// 		} else {
	// 			$json['error'] = $this->language->get('error_upload');
	// 		}
	// 	}

	// 	if (!$json) {
	// 		$file = $filename . '.' . token(32);

	// 		move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

	// 		$json['filename'] = $file;
	// 		$json['mask'] = $filename;

	// 		$json['success'] = $this->language->get('text_upload');
	// 	}

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	// public function autocomplete() {
	// 	$json = array();

	// 	if (isset($this->request->get['filter_name'])) {
	// 		$this->load->model('catalog/customer');

	// 		$filter_data = array(
	// 			'filter_name' => $this->request->get['filter_name'],
	// 			//'filter_c_id' => $this->request->get['filter_c_id'],
	// 			'start'       => 0,
	// 			'limit'       => 20
	// 		);

	// 		$results = $this->model_catalog_customer->getCustomers($filter_data);

	// 		// echo "<pre>";
	// 		// print_r($results);
	// 		// exit();

	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'c_id' => $result['c_id'],
	// 				'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['name'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
	public function autocompletename() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/customer');
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_customer->getCustomers($data);
			// echo "<pre>";
			// print_r($this->request->get);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'name'    => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'contact' => $result['contact'],
					'address' => $result['address'],
					'email' => $result['email'],
					'email2' => $result['email2'],
					'code' => $result['code'],
					'mobile_no' => $result['mobile_no'],
					'alternate_no' => $result['alternate_no'],
					'gst_no' => $result['gst_no'],
					'msr_no' => $result['msr_no'],
					'discount' => $result['discount'],
					'landmark' => $result['landmark'],
					'area_1' => $result['area_1'],
					'area_2' => $result['area_2'],
					'dob' => $result['dob'],
					'anniversary' => $result['anniversary'],
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletemsrno() {
		$json = array();
		if (isset($this->request->get['filter_msr_no'])) {
			$this->load->model('catalog/customer');
			$data = array(
				'filter_msr_no' => $this->request->get['filter_msr_no'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_customer->getCustomers($data);
			// echo "<pre>";
			// print_r($this->request->get);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'name'    => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'contact' => $result['contact'],
					'address' => $result['address'],
					'email' => $result['email'],
					'email2' => $result['email2'],
					'code' => $result['code'],
					'mobile_no' => $result['mobile_no'],
					'alternate_no' => $result['alternate_no'],
					'gst_no' => $result['gst_no'],
					'msr_no' => $result['msr_no'],
					'discount' => $result['discount'],
					'landmark' => $result['landmark'],
					'area_1' => $result['area_1'],
					'area_2' => $result['area_2'],
					'dob' => $result['dob'],
					'anniversary' => $result['anniversary'],
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}


	public function autocompletecontact() {
		$json = array();
		if (isset($this->request->get['filter_contact']) ) {
			$this->load->model('catalog/customer');
			$data = array(
				'filter_contact' => $this->request->get['filter_contact'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_customer->getCustomers($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'contact'    => strip_tags(html_entity_decode($result['contact'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['contact'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}