<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogMsrRefund extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/msr_refund');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/msr_refund');

		$this->getList();
	}
	public function add() {
		$this->load->language('catalog/msr_refund');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/msr_refund');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')  && $this->validateForm()) {

			$creditid = $this->model_catalog_msr_refund->addrechageamt($this->request->post);
			//$this->printCredit($creditid);
			$this->print_html($creditid);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	
	public function edit() {
		$this->load->language('catalog/msr_refund');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/msr_refund');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_catalog_msr_refund->editCredit($this->request->get['item_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/msr_refund');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/msr_refund');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_msr_refund->deleteCredit($item_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_customername'])) {
			$filter_customername = $this->request->get['filter_customername'];
		} else {
			$filter_customername = null;
		}

		if (isset($this->request->get['filter_cust_id'])) {
			$filter_cust_id = $this->request->get['filter_cust_id'];
		} else {
			$filter_cust_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'customer_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_cust_id'])) {
			$url .= '&filter_cust_id=' . $this->request->get['filter_cust_id'];
		}
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/msr_refund/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/msr_refund/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		$filter_data = array(
			'filter_cust_id' => $filter_cust_id,
			'filter_customername' => $filter_customername,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$item_total = $this->model_catalog_msr_refund->getTotalrecharge_trans($filter_data);
		$results = $this->model_catalog_msr_refund->getmsr_trans($filter_data);

		foreach ($results as $result) {
			$data['items'][] = array(
				'cust_id' 			=> $result['id'],
				'customername'  	=> $result['cust_name'],
				'contact'			=> $result['cust_mob_no'],
				'msr_no'       		=> $result['msr_no'],
				'pre_bal'       	=> $result['cust_pre_amt'],
				'amount'       		=> $result['amount'],
				'extra_amt'       	=> $result['extra_amt'],
				'total_amt'       	=> $result['total_amt'],
				'payby'       		=> $result['pay_by'],
				'edit'        		=> $this->url->link('catalog/msr_refund/edit', 'token=' . $this->session->data['token'] . '&cust_id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_cust_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_cust_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_customer_name'] = $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . '&sort=customer_name' . $url, true);
		$data['sort_contact'] = $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . '&sort=contact' . $url, true);
		$data['sort_credit'] = $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . '&sort=credit' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_cust_id'])) {
			$url .= '&filter_cust_id=' . $this->request->get['filter_cust_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_customername'] = $filter_customername;
		$data['filter_cust_id'] = $filter_cust_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/msr_refund_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = "MSR Refund";;

		$data['text_form'] = !isset($this->request->get['c_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['msr_no'])) {
			$data['error_msr_no'] = $this->error['msr_no'];
		} else {
			$data['error_msr_no'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "MSR Refund",
			'href' => $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['c_id'])) {
			$data['action'] = $this->url->link('catalog/msr_refund/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/msr_refund/edit', 'token=' . $this->session->data['token'] . '&c_id=' . $this->request->get['c_id'] . $url, true);
		}

		$data['save'] = $this->url->link('catalog/msr_refund/add', 'token=' . $this->session->data['token'] . $url, true);

		$data['cancel'] = $this->url->link('catalog/msr_refund', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_msr_refund->getCredit($this->request->get['c_id']);
		}

		if (isset($this->request->post['c_id'])) {
			$data['c_id'] = $this->request->post['c_id'];
		} elseif (!empty($item_info)) {
			$data['c_id'] = $item_info['c_id'];
		} else {
			$data['c_id'] = '';
		}

		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}


		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}

		if (isset($this->request->post['contact'])) {
			$data['contact'] = $this->request->post['contact'];
		} elseif (!empty($item_info)) {
			$data['contact'] = $item_info['contact'];
		} else {
			$data['contact'] = '';
		}

		if (isset($this->request->post['remark'])) {
			$data['remark'] = $this->request->post['remark'];
		} elseif (!empty($item_info)) {
			$data['remark'] = $item_info['remark'];
		} else {
			$data['remark'] = '';
		}

		if (isset($this->request->post['msr_no'])) {
			$data['msr_no'] = $this->request->post['msr_no'];
		} elseif (!empty($item_info)) {
			$data['msr_no'] = $item_info['msr_no'];
		} else {
			$data['msr_no'] = '';
		}

		if (isset($this->request->post['msr'])) {
			$data['msr'] = $this->request->post['msr'];
		} elseif (!empty($item_info)) {
			$data['msr'] = $item_info['msr_no'];
		} else {
			$data['msr'] = '';
		}

		if (isset($this->request->post['amt'])) {
			$data['amt'] = $this->request->post['amt'];
		} elseif (!empty($item_info)) {
			$data['amt'] = $item_info['amt'];
		} else {
			$data['amt'] = 0;
		}

		if (isset($this->request->post['extra_amt'])) {
			$data['extra_amt'] = $this->request->post['extra_amt'];
		} elseif (!empty($item_info)) {
			$data['extra_amt'] = $item_info['extra_amt'];
		} else {
			$data['extra_amt'] = 0;
		}

		if (isset($this->request->post['payby'])) {
			$data['payby'] = $this->request->post['payby'];
		} elseif (!empty($item_info)) {
			$data['payby'] = $item_info['payby'];
		} else {
			$data['payby'] = 0;
		}

		if (isset($this->request->post['pre_bal'])) {
			$data['pre_bal'] = $this->request->post['pre_bal'];
		} elseif (!empty($item_info)) {
			$data['pre_bal'] = $item_info['pre_bal'];
		} else {
			$data['pre_bal'] = '';
		}

		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (!empty($item_info)) {
			$data['total_amt'] = $item_info['total_amt'];
		} else {
			$data['total_amt'] = 0;
		}

		//$data['add_customer'] = $this->url->link('catalog/customer/add', 'token=' . $this->session->data['token'] . $url.'&refer=1', true);

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/msr_refund_form', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/msr_refund')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if($this->request->post['msr'] == ''){
			$this->error['msr_no'] = 'Please Enter MSR NO.';
		}

		return !$this->error;
	}


	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/customercredit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/customercredit');

		return !$this->error;
	}


	public function printCredit ($printCredit){
		$final_daatas = $this->db->query("SELECT * FROM oc_msr_recharge_transaction WHERE id = '".$printCredit."' ")->row;
		// echo'<pre>';
		// print_r($final_daatas);
		// exit;
		$dates = date('Y-m-d');
		$current_date = date('d-m-Y',strtotime($dates));
			$this->load->model('catalog/order');

		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->text("MSR Refund");
		    $printer->feed(1);
		    $printer->text("Issue Slip");
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->feed(1);
		   	$printer->text("Date:".$current_date);
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text("MSR No. : ".$final_daatas['msr_no']);
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    
		   	$printer->text(str_pad("Cust Name",20)."".str_pad("Mobile No.", 12).""."Refund Amt");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    
		    $printer->setEmphasis(false);
		   	$printer->feed(1);

		    $total_items_normal = 0;
			$total_quantity_normal = 0;
			$total_amt_normal = 0;
		    
			$final_daatas['cust_name'] = utf8_substr(html_entity_decode($final_daatas['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 19);
			$final_daatas['cust_mob_no'] = utf8_substr(html_entity_decode($final_daatas['cust_mob_no'], ENT_QUOTES, 'UTF-8'), 0, 11);
			//$final_daatas['amount'] = utf8_substr(html_entity_decode($final_daatas['amount'], ENT_QUOTES, 'UTF-8'), 0, 5);
	    	$printer->text("".str_pad($final_daatas['cust_name'],20)."".str_pad($final_daatas['cust_mob_no'],12)."".$final_daatas['amount']);
	    	$printer->feed(1);
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
			$printer->feed(2);
		    $printer->cut();
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getForm();
	}


	public function print_html($printCredit){
		$final_daatas = array();
		$final_daatas = $this->db->query("SELECT * FROM oc_msr_recharge_transaction WHERE id = '".$printCredit."' ")->row;
		$json = array();
		$dates = date('Y-m-d');
		$this->load->model('catalog/order');
		$data['current_date'] = date('d-m-Y',strtotime($dates));
		$data['redirect_page'] = $this->url->link('catalog/msr_refund/redir_page', 'token=' . $this->session->data['token'], true);
		$data['hotel_name'] = $this->model_catalog_order->get_settings('HOTEL_NAME');
		$data['hotel_addss'] = $this->model_catalog_order->get_settings('HOTEL_ADD');
		$data['final_datas'] = $final_daatas;

		$html = $this->load->view('sale/msr_refund_html', $data);
		echo $html;
		exit();
		
	}

	public function redir_page(){
		$this->getForm();
	}
	

	public function mob_search() {
		//echo'inn';exit;
		$json = array();
		if (isset($this->request->get['filter_mob_no'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE contact = '" .$this->request->get['filter_mob_no']. "' ")->rows;

			foreach ($results as $result) {
				$pre_bal_data = $this->db->query("SELECT * FROM oc_msr_recharge_transaction WHERE cust_id = '".$result['c_id']."'");
				
				if($pre_bal_data->num_rows > 0){
					$pre_bal = $pre_bal_data->row['cust_pre_amt'];
				} else{
					$pre_bal = 0;
				}
				
				$json = array(
					'name' => $result['name'],
					'contact' => $result['contact'],
					'c_id' => $result['c_id'],
					'msr_no' => $result['msr_no'],
					'pre_bal' => $pre_bal,

				);
			}

		}

		$sort_order = array();

		/*foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);*/
		$this->response->setOutput(json_encode($json));
	}


	

	public function getPreviousBal(){
		$this->load->model('catalog/msr_refund');
		//echo'<pre>';print_r($this->request->get);
		$json = array();
			$pre_bal = $this->model_catalog_msr_refund->pre_bal($this->request->get['msr_no'], $this->request->get['cust_id']);
			$json = array(
				'pre_bal' 			=> $pre_bal
			);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocompletecustmsrno() {
		$json = array();
		if (isset($this->request->get['filter_msr_no'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE msr_no = '" .$this->request->get['filter_msr_no']. "' LIMIT 0,15")->rows;

			foreach ($results as $result) {
					$pre_bal_data = $this->db->query("SELECT * FROM oc_msr_recharge_transaction WHERE cust_id = '".$result['c_id']."'");
				
			//$pre_bal = $this->model_catalog_msr_refund->pre_bal($this->request->get['msr_no'], $result['c_id']);


				if($pre_bal_data->num_rows > 0){
					$pre_bal = $pre_bal_data->row['cust_pre_amt'];
				} else{
					$pre_bal = 0;
				}
				
				$json[] = array(
					'name' => $result['name'],
					'contact' => $result['contact'],
					'c_id' => $result['c_id'],
					'msr_no' => $result['msr_no'],
					'pre_bal' => $pre_bal,
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_customername'])) {
			$this->load->model('catalog/msr_refund');

			$data = array(
				'filter_customername' => $this->request->get['filter_customername'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_msr_refund->getmsr_search($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['cust_id'],
					'customername'  => $result['cust_name'],
					'contact'  => $result['cust_mob_no'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['customername'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}