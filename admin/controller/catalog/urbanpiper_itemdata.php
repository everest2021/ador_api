<?php

require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class Controllercatalogurbanpiperitemdata extends Controller {

	public function index() {
		$this->load->model('catalog/settlement');
		$this->getList();
	}


	/*public function insert_datas(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		// echo'<pre>';
		// print_r($this->request->get);
		//  exit;

		if(isset($this->request->get['order_id'])){
			$orderid = $this->request->get['order_id'];
			$prepare_time = $this->request->get['prepare_time'];
			$reject_reason = $this->request->get['reject_reason'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($prepare_time != ''){

				$body_data = array(
		  			'merchant_id' => MERCHANT_ID,
		  			'order_id' 	=> $orderid,
		  			'preparation_time' 	=> $prepare_time
		  		);
				
				$body = json_encode($body_data);
				//echo '<pre>'; print_r($body) ;exit;
				$url = "https://api.werafoods.com/pos/v2/order/accept";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response  = curl_exec($ch);
				$datas = json_decode($response,true);
					// echo'<pre>';
					// print_r($datas);
					// exit;
				if($datas['code'] == 1){
				$this->db->query("UPDATE oc_werafood_orders SET status = 'Accept' WHERE order_id = '".$orderid."'");

				$info_datas = $this->db->query("SELECT * FROM `oc_werafood_orders` WHERE order_id = '".$orderid."'")->row;
				$cust_datas = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$orderid."'")->row;
				$full_address = $cust_datas['address'].''.$cust_datas['delivery_area'].''.$cust_datas['address_instructions'];
				// echo'<pre>';
				// print_r($info_datas);
				// exit();
				$dates = date('Y-m-d',strtotime($info_datas['order_date_time']));
				$times = date('h:i:s',strtotime($info_datas['order_date_time']));

				date_default_timezone_set("Asia/Kolkata");
				$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date = date('Y-m-d');
				}

				$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
				$o_kot_no = 1;
				if(isset($kot_no_datas['kot_no'])){
					$o_kot_no = $kot_no_datas['kot_no'] + 1;
				}

				$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
							`location_id` = '0',
							`parcel_status` = '0', 
							`cust_name` = '" . $this->db->escape($cust_datas['name']) ."', 
							`cust_id` = '" . $this->db->escape($cust_datas['customer_id']) ."',
							`cust_contact` = '" . $this->db->escape($cust_datas['phone_number']) ."',
							`cust_address` = '" . $this->db->escape($full_address) ."',
							`cust_email` = '" . $this->db->escape($cust_datas['email']) ."',
							`t_name` = '0',
							`table_id` = '0',  
							`ftotal` = '" . $this->db->escape($info_datas['net_amount']) . "',
							`ftotal_discount` = '" . $this->db->escape($info_datas['net_amount']) . "',
							`gst` = '" . $this->db->escape($info_datas['gst']) . "',
							`ftotalvalue` = '" . $this->db->escape($info_datas['discount']) . "',
							`discount` = '" . $this->db->escape($info_datas['discount']) . "',
							`grand_total` = '" . $this->db->escape($info_datas['gross_amount']) . "',
							`bill_date` = '".$last_open_date."',
							`date_added` = '".$dates."',
							`time_added` = '".$times."',
							`kot_no` = '".$o_kot_no."',
							`wera_order_id` = '".$orderid."',
							`order_from` = '".$info_datas['order_from']."',
							`payment_status` = '1',
							`pay_method` = '1',
							`order_packaging` = '" . $this->db->escape($info_datas['order_packaging']) . "',
							`packaging_cgst` = '" . $this->db->escape($info_datas['packaging_cgst']) . "',
							`packaging_sgst` = '" . $this->db->escape($info_datas['packaging_sgst']) . "',
							`packaging_cgst_percent` = '" . $this->db->escape($info_datas['packaging_cgst_percent']) . "',
							`packaging_sgst_percent` = '" . $this->db->escape($info_datas['packaging_sgst_percent']) . "',
							`packaging` = '" . $this->db->escape($info_datas['packaging']) . "',
							`ftotal_discount` = '" . $this->db->escape($info_datas['ftotal_discount']) . "',
							`discount` = '" . $this->db->escape($info_datas['discount']) . "',
							`discount_reason` = '" . $this->db->escape($info_datas['discount_reason']) . "',
							`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
							`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
						$order_id = $this->db->getLastId();
						date_default_timezone_set("Asia/Kolkata");
						$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
						$last_open_dates = $this->db->query($last_open_date_sql);
						if($last_open_dates->num_rows > 0){
							$last_open_date = $last_open_dates->row['bill_date'];
						} else {
							$last_open_date = date('Y-m-d');
						}

						$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items` oit  WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['kot_no'];
							$kotno = $kot_no2 + 1;
						} else{
							$kotno = 1;
						}

						$item_datas = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$orderid."'")->rows;
						foreach ($item_datas as $ikey => $ivalue) {
						
							$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
										`order_id` = '" . $this->db->escape($order_id) . "',   
										`code` = '" . $this->db->escape($ivalue['item_id']) ."',
										`name` = '" . htmlspecialchars_decode($ivalue['item_name']) ."', 
										`qty` = '" . $this->db->escape($ivalue['item_quantity']) . "', 
										`rate` = '" . $this->db->escape($ivalue['item_unit_price']) . "',  
										`amt` = '" . $this->db->escape($ivalue['subtotal']) . "',
										`ismodifier` = '1',  
										`nc_kot_status` = '0',
										`nc_kot_reason` = '',
										`transfer_qty` = '0',
										`parent_id` = '0', 
										`parent` = '0',  
										`subcategoryid` = '1',
										`kot_status` = '0',
										`kot_no` = '" . $this->db->escape($kotno) . "',
										`prefix` = '',
										`pre_qty` = '',
										`kitchen_display` = '',
										`is_liq` = '0',
										`is_new` = '1',
										`cancelstatus` = '0',
										`discount_per` = '',
										`discount_value` = '" . $this->db->escape($ivalue['discount']) . "',
										`tax1` = '" . $this->db->escape($ivalue['cgst_percent']) . "',
										`tax2` = '" . $this->db->escape($ivalue['sgst_percent']) . "',
										`tax1_value` = '" . $this->db->escape($ivalue['cgst']) . "',
										`tax2_value` = '" . $this->db->escape($ivalue['sgst']) . "',
										`stax` = '',
										`captain_id` = '0',
										`captain_commission` = '0',
										`waiter_id` = '0',
										`waiter_commission` = '0',
										`message` = '".$this->db->escape($ivalue['instructions'])."',
										`bill_date` = '".$last_open_date."',
										`date` = '".date('Y-m-d')."',
										`time` = '".date('H:i:s')."',
										`login_id` = '" . $this->db->escape($this->user->getId()) . "',
										`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
							}
						}
					} 
					$json['done'] = '<script>parent.closeIFrame();</script>';
					$json['info'] = 1;
					$this->response->setOutput(json_encode($json));
				} elseif($datas['code'] == 2) {
					$json['info'] = 2;
					$json['reason'] = $datas['msg'];
					//$json['done'] = '<script>parent.closeIFrame();</script>';
				}else {
					$json['done'] = '<script>parent.closeIFrame();</script>';
					$json['info'] = 0;
					$this->response->setOutput(json_encode($json));
				}
	}*/

	public function reject_order(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		if(isset($this->request->get['order_id'])){
			sleep(5);
			$orderid = $this->request->get['order_id'];
			$reject_reason = $this->request->get['reject_reason'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($reject_reason != ''){
				$body_data = array(
				    "new_status" => "Cancelled",
				    "message" => $reject_reason
				 );
				$body = json_encode($body_data);
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt_array($curl, array(
				  	CURLOPT_URL => sprintf(ORDER_UPDATE_API,$orderid),
				  	CURLOPT_RETURNTRANSFER => true,
				  	CURLOPT_ENCODING => '',
				  	CURLOPT_MAXREDIRS => 10,
				 	CURLOPT_TIMEOUT => 0,
				  	CURLOPT_FOLLOWLOCATION => true,
				  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  	CURLOPT_CUSTOMREQUEST => 'PUT',
				  	CURLOPT_POSTFIELDS => $body,
				  	CURLOPT_HTTPHEADER => array(
				    	'Authorization:'.URBANPIPER_API_KEY,
				    	'Content-Type: application/json',
				  	),
				));

				$response = curl_exec($curl);
				if (curl_errno($curl)) {
		    		$error_msg = curl_error($curl);
				}

				curl_close($curl);
				$datas = json_decode($response,true);
				//echo "<pre>";print_r($datas);exit;
				$error = 0;
				$success = 0;
				if($datas['status'] == 'success'){
					$json['success'] = 'Food Cancelled Succesfully...!';
					  $check_channel = $this->db->query("SELECT * FROM `oc_orders_app` WHERE online_order_id = '".$orderid."'");
					  if($check_channel->num_rows > 0){
					  		if($check_channel->row['online_channel'] == 'swiggy'){
					  			$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Cancelled' ,swiggy_cancel = 'You will get a call from swiggy' ,swiggy_cancel_show_msg = '0' WHERE online_order_id = '".$check_channel->row['online_order_id']."' ");
					  		} else {
					  			$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Cancelled' WHERE online_order_id = '".$check_channel->row['online_order_id']."' ");
					  		}
					  }
					
					$json['info'] = 1;
				} elseif($datas['message'] == 'Cancellation of Swiggy orders is not allowed. Callback requested instead.') {
					$json['errors'] = $datas['message'];
					$check_channel = $this->db->query("SELECT * FROM `oc_orders_app` WHERE online_order_id = '".$orderid."'");
					  if($check_channel->num_rows > 0){
					  		if($check_channel->row['online_channel'] == 'swiggy'){
					  			$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Cancelled' ,swiggy_cancel = 'You will get a call from swiggy' ,swiggy_cancel_show_msg = '0' WHERE online_order_id = '".$check_channel->row['online_order_id']."' ");
					  		} else {
					  			$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Cancelled' WHERE online_order_id = '".$check_channel->row['online_order_id']."' ");
					  		}
					  }
					$success = 0;
				}  elseif($datas['status'] == 'error') {
					$error = 1;
					$json['errors'] = $datas['message'];
				} else if (isset($error_msg)) {
					$json['errors'] = $error_msg;
					$error = 1;
				}   else {
					$error = 1;
					$json['info'] = 0;
					$json['errors'] = $response;
				}

				 $error_plus = 0;
				$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Cancelled' AND `order_id` = '".$this->db->escape($this->request->get['order_id'])."'  ORDER BY id desc Limit 1");
				if ($check_error->num_rows > 0) {
					if($error == 1){
						//if($check_error->row['failure']){
							$error_plus = $check_error->row['failure'] + 1;
						//}
					}
					$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '".$this->db->escape($success)."', 
							`failure` = '".$this->db->escape($error_plus)."' ,
							 `date`= NOW(),
							`order_id` = '".$this->db->escape($this->request->get['order_id'])."' 
							WHERE `event` = 'Cancelled' ");
				} else {
					$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '".$this->db->escape($success)."', 
							`failure` = '".$this->db->escape($error)."'  ,
							 `date`= NOW(),
							`order_id` = '".$this->db->escape($this->request->get['order_id'])."' ,
							`event` = 'Cancelled'
						");
				}
				sleep(2);
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		
		$this->response->setOutput(json_encode($json));

	}

	public function reject_order_swiggy(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		if(isset($this->request->get['order_id'])){
			$orderid = $this->request->get['order_id'];
			$reject_reason = $this->request->get['reject_reason'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($reject_reason != ''){
				$body_data = array(
	  				'merchant_id' => MERCHANT_ID,
	  				'order_id' => $orderid,
	  				'remark'  =>	$reject_reason,
	  			);
		
				$body = json_encode($body_data);
				//echo '<pre>'; print_r($body) ;exit;
				$url = "https://api.werafoods.com/pos/v2/order/callsupport";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response  = curl_exec($ch);
				$datas = json_decode($response,true);
					// echo'<pre>';
					// print_r($datas);
					// exit;
				if($datas['code'] == 1){
					$this->db->query("UPDATE oc_werafood_orders SET status = '".$reject_reason."' WHERE order_id = '".$orderid."'");
					$json['info'] = 1;
				} elseif($datas['code'] == 2) {
					$json['info'] = 2;
					$json['reason'] = $datas['msg'];
				} else {
					$json['info'] = 0;
				}
				// $info_datas = $this->db->query("SELECT * FROM `oc_werafood_orders` WHERE order_id = '".$orderid."'")->row;
				// $cust_datas = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$orderid."'")->row;

				// // echo'<pre>';
				// // print_r($info_datas);
				// // exit();
				// $dates = date('Y-m-d',strtotime($info_datas['order_date_time']));
				// $times = date('h:i:s',strtotime($info_datas['order_date_time']));

				// date_default_timezone_set("Asia/Kolkata");
				// $last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				// $last_open_dates = $this->db->query($last_open_date_sql);
				// if($last_open_dates->num_rows > 0){
				// 	$last_open_date = $last_open_dates->row['bill_date'];
				// } else {
				// 	$last_open_date = date('Y-m-d');
				// }

				// $kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
				// $o_kot_no = 1;
				// if(isset($kot_no_datas['kot_no'])){
				// 	$o_kot_no = $kot_no_datas['kot_no'] + 1;
				// }

				// $this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
				// 			`location_id` = '8',
				// 			`parcel_status` = '0', 
				// 			`cust_name` = '" . $this->db->escape($cust_datas['name']) ."', 
				// 			`cust_id` = '" . $this->db->escape($cust_datas['customer_id']) ."',
				// 			`cust_contact` = '" . $this->db->escape($cust_datas['phone_number']) ."',
				// 			`cust_address` = '" . $this->db->escape($cust_datas['address']) ."',
				// 			`cust_email` = '" . $this->db->escape($cust_datas['email']) ."',
				// 			`t_name` = '1',
				// 			`table_id` = '1',  
				// 			`ftotal` = '" . $this->db->escape($info_datas['net_amount']) . "',
				// 			`ftotal_discount` = '" . $this->db->escape($info_datas['net_amount']) . "',
				// 			`gst` = '" . $this->db->escape($info_datas['gst']) . "',
				// 			`ftotalvalue` = '" . $this->db->escape($info_datas['discount']) . "',
				// 			`discount` = '" . $this->db->escape($info_datas['discount']) . "',
				// 			`grand_total` = '" . $this->db->escape($info_datas['gross_amount']) . "',
				// 			`bill_date` = '".$dates."',
				// 			`date_added` = '".$dates."',
				// 			`time_added` = '".$times."',
				// 			`kot_no` = '".$o_kot_no."',
				// 			`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
				// 			`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
				// 			`login_id` = '" . $this->db->escape($this->user->getId()) . "',
				// 			`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
				// 		$order_id = $this->db->getLastId();
				// 		date_default_timezone_set("Asia/Kolkata");
				// 		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				// 		$last_open_dates = $this->db->query($last_open_date_sql);
				// 		if($last_open_dates->num_rows > 0){
				// 			$last_open_date = $last_open_dates->row['bill_date'];
				// 		} else {
				// 			$last_open_date = date('Y-m-d');
				// 		}

				// 		$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items` oit  WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
				// 		if($kotno2->num_rows > 0){
				// 			$kot_no2 = $kotno2->row['kot_no'];
				// 			$kotno = $kot_no2 + 1;
				// 		} else{
				// 			$kotno = 1;
				// 		}

				// 		$item_datas = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$orderid."'")->rows;
				// 		foreach ($item_datas as $ikey => $ivalue) {
						
				// 			$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
				// 						`order_id` = '" . $this->db->escape($order_id) . "',   
				// 						`code` = '" . $this->db->escape($ivalue['item_id']) ."',
				// 						`name` = '" . htmlspecialchars_decode($ivalue['item_name']) ."', 
				// 						`qty` = '" . $this->db->escape($ivalue['item_quantity']) . "', 
				// 						`rate` = '" . $this->db->escape($ivalue['item_unit_price']) . "',  
				// 						`amt` = '" . $this->db->escape($ivalue['subtotal']) . "',
				// 						`ismodifier` = '1',  
				// 						`nc_kot_status` = '0',
				// 						`nc_kot_reason` = '',
				// 						`transfer_qty` = '0',
				// 						`parent_id` = '0', 
				// 						`parent` = '0',  
				// 						`subcategoryid` = '1',
				// 						`kot_status` = '0',
				// 						`kot_no` = '" . $this->db->escape($kotno) . "',
				// 						`prefix` = '',
				// 						`pre_qty` = '',
				// 						`kitchen_display` = '',
				// 						`is_liq` = '0',
				// 						`is_new` = '1',
				// 						`cancelstatus` = '0',
				// 						`discount_per` = '',
				// 						`discount_value` = '" . $this->db->escape($ivalue['discount']) . "',
				// 						`tax1` = '" . $this->db->escape($ivalue['cgst_percent']) . "',
				// 						`tax2` = '" . $this->db->escape($ivalue['sgst_percent']) . "',
				// 						`tax1_value` = '" . $this->db->escape($ivalue['cgst']) . "',
				// 						`tax2_value` = '" . $this->db->escape($ivalue['sgst']) . "',
				// 						`stax` = '',
				// 						`captain_id` = '0',
				// 						`captain_commission` = '0',
				// 						`waiter_id` = '0',
				// 						`waiter_commission` = '0',
				// 						`message` = '',
				// 						`bill_date` = '".$dates."',
				// 						`date` = '".date('Y-m-d')."',
				// 						`time` = '".date('H:i:s')."',
				// 						`login_id` = '" . $this->db->escape($this->user->getId()) . "',
				// 						`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
				// 			}
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		
		$this->response->setOutput(json_encode($json));

	}

	public function dispatched(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		if(isset($this->request->get['order_id'])){
			$orderid = $this->request->get['order_id'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($orderid != ''){
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt_array($curl, array(
				  	CURLOPT_URL => sprintf(ORDER_UPDATE_API,$orderid),
				  	CURLOPT_RETURNTRANSFER => true,
				  	CURLOPT_ENCODING => '',
				  	CURLOPT_MAXREDIRS => 10,
				 	CURLOPT_TIMEOUT => 0,
				  	CURLOPT_FOLLOWLOCATION => true,
				  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  	CURLOPT_CUSTOMREQUEST => 'PUT',
				  	CURLOPT_POSTFIELDS =>'{
						"new_status": "Dispatched",
						"message": "Order Dispatched"
					}',
				  	CURLOPT_HTTPHEADER => array(
				    	'Authorization:'.URBANPIPER_API_KEY,
				    	'Content-Type: application/json',
				  	),
				));

				$response = curl_exec($curl);

				curl_close($curl);
				$datas = json_decode($response,true);
				if($datas['status'] == 'success'){
					$json['success'] = 'Order Dispatched Send Succesfully...!';
						//$this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
					$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Dispatched' WHERE online_order_id = '".$orderid."' ");
					$json['info'] = 1;
				} elseif($datas['status'] == 'error') {
					$json['errors'] = $datas['message'];
				} else {
					$json['info'] = 0;
					$json['errors'] = $response;
				}
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$this->response->setOutput(json_encode($json));
	}

	public function order_ready(){
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');
		if(isset($this->request->get['order_id'])){
			//sleep(5);
			$orderid = $this->request->get['order_id'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($orderid != ''){
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt_array($curl, array(
				  	//CURLOPT_URL => 'https://pos-int.urbanpiper.com/external/api/v1/orders/'.$orderid.'/status/',
				  	CURLOPT_URL => sprintf(ORDER_UPDATE_API,$orderid),
				  	CURLOPT_RETURNTRANSFER => true,
				  	CURLOPT_ENCODING => '',
				  	CURLOPT_MAXREDIRS => 10,
				 	CURLOPT_TIMEOUT => 0,
				  	CURLOPT_FOLLOWLOCATION => true,
				  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  	CURLOPT_CUSTOMREQUEST => 'PUT',
				  	CURLOPT_POSTFIELDS =>'{
						"new_status": "Food Ready",
						"message": "Food Ready Please Take Order"
					}',
				  	CURLOPT_HTTPHEADER => array(
				    	'Authorization:'.URBANPIPER_API_KEY,
				    	'Content-Type: application/json',
				  	),
				));



				$response = curl_exec($curl);
				if (curl_errno($curl)) {
				    $error_msg = curl_error($curl);
				}

				curl_close($curl);
				$datas = json_decode($response,true);
				$error = 0;
				$success = 0;
				if($datas['status'] == 'success'){
					$json['success'] = 'Food Ready Send Succesfully...!';
						//$this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
					$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Food Ready' WHERE online_order_id = '".$orderid."' ");
					$json['info'] = 1;
					$json['done'] = '<script>parent.closeIFrame();</script>';
					$success = 1;
				} elseif($datas['status'] == 'error') {
					$json['errors'] = $datas['message'];
					$error = 1;
				} else if (isset($error_msg)) {
					$json['errors'] = $error_msg;
					$error = 1;
				}else {
					$json['info'] = 0;
					$json['errors'] = $response;
					$error = 1;
				}
				$error_plus = 0;
				$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Food Ready' ORDER BY id desc Limit 1");
				if ($check_error->num_rows > 0) {
					if($error == 1){
						//if($check_error->row['failure']){
							$error_plus = $check_error->row['failure'] + 1;
						//}
					}
					$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '".$this->db->escape($success)."', 
							`failure` = '".$this->db->escape($error_plus)."'  ,
							 `date`= NOW(),
							`order_id`= '".$this->db->escape($orderid)."'
							WHERE `event` = 'Food Ready' ");
				} else {
					$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
							`sucess`= '".$this->db->escape($success)."', 
							`failure` = '".$this->db->escape($error)."'  ,
							 `date`= NOW(),
							`order_id`= '".$this->db->escape($orderid)."',
							`event` = 'Food Ready'
						");
				}
				sleep(2);
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$this->response->setOutput(json_encode($json));
	}

	// public function update_order() {
	// 	$this->load->model('catalog/order');
	// 	$this->load->model('catalog/msr_recharge');
	// 	$this->load->language('catalog/order');

	// 	// echo'<pre>';
	// 	// print_r($this->request->get);
	// 	//  exit;

	// 	if(isset($this->request->get['order_id'])){
	// 		$orderid = $this->request->get['order_id'];
	// 		//$prepare_time = $this->request->get['prepare_time'];
	// 		//$reject_reason = $this->request->get['reject_reason'];
	// 		// echo'inn';
	// 		// 	exit;
	// 		$edit = 0;
	// 		if($prepare_time != ''){

	// 			$body_data = array(
	// 	  			'merchant_id' => MERCHANT_ID,
	// 	  			'order_id' 	=> $orderid,
	// 	  			'preparation_time' 	=> $prepare_time
	// 	  		);
				
	// 			$body = json_encode($body_data);
	// 			//echo '<pre>'; print_r($body) ;exit;
	// 			$url = "https://api.werafoods.com/pos/v2/order/accept";
	// 			$ch = curl_init();
	// 			curl_setopt($ch, CURLOPT_URL, $url);
	// 			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
	// 			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	// 			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	// 			curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
	// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 			$response  = curl_exec($ch);
	// 			$datas = json_decode($response,true);
	// 				// echo'<pre>';
	// 				// print_r($datas);
	// 				// exit;

	// 			if($datas['code'] == 1){
	// 				date_default_timezone_set("Asia/Kolkata");
	// 				$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
	// 				$last_open_dates = $this->db->query($last_open_date_sql);
	// 				if($last_open_dates->num_rows > 0){
	// 					$last_open_date = $last_open_dates->row['bill_date'];
	// 				} else {
	// 					$last_open_date = date('Y-m-d');
	// 				}

	// 				$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."'  ORDER BY `kot_no` DESC LIMIT 1")->row;
	// 				$o_kot_no = 1;
	// 				if(isset($kot_no_datas['kot_no'])){
	// 					$o_kot_no = $kot_no_datas['kot_no'] + 1;
	// 				}
	// 				$this->db->query("UPDATE oc_werafood_orders SET status = 'Accept' WHERE order_id = '".$orderid."'");

	// 				$info_datas = $this->db->query("SELECT * FROM `oc_werafood_orders` WHERE order_id = '".$orderid."'")->row;
	// 				$cust_datas = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$orderid."'")->row;
	// 				$full_address = $cust_datas['address'].''.$cust_datas['delivery_area'].''.$cust_datas['address_instructions'];

	// 				$item_datas = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$orderid."'")->rows;

	// 				// echo'<pre>';
	// 				// print_r($info_datas);
	// 				// exit();
	// 				$dates = date('Y-m-d',strtotime($info_datas['order_date_time']));
	// 				$times = date('h:i:s',strtotime($info_datas['order_date_time']));
	// 				$qty = 0;
	// 				foreach ($item_datas as $idkey => $idvalue) {
	// 					$qty = $qty + $idvalue['item_quantity'];

	// 				}

	// 				$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
	// 							`location_id` = '9999',
	// 							`parcel_status` = '0', 
	// 							`cust_name` = '" . $this->db->escape($cust_datas['name']) ."', 
	// 							`cust_id` = '" . $this->db->escape($cust_datas['customer_id']) ."',
	// 							`cust_contact` = '" . $this->db->escape($cust_datas['phone_number']) ."',
	// 							`cust_address` = '" . $this->db->escape($full_address) ."',
	// 							`cust_email` = '" . $this->db->escape($cust_datas['email']) ."',
	// 							`t_name` = '9999',
	// 							`kot_no` = '".$o_kot_no."',
	// 							`location` = 'ONLINE ORDERS',
	// 							`table_id` = '9999',  
	// 							`ftotal` = '" . $this->db->escape($info_datas['net_amount']) . "',
	// 							`ftotal_discount` = '" . $this->db->escape($info_datas['net_amount']) . "',
	// 							`gst` = '" . $this->db->escape($info_datas['gst']) . "',
	// 							`ftotalvalue` = '" . $this->db->escape($info_datas['discount']) . "',
	// 							`discount` = '" . $this->db->escape($info_datas['discount']) . "',
	// 							`grand_total` = '" . $this->db->escape($info_datas['gross_amount']) . "',
	// 							`bill_date` = '".$last_open_date."',
	// 							`date_added` = '".$dates."',
	// 							`time_added` = '".$times."',
	// 							`wera_order_id` = '".$orderid."',
	// 							`order_from` = '".$info_datas['order_from']."',
	// 							`payment_status` = '1',
	// 							`pay_method` = '1',
	// 							`item_quantity` = '".$qty."',
	// 							`total_items` = '".$qty."',
	// 							`total_payment` = '" . $this->db->escape($info_datas['gross_amount']) . "',
	// 							`order_packaging` = '" . $this->db->escape($info_datas['order_packaging']) . "',
	// 							`packaging_cgst` = '" . $this->db->escape($info_datas['packaging_cgst']) . "',
	// 							`packaging_sgst` = '" . $this->db->escape($info_datas['packaging_sgst']) . "',
	// 							`packaging_cgst_percent` = '" . $this->db->escape($info_datas['packaging_cgst_percent']) . "',
	// 							`packaging_sgst_percent` = '" . $this->db->escape($info_datas['packaging_sgst_percent']) . "',

	// 							`packaging` = '" . $this->db->escape($info_datas['packaging']) . "',
	// 							`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
	// 							`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
	// 							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
	// 							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
					
	// 				$order_id = $this->db->getLastId();
	// 				date_default_timezone_set("Asia/Kolkata");
	// 				$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
	// 				$last_open_dates = $this->db->query($last_open_date_sql);
	// 				if($last_open_dates->num_rows > 0){
	// 					$last_open_date = $last_open_dates->row['bill_date'];
	// 				} else {
	// 					$last_open_date = date('Y-m-d');
	// 				}

	// 				$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_items` oit  WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
	// 				if($kotno2->num_rows > 0){
	// 					$kot_no2 = $kotno2->row['kot_no'];
	// 					$kotno = $kot_no2 + 1;
	// 				} else{
	// 					$kotno = 1;
	// 				}

					
	// 				foreach ($item_datas as $ikey => $ivalue) {
					
	// 					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
	// 								`order_id` = '" . $this->db->escape($order_id) . "',   
	// 								`code` = '" . $this->db->escape($ivalue['item_id']) ."',
	// 								`name` = '" . htmlspecialchars_decode($ivalue['item_name']) ."', 
	// 								`qty` = '" . $this->db->escape($ivalue['item_quantity']) . "', 
	// 								`rate` = '" . $this->db->escape($ivalue['item_unit_price']) . "',  
	// 								`amt` = '" . $this->db->escape($ivalue['subtotal']) . "',
	// 								`ismodifier` = '1',  
	// 								`nc_kot_status` = '0',
	// 								`nc_kot_reason` = '',
	// 								`transfer_qty` = '0',
	// 								`parent_id` = '0', 
	// 								`parent` = '0',  
	// 								`subcategoryid` = '1',
	// 								`kot_status` = '0',
	// 								`kot_no` = '" . $this->db->escape($kotno) . "',
	// 								`prefix` = '',
	// 								`pre_qty` = '',
	// 								`kitchen_display` = '',
	// 								`is_liq` = '0',
	// 								`is_new` = '0',
	// 								`cancelstatus` = '0',
	// 								`discount_per` = '',
	// 								`discount_value` = '" . $this->db->escape($ivalue['discount']) . "',
	// 								`tax1` = '" . $this->db->escape($ivalue['cgst_percent']) . "',
	// 								`tax2` = '" . $this->db->escape($ivalue['sgst_percent']) . "',
	// 								`tax1_value` = '" . $this->db->escape($ivalue['cgst']) . "',
	// 								`tax2_value` = '" . $this->db->escape($ivalue['sgst']) . "',
	// 								`stax` = '',
	// 								`captain_id` = '0',
	// 								`captain_commission` = '0',
	// 								`waiter_id` = '0',
	// 								`waiter_commission` = '0',
	// 								`message` = '".$this->db->escape($ivalue['instructions'])."',
	// 								`packaging_amt` = '".$this->db->escape($ivalue['packaging'])."',
	// 								`bill_date` = '".$last_open_date."',
	// 								`date` = '".date('Y-m-d')."',
	// 								`time` = '".date('H:i:s')."',
	// 								`login_id` = '" . $this->db->escape($this->user->getId()) . "',
	// 								`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
	// 				}


	// 				//$order_id = 44;
	// 				$anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY subcategoryid")->rows;
	// 				$anss_1 = array();
	// 				$anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;

	// 				$liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
	// 				$liqurs_1 = array();
	// 				$liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
	// 				$user_id= $this->user->getId();
	// 				$user = $this->db->query("SELECT `checkkot` FROM oc_user WHERE user_id = '".$user_id."'")->row;
	// 				// if($user['checkkot'] == '1'){
	// 				// 	$checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
	// 				// }

	// 				$checkkot = array();
				
	// 				$infos_normal = array();
	// 				$infos_cancel = array();
	// 				$infos_liqurnormal = array();
	// 				$infos_liqurcancel = array();
	// 				$infos = array();
	// 				$modifierdata = array();
	// 				$allKot = array();
	// 				$ans = $this->db->query("SELECT `order_id`, `wera_order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
	// 				// echo'<pre>';
	// 				// print_r($ans);
	// 				// exit;
	// 				foreach ($ans as $resultt) {
	// 					$infoss[] = array(
	// 						'order_id'   => $resultt['order_id'],
	// 						'wera_order_id'   => $resultt['wera_order_id'],

	// 						'time_added'  => $resultt['time_added'],
	// 						'date_added'  => $resultt['date_added'],
	// 						'location'  => $resultt['location'],
	// 						'location_id' => $resultt['location_id'],
	// 						't_name'    => $resultt['t_name'],
	// 						'table_id'  => $resultt['table_id'],
	// 						'waiter'    => $resultt['waiter'],
	// 						'waiter_id'    => $resultt['waiter_id'],
	// 						'captain'   => $resultt['captain'],
	// 						'captain_id'   => $resultt['captain_id'],
	// 						'item_quantity'   => $resultt['item_quantity'],
	// 						'total_items'   => $resultt['total_items'],
	// 						'login_id' => $resultt['login_id'],
	// 						'login_name' => $resultt['login_name'],
	// 						'person' => $resultt['person'],
	// 						'ftotal' => $resultt['ftotal'],
	// 						'ltotal' => $resultt['ltotal'],
	// 						'grand_total' => $resultt['grand_total'],
	// 						'bill_status' => $resultt['bill_status'],
	// 					);
	// 				}
	// 				// echo'<pre>';
	// 				// print_r($anss);
	// 				// exit();
	// 				$kot_group_datas = array();
	// 				$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup")->rows;
	// 				foreach($printerinfos as $pkeys => $pvalues){
	// 					if($pvalues['subcategory'] != ''){
	// 						$subcategoryid_exp = explode(',', $pvalues['subcategory']);
	// 						foreach($subcategoryid_exp as $skey => $svalue){
	// 							$kot_group_datas[$svalue] = $pvalues;
	// 						}
	// 					}
	// 				}


				
	// 				foreach ($anss as $lkey => $result) {
	// 					foreach($anss as $lkeys => $resultss){
	// 						if($lkey == $lkeys) {

	// 						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['code'] = '';
	// 								}
	// 							}
	// 						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['qty'] = $result['qty'] + $resultss['qty'];
	// 									if($result['nc_kot_status'] == '0'){
	// 										$result['amt'] = $result['qty'] * $result['rate'];
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}

	// 					if($result['code'] != ''){
	// 						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
	// 							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
	// 						}

	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 									$qty = (int)$result['qty'];
	// 							} else {
	// 									$qty = $result['qty'];
	// 							}
	// 						} else {
	// 							$qty = $result['qty'];
	// 						}

							
	// 						$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
	// 							'id'				=> $result['id'],
	// 							'name'           	=> $result['name'],
	// 							'qty'         		=> $qty,
	// 							'code'         		=> $result['code'],
	// 							'amt'				=> $result['amt'],
	// 							'rate'				=> $result['rate'],
	// 							'message'         	=> $result['message'],
	// 							'subcategoryid'		=> $result['subcategoryid'],
	// 							'kot_no'            => $result['kot_no'],
	// 							'time_added'        => $result['time'],
	// 						);

	// 						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$flag = 0;
	// 					}
	// 				}

	// 				foreach ($checkkot as $lkey => $result) {
	// 					foreach($checkkot as $lkeys => $resultss){
	// 						if($lkey == $lkeys) {

	// 						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['code'] = '';
	// 								}
	// 							}
	// 						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['qty'] = $result['qty'] + $resultss['qty'];
	// 									if($result['nc_kot_status'] == '0'){
	// 										$result['amt'] = $result['qty'] * $result['rate'];
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}
	// 					if($result['code'] != ''){
	// 						$allKot[] = array(
	// 							'order_id'			=> $result['order_id'],
	// 							'id'				=> $result['id'],
	// 							'name'           	=> $result['name'],
	// 							'qty'         		=> $result['qty'],
	// 							'rate'         		=> $result['rate'],
	// 							'amt'         		=> $result['amt'],
	// 							'subcategoryid'		=> $result['subcategoryid'],
	// 							'message'         	=> $result['message'],
	// 							'kot_no'            => $result['kot_no'],
	// 							'time_added'        => $result['time'],
	// 						);
	// 						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$flag = 0;
	// 					}
	// 				}
				
	// 				$total_items_normal = 0;
	// 				$total_quantity_normal = 0;
	// 				foreach ($anss_1 as $result) {
	// 					if($result['qty'] > $result['pre_qty']) {
	// 						$tem = $result['qty'] - $result['pre_qty'];
	// 						$ans=abs($tem);
	// 						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
	// 							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
	// 						}

	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 									$qty = (int)$result['qty'];
	// 							} else {
	// 									$qty = $result['qty'];
	// 							}
	// 						} else {
	// 							$qty = $result['qty'];
	// 						}
	// 						$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
	// 							'name'          	=> $result['name'],
	// 							'qty'         		=> $qty,
	// 							'code'         		=> $result['code'],
	// 							'amt'				=> $result['amt'],
	// 							'rate'				=> $result['rate'],
	// 							'message'       	=> $result['message'],
	// 							'subcategoryid'		=> $result['subcategoryid'],
	// 							'kot_no'        	=> $result['kot_no'],
	// 							'time_added'        => $result['time'],
	// 						);
	// 						$total_items_normal ++;
	// 						$total_quantity_normal = $total_quantity_normal + $ans;
	// 						$flag = 0;
	// 					}
	// 				}

	// 				foreach ($anss_2 as $lkey => $result) {
	// 					foreach($anss as $lkeys => $resultss){
	// 						if($lkey == $lkeys) {

	// 						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['code'] = '';
	// 								}
	// 							}
	// 						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
	// 							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
	// 								if($result['parent'] == '0'){
	// 									$result['qty'] = $result['qty'] + $resultss['qty'];
	// 									if($result['nc_kot_status'] == '0'){
	// 										$result['amt'] = $result['qty'] * $result['rate'];
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}

	// 					if($result['code'] != ''){
	// 						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
	// 							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
	// 						}
	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 									$qty = (int)$result['qty'];
	// 							} else {
	// 									$qty = $result['qty'];
	// 							}
	// 						} else {
	// 							$qty = $result['qty'];
	// 						}
	// 						$infos_cancel[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
	// 							'id'				=> $result['id'],
	// 							'name'          	=> $result['name']." (cancel)",
	// 							'qty'         		=> $result['qty'],
	// 							'rate'         		=> $result['rate'],
	// 							'amt'				=> $result['amt'],
	// 							'message'       	=> $result['message'],
	// 							'subcategoryid'		=> $result['subcategoryid'],
	// 							'kot_no'        	=> $result['kot_no'],
	// 							'time_added'        => $result['time'],
	// 						);
	// 						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$flag = 0;
	// 					}
	// 				}

	// 				foreach ($liqurs as $lkey => $liqur) {
	// 					foreach($liqurs as $lkeys => $liqurss){
	// 						if($lkey == $lkeys) {

	// 						} elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
	// 							if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
	// 								if($liqur['parent'] == '0'){
	// 									$liqur['code'] = '';
	// 								}
	// 							}
	// 						} elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
	// 							if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
	// 								if($liqur['parent'] == '0'){
	// 									$liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
	// 									if($liqur['nc_kot_status'] == '0'){
	// 										$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}

	// 					if($liqur['code'] != ''){
	// 						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
	// 							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
	// 						}
	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 									$qty = (int)$result['qty'];
	// 							} else {
	// 									$qty = $result['qty'];
	// 							}
	// 						} else {
	// 							$qty = $result['qty'];
	// 						}

	// 						$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
	// 							'id'				=> $liqur['id'],
	// 							'name'           	=> $liqur['name'],
	// 							'qty'         		=> $qty,
	// 							'amt'				=> $liqur['amt'],
	// 							'rate'				=> $liqur['rate'],
	// 							'message'         	=> $liqur['message'],
	// 							'subcategoryid'		=> $liqur['subcategoryid'],
	// 							'kot_no'            => $liqur['kot_no'],
	// 							'time_added'        => $liqur['time'],
	// 						);
	// 						// echo "<pre>";
	// 						// print_r($liqur['id']);
	// 						$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
	// 						$flag = 0;
	// 					}
	// 				}
	// 				// echo'</br>';
	// 				// print_r($modifierdata);
	// 				// exit;

	// 				foreach ($liqurs_1 as $liqur) {
	// 					if($liqur['qty'] > $liqur['pre_qty']) {
	// 						$tem = $liqur['qty'] - $liqur['pre_qty'];
	// 						$ans=abs($tem);
	// 						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
	// 							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
	// 						}

	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 								$qty = (int)$ans;
	// 							} else {
	// 									$qty = $ans;
	// 							}
	// 						} else {
	// 							$qty = $ans;
	// 						}
	// 						$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
	// 							'name'          	=> $liqur['name'],
	// 							'qty'         		=> $qty,
	// 							'amt'				=> $liqur['amt'],
	// 							'rate'				=> $liqur['rate'],
	// 							'message'       	=> $liqur['message'],
	// 							'subcategoryid'		=> $liqur['subcategoryid'],
	// 							'kot_no'        	=> $liqur['kot_no'],
	// 							'time_added'        => $liqur['time'],
	// 						);
	// 						$total_items_liquor_normal ++;
	// 						$total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
	// 						$flag = 0;
	// 					}
	// 				}

	// 				$total_items_liquor_cancel = 0;
	// 				$total_quantity_liquor_cancel = 0;
	// 				foreach ($liqurs_2 as $lkey => $liqur) {
	// 					foreach($liqurs_2 as $lkeys => $liqurs){
	// 						if($lkey == $lkeys) {

	// 						} elseif($lkey > $lkeys && $liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1'){
	// 							if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
	// 								if($liqur['parent'] == '0'){
	// 									$liqur['code'] = '';
	// 								}
	// 							}
	// 						} elseif ($liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1') {
	// 							if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
	// 								if($liqur['parent'] == '0'){
	// 									$liqur['qty'] = $liqur['qty'] + $liqurs['qty'];
	// 									if($liqur['nc_kot_status'] == '0'){
	// 										$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}

	// 					if($liqur['code'] != ''){
	// 						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
	// 							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
	// 						}

	// 						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 						if($decimal_mesurement->num_rows > 0){
	// 							if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 								$qty = (int)$liqur['qty'];
	// 							} else {
	// 								$qty = $liqur['qty'];
	// 							}
	// 						} else {
	// 							$qty = $liqur['qty'];
	// 						}
	// 						$infos_liqurcancel[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
	// 							'id'      			=> $liqur['id'],
	// 							'name'          	=> $liqur['name']."(cancel)",
	// 							'qty'         		=> $qty,
	// 							'amt'         		=> $liqur['amt'],
	// 							'rate'         		=> $liqur['rate'],
	// 							'message'       	=> $liqur['message'],
	// 							'subcategoryid'		=> $liqur['subcategoryid'],
	// 							'kot_no'        	=> $liqur['kot_no'],
	// 							'time_added'        => $liqur['time'],
	// 						);
	// 						//$text = "Cancelled Bot <br>Orderid :".$order_id.", Item Name :".$result['name'].", Qty :".$result['qty'].", Amt :".$result['amt'].", Kot_no :".$result['kot_no'];
	// 						//echo $text;
	// 						$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
	// 						$total_items_liquor_cancel ++;
	// 						$total_quantity_liquor_cancel = $liqur['qty'];
	// 						$flag = 0;
	// 					}
	// 				}

	// 				if(!isset($flag)){
	// 					$anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
	// 					$total_items = 0;
	// 					$total_quantity = 0;
	// 					foreach ($anss as $result) {
	// 						$infos[] = array(
	// 							'name'           	=> $result['name'],
	// 							'qty'         	 	=> $result['qty'],
	// 							'amt'				=> $result['amt'],
	// 							'rate'				=> $result['rate'],
	// 							'message'        	=> $result['message'],
	// 							'subcategoryid'		=> $result['subcategoryid'],
	// 							'kot_no'        	=> $result['kot_no'],
	// 							'time_added' 		=> $result['time'],
	// 						);
	// 						$total_items ++;
	// 						$total_quantity = $total_quantity + $result['qty'];
	// 					}
	// 				}
	// 				$this->db->query("UPDATE " . DB_PREFIX . "order_items SET `kot_status` = '1', `is_new` = '1' WHERE  order_id = '".$order_id."'");

	// 				$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
	// 				if($locationData->num_rows > 0){
	// 					$locationData = $locationData->row;
	// 					if($locationData['kot_different'] == 1){
	// 						$kot_different = 1;
	// 					} else {
	// 						$kot_different = 0;
	// 					}
	// 					$kot_copy = $locationData['kot_copy'];
	// 					$bill_copy = $locationData['bill_copy'];
	// 					$direct_bill = $locationData['direct_bill'];
	// 					$bill_printer_type = $locationData['bill_printer_type'];
	// 					$bill_printer_name = $locationData['bill_printer_name'];
	// 				} else{
	// 					$kot_different = 0;
	// 					$kot_copy = 1;
	// 					$direct_bill = 0;
	// 					$bill_copy = 1;
	// 					$bill_printer_type = '';
	// 					$bill_printer_name = '';
	// 				}


	// 				$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
	// 				// echo'<pre>';
	// 				// print_r($infos_normal);
	// 				// exit;
	// 				if(( $direct_bill == 0 && $edit == '0') || $kot_copy > 0){
	// 					if($kot_copy > 0 ){
	// 						if($infos_normal){
	// 							foreach($infos_normal as $nkeys => $nvalues){
					
	// 							 	$printtype = '';
	// 							 	$printername = '';
	// 							 	$description = '';
	// 							 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
	// 							 	$printername = $this->user->getPrinterName();
	// 							 	$printertype = $this->user->getPrinterType();
								 	
	// 							 	if($printertype != ''){
	// 							 		$printtype = $printertype;
	// 							 	} else {
	// 							 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
	// 										$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
	// 							 		}
	// 								}

	// 								if ($printername != '') {
	// 									$printname = $printername;
	// 								} else {
	// 									if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
	// 										$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
	// 									}
	// 								}

	// 								if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
	// 									$description = $kot_group_datas[$sub_category_id_compare]['description'];
	// 								}

	// 								$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
	// 								if($printerModel ==0){

	// 									try {
	// 								 		if($printtype == 'Network'){
	// 									 		$connector = new NetworkPrintConnector($printername, 9100);
	// 									 	} elseif($printtype == 'Windows'){
	// 									 		$connector = new WindowsPrintConnector($printername);
	// 									 	} else {
	// 									 		$connector = '';
	// 									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
	// 									 	}
	// 									 	if($connector != ''){
	// 									 		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
	// 									 		if($printerModel == 0){
	// 										 		if($kot_different == 0){
	// 											    	//echo"innnn kot";exit;
	// 												    $printer = new Printer($connector);
	// 												    $printer->selectPrintMode(32);
	// 												   	$printer->setEmphasis(true);
	// 												   	$printer->setTextSize(2, 1);
	// 												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 												    for($i = 1; $i <= $kot_copy; $i++){
	// 												    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
	// 														   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 											    			$printer->feed(1);
	// 											    		}
	// 													    $printer->text($infoss[0]['location']);
	// 													    $printer->feed(1);
	// 													    $printer->text($description);
	// 													    $printer->feed(1);
	// 													    $printer->setTextSize(2, 1);
	// 													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 													    $printer->text("Tbl.No ".$table_id);
	// 													    $printer->feed(1);
	// 													    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 														$printer->setTextSize(1, 1);
	// 													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],25)."K.Ref.No :".$infoss[0]['order_id']);
	// 														$printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													   	$printer->setTextSize(1, 1);
	// 													    $printer->text(str_pad("Date :".date('d/m/y', strtotime($infoss[0]['date_added'])),25)."Time :".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->text("Order From: ".$info_datas['order_from']."");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(false);
	// 													   	$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 												   		$printer->setTextSize(2, 2);
	// 												   		$printer->text("Order No: ".$infoss[0]['wera_order_id']."");
	// 												   		$printer->setTextSize(1, 1);
	// 													    $printer->feed(1);
	// 													    $printer->text("----------------------------------------------");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													    $kot_no_string = '';
	// 													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

	// 													    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
	// 													    	$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 															$total_amount_normal = 0;
	// 													    	foreach($nvalues as $keyss => $nvalue){
	// 														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
															    		
	// 														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
	// 														    	if($nvalue['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 														    	if($modifierdata != array()){
	// 															    	foreach($modifierdata as $key => $value){
	// 														    			$printer->setTextSize(1, 1);
	// 														    			if($key == $nvalue['id']){
	// 														    				foreach($value as $modata){
	// 																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
	// 																	    		$printer->feed(1);
	// 																    		}
	// 																    	}
	// 														    		}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
	// 												    			$kot_no_string .= $nvalue['kot_no'].",";
	// 														    }
	// 												    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
														    	
	// 														    $printer->setTextSize(1, 1);
	// 														    $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')));
	// 												    		$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 													    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
	// 												    		$printer->feed(1);
	// 												    		$printer->setTextSize(2, 1);
	// 												    		$printer->text("G.Total :".$total_g );
	// 												    		$printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 													    } else {
	// 															$printer->text("Qty     Description");
	// 													    	$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 														    foreach($nvalues as $keyss => $valuess){
	// 														    	$printer->setTextSize(2, 1);
	// 													    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
	// 														    	$kot_no_string .= $valuess['kot_no'].",";
	// 													    	}

	// 													    	$printer->setTextSize(1, 1);
	// 													    	$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8'),0,46));
	// 												    		$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
	// 														    $printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 														}
	// 													}
	// 												} else{
	// 													$printer = new Printer($connector);
	// 												    $printer->selectPrintMode(32);
	// 												   	$printer->setEmphasis(true);
	// 												   	$printer->setTextSize(2, 1);
	// 												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 												   	for($i = 1; $i <= $kot_copy; $i++){
	// 												   		//echo'innn2';exit;
	// 													    $printer->text($infoss[0]['location']);
	// 													    $printer->feed(1);
	// 													    $printer->text($description);
	// 													    $printer->feed(1);
	// 													    $printer->setTextSize(2, 1);
	// 													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 													    $printer->text("Tbl.No ".$table_id);
	// 													    $printer->feed(1);
	// 													    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 													    $printer->setTextSize(1, 1);
	// 													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
	// 														$printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													   	$printer->setTextSize(1, 1);
	// 													    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(false);
	// 													   	$printer->setTextSize(1, 1);
	// 													   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->text("----------------------------------------------");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													    $kot_no_string = '';
	// 													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
	// 													    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
	// 														   	$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 													    	$total_amount_normal = 0;
	// 													    	foreach($nvalues as $keyss => $nvalue){
	// 														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
	// 														    	if($nvalue['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 														    	if($modifierdata != array()){
	// 															    	foreach($modifierdata as $key => $value){
	// 														    			$printer->setTextSize(1, 1);
	// 														    			if($key == $nvalue['id']){
	// 														    				foreach($value as $modata){
	// 																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
	// 																	    		$printer->feed(1);
	// 																    		}
	// 																    	}
	// 														    		}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
	// 												    			$kot_no_string .= $nvalue['kot_no'].",";
	// 														    }
	// 													    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

	// 														    $printer->setTextSize(1, 1);
	// 														    $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8') ));
	// 												    		$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 													    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
	// 												    		$printer->feed(1);
	// 												    		$printer->setTextSize(2, 1);
	// 												    		$printer->text("G.Total :".$total_g );
	// 												    		$printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 													    } else {
	// 														    $printer->text("Qty     Description");
	// 														    $printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 														    foreach($nvalues as $keyss => $valuess){
	// 														    	//echo'<pre>';print_r($valuess);exit;
	// 														    	$printer->setTextSize(2, 1);
	// 														    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
	// 														    	$kot_no_string .= $valuess['kot_no'].",";
	// 													    	}
	// 															$printer->setTextSize(1, 1);
	// 															$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 														    $printer->text("----------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
	// 														    $printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 														}
	// 														foreach($nvalues as $keyss => $valuess){
	// 															$valuess['qty'] = round($valuess['qty']);
	// 															for($i = 1; $i <= $valuess['qty']; $i++){
	// 																$total_items_normal = 0;
	// 																$total_quantity_normal = 0;
	// 																$qtydisplay = 1;
	// 																//echo $valuess['qty'];
	// 																$printer = new Printer($connector);
	// 															    $printer->selectPrintMode(32);
	// 															   	$printer->setEmphasis(true);
	// 															   	$printer->setTextSize(2, 1);
	// 															   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 															    $printer->text($infoss[0]['location']);
	// 															    $printer->feed(1);
	// 															    $printer->text($description);
	// 															    $printer->feed(1);
	// 															    $printer->setTextSize(2, 1);
	// 															   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 															    $printer->text("Tbl.No ".$table_id);
	// 															    $printer->feed(1);
	// 															    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 															    $printer->setTextSize(1, 1);
	// 															    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
	// 																$printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															   	$printer->setTextSize(1, 1);
	// 															    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(false);
	// 															   	$printer->setTextSize(1, 1);
	// 															   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
	// 															    $printer->feed(1);
	// 															    $printer->text("----------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															    $printer->text("Qty     Description");
	// 															    $printer->feed(1);
	// 															    $printer->text("----------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(false);
	// 														    	$printer->setTextSize(2, 1);
	// 													    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,30,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 															    $printer->setTextSize(2, 1);
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal ++;
	// 														    	$qtydisplay ++;
	// 														    	$printer->setTextSize(1, 1);
	// 														    	$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 															    $printer->text("----------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
	// 															    $printer->feed(2);
	// 															    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 															    $printer->cut();
	// 																$printer->feed(2);
	// 															}
	// 														}
	// 													}
	// 												}
	// 											} else {
													
	// 											}
	// 											// Close printer //
	// 										    $printer->close();
	// 										    $kot_no_string = rtrim($kot_no_string, ',');
	// 											$this->db->query("UPDATE oc_order_items SET printstatus = 2,is_new = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
	// 										}
	// 									} catch (Exception $e) {
	// 									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
	// 									    if(isset($kot_no_string)){
	// 										    $kot_no_string = rtrim($kot_no_string, ',');
	// 										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
	// 										} else {
	// 											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 										}
	// 									    $this->session->data['warning'] = $printername." "."Not Working";
	// 										//continue;
	// 									}
	// 								} else {  // 45 space code starts from here

	// 									try {
	// 								 		if($printtype == 'Network'){
	// 									 		$connector = new NetworkPrintConnector($printername, 9100);
	// 									 	} elseif($printtype == 'Windows'){
	// 									 		$connector = new WindowsPrintConnector($printername);
	// 									 	} else {
	// 									 		$connector = '';
	// 									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
	// 									 	}
	// 									 	if($connector != ''){
	// 										 		if($kot_different == 0){
	// 											    	//echo"innnn kot";exit;
	// 												    $printer = new Printer($connector);
	// 												    $printer->selectPrintMode(32);
	// 												   	$printer->setEmphasis(true);
	// 												   	$printer->setTextSize(2, 1);
	// 												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 												   	// $printer->text('45 Spacess');
	// 							    					$printer->feed(1);
	// 												    for($i = 1; $i <= $kot_copy; $i++){
	// 												    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
	// 														   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 											    			$printer->feed(1);
	// 											    		}
	// 													    $printer->text($infoss[0]['location']);
	// 													    $printer->feed(1);
	// 													    $printer->text($description);
	// 													    $printer->feed(1);
	// 													    $printer->setTextSize(2, 1);
	// 													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 													    $printer->text("Tbl.No ".$table_id);
	// 													    $printer->feed(1);
	// 													    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 														$printer->setTextSize(1, 1);
	// 													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
	// 														$printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													   	$printer->setTextSize(1, 1);
	// 													    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->text("Order From: ".$info_datas['order_from']."");
	// 													    $printer->setEmphasis(false);
	// 													   	$printer->setTextSize(1, 1);
	// 													   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->text("------------------------------------------");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													    $kot_no_string = '';
	// 													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

	// 													    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
	// 													    	$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 															$total_amount_normal = 0;
	// 													    	foreach($nvalues as $keyss => $nvalue){
	// 														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
															    		
	// 														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
	// 														    	if($nvalue['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 														    	if($modifierdata != array()){
	// 															    	foreach($modifierdata as $key => $value){
	// 														    			$printer->setTextSize(1, 1);
	// 														    			if($key == $nvalue['id']){
	// 														    				foreach($value as $modata){
	// 																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 																	    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
	// 																	    		$printer->feed(1);
	// 																    		}
	// 																    	}
	// 														    		}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
	// 												    			$kot_no_string .= $nvalue['kot_no'].",";
	// 														    }
	// 												    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
														    
	// 														    $printer->setTextSize(1, 1);
	// 														    $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 													    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
	// 												    		$printer->feed(1);
	// 												    		$printer->setTextSize(2, 1);
	// 												    		$printer->text("G.Total :".$total_g );
	// 												    		$printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 													    } else {
	// 															$printer->text("Qty     Description");
	// 													    	$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 														    foreach($nvalues as $keyss => $valuess){
	// 														    	$printer->setTextSize(2, 1);
	// 													    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
	// 														    	$kot_no_string .= $valuess['kot_no'].",";
	// 													    	}

	// 													    	$printer->setTextSize(1, 1);
	// 													    	$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
	// 														    $printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 														}
	// 													}
	// 												} else{
	// 													$printer = new Printer($connector);
	// 												    $printer->selectPrintMode(32);
	// 												   	$printer->setEmphasis(true);
	// 												   	$printer->setTextSize(2, 1);
	// 												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 												   	for($i = 1; $i <= $kot_copy; $i++){
	// 												   		//echo'innn2';exit;
	// 													    $printer->text($infoss[0]['location']);
	// 													    $printer->feed(1);
	// 													    $printer->text($description);
	// 													    $printer->feed(1);
	// 													    $printer->setTextSize(2, 1);
	// 													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 													    $printer->text("Tbl.No ".$table_id);
	// 													    $printer->feed(1);
	// 													    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 													    $printer->setTextSize(1, 1);
	// 													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
	// 														$printer->feed(1);
	// 														$printer->text("Order From: ".$info_datas['order_from']."");
	// 														$printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 														$printer->feed(1);
	// 													   	$printer->setTextSize(1, 1);
	// 													    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(false);
	// 													   	$printer->setTextSize(1, 1);
	// 													   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
	// 													    $printer->feed(1);
	// 													    $printer->text("------------------------------------------");
	// 													    $printer->feed(1);
	// 													    $printer->setEmphasis(true);
	// 													    $kot_no_string = '';
	// 													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
	// 													    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
	// 														   	$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 													    	$total_amount_normal = 0;
	// 													    	foreach($nvalues as $keyss => $nvalue){
	// 														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 														    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
	// 														    	if($nvalue['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 														    	if($modifierdata != array()){
	// 															    	foreach($modifierdata as $key => $value){
	// 														    			$printer->setTextSize(1, 1);
	// 														    			if($key == $nvalue['id']){
	// 														    				foreach($value as $modata){
	// 																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
	// 																	    		$printer->feed(1);
	// 																    		}
	// 																    	}
	// 														    		}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
	// 												    			$kot_no_string .= $nvalue['kot_no'].",";
	// 														    }
	// 													    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

	// 														    $printer->setTextSize(1, 1);
	// 														    $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 													    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
	// 												    		$printer->feed(1);
	// 												    		$printer->setTextSize(2, 1);
	// 												    		$printer->text("G.Total :".$total_g );
	// 												    		$printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 													    } else {
	// 														    $printer->text("Qty     Description");
	// 														    $printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(false);
	// 														    $total_items_normal = 0;
	// 															$total_quantity_normal = 0;
	// 														    foreach($nvalues as $keyss => $valuess){
	// 														    	//echo'<pre>';print_r($valuess);exit;
	// 														    	$printer->setTextSize(2, 1);
	// 														    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->setTextSize(1, 1);
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
	// 														    	$kot_no_string .= $valuess['kot_no'].",";
	// 													    	}
	// 															$printer->setTextSize(1, 1);
	// 															$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 														    $printer->text("------------------------------------------");
	// 														    $printer->feed(1);
	// 														    $printer->setEmphasis(true);
	// 														    $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
	// 														    $printer->feed(2);
	// 														    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 														    $printer->cut();
	// 															$printer->feed(2);
	// 														}
	// 														foreach($nvalues as $keyss => $valuess){
	// 															$valuess['qty'] = round($valuess['qty']);
	// 															for($i = 1; $i <= $valuess['qty']; $i++){
	// 																$total_items_normal = 0;
	// 																$total_quantity_normal = 0;
	// 																$qtydisplay = 1;
	// 																//echo $valuess['qty'];
	// 																$printer = new Printer($connector);
	// 															    $printer->selectPrintMode(32);
	// 															   	$printer->setEmphasis(true);
	// 															   	$printer->setTextSize(2, 1);
	// 															   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 															    $printer->text($infoss[0]['location']);
	// 															    $printer->feed(1);
	// 															    $printer->text($description);
	// 															    $printer->feed(1);
	// 															    $printer->setTextSize(2, 1);
	// 															   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
	// 															    $printer->text("Tbl.No ".$table_id);
	// 															    $printer->feed(1);
	// 															    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 															    $printer->setTextSize(1, 1);
	// 															    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
	// 																$printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															   	$printer->setTextSize(1, 1);
	// 															   $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(false);
	// 															   	$printer->setTextSize(1, 1);
	// 															   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
	// 															    $printer->feed(1);
	// 															    $printer->text("------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															    $printer->text("Qty     Description");
	// 															    $printer->feed(1);
	// 															    $printer->text("------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(false);
	// 														    	$printer->setTextSize(2, 1);
	// 													    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,20,"\n"));
	// 														    	if($valuess['message'] != ''){
	// 														    		$printer->feed(1);
	// 														    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
	// 														    	}
	// 														    	$printer->feed(1);
	// 													    		foreach($modifierdata as $key => $value){
	// 													    			$printer->setTextSize(1, 1);
	// 													    			if($key == $valuess['id']){
	// 													    				foreach($value as $modata){
	// 																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
	// 																    		$printer->feed(1);
	// 															    		}
	// 															    	}
	// 														    	}
	// 															    $printer->setTextSize(2, 1);
	// 														    	$total_items_normal ++ ;
	// 														    	$total_quantity_normal ++;
	// 														    	$qtydisplay ++;
	// 														    	$printer->setTextSize(1, 1);
	// 														    	$printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['order_instructions'], ENT_QUOTES, 'UTF-8')) );
	// 												    		$printer->feed(1);
	// 															    $printer->text("------------------------------------------");
	// 															    $printer->feed(1);
	// 															    $printer->setEmphasis(true);
	// 															    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
	// 															    $printer->feed(2);
	// 															    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 															    $printer->cut();
	// 																$printer->feed(2);
	// 															}
	// 														}
	// 													}
	// 												}
	// 											// Close printer //
	// 										    $printer->close();
	// 										    $kot_no_string = rtrim($kot_no_string, ',');
	// 											$this->db->query("UPDATE oc_order_items SET printstatus = 2,is_new = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
	// 										}
	// 									} catch (Exception $e) {
	// 									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
	// 									    if(isset($kot_no_string)){
	// 										    $kot_no_string = rtrim($kot_no_string, ',');
	// 										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
	// 										} else {
	// 											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 										}
	// 									    $this->session->data['warning'] = $printername." "."Not Working";
	// 										//continue;
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}
	// 				}else {
	// 					$json = array();
	// 					$json = array(
	// 						'status' => 0,
	// 					);

	// 					$this->response->addHeader('Content-Type: application/json');
	// 					$this->response->setOutput(json_encode($json));	
	// 				}


	// 				if(($infoss[0]['bill_status'] == 0 && $edit == '0') ){
	// 					if($direct_bill == 0 ){
							
	// 						$merge_datas = array();
	// 						$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 						if($edit == '0'){
	// 							$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 							$last_open_dates = $this->db->query($last_open_date_sql);
	// 							if($last_open_dates->num_rows > 0){
	// 								$last_open_date = $last_open_dates->row['bill_date'];
	// 							} else {
	// 								$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 								$last_open_dates = $this->db->query($last_open_date_sql);
	// 								if($last_open_dates->num_rows > 0){
	// 									$last_open_date = $last_open_dates->row['bill_date'];
	// 								} else {
	// 									$last_open_date = date('Y-m-d');
	// 								}
	// 							}

	// 							$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 							$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
	// 							if($last_open_dates_liq->num_rows > 0){
	// 								$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 							} else {
	// 								$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 								$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
	// 								if($last_open_dates_liq->num_rows > 0){
	// 									$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 								} else {
	// 									$last_open_date_liq = date('Y-m-d');
	// 								}
	// 							}

	// 							$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
	// 							$last_open_dates_order = $this->db->query($last_open_date_sql_order);
	// 							if($last_open_dates_order->num_rows > 0){
	// 								$last_open_date_order = $last_open_dates_order->row['bill_date'];
	// 							} else {
	// 								$last_open_date_order = date('Y-m-d');
	// 							}
								
	// 							$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
								
	// 							if($ordernogenrated['order_no'] == '0'){
	// 								$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
	// 								$orderno = 1;
	// 								if(isset($orderno_q['order_no'])){
	// 									$orderno = $orderno_q['order_no'] + 1;
	// 								}
									
	// 								$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 								if($kotno2->num_rows > 0){
	// 									$kot_no2 = $kotno2->row['billno'];
	// 									$kotno = $kot_no2 + 1;
	// 								} else{
	// 									$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 									if($kotno2->num_rows > 0){
	// 										$kot_no2 = $kotno2->row['billno'];
	// 										$kotno = $kot_no2 + 1;
	// 									} else {
	// 										$kotno = 1;
	// 									}
	// 								}

	// 								$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 								if($kotno1->num_rows > 0){
	// 									$kot_no1 = $kotno1->row['billno'];
	// 									$botno = $kot_no1 + 1;
	// 								} else{
	// 									$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 									if($kotno1->num_rows > 0){
	// 										$kot_no1 = $kotno1->row['billno'];
	// 										$botno = $kot_no1 + 1;
	// 									}else {
	// 										$botno = 1;
	// 									}
	// 								}

	// 								$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
	// 								$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

	// 								// echo'<pre>';
	// 								// print_r($this->session->data);
	// 								// exit;
	// 								if(isset($this->session->data['cash'])){
	// 									$cashh = $this->session->data['cash'];
	// 								} else{
	// 									$cashh = '';
	// 								}

	// 								if(isset($this->session->data['credit'])){
	// 									$creditt = $this->session->data['credit'];
	// 								} else{
	// 									$creditt = '';
	// 								}

	// 								if(isset($this->session->data['online'])){
	// 									$onlinee = $this->session->data['online'];
	// 								} else{
	// 									$onlinee = '';
	// 								}

	// 								if(isset($this->session->data['onac'])){
	// 									$onacc = $this->session->data['onac'];
	// 								} else{
	// 									$onacc = '';
	// 								}

	// 								if(isset($this->session->data['payment_type'])){
	// 									$payment_type = $this->session->data['payment_type'];
	// 									if($payment_type==0){
	// 										$payment_type = '';
	// 									}
	// 								} else{
	// 									$payment_type = '';
	// 								}

	// 								if($cashh != '0' && $cashh != ''){
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."', payment_status = '1', total_payment = '".$cashh."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

	// 								}elseif( $creditt != '0' && $creditt != ''){
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."', payment_status = '1', total_payment = '".$creditt."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
	// 								}elseif( $onlinee != '0' && $onlinee != ''){
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$onlinee."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
										
	// 								}elseif( $onacc != '0' && $onacc != ''){
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."',onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$this->session->data['onac']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
										
	// 								} elseif($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
	// 								} else{
	// 									$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
	// 								}
	// 								$ordernogenrated['order_no'] = $orderno;
	// 								$this->db->query($update_sql);
	// 								$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 								if($apporder['app_order_id'] != '0'){
	// 									$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
	// 								}
	// 							} 
	// 							if($ordernogenrated['order_no'] != '0'){
	// 								$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
	// 							}
	// 						}

	// 						$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
	// 						$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
	// 						foreach($tests as $test){
	// 							$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
	// 							$testfoods[] = array(
	// 								'tax1' => $test['tax1'],
	// 								'amt' => $amt,
	// 								'tax1_value' => $test['tax1_value']
	// 							);
	// 						}
							
	// 						$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
	// 						foreach($testss as $testa){
	// 							$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
	// 							$testliqs[] = array(
	// 								'tax1' => $testa['tax1'],
	// 								'amt' => $amts,
	// 								'tax1_value' => $testa['tax1_value']
	// 							);
	// 						}
	// 						$infoslb = array();
	// 						$infosb = array();
	// 						$flag = 0;
	// 						$totalquantityfood = 0;
	// 						$totalquantityliq = 0;
	// 						$disamtfood = 0;
	// 						$disamtliq = 0;
	// 						$modifierdatabill = array();
	// 						foreach ($anssb as $lkey => $result) {
	// 							foreach($anssb as $lkeys => $results){
	// 								if($lkey == $lkeys) {

	// 								} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
	// 									if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 										if($result['parent'] == '0'){
	// 											$result['code'] = '';
	// 										}
	// 									}
	// 								} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
	// 									if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 										if($result['parent'] == '0'){
	// 											$result['qty'] = $result['qty'] + $results['qty'];
	// 											if($result['nc_kot_status'] == '0'){
	// 												$result['amt'] = $result['qty'] * $result['rate'];
	// 											}
	// 										}
	// 									}
	// 								}
	// 							}
	// 							if($result['code'] != ''){
	// 								$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 									if($decimal_mesurement->num_rows > 0){
	// 										if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 												$qty = (int)$result['qty'];
	// 										} else {
	// 												$qty = $result['qty'];
	// 										}
	// 									} else {
	// 										$qty = $result['qty'];
	// 									}
	// 								if($result['is_liq']== 0){
	// 									$infosb[] = array(
	// 										'billno'		=> $result['billno'],
	// 										'id'			=> $result['id'],
	// 										'name'          => $result['name'],
	// 										'rate'          => $result['rate'],
	// 										'amt'           => $result['amt'],
	// 										'qty'         	=> $qty,
	// 										'tax1'         	=> $result['tax1'],
	// 										'tax2'          => $result['tax2']
	// 									);
	// 									$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 									$totalquantityfood = $totalquantityfood + $result['qty'];
	// 									$disamtfood = $disamtfood + $result['discount_value'];
	// 								} else {
	// 									$flag = 1;
	// 									$infoslb[] = array(
	// 										'billno'		=> $result['billno'],
	// 										'id'			=> $result['id'],
	// 										'name'          => $result['name'],
	// 										'rate'          => $result['rate'],
	// 										'amt'           => $result['amt'],
	// 										'qty'         	=> $qty,
	// 										'tax1'         	=> $result['tax1'],
	// 										'tax2'          => $result['tax2']
	// 									);
	// 									$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 									$totalquantityliq = $totalquantityliq + $result['qty'];
	// 									$disamtliq = $disamtliq + $result['discount_value'];
	// 								}
	// 							}
	// 						}

	// 						if($ansb['parcel_status'] == '0'){
	// 							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 								$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
	// 							} else{
	// 								$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
	// 							}
	// 						} else {
	// 							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 								$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
	// 							} else{
	// 								$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
	// 							}
	// 						}

	// 						$csgst=$ansb['gst'] / 2;
	// 						$csgsttotal = $ansb['gst'];

	// 						$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 						$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
	// 						$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 						$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 						$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
	// 						$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 						$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
	// 						if($ansb['advance_amount'] == '0.00'){
	// 							$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						} else{
	// 							$gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 						}
	// 						//$gtotal = ceil($gtotal);
	// 						$gtotal = round($gtotal);


	// 						if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
	// 							$printtype = $bill_printer_type;
	// 				 			$printername = $bill_printer_name;
	// 							if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
	// 								$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
	// 							 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
	// 							}
				
	// 							$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
	// 							if($printerModel ==0){
	// 								try {
	// 							    	if($printtype == 'Network'){
	// 								 		$connector = new NetworkPrintConnector($printername, 9100);
	// 								 	} else if($printtype == 'Windows'){
	// 								 		$connector = new WindowsPrintConnector($printername);
	// 								 	} else {
	// 								 		$connector = '';
	// 								 	}
	// 								    if($connector != ''){
	// 									    $printer = new Printer($connector);
	// 									    $printer->selectPrintMode(32);

	// 									   	$printer->setEmphasis(true);
	// 									   	$printer->setTextSize(2, 1);
	// 									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									    $printer->feed(1);
	// 									   	//$printer->setFont(Printer::FONT_B);
	// 										for($i = 1; $i <= $bill_copy; $i++){
	// 										    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 										    $printer->feed(1);
	// 										    $printer->setTextSize(1, 1);
	// 										    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 										    //$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 										    //$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->setEmphasis(true);
	// 										   	$printer->setTextSize(1, 1);
	// 										   	$printer->feed(1);
	// 										    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
											
	// 									}
	// 									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ansb['cust_name']));
	// 										$printer->feed(1);
	// 										$printer->text(("Mobile :".$ansb['cust_contact']));
	// 										$printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
	// 									 	$printer->text(("Address : ".$ansb['cust_address']));
	// 										$printer->feed(1);
	// 										$printer->text("Gst No :".$ansb['gst_no']);
	// 										$printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ansb['cust_name']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ansb['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
	// 									 	$printer->text(("Mobile :".$ansb['cust_contact']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ansb['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
	// 									 	$printer->text("Name : ".$ansb['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ansb['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
	// 									 	$printer->text("Mobile :".$ansb['cust_contact']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ansb['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 									    $printer->text("Name :".$ansb['cust_name']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 									    $printer->text("Mobile :".$ansb['cust_contact']."");
	// 									    $printer->feed(1);
	// 									}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
	// 									    $printer->text("Address : ".$ansb['cust_address']."");
	// 									    $printer->feed(1);
	// 									}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
	// 									    $printer->text("Gst No :".$ansb['gst_no']."");
	// 									    $printer->feed(1);
	// 									}else{
	// 									    $printer->text("Name : ".$ansb['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Mobile :".$ansb['cust_contact']."");
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ansb['cust_address']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ansb['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 											$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
	// 											$printer->setEmphasis(true);
	// 										   	$printer->setTextSize(1, 1);
	// 										   	if($infosb){
	// 										   		$printer->feed(1);
	// 										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
	// 										   		//$printer->feed(1);
	// 										   		/*$printer->text("Order From: ".$info_datas['order_from']."");
	// 										   		$printer->feed(1);
	// 										   		$printer->setTextSize(1, 1);
	// 										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    	//$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
	// 										    	$printer->setTextSize(2, 2);
	// 										   		$printer->text("Order No: ".$infoss[0]['wera_order_id']."");*/
	// 										   		$printer->setTextSize(1, 1);
	// 										    	$printer->feed(1);
	// 										   		//$printer->setTextSize(1, 1);
	// 										   		//$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
	// 											    //$printer->feed(1);
	// 										   		$printer->setEmphasis(false);
	// 											    $printer->text("----------------------------------------------");
	// 												$printer->feed(1);
	// 												$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 												$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 												$printer->feed(1);
	// 										   	 	$printer->text("----------------------------------------------");
	// 												$printer->feed(1);
	// 												$printer->setEmphasis(false);
	// 												$total_items_normal = 0;
	// 												$total_quantity_normal = 0;
	// 											    foreach($infosb as $nkey => $nvalue){
	// 											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 											    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
	// 											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 											    	$printer->feed(1);
	// 											    	if($modifierdatabill != array()){
	// 												    	foreach($modifierdatabill as $key => $value){
	// 											    			$printer->setTextSize(1, 1);
	// 											    			if($key == $nvalue['id']){
	// 											    				foreach($value as $modata){
	// 														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 														    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 														    		$printer->feed(1);
	// 													    		}
	// 													    	}
	// 											    		}
	// 											    	}
	// 											    	$total_items_normal ++ ;
	// 								    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 								    			}
	// 											    $printer->text("----------------------------------------------");
	// 											    $printer->feed(1);
	// 											    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
	// 												$printer->feed(1);
	// 											    $printer->setEmphasis(false);
	// 											   	$printer->setTextSize(1, 1);
	// 											    $printer->text("----------------------------------------------");
	// 												$printer->feed(1);
	// 												/*foreach($testfoods as $tkey => $tvalue){
	// 													$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
	// 											    	$printer->feed(1);
	// 												}
	// 												$printer->text("----------------------------------------------");
	// 												*/	
	// 												$printer->feed(1);
	// 												$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 												if($ansb['fdiscountper'] != '0'){
	// 													$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
	// 													$printer->feed(1);
	// 												} elseif($ansb['discount'] != '0'){
	// 													$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
	// 													$printer->feed(1);
	// 												}
	// 												$printer->text(str_pad("",35)."SCGST :".$csgst."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",35)."CCGST :".$csgst."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",30)."Packaging :".$info_datas['packaging']."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",25)."Packaging CGST :".$info_datas['packaging_cgst']."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",25)."Packaging SGST :".$info_datas['packaging_sgst']."");
	// 												$printer->feed(1);
													
	// 												$printer->setEmphasis(true);
	// 												$printer->text(str_pad("",25)."Net total :".($ansb['grand_total'])."");
	// 												$printer->setEmphasis(false);
													

	// 											}
	// 											$printer->feed(1);
	// 											$printer->text("----------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											$printer->setEmphasis(true);
	// 											$printer->setTextSize(2, 2);
	// 											$printer->setJustification(Printer::JUSTIFY_RIGHT);
	// 											$printer->text("GRAND TOTAL  :  ".round($ansb['grand_total']));
	// 											$printer->setTextSize(1, 1);
	// 											$printer->feed(1);

												
	// 											$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 											// echo $SETTLEMENT_status;
	// 											// exit;
	// 											if($SETTLEMENT_status == '1'){
	// 												if(isset($this->session->data['credit'])){
	// 													$credit = $this->session->data['credit'];
	// 												} else {
	// 													$credit = '0';
	// 												}
	// 												if(isset($this->session->data['cash'])){
	// 													$cash = $this->session->data['cash'];
	// 												} else {
	// 													$cash = '0';
	// 												}
	// 												if(isset($this->session->data['online'])){
	// 													$online = $this->session->data['online'];
	// 												} else {
	// 													$online ='0';
	// 												}

	// 												if(isset($this->session->data['onac'])){
	// 													$onac = $this->session->data['onac'];
	// 													$onaccontact = $this->session->data['onaccontact'];
	// 													$onacname = $this->session->data['onacname'];

	// 												} else {
	// 													$onac ='0';
	// 												}
	// 											}
	// 											if($SETTLEMENT_status=='1'){
	// 												if($credit!='0' && $credit!=''){
	// 													$printer->text("PAY BY: CARD");
	// 												}
	// 												if($online!='0' && $online!=''){
	// 													$printer->text("PAY BY: ONLINE");
	// 												}
	// 												if($cash!='0' && $cash!=''){
	// 													$printer->text("PAY BY: CASH");
	// 												}
	// 												if($onac!='0' && $onac!=''){
	// 													$printer->text("PAY BY: ON.ACCOUNT");
	// 													$printer->feed(1);
	// 													$printer->text("Name: ".$onacname."");
	// 													$printer->feed(1);
	// 													$printer->text("Contact: ".$onaccontact."");
	// 												}
	// 											}
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->text("----------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											$printer->setEmphasis(false);
	// 										   	$printer->setTextSize(1, 1);
												
	// 											$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 											/*$printer->feed(1);
	// 								   			$printer->setTextSize(2, 2);

	// 											$printer->text("Order OTP :".($info_datas['order_otp'] ));*/
	// 											$printer->feed(1);
	// 								   			$printer->setTextSize(1, 1);

	// 											if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 												$printer->feed(1);
	// 											}
	// 											if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 												$printer->feed(1);
	// 											}
	// 											$printer->text("----------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 											if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 											}
	// 									   		$printer->feed(1);
	// 									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    	$printer->setTextSize(2, 2);
	// 											$printer->text("Order From: ".$info_datas['order_from']."");
	// 											$printer->feed(1);
	// 									   		$printer->text("Order No: ".$infoss[0]['wera_order_id']."");
	// 									   		$printer->feed(1);
	// 											$printer->text("Order OTP :".($info_datas['order_otp'] ));

	// 											$printer->feed(2);
	// 											$printer->cut();
	// 										    // Close printer //
	// 										}
	// 									    $printer->close();
	// 									    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 									}
	// 								} catch (Exception $e) {
	// 								    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 									$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
	// 								}
									
	// 							} else {  // 45 space code starts from here

	// 								try {
	// 							    	if($printtype == 'Network'){
	// 								 		$connector = new NetworkPrintConnector($printername, 9100);
	// 								 	} else if($printtype == 'Windows'){
	// 								 		$connector = new WindowsPrintConnector($printername);
	// 								 	} else {
	// 								 		$connector = '';
	// 								 	}
	// 								    if($connector != ''){
	// 									    $printer = new Printer($connector);
	// 									    $printer->selectPrintMode(32);

	// 									   	$printer->setEmphasis(true);
	// 									   	$printer->setTextSize(2, 1);
	// 									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									    $printer->feed(1);
	// 									    // $printer->text('45 Spacess');
	// 						    			$printer->feed(1);
	// 									   	//$printer->setFont(Printer::FONT_B);
	// 										for($i = 1; $i <= $bill_copy; $i++){
	// 										    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 										    $printer->feed(1);
	// 										    $printer->setTextSize(1, 1);
	// 										    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 										    //$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 										    //$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->setEmphasis(true);
	// 										   	$printer->setTextSize(1, 1);
	// 										   	$printer->feed(1);
	// 										    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
													
	// 											}
	// 											else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 											 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
	// 												$printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
	// 											 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
	// 												$printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
	// 											 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
	// 											    $printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
	// 											 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
	// 											    $printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
	// 											 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
	// 											    $printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
	// 											 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
	// 											    $printer->feed(1);
	// 											}
	// 											else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 											    $printer->text("Name :".$ansb['cust_name']."");
	// 											    $printer->feed(1);
	// 											}
	// 											else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
	// 											    $printer->text("Mobile :".$ansb['cust_contact']."");
	// 											    $printer->feed(1);
	// 											}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
	// 											    $printer->text("Address : ".$ansb['cust_address']."");
	// 											    $printer->feed(1);
	// 											}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
	// 											    $printer->text("Gst No :".$ansb['gst_no']."");
	// 											    $printer->feed(1);
	// 											}else{
	// 											    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
	// 											    $printer->feed(1);
	// 											    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
	// 											    $printer->feed(1);
	// 											}
	// 											$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
	// 											$printer->setEmphasis(true);
	// 										   	$printer->setTextSize(1, 1);
	// 										   	if($infosb){
	// 										   		$printer->feed(1);
	// 										   		$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 										   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
	// 										   		//$printer->setTextSize(1, 1);
	// 										   		$printer->feed(1);
	// 										   		/*$printer->text("Order From: ".$info_datas['order_from']."");
	// 										   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    	$printer->setTextSize(2, 2);
	// 										   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 										   		$printer->feed(1);
	// 										   		$printer->setTextSize(1, 1);*/
											   	
	// 										   		$printer->setEmphasis(false);
	// 											    $printer->text("------------------------------------------");
	// 												$printer->feed(1);
	// 												$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 												$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 												$printer->feed(1);
	// 										   	 	$printer->text("------------------------------------------");
	// 												$printer->feed(1);
	// 												$printer->setEmphasis(false);
	// 												$total_items_normal = 0;
	// 												$total_quantity_normal = 0;
	// 											    foreach($infosb as $nkey => $nvalue){
	// 											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 											    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
	// 											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 											    	$printer->feed(1);
	// 											    	if($modifierdatabill != array()){
	// 												    	foreach($modifierdatabill as $key => $value){
	// 											    			$printer->setTextSize(1, 1);
	// 											    			if($key == $nvalue['id']){
	// 											    				foreach($value as $modata){
	// 														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 														    		$printer->feed(1);
	// 													    		}
	// 													    	}
	// 											    		}
	// 											    	}
	// 											    	$total_items_normal ++ ;
	// 								    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	// 								    			}
	// 											    $printer->text("------------------------------------------");
	// 											    $printer->feed(1);
	// 											    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
	// 												$printer->feed(1);
	// 											    $printer->setEmphasis(false);
	// 											   	$printer->setTextSize(1, 1);
	// 											    $printer->text("------------------------------------------");
	// 												$printer->feed(1);
													
	// 												$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 												if($ansb['fdiscountper'] != '0'){
	// 													$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
	// 													$printer->feed(1);
	// 												} elseif($ansb['discount'] != '0'){
	// 													$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
	// 													$printer->feed(1);
	// 												}
	// 												$printer->text(str_pad("",25)."SCGST :".$csgst."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",25)."CCGST :".$csgst."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",20)."Packaging :".$info_datas['packaging']."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",20)."Packaging CGST :".$info_datas['packaging_cgst']."");
	// 												$printer->feed(1);
	// 												$printer->text(str_pad("",20)."Packaging SGST :".$info_datas['packaging_sgst']."");
	// 												$printer->feed(1);
													
	// 												$printer->setEmphasis(true);
	// 												$printer->text(str_pad("",25)."Net total :".($ansb['grand_total'])."");
	// 												$printer->setEmphasis(false);	

	// 											}
												
	// 											$printer->feed(1);
	// 											$printer->text("------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setEmphasis(true);
	// 											$printer->setTextSize(2, 2);
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											$printer->text("GRAND TOTAL : ".round($ansb['grand_total']));
	// 											$printer->setTextSize(1, 1);
	// 											$printer->feed(1);
	// 											if($ansb['dtotalvalue']!=0){
	// 												$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
	// 												$printer->feed(1);
	// 											}
	// 											$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 											if($SETTLEMENT_status == '1'){
	// 												if(isset($this->session->data['credit'])){
	// 													$credit = $this->session->data['credit'];
	// 												} else {
	// 													$credit = '0';
	// 												}
	// 												if(isset($this->session->data['cash'])){
	// 													$cash = $this->session->data['cash'];
	// 												} else {
	// 													$cash = '0';
	// 												}
	// 												if(isset($this->session->data['online'])){
	// 													$online = $this->session->data['online'];
	// 												} else {
	// 													$online ='0';
	// 												}

	// 												if(isset($this->session->data['onac'])){
	// 													$onac = $this->session->data['onac'];
	// 													$onaccontact = $this->session->data['onaccontact'];
	// 													$onacname = $this->session->data['onacname'];

	// 												} else {
	// 													$onac ='0';
	// 												}
	// 											}
	// 											if($SETTLEMENT_status=='1'){
	// 												if($credit!='0' && $credit!=''){
	// 													$printer->text("PAY BY: CARD");
	// 												}
	// 												if($online!='0' && $online!=''){
	// 													$printer->text("PAY BY: ONLINE");
	// 												}
	// 												if($cash!='0' && $cash!=''){
	// 													$printer->text("PAY BY: CASH");
	// 												}
	// 												if($onac!='0' && $onac!=''){
	// 													$printer->text("PAY BY: ON.ACCOUNT");
	// 													$printer->feed(1);
	// 													$printer->text("Name: '".$onacname."'");
	// 													$printer->feed(1);
	// 													$printer->text("Contact: '".$onaccontact."'");
	// 												}
	// 											}
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										    $printer->text("------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 											$printer->setEmphasis(false);
	// 										   	$printer->setTextSize(1, 1);
												
	// 											$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 											/*$printer->feed(1);
	// 								   			$printer->setTextSize(2, 2);

	// 											$printer->text("Order OTP :".($info_datas['order_otp']) );
	// 											$printer->feed(1);*/
	// 								   			//$printer->setTextSize(1, 1);
	// 											$printer->feed(1);
	// 											if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 												$printer->feed(1);
	// 											}
	// 											if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 												$printer->feed(1);
	// 											}
	// 											$printer->text("------------------------------------------");
	// 											$printer->feed(1);
	// 											$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 											if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 												$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 											}

	// 											$printer->feed(1);
	// 									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    	$printer->setTextSize(2, 2);
	// 											$printer->text("Order From: ".$info_datas['order_from']."");
	// 											$printer->feed(1);
	// 									   		$printer->text("Order No: ".$infoss[0]['wera_order_id']."");
	// 									   		$printer->feed(1);
	// 											$printer->text("Order OTP :".($info_datas['order_otp'] ));

	// 											$printer->feed(2);
	// 											$printer->cut();
	// 										    // Close printer //
	// 										}
	// 									    $printer->close();

	// 									    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 									}
	// 								} catch (Exception $e) {
	// 								    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 									$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
	// 								}
	// 							}
	// 						}
	// 						$json = array();
	// 						$json = array(
								
	// 							'status' => 1,
								
	// 						);
	// 						$this->response->addHeader('Content-Type: application/json');
	// 						$this->response->setOutput(json_encode($json));
	// 					}
	// 				}

	// 				if($reject_reason != ''){
	// 					$this->db->query("UPDATE oc_werafood_orders SET status = '".$reject_reason."' WHERE order_id = '".$orderid."'");
	// 					// echo'inn';
	// 					// exit;
	// 				}
	// 				$json['done'] = '<script>parent.closeIFrame();</script>';
	// 				$json['info'] = 1;
	// 				$this->response->setOutput(json_encode($json));
	// 			} elseif($datas['code'] == 2) {
	// 				$json['info'] = 2;
	// 				$json['reason'] = $datas['msg'];
	// 				//$json['done'] = '<script>parent.closeIFrame();</script>';
	// 			} else {
	// 				$json['done'] = '<script>parent.closeIFrame();</script>';
	// 				$json['info'] = 0;
	// 				$this->response->setOutput(json_encode($json));
	// 			}
	// 		}
	// 	} 
	// }

	public function update_order() {
        $json = array();
        if(isset($this->request->get['order_id'])){
        	//sleep(5);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt_array($curl, array(
               // CURLOPT_URL => 'https://pos-int.urbanpiper.com/external/api/v1/orders/'.$this->request->get['order_id'].'/status/',
                CURLOPT_URL => sprintf(ORDER_UPDATE_API,$this->request->get['order_id']),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS =>'{
                    "new_status": "Acknowledged",
                    "message": "Order Accepted from restaurant"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Authorization:'.URBANPIPER_API_KEY,
                    'Content-Type: application/json',
                ),
            ));

            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }

            curl_close($curl);
            $error = 0;
            $success = 0;
            $datas = json_decode($response,true);//echo "<pre>";print_r($datas);exit;
            if($datas['status'] == 'success'){
                $json['success'] = 'Order Is Accepted...!';
                $success = 1;
                //$json['info'] = 1;
            } elseif($datas['status'] == 'error') {
                $json['errors'] = $datas['message'];
                $error = 1;
            } else if (isset($error_msg)) {
                $json['errors'] = $error_msg;
                $error = 1;
            } else {
                $json['info'] = 0;
                $json['errors'] = $response;
                $error = 1;
            }
            $error_plus = 0;
            $check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Acknowledged' AND  `order_id` = '".$this->db->escape($this->request->get['order_id'])."' ORDER BY id desc Limit 1");
            if ($check_error->num_rows > 0) {
                if($error == 1){
                    //if($check_error->row['failure']){
                        $error_plus = $check_error->row['failure'] + 1;
                   // }
                }
                $this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
                        `sucess`= '".$this->db->escape($success)."', 
                         `date`= NOW(),
                        `failure` = '".$this->db->escape($error_plus)."' ,
                        `order_id` = '".$this->db->escape($this->request->get['order_id'])."' 
                        WHERE `event` = 'Acknowledged' ");
            } else {
                $this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
                        `sucess`= '".$this->db->escape($success)."', 
                         `date`= NOW(),
                        `failure` = '".$this->db->escape($error)."'  ,
                        `order_id` = '".$this->db->escape($this->request->get['order_id'])."' ,
                        `event` = 'Acknowledged'
                    ");
            }
        }//echo "<pre>";print_r($json['success']);exit;
        if(isset($json['success'])){

            $edit = 0;
            $kot_order_id = $this->db->query("SELECT * FROM oc_order_info WHERE urbanpiper_order_id = '".$this->request->get['order_id']."'");
            if ($kot_order_id->num_rows > 0) {
                $this->session->data['warning'] = 'Kot already printed';
                $this->request->post = array();
                $_POST = array();
                $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true)); 
            }

			   //          $url = UPDATE_KOT_STATUS_URBANPIPER;
			   //          $final_data = array('orderid' => $this->request->get['order_id']);
			   //          $data_json = json_encode($final_data);
			   //          $ch = curl_init();
			   //          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			   //          curl_setopt($ch, CURLOPT_URL, $url);
			   //          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
			   //          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			   //          curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			   //          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			   //          $response  = curl_exec($ch);
			   //          $resultsz = json_decode($response,true);

            $results = $this->db->query("UPDATE oc_orders_app SET kot_status = 1 ,online_order_status = 'Acknowledged' WHERE online_order_id = '".$this->request->get['order_id']."' ");


            //$info_datas = $this->db->query("SELECT * FROM `oc_orders_app` WHERE online_order_id = '".$this->request->get['order_id']."'")->row;
           	 $info_datas = $this->db->query("SELECT zomato_order_id,zomato_rider_otp,online_instruction,online_channel AS order_from,total_charge,delivery_charge,discount_reason,ftotalvalue FROM `oc_orders_app` WHERE online_order_id = '".$this->request->get['order_id']."'")->row;

            //$this->log->write('---------------------------Kot Print Start--------------------------');
            //$this->log->write('User Name : ' .$this->user->getUserName());
            
            // echo'<pre>';
            // print_r($this->request->get);
            // exit;
            $this->load->language('customer/customer');
            $this->load->model('catalog/urbanpiper_itemdata');
            $this->load->model('catalog/msr_recharge');
            $this->load->model('catalog/order');
            $this->load->language('catalog/order');
            $this->document->setTitle($this->language->get('heading_title'));
            $order_id = 0;
            $order_id = $this->model_catalog_urbanpiper_itemdata->addOrder($this->request->get['order_id']);
            if(isset($this->session->data['cust_id'])){
                $cust_id = $this->session->data['cust_id'];
            } else{
                $cust_id = '';
            }
            

            $anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY subcategoryid")->rows;
            $anss_1 = array();
            $anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;

            $liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
            $liqurs_1 = array();
            $liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
            $user_id= $this->user->getId();
            $user = $this->db->query("SELECT `checkkot` FROM oc_user WHERE user_id = '".$user_id."'")->row;
            // if($user['checkkot'] == '1'){
            //  $checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
            // }

            $checkkot = array();

            $infos_normal = array();
            $infos_cancel = array();
            $infos_liqurnormal = array();
            $infos_liqurcancel = array();
            $infos = array();
            $modifierdata = array();
            $allKot = array();
            $ans = $this->db->query("SELECT `order_id`, `urbanpiper_order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
            // echo'<pre>';
            // print_r($ans);
            // exit;
            foreach ($ans as $resultt) {
                $infoss[] = array(
                    'order_id'   => $resultt['order_id'],
                    'urbanpiper_order_id'   => $resultt['urbanpiper_order_id'],

                    'time_added'  => $resultt['time_added'],
                    'date_added'  => $resultt['date_added'],
                    'location'  => $resultt['location'],
                    'location_id' => $resultt['location_id'],
                    't_name'    => $resultt['t_name'],
                    'table_id'  => $resultt['table_id'],
                    'waiter'    => $resultt['waiter'],
                    'waiter_id'    => $resultt['waiter_id'],
                    'captain'   => $resultt['captain'],
                    'captain_id'   => $resultt['captain_id'],
                    'item_quantity'   => $resultt['item_quantity'],
                    'total_items'   => $resultt['total_items'],
                    'login_id' => $resultt['login_id'],
                    'login_name' => $resultt['login_name'],
                    'person' => $resultt['person'],
                    'ftotal' => $resultt['ftotal'],
                    'ltotal' => $resultt['ltotal'],
                    'grand_total' => $resultt['grand_total'],
                    'bill_status' => $resultt['bill_status'],
                );
            }
            // echo'<pre>';
            // print_r($anss);
            // exit();
            $kot_group_datas = array();
            $printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup")->rows;
            foreach($printerinfos as $pkeys => $pvalues){
                if($pvalues['subcategory'] != ''){
                    $subcategoryid_exp = explode(',', $pvalues['subcategory']);
                    foreach($subcategoryid_exp as $skey => $svalue){
                        $kot_group_datas[$svalue] = $pvalues;
                    }
                }
            }



            foreach ($anss as $lkey => $result) {
                foreach($anss as $lkeys => $resultss){
                    if($lkey == $lkeys) {

                    } elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['code'] = '';
                            }
                        }
                    } elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['qty'] = $result['qty'] + $resultss['qty'];
                                if($result['nc_kot_status'] == '0'){
                                    $result['amt'] = $result['qty'] * $result['rate'];
                                }
                            }
                        }
                    }
                }

                if($result['code'] != ''){
                    if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
                        $kot_group_datas[$result['subcategoryid']]['code'] = 1;
                    }

                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                        } else {
                                $qty = $result['qty'];
                        }
                    } else {
                        $qty = $result['qty'];
                    }

                    
                    $infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
                        'id'                => $result['id'],
                        'name'              => $result['name'],
                        'qty'               => $qty,
                        'code'              => $result['code'],
                        'amt'               => $result['amt'],
                        'rate'              => $result['rate'],
                        'message'           => $result['message'],
                        'subcategoryid'     => $result['subcategoryid'],
                        'kot_no'            => $result['kot_no'],
                        'time_added'        => $result['time'],
                    );

                    $modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                    $flag = 0;
                }
            }

            foreach ($checkkot as $lkey => $result) {
                foreach($checkkot as $lkeys => $resultss){
                    if($lkey == $lkeys) {

                    } elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['code'] = '';
                            }
                        }
                    } elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['qty'] = $result['qty'] + $resultss['qty'];
                                if($result['nc_kot_status'] == '0'){
                                    $result['amt'] = $result['qty'] * $result['rate'];
                                }
                            }
                        }
                    }
                }
                if($result['code'] != ''){
                    $allKot[] = array(
                        'order_id'          => $result['order_id'],
                        'id'                => $result['id'],
                        'name'              => $result['name'],
                        'qty'               => $result['qty'],
                        'rate'              => $result['rate'],
                        'amt'               => $result['amt'],
                        'subcategoryid'     => $result['subcategoryid'],
                        'message'           => $result['message'],
                        'kot_no'            => $result['kot_no'],
                        'time_added'        => $result['time'],
                    );
                    $modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                    $flag = 0;
                }
            }

            $total_items_normal = 0;
            $total_quantity_normal = 0;
            foreach ($anss_1 as $result) {
                if($result['qty'] > $result['pre_qty']) {
                    $tem = $result['qty'] - $result['pre_qty'];
                    $ans=abs($tem);
                    if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
                        $kot_group_datas[$result['subcategoryid']]['code'] = 1;
                    }

                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                        } else {
                                $qty = $result['qty'];
                        }
                    } else {
                        $qty = $result['qty'];
                    }
                    $infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
                        'name'              => $result['name'],
                        'qty'               => $qty,
                        'code'              => $result['code'],
                        'amt'               => $result['amt'],
                        'rate'              => $result['rate'],
                        'message'           => $result['message'],
                        'subcategoryid'     => $result['subcategoryid'],
                        'kot_no'            => $result['kot_no'],
                        'time_added'        => $result['time'],
                    );
                    $total_items_normal ++;
                    $total_quantity_normal = $total_quantity_normal + $ans;
                    $flag = 0;
                }
            }

            foreach ($anss_2 as $lkey => $result) {
                foreach($anss as $lkeys => $resultss){
                    if($lkey == $lkeys) {

                    } elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['code'] = '';
                            }
                        }
                    } elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
                        if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
                            if($result['parent'] == '0'){
                                $result['qty'] = $result['qty'] + $resultss['qty'];
                                if($result['nc_kot_status'] == '0'){
                                    $result['amt'] = $result['qty'] * $result['rate'];
                                }
                            }
                        }
                    }
                }

                if($result['code'] != ''){
                    if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
                        $kot_group_datas[$result['subcategoryid']]['code'] = 1;
                    }
                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                        } else {
                                $qty = $result['qty'];
                        }
                    } else {
                        $qty = $result['qty'];
                    }
                    $infos_cancel[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
                        'id'                => $result['id'],
                        'name'              => $result['name']." (cancel)",
                        'qty'               => $result['qty'],
                        'rate'              => $result['rate'],
                        'amt'               => $result['amt'],
                        'message'           => $result['message'],
                        'subcategoryid'     => $result['subcategoryid'],
                        'kot_no'            => $result['kot_no'],
                        'time_added'        => $result['time'],
                    );
                    $modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                    $flag = 0;
                }
            }

            foreach ($liqurs as $lkey => $liqur) {
                foreach($liqurs as $lkeys => $liqurss){
                    if($lkey == $lkeys) {

                    } elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
                        if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
                            if($liqur['parent'] == '0'){
                                $liqur['code'] = '';
                            }
                        }
                    } elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
                        if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
                            if($liqur['parent'] == '0'){
                                $liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
                                if($liqur['nc_kot_status'] == '0'){
                                    $liqur['amt'] = $liqur['qty'] * $liqur['rate'];
                                }
                            }
                        }
                    }
                }

                if($liqur['code'] != ''){
                    if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
                        $kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
                    }
                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                        } else {
                                $qty = $result['qty'];
                        }
                    } else {
                        $qty = $result['qty'];
                    }

                    $infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
                        'id'                => $liqur['id'],
                        'name'              => $liqur['name'],
                        'qty'               => $qty,
                        'amt'               => $liqur['amt'],
                        'rate'              => $liqur['rate'],
                        'message'           => $liqur['message'],
                        'subcategoryid'     => $liqur['subcategoryid'],
                        'kot_no'            => $liqur['kot_no'],
                        'time_added'        => $liqur['time'],
                    );
                    // echo "<pre>";
                    // print_r($liqur['id']);
                    $modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
                    $flag = 0;
                }
            }
            // echo'</br>';
            // print_r($modifierdata);
            // exit;

            foreach ($liqurs_1 as $liqur) {
                if($liqur['qty'] > $liqur['pre_qty']) {
                    $tem = $liqur['qty'] - $liqur['pre_qty'];
                    $ans=abs($tem);
                    if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
                        $kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
                    }

                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                            $qty = (int)$ans;
                        } else {
                                $qty = $ans;
                        }
                    } else {
                        $qty = $ans;
                    }
                    $infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
                        'name'              => $liqur['name'],
                        'qty'               => $qty,
                        'amt'               => $liqur['amt'],
                        'rate'              => $liqur['rate'],
                        'message'           => $liqur['message'],
                        'subcategoryid'     => $liqur['subcategoryid'],
                        'kot_no'            => $liqur['kot_no'],
                        'time_added'        => $liqur['time'],
                    );
                    $total_items_liquor_normal ++;
                    $total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
                    $flag = 0;
                }
            }

            $total_items_liquor_cancel = 0;
            $total_quantity_liquor_cancel = 0;
            foreach ($liqurs_2 as $lkey => $liqur) {
                foreach($liqurs_2 as $lkeys => $liqurs){
                    if($lkey == $lkeys) {

                    } elseif($lkey > $lkeys && $liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1'){
                        if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
                            if($liqur['parent'] == '0'){
                                $liqur['code'] = '';
                            }
                        }
                    } elseif ($liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1') {
                        if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
                            if($liqur['parent'] == '0'){
                                $liqur['qty'] = $liqur['qty'] + $liqurs['qty'];
                                if($liqur['nc_kot_status'] == '0'){
                                    $liqur['amt'] = $liqur['qty'] * $liqur['rate'];
                                }
                            }
                        }
                    }
                }

                if($liqur['code'] != ''){
                    if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
                        $kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
                    }

                    $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                    if($decimal_mesurement->num_rows > 0){
                        if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                            $qty = (int)$liqur['qty'];
                        } else {
                            $qty = $liqur['qty'];
                        }
                    } else {
                        $qty = $liqur['qty'];
                    }
                    $infos_liqurcancel[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
                        'id'                => $liqur['id'],
                        'name'              => $liqur['name']."(cancel)",
                        'qty'               => $qty,
                        'amt'               => $liqur['amt'],
                        'rate'              => $liqur['rate'],
                        'message'           => $liqur['message'],
                        'subcategoryid'     => $liqur['subcategoryid'],
                        'kot_no'            => $liqur['kot_no'],
                        'time_added'        => $liqur['time'],
                    );
                    //$text = "Cancelled Bot <br>Orderid :".$order_id.", Item Name :".$result['name'].", Qty :".$result['qty'].", Amt :".$result['amt'].", Kot_no :".$result['kot_no'];
                    //echo $text;
                    $modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
                    $total_items_liquor_cancel ++;
                    $total_quantity_liquor_cancel = $liqur['qty'];
                    $flag = 0;
                }
            }

            if(!isset($flag)){
                $anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
                $total_items = 0;
                $total_quantity = 0;
                foreach ($anss as $result) {
                    $infos[] = array(
                        'name'              => $result['name'],
                        'qty'               => $result['qty'],
                        'amt'               => $result['amt'],
                        'rate'              => $result['rate'],
                        'message'           => $result['message'],
                        'subcategoryid'     => $result['subcategoryid'],
                        'kot_no'            => $result['kot_no'],
                        'time_added'        => $result['time'],
                    );
                    $total_items ++;
                    $total_quantity = $total_quantity + $result['qty'];
                }
            }
            $this->db->query("UPDATE " . DB_PREFIX . "order_items SET `kot_status` = '1', `is_new` = '1' WHERE  order_id = '".$order_id."'");

            $locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
            if($locationData->num_rows > 0){
                $locationData = $locationData->row;
                if($locationData['kot_different'] == 1){
                    $kot_different = 1;
                } else {
                    $kot_different = 0;
                }
                $kot_copy = $locationData['kot_copy'];
                $bill_copy = $locationData['bill_copy'];
                $direct_bill = $locationData['direct_bill'];
                $bill_printer_type = $locationData['bill_printer_type'];
                $bill_printer_name = $locationData['bill_printer_name'];
            } else{
                $kot_different = 0;
                $kot_copy = 1;
                $direct_bill = 0;
                $bill_copy = 1;
                $bill_printer_type = '';
                $bill_printer_name = '';
            }


            $LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
            // echo'<pre>';
            // print_r($infos_normal);
            // exit;
            if(( $direct_bill == 0 && $edit == '0') || $kot_copy > 0){
                if($kot_copy > 0 ){
                    if($infos_normal){
                        foreach($infos_normal as $nkeys => $nvalues){
            
                            $printtype = '';
                            $printername = '';
                            $description = '';
                            $sub_category_id_compare = $nvalues[0]['subcategoryid'];
                            $printername = $this->user->getPrinterName();
                            $printertype = $this->user->getPrinterType();
                            
                            if($printertype != ''){
                                $printtype = $printertype;
                            } else {
                                if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
                                    $printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
                                }
                            }

                            if ($printername != '') {
                                $printname = $printername;
                            } else {
                                if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
                                    $printername = $kot_group_datas[$sub_category_id_compare]['printer'];
                                }
                            }

                            if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
                                $description = $kot_group_datas[$sub_category_id_compare]['description'];
                            }

                            $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                            if($printerModel ==0){

                                try {
                                    if($printtype == 'Network'){
                                        $connector = new NetworkPrintConnector($printername, 9100);
                                    } elseif($printtype == 'Windows'){
                                        $connector = new WindowsPrintConnector($printername);
                                    } else {
                                        $connector = '';
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
                                    }
                                    if($connector != ''){
                                        $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                                        if($printerModel == 0){
                                            if($kot_different == 0){
                                                //echo"innnn kot";exit;
                                                $printer = new Printer($connector);
                                                $printer->selectPrintMode(32);
                                                $printer->setEmphasis(true);
                                                $printer->setTextSize(2, 1);
                                                $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                for($i = 1; $i <= $kot_copy; $i++){
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
                                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                                        $printer->feed(1);
                                                    }
                                                    $printer->text($infoss[0]['location']);
                                                    $printer->feed(1);
                                                    $printer->text($description);
                                                    $printer->feed(1);
                                                   /* $printer->setTextSize(2, 1);
                                                    $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                    $printer->text("Tbl.No ".$table_id);
                                                    $printer->feed(1);*/
                                                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],25)."K.Ref.No :".$infoss[0]['order_id']);
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(str_pad("Date :".date('d/m/y', strtotime($infoss[0]['date_added'])),25)."Time :".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
                                                    $printer->feed(1);
                                                    $printer->text("Order Reference: ".$infoss[0]['order_id']."");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(false);
                                                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                    $printer->setTextSize(2, 2);
                                                    // $printer->text("Online Order No:".$infoss[0]['urbanpiper_order_id']."");
                                                    // $printer->setTextSize(1, 1);
                                                    // $printer->feed(1);
                                                    if($info_datas['zomato_order_id'] > 0){
                                                        //$printer->text("Aggre. Order ID:".$info_datas['zomato_order_id']."");
                                                        $printer->text("Aggre. Order ID: ".substr($info_datas['zomato_order_id'], -5)."");
                                                        $printer->setTextSize(1, 1);
                                                        $printer->feed(1);
                                                    }
                                                    if($info_datas['zomato_rider_otp'] != ''){
                                                        $printer->text("Order OTP :".$info_datas['zomato_rider_otp']."");
                                                        $printer->setTextSize(1, 1);
                                                        $printer->feed(1);
                                                    }
                                                    $printer->text("----------------------------------------------");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $kot_no_string = '';
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

                                                        $printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        $total_amount_normal = 0;
                                                        foreach($nvalues as $keyss => $nvalue){
                                                            $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                            $nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
                                                                
                                                            $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
                                                            if($nvalue['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            if($modifierdata != array()){
                                                                foreach($modifierdata as $key => $value){
                                                                    $printer->setTextSize(1, 1);
                                                                    if($key == $nvalue['id']){
                                                                        foreach($value as $modata){
                                                                            $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                            $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                            $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                                            $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
                                                                            $printer->feed(1);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                                            $total_amount_normal = $total_amount_normal + $nvalue['amt'];
                                                            $kot_no_string .= $nvalue['kot_no'].",";
                                                        }
                                                        $total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
                                                        
                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')));
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
                                                        $printer->feed(1);
                                                        $printer->setTextSize(2, 1);
                                                        $printer->text("G.Total :".$total_g );
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    } else {
                                                        $printer->text("Qty     Description");
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        foreach($nvalues as $keyss => $valuess){
                                                            $printer->setTextSize(2, 1);
                                                            $valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],50,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $valuess['qty'];
                                                            $kot_no_string .= $valuess['kot_no'].",";
                                                        }

                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8'),0,46));
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    }
                                                }
                                            } else{
                                                $printer = new Printer($connector);
                                                $printer->selectPrintMode(32);
                                                $printer->setEmphasis(true);
                                                $printer->setTextSize(2, 1);
                                                $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                for($i = 1; $i <= $kot_copy; $i++){
                                                    //echo'innn2';exit;
                                                    $printer->text($infoss[0]['location']);
                                                    $printer->feed(1);
                                                    $printer->text($description);
                                                    $printer->feed(1);
                                                    /*$printer->setTextSize(2, 1);
                                                    $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                    $printer->text("Tbl.No ".$table_id);
                                                    $printer->feed(1);*/
                                                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(false);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
                                                    $printer->feed(1);
                                                    $printer->text("----------------------------------------------");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $kot_no_string = '';
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
                                                        $printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        $total_amount_normal = 0;
                                                        foreach($nvalues as $keyss => $nvalue){
                                                            $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                            $nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
                                                            $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
                                                            if($nvalue['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            if($modifierdata != array()){
                                                                foreach($modifierdata as $key => $value){
                                                                    $printer->setTextSize(1, 1);
                                                                    if($key == $nvalue['id']){
                                                                        foreach($value as $modata){
                                                                            $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                            $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                            $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                                            $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
                                                                            $printer->feed(1);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                                            $total_amount_normal = $total_amount_normal + $nvalue['amt'];
                                                            $kot_no_string .= $nvalue['kot_no'].",";
                                                        }
                                                        $total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8') ));
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
                                                        $printer->feed(1);
                                                        $printer->setTextSize(2, 1);
                                                        $printer->text("G.Total :".$total_g );
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    } else {
                                                        $printer->text("Qty     Description");
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        foreach($nvalues as $keyss => $valuess){
                                                            //echo'<pre>';print_r($valuess);exit;
                                                            $printer->setTextSize(2, 1);
                                                            $valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],50,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $valuess['qty'];
                                                            $kot_no_string .= $valuess['kot_no'].",";
                                                        }
                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                        $printer->text("----------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    }
                                                    foreach($nvalues as $keyss => $valuess){
                                                        $valuess['qty'] = round($valuess['qty']);
                                                        for($i = 1; $i <= $valuess['qty']; $i++){
                                                            $total_items_normal = 0;
                                                            $total_quantity_normal = 0;
                                                            $qtydisplay = 1;
                                                            //echo $valuess['qty'];
                                                            $printer = new Printer($connector);
                                                            $printer->selectPrintMode(32);
                                                            $printer->setEmphasis(true);
                                                            $printer->setTextSize(2, 1);
                                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                            $printer->text($infoss[0]['location']);
                                                            $printer->feed(1);
                                                            $printer->text($description);
                                                            $printer->feed(1);
                                                            /*$printer->setTextSize(2, 1);
                                                            $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text("Tbl.No ".$table_id);
                                                            $printer->feed(1);*/
                                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(false);
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
                                                            $printer->feed(1);
                                                            $printer->text("----------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->text("Qty     Description");
                                                            $printer->feed(1);
                                                            $printer->text("----------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(false);
                                                            $printer->setTextSize(2, 1);
                                                            $printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,30,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $printer->setTextSize(2, 1);
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal ++;
                                                            $qtydisplay ++;
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                            $printer->text("----------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
                                                            $printer->feed(2);
                                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                            $printer->cut();
                                                            $printer->feed(2);
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            
                                        }
                                        // Close printer //
                                        $printer->close();
                                        $kot_no_string = rtrim($kot_no_string, ',');
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 2,is_new = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
                                    }
                                } catch (Exception $e) {
                                    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
                                    if(isset($kot_no_string)){
                                        $kot_no_string = rtrim($kot_no_string, ',');
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
                                    } else {
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    }
                                    $this->session->data['warning'] = $printername." "."Not Working";
                                    //continue;
                                }
                            } else {  // 45 space code starts from here

                                try {
                                    if($printtype == 'Network'){
                                        $connector = new NetworkPrintConnector($printername, 9100);
                                    } elseif($printtype == 'Windows'){
                                        $connector = new WindowsPrintConnector($printername);
                                    } else {
                                        $connector = '';
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
                                    }
                                    if($connector != ''){
                                            if($kot_different == 0){
                                                //echo"innnn kot";exit;
                                                $printer = new Printer($connector);
                                                $printer->selectPrintMode(32);
                                                $printer->setEmphasis(true);
                                                $printer->setTextSize(2, 1);
                                                $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                // $printer->text('45 Spacess');
                                                $printer->feed(1);
                                                for($i = 1; $i <= $kot_copy; $i++){
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
                                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                                        $printer->feed(1);
                                                    }
                                                    $printer->text($infoss[0]['location']);
                                                    $printer->feed(1);
                                                    $printer->text($description);
                                                    $printer->feed(1);
                                                   /* $printer->setTextSize(2, 1);
                                                    $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                    $printer->text("Tbl.No ".$table_id);
                                                    $printer->feed(1);*/
                                                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                                    $printer->feed(1);
                                                    $printer->text("Order Reference: ".$info_datas['order_from']."");
                                                    $printer->setEmphasis(false);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
                                                    $printer->feed(1);
                                                    $printer->text("------------------------------------------");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $kot_no_string = '';
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

                                                        $printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        $total_amount_normal = 0;
                                                        foreach($nvalues as $keyss => $nvalue){
                                                            $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                            $nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
                                                                
                                                            $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
                                                            if($nvalue['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            if($modifierdata != array()){
                                                                foreach($modifierdata as $key => $value){
                                                                    $printer->setTextSize(1, 1);
                                                                    if($key == $nvalue['id']){
                                                                        foreach($value as $modata){
                                                                            $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                            $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                            $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                                            $printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
                                                                            $printer->feed(1);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                                            $total_amount_normal = $total_amount_normal + $nvalue['amt'];
                                                            $kot_no_string .= $nvalue['kot_no'].",";
                                                        }
                                                        $total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
                                                    
                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
                                                        $printer->feed(1);
                                                        $printer->setTextSize(2, 1);
                                                        $printer->text("G.Total :".$total_g );
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    } else {
                                                        $printer->text("Qty     Description");
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        foreach($nvalues as $keyss => $valuess){
                                                            $printer->setTextSize(2, 1);
                                                            $valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],30,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $valuess['qty'];
                                                            $kot_no_string .= $valuess['kot_no'].",";
                                                        }

                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    }
                                                }
                                            } else{
                                                $printer = new Printer($connector);
                                                $printer->selectPrintMode(32);
                                                $printer->setEmphasis(true);
                                                $printer->setTextSize(2, 1);
                                                $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                for($i = 1; $i <= $kot_copy; $i++){
                                                    //echo'innn2';exit;
                                                    $printer->text($infoss[0]['location']);
                                                    $printer->feed(1);
                                                    $printer->text($description);
                                                    $printer->feed(1);
                                                    $printer->setTextSize(2, 1);
                                                   /*   $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                    $printer->text("Tbl.No ".$table_id);
                                                    $printer->feed(1);*/
                                                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
                                                    $printer->feed(1);
                                                    $printer->text("Order Reference: ".$info_datas['order_from']."");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $printer->feed(1);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(false);
                                                    $printer->setTextSize(1, 1);
                                                    $printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
                                                    $printer->feed(1);
                                                    $printer->text("------------------------------------------");
                                                    $printer->feed(1);
                                                    $printer->setEmphasis(true);
                                                    $kot_no_string = '';
                                                    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
                                                        $printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        $total_amount_normal = 0;
                                                        foreach($nvalues as $keyss => $nvalue){
                                                            $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                            $nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
                                                            $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                            $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
                                                            if($nvalue['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            if($modifierdata != array()){
                                                                foreach($modifierdata as $key => $value){
                                                                    $printer->setTextSize(1, 1);
                                                                    if($key == $nvalue['id']){
                                                                        foreach($value as $modata){
                                                                            $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                            $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                            $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                                            $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
                                                                            $printer->feed(1);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                                            $total_amount_normal = $total_amount_normal + $nvalue['amt'];
                                                            $kot_no_string .= $nvalue['kot_no'].",";
                                                        }
                                                        $total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
                                                        $printer->feed(1);
                                                        $printer->setTextSize(2, 1);
                                                        $printer->text("G.Total :".$total_g );
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    } else {
                                                        $printer->text("Qty     Description");
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(false);
                                                        $total_items_normal = 0;
                                                        $total_quantity_normal = 0;
                                                        foreach($nvalues as $keyss => $valuess){
                                                            //echo'<pre>';print_r($valuess);exit;
                                                            $printer->setTextSize(2, 1);
                                                            $valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->setTextSize(1, 1);
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],20,"\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal = $total_quantity_normal + $valuess['qty'];
                                                            $kot_no_string .= $valuess['kot_no'].",";
                                                        }
                                                        $printer->setTextSize(1, 1);
                                                        $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                        $printer->text("------------------------------------------");
                                                        $printer->feed(1);
                                                        $printer->setEmphasis(true);
                                                        $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
                                                        $printer->feed(2);
                                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                        $printer->cut();
                                                        $printer->feed(2);
                                                    }
                                                    foreach($nvalues as $keyss => $valuess){
                                                        $valuess['qty'] = round($valuess['qty']);
                                                        for($i = 1; $i <= $valuess['qty']; $i++){
                                                            $total_items_normal = 0;
                                                            $total_quantity_normal = 0;
                                                            $qtydisplay = 1;
                                                            //echo $valuess['qty'];
                                                            $printer = new Printer($connector);
                                                            $printer->selectPrintMode(32);
                                                            $printer->setEmphasis(true);
                                                            $printer->setTextSize(2, 1);
                                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                            $printer->text($infoss[0]['location']);
                                                            $printer->feed(1);
                                                            $printer->text($description);
                                                            $printer->feed(1);
                                                           /* $printer->setTextSize(2, 1);
                                                            $table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
                                                            $printer->text("Tbl.No ".$table_id);
                                                            $printer->feed(1);*/
                                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->setTextSize(1, 1);
                                                           $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(false);
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
                                                            $printer->feed(1);
                                                            $printer->text("------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->text("Qty     Description");
                                                            $printer->feed(1);
                                                            $printer->text("------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(false);
                                                            $printer->setTextSize(2, 1);
                                                            $printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,20,"\n"));
                                                            if($valuess['message'] != ''){
                                                                $printer->feed(1);
                                                                $printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
                                                            }
                                                            $printer->feed(1);
                                                            foreach($modifierdata as $key => $value){
                                                                $printer->setTextSize(1, 1);
                                                                if($key == $valuess['id']){
                                                                    foreach($value as $modata){
                                                                        $printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
                                                                        $printer->feed(1);
                                                                    }
                                                                }
                                                            }
                                                            $printer->setTextSize(2, 1);
                                                            $total_items_normal ++ ;
                                                            $total_quantity_normal ++;
                                                            $qtydisplay ++;
                                                            $printer->setTextSize(1, 1);
                                                            $printer->text("Instructions :".utf8_substr(html_entity_decode($info_datas['online_instruction'], ENT_QUOTES, 'UTF-8')) );
                                                        $printer->feed(1);
                                                            $printer->text("------------------------------------------");
                                                            $printer->feed(1);
                                                            $printer->setEmphasis(true);
                                                            $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
                                                            $printer->feed(2);
                                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                                            $printer->cut();
                                                            $printer->feed(2);
                                                        }
                                                    }
                                                }
                                            }
                                        // Close printer //
                                        $printer->close();
                                        $kot_no_string = rtrim($kot_no_string, ',');
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 2,is_new = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
                                    }
                                } catch (Exception $e) {
                                    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
                                    if(isset($kot_no_string)){
                                        $kot_no_string = rtrim($kot_no_string, ',');
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
                                    } else {
                                        $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    }
                                    $this->session->data['warning'] = $printername." "."Not Working";
                                    //continue;
                                }
                            }
                        }
                    }
                }
            }else {
                //$json = array();
                // $json = array(
                //  'status' => 0,
                // );

                // $this->response->addHeader('Content-Type: application/json');
                // $this->response->setOutput(json_encode($json));  
            }


            if(($infoss[0]['bill_status'] == 0 && $edit == '0') ){
               // if($direct_bill == 0 ){
                    
                    $merge_datas = array();
                    $ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($edit == '0'){
                        $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                        $last_open_dates = $this->db->query($last_open_date_sql);
                        if($last_open_dates->num_rows > 0){
                            $last_open_date = $last_open_dates->row['bill_date'];
                        } else {
                            $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                            $last_open_dates = $this->db->query($last_open_date_sql);
                            if($last_open_dates->num_rows > 0){
                                $last_open_date = $last_open_dates->row['bill_date'];
                            } else {
                                $last_open_date = date('Y-m-d');
                            }
                        }

                        $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
                        $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
                        if($last_open_dates_liq->num_rows > 0){
                            $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                        } else {
                            $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
                            $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
                            if($last_open_dates_liq->num_rows > 0){
                                $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                            } else {
                                $last_open_date_liq = date('Y-m-d');
                            }
                        }

                        $last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
                        $last_open_dates_order = $this->db->query($last_open_date_sql_order);
                        if($last_open_dates_order->num_rows > 0){
                            $last_open_date_order = $last_open_dates_order->row['bill_date'];
                        } else {
                            $last_open_date_order = date('Y-m-d');
                        }
                        
                        $ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                        
                        if($ordernogenrated['order_no'] == '0'){
                            $orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
                            $orderno = 1;
                            if(isset($orderno_q['order_no'])){
                                $orderno = $orderno_q['order_no'] + 1;
                            }
                            
                            $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                            if($kotno2->num_rows > 0){
                                $kot_no2 = $kotno2->row['billno'];
                                $kotno = $kot_no2 + 1;
                            } else{
                                $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                                if($kotno2->num_rows > 0){
                                    $kot_no2 = $kotno2->row['billno'];
                                    $kotno = $kot_no2 + 1;
                                } else {
                                    $kotno = 1;
                                }
                            }

                            $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                            if($kotno1->num_rows > 0){
                                $kot_no1 = $kotno1->row['billno'];
                                $botno = $kot_no1 + 1;
                            } else{
                                $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                                if($kotno1->num_rows > 0){
                                    $kot_no1 = $kotno1->row['billno'];
                                    $botno = $kot_no1 + 1;
                                }else {
                                    $botno = 1;
                                }
                            }

                            $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
                            $this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

                            // echo'<pre>';
                            // print_r($this->session->data);
                            // exit;
                            if(isset($this->session->data['cash'])){
                                $cashh = $this->session->data['cash'];
                            } else{
                                $cashh = '';
                            }

                            if(isset($this->session->data['credit'])){
                                $creditt = $this->session->data['credit'];
                            } else{
                                $creditt = '';
                            }

                            if(isset($this->session->data['online'])){
                                $onlinee = $this->session->data['online'];
                            } else{
                                $onlinee = '';
                            }

                            if(isset($this->session->data['onac'])){
                                $onacc = $this->session->data['onac'];
                            } else{
                                $onacc = '';
                            }

                            if(isset($this->session->data['payment_type'])){
                                $payment_type = $this->session->data['payment_type'];
                                if($payment_type==0){
                                    $payment_type = '';
                                }
                            } else{
                                $payment_type = '';
                            }

                            if($cashh != '0' && $cashh != ''){
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."', payment_status = '1', total_payment = '".$cashh."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

                            }elseif( $creditt != '0' && $creditt != ''){
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."', payment_status = '1', total_payment = '".$creditt."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
                            }elseif( $onlinee != '0' && $onlinee != ''){
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$onlinee."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
                                
                            }elseif( $onacc != '0' && $onacc != ''){
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."',onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$this->session->data['onac']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
                                
                            } elseif($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                            } else{
                                $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                            }
                            $ordernogenrated['order_no'] = $orderno;
                            $this->db->query($update_sql);
                            $apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                            if($apporder['app_order_id'] != '0'){
                                $this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
                            }
                        } 
                        if($ordernogenrated['order_no'] != '0'){
                            $merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
                        }
                    }

                    $anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
                    $tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
                    foreach($tests as $test){
                        $amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
                        $testfoods[] = array(
                            'tax1' => $test['tax1'],
                            'amt' => $amt,
                            'tax1_value' => $test['tax1_value']
                        );
                    }
                    
                    $testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
                    foreach($testss as $testa){
                        $amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
                        $testliqs[] = array(
                            'tax1' => $testa['tax1'],
                            'amt' => $amts,
                            'tax1_value' => $testa['tax1_value']
                        );
                    }
                    $infoslb = array();
                    $infosb = array();
                    $flag = 0;
                    $totalquantityfood = 0;
                    $totalquantityliq = 0;
                    $disamtfood = 0;
                    $disamtliq = 0;
                    $modifierdatabill = array();
                    foreach ($anssb as $lkey => $result) {
                        foreach($anssb as $lkeys => $results){
                            if($lkey == $lkeys) {

                            } elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
                                if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                    if($result['parent'] == '0'){
                                        $result['code'] = '';
                                    }
                                }
                            } elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
                                if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                    if($result['parent'] == '0'){
                                        $result['qty'] = $result['qty'] + $results['qty'];
                                        if($result['nc_kot_status'] == '0'){
                                            $result['amt'] = $result['qty'] * $result['rate'];
                                        }
                                    }
                                }
                            }
                        }
                        if($result['code'] != ''){
                            $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                                if($decimal_mesurement->num_rows > 0){
                                    if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                            $qty = (int)$result['qty'];
                                    } else {
                                            $qty = $result['qty'];
                                    }
                                } else {
                                    $qty = $result['qty'];
                                }
                            if($result['is_liq']== 0){
                                $infosb[] = array(
                                    'billno'        => $result['billno'],
                                    'id'            => $result['id'],
                                    'name'          => $result['name'],
                                    'rate'          => $result['rate'],
                                    'amt'           => $result['amt'],
                                    'qty'           => $qty,
                                    'tax1'          => $result['tax1'],
                                    'tax2'          => $result['tax2']
                                );
                                $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                                $totalquantityfood = $totalquantityfood + $result['qty'];
                                $disamtfood = $disamtfood + $result['discount_value'];
                            } else {
                                $flag = 1;
                                $infoslb[] = array(
                                    'billno'        => $result['billno'],
                                    'id'            => $result['id'],
                                    'name'          => $result['name'],
                                    'rate'          => $result['rate'],
                                    'amt'           => $result['amt'],
                                    'qty'           => $qty,
                                    'tax1'          => $result['tax1'],
                                    'tax2'          => $result['tax2']
                                );
                                $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                                $totalquantityliq = $totalquantityliq + $result['qty'];
                                $disamtliq = $disamtliq + $result['discount_value'];
                            }
                        }
                    }

                    if($ansb['parcel_status'] == '0'){
                        if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                            $gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
                        } else{
                            $gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
                        }
                    } else {
                        if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                            $gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
                        } else{
                            $gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
                        }
                    }

                    $csgst=$ansb['gst'] / 2;
                    $csgsttotal = $ansb['gst'];

                    $ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
                    $ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
                    $ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
                    $ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
                    $ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                    $ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                    $ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
                    $ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
                    $ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
                    $ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
                    $csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
                    if($ansb['advance_amount'] == '0.00'){
                        $gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
                    } else{
                        $gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
                    }
                    //$gtotal = ceil($gtotal);
                    $gtotal = round($gtotal);


                    if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
                        $printtype = $bill_printer_type;
                        $printername = $bill_printer_name;
                        if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
                            $printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
                            $printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
                        }

                        $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                        if($printerModel ==0){
                            try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    $printer->setTextSize(2, 1);
                                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                                    $printer->feed(1);
                                    //$printer->setFont(Printer::FONT_B);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                        //$printer->setJustification(Printer::JUSTIFY_CENTER);
                                        //$printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
                                    
                                }
                                else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                    $printer->text(("Name : ".$ansb['cust_name']));
                                    $printer->feed(1);
                                    $printer->text(("Mobile :".$ansb['cust_contact']));
                                    $printer->feed(1);
                                }
                                else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
                                    $printer->text(("Address : ".$ansb['cust_address']));
                                    $printer->feed(1);
                                    // $printer->text("Gst No :".$ansb['gst_no']);
                                    // $printer->feed(1);
                                }
                                else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
                                    $printer->text(("Name : ".$ansb['cust_name']));
                                    $printer->feed(1);
                                    $printer->text("Address : ".$ansb['cust_address']);
                                    $printer->feed(1);
                                }
                                else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
                                    $printer->text(("Mobile :".$ansb['cust_contact']));
                                    $printer->feed(1);
                                    // $printer->text("Gst No :".$ansb['gst_no']."");
                                    // $printer->feed(1);
                                }
                                else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
                                    $printer->text("Name : ".$ansb['cust_name']);
                                    $printer->feed(1);
                                    // $printer->text("Gst No :".$ansb['gst_no']);
                                    // $printer->feed(1);
                                }
                                else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
                                    $printer->text("Mobile :".$ansb['cust_contact']);
                                    $printer->feed(1);
                                    $printer->text("Address : ".$ansb['cust_address']);
                                    $printer->feed(1);
                                }
                                else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                    $printer->text("Name :".$ansb['cust_name']."");
                                    $printer->feed(1);
                                }
                                else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                    $printer->text("Mobile :".$ansb['cust_contact']."");
                                    $printer->feed(1);
                                }else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
                                    $printer->text("Address : ".$ansb['cust_address']."");
                                    $printer->feed(1);
                                }else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
                                    // $printer->text("Gst No :".$ansb['gst_no']."");
                                    // $printer->feed(1);
                                }else{
                                    $printer->text("Name : ".$ansb['cust_name']);
                                    $printer->feed(1);
                                    $printer->text("Mobile :".$ansb['cust_contact']."");
                                    $printer->feed(1);
                                    $printer->text("Address : ".$ansb['cust_address']);
                                    $printer->feed(1);
                                    // $printer->text("Gst No :".$ansb['gst_no']);
                                    // $printer->feed(1);
                                }
                                        $printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infosb){
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
                                            //$printer->feed(1);
                                            /*$printer->text("Order Reference: ".$info_datas['order_from']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            //$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No ".$infoss[0]['urbanpiper_order_id']."");*/
                                            $printer->setTextSize(1, 1);
                                            $printer->feed(1);
                                            //$printer->setTextSize(1, 1);
                                            //$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
                                            //$printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infosb as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                    foreach($modifierdatabill as $key => $value){
                                                        $printer->setTextSize(1, 1);
                                                        if($key == $nvalue['id']){
                                                            foreach($value as $modata){
                                                                $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                                $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                $printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                $printer->feed(1);
                                                            }
                                                        }
                                                    }
                                                }
                                                $total_items_normal ++ ;
                                                $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                            }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            /*foreach($testfoods as $tkey => $tvalue){
                                                $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                                $printer->feed(1);
                                            }
                                            $printer->text("----------------------------------------------");
                                            */  
                                            $printer->feed(1);
                                           /* $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            if($ansb['fdiscountper'] != '0'){
                                                $printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
                                                $printer->feed(1);
                                            } elseif($ansb['discount'] != '0'){
                                                $printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
                                                $printer->feed(1);
                                            }*/
                                            $printer->text(str_pad("",35)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",35)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                            if($info_datas['total_charge'] > 0){
                                            	$printer->text(str_pad("",31)."Packaging :".number_format($info_datas['total_charge'],2)."");
                                           	 	$printer->feed(1);
                                           	}
                                           	 if($info_datas['delivery_charge'] > 0){
                                            	$printer->text(str_pad("",24)."Delivery charge :".number_format($info_datas['delivery_charge'],2)."");
                                           	 	$printer->feed(1);
                                           	}
                                            if($info_datas['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$info_datas['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($info_datas['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($info_datas['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",25)."Packaging CGST :".$info_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."Packaging SGST :".$info_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",25)."Net total :".($ansb['grand_total'])."");
                                            $printer->setEmphasis(false);
                                            

                                        }
                                        $printer->feed(1);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_RIGHT);
                                        //$printer->text("GRAND TOTAL  :  ".round($ansb['grand_total']));
                                        $printer->text("GRAND TOTAL  :  ".$ansb['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);

                                        
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        // echo $SETTLEMENT_status;
                                        // exit;
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: ".$onacname."");
                                                $printer->feed(1);
                                                $printer->text("Contact: ".$onaccontact."");
                                            }
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        /*$printer->feed(1);
                                        $printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($info_datas['order_otp'] ));*/
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        /*$printer->text("Order Reference: ".$infoss[0]['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$infoss[0]['urbanpiper_order_id']."");
                                        $printer->feed(1);*/
                                        if($info_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$info_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($info_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                        if($info_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($info_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }

                                        /*$printer->text("Order OTP :".($info_datas['order_otp'] ));

                                        $printer->feed(2);*/
                                        $printer->cut();
                                        // Close printer //
                                    }
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
                            }
                            
                        } else {  // 45 space code starts from here

                            try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    $printer->setTextSize(2, 1);
                                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                                    $printer->feed(1);
                                    // $printer->text('45 Spacess');
                                    $printer->feed(1);
                                    //$printer->setFont(Printer::FONT_B);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                        //$printer->setJustification(Printer::JUSTIFY_CENTER);
                                        //$printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
                                            
                                        }
                                        else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                            $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
                                            $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
                                            $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
                                            $printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
                                            $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
                                            $printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                            $printer->text("Name :".$ansb['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
                                            $printer->text("Mobile :".$ansb['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
                                            $printer->text("Address : ".$ansb['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
                                            /*$printer->text("Gst No :".$ansb['gst_no']."");
                                            $printer->feed(1);*/
                                        }else{
                                            $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
                                            $printer->feed(1);
                                            // $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        $printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infosb){
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                            $printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
                                            //$printer->setTextSize(1, 1);
                                            $printer->feed(1);
                                            /*$printer->text("Order Reference: ".$info_datas['order_from']."");
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$wera_order_datas['order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);*/
                                        
                                            $printer->setEmphasis(false);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infosb as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                    foreach($modifierdatabill as $key => $value){
                                                        $printer->setTextSize(1, 1);
                                                        if($key == $nvalue['id']){
                                                            foreach($value as $modata){
                                                                $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                                $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                $printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                $printer->feed(1);
                                                            }
                                                        }
                                                    }
                                                }
                                                $total_items_normal ++ ;
                                                $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
                                            }
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            
                                           /* $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            if($ansb['fdiscountper'] != '0'){
                                                $printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
                                                $printer->feed(1);
                                            } elseif($ansb['discount'] != '0'){
                                                $printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
                                                $printer->feed(1);
                                            }*/
                                            $printer->text(str_pad("",25)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                           if($info_datas['total_charge'] > 0){
                                            	$printer->text(str_pad("",31)."Packaging :".number_format($info_datas['total_charge'],2)."");
                                           	 	$printer->feed(1);
                                           	}
                                           	 if($info_datas['delivery_charge'] > 0){
                                            	$printer->text(str_pad("",24)."Delivery charge :".number_format($info_datas['delivery_charge'],2)."");
                                           	 	$printer->feed(1);
                                           	}
                                            if($info_datas['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$info_datas['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($info_datas['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($info_datas['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",20)."Packaging CGST :".$info_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",20)."Packaging SGST :".$info_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",25)."Net total :".($ansb['grand_total'])."");
                                            $printer->setEmphasis(false);   

                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        //$printer->text("GRAND TOTAL : ".round($ansb['grand_total']));
                                        $printer->text("GRAND TOTAL : ".$ansb['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ansb['dtotalvalue']!=0){
                                            $printer->text("Delivery Charge:".$ansb['dtotalvalue']);
                                            $printer->feed(1);
                                        }
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        /*$printer->feed(1);
                                        $printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($info_datas['order_otp']) );
                                        $printer->feed(1);*/
                                        //$printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }

                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        /*$printer->text("Order Reference: ".$infoss[0]['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$infoss[0]['urbanpiper_order_id']."");
                                        $printer->feed(1);*/
                                         if($info_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$info_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($info_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                        if($info_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($info_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        /*$printer->text("Order OTP :".($info_datas['order_otp'] ));

                                        $printer->feed(2);*/
                                        $printer->cut();
                                        // Close printer //
                                    }
                                    $printer->close();

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
                            }
                        }
                    }
                    //$json = array();
                    // $json = array(
                        
                    //  'status' => 1,
                        
                    // );
                    // $this->response->addHeader('Content-Type: application/json');
                    // $this->response->setOutput(json_encode($json));
               // }
            }

            /*if($reject_reason != ''){
                $this->db->query("UPDATE oc_werafood_orders SET status = '".$reject_reason."' WHERE order_id = '".$orderid."'");
                // echo'inn';
                // exit;
            }*/
            //$json['success'] = 'Food Ready Send Succesfully...!';
            //sleep(2);
            $json['done'] = '<script>parent.closeIFrame();</script>';
            $json['info'] = 1;
            $this->response->setOutput(json_encode($json));
        	
        } elseif(isset($json['errors'])) {
            //$json['errors'] = $datas['message'];
            $json['done'] = '<script>parent.closeIFrame();</script>';
          	 $this->response->setOutput(json_encode($json));
        } else {
            $json['info'] = 0;
            $json['errors'] = $response;
            //$this->response->setOutput(json_encode($json));
        }
    }


	//bill printcode is urbanpiper code
	public function bill_print() {
        
        if (isset($this->request->get['order_id'])) {

            //$orderid = $this->request->get['order_id'];

            $order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE urbanpiper_order_id = '".$this->request->get['order_id']."'")->row;
            $order_id = $order_ids['order_id'];
            //$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

            /*$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

            $order_id = $order_ids['order_id'];*/
            $ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
            $zomato_order_id_datas = $this->db->query("SELECT `zomato_order_id`,`zomato_rider_otp` FROM `oc_orders_app` WHERE online_order_id = '".$this->request->get['order_id']."'")->row;
        
            $duplicate = '0';
            $advance_id = '0';
            $advance_amount = '0';
            $grand_total = '0';
            $orderno = '0';
                /*echo'<pre>';
            print_r($ans);
            exit;*/
            if(($ans['bill_status'] == 1) || ($ans['bill_status'] == 0)){
            
                date_default_timezone_set('Asia/Kolkata');
                $this->load->language('customer/customer');
                $this->load->model('catalog/order');
                $merge_datas = array();
                $this->document->setTitle('BILL');
                $te = 'BILL';
                
                $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                $last_open_dates = $this->db->query($last_open_date_sql);
                if($last_open_dates->num_rows > 0){
                    $last_open_date = $last_open_dates->row['bill_date'];
                } else {
                    $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                    $last_open_dates = $this->db->query($last_open_date_sql);
                    if($last_open_dates->num_rows > 0){
                        $last_open_date = $last_open_dates->row['bill_date'];
                    } else {
                        $last_open_date = date('Y-m-d');
                    }
                }

                $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                if($last_open_dates_liq->num_rows > 0){
                    $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                } else {
                    $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                    $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                    if($last_open_dates_liq->num_rows > 0){
                        $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                    } else {
                        $last_open_date_liq = date('Y-m-d');
                    }
                }

                $last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
                $last_open_dates_order = $this->db->query($last_open_date_sql_order);
                if($last_open_dates_order->num_rows > 0){
                    $last_open_date_order = $last_open_dates_order->row['bill_date'];
                } else {
                    $last_open_date_order = date('Y-m-d');
                }

                if($ans['order_no'] == '0'){
                    $orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
                    $orderno = 1;
                    if(isset($orderno_q['order_no'])){
                        $orderno = $orderno_q['order_no'] + 1;
                    }

                    $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                    if($kotno2->num_rows > 0){
                        $kot_no2 = $kotno2->row['billno'];
                        $kotno = $kot_no2 + 1;
                    } else{
                        $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                        if($kotno2->num_rows > 0){
                            $kot_no2 = $kotno2->row['billno'];
                            $kotno = $kot_no2 + 1;
                        } else {
                            $kotno = 1;
                        }
                    }

                    $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                    if($kotno1->num_rows > 0){
                        $kot_no1 = $kotno1->row['billno'];
                        $botno = $kot_no1 + 1;
                    } else{
                        $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                        if($kotno1->num_rows > 0){
                            $kot_no1 = $kotno1->row['billno'];
                            $botno = $kot_no1 + 1;
                        } else {
                            $botno = 1;
                        }
                    }

                    $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
                    $this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

                    //$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
                        $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    } else{
                        $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    }
                    $apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($apporder['app_order_id'] != '0'){
                        $this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
                    }
                    $this->db->query($update_sql);
                }

                if($advance_id != '0'){
                    $this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
                }
                $anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
                // echo'<pre>';
                // print_r($anss);
                // exit;

                $testfood = array();
                $testliq = array();
                $testtaxvalue1food = 0;
                $testtaxvalue1liq = 0;
                $tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
                foreach($tests as $test){
                    $amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
                    $testfoods[] = array(
                        'tax1' => $test['tax1'],
                        'amt' => $amt,
                        'tax1_value' => $test['tax1_value']
                    );
                }
                
                $testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
                foreach($testss as $testa){
                    $amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
                    $testliqs[] = array(
                        'tax1' => $testa['tax1'],
                        'amt' => $amts,
                        'tax1_value' => $testa['tax1_value']
                    );
                }
                
                $infosl = array();
                $infos = array();
                $flag = 0;
                $totalquantityfood = 0;
                $totalquantityliq = 0;
                $disamtfood = 0;
                $disamtliq = 0;
                $modifierdatabill = array();
                foreach ($anss as $lkey => $result) {
                    foreach($anss as $lkeys => $results){
                        if($lkey == $lkeys) {

                        } elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['code'] = '';
                                }
                            }
                        } elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['qty'] = $result['qty'] + $results['qty'];
                                    if($result['nc_kot_status'] == '0'){
                                        $result['amt'] = $result['qty'] * $result['rate'];
                                    }
                                }
                            }
                        }
                    }
                    
                    if($result['code'] != ''){
                        $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                        if($decimal_mesurement->num_rows > 0){
                            if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                            } else {
                                    $qty = $result['qty'];
                            }
                        } else {
                            $qty = $result['qty'];
                        }
                        if($result['is_liq']== 0){
                            $infos[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityfood = $totalquantityfood + $result['qty'];
                            $disamtfood = $disamtfood + $result['discount_value'];
                        } else {
                            $flag = 1;
                            $infosl[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityliq = $totalquantityliq + $result['qty'];
                            $disamtliq = $disamtliq + $result['discount_value'];
                        }
                    }
                }
                
                $merge_datas = array();
                if($orderno != '0'){
                    $merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
                }
                
                if($ans['parcel_status'] == '0'){
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    } else {
                        $gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    }
                } else {
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    } else {
                        $gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    }
                }

                
                
                $csgst=$ans['gst']/2;
                $csgsttotal = $ans['gst'];

                $ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
                $ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
                $ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
                $csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
                if($ans['advance_amount'] == '0.00'){
                    $gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
                } else{
                    $gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
                }
                //$gtotal = ceil($gtotal);
                $gtotal = round($gtotal);

                $printtype = '';
                $printername = '';

                if ($printtype == '' || $printername == '' ) {
                    $printtype = $this->user->getPrinterType();
                    $printername = $this->user->getPrinterName();
                    $bill_copy = 1;
                }

                if ($printtype == '' || $printername == '' ) {
                    $locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
                    if($locationData->num_rows > 0){
                        $locationData = $locationData->row;
                        $printtype = $locationData['bill_printer_type'];
                        $printername = $locationData['bill_printer_name'];
                        $bill_copy = $locationData['bill_copy'];
                    } else{
                        $printtype = '';
                        $printername = '';
                        $bill_copy = 1;
                    }
                }
                // $this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
                
                if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
                    $printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
                    $printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
                }
                $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                $LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

                    if($printerModel ==0){
                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                        //if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            /*$printer->text("Gst No :".$ans['gst_no']."");
                                            $printer->feed(1);*/
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                           /* $printer->text("Gst No :".$ans['gst_no']);
                                            $printer->feed(1);*/
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                            //$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
                                            $printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['date_added'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s', strtotime($ans['time_added']))."");
                                            $printer->feed(1);
                                                    
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            /*// foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);*/
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",35)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",35)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                            if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                           

                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            //order_ids
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_RIGHT);
                                        //$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL  :  ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                        /*$printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                        $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        /*$printer->text("Order Reference: ".$ans['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        $printer->feed(1);*/
                                        if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                       if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                        
                    }
                    else{  // 45 space code starts from here

                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    // $printer->text('45 Spacess');
                                    $printer->feed(1);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                       // if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
                                            $printer->feed(1);
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            // foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            // $printer->text("------------------------------------------");
                                            // $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",25)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                           if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        if($ans['advance_amount'] != '0.00'){
                                            $printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
                                            $printer->feed(1);
                                        }
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        //$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL : ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ans['dtotalvalue']!=0){
                                                $printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
                                                $printer->feed(1);
                                            }
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                       /*   $printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                                $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        // $printer->text("Order Reference: ".$ans['order_id']."");
                                        // $printer->feed(1);
                                        // $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        // $printer->feed(1);
                                         if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                        if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                    }
                    if($ans['bill_status'] == 1 && $duplicate == '1'){
                        $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
                    } else{
                        $json = array();
                        $json = array(
                            'LOCAL_PRINT' => 0,
                            'status' => 1,
                        );
                    }
                    $json['done'] = '<script>parent.closeIFrame();</script>';
                    $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                
            } else {
                $this->session->data['warning'] = 'Bill already printed';
                $this->request->post = array();
                $_POST = array();
                $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
            }
        }

        // $this->request->post = array();
        // $_POST = array();
        // $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
    }

	// public function bill_print() {
		
	// 	if (isset($this->request->get['order_id'])) {
	// 		$orderid = $this->request->get['order_id'];
	// 		$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

	// 		$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

	// 		$order_id = $order_ids['order_id'];
	// 		$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 		// echo'<pre>';
	// 		// print_r($ans);
	// 		// exit;
			
	// 		$duplicate = '0';
	// 		$advance_id = '0';
	// 		$advance_amount = '0';
	// 		$grand_total = '0';
	// 		$orderno = '0';
			
	// 		if(($ans['bill_status'] == 1 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
			
	// 			date_default_timezone_set('Asia/Kolkata');
	// 			$this->load->language('customer/customer');
	// 			$this->load->model('catalog/order');
	// 			$merge_datas = array();
	// 			$this->document->setTitle('BILL');
	// 			$te = 'BILL';
				
	// 			$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 			$last_open_dates = $this->db->query($last_open_date_sql);
	// 			if($last_open_dates->num_rows > 0){
	// 				$last_open_date = $last_open_dates->row['bill_date'];
	// 			} else {
	// 				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 				$last_open_dates = $this->db->query($last_open_date_sql);
	// 				if($last_open_dates->num_rows > 0){
	// 					$last_open_date = $last_open_dates->row['bill_date'];
	// 				} else {
	// 					$last_open_date = date('Y-m-d');
	// 				}
	// 			}

	// 			$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

	// 			$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

	// 			if($last_open_dates_liq->num_rows > 0){
	// 				$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 			} else {
	// 				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

	// 				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

	// 				if($last_open_dates_liq->num_rows > 0){
	// 					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 				} else {
	// 					$last_open_date_liq = date('Y-m-d');
	// 				}
	// 			}

	// 			$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
	// 			$last_open_dates_order = $this->db->query($last_open_date_sql_order);
	// 			if($last_open_dates_order->num_rows > 0){
	// 				$last_open_date_order = $last_open_dates_order->row['bill_date'];
	// 			} else {
	// 				$last_open_date_order = date('Y-m-d');
	// 			}

	// 			if($ans['order_no'] == '0'){
	// 				$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
	// 				$orderno = 1;
	// 				if(isset($orderno_q['order_no'])){
	// 					$orderno = $orderno_q['order_no'] + 1;
	// 				}

	// 				$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 				if($kotno2->num_rows > 0){
	// 					$kot_no2 = $kotno2->row['billno'];
	// 					$kotno = $kot_no2 + 1;
	// 				} else{
	// 					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 					if($kotno2->num_rows > 0){
	// 						$kot_no2 = $kotno2->row['billno'];
	// 						$kotno = $kot_no2 + 1;
	// 					} else {
	// 						$kotno = 1;
	// 					}
	// 				}

	// 				$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 				if($kotno1->num_rows > 0){
	// 					$kot_no1 = $kotno1->row['billno'];
	// 					$botno = $kot_no1 + 1;
	// 				} else{
	// 					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 					if($kotno1->num_rows > 0){
	// 						$kot_no1 = $kotno1->row['billno'];
	// 						$botno = $kot_no1 + 1;
	// 					} else {
	// 						$botno = 1;
	// 					}
	// 				}

	// 				$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
	// 				$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

	// 				//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 				if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
	// 					$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
	// 				} else{
	// 					$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
	// 				}
	// 				$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 				if($apporder['app_order_id'] != '0'){
	// 					$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
	// 				}
	// 				$this->db->query($update_sql);
	// 			}

	// 			if($advance_id != '0'){
	// 				$this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
	// 			}
	// 			$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
	// 			// echo'<pre>';
	// 			// print_r($anss);
	// 			// exit;

	// 			$testfood = array();
	// 			$testliq = array();
	// 			$testtaxvalue1food = 0;
	// 			$testtaxvalue1liq = 0;
	// 			$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
	// 			foreach($tests as $test){
	// 				$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
	// 				$testfoods[] = array(
	// 					'tax1' => $test['tax1'],
	// 					'amt' => $amt,
	// 					'tax1_value' => $test['tax1_value']
	// 				);
	// 			}
				
	// 			$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
	// 			foreach($testss as $testa){
	// 				$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
	// 				$testliqs[] = array(
	// 					'tax1' => $testa['tax1'],
	// 					'amt' => $amts,
	// 					'tax1_value' => $testa['tax1_value']
	// 				);
	// 			}
				
	// 			$infosl = array();
	// 			$infos = array();
	// 			$flag = 0;
	// 			$totalquantityfood = 0;
	// 			$totalquantityliq = 0;
	// 			$disamtfood = 0;
	// 			$disamtliq = 0;
	// 			$modifierdatabill = array();
	// 			foreach ($anss as $lkey => $result) {
	// 				foreach($anss as $lkeys => $results){
	// 					if($lkey == $lkeys) {

	// 					} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
	// 						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 							if($result['parent'] == '0'){
	// 								$result['code'] = '';
	// 							}
	// 						}
	// 					} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
	// 						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 							if($result['parent'] == '0'){
	// 								$result['qty'] = $result['qty'] + $results['qty'];
	// 								if($result['nc_kot_status'] == '0'){
	// 									$result['amt'] = $result['qty'] * $result['rate'];
	// 								}
	// 							}
	// 						}
	// 					}
	// 				}
					
	// 				if($result['code'] != ''){
	// 					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 					if($decimal_mesurement->num_rows > 0){
	// 						if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 								$qty = (int)$result['qty'];
	// 						} else {
	// 								$qty = $result['qty'];
	// 						}
	// 					} else {
	// 						$qty = $result['qty'];
	// 					}
	// 					if($result['is_liq']== 0){
	// 						$infos[] = array(
	// 							'id'			=> $result['id'],
	// 							'billno'		=> $result['billno'],
	// 							'name'          => $result['name'],
	// 							'rate'          => $result['rate'],
	// 							'amt'           => $result['amt'],
	// 							'qty'         	=> $qty,
	// 							'tax1'         	=> $result['tax1'],
	// 							'tax2'          => $result['tax2'],
	// 							'discount_value'=> $result['discount_value']
	// 						);
	// 						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$totalquantityfood = $totalquantityfood + $result['qty'];
	// 						$disamtfood = $disamtfood + $result['discount_value'];
	// 					} else {
	// 						$flag = 1;
	// 						$infosl[] = array(
	// 							'id'			=> $result['id'],
	// 							'billno'		=> $result['billno'],
	// 							'name'          => $result['name'],
	// 							'rate'          => $result['rate'],
	// 							'amt'           => $result['amt'],
	// 							'qty'         	=> $qty,
	// 							'tax1'         	=> $result['tax1'],
	// 							'tax2'          => $result['tax2'],
	// 							'discount_value'=> $result['discount_value']
	// 						);
	// 						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$totalquantityliq = $totalquantityliq + $result['qty'];
	// 						$disamtliq = $disamtliq + $result['discount_value'];
	// 					}
	// 				}
	// 			}
				
	// 			$merge_datas = array();
	// 			if($orderno != '0'){
	// 				$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
	// 			}
				
	// 			if($ans['parcel_status'] == '0'){
	// 				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 					$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
	// 				} else {
	// 					$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
	// 				}
	// 			} else {
	// 				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 					$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
	// 				} else {
	// 					$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
	// 				}
	// 			}

				
				
	// 			$csgst=$ans['gst']/2;
	// 			$csgsttotal = $ans['gst'];

	// 			$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
	// 			$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
	// 			$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 			$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
	// 			if($ans['advance_amount'] == '0.00'){
	// 				$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			} else{
	// 				$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			}
	// 			//$gtotal = ceil($gtotal);
	// 			$gtotal = round($gtotal);

	// 			$printtype = '';
	// 			$printername = '';

	// 			if ($printtype == '' || $printername == '' ) {
	// 				$printtype = $this->user->getPrinterType();
	// 				$printername = $this->user->getPrinterName();
	// 				$bill_copy = 1;
	// 			}

	// 			if ($printtype == '' || $printername == '' ) {
	// 				$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
	// 				if($locationData->num_rows > 0){
	// 					$locationData = $locationData->row;
	// 					$printtype = $locationData['bill_printer_type'];
	// 					$printername = $locationData['bill_printer_name'];
	// 					$bill_copy = $locationData['bill_copy'];
	// 				} else{
	// 					$printtype = '';
	// 					$printername = '';
	// 					$bill_copy = 1;
	// 				}
	// 			}
	// 			$this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
				
	// 			if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
	// 				$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
	// 			 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
	// 			}
	// 			$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
	// 			$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

	// 				if($printerModel ==0){
	// 					try {
	// 						    if($printtype == 'Network'){
	// 						 		$connector = new NetworkPrintConnector($printername, 9100);
	// 						 	} else if($printtype == 'Windows'){
	// 						 		$connector = new WindowsPrintConnector($printername);
	// 						 	} else {
	// 						 		$connector = '';
	// 						 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						 	}
	// 						 	if($connector != ''){
	// 							    $printer = new Printer($connector);
	// 							    $printer->selectPrintMode(32);

	// 							   	$printer->setEmphasis(true);
	// 							   	for($i = 1; $i <= $bill_copy; $i++){
	// 								   	$printer->setTextSize(2, 1);
	// 								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 								    $printer->feed(1);
	// 								    $printer->setTextSize(1, 1);
	// 								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 								    if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 								    	$printer->feed(1);
	// 								    	$printer->text("Duplicate Bill");
	// 								    }
	// 								    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								    $printer->feed(1);
	// 								    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 										$printer->feed(1);
	// 										$printer->text(("Mobile :".$ans['cust_contact']));
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Address : ".$ans['cust_address']));
	// 										$printer->feed(1);
	// 										$printer->text("Gst No :".$ans['gst_no']);
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Mobile :".$ans['cust_contact']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text("Mobile :".$ans['cust_contact']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Name :".$ans['cust_name']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}else{
	// 									    $printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 								   	if($infos){
									   			
	// 								   		$printer->feed(1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
	// 								   		$printer->feed(1);
	// 								   		/*$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->setTextSize(2, 2);
	// 								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 								   		$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);*/
	// 								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 								    	/*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
	// 								    	$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
	// 									    $printer->feed(1);*/
	// 								   		$printer->setEmphasis(false);
	// 									    $printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 										$printer->feed(1);
	// 								   	 	$printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setEmphasis(false);
	// 										$total_items_normal = 0;
	// 										$total_quantity_normal = 0;
	// 									    foreach($infos as $nkey => $nvalue){
	// 									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
	// 									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 									    	$printer->feed(1);
	// 								    	 	if($modifierdatabill != array()){
	// 											    	foreach($modifierdatabill as $key => $value){
	// 										    			$printer->setTextSize(1, 1);
	// 										    			if($key == $nvalue['id']){
	// 										    				foreach($value as $modata){
	// 										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 													    		$printer->feed(1);
	// 												    		}
	// 												    	}
	// 										    		}
	// 										    	}
	// 										    	$total_items_normal ++ ;
	// 									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

	// 									    }
	// 									    $printer->text("----------------------------------------------");
	// 									    $printer->feed(1);
	// 									    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
	// 									    $printer->feed(1);
	// 									    $printer->setEmphasis(false);
	// 									   	$printer->setTextSize(1, 1);
	// 									    $printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										/*// foreach($testfoods as $tkey => $tvalue){
	// 										// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
	// 									 //    	$printer->feed(1);
	// 										// }
	// 										$printer->text("----------------------------------------------");
	// 										$printer->feed(1);*/
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										if($ans['fdiscountper'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										} elseif($ans['discount'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										}
	// 										$printer->text(str_pad("",35)."SCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",35)."CCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",30)."Packaging :".$wera_order_datas['packaging']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
	// 										$printer->feed(1);
											
	// 										//order_ids
											
	// 										$printer->setEmphasis(true);
	// 										$printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
	// 										$printer->setEmphasis(false);
	// 									}
									   	
	// 									$printer->feed(1);
	// 									$printer->text("----------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(true);
	// 									$printer->setTextSize(2, 2);
	// 									$printer->setJustification(Printer::JUSTIFY_RIGHT);
	// 									$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
	// 									$printer->setTextSize(1, 1);
	// 									$printer->feed(1);
										
	// 									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 									if($SETTLEMENT_status == '1'){
	// 										if(isset($this->session->data['credit'])){
	// 											$credit = $this->session->data['credit'];
	// 										} else {
	// 											$credit = '0';
	// 										}
	// 										if(isset($this->session->data['cash'])){
	// 											$cash = $this->session->data['cash'];
	// 										} else {
	// 											$cash = '0';
	// 										}
	// 										if(isset($this->session->data['online'])){
	// 											$online = $this->session->data['online'];
	// 										} else {
	// 											$online ='0';
	// 										}

	// 										if(isset($this->session->data['onac'])){
	// 											$onac = $this->session->data['onac'];
	// 											$onaccontact = $this->session->data['onaccontact'];
	// 											$onacname = $this->session->data['onacname'];

	// 										} else {
	// 											$onac ='0';
	// 										}
	// 									}
	// 									if($SETTLEMENT_status=='1'){
	// 										if($credit!='0' && $credit!=''){
	// 											$printer->text("PAY BY: CARD");
	// 										}
	// 										if($online!='0' && $online!=''){
	// 											$printer->text("PAY BY: ONLINE");
	// 										}
	// 										if($cash!='0' && $cash!=''){
	// 											$printer->text("PAY BY: CASH");
	// 										}
	// 										if($onac!='0' && $onac!=''){
	// 											$printer->text("PAY BY: ON.ACCOUNT");
	// 											$printer->feed(1);
	// 											$printer->text("Name: '".$onacname."'");
	// 											$printer->feed(1);
	// 											$printer->text("Contact: '".$onaccontact."'");
	// 										}
	// 									}
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->text("----------------------------------------------");
	// 								    $printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(false);
	// 								   	$printer->setTextSize(1, 1);
									   
										
	// 									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 									$printer->feed(1);
	// 								   /*	$printer->setTextSize(2, 2);

	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
	// 											$printer->feed(1);*/
	// 								   	$printer->setTextSize(1, 1);

	// 									if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 										$printer->feed(1);
	// 									}
	// 									if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 										$printer->feed(1);
	// 									}
	// 									$printer->text("----------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 									}
	// 									$printer->feed(1);
	// 							   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 							    	$printer->setTextSize(2, 2);
	// 									$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 									$printer->feed(1);
	// 							   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 							   		$printer->feed(1);
	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp']));

	// 									$printer->feed(2);
	// 									$printer->cut();
	// 								}
	// 								// Close printer //
	// 							    $printer->close();
	// 						    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

	// 							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 							}
	// 						} catch (Exception $e) {
	// 						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->session->data['warning'] = $printername." "."Not Working";
	// 						}
						
	// 				}
	// 				else{  // 45 space code starts from here

	// 					try {
	// 						    if($printtype == 'Network'){
	// 						 		$connector = new NetworkPrintConnector($printername, 9100);
	// 						 	} else if($printtype == 'Windows'){
	// 						 		$connector = new WindowsPrintConnector($printername);
	// 						 	} else {
	// 						 		$connector = '';
	// 						 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						 	}
	// 						 	if($connector != ''){
	// 							    $printer = new Printer($connector);
	// 							    $printer->selectPrintMode(32);

	// 							   	$printer->setEmphasis(true);
	// 							   	// $printer->text('45 Spacess');
	// 				    			$printer->feed(1);
	// 							   	for($i = 1; $i <= $bill_copy; $i++){
	// 								   	$printer->setTextSize(2, 1);
	// 								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 								    $printer->feed(1);
	// 								    $printer->setTextSize(1, 1);
	// 								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 								    if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 								    	$printer->feed(1);
	// 								    	$printer->text("Duplicate Bill");
	// 								    }
	// 								    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								    $printer->feed(1);
	// 								    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Name :".$ans['cust_name']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}else{
	// 									    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 								   	if($infos){
									   			
	// 								   		$printer->feed(1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
	// 								   		$printer->feed(1);
	// 								   		/*$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->setTextSize(2, 2);
	// 								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 								   		$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);*/
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 								    	/*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
	// 								    	$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
	// 									    $printer->feed(1);*/
	// 								   		$printer->setEmphasis(false);
	// 									    $printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 										$printer->feed(1);
	// 								   	 	$printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setEmphasis(false);
	// 										$total_items_normal = 0;
	// 										$total_quantity_normal = 0;
	// 									    foreach($infos as $nkey => $nvalue){
	// 									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
	// 									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 									    	$printer->feed(1);
	// 								    	 	if($modifierdatabill != array()){
	// 											    	foreach($modifierdatabill as $key => $value){
	// 										    			$printer->setTextSize(1, 1);
	// 										    			if($key == $nvalue['id']){
	// 										    				foreach($value as $modata){
	// 										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 													    		$printer->feed(1);
	// 												    		}
	// 												    	}
	// 										    		}
	// 										    	}
	// 										    	$total_items_normal ++ ;
	// 									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

	// 									    }
	// 									    $printer->text("------------------------------------------");
	// 									    $printer->feed(1);
	// 									    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
	// 									    $printer->feed(1);
	// 									    $printer->setEmphasis(false);
	// 									   	$printer->setTextSize(1, 1);
	// 									    $printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										// foreach($testfoods as $tkey => $tvalue){
	// 										// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
	// 									 //    	$printer->feed(1);
	// 										// }
	// 										// $printer->text("------------------------------------------");
	// 										// $printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										if($ans['fdiscountper'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										} elseif($ans['discount'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										}
	// 										$printer->text(str_pad("",25)."SCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."CCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging :".$wera_order_datas['packaging']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
	// 										$printer->feed(1);
											
	// 										$printer->setEmphasis(true);
	// 										$printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
	// 										$printer->setEmphasis(false);
	// 									}
									   	
	// 									$printer->feed(1);
	// 									$printer->text("------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(true);
	// 									if($ans['advance_amount'] != '0.00'){
	// 										$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
	// 										$printer->feed(1);
	// 									}
	// 									$printer->setTextSize(2, 2);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
	// 									$printer->setTextSize(1, 1);
	// 									$printer->feed(1);
	// 									if($ans['dtotalvalue']!=0){
	// 											$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
	// 											$printer->feed(1);
	// 										}
	// 									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 									if($SETTLEMENT_status == '1'){
	// 										if(isset($this->session->data['credit'])){
	// 											$credit = $this->session->data['credit'];
	// 										} else {
	// 											$credit = '0';
	// 										}
	// 										if(isset($this->session->data['cash'])){
	// 											$cash = $this->session->data['cash'];
	// 										} else {
	// 											$cash = '0';
	// 										}
	// 										if(isset($this->session->data['online'])){
	// 											$online = $this->session->data['online'];
	// 										} else {
	// 											$online ='0';
	// 										}

	// 										if(isset($this->session->data['onac'])){
	// 											$onac = $this->session->data['onac'];
	// 											$onaccontact = $this->session->data['onaccontact'];
	// 											$onacname = $this->session->data['onacname'];

	// 										} else {
	// 											$onac ='0';
	// 										}
	// 									}
	// 									if($SETTLEMENT_status=='1'){
	// 										if($credit!='0' && $credit!=''){
	// 											$printer->text("PAY BY: CARD");
	// 										}
	// 										if($online!='0' && $online!=''){
	// 											$printer->text("PAY BY: ONLINE");
	// 										}
	// 										if($cash!='0' && $cash!=''){
	// 											$printer->text("PAY BY: CASH");
	// 										}
	// 										if($onac!='0' && $onac!=''){
	// 											$printer->text("PAY BY: ON.ACCOUNT");
	// 											$printer->feed(1);
	// 											$printer->text("Name: '".$onacname."'");
	// 											$printer->feed(1);
	// 											$printer->text("Contact: '".$onaccontact."'");
	// 										}
	// 									}
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->text("------------------------------------------");
	// 								    $printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(false);
	// 								   	$printer->setTextSize(1, 1);
									   
										
	// 									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 									$printer->feed(1);
	// 								   	/*$printer->setTextSize(2, 2);

	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
	// 											$printer->feed(1);*/
	// 								   	$printer->setTextSize(1, 1);

	// 									if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 										$printer->feed(1);
	// 									}
	// 									if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 										$printer->feed(1);
	// 									}
	// 									$printer->text("------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 									}
	// 									$printer->feed(1);
	// 							   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 							    	$printer->setTextSize(2, 2);
	// 									$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 									$printer->feed(1);
	// 							   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 							   		$printer->feed(1);
	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp'] ));

	// 									$printer->feed(2);
	// 									$printer->cut();
	// 								}
	// 								// Close printer //
	// 							    $printer->close();
	// 						    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

	// 							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 							}
	// 						} catch (Exception $e) {
	// 						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->session->data['warning'] = $printername." "."Not Working";
	// 						}
	// 				}
	// 				if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 					$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// 				} else{
	// 					$json = array();
	// 					$json = array(
	// 						'LOCAL_PRINT' => 0,
	// 						'status' => 1,
	// 					);
	// 				}
	// 				$json['done'] = '<script>parent.closeIFrame();</script>';
	// 				$json['info'] = 1;
	// 				$this->response->setOutput(json_encode($json));
				
	// 		} else {
	// 			$this->session->data['warning'] = 'Bill already printed';
	// 			$this->request->post = array();
	// 			$_POST = array();
	// 			$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// 		}
	// 	}

	// 	// $this->request->post = array();
	// 	// $_POST = array();
	// 	// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// }

	

	/*public function getlist(){
		$url = '';
		
		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		} else{
			$data['order_id'] = '';
		}
		$data['final_datas'] = array();
		$final_data = array();
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$msggg = '';
			$body_data = array(
				'merchant_id' => MERCHANT_ID,
				'order_id' => $order_id,
			);

			$body = json_encode($body_data);
			
			$url= "https://api.werafoods.com/pos/v2/order/getde";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
			
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($ch);
			$datass = json_decode($response,true);
				// echo'<pre>';
				// print_r($datass);
				// exit;
			if($datass['code'] == 1){
				$this->db->query("UPDATE oc_werafood_order_rider SET rider_name = '".$datass['details']['rider_name']."', rider_number = '".$datass['details']['rider_number']."', rider_status = '".$datass['details']['rider_status']."', time_to_arrive = '".$datass['details']['time_to_arrive']."' WHERE order_id = '".$order_id."'");
			} elseif($datass['code'] == 2){
				$msggg = $datass['msg'];
			} 

			curl_close($ch);


			$data['msggg'] = $msggg;
			$customer_data = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$order_id."'")->row;
			$final_data['customer_data'] = array(
				'name' => $customer_data['name'],
				'phone_number' => $customer_data['phone_number'],
				'email' => $customer_data['email'],
				'address' => $customer_data['address'],

			);

			$results = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$order_id."'")->rows;
			$total_qty = 0;
			$total_items = 0;
			foreach ($results as $result) {
				$total_qty = $total_qty + $result['item_quantity'];
				$final_data['item_data'][] = array(
					
					'order_id' => $result['order_id'],
					'instructions' => strip_tags(html_entity_decode( $result['instructions'], ENT_QUOTES, 'UTF-8')),
					'item_quantity' => $result['item_quantity'],
					'item_unit_price' => $result['item_unit_price'],
					'subtotal' => $result['subtotal'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
				$total_items++;
			}

			$status = $this->db->query("SELECT status,gross_amount,order_from,enable_delivery,order_instructions,order_otp FROM oc_werafood_orders WHERE order_id = '".$order_id."'")->row;
			$data['status'] = $status['status'];
			$data['order_from'] = $status['order_from'];
			if($status['enable_delivery'] == 0){
				$enable_del = 'Source Fulfilled order';
			} else {
				$enable_del = 'Restaurant Fulfilled Order';
			}
			$data['enable_delivery'] = $enable_del;
			$data['order_instructions'] = $status['order_instructions'];
			$data['order_otp'] = $status['order_otp'];

			// echo'<pre>';
			// print_r($data['order_from']);
			// exit;
			$data['grand_total'] = $status['gross_amount'];
			$data['total_qty'] = $total_qty;
			$data['total_item'] = $total_items;

			$final_data['rider_data'] = array();
			$rider_datass = $this->db->query("SELECT * FROM `oc_werafood_order_rider` WHERE order_id = '".$order_id."'");
			if($rider_datass->num_rows > 0){
				$rider_data = $rider_datass->row;
				$final_data['rider_data'] = array(
					'rider_name' => $rider_data['rider_name'],
					'rider_number' => $rider_data['rider_number'],
					'rider_status' => $rider_data['rider_status'],
					'time_to_arrive' => $rider_data['time_to_arrive'],
				);
			}


		}
		//echo $status['status'];exit;
		$foodready_times = array(
			'10' => '10',
			'15' => '15',
			'20' => '20',
			'25' => '25',
			'30' => '30',
			'35' => '35',
			'40' => '40',
			'45' => '45',
			'50' => '50',
			'55' => '55',
			'60' => '60',
		);
		

		$reject_reasons = array(
			'1'  => 'Items out of stock.',
			'2'  => 'No delivery boys available.',
			'3'  => 'Nearing closing time',
			'4'  => 'Out of Subzone/Area',
			'5'  => 'Kitchen is Full',
		);

		$data['foodready_time'] = $foodready_times;
		$data['reject_reason'] = $reject_reasons;

		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['final_datas'] = $final_data;

		$data['action_comp'] = $this->url->link('catalog/complimentary/complimentary_direct', 'token=' . $this->session->data['token'] . $url, true);

		$data['action'] = $this->url->link('catalog/settlement/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/urbanpiper_itemdata', $data));
	}*/

	public function getlist(){
		$url = '';
		
		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		} else{
			$data['order_id'] = '';
		}
		$data['final_datas'] = array();
		$final_data = array();
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$results = $this->db->query("SELECT * FROM `oc_order_item_app` WHERE online_order_id = '".$order_id."'")->rows;
			$total_qty = 0;
			$total_items = 0;
			foreach ($results as $result) {
				$total_qty = $total_qty + $result['qty'];
				$final_data['item_data'][] = array(
					'order_id' => $result['online_order_id'],
					'item_quantity' => $result['qty'],
					'item_unit_price' => number_format($result['price'],2),
					'subtotal' => number_format($result['total'],2),
					'item_name'        => strip_tags(html_entity_decode($result['item'], ENT_QUOTES, 'UTF-8')),
					'gst' =>number_format($result['gst'],2) ,
					'gst_val' => number_format($result['gst_val'],2),
					'grand_tot' => number_format($result['grand_tot'],2),
				);
				$total_items++;
			}

			$data['show_accept'] = 0;
			$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Acknowledged' AND `order_id`  = '".$this->db->escape($order_id)."'    ORDER BY id desc Limit 1");
			if ($check_error->num_rows > 0) {
				if($check_error->row['failure'] == 3){
					$data['show_accept'] = 1;
				}
			}

			$data['show_cancelled'] = 0;
			$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Cancelled'  AND `order_id`  = '".$this->db->escape($order_id)."' ORDER BY id desc Limit 1");
			if ($check_error->num_rows > 0) {
				if($check_error->row['failure'] == 3){
					$data['show_cancelled'] = 1;
				}
			}

			$data['show_food_ready'] = 0;
			$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'Food Ready'  AND `order_id`  = '".$this->db->escape($order_id)."' ORDER BY id desc Limit 1");
			if ($check_error->num_rows > 0) {
				if($check_error->row['failure'] == 3){
					$data['show_food_ready'] = 1;
				}
			}

			$status = $this->db->query("SELECT  * FROM oc_orders_app WHERE online_order_id = '".$order_id."'")->row;
			$data['swiggy_cancel_msg'] = '';
			if(($status['swiggy_cancel'] != '') && ($status['swiggy_cancel_show_msg'] == 0)){
				$this->db->query("UPDATE oc_orders_app SET online_order_status = 'Cancelled' ,swiggy_cancel = 'You will get a call from swiggy' ,swiggy_cancel_show_msg = '1' WHERE online_order_id = '".$status['online_order_id']."' ");
				$data['swiggy_cancel_msg']= $status['swiggy_cancel'];
			}
			$data['status'] = $status['online_order_status'];
			$data['ftotalvalue'] = $status['ftotalvalue'];
			$data['coupon'] = $status['discount_reason'];
			$data['zomato_order_id'] = $status['zomato_order_id'];
			$data['zomato_order_type'] = $status['zomato_order_type'];
			$data['online_deliery_type'] = $status['online_deliery_type'];
			$data['enable_delivery'] = $status['online_order_type_placed_delie'];
			$data['online_channel'] = $status['online_channel'];
			$data['order_instructions'] = $status['online_instruction'];
			$data['packaging'] =number_format($status['packaging'],2);
			$data['delivery_charge'] =number_format($status['delivery_charge'],2);
			$data['total_charge'] =number_format($status['total_charge'],2);
			$data['grand_total'] = number_format($status['grand_tot'],2);
			$data['total_qty'] = $total_qty;
			$data['total_item'] = $total_items;
			$data['zomato_discount'] = $status['zomato_discount'];;
			//$data['Merchant_Sponsored_Discount'] = ($status['ftotalvalue'] - $status['online_instruction']);
			$data['Merchant_Sponsored_Discount'] = ($status['ftotalvalue'] - $status['zomato_discount']);
			//$customer_data = $this->db->query("SELECT * FROM `oc_customer_app` WHERE customer_id = '".$status['cust_id']."'")->row;
			$customer_address_data = $this->db->query("SELECT * FROM `oc_address_app` WHERE customer_id = '".$status['cust_id']."'AND online_order_id = '".$order_id."'")->row;
			$final_data['customer_data'] = array(
				'name' => $status['firstname'],
				'phone_number' => $status['telephone'],
				'email' => $status['email'],
				'address' => $customer_address_data['address_1'].','.$customer_address_data['address_2'].','.$customer_address_data['city'].','.$customer_address_data['sub_locality'].','.$customer_address_data['postcode'],

			);
			$data['zomato_rider_otp'] = $status['zomato_rider_otp'];
			$final_data['rider_data'] = array();
			$rider_datass = $this->db->query("SELECT * FROM `oc_urbanpiper_rider` WHERE real_order_id = '".$order_id."' ORDER BY id DESC");
			if($rider_datass->num_rows > 0){
				$inc = 0;
				foreach ($rider_datass->rows as $rider_data) {
					$rider_numbers = '';
					$order_color = '';
					if($inc == 0){ 
						$order_color = "notification-message-unread";
					} 
					if( $rider_data['rider_alt_phone'] != ''){
						$rider_numbers = $rider_data['rider_phone'].'/'.$rider_data['rider_alt_phone'];
					} else {
						$rider_numbers = $rider_data['rider_phone'];
					}
					$final_data['rider_data'][] = array(
						'order_color' =>$order_color,
						'rider_name' => $rider_data['rider_name'].'  '.'('.$rider_data['channel_name'].')',
						'rider_status' => $rider_data['rider_status'],
						'rider_numbers' =>$rider_numbers,
					);
					$inc++;
				}
			}
			$final_data['urbanpiper_order_status'] = array();
			$order_status_urban_sql = $this->db->query("SELECT * FROM `oc_urbunpiper_order_status` WHERE urbanpiper_order_id = '".$order_id."' ORDER BY id DESC");
			if($order_status_urban_sql->num_rows > 0){
				 $increment = 0 ;
				foreach ($order_status_urban_sql->rows as $order_status_urban_sql) {
					$order_color = '';
					if($increment == 0){ 
						$order_color = "notification-message-unread";
					} 
					$final_data['urbanpiper_order_status'] [] = array(
						'order_color' =>$order_color,
						'timestamp' => $order_status_urban_sql['timestamp'],
						'new_state' => $order_status_urban_sql['new_state'],
						'message' =>ucwords(str_replace('_', ' ', $order_status_urban_sql['message'])),
					);
					$increment++;
				}
			}
			$reject_reasons = array(
				'item_out_of_stock'  => 'Items Out Of Stock.',
				'store_closed'  => 'Store Closed',
				'store_busy'  => 'Store Busy',
				'rider_not_available'  => 'Rider Not Available',
				'out_of_delivery_radius'  => 'Out Of Delivery Radius',
				'connectivity_issue'  => 'Connectivity Issue',
				'total_missmatch'  => 'Total Missmatch',
				'invalid_item'  => 'Invalid Item',
				'unspecified'  => 'Unspecified',
			);
			$data['reject_reason'] = $reject_reasons;
			//echo "<pre>";print_r($final_data['rider_data']);exit;
		}
		$data['final_datas'] = $final_data;
		$data['action_comp'] = $this->url->link('catalog/complimentary/complimentary_direct', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/settlement/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/urbanpiper_itemdata', $data));
	}

	
}