<?php
class ControllerCatalogStockReportLoose extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockreportloose');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/stockreportloose');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockreportloose', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['item_name'])){
			$data['item_name'] = $this->request->post['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('Y-m-d');
		}

		if(isset($this->request->post['item_id'])){
			$data['item_id'] = $this->request->post['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		if(isset($this->request->post['unit_id'])){
			$data['unit_id'] = $this->request->post['unit_id'];
		}
		else{
			$data['unit_id'] = '';
		}

		if(isset($this->request->post['type'])){
			$data['type'] = $this->request->post['type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->post['store'])){
			$data['store_id'] = $this->request->post['store'];
			$store_id = $this->request->post['store'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = '';
		$availabe_qty = 0;
		$final_data = array();
		//echo $store_id;
		if(!empty($this->request->post['store']) && !empty($data['type'])){
			$start_date = date('Y-m-d', strtotime($data['startdate']));
			$sql = "SELECT * FROM oc_brand WHERE type_id = '".$data['type']."'";

			if(!empty($this->request->post['item_id']) && !empty($this->request->post['item_name'])){
				$sql .= " AND type_id = '".$this->request->post['item_id']."' AND brand = '".$this->request->post['item_name']."'";
			} 

			$brand_datas = $this->db->query($sql)->rows;
			$qtysss = 0;
			$pre_qty = 0;
			
			foreach($brand_datas as $brand_data){
				$pre_date = date('Y-m-d', strtotime($start_date . "-1 day"));
				$last_stock = $this->db->query("SELECT closing_balance,purchase_size,loose_status FROM purchase_test WHERE item_code = '".$brand_data['brand_id']."' AND store_id = '".$store_id."' AND invoice_date = '".$pre_date."' ")->rows;
				$total_pre_qty = 0;
				foreach ($last_stock as $pkey => $pvalue) {
					$total_pre_qty = $total_pre_qty + $pvalue['loose_status'];
				}
				$purchase_datass = $this->db->query("SELECT * FROM `oc_purchase_loose_items_transaction` plit LEFT JOIN `oc_purchase_loose_transaction` plt ON (plit.`p_id` = plt.`id`)  WHERE  plit.type = '".$brand_data['type_id']."' AND plit.description_id = '".$brand_data['brand_id']."' AND invoice_date = '".$start_date."'  ")->rows;
				$pre_qtys = 0;
				$qtys = 0;
				$loose_qty = 0;

				foreach ($purchase_datass as $pkey => $pvalue) {
					if (strpos($pvalue['unit'], 'ML') !== false) {
						$qtysss = str_replace("ML","",$pvalue['unit']);
					} elseif (strpos($pvalue['unit'], 'Ltr') !== false) {
						$qtydd = str_replace("Ltr","",$pvalue['unit']);
						$qtysss = $qtydd * 1000;
					}
					$loose_qty = $qtysss * $pvalue['qty'];
					$qtys = $qtys + $loose_qty;
				}
				if($data['type'] == '3'){
					$is_liq = '0';
				} else {
					$is_liq = '1';
				}

				$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$brand_data['brand_id']."'  AND is_liq = '".$is_liq."'");
				/*echo "<pre>";
				print_r($item_data->rows);
				exit;
				echo "<br>";*/
				$sale_qty = 0;
				$brand_qtys_sale = 0;
				foreach($item_data->rows as $key){
					/*echo'inn';
					echo "<br>";*/
					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '".$is_liq."' AND oi.`bill_date` = '".$start_date."' AND code = '".$key['item_code']."'  GROUP BY code");
					
					if($order_item_data->num_rows > 0){
						if($order_item_data->row['code'] != ''){
							$brand_size = $this->db->query("SELECT size FROM oc_brand_type WHERE id = '".$key['quantity']."'  AND type_id = '".$brand_data['type_id']."' AND loose = '1' ");
							if($brand_size->num_rows > 0){
								if (strpos($brand_size->row['size'], 'ML') !== false) {
									$brand_qtys = (int)$brand_size->row['size'];
									$brand_qtys_sale = $brand_qtys * $order_item_data->row['sale_qty'];
									// $total_pre_qty = $total_pre_qty + $pre_qty;
								} 
							}
							$sale_qty =  $sale_qty + $brand_qtys_sale;
						} else{
							$sale_qty = 0;
						}
					}
				}

				$availabe_qty = $total_pre_qty + $qtys -  $sale_qty;

				$final_data[] = array(
					'item_name'			=> $brand_data['brand'],
					'loose_today'		=> $qtys,
					'avail_quantity' 	=> $availabe_qty,
					'pre_qty' 	=> $total_pre_qty,
					'sale_todays' 	=> $sale_qty,
				);
			}
		}
		$data['final_data'] = $final_data;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		$data['typess'] = $this->db->query("SELECT * FROM oc_brand GROUP by type_id")->rows;


		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['action'] = $this->url->link('catalog/stockreportloose', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockreportloose', $data));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	 public function autocomplete() {
	 	$json = array();
	 	if(isset($this->request->get['filter_name'])){
	 		$results = $this->db->query("SELECT type_id, brand FROM oc_brand WHERE brand LIKE '%".$this->request->get['filter_name']."%' LIMIT 0,10")->rows;
	 		foreach($results as $key){
	 			$json[] = array(
	 						'id'	=> $key['type_id'],
	 						'name'  => $key['brand']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	}

	 public function getUnit() {
	 	$json = array();
	 	if(isset($this->request->get['filter_id'])){
			$results = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$this->request->get['filter_id']."' AND loose = 1")->rows;
			$jsons[] = array(
							'id' => '',
							'name' => 'Please Select'
						);
	 		foreach($results as $key){
	 			$jsons[] = array(
	 						'id'	=> $key['id'],
	 						'name'  => $key['size']
	 					  );
	 		}
	 	}
	 	$json['units'] = $jsons;
	 	$this->response->setOutput(json_encode($json));
	}
}