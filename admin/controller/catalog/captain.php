<?php
class ControllerCatalogCaptain extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/captain');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/captain');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/captain');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/captain');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_captain->addCaptain($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_c_name'])) {
				$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
			}

			if (isset($this->request->get['filter_captain_id'])) {
				$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/captain');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/captain');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_captain->editCaptain($this->request->get['captain_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_c_name'])) {
				$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
			}

			if (isset($this->request->get['filter_c_code'])) {
				$url .= '&filter_c_code=' . $this->request->get['filter_c_code'];
			}

			// if (isset($this->request->get['filter_waiter_id'])) {
			// 	$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			// }

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/captain');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/captain');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $captain_id) {
				$this->model_catalog_captain->deleteCaptain($captain_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_c_name'])) {
				$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
			}

			if (isset($this->request->get['filter_captain_id'])) {
				$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_c_name'])) {
			$filter_c_name = $this->request->get['filter_c_name'];
		} else {
			$filter_c_name = null;
		}

		if (isset($this->request->get['filter_captain_id'])) {
			$filter_captain_id = $this->request->get['filter_captain_id'];
		} else {
			$filter_captain_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_c_name'])) {
			$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
		}

		if (isset($this->request->get['filter_captain_id'])) {
			$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/captain/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/captain/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_c_name' => $filter_c_name,
			'filter_captain_id' => $filter_captain_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_captain->getTotalCaptain();

		$results = $this->model_catalog_captain->getCaptains($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$data['captains'][] = array(
				'captain_id' => $result['captain_id'],
				'c_name'        => $result['c_name'],
				'c_code'        => $result['c_code'],
				'edit'        => $this->url->link('catalog/captain/edit', 'token=' . $this->session->data['token'] . '&captain_id=' . $result['captain_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_c_name'])) {
			$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
		}

		if (isset($this->request->get['filter_captain_id'])) {
			$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        $data['sort_c_name'] = $this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . '&sort=c_name' . $url, true);
        
		$url = '';

		if (isset($this->request->get['filter_c_name'])) {
			$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
		}

		if (isset($this->request->get['filter_captain_id'])) {
			$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
		}	

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_c_name'] = $filter_c_name;
		$data['filter_captain_id'] = $filter_captain_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/captain_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['waiter_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['c_name'])) {
			$data['error_c_name'] = $this->error['c_name'];
		} else {
			$data['error_c_name'] = array();
		}

		if (isset($this->error['c_code'])) {
			$data['error_c_code'] = $this->error['c_code'];
		} else {
			$data['error_c_code'] = array();
		}

		if (isset($this->error['captain_id'])) {
			$data['error_captain_id'] = $this->error['captain_id'];
		} else {
			$data['error_captain_id'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_c_name'])) {
			$url .= '&filter_c_name=' . $this->request->get['filter_c_name'];
		}

		if (isset($this->request->get['filter_captain_id'])) {
			$url .= '&filter_captain_id=' . $this->request->get['filter_captain_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['captain_id'])) {
			$data['action'] = $this->url->link('catalog/captain/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/captain/edit', 'token=' . $this->session->data['token'] . '&captain_id=' . $this->request->get['captain_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/captain', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['captain_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$captain_info = $this->model_catalog_captain->getCaptain($this->request->get['captain_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['c_code'])) {
			$data['c_code'] = $this->request->get['c_code'];
		} elseif (!empty($captain_info)) {
			$data['c_code'] = $captain_info['c_code'];
		} else {
			$data['c_code'] = '';
		}

		if (isset($this->request->post['c_name'])) {
			$data['c_name'] = $this->request->post['c_name'];
		} elseif (!empty($captain_info)) {
			$data['c_name'] = $captain_info['c_name'];
		} else {
			$data['c_name'] = '';
		}


		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/captain_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/captain')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['c_name']) < 2) || (utf8_strlen($this->request->post['c_name']) > 255)) {
			$this->error['c_name'] = 'Enter valid Name';
		} else {
			if(isset($this->request->get['captain_id'])){
				$is_exist = $this->db->query("SELECT `captain_id` FROM `oc_captain` WHERE `c_code` = '".$datas['c_code']."' AND `captain_id` <> '".$this->request->get['captain_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['c_code'] = 'Code Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `captain_id` FROM `oc_captain` WHERE `c_code` = '".$datas['c_code']."' ");
				if($is_exist->num_rows > 0){
					$this->error['c_code'] = 'Code Already Exists';
				}
			}
		}

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/captain')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_c_name'])) {
			$this->load->model('catalog/captain');

			$filter_data = array(
				'filter_c_name' => $this->request->get['filter_c_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_captain->getCaptains($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'captain_id' => $result['captain_id'],
					'c_name'        => strip_tags(html_entity_decode($result['c_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['c_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}