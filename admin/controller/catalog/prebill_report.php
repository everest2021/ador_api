<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class ControllerCatalogPrebillreport extends Controller {
	private $error = array();

	public function index() {
		$this->getList();
	}

	public function custom_sort($a, $b) {
		$c = $b['Year'] - $a['Year'];
		$c .= $a['Name of Foal'] > $b['Name of Foal'];
		return $c;
	}


	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/billwiseitem_report');
		$this->document->setTitle($this->language->get('Previous Bill Print'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Previous Bill Print'),
			'href' => $this->url->link('catalog/prebill_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_billno'])){
			$data['billno'] = $this->request->post['filter_billno'];
		}
		else{
			$data['billno'] = '';
		}


		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['order_datas'] = array();
		$order_datas = array();
		//$data['cancelamount'] = '';
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){

		$startdate = strtotime($this->request->post['filter_startdate']);
		$enddate =  strtotime($this->request->post['filter_enddate']);

		$billno =  $this->request->post['filter_billno'];
		
		$start_date = date('Y-m-d', $startdate);
		$end_date = date('Y-m-d', $enddate);


		date_default_timezone_set("Asia/Kolkata");
		// $last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info_report` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		// $last_open_dates = $this->db->query($last_open_date_sql);
		// if($last_open_dates->num_rows > 0){
		// 	$last_open_date = $last_open_dates->row['bill_date'];
		// } else {
		// 	$last_open_date = date('Y-m-d');
		// }

		//$order_datas = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '0' AND oi.`bill_status` = '1' AND oi.`bill_date` = '".$last_open_date."' GROUP BY oit.`order_id` ORDER BY oi.`order_no` ")->rows;
		$sql = "SELECT * FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.`order_id` = oit.`order_id`) WHERE 1=1";
		
		if (!empty($start_date)) {
				$sql .= " AND oi.`bill_date` >= '" . $this->db->escape($start_date) . "'";
			}
			if (!empty($end_date)) {
				$sql .= " AND oi.`bill_date` <= '" . $this->db->escape($end_date) . "'";
			}

			if (!empty($billno)) {
				$sql .= " AND oi.`order_id` = '" . $this->db->escape($billno) . "'";
			}
			
			$sql .= " AND oit.`cancelstatus` = '0' AND oi.`bill_status` = '1' AND oi.`cancel_status` = '0'";

			$sql .= " GROUP BY oit.`order_id` ORDER BY oi.`order_no` ";

			$order_datas = $this->db->query($sql)->rows;

		foreach ($order_datas as $okey => $ovalue) {
			$food_order_item_datas = $this->db->query("SELECT `billno` as food_bill_no FROM `oc_order_items_report` oit WHERE oit.`order_id` = '".$ovalue['order_id']."' AND `is_liq` = '0' AND `cancelstatus` = '0' ORDER BY oit.`order_id` DESC LIMIT 1");
			$food_bill_no = 0;
			if($food_order_item_datas->num_rows > 0){
				$food_order_item_data = $food_order_item_datas->row;
				$food_bill_no = $food_order_item_data['food_bill_no'];
			}
			$liq_order_item_datas = $this->db->query("SELECT `billno` as liq_bill_no FROM `oc_order_items_report` oit WHERE oit.`order_id` = '".$ovalue['order_id']."' AND `is_liq` = '1' AND `cancelstatus` = '0' ORDER BY oit.`order_id` DESC LIMIT 1");
			$liq_bill_no = 0;
			if($liq_order_item_datas->num_rows > 0){
				$liq_order_item_data = $liq_order_item_datas->row;
				$liq_bill_no = $liq_order_item_data['liq_bill_no'];
			}
			$order_datas[$okey]['food_bill_no'] = $food_bill_no;
			$order_datas[$okey]['liq_bill_no'] = $liq_bill_no;

			$order_datas[$okey]['href'] = $this->url->link('catalog/prebill_report/duplicate_print', 'token=' . $this->session->data['token'].'&order_id=' . $ovalue['order_id'].'&duplicate=1' , true);
		}	
	}

		//usort($order_datas, "custom_sort");

		// echo'<pre>';
		// print_r($order_datas);
		// exit;
		$sort_order = array();

		foreach ($order_datas as $key => $value) {
  			$sort_order[$key] = $value['food_bill_no'];
    	}
 
 		array_multisort($sort_order, SORT_ASC, $order_datas);
		$data['order_datas'] = $order_datas;
		

		$testingliq = $this->db->query("SELECT * FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_status` = '1' AND oit.`is_liq` = '1' AND oit.`cancelstatus` = '0' AND oi.`pay_method` = '1' AND oi.`bill_date` = '".date('Y-m-d')."' AND oi.`liq_cancel` <> '1' AND oit.`ismodifier` = '1' GROUP BY oit.`order_id`")->rows;
		$data['testingliq'] = $testingliq;

		if(!empty($this->session->data['tablet'])){
			$data['tablet'] = $this->session->data['tablet'];
		} else{
			$data['tablet'] = '';
		}

		if(isset($this->request->get['qwerty'])){
			$data['qwerty'] = $this->request->get['qwerty'];
		} else{
			$data['qwerty'] = '';
		}
		$data['cancel_bill'] = $this->user->getCanceBill();
		$data['edit_bill'] = $this->user->getEditBill();
		$data['duplicate_bill'] = $this->user->getDuplicateBill();
		// echo'<pre>';
		// print_r($printername);
		// exit;
		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$data['action'] = $this->url->link('catalog/prebill_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['duplicate_bill'] = $this->url->link('catalog/prebill_report/duplicate_print', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = 'Previous Bill Print';

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/prebill_report', $data));
	}

	

	public function getdatafood(){
		$json = array();
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if(isset($this->request->get['orderidfood'])) {
			$anss = $this->db->query("SELECT * FROM oc_order_items_report WHERE order_id ='".$this->request->get['orderidfood']."' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY `is_liq` ")->rows;
			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}

				if($result['code'] != ''){
					$json[] = array(
						'name' => $result['name'],
						'qty' => $result['qty'],
						'rate' => $result['rate'],
						'amt' => $result['amt'],
					);
				}
			}
		}
		$this->response->setOutput(json_encode($json));
	}


	public function duplicate_print() {
		// echo'inn';
		// exit();
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$ans = $this->db->query("SELECT * FROM oc_order_info_report WHERE order_id = '".$order_id."'")->row;
		// 	echo'<pre>';
		// print_r($ans);
		// exit;
			if(isset($this->request->get['duplicate'])){
				$duplicate = $this->request->get['duplicate'];
			} else{
				$duplicate = '0';
			}

			if(isset($this->request->get['advance_id'])){
				$advance_id = $this->request->get['advance_id'];
			} else{
				$advance_id = '0';
			}

			if(isset($this->request->get['advance_amount'])){
				$advance_amount = $this->request->get['advance_amount'];
			} else{
				$advance_amount = '0';
			}

			if(isset($this->request->get['grand_total'])){
				$grand_total = $this->request->get['grand_total'];
			} else{
				$grand_total = '0';
			}
			$orderno = '0';

			if(($ans['bill_status'] == 0 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
				date_default_timezone_set('Asia/Kolkata');
				$this->load->language('customer/customer');
				$this->load->model('catalog/order');
				$merge_datas = array();
				$this->document->setTitle('BILL');
				$te = 'BILL';
				
				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date = date('Y-m-d');
				}

				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
				if($last_open_dates_liq->num_rows > 0){
					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
				} else {
					$last_open_date_liq = date('Y-m-d');
				}

				$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info_report` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
				$last_open_dates_order = $this->db->query($last_open_date_sql_order);
				if($last_open_dates_order->num_rows > 0){
					$last_open_date_order = $last_open_dates_order->row['bill_date'];
				} else {
					$last_open_date_order = date('Y-m-d');
				}

				if($ans['order_no'] == '0'){
					$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info_report` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
					$orderno = 1;
					if(isset($orderno_q['order_no'])){
						$orderno = $orderno_q['order_no'] + 1;
					}

					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
					if($kotno2->num_rows > 0){
						$kot_no2 = $kotno2->row['billno'];
						$kotno = $kot_no2 + 1;
					} else{
						$kotno = 1;
					}

					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
					if($kotno1->num_rows > 0){
						$kot_no1 = $kotno1->row['billno'];
						$botno = $kot_no1 + 1;
					} else{
						$botno = 1;
					}

					// $this->db->query("UPDATE oc_order_items_report SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
					// $this->db->query("UPDATE oc_order_items_report SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

					// //$ansbb = $this->db->query("SELECT * FROM oc_order_info_report WHERE order_id = '".$order_id."'")->row;
					// if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
					// 	$update_sql = "UPDATE `oc_order_info_report` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
					// } else{
					// 	$update_sql = "UPDATE `oc_order_info_report` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
					// }
					// $apporder = $this->db->query("SELECT app_order_id FROM oc_order_info_report WHERE order_id = '".$order_id."'")->row;
					// if($apporder['app_order_id'] != '0'){
					// 	$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
					// }
					// $this->db->query($update_sql);
				}

				// if($advance_id != '0'){
				// 	$this->db->query("UPDATE oc_order_info_report SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
				// }
				$anss = $this->db->query("SELECT * FROM oc_order_items_report WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

				$testfood = array();
				$testliq = array();
				$testtaxvalue1food = 0;
				$testtaxvalue1liq = 0;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items_report` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items_report` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				
				$infosl = array();
				$infos = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}
						if($result['is_liq']== 0){
							$infos[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items_report WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infosl[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items_report WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}
				
				$merge_datas = array();
				if($orderno != '0'){
					$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info_report WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
				}
				
				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else {
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else {
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}

				if($duplicate == '1'){
					$this->db->query("UPDATE `oc_order_info_report` SET duplicate = '1', duplicate_time = '".date('h:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE `order_id` = '".$order_id."'");
					$duplicatebilldata = $this->db->query("SELECT `order_no`, grand_total, item_quantity, `login_name`, `duplicate_time` FROM `oc_order_info_report` WHERE `duplicate` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND day_close_status = '0' AND order_id = '".$order_id."'");
					$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
					$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
					$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
					if ($setting_value_link_1 != '') {
						$text = "Duplicate%20Bill%20-%20Ref%20No%20:".$duplicatebilldata->row['order_no'].",%20Qty%20:".$duplicatebilldata->row['item_quantity'].",%20Amt%20:".$duplicatebilldata->row['grand_total'].",%20login%20name%20:".$duplicatebilldata->row['login_name'].",%20Time%20:".$duplicatebilldata->row['duplicate_time'];
						$link = $link_1."&phone=".$setting_value_number."&text=".$text;
						//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
						//echo $link;exit;
						$ret = file($link);
					} 
				}
				
				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				if($ans['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				$gtotal = ceil($gtotal);

				$printtype = '';
				$printername = '';

				if ($printtype == '' || $printername == '' ) {
					$printtype = $this->user->getPrinterType();
					$printername = $this->user->getPrinterName();
					$bill_copy = 1;
				}

				if ($printtype == '' || $printername == '' ) {
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$printtype = $locationData['bill_printer_type'];
						$printername = $locationData['bill_printer_name'];
						$bill_copy = $locationData['bill_copy'];
					} else{
						$printtype = '';
						$printername = '';
						$bill_copy = 1;
					}
				}
				
				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}

				try {
				    if($printtype == 'Network'){
				 		$connector = new NetworkPrintConnector($printername, 9100);
				 	} else if($printtype == 'Windows'){
				 		$connector = new WindowsPrintConnector($printername);
				 	} else {
				 		$connector = '';
				 	// 	$this->db->query("UPDATE oc_order_info_report SET printstatus = 1 WHERE order_id = '".$order_id."'");
						// $this->db->query("UPDATE oc_order_items_report SET printstatus = 1 WHERE order_id = '".$order_id."'");
				 	}
				 	if($connector != ''){
					    $printer = new Printer($connector);
					    $printer->selectPrintMode(32);

					   	$printer->setEmphasis(true);
					   	for($i = 1; $i <= $bill_copy; $i++){
						   	$printer->setTextSize(2, 1);
						   	$printer->setJustification(Printer::JUSTIFY_CENTER);
						   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
						    $printer->feed(1);
						    $printer->setTextSize(1, 1);
						    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
						    if($ans['bill_status'] == 1 && $duplicate == '1'){
						    	$printer->feed(1);
						    	$printer->text("Duplicate Bill");
						    }
						    $printer->setJustification(Printer::JUSTIFY_CENTER);
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->setEmphasis(true);
						   	$printer->setTextSize(1, 1);
							if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
								
							}
							else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
							 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
								$printer->feed(1);
							}
							else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
							 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
								$printer->feed(1);
							}
							else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
							 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
							    $printer->feed(1);
							}
							else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
							 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
							    $printer->feed(1);
							}
							else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
							 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
							    $printer->feed(1);
							}
							else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
							 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
							    $printer->feed(1);
							}
							else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
							    $printer->text("Name :".$ans['cust_name']."");
							    $printer->feed(1);
							}
							else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
							    $printer->text("Mobile :".$ans['cust_contact']."");
							    $printer->feed(1);
							}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
							    $printer->text("Address : ".$ans['cust_address']."");
							    $printer->feed(1);
							}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
							    $printer->text("Gst No :".$ans['gst_no']."");
							    $printer->feed(1);
							}else{
							    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
							    $printer->feed(1);
							    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
							    $printer->feed(1);
							}
							if($ans['complimentary_status'] != '1'){

								$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
							} else {
								$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$ans['order_no']);

							}
						    $printer->setEmphasis(true);
						   	$printer->setTextSize(1, 1);
						   	if($infos){
						   			
						   		$printer->feed(1);
						   		$printer->setJustification(Printer::JUSTIFY_CENTER);
						   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('h:i:s'));
						   		$printer->feed(1);
						   		if($ans['complimentary_status'] != '1'){
									$printer->text("Ref no: ".$orderno."");
								} else {
									$printer->text("Ref no: ".$ans['order_no']."");
								}
						   		
						   		$printer->feed(1);
						   		$printer->setTextSize(1, 1);
						   		$printer->setJustification(Printer::JUSTIFY_LEFT);
						    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
						    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
						    	$printer->feed(1);
						   		$printer->setTextSize(1, 1);
						   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
							    $printer->feed(1);
						   		$printer->setEmphasis(false);
							    $printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
								$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
								$printer->feed(1);
						   	 	$printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setEmphasis(false);
								$total_items_normal = 0;
								$total_quantity_normal = 0;
							    foreach($infos as $nkey => $nvalue){
							    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
							    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
							    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
							    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
							    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
							    	$printer->feed(1);
						    	 	if($modifierdatabill != array()){
									    	foreach($modifierdatabill as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
								    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
											    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
							    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

							    }
							    $printer->text("----------------------------------------------");
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							    $printer->text("----------------------------------------------");
								$printer->feed(1);
								foreach($testfoods as $tkey => $tvalue){
									$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
							    	$printer->feed(1);
								}

								$printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
								if($ans['complimentary_status'] != '1'){
									if($ans['fdiscountper'] != '0'){
										$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
										$printer->feed(1);
									} elseif($ans['discount'] != '0'){
										$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
										$printer->feed(1);
									}
								}
								
								if($ans['complimentary_status'] != '1'){
									$printer->text(str_pad("",33)."SCGST :".$csgst."");
									$printer->feed(1);
									$printer->text(str_pad("",33)."CCGST :".$csgst."");
									$printer->feed(1);
									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
											$printer->feed(1);
											$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
										}else{
											$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
											$printer->feed(1);
											$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$netamountfood = ($ans['ftotal'] - ($disamtfood));
										} else{
											$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
										}
									}

									$printer->setEmphasis(true);
									$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
									$printer->setEmphasis(false);
								} else {
									$printer->text(str_pad("",33)."SCGST :0");
									$printer->feed(1);
									$printer->text(str_pad("",33)."CCGST :0");
									$printer->feed(1);
									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$printer->text(str_pad("",34)."SCRG :0");
											$printer->feed(1);
											//$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											$netamountfood = 0;
										}else{
											$printer->text(str_pad("",34)."SCRG :0");
											$printer->feed(1);
											//$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											$netamountfood = 0;
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											//$netamountfood = ($ans['ftotal'] - ($disamtfood));
											$netamountfood = 0;
										} else{
											//$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
											$netamountfood = 0;
										}
									}
									$printer->setEmphasis(true);
									$printer->text(str_pad("",29)."Net total :0");
									$printer->setEmphasis(false);


								}
							}
							
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->setEmphasis(true);
						   	$printer->setTextSize(1, 1);
					   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
					   			$printer->setJustification(Printer::JUSTIFY_CENTER);
								$printer->feed(1);	
								$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
								if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
									$printer->feed(1);				   		
							   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
						   		}
						   		$printer->feed(1);	
						   		$printer->setJustification(Printer::JUSTIFY_LEFT);
						   	}
						   	if($infosl){
						   		$printer->feed(1);
						   		$printer->setJustification(Printer::JUSTIFY_CENTER);
						   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('h:i:s'));
						   		$printer->feed(1);
						   		$printer->text("Ref no: ".$orderno."");
						 		$printer->feed(1);
						 		$printer->setEmphasis(true);
						   		$printer->setTextSize(1, 1);
						   		$printer->setJustification(Printer::JUSTIFY_LEFT);
						    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
						    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
						    	$printer->feed(1);
						   		$printer->setTextSize(1, 1);
						   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
							    $printer->feed(1);
						   		$printer->setEmphasis(false);
							    $printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
								$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
								$printer->feed(1);
						    	$printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setEmphasis(false);
								$total_items_liquor_normal = 0;
							    $total_quantity_liquor_normal = 0;
							    foreach($infosl as $nkey => $nvalue){
							    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
							    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
							    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
							    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
							    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
							    	$printer->feed(1);
						    	 	if($modifierdatabill != array()){
								    	foreach($modifierdatabill as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $nvalue['id']){
							    				foreach($value as $modata){
							    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
										    		$printer->feed(1);
									    		}
									    	}
							    		}
							    	}

							    	$total_items_liquor_normal ++;
							    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
							    }
							    $printer->text("----------------------------------------------");
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							    $printer->text("----------------------------------------------");
								$printer->feed(1);
								foreach($testliqs as $tkey => $tvalue){
									$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
							    	$printer->feed(1);
								}
								$printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
								if($ans['complimentary_status'] != '1'){
									if($ans['ldiscountper'] != '0'){
										$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
										$printer->feed(1);
									} elseif($ans['ldiscount'] != '0'){
										$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
										$printer->feed(1);
									}
								}
								
								if($ans['complimentary_status'] != '1'){
									$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
									$printer->feed(1);
									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
											$printer->feed(1);
											$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
										} else{
											$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
											$printer->feed(1);
											$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
										} else{
											$netamountliq = ($ans['ltotal'] - ($disamtliq));
										}
									}
									$printer->setEmphasis(true);
									$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								}else {
									$printer->text(str_pad("",35)."VAT :'0'");
									$printer->feed(1);

									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$printer->text(str_pad("",34)."SCRG :'0'");
											$printer->feed(1);
											//$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											$netamountliq = 0;
										} else{
											$printer->text(str_pad("",34)."SCRG :'0'");
											$printer->feed(1);
											//$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											$netamountliq = 0;
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											//$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
											$netamountliq = 0;
										} else{
											//$netamountliq = ($ans['ltotal'] - ($disamtliq));
											$netamountliq = 0;
										}
									}
									$printer->setEmphasis(true);
									$printer->text(str_pad("",29)."Net total :0");
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								}
								
							}
							$printer->feed(1);
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->setEmphasis(true);
							if($ans['advance_amount'] != '0.00'){
								$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
								$printer->feed(1);
							}
							$printer->setTextSize(2, 2);
							$printer->setJustification(Printer::JUSTIFY_RIGHT);
							if($ans['complimentary_status'] != '1'){
								$printer->text("GRAND TOTAL  :  ".$gtotal);
							} else {
								$printer->text("GRAND TOTAL  : 0 ");
							}
							$printer->setTextSize(1, 1);
							$printer->feed(1);
							$printer->text("Delivery Charge:".$ans['dtotalvalue']);
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("----------------------------------------------");
						    $printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						   
							if($merge_datas){
								$printer->feed(2);
								$printer->setJustification(Printer::JUSTIFY_CENTER);
								$printer->text("Merged Bills");
								$printer->feed(1);
								$mcount = count($merge_datas) - 1;
								foreach($merge_datas as $mkey => $mvalue){
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->text("Bill Number :".$mvalue['order_no']."");
									$printer->feed(1);
									$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
									$printer->feed(1);
									$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
									$printer->feed(1);
									if($mvalue['ltotal'] > '0'){
										$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
									}
									$printer->feed(1);
									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
										} else{
											$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
										} else{
											$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
										}
									}
									if($ans['complimentary_status'] != '1'){
										$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
									} else {
										$printer->text("GRAND TOTAL  :  0");
									}
									$printer->feed(1);
									if($ans['parcel_status'] == '0'){
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
										} else{
											$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
										}
									} else{
										if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
											$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
										} else{
											$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
										}
									}
									if($mkey < $mcount){ 
										$printer->text("----------------------------------------------");
										$printer->feed(1);
									}
								}
								$printer->feed(1);
								$printer->text("----------------------------------------------");
								$printer->feed(1);
								$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
								$printer->feed(1);
							}
							$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
							$printer->feed(1);
							if($ans['complimentary_status'] == '1'){
								$printer->text("Complimentary Resion  :  ".$ans['complimentary_resion']);
							} 
							if($this->model_catalog_order->get_settings('TEXT1') != ''){
								$printer->text($this->model_catalog_order->get_settings('TEXT1'));
								$printer->feed(1);
							}
							$printer->feed(1);
							if($this->model_catalog_order->get_settings('TEXT2') != ''){
								$printer->text($this->model_catalog_order->get_settings('TEXT2'));
								$printer->feed(1);
							}
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							if($this->model_catalog_order->get_settings('TEXT3') != ''){
								$printer->text($this->model_catalog_order->get_settings('TEXT3'));
							}
							$printer->feed(2);
							$printer->cut();
						}
						// Close printer //
					    $printer->close();
					    $this->db->query("UPDATE oc_order_info_report SET printstatus = 2 WHERE order_id = '".$order_id."'");
						$this->db->query("UPDATE oc_order_items_report SET printstatus = 2 WHERE order_id = '".$order_id."'");
					}
				} catch (Exception $e) {
				    $this->db->query("UPDATE oc_order_info_report SET printstatus = 1 WHERE order_id = '".$order_id."'");
				    $this->db->query("UPDATE oc_order_items_report SET printstatus = 1 WHERE order_id = '".$order_id."'");
				    $this->session->data['warning'] = $printername." "."Not Working";
				}
				if($ans['bill_status'] == 1 && $duplicate == '1'){
					$this->response->redirect($this->url->link('catalog/prebill_report', 'token=' . $this->session->data['token'],true));
				} else{
					$json = array();
					$json = array(
						'status' => 1,
					);
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			} else {
				$this->session->data['warning'] = 'Bill already printed';
				$this->request->post = array();
				$_POST = array();
				$this->response->redirect($this->url->link('catalog/prebill_report', 'token=' . $this->session->data['token'],true));
			}
		}
		// $this->request->post = array();
		// $_POST = array();
		// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}
	


	
}

	