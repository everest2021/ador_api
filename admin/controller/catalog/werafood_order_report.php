<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');
class ControllerCatalogWerafoodOrderReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle('Online Order Report');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle('Online Order Report');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "Online Order Report",
			'href' => $this->url->link('catalog/werafood_order_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}
			
		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_type'])){
			$data['type'] = $this->request->post['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		if(isset($this->request->post['agg_order_id'])){
			$data['agg_order_id'] = $this->request->post['agg_order_id'];
		}
		else{
			$data['agg_order_id'] = '';
		}

		if(isset($this->request->post['filter_order_status'])){
			$data['filter_order_status'] = $this->request->post['filter_order_status'];
		}else{
			$data['filter_order_status'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['types'] = array(
				'Online' => 'Online',
				'zomato' => 'zomato',
				'swiggy' => 'swiggy',
				'Uber'	=>'Uber',
		);

		$data['urbanpiper_order_status'] = array(
				'Placed' => 'Placed',
				'Acknowledged' => 'Acknowledged',
				'Food Ready' => 'Food Ready',
				'Dispatched' => 'Dispatched',
				'Completed'	=>'Completed',
				'Cancelled'	=>'Cancelled',
		);

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'] ) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$type =  $this->request->post['filter_type'];

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);
			foreach($dates as $date){
				$sql = "SELECT * FROM `oc_orders_app` WHERE (online_order_id != '') ";
					if($data['type'] != ''){
						$sql .= "AND `online_channel` = '".$data['type']."' ";
					}
					if($data['filter_order_status'] != ''){
						$sql .= "AND `online_order_status` = '".$data['filter_order_status']."' ";
					}

					if($data['agg_order_id'] != ''){
						$sql .= "AND `zomato_order_id` = '".$data['agg_order_id']."' ";
					}
					$sql .= "AND `order_date` = '".$date."' ";
					$billdata_sql = $this->db->query($sql);
					$final_data = array();
					if($billdata_sql->num_rows > 0){
						foreach ($billdata_sql->rows as $rkey => $rvalue) {
							$final_data[$rkey] = $rvalue;
							$message_sql = $this->db->query("SELECT `message`,`new_state` FROM `oc_urbunpiper_order_status` WHERE urbanpiper_order_id = '".$rvalue['online_order_id']."' ORDER BY CAST(id AS int) DESC LIMIT 1");
								if($message_sql->num_rows > 0){
									$message_order_status = $message_sql->row['message'];
									$order_status = $message_sql->row['new_state'];
								} else {
									$message_order_status = '';
									$order_status = '';
								}
							
							if($message_sql->row['new_state'] == 'Cancelled'){
								$final_data[$rkey]['message'] = $message_order_status;
							} else {
								$final_data[$rkey]['message'] = '';
							}

							$final_data[$rkey]['new_state'] = $order_status;
						}
						$billdata[$date] = $final_data;
					}
					
			}
		}
		
		$data['billdatas'] = $billdata;
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('catalog/werafood_order_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/werafood_order_report', $data));
	}

	public function prints() {
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$billdatas = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$cancelamount = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}
				

			$discount = 0;
			$grandtotal = 0;
			$vat = 0; 
			$gst = 0; 
			$discountvalue = 0; 
			$afterdiscount = 0; 
			$stax = 0; 
			$roundoff = 0; 
			$total = 0; 
			$advance = 0;
			$foodtotal = 0;
			$liqtotal = 0;
			$fdistotal = 0;
			$ldistotal = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Bill Wise Sales Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			  	$printer->text(str_pad("Ref No",7)."".str_pad("Food",7)."".str_pad("Bar",7)."".str_pad("Disc",7).""
				  		.str_pad("Total",7)."".str_pad("Pay By",8)."T No");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($billdata as $key => $value){
			  		$printer->setJustification(Printer::JUSTIFY_CENTER);
			  		$printer->text($key);
			  		$printer->feed(1);
			  		$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	foreach ($value as $data) {
				  		if($data['food_cancel'] == 1) {
				  			$data['ftotal'] = 0;
				  		}
				  		if($data['liq_cancel'] == 1) {
				  			$data['ltotal'] = 0;
				  		}
				  		if($data['pay_cash'] != '0') {
				  			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Card",8).$data['t_name']);
					  	}else if($data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("OnAc",8).$data['t_name']);
					  	}else if($data['mealpass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Meal Pass",8).$data['t_name']);
					  	}else if($data['pass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pass",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash+Card",8).$data['t_name']);
					  	}else{
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pending",8).$data['t_name']);
					  	}
				  		$printer->feed(1);
				  		  if($data['liq_cancel'] == 1){
				  		  	$vat = 0;
				  		  } else{
				  		  	$vat = $vat + $data['vat'];
				  		  }
				  		  if($data['food_cancel'] == 1){
				  		  	$gst = 0;
				  		  } else{
				  		  	$gst = $gst + $data['gst'];
				  		  }
				  		  $stax = $stax + $data['stax'];
				  		  if($data['food_cancel'] == 1){
				  		  	$data['ftotalvalue'] = 0; 
				  		  } 
				  		  if($data['liq_cancel'] == 1){
				  		  	$data['ltotalvalue'] = 0;
				  		  }
				  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
				  		  if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
				  		  	$total = $grandtotal + $stax;
				  		  } else{
			  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
			  		  	 	$total = $grandtotal + $gst + $vat + $stax; 
				  		  }
				  		  $discountvalue = $discountvalue + $discount;
				  		  $afterdiscount = $total - $discountvalue;
				  		  $roundoff = $roundoff + $data['roundtotal'];
				  		  $advance = $advance + $data['advance_amount'];
				  	}
				}
			  	$printer->setEmphasis(false);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Bill Total :",20)."".$grandtotal);
			  	$printer->setEmphasis(false);
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("O- Chrg Amt (+) :",20)."0");
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("Vat Amt (+) :",20)."".$vat);
			  	$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("S- Chrg Amt (+) :",20)."".$stax);
	  			$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("GST Amt (+) :",20)."".$gst);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("SBC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("R-Off Amt (+) :",20)."".$roundoff/2);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Total :",20)."".($total + $roundoff));
			  	$printer->feed(1);
			  	$printer->setEmphasis(false);
			  	$printer->text(str_pad("",20)."".str_pad("Discount Amt (-) :",20)."".$discountvalue);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Cancel Bill Amt (-) :",20).$cancelamount);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Comp. Amt (-) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance. Amt (-) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Net Amount :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance Amt (+) :",20).$advancetotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Grand Total (+) :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Food Total (+) :",20)."".$foodtotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Total Food Discount (+) :",20)."". $fdistotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Liquar Total (+) :",20)."".$liqtotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("",10)."".str_pad("Total Liquar Discount (+) :",20)."". $ldistotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}

	public function bill_print() {
        
        if (isset($this->request->get['order_id'])) {

            $orderid = $this->request->get['order_id'];

            $order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE urbanpiper_order_id = '".$this->request->get['order_id']."'")->row;
            $order_id = $order_ids['order_id'];

            //$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

            /*$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

            $order_id = $order_ids['order_id'];*/
            $ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
            $zomato_order_id_datas = $this->db->query("SELECT `zomato_order_id`,`zomato_rider_otp` FROM `oc_orders_app` WHERE online_order_id = '".$this->request->get['order_id']."'")->row;
        
            $duplicate = '0';
            $advance_id = '0';
            $advance_amount = '0';
            $grand_total = '0';
            $orderno = '0';
                /*echo'<pre>';
            print_r($ans);
            exit;*/
            if(($ans['bill_status'] == 1 ) || ($ans['bill_status'] == 0)){
            
                date_default_timezone_set('Asia/Kolkata');
                $this->load->language('customer/customer');
                $this->load->model('catalog/order');
                $merge_datas = array();
                $this->document->setTitle('BILL');
                $te = 'BILL';
                
                $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                $last_open_dates = $this->db->query($last_open_date_sql);
                if($last_open_dates->num_rows > 0){
                    $last_open_date = $last_open_dates->row['bill_date'];
                } else {
                    $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                    $last_open_dates = $this->db->query($last_open_date_sql);
                    if($last_open_dates->num_rows > 0){
                        $last_open_date = $last_open_dates->row['bill_date'];
                    } else {
                        $last_open_date = date('Y-m-d');
                    }
                }

                $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                if($last_open_dates_liq->num_rows > 0){
                    $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                } else {
                    $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                    $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                    if($last_open_dates_liq->num_rows > 0){
                        $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                    } else {
                        $last_open_date_liq = date('Y-m-d');
                    }
                }

                $last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
                $last_open_dates_order = $this->db->query($last_open_date_sql_order);
                if($last_open_dates_order->num_rows > 0){
                    $last_open_date_order = $last_open_dates_order->row['bill_date'];
                } else {
                    $last_open_date_order = date('Y-m-d');
                }

                if($ans['order_no'] == '0'){
                    $orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
                    $orderno = 1;
                    if(isset($orderno_q['order_no'])){
                        $orderno = $orderno_q['order_no'] + 1;
                    }

                    $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                    if($kotno2->num_rows > 0){
                        $kot_no2 = $kotno2->row['billno'];
                        $kotno = $kot_no2 + 1;
                    } else{
                        $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                        if($kotno2->num_rows > 0){
                            $kot_no2 = $kotno2->row['billno'];
                            $kotno = $kot_no2 + 1;
                        } else {
                            $kotno = 1;
                        }
                    }

                    $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                    if($kotno1->num_rows > 0){
                        $kot_no1 = $kotno1->row['billno'];
                        $botno = $kot_no1 + 1;
                    } else{
                        $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                        if($kotno1->num_rows > 0){
                            $kot_no1 = $kotno1->row['billno'];
                            $botno = $kot_no1 + 1;
                        } else {
                            $botno = 1;
                        }
                    }

                   // $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
                    //$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

                    //$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
                       // $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    } else{
                        //$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    }
                    $apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($apporder['app_order_id'] != '0'){
                       // $this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
                    }
                    $this->db->query($update_sql);
                }

                if($advance_id != '0'){
                   // $this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
                }
                $anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
                // echo'<pre>';
                // print_r($anss);
                // exit;

                $testfood = array();
                $testliq = array();
                $testtaxvalue1food = 0;
                $testtaxvalue1liq = 0;
                $tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
                foreach($tests as $test){
                    $amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
                    $testfoods[] = array(
                        'tax1' => $test['tax1'],
                        'amt' => $amt,
                        'tax1_value' => $test['tax1_value']
                    );
                }
                
                $testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
                foreach($testss as $testa){
                    $amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
                    $testliqs[] = array(
                        'tax1' => $testa['tax1'],
                        'amt' => $amts,
                        'tax1_value' => $testa['tax1_value']
                    );
                }
                
                $infosl = array();
                $infos = array();
                $flag = 0;
                $totalquantityfood = 0;
                $totalquantityliq = 0;
                $disamtfood = 0;
                $disamtliq = 0;
                $modifierdatabill = array();
                foreach ($anss as $lkey => $result) {
                    foreach($anss as $lkeys => $results){
                        if($lkey == $lkeys) {

                        } elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['code'] = '';
                                }
                            }
                        } elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['qty'] = $result['qty'] + $results['qty'];
                                    if($result['nc_kot_status'] == '0'){
                                        $result['amt'] = $result['qty'] * $result['rate'];
                                    }
                                }
                            }
                        }
                    }
                    
                    if($result['code'] != ''){
                        $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                        if($decimal_mesurement->num_rows > 0){
                            if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                            } else {
                                    $qty = $result['qty'];
                            }
                        } else {
                            $qty = $result['qty'];
                        }
                        if($result['is_liq']== 0){
                            $infos[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityfood = $totalquantityfood + $result['qty'];
                            $disamtfood = $disamtfood + $result['discount_value'];
                        } else {
                            $flag = 1;
                            $infosl[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityliq = $totalquantityliq + $result['qty'];
                            $disamtliq = $disamtliq + $result['discount_value'];
                        }
                    }
                }
                
                $merge_datas = array();
                if($orderno != '0'){
                    $merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
                }
                
                if($ans['parcel_status'] == '0'){
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    } else {
                        $gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    }
                } else {
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    } else {
                        $gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    }
                }

                
                
                $csgst=$ans['gst']/2;
                $csgsttotal = $ans['gst'];

                $ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
                $ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
                $ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
                $csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
                if($ans['advance_amount'] == '0.00'){
                    $gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
                } else{
                    $gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
                }
                //$gtotal = ceil($gtotal);
                $gtotal = round($gtotal);

                $printtype = '';
                $printername = '';

                if ($printtype == '' || $printername == '' ) {
                    $printtype = $this->user->getPrinterType();
                    $printername = $this->user->getPrinterName();
                    $bill_copy = 1;
                }

                if ($printtype == '' || $printername == '' ) {
                    $locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
                    if($locationData->num_rows > 0){
                        $locationData = $locationData->row;
                        $printtype = $locationData['bill_printer_type'];
                        $printername = $locationData['bill_printer_name'];
                        $bill_copy = $locationData['bill_copy'];
                    } else{
                        $printtype = '';
                        $printername = '';
                        $bill_copy = 1;
                    }
                }
                // $this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
                
                if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
                    $printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
                    $printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
                }
                $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                $LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

                    if($printerModel ==0){
                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                        //if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            /*$printer->text("Gst No :".$ans['gst_no']."");
                                            $printer->feed(1);*/
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                           /* $printer->text("Gst No :".$ans['gst_no']);
                                            $printer->feed(1);*/
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                            //$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
                                            $printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['date_added'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s', strtotime($ans['time_added']))."");
                                            $printer->feed(1);
                                                    
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            /*// foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);*/
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",35)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",35)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                            if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                           

                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            //order_ids
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_RIGHT);
                                        //$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL  :  ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                        /*$printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                        $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        $printer->text("Order Reference: ".$ans['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        $printer->feed(1);
                                        if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                        if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                        
                    }
                    else{  // 45 space code starts from here

                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    // $printer->text('45 Spacess');
                                    $printer->feed(1);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                       // if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
                                            $printer->feed(1);
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            // foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            // $printer->text("------------------------------------------");
                                            // $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",25)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                           if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        if($ans['advance_amount'] != '0.00'){
                                            $printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
                                            $printer->feed(1);
                                        }
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        //$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL : ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ans['dtotalvalue']!=0){
                                                $printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
                                                $printer->feed(1);
                                            }
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                       /*   $printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                                $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        $printer->text("Order Reference: ".$ans['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        $printer->feed(1);
                                         if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->feed(1);
                                        }
                                        if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                    }
                    if($ans['bill_status'] == 1 && $duplicate == '1'){
                        $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
                    } else{
                        $json = array();
                        $json = array(
                            'LOCAL_PRINT' => 0,
                            'status' => 1,
                        );
                    }
                    $json['done'] = '<script>parent.closeIFrame();</script>';
                    $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                
            } else {
                $this->session->data['warning'] = 'Bill already printed';
                $this->request->post = array();
                $_POST = array();
                $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
            }
        }

        // $this->request->post = array();
        // $_POST = array();
        // $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
    }

	public function export(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle('Online Order Report');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Online Order Report',
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = 0 ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
				
		}
		//echo "<pre>";print_r($billdata);exit;
		$data['billdatas'] = $billdata;
		$data['action'] = $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/billwise_report_html', $data);
		
		$filename = 'BillWiseReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function sendmail(){
		//require 'phpmailer/PHPMailerAutoload.php';

		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle('Online Order Report');

		
		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = 0 AND `complimentary_status` = '0'ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
				
		}


		//echo "<pre>";print_r($billdata);exit;
		$data['billdatas'] = $billdata;

		//$data['action'] = $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$html = $this->load->view('sale/billwise_report_html', $data);
		//echo $html;exit;
		$filename = 'BillWiseReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');

		
		$filename = 'Bill Wise Report';
		define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $output = $dompdf->output();
	    $date = date('Y-m-d');
	    $backup_name = DIR_DOWNLOAD."BillWiseReport.pdf";
	    file_put_contents($backup_name, $output);

	    $filename = "BillWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;

	 	$to_emails = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT');
	 	$password = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT_PASSWORD');
	 	$username = $this->model_catalog_order->get_settings('EMAIL_USERNAME');

		$mail = new PHPMailer();
		$message = "Bill Wise Report";
		$subject = "Bill Wise Report";

	 	
	 	// echo $from_email;
	 	// echo'<br/>';
	 	// echo $to_emails;
	 	// echo'<br/>';
	 	// echo $password;
	 	// echo'<br/>';
	 	// exit;
		// $to_emails = 'myadav00349@gmail.com';
		// $from_email = 'report@loyalrisk.in';
		// $password = '2020loyalrisk';

		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
			$to_emails_array[]= $to_emails;
			$to_emails_array[]= $to_emails;
		} else {
			$to_emails_array[] = $to_emails; 
			$to_emails_array[]= $to_emails;
		}

		$date = date('Y-m-d');
		$filename = "BillWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;
		
		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = $username;//$this->config->get('config_smtp_username');
		$mail->Password = $password;//$this->config->get('config_smtp_password');
		// $mail->Username = 'report@loyalrisk.in';//$this->config->get('config_smtp_username');
		// $mail->Password = '2020loyalrisk';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($username, 'Hotel');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			//$to_name = 'Aaron Fargose';
			$mail->AddAddress($evalue);
		}
		
		if($mail->Send()) {
			//echo"send";
			$this->session->data['success'] = 'Mail Sent';
		} else {
			//echo"not send";
			$this->session->data['success'] = 'Mail Not Sent';
		}
	 	//echo 'Done';exit;
	 	//$this->getList();
		$this->response->redirect($this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'], true));
	}

	public function function_ranking(){
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
				$results = $this->db->query("SELECT * FROM `oc_order_item_app` WHERE online_order_id = '".$order_id."'")->rows;
				$total_qty = 0;
				$total_items = 0;
				$html = '';
					foreach ($results as $result) {
						$total_qty = $total_qty + $result['qty'];

						$html .= '<tr  style="background-color: #fff;color: #000;font-family: monospace;font-size: 120%;cursor: pointer;">';
							$html .= '<td   style="cursor: pointer;">'. $result['online_order_id'].'</td>';
							$html .= '<td   style="cursor: pointer;" >'.strip_tags(html_entity_decode($result['item'], ENT_QUOTES, 'UTF-8')).'</td>';
							$html .= '<td   style="cursor: pointer;" >'.$result['qty'].'</td>';
							$html .= '<td   style="cursor: pointer;" colspan="4">'.number_format($result['price'],2).'</td>';
							$html .= '<td    style="cursor: pointer;">'. number_format($result['total'],2).'</td>';
							$html .= '<td  style="cursor: pointer;">'.number_format($result['gst'],2).'</td>';
							$html .= '<td  style="cursor: pointer;">'. number_format($result['gst_val'],2).'</td>';
							$html .= '<td  style="cursor: pointer;">'.number_format($result['grand_tot'],2).'</td>';
						$html .= '</tr>';
						$total_items++;
					}
					$status = $this->db->query("SELECT  * FROM oc_orders_app WHERE online_order_id = '".$order_id."'")->row;
					$html .= '<tr  style="font-size: 20px;">';
						$html .= '<td   style="cursor: pointer;">T.Items</td>';
						$html .= '<td   style="cursor: pointer;" >'.$total_items.'</td>';
						$html .= '<td   style="cursor: pointer;">T.Qty</td>';
						$html .= '<td   style="cursor: pointer;">'.$total_qty.'</td>';
						 if(!empty($status['ftotalvalue'] != '')) { 
								$html .= '<td   colspan="3" style="cursor: pointer;color: black;white-space:pre">Discount:'.$status['ftotalvalue'].'</td>';
							 }else { 
								$html .= '<td   colspan="3" ></td>';
							 } 
							$html .= '<td   style="cursor: pointer;">Charge</td>';
						$html .= '<td   style="cursor: pointer;" >'.number_format($status['total_charge'],2).'</td>';
						$html .= '<td   style="cursor: pointer;">G.Total</td>';
						$html .= '<td   style="cursor: pointer;" >'.number_format($status['grand_tot'],2).'</td>';
					$html .= '</tr>';

				/*$html .= '</tbody>';
				$html .= '</table>';
				$html .= '</td>';*/
				$json['html'] = $html;
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
		}
}