<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class ControllerCatalogComplimentary extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/table');
		$this->load->model('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$t_name = $this->request->post['table_frm'];
			$resion = $this->request->post['resion'];
			$login_name = $this->request->post['login_name'];
			$password = $this->request->post['password'];
			$table_infos = $this->db->query("SELECT * FROM oc_order_info WHERE t_name = '".$t_name."' AND day_close_status = '0' AND bill_status = '0' AND cancel_status = '0'  AND complimentary_status <> '1' AND payment_status = '0' AND bill_status ='0' ")->rows;
			foreach ($table_infos as $key => $settledata) {
				$user_infos = $this->db->query("SELECT * FROM oc_user WHERE username = '".$login_name."' AND complimentary_status = '1' ");
				if($user_infos->num_rows > 0){
					date_default_timezone_set('Asia/Kolkata');
					$this->load->language('customer/customer');
					$this->load->model('catalog/order');
					$merge_datas = array();
					$this->document->setTitle('BILL');
					$te = 'BILL';
					$order_id = $settledata['order_id'];
					$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$settledata['order_id']."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
					$testfoods = array();
					$testliqs = array();
					$testtaxvalue1food = 0;
					$testtaxvalue1liq = 0;
					$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$settledata['order_id']."' AND cancelstatus = '0' AND ismodifier = '1' GROUP BY tax1")->rows;
					foreach($tests as $test){
						$amt = $test['amt'] - $test['discount_value'];
						if($test['is_liq'] == '0'){
							$testfoods[] = array(
								'tax1' => $test['tax1'],
								'amt' => $amt,
								'tax1_value' => $test['tax1_value']
							);
						} else{
							$testliqs[] = array(
								'tax1' => $test['tax1'],
								'amt' => $amt,
								'tax1_value' => $test['tax1_value']
							);
						}
					}
					$countfood = $this->db->query("SELECT count(*) as id FROM oc_order_items WHERE is_liq = '0' AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0' AND ismodifier = '1'")->row;
					$totalitemsfood = 0;
					if(isset($countfood['id'])){
						$totalitemsfood = $countfood['id'];
					}
					$countliq = $this->db->query("SELECT count(*) as id FROM oc_order_items WHERE is_liq = '1' AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0' AND ismodifier = '1'")->row;
					$totalitemsliq = 0;
					if(isset($countliq['id'])){
						$totalitemsliq = $countliq['id'];
					}

					$infosl = array();
					$infos = array();
					$flag = 0;
					$totalquantityfood = 0;
					$totalquantityliq = 0;
					$disamtfood = 0;
					$disamtliq = 0;
					$modifierdatabill = array();

					foreach ($anss as $lkey => $result) {

						foreach($anss as $lkeys => $results){
							if($lkey == $lkeys) {

							} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['code'] = '';
									}
								}
							} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['qty'] = $result['qty'] + $results['qty'];
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}

						if($result['code'] != ''){
							if($result['is_liq']== 0){
								$infos[] = array(
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $result['qty'],
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2'],
									'discount_value'=> $result['discount_value']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT * FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityfood = $totalquantityfood + $result['qty'];
								$disamtfood = $disamtfood + $result['discount_value'];
							} else {
								$flag = 1;
								$infosl[] = array(
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $result['qty'],
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2'],
									'discount_value'=> $result['discount_value']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT * FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityliq = $totalquantityliq + $result['qty'];
								$disamtliq = $disamtliq + $result['discount_value'];
							}
						}
					}

					$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' AND order_no <> '0' ORDER BY `bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}

					$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
					$last_open_dates_order = $this->db->query($last_open_date_sql_order);
					if($last_open_dates_order->num_rows > 0){
						$last_open_date_order = $last_open_dates_order->row['bill_date'];
					} else {
						$last_open_date_order = date('Y-m-d');
					}

					$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$settledata['order_id']."'")->row;
					if($ordernogenrated['order_no'] == '0'){
						$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
						$orderno = 1;
						if(isset($orderno_q['order_no'])){
							$orderno = $orderno_q['order_no'] + 1;
						}

						/**************************************** Start Bill number different for liquior and Food ************************************************/

						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						// print_r($kotno);
						// exit();
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else{
							$kotno = 1;
						}

						
						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						// print_r($kotno);
						// exit();
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else{
							$botno = 1;
						}

						//$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0'");
						//$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0'");

						/**************************************** End Bill number different for liquior and Food ************************************************/
						$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$settledata['order_id']."'")->row;
						// if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
						// 	$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$$settledata['order_id']."', out_time = '".date('h:i:s')."', pay_cash = '".$ansbb['grand_total']."', payment_status = '1', total_payment = '".$ansbb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
						// } else{
						// 	$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$settledata['order_id']."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$settledata['order_id']."' ";
						// }
						$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$settledata['order_id']."'")->row;
						if($apporder['app_order_id'] != '0'){
							$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
						}
						//$this->db->query($update_sql);
						//$this->log->write($update_sql);
					}
					// echo'<pre>';
					// print_r($settledata['order_id']);
					// exit;
					$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$settledata['order_id']."'")->row;
					$ansfood = $this->db->query("SELECT billno FROM oc_order_items WHERE is_liq = '0' AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0'")->row;
					$ansliq = $this->db->query("SELECT billno FROM oc_order_items WHERE is_liq = '1' AND order_id = '".$settledata['order_id']."' AND cancelstatus = '0'")->row;
					
					$merge_datas = array();
					if($ordernogenrated['order_no'] != '0'){
						$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
					}
				
				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else{
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				}else{
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else{
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}

				
					//$this->db->query("UPDATE `oc_order_info` SET duplicate = '1', duplicate_time = '".date('h:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE `order_id` = '".$settledata['order_id']."'");
					//$duplicatebilldata = $this->db->query("SELECT `order_no`, grand_total, item_quantity, `login_name`, `duplicate_time` FROM `oc_order_info` WHERE `duplicate` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND day_close_status = '0' AND order_id = '".$settledata['order_id']."'");
					
						
						$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
						$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
						$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
					if ($setting_value_link_1 != '') {
						//$text = "Duplicate%20Bill%20-%20Ref%20No%20:".$duplicatebilldata->row['order_no'].",%20Qty%20:".$duplicatebilldata->row['item_quantity'].",%20Amt%20:".$duplicatebilldata->row['grand_total'].",%20login%20name%20:".$duplicatebilldata->row['login_name'].",%20Time%20:".$duplicatebilldata->row['duplicate_time'];
						//$link = $link_1."&phone=".$setting_value_number."&text=".$text;
						//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
						//echo $link;exit;
						//$ret = file($link);
					} 
					//$this->log->write("Duplicate ".$text);
					//$this->log->write($ret);
						//echo $text;
				

				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				//$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				//$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				// if($ans['advance_amount'] == '0.00'){
				// 	$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				// } else{
				// 	$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				// }
				//$gtotal = ceil($gtotal);
				$ans['vat'] = 0;
				$csgst= 0;
				$gtotal = 0;

					$billData = $this->db->query("SELECT table_id, location_id FROM oc_order_info WHERE order_id = '".$settledata['order_id']."'")->row;
					// echo'<pre>';
					// print_r($billData);
					// exit;
					$locationData =  $this->db->query("SELECT * FROM oc_location WHERE location_id = '".$billData['location_id']."'")->row;
					if($billData['table_id'] >= $locationData['table_from'] && $billData['table_id'] <= $locationData['table_to']){
						$printtype = $locationData['bill_printer_type'];
					 	$printername = $locationData['bill_printer_name'];
					}	


				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}

				try {
				    if($printtype == 'Network'){
				 		$connector = new NetworkPrintConnector($printername, 9100);
				 	} else if($printtype == 'Windows'){
				 		$connector = new WindowsPrintConnector($printername);
				 	} else {
				 		$connector = '';
				 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
						$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
				 	}
				 	if($connector != ''){
					    // Enter the share name for your USB printer here
					   	//$connector = new NetworkPrintConnector("192.168.1.23", 9100);
					   	//$connector = new NetworkPrintConnector("192.168.123.10", 9100);
					    //$connector = new WindowsPrintConnector("XP-58C");
					    // Print a "Hello world" receipt" //
					    $printer = new Printer($connector);
					    $printer->selectPrintMode(32);

					   	$printer->setEmphasis(true);
					   	$printer->setTextSize(2, 1);
					   	$printer->setJustification(Printer::JUSTIFY_CENTER);
					    //$printer->feed(1);
					   	//$printer->setFont(Printer::FONT_B);
					   	$printer->text("Complimentary Bill Print");
					   	$printer->feed(1);
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
					    $printer->feed(1);
					    $printer->setTextSize(1, 1);
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
					    
					    //$printer->text("Date :".str_pad(date('d/m/Y'),22)."Time :".date('h:i:s')."");
					    $printer->setJustification(Printer::JUSTIFY_CENTER);
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);



					    if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
							
						}
						else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
							$printer->feed(1);
						}
						else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
						 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
							$printer->feed(1);
						}
						else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
						    $printer->feed(1);
						}
						else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
						 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
						    $printer->feed(1);
						}
						else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
						 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
						    $printer->feed(1);
						}
						else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
						 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
						    $printer->feed(1);
						}
						else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
						    $printer->text("Name :".$ans['cust_name']."");
						    $printer->feed(1);
						}
						else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
						    $printer->text("Mobile :".$ans['cust_contact']."");
						    $printer->feed(1);
						}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
						    $printer->text("Address : ".$ans['cust_address']."");
						    $printer->feed(1);
						}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
						    $printer->text("Gst No :".$ans['gst_no']."");
						    $printer->feed(1);
						}else{
						    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
						    $printer->feed(1);
						    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
						    $printer->feed(1);
						}
						$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$settledata['order_no']);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	if($infos){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_CENTER);
					   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($settledata['order_no'],5)."Time :".date('h:i:s'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$ans['order_no']."");
					   		$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
					    	$printer->text(str_pad("Loc",10)."".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ans['location'],10)."".str_pad($settledata['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",30)."".str_pad("Rate",8)."".str_pad("Qty",5).""."Amt");
							$printer->feed(1);
					   	 	$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
						    foreach($infos as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 28);
						    	$nvalue['rate'] =html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
						    	//$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 4);
						    	$printer->text("".str_pad($nvalue['name'],30)."".str_pad($nvalue['rate'],8)."".str_pad($nvalue['qty'],5)."".$nvalue['amt']);
						    	$printer->feed(1);
					    	 	if($modifierdatabill != array()){
								    	foreach($modifierdatabill as $key => $value){
							    			//$printer->setTextSize(1, 1);

							    			if($key == $nvalue['id']){
							    				foreach($value as $modata){
							    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 28);
											    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
											    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);

										    		$printer->text("".str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],5)."".$modata['amt']);
										    		$printer->feed(1);
									    		}
									    	}
							    		}
							    	}

						    }
						    $printer->text("----------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($totalitemsfood,5)."T quantity :".str_pad($totalquantityfood,5)."F.Total :".$ans['ftotal']);
						    $printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							foreach($testfoods as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ans['fdiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
								$printer->feed(1);
							} elseif($ans['discount'] != '0'){
								$printer->text(str_pad("",22)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",33)."SCGST :".$csgst."");
							$printer->feed(1);
							$printer->text(str_pad("",33)."CCGST :".$csgst."");
							$printer->feed(1);
							if($ans['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",34)."SCRG :0");
									$printer->feed(1);
									//$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
									$netamountfood = 0;
								}else{
									$printer->text(str_pad("",34)."SCRG :0");
									$printer->feed(1);
									//$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
									$netamountfood = 0;
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									//$netamountfood = ($ans['ftotal'] - ($disamtfood));
									$netamountfood = 0;
								} else{
									//$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
									$netamountfood = 0;
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
							$printer->setEmphasis(false);
						}
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
				   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
				   			$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->feed(1);	
							$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
							if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
								$printer->feed(1);				   		
						   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
					   		}
					   		$printer->feed(1);	
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					   	}
					   	if($infosl){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_CENTER);
					   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($settledata['order_no'],5)."Time :".date('h:i:s'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$settledata['order_no']."");
					 		$printer->feed(1);
					 		$printer->setEmphasis(true);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
					    	$printer->text(str_pad("Loc",10)."".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ans['location'],10)."".str_pad($settledata['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",30)."".str_pad("Rate",8)."".str_pad("Qty",5).""."Amt");
							$printer->feed(1);
					    	$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
						    foreach($infosl as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 28);
						    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
						    	//$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 4);
						    	$printer->text("".str_pad($nvalue['name'],30)."".str_pad($nvalue['rate'],8)."".str_pad($nvalue['qty'],5)."".$nvalue['amt']);
						    	$printer->feed(1);
					    	 	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
						    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 28);
										    	$modata['rate'] = html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],5)."".$modata['amt']);
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    }
						    $printer->text("----------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($totalitemsliq,5)."T quantity :".str_pad($totalquantityliq,5)."L.Total :".$ans['ltotal']);
						    $printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							foreach($testliqs as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ans['ldiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
								$printer->feed(1);
							} elseif($ans['ldiscount'] != '0'){
								$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
							$printer->feed(1);
							if($ans['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",34)."SCRG :0");
									$printer->feed(1);
									//$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
									$netamountliq = 0;
								} else{
									$printer->text(str_pad("",34)."SCRG :0");
									$printer->feed(1);
									//$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
									$netamountliq = 0;
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									//$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
									$netamountliq = 0;
								} else{
									//$netamountliq = ($ans['ltotal'] - ($disamtliq));
									$netamountliq = 0;
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						}
						$printer->feed(1);
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
						if($ans['advance_amount'] != '0.00'){
							$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
							$printer->feed(1);
						}
						$printer->setTextSize(2, 2);
						$printer->setJustification(Printer::JUSTIFY_RIGHT);
						$printer->text("Grand Total:".$gtotal);
						$printer->setTextSize(1, 1);
						$printer->feed(1);
						$printer->text("Delivery Charge:".$ans['dtotalvalue']);
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("----------------------------------------------");
					    $printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
						if($merge_datas){
							$printer->feed(2);
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->text("Merged Bills");
							$printer->feed(1);
							$mcount = count($merge_datas) - 1;
							foreach($merge_datas as $mkey => $mvalue){
								$printer->setJustification(Printer::JUSTIFY_LEFT);
								$printer->text("Bill Number :".$settledata['order_no']."");
								$printer->feed(1);
								$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
								$printer->feed(1);
								$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
								$printer->feed(1);
								if($mvalue['ltotal'] > '0'){
									$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
								}
								$printer->feed(1);
								if($ans['parcel_status'] == '0'){
									if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
										//$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
										$sub_gtotal = 0;
									} else{
										//$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
										$sub_gtotal = 0;
									}
								} else{
									if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
										//$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
										$sub_gtotal = 0;
									} else{
										
										$sub_gtotal = 0;
										//$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
									}
								}
								$printer->text("Grand Total :".$sub_gtotal."");
								$printer->feed(1);
								if($ans['parcel_status'] == '0'){
									if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
										// $gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
										$gtotal = 0;
									} else{
										// $gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
										$gtotal = 0;
									}
								} else{
									if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
										// $gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
										$gtotal = 0;
									} else{
										// $gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
										$gtotal = 0;
									}
								}
								if($mkey < $mcount){ 
									$printer->text("----------------------------------------------");
									$printer->feed(1);
								}
							}
							$printer->feed(1);
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->text(str_pad("Merged Grand Total",38).$gtotal."");
							$printer->feed(1);
						}
						$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
						$printer->feed(1);
						$printer->text("Complimentary Resion :".$resion);
						$printer->feed(1);

						if($this->model_catalog_order->get_settings('TEXT1') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT1'));
							$printer->feed(1);
						}
						if($this->model_catalog_order->get_settings('TEXT2') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT2'));
							$printer->feed(1);
						}
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_CENTER);
						if($this->model_catalog_order->get_settings('TEXT3') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT3'));
						}
						$printer->feed(2);
						$printer->cut();
					    // Close printer //
					    $printer->close();
					}
				} catch (Exception $e) {
				    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
				    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
				    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
				    $this->session->data['warning'] = $printername." "."Not Working";
				    //continue;
				}
				// echo'<pre>';
				// print_r("SELECT * FROM oc_user WHERE username = '".$login_name."' AND complimentary_status = '1' ");
				// exit;
					$ischecked = 1;
					$this->db->query("UPDATE oc_order_info SET
										pay_method = '1',
										payment_status = '1',
										complimentary_status = '1',
										bill_status = '1',
										complimentary_resion = '".$resion."',
										pay_cash = '".$settledata['grand_total']."',
										pay_card = '".$settledata['pay_card']."',
										card_no = '".$settledata['card_no']."',
										creditcustname  = '".$settledata['creditcustname']."',
										mealpass  = '".$settledata['mealpass']."',
										pass  = '".$settledata['pass']."',
										roomservice  = '".$settledata['roomservice']."',
										roomno  = '".$settledata['roomno']."',
										msrcard  = '".$settledata['msrcard']."',
										msrcardno  = '".$settledata['msrcardno']."',
										msrcustname  = '".$settledata['msrcustname']."',
										onac  = '".$settledata['onac']."',
										onaccust  = '".$settledata['onaccust']."',
										onaccontact  = '".$settledata['onaccontact']."',
										onacname  = '".$settledata['onacname']."',
										total_payment  = '".$settledata['grand_total']."',
										tip  = '".$settledata['tip']."',
										modify_remark = '".$settledata['modify_remark']."'
										WHERE order_id = '".$settledata['order_id']."'");
					$this->db->query("UPDATE oc_order_items SET 
										complimentary_status = '1',

										complimentary_resion = '".$resion."'
										WHERE order_id = '".$settledata['order_id']."'");
					echo'Complimentary Bill Printed Successfully!!!!!!';
					//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token']));
				} else{
					$ischecked = 0;
					echo 'Complimentary Status Not Tick !!!!!!!!!!!!!!!!!
					 OR  User Name or Password Not Correct.!!!!!!!!!!!';
					// echo 'OR';
					// echo 'User Name AND Password Not Correct';
				}
			}
		
		}
	}


	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['table_to'])) {
			$data['table_to'] = $this->error['table_to'];
		} else {
			$data['table_to'] = array();
		}

		$url = '';

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_table_id'])) {
			$url .= '&filter_table_id=' . $this->request->get['filter_table_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$table_froms = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND cancel_status <> 1 ORDER BY `order_id` ")->rows;
		
		$data['tables_from'] = array();
		foreach ($table_froms as $dvalue) {
			$data['tables_from'][$dvalue['table_id']]= $dvalue['t_name'];
		}

		
		// echo'<pre>';
		// print_r($data['table_infos']);
		// exit;
		$this->log->write(print_r($this->request->get, true));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		// if (!isset($this->request->get['order_no'])) {
		// 	$data['action'] = $this->url->link('catalog/complimentary/edit', 'token=' . $this->session->data['token'] . $url, true);
		// } else {
		// 	$data['action'] = $this->url->link('catalog/complimentary/edit', 'token=' . $this->session->data['token'] . '&order_no=' . $this->request->get['order_no'] . '&table_frm=' . $this->request->get['table_frm'] . '&table_frm1=' . $this->request->get['table_frm1'] . $url, true);
		// }

		$data['cancel'] = $this->url->link('catalog/complimentary', 'token=' . $this->session->data['token'] . $url, true);
		$data['update'] = $this->url->link('catalog/complimentary/edit', 'token=' . $this->session->data['token'] .'&table_frm=' . $this->request->get['table_frm'] . '&table_frm1=' . $this->request->get['table_frm1'] .  $url, true);

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (isset($this->request->get['order_no'])) {
			$data['order_no'] = $this->request->get['order_no'];
		} else {
			$data['order_no'] = '';
		}

		if (isset($this->request->post['table_frm'])) {
			$data['table_frm'] = $this->request->post['table_frm'];
		} elseif (isset($this->request->get['table_frm'])) {
			$data['table_frm'] = $this->request->get['table_frm'];
		} else {
			$data['table_frm'] = '';
		}

		if (isset($this->request->post['table_frm1'])) {
			$data['table_frm1'] = $this->request->post['table_frm1'];
		} if (isset($this->request->get['table_frm1'])) {
			$data['table_frm1'] = $this->request->get['table_frm1'];
		} else {
			$data['table_frm1'] = '';
		}

		if (isset($this->request->post['table_to'])) {
			$data['table_to'] = $this->request->post['table_to'];
		} else {
			$data['table_to'] = '';
		}

		if (isset($this->request->post['table_to1'])) {
			$data['table_to1'] = $this->request->post['table_to1'];
		} else {
			$data['table_to1'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/complimentary_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/complimentary')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!isset($this->request->post['table_frm']) || $this->request->post['table_frm'] == '' || $this->request->post['table_frm'] == '0'){
			$this->error['table_frm'] = 'Please Select Table From';
			$this->error['status'] = 4;	
		}

		if ($this->request->post['table_frm'] == $this->request->post['table_to']) {
			$this->error['table_to'] = 'From and To Table are Same';
			$this->error['status'] = 1;
		} else {
			$table_to = $this->request->post['table_to'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$table_to);
			$sql = "SELECT `location_id` FROM " . DB_PREFIX . "location WHERE 1=1 ";
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		 	$is_exist_tables = $this->db->query($sql);
		 	if($is_exist_tables->num_rows > 0){
		 		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date = date('Y-m-d');
				}

				$order_info_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND `table_id` = '".$table_to."' ORDER BY `order_id` ");
			 	if($order_info_datas->num_rows > 0){
			 		//$this->error['status'] = 3;		
			 	}
		 	} else {
		 		$this->error['status'] = 2;	
		 	}
		}
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/complimentary')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/table');

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/table');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/complimentary')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/table');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_table->getTables($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'table_id' => $result['table_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function complimentary_direct() {
		if(isset($this->request->get['order_id'])) {
			$this->load->model('catalog/order');
			$comp_reason =	$this->session->data['complimentary_reason'];

			
			$te = 'BILL';
			if(isset($this->request->get['edit']) && ($this->request->get['edit'] == 1)){
				$edit = $this->request->get['edit'];
			} else{
				$edit = '0';
			}
			if(isset($this->request->get['checkkot'])){
				$getcheckkot = $this->request->get['checkkot'];
			} else{
				$getcheckkot = '0';
			}
			$checkkot = array();
			$order_id = $this->request->get['order_id'];
			// echo '<pre>';
			// print_r($order_id);
			// exit;
			$anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY subcategoryid")->rows;
			$anss_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;

			$liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
			$user_id= $this->user->getId();
			$user = $this->db->query("SELECT `checkkot` FROM oc_user WHERE user_id = '".$user_id."'")->row;
			if($getcheckkot == '1' && $user['checkkot'] == '1'){
				$checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
			}

			// echo'<pre>';
			// print_r($order_id);
			// echo'<br />';

			// print_r($anss);
			//exit;

			
			$infos_normal = array();
			$infos_cancel = array();
			$infos_liqurnormal = array();
			$infos_liqurcancel = array();
			$infos = array();
			$modifierdata = array();
			$allKot = array();
			$ans = $this->db->query("SELECT `order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
			foreach ($ans as $resultt) {
				$infoss[] = array(
					'order_id'   => $resultt['order_id'],
					'time_added'  => $resultt['time_added'],
					'date_added'  => $resultt['date_added'],
					'location'  => $resultt['location'],
					'location_id' => $resultt['location_id'],
					't_name'    => $resultt['t_name'],
					'table_id'  => $resultt['table_id'],
					'waiter'    => $resultt['waiter'],
					'waiter_id'    => $resultt['waiter_id'],
					'captain'   => $resultt['captain'],
					'captain_id'   => $resultt['captain_id'],
					'item_quantity'   => $resultt['item_quantity'],
					'total_items'   => $resultt['total_items'],
					'login_id' => $resultt['login_id'],
					'login_name' => $resultt['login_name'],
					'person' => $resultt['person'],
					'ftotal' => $resultt['ftotal'],
					'ltotal' => $resultt['ltotal'],
					'grand_total' => $resultt['grand_total'],
					'bill_status' => $resultt['bill_status'],

				);
			}
			// echo'<pre>';
			// print_r($infoss);
			// exit();

			$kot_group_datas = array();
			$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup")->rows;
			foreach($printerinfos as $pkeys => $pvalues){
				if($pvalues['subcategory'] != ''){
					$subcategoryid_exp = explode(',', $pvalues['subcategory']);
					foreach($subcategoryid_exp as $skey => $svalue){
						$kot_group_datas[$svalue] = $pvalues;
					}
				}
			}
			
			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}

				if($result['code'] != ''){
					if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
						$kot_group_datas[$result['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$result['qty'];
					} else {
							$qty = $result['qty'];
					}
					$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
						'id'				=> $result['id'],
						'name'           	=> $result['name'],
						'qty'         		=> $qty,
						'code'         		=> $result['code'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'         	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'            => $result['kot_no'],
						'time_added'        => $result['time'],
					);
					$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}

			// echo'<pre>';
			// print_r($kot_group_datas);
			// echo'<br/>';
			// print_r($infos_normal);
			//exit;
			
			foreach ($liqurs as $lkey => $liqur) {
				foreach($liqurs as $lkeys => $liqurss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
						if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['code'] = '';
							}
						}
					} elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
						if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
								if($liqur['nc_kot_status'] == '0'){
									$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
								}
							}
						}
					}
				}

				if($liqur['code'] != ''){
					if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
						$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
					}
					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$liqur['qty'];
					} else {
							$qty = $liqur['qty'];
					}

					$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
						'id'				=> $liqur['id'],
						'name'           	=> $liqur['name'],
						'qty'         		=> $qty,
						'amt'				=> $liqur['amt'],
						'rate'				=> $liqur['rate'],
						'message'         	=> $liqur['message'],
						'subcategoryid'		=> $liqur['subcategoryid'],
						'kot_no'            => $liqur['kot_no'],
						'time_added'        => $liqur['time'],
					);
					// echo "<pre>";
					// print_r($liqur['id']);
					$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}
			// echo'</br>';
			// print_r($modifierdata);
			// exit;

			foreach ($liqurs_1 as $liqur) {
				if($liqur['qty'] > $liqur['pre_qty']) {
					$tem = $liqur['qty'] - $liqur['pre_qty'];
					$ans=abs($tem);
					if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
						$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$ans;
					} else {
							$qty = $ans;
					}

					$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
						'name'          	=> $liqur['name'],
						'qty'         		=> $qty,
						'amt'				=> $liqur['amt'],
						'rate'				=> $liqur['rate'],
						'message'       	=> $liqur['message'],
						'subcategoryid'		=> $liqur['subcategoryid'],
						'kot_no'        	=> $liqur['kot_no'],
						'time_added'        => $liqur['time'],
					);
					$total_items_liquor_normal ++;
					$total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
					$flag = 0;
				}
			}

			$total_items_liquor_cancel = 0;
			$total_quantity_liquor_cancel = 0;
			

			if(!isset($flag)){
				$anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
				$total_items = 0;
				$total_quantity = 0;
				foreach ($anss as $result) {
					$infos[] = array(
						'name'           	=> $result['name'],
						'qty'         	 	=> $result['qty'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'        	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'        	=> $result['kot_no'],
						'time_added' 		=> $result['time'],
					);
					$total_items ++;
					$total_quantity = $total_quantity + $result['qty'];
				}
			}
			$this->db->query("UPDATE " . DB_PREFIX . "order_items SET `kot_status` = '1', `is_new` = '1' WHERE  order_id = '".$order_id."'");
		}

		$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
		if($locationData->num_rows > 0){
			$locationData = $locationData->row;
			if($locationData['kot_different'] == 1){
				$kot_different = 1;
			} else {
				$kot_different = 0;
			}
			$kot_copy = $locationData['kot_copy'];
			$bill_copy = $locationData['bill_copy'];
			$direct_bill = $locationData['direct_bill'];
			$bill_printer_type = $locationData['bill_printer_type'];
			$bill_printer_name = $locationData['bill_printer_name'];
		} else{
			$kot_different = 0;
			$kot_copy = 1;
			$direct_bill = 0;
			$bill_printer_type = '';
			$bill_printer_name = '';
		}
		// $cust_id = $this->session->data['c_id'];
		// $msr_no = $this->session->data['msr_no'];

		// $msr_bal = $this->model_catalog_msr_recharge->pre_bal($msr_no,$cust_id);
		// echo'<pre>';
		// print_r($msr_bal);
		// exit;
		
	

		$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

		if(($LOCAL_PRINT == 0 && $direct_bill == 0 && $edit == '0') || $kot_copy > 0){
			if($kot_copy > 0 && $LOCAL_PRINT == 0){
				if($infos_normal){
					foreach($infos_normal as $nkeys => $nvalues){
					 	$printtype = '';
					 	$printername = '';
					 	$description = '';
					 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
					 	$printername = $this->user->getPrinterName();
					 	$printertype = $this->user->getPrinterType();
					 	
					 	if($printertype != ''){
					 		$printtype = $printertype;
					 	} else {
					 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
								$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
					 		}
						}

						if ($printername != '') {
							$printname = $printername;
						} else {
							if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
								$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
							}
						}

						if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
							$description = $kot_group_datas[$sub_category_id_compare]['description'];
						}

						$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
						if($printerModel ==0){

							try {
						 		if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} elseif($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
							 		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
							 		if($printerModel == 0){
								 		if($kot_different == 0){
									    	//echo"innnn kot";exit;
										    $printer = new Printer($connector);
										    $printer->selectPrintMode(32);
										   	$printer->setEmphasis(true);
										   	$printer->setTextSize(2, 1);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										    for($i = 1; $i <= $kot_copy; $i++){
										    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
											    $printer->text("Complimentary KOT ");
									    		$printer->feed(1);
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

											    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    		
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
													$printer->text("Qty     Description");
											    	$printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_normal ++ ;
												    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}

											    	$printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											}
										} else{
											$printer = new Printer($connector);
										    $printer->selectPrintMode(32);
										   	$printer->setEmphasis(true);
										   	$printer->setTextSize(2, 1);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										   	for($i = 1; $i <= $kot_copy; $i++){
										   		//echo'innn2';exit;
											    $printer->text("Complimentary KOT ");
											    $printer->feed(1);

											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												   	$printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
											    	$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
											    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

												    $printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	//echo'<pre>';print_r($valuess);exit;
												    	$printer->setTextSize(2, 1);
												    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_normal ++ ;
												    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
													$printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
												foreach($nvalues as $keyss => $valuess){
													$valuess['qty'] = round($valuess['qty']);
													for($i = 1; $i <= $valuess['qty']; $i++){
														$total_items_normal = 0;
														$total_quantity_normal = 0;
														$qtydisplay = 1;
														//echo $valuess['qty'];
														$printer = new Printer($connector);
													    $printer->selectPrintMode(32);
													   	$printer->setEmphasis(true);
													   	$printer->setTextSize(2, 1);
													   	$printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
													    $printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
												    	$printer->setTextSize(2, 1);
											    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,30,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
													    $printer->setTextSize(2, 1);
												    	$total_items_normal ++ ;
												    	$total_quantity_normal ++;
												    	$qtydisplay ++;
												    	$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											}
										}
									} else {
										
									}
									// Close printer //
								    $printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
							    if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
								//continue;
							}
							
						} else {  // 45 space code starts from here

							try {
						 		if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} elseif($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								 		if($kot_different == 0){
									    	//echo"innnn kot";exit;
										    $printer = new Printer($connector);
										    $printer->selectPrintMode(32);
										   	$printer->setEmphasis(true);
										   	$printer->setTextSize(2, 1);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										   	// $printer->text('45 Spacess');
					    					$printer->feed(1);
										    for($i = 1; $i <= $kot_copy; $i++){
										    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
									    		$printer->text("Complimentary KOT ");
											    $printer->feed(1);
										   		
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

											    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    		
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
													$printer->text("Qty     Description");
											    	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_normal ++ ;
												    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}

											    	$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											}
										} else{
											$printer = new Printer($connector);
										    $printer->selectPrintMode(32);
										   	$printer->setEmphasis(true);
										   	$printer->setTextSize(2, 1);
										   	$printer->setJustification(Printer::JUSTIFY_CENTER);
										   	for($i = 1; $i <= $kot_copy; $i++){
										   		//echo'innn2';exit;
										   		$printer->text("Complimentary KOT ");
											    $printer->feed(1);
										   		
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												   	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
											    	$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
											    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

												    $printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	//echo'<pre>';print_r($valuess);exit;
												    	$printer->setTextSize(2, 1);
												    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_normal ++ ;
												    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
													$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
												foreach($nvalues as $keyss => $valuess){
													$valuess['qty'] = round($valuess['qty']);
													for($i = 1; $i <= $valuess['qty']; $i++){
														$total_items_normal = 0;
														$total_quantity_normal = 0;
														$qtydisplay = 1;
														//echo $valuess['qty'];
														$printer = new Printer($connector);
													    $printer->selectPrintMode(32);
													   	$printer->setEmphasis(true);
													   	$printer->setTextSize(2, 1);
													   	$printer->setJustification(Printer::JUSTIFY_CENTER);
													   	$printer->text("Complimentary KOT ");
											    		$printer->feed(1);
										   		
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
													    $printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													   $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
												    	$printer->setTextSize(2, 1);
											    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,20,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
												    	}
												    	$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
													    $printer->setTextSize(2, 1);
												    	$total_items_normal ++ ;
												    	$total_quantity_normal ++;
												    	$qtydisplay ++;
												    	$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											}
										}
									// Close printer //
								    $printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
							    if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
								//continue;
							}
						}
					}
				}


				if($infos_liqurnormal){
					foreach($infos_liqurnormal as $nkeys => $nvalues){
					 	$printtype = '';
					 	$printername = '';
					 	$description = '';
					 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];

					 	$printername = $this->user->getPrinterName();
					 	$printertype = $this->user->getPrinterType();
					 	
					 	if($printertype != ''){
					 		$printtype = $printertype;
					 	} else {
					 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
								$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
					 		}
						}

						if ($printername != '') {
							$printname = $printername;
						} else {
							if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
								$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
							}
						}

						if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
							$description = $kot_group_datas[$sub_category_id_compare]['description'];
						}

						$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
						if($printerModel ==0){

							try {
					 			if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    if($kot_different == 0){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									    for($i = 1; $i <= $kot_copy; $i++){
									    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
								    		$printer->text("Complimentary BOT ");
											 $printer->feed(1);
										   		
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text($description);
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text("BOT No    Wtr    Cpt               ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
										    $printer->feed(1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $kot_no_string = '';
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($nvalues as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    			$kot_no_string .= $nvalue['kot_no'].",";
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
										    } else {
												$printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($nvalues as $keyss => $valuess){
											    	$printer->setTextSize(2, 1);
										    	  	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
											    	$kot_no_string .= $valuess['kot_no'].",";
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
											    $printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
											}
										}
									} else{
										$printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									    for($i = 1; $i <= $kot_copy; $i++){
									    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
								    		$printer->text("Complimentary BOT ");
											$printer->feed(1);
										   		
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text($description);
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text("BOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
										    $printer->feed(1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $kot_no_string = '';
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($nvalues as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    			$kot_no_string .= $nvalue['kot_no'].",";
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
										    } else {
												$printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($nvalues as $keyss => $valuess){
											    	$printer->setTextSize(2, 1);
										    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    	$printer->text($valuess['qty']." ".str_pad($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
											    	$kot_no_string .= $valuess['kot_no'].",";
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
											    $printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
											}
											foreach($nvalues as $keyss => $valuess){
												$valuess['qty'] = round($valuess['qty']);
												for($i = 1; $i <= $valuess['qty']; $i++){
													$total_items_liquor_normal = 0;
												    $total_quantity_liquor_normal = 0;
												    $qtydisplay = 1;
													$printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
											    	$printer->setTextSize(2, 1);
										    	  	$printer->text($qtydisplay." ".str_pad($valuess['name']."-".$i,20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal ++;
											    	$qtydisplay ++;
											    	$printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											}
										}
									}
								    // Close printer //
								    $printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
							    if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
							    //continue;
							}
						} else {  // 45 space code starts from here
							try {
					 			if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    if($kot_different == 0){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	// $printer->text('45 Spacess');
				    					$printer->feed(1);		
									    for($i = 1; $i <= $kot_copy; $i++){
									    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
								    		$printer->text("Complimentary BOT ");
											    $printer->feed(1);
										   		
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text($description);
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],8).str_pad("Persons :".$infoss[0]['person'],8)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(" ".$nvalues[0]['kot_no']."   ".$infoss[0]['waiter_id']."   ".$infoss[0]['captain_id']."               ".date('H:i', strtotime($nvalues[0]['time_added']))."");
										    $printer->feed(1);
										    $printer->feed(1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $kot_no_string = '';
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($nvalues as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
											    	}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    			$kot_no_string .= $nvalue['kot_no'].",";
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
										    } else {
												$printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($nvalues as $keyss => $valuess){
											    	$printer->setTextSize(2, 1);
										    	  	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
											    	$kot_no_string .= $valuess['kot_no'].",";
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
											    $printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
											}
										}
									} else{
										$printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									    for($i = 1; $i <= $kot_copy; $i++){
									    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
								    		$printer->text("Complimentary BOT ");
											    $printer->feed(1);
										   		
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text($description);
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
										    $printer->feed(1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $kot_no_string = '';
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($nvalues as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
											    	}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    			$kot_no_string .= $nvalue['kot_no'].",";
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
										    } else {
												$printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_liquor_normal = 0;
											    $total_quantity_liquor_normal = 0;
											    foreach($nvalues as $keyss => $valuess){
											    	$printer->setTextSize(2, 1);
										    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    	$printer->text($valuess['qty']." ".str_pad($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
											    	$kot_no_string .= $valuess['kot_no'].",";
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
											    $printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->cut();
												$printer->feed(2);
											}
											foreach($nvalues as $keyss => $valuess){
												$valuess['qty'] = round($valuess['qty']);
												for($i = 1; $i <= $valuess['qty']; $i++){
													$total_items_liquor_normal = 0;
												    $total_quantity_liquor_normal = 0;
												    $qtydisplay = 1;
													$printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
										    		$printer->text("Complimentary KOT ");
											    	$printer->feed(1);
										   		
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
											    	$printer->setTextSize(2, 1);
										    	  	$printer->text($qtydisplay." ".str_pad($valuess['name']."-".$i,20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],24,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
											    	$total_items_liquor_normal ++;
											    	$total_quantity_liquor_normal ++;
											    	$qtydisplay ++;
											    	$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											}
										}
									}
								    // Close printer //
								    $printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
							    if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
							    //continue;
							}
						}			
					}
				}
			}
		}else {

			$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');
			$KOT_RATE_AMT = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
			$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
			$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
			$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
			$BAR_NAME =	$this->model_catalog_order->get_settings('BAR_NAME');
			$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
			$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
			$GST_NO	= $this->model_catalog_order->get_settings('GST_NO');
			$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
			$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
			$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');

			if(isset($this->session->data['credit'])){
				$credit = $this->session->data['credit'];
			} else {
				$credit = '0';
			}
			if(isset($this->session->data['cash'])){
				$cash = $this->session->data['cash'];
			} else {
				$cash = '0';
			}
			if(isset($this->session->data['online'])){
				$online = $this->session->data['online'];
			} else {
				$online ='0';
			}

			if(isset($this->session->data['onac'])){
				$onac = $this->session->data['onac'];
				$onaccontact = $this->session->data['onaccontact'];
				$onacname = $this->session->data['onacname'];

			} else {
				$onac ='0';
				$onaccontact = '';
				$onacname = '';
			}

			if(isset($testliqs)){
				$testliqs = $testliqs;
			} else {
				$testliqs =array();
			}

			if(isset($testfoods)){
				$testfoods = $testfoods;
			} else {
				$testfoods =array();
			}

			if(isset($infoss)){
				$infoss = $infoss;
			} else {
				$infoss =array();
			}

			if(isset($ansb)){
				$ansb = $ansb;
			} else {
				$ansb =array();
			}

			if(isset($ansb)){
				$ansb = $ansb;
			} else {
				$ansb =array();
			}

			$final_datas = array();
			if($local_print == 1){
				$final_datas = array(
					'aaaaa' => 0000,
					'infoss' => $infoss,
					'infos_normal' => $infos_normal,
					'modifierdata' => $modifierdata,
					'allKot' => $allKot,
					'infos_liqurnormal' => $infos_liqurnormal,
					'infos_liqurcancel' => $infos_liqurcancel,
					'infos_cancel' => $infos_cancel,
					'kot_different' => $kot_different,
					'edit' => $edit,
					'ansb' => $ansb,
					'infosb' => $infosb,
					'infoslb' => $infoslb,
					'merge_datas' => $merge_datas,
					'modifierdatabill' => $modifierdatabill,
					'KOT_RATE_AMT' =>$KOT_RATE_AMT,
					'HOTEL_NAME' => $HOTEL_NAME,
					'HOTEL_ADD' =>$HOTEL_ADD,
					'INCLUSIVE' => $INCLUSIVE,
					'BAR_NAME' => $BAR_NAME,
					'BAR_ADD' => $BAR_ADD,
					'GST_NO' => $GST_NO,
					'SETTLEMENT_status' => $SETTLEMENT_status,
					'direct_bill' => $direct_bill,
					'bill_copy' =>$bill_copy,
					'localprint' => $local_print,
					'kot_copy' => $kot_copy,
					'testfoods' => $testfoods,
					'testliqs' => $testliqs,
				);
			}
			
			$json = array();
			$json = array(
				'status' => 0,
				'final_datas' => $final_datas,
				'LOCAL_PRINT' => $local_print,

				
			);

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));	

		}


		if(($infoss[0]['bill_status'] == 0 && $edit == '0') || ($infoss[0]['bill_status'] != 0 && $edit == '1')){
			if($LOCAL_PRINT == 0 && ($direct_bill == 1 || $edit == '1')){
				
				$merge_datas = array();
				$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
				if($edit == '0'){
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}

					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq = date('Y-m-d');
					}

					$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
					$last_open_dates_order = $this->db->query($last_open_date_sql_order);
					if($last_open_dates_order->num_rows > 0){
						$last_open_date_order = $last_open_dates_order->row['bill_date'];
					} else {
						$last_open_date_order = date('Y-m-d');
					}
					
					$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($ordernogenrated['order_no'] == '0'){
						$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
						$orderno = 1;
						if(isset($orderno_q['order_no'])){
							$orderno = $orderno_q['order_no'] + 1;
						}

						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else{
							$kotno = 1;
						}

						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else{
							$botno = 1;
						}

						// $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
						// $this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

						// echo'<pre>';
						// print_r($this->session->data);
						// exit;
						
						
						//$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
						
						$ordernogenrated['order_no'] = $orderno;
						//$this->db->query($update_sql);
						$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
						if($apporder['app_order_id'] != '0'){
							$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
						}
					} 
					if($ordernogenrated['order_no'] != '0'){
						$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
					}
				}

				$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				$infoslb = array();
				$infosb = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();
				foreach ($anssb as $lkey => $result) {
					foreach($anssb as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
							if ($decimal_mesurement == 0) {
									$qty = (int)$result['qty'];
							} else {
									$qty = $result['qty'];
							}
						if($result['is_liq']== 0){
							$infosb[] = array(
								'billno'		=> $result['billno'],
								'id'			=> $result['id'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infoslb[] = array(
								'billno'		=> $result['billno'],
								'id'			=> $result['id'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}
				
				if($ansb['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
					} else{
						$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
					} else{
						$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
					}
				}

				//$onac = $this->session->data['onac'];

			// echo'<pre>';
			// print_r($this->session->data);
			// exit;
			

				$csgst=0;
				$csgsttotal = 0;

				$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				if($ansb['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = 0;


				if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
					$printtype = $bill_printer_type;
		 			$printername = $bill_printer_name;
					if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
						$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
					 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
					}

					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel ==0){


						try {
					    	if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	$printer->text("COMPLIMENTARY BILL ");
								$printer->feed(1);
										   		
							    $printer->feed(1);
							   	//$printer->setFont(Printer::FONT_B);
								for($i = 1; $i <= $bill_copy; $i++){
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    //$printer->setJustification(Printer::JUSTIFY_CENTER);
								    //$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	$printer->feed(1);
								    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
										
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Name :".$ansb['cust_name']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
									    $printer->text("Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infosb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infosb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
												    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			}
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$amt = 0;
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($amt."% On ".$amt." is ".$amt);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."%) :".$amt."");
											$printer->feed(1);
										} elseif($ansb['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."rs):".$amt."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",30)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",30)."CCGST :".$csgst."");
										$amt=0;
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",33)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											} else{
												$printer->text(str_pad("",33)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ansb['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",29)."Net total :".$amt."");
										$printer->setEmphasis(false);
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infoslb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time :".date('h:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infoslb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    }
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$amt = 0;
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($amt."% On ".$amt." is ".$amt);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."%) :".$amt."");
											$printer->feed(1);
										} elseif($ansb['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."rs) :".$amt."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",32)."VAT :".$amt."");
										$printer->feed(1);
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",31)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											} else{
												$printer->text(str_pad("",31)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ansb['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".$amt."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									$amts = 0;
									if($ansb['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",38).$amts."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_RIGHT);
									$printer->text(str_pad("",28)."GRAND TOTAL : ".$amts);
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ansb['dtotalvalue']!=0){
										$printer->text("Delivery Charge:".$amts);
										$printer->feed(1);
									}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									// echo $SETTLEMENT_status;
									// exit;
									if($SETTLEMENT_status){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: ".$onacname."");
											$printer->feed(1);
											$printer->text("Contact: ".$onaccontact."");
										}
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->feed(1);
									$printer->text("Complimentary Resion: ".$comp_reason."");

									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number :".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$amts);
											$printer->feed(1);
											$printer->text(str_pad("",32)."CGST :".$amts);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$amts."");
											}
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL : ".$amts."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("----------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",38).$amts."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								    // Close printer //
								}
							    $printer->close();
							 //    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
						}
						
					}
					else{  // 45 space code starts from here


						try {
					    	if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							    $printer->feed(1);
							    $printer->text("COMPLIMENTARY BILL ");
								$printer->feed(1);
										   		
				    			$printer->feed(1);
							   	//$printer->setFont(Printer::FONT_B);
								for($i = 1; $i <= $bill_copy; $i++){
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    //$printer->setJustification(Printer::JUSTIFY_CENTER);
								    //$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	$printer->feed(1);
								    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
										
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Name :".$ansb['cust_name']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
									    $printer->text("Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infosb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infosb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			}
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$amt = 0;
										if($ansb['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."%) :".$amt."");
											$printer->feed(1);
										} elseif($ansb['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."rs):".$amt."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",25)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",25)."CCGST :".$csgst."");
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",25)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											} else{
												$printer->text(str_pad("",25)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ansb['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".$amt."");
										$printer->setEmphasis(false);
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infoslb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infoslb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    }
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$amt = 0;
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($amt."% On ".$amt." is ".$amt);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$amt=0;
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."%) :".$amt."");
											$printer->feed(1);
										} elseif($ansb['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$amt."rs) :".$amt."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",21)."VAT :".$amt."");
										$printer->feed(1);
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",21)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											} else{
												$printer->text(str_pad("",21)."SCRG :".$amt."");
												$printer->feed(1);
												$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ansb['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".($amt)."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									if($ansb['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->text("GRAND TOTAL : ".$gtotal);
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ansb['dtotalvalue']!=0){
										$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
										$printer->feed(1);
									}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									if($SETTLEMENT_status){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: '".$onacname."'");
											$printer->feed(1);
											$printer->text("Contact: '".$onaccontact."'");
										}
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->feed(1);
									$printer->text("Complimentary Resion: ".$comp_reason."");

									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number :".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],20)."SGST :".$mvalue['gst']/2);
											$printer->feed(1);
											$printer->text(str_pad("",20)."CGST :".$mvalue['gst']/2);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],20)."VAT :".$mvalue['vat']."");
											}
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL : ".$sub_gtotal."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",35).$gtotal."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								    // Close printer //
								}
							    $printer->close();
							 //    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
						}
					}
					$this->db->query("UPDATE oc_order_info SET
										pay_method = '1',
										payment_status = '1',
										complimentary_status = '1',
										bill_status = '1',
										complimentary_resion = '".$comp_reason."',
										pay_cash = '0',
										pay_card = '0',
										card_no = '0',
										total_payment  = '".$gtotal."'
										WHERE order_id = '".$order_id."'");
					$this->db->query("UPDATE oc_order_items SET 
										complimentary_status = '1',

										complimentary_resion = '".$comp_reason."'
										WHERE order_id = '".$order_id."'");
					//echo'Complimentary Bill Printed Successfully!!!!!!';
					unset($this->session->data['complimentary_resion']);
					unset($this->session->data['compl_status']);
					 
				}
				$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');

				$json = array();
				$json = array(
					'direct_bill' => $direct_bill,
					'order_id' => $order_id,
					'grand_total' => $gtotal,
					'status' => 1,
					'orderidmodify' =>$order_id,
					'edit' => $edit,
					'LOCAL_PRINT' => $local_print,
				);
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				unset($this->session->data['cash']);
				unset($this->session->data['credit']);
				unset($this->session->data['online']);
				unset($this->session->data['onac']);
				unset($this->session->data['onaccust']);
				unset($this->session->data['onaccontact']);
				unset($this->session->data['onacname']);
			} else {
				$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');
						$KOT_RATE_AMT = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
						$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
						$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
						$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
						$BAR_NAME =	$this->model_catalog_order->get_settings('BAR_NAME');
						$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
						$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
						$GST_NO	= $this->model_catalog_order->get_settings('GST_NO');
						$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
						$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
						$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');

						if(isset($this->session->data['credit'])){
							$credit = $this->session->data['credit'];
						} else {
							$credit = '0';
						}
						if(isset($this->session->data['cash'])){
							$cash = $this->session->data['cash'];
						} else {
							$cash = '0';
						}
						if(isset($this->session->data['online'])){
							$online = $this->session->data['online'];
						} else {
							$online ='0';
						}

						if(isset($this->session->data['onac'])){
							$onac = $this->session->data['onac'];
							$onaccontact = $this->session->data['onaccontact'];
							$onacname = $this->session->data['onacname'];

						} else {
							$onac ='0';
							$onaccontact = '';
							$onacname = '';
						}

						if(isset($testliqs)){
							$testliqs = $testliqs;
						} else {
							$testliqs =array();
						}

						if(isset($testfoods)){
							$testfoods = $testfoods;
						} else {
							$testfoods =array();
						}



						$final_datas = array();
						if($local_print == 1){
							$final_datas = array(
								'infoss' => $infoss,
								'infos_normal' => $infos_normal,
								'modifierdata' => $modifierdata,
								'allKot' => $allKot,
								'infos_liqurnormal' => $infos_liqurnormal,
								'infos_liqurcancel' => $infos_liqurcancel,
								'infos_cancel' => $infos_cancel,
								'kot_different' => $kot_different,
								'edit' => $edit,
								'ansb' => $ansb,
								'infosb' => $infosb,
								'infoslb' => $infoslb,
								'merge_datas' => $merge_datas,
								'modifierdatabill' => $modifierdatabill,
								'KOT_RATE_AMT' =>$KOT_RATE_AMT,
								'HOTEL_NAME' => $HOTEL_NAME,
								'HOTEL_ADD' =>$HOTEL_ADD,
								'INCLUSIVE' => $INCLUSIVE,
								'BAR_NAME' => $BAR_NAME,
								'BAR_ADD' => $BAR_ADD,
								'GST_NO' => $GST_NO,
								'SETTLEMENT_status' => $SETTLEMENT_status,
								'direct_bill' => $direct_bill,
								'bill_copy' =>$bill_copy,
								'localprint' => $local_print,
								'kot_copy' => $kot_copy,
								'testfoods' => $testfoods,
								'testliqs' => $testliqs,
								'cash' => $cash,
								'credit' => $credit,
								'online' => $online,
								'onac' => $onac,
								'onaccontact' => $onaccontact,
								'onacname' => $onacname,

								'TEXT1' => $TEXT1,
								'TEXT2' => $TEXT2,
								'TEXT3' => $TEXT3,


							);
						}
						
						$json = array();
						$json = array(
							'status' => 0,
							'final_datas' => $final_datas,
							'LOCAL_PRINT' => $local_print,

							
						);

						$this->response->addHeader('Content-Type: application/json');
						$this->response->setOutput(json_encode($json));	
						unset($this->session->data['cash']);
						unset($this->session->data['credit']);
						unset($this->session->data['online']);
						unset($this->session->data['onac']);
						unset($this->session->data['onaccust']);
						unset($this->session->data['onaccontact']);
						unset($this->session->data['onacname']);
					}
					
		} else{
			$json = array();
			$json = array(
				'status' => 0,
				'localprints' => $local_print,

			);
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['warning'] = 'Bill already printed';
		}
	}
}