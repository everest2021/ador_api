<?php
//include DIR.'services/testping.php';
class ControllerCatalogkotgroup extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/kotgroup');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotgroup');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/kotgroup');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotgroup');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_kotgroup->addDepartment($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			// if (isset($this->request->get['filter_name'])) {
			// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
			// }

			// if (isset($this->request->get['filter_code'])) {
			// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
			// }


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/kotgroup');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotgroup');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_kotgroup->editDepartment($this->request->get['code'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			// if (isset($this->request->get['filter_name'])) {
			// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
			// }

			// if (isset($this->request->get['filter_code'])) {
			// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
			// }

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/kotgroup');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/kotgroup');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $code) {
				$this->model_catalog_kotgroup->deleteDepartment($code);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			// if (isset($this->request->get['filter_name'])) {
			// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
			// }

			// if (isset($this->request->get['filter_code'])) {
			// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
			// }

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		// if (isset($this->request->get['filter_name'])) {
		// 	$filter_name = $this->request->get['filter_name'];
		// } else {
		// 	$filter_name = null;
		// }

		// if (isset($this->request->get['filter_code'])) {
		// 	$filter_code = $this->request->get['filter_code'];
		// } else {
		// 	$filter_code = null;
		// }

		$data['departments'] = array();

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		// if (isset($this->request->get['filter_name'])) {
		// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
		// }

		// if (isset($this->request->get['filter_code'])) {
		// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
		// }

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/kotgroup/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/kotgroup/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			// 'filter_name' => $filter_name,
			// 'filter_code' => $filter_code,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_kotgroup->getTotalDepartment();

		$results = $this->model_catalog_kotgroup->getDepartments($filter_data);
		//echo '<pre>';
		//print_r($results);
		//exit();
		//$printerping = new PrinterPing(); 
	 	foreach ($results as $result) {
			if($result['subcategory'] != ''){
				$subcategory_explode = explode(',', $result['subcategory']);
				$subcategory = '';
				foreach($subcategory_explode as $skey => $svalue){
					$subcategory_names = $this->db->query("SELECT `name` FROM `oc_subcategory` WHERE `category_id` = '".$svalue."' ");
					if($subcategory_names->num_rows > 0){
						$subcategory_name = $subcategory_names->row['name'];
						$subcategory .= $subcategory_name.', ';
					}
				}
				$subcategory = rtrim($subcategory, ', ');
			}
			// $ip = $result['printer'];
			// exec("ping -n 3 $ip", $output, $status);
			// echo $status;
			$data['departments'][] = array(
				'code' 			=> $result['code'],
				'description'   => $result['description'],
				'printer'       => $result['printer'],
				'subcategory'   => $subcategory,
				//'status'		=> $status,
				'edit'        	=> $this->url->link('catalog/kotgroup/edit', 'token=' . $this->session->data['token'] . '&code=' . $result['code'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		// if (isset($this->request->get['filter_name'])) {
		// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
		// }

		// if (isset($this->request->get['filter_code'])) {
		// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
		// }

		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data['sort_name'] = $this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		
		$url = '';

		// if (isset($this->request->get['filter_name'])) {
		// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
		// }

		// if (isset($this->request->get['filter_code'])) {
		// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
		// }


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		// $data['filter_name'] = $filter_name;
		// $data['filter_code'] = $filter_code;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kotgroup_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['code']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		$url = '';

		// if (isset($this->request->get['filter_name'])) {
		// 	$url .= '&filter_name=' . $this->request->get['filter_name'];
		// }

		// if (isset($this->request->get['filter_code'])) {
		// 	$url .= '&filter_code=' . $this->request->get['filter_code'];
		// }

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['code'])) {
			$data['action'] = $this->url->link('catalog/kotgroup/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/kotgroup/edit', 'token=' . $this->session->data['token'] . '&code=' . $this->request->get['code'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/kotgroup', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$department_info = $this->model_catalog_kotgroup->getDepartment($this->request->get['code']); 
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['code'])) {
			$data['code'] = $this->request->get['code'];
		} elseif (!empty($department_info)) {
			$data['code'] = $department_info['code'];
		} else {
			$lastcode = $this->db->query("SELECT code FROM oc_kotgroup ORDER BY code DESC limit 1");
			if($lastcode->num_rows > 0){
				$codeval = $lastcode->row['code'];
				$data['code'] = $codeval + 1; 
			} else{
				$data['code'] = '1';
			}
		}

		$data['subcategory'] = array();
		if (isset($this->request->post['subcategory'])) {
			$data['subcategory'] = $this->request->post['subcategory'];
		} elseif (!empty($department_info)) {
			$data['subcategory'] = explode(',', $department_info['subcategory']);
		} else {
			$data['subcategory'] = array();
		}

		// echo '<pre>';
		// print_r($data['subcategory']);
		// exit;

		if (isset($this->request->post['printertype'])) {
			$data['printertype'] = $this->request->post['printertype'];
		} elseif (!empty($department_info)) {
			$data['printertype'] = $department_info['printer_type'];
		} else {
			$data['printertype'] = '';
		}

		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (!empty($department_info)) {
			$data['description'] = $department_info['description'];
		} else {
			$data['description'] = '';
		}

		if (isset($this->request->post['printer'])) {
			$data['printer'] = $this->request->post['printer'];
		} elseif (!empty($department_info)) {
			$data['printer'] = $department_info['printer'];
		} else {
			$data['printer'] = '';
		}

		if (isset($this->request->post['master_kotprint'])) {
			$data['master_kotprint'] = $this->request->post['master_kotprint'];
		} elseif (!empty($department_info)) {
			$data['master_kotprint'] = $department_info['master_kotprint'];
		} else {
			$data['master_kotprint'] = '0';
		}

		$data['printertypes'] = array(
								'Network' => 'Network',
								'Windows' => 'Windows' 
								);

		$data['subcategorys'] = $this->db->query("SELECT name, category_id FROM oc_subcategory")->rows;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/kotgroup_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/kotgroup')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $datas = $this->request->post;
		// if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
		// 	$this->error['name'] = 'Please Enter department Name';
		// }
		// else {
		// 	if(isset($this->request->get['code'])){
		// 		$is_exist = $this->db->query("SELECT `code` FROM `oc_department` WHERE `name` = '".$datas['name']."' AND `code` <> '".$this->request->get['code']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Department Name Already Exists';
		// 		}
		// 	} else {
		// 		$is_exist = $this->db->query("SELECT `code` FROM `oc_department` WHERE `name` = '".$datas['name']."' ");
		// 		if($is_exist->num_rows > 0){
		// 			$this->error['name'] = 'Department Name Already Exists';
		// 		}
		// 	}
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/kotgroup')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	
	// public function autocomplete() {
	// 	$json = array();

	// 	if (isset($this->request->get['filter_name'])) {
	// 		$this->load->model('catalog/kotgroup');

	// 		$filter_data = array(
	// 			'filter_name' => $this->request->get['filter_name'],
	// 			'start'       => 0,
	// 			'limit'       => 20
	// 		);

	// 		$results = $this->model_catalog_kotgroup->getDepartments($filter_data);

	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'code' => $result['code'],
	// 				'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['name'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
}