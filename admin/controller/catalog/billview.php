<?php

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class ControllerCatalogBillView extends Controller {
	private $error = array();

	public function index() {
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		
		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$order_datas = $this->db->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON(oi.`order_id` = oit.`order_id`) WHERE oit.`cancelstatus` = '0' AND oi.`bill_status` = '1' AND oi.`bill_date` = '".$last_open_date."' AND oi.`complimentary_status` <> '1' GROUP BY oit.`order_id` ORDER BY oi.`order_no` ")->rows;
		foreach ($order_datas as $okey => $ovalue) {
			$food_order_item_datas = $this->db->query("SELECT `billno` as food_bill_no FROM `oc_order_items` oit WHERE oit.`order_id` = '".$ovalue['order_id']."' AND `is_liq` = '0' AND `cancelstatus` = '0' ORDER BY oit.`order_id` DESC LIMIT 1");
			$food_bill_no = 0;
			if($food_order_item_datas->num_rows > 0){
				$food_order_item_data = $food_order_item_datas->row;
				$food_bill_no = $food_order_item_data['food_bill_no'];
			}
			$liq_order_item_datas = $this->db->query("SELECT `billno` as liq_bill_no FROM `oc_order_items` oit WHERE oit.`order_id` = '".$ovalue['order_id']."' AND `is_liq` = '1' AND `cancelstatus` = '0' ORDER BY oit.`order_id` DESC LIMIT 1");
			$liq_bill_no = 0;
			if($liq_order_item_datas->num_rows > 0){
				$liq_order_item_data = $liq_order_item_datas->row;
				$liq_bill_no = $liq_order_item_data['liq_bill_no'];
			}
			$order_datas[$okey]['food_bill_no'] = $food_bill_no;
			$order_datas[$okey]['liq_bill_no'] = $liq_bill_no;
		}	
		$data['order_datas'] = $order_datas;
		// echo'<pre>';
		// print_r($order_datas);
		// exit;

		$testingliq = $this->db->query("SELECT * FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_status` = '1' AND oit.`is_liq` = '1' AND oit.`cancelstatus` = '0' AND oi.`pay_method` = '1' AND oi.`bill_date` = '".date('Y-m-d')."' AND oi.`liq_cancel` <> '1' AND oit.`ismodifier` = '1' GROUP BY oit.`order_id`")->rows;
		$data['testingliq'] = $testingliq;

		if(!empty($this->session->data['tablet'])){
			$data['tablet'] = $this->session->data['tablet'];
		} else{
			$data['tablet'] = '';
		}

		if(isset($this->request->get['qwerty'])){
			$data['qwerty'] = $this->request->get['qwerty'];
		} else{
			$data['qwerty'] = '';
		}
		$data['cancel_bill'] = $this->user->getCanceBill();
		$data['edit_bill'] = $this->user->getEditBill();
		$data['duplicate_bill'] = $this->user->getDuplicateBill();
		// echo'<pre>';
		// print_r($printername);
		// exit;
		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		$data['KOT_BILL_CANCEL_REASON'] = $this->model_catalog_order->get_settings('KOT_BILL_CANCEL_REASON');
		

		
		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/billview', $data));
	}

	public function getdatafood(){
		$json = array();
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		if(isset($this->request->get['orderidfood'])) {
			$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id ='".$this->request->get['orderidfood']."' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY `is_liq` ")->rows;
			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}

				if($result['code'] != ''){
					$json[] = array(
						'name' => $result['name'],
						'qty' => $result['qty'],
						'rate' => $result['rate'],
						'amt' => $result['amt'],
					);
				}
			}
		}
		$this->response->setOutput(json_encode($json));
	}

	public function getdataliq(){
		$json = array();
		if (isset($this->request->get['orderidliq'])) {
			$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id ='".$this->request->get['orderidliq']."' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}

				if($result['code'] != ''){
					$json[] = array(
						'name' => $result['name'],
						'qty' => $result['qty'],
						'rate' => $result['rate'],
						'amt' => $result['amt'],
					);
				}
			}
		}
		$this->response->setOutput(json_encode($json));
	}

	public function delete(){
		$this->load->model('catalog/order');
		date_default_timezone_set('Asia/Kolkata');
		$this->db->query("UPDATE oc_order_items SET cancel_bill = '1', `cancel_kot_reason` = '".$this->request->get['reason']."' , `time` = '".date('H:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE order_id = '".$this->request->get['orderid']."' ");

		$cancelleditems = $this->db->query("SELECT id FROM oc_order_items WHERE cancel_bill = '1'")->rows;
		foreach($cancelleditems as $cancelleditem){
			$this->db->query("UPDATE oc_order_items SET cancel_bill = '1', `time` = '".date('H:i:s')."',  `cancel_kot_reason` = '".$this->request->get['reason']."' , login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."'  WHERE parent_id = '".$cancelleditem['id']."'");
		}
		
		if($this->request->get['is_liq'] == '0'){
			$this->db->query("UPDATE oc_order_info SET `food_cancel` = '1', `cancel_status` = '1',`cancel_bill_reason` = '".$this->request->get['reason']."'  WHERE `order_id` = '".$this->request->get['orderid']."' ");
		} else{
			$this->db->query("UPDATE oc_order_info SET `liq_cancel` = '1', `cancel_status` = '1' ,`cancel_bill_reason` = '".$this->request->get['reason']."' WHERE `order_id` = '".$this->request->get['orderid']."' ");
		}

		$cancelBillSms = $this->db->query("SELECT billno, COALESCE(SUM(amt - discount_value),0) as amt, SUM(qty) as qty, oit.`login_name`, oit.`time` FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`day_close_status` = '0' AND oit.`ismodifier` = '1' AND oit.`order_id` = '".$this->request->get['orderid']."' AND oi.`cancel_status` = '1' AND oit.`cancel_bill` = '1' GROUP BY oit.`order_id`")->rows;
		foreach($cancelBillSms as $cancel){
			$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
			$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
			//echo $setting_value_link_1;exit;
			$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
			if ($setting_value_link_1 != '') {
				$text = "Cancelled%20Bill%20-%20Billno%20:".$cancel['billno'].",%20Qty%20:".$cancel['qty'].",%20Amt%20:".$cancel['amt'].",%20login%20name%20:".$cancel['login_name'].",%20Time%20:".$cancel['time'];
				//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
				$link = $link_1."&phone=".$setting_value_number."&text=".$text;
				//echo $link;exit;
				//file($link);
			}
		}
		//exit();
		//$this->response->setOutput(json_encode($json));

		//$this->response->redirect($this->url->link('catalog/order/printcancel', 'token=' . $this->session->data['token'].'&orderid='.$this->request->get['orderid'].'&cancel=1', true));

		$json['status'] = 1;
		$this->response->setOutput(json_encode($json));

		//$this->response->redirect($this->url->link('catalog/billview', 'token=' . $this->session->data['token'], true));
	}


	
}

	