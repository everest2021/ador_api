<?php
class ControllerCatalogShiftclose extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle('Shift Close');
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/table');
		$this->document->setTitle('Shift Close');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;

			$shift_data =  $this->db->query("SELECT shift_id FROM oc_order_info  order by shift_id desc LIMIT 1 ");
			if($shift_data->num_rows > 0){
				$shift_id = $shift_data->row['shift_id'] + 1;
			} else {
				$shift_id = 1;
				
			}
			$login_name = $this->user->getUserName();
			$login_id = $this->user->getId();
			$shift_date = date('Y-m-d');
			$shift_time = date("h:i:s");
			// echo'<pre>';
			// print_r($shift_time);
			// exit();
			$this->db->query("UPDATE `oc_order_info` SET `shiftclose_status` = '1', `shift_id` = '".$shift_id."', `shift_date` = '".$shift_date."', `shift_time` = '".$shift_time."' ,`shift_username` = '".$login_name."' ,`shift_user_id` = '".$login_id."' WHERE `shiftclose_status` = '0' ");

			$this->log->write("UPDATE `oc_order_info` SET `shiftclose_status` = '1', `shift_id` = '".$shift_id."', `shift_date` = '".$shift_date."', `shift_time` = '".$shift_time."' ,`shift_username` = '".$login_name."' ,`shift_user_id` = '".$login_id."' WHERE `shiftclose_status` = '0' ");

			unset($this->session->data['warning1']);
			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			$this->response->setOutput(json_encode($json));
		} else {
			if(isset($this->error['open_tran'])){
				$json['info'] = 0;
			} elseif(isset($this->error['open_tran_1'])){
				$json['info'] = 2;
			}
			$json['action'] = $this->url->link('catalog/shiftclose/edit', 'token=' . $this->session->data['token'], true);
			$this->response->setOutput(json_encode($json));
		}
	}

	
	protected function getForm() {
		$data['heading_title'] = 'Shift Close';//$this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['action'] = $this->url->link('catalog/shiftclose/edit', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['cancel'] = $this->url->link('catalog/shiftclose', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];

		// $last_open_date_sql = "SELECT `login_name`,`login_id`,`bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
		// $last_open_dates = $this->db->query($last_open_date_sql);
		// if($last_open_dates->num_rows > 0){
			// $login_id = $last_open_dates->row['login_id'];
			// $login_name = $last_open_dates->row['login_name'];
			$login_name = $this->user->getUserName();
			$login_id = $this->user->getId();
			$open_date = date('Y-m-d');

			//$open_date = $last_open_dates->row['bill_date'];
		// } else {
		// 	$login_id = '';
		// 	$login_name = '';
		// 	$open_date = date('Y-m-d');
		// }

		//$this->log->write(print_r($this->request->get, true));
		
		if (isset($this->request->post['open_date'])) {
			$data['open_date'] = $this->request->post['open_date'];
		} else {
			$data['open_date'] = $open_date;
		}
		
		if (isset($this->request->post['login_id'])) {
			$data['login_id'] = $this->request->post['login_id'];
		} else {
			$data['login_id'] = $login_id;
		}
		
		if (isset($this->request->post['login_name'])) {
			$data['login_name'] = $this->request->post['login_name'];
		} else {
			$data['login_name'] = $login_name;
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/shiftclose', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/dayclose')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/dayclose')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	
}