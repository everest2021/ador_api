<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class Controllercatalogurbanpiperorder extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('catalog/urbanpiper_order');
		if($this->is_connected() == false){
			$this->session->data['warning'] = "Please Connect On Internet For Online Orders";
			$this->response->redirect($this->url->link('catalog/order'));
		} 
		$this->getItems();
	}

	public function add() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('catalog/urbanpiper_order');
		
		$this->getList();
	}

	public function order_cancel() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		//$this->load->model('catalog/urbanpiper_order');

		$body_data = array(
  			'merchant_id' => MERCHANT_ID
  		);
		
		$body = json_encode($body_data);
		//echo '<pre>'; print_r($body) ;exit;
		$url = "https://api.werafoods.com/pos/v2/merchant/cancelorders";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		$datas = json_decode($response,true);
		// echo'<pre>';
		// print_r($datas);
		// exit();
		curl_close($ch);
		if($datas['code'] == 1){
			foreach ($datas['details'] as $dkey => $dvalue) {
				$this->db->query("UPDATE oc_werafood_orders SET status = '4' WHERE order_id = '".$dvalue['order_id']."'");
					
			}
			$json['info'] = 1;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['success'] = 'Items Send Successfully!';
		} elseif($datas['code'] == 2) {
			$json['info'] = 2;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}

	public function getList() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		//$this->load->model('catalog/urbanpiper_order');

		
		$body_data = array(
  			'merchant_id' => MERCHANT_ID
  		);
		
		$body = json_encode($body_data);
		//echo '<pre>'; print_r($body) ;exit;
		$url = "https://api.werafoods.com/pos/v2/merchant/pendingorders";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		$datas = json_decode($response,true);
			/*echo'<pre>';
			print_r($datas);
			exit;*/

		foreach ($datas['details']['orders'] as $okey => $ovalue) {
			$order_datas = $this->db->query("SELECT order_id FROM oc_werafood_orders WHERE order_id = '".$ovalue['order_id']."'");
			if($order_datas->num_rows == 0) {
			$order_date_time = date('Y-m-d H:i:s', $ovalue['order_date_time']);
			$order_date = date('Y-m-d', $ovalue['order_date_time']);

				$this->db->query("INSERT INTO `oc_werafood_orders` SET 
							`order_id` = '".$ovalue['order_id']."',
							`restaurant_id` = '".$ovalue['restaurant_id']."',
							`restaurant_name` = '".$ovalue['restaurant_name']."',
							`external_order_id` = '".$ovalue['external_order_id']."',
							`order_from` = '".$ovalue['order_from']."',
							`status` = '".$ovalue['status']."',
							`order_date_time` = '".$order_date_time."',
							`date` = '".$order_date."',

							`enable_delivery` = '".$ovalue['enable_delivery']."',
							`net_amount` = '".$ovalue['net_amount']."',
							`gross_amount` = '".$ovalue['gross_amount']."',
							`payment_mode` = '".$ovalue['payment_mode']."',
							`order_type` = '".$ovalue['order_type']."',
							`order_instructions` = '".strip_tags(html_entity_decode( $ovalue['order_instructions'], ENT_QUOTES, 'UTF-8'))  ."',
							`foodready` = '".$ovalue['foodready']."',
							`packaging` = '".$ovalue['packaging']."',
							`packaging_cgst_percent` = '".$ovalue['packaging_cgst_percent']."',
							`packaging_sgst_percent` = '".$ovalue['packaging_sgst_percent']."',
							`packaging_cgst` = '".$ovalue['packaging_cgst']."',
							`packaging_sgst` = '".$ovalue['packaging_sgst']."',
							`gst` = '".$ovalue['gst']."',
							`cgst` = '".$ovalue['cgst']."',
							`sgst` = '".$ovalue['sgst']."',
							`discount` = '".$ovalue['discount']."',
							`order_otp` = '".$ovalue['order_otp']."',
							`is_pop_order` = '".$ovalue['is_pop_order']."',
							`delivery_charge` = '".$ovalue['delivery_charge']."'
				");

				$this->db->query("INSERT INTO `oc_werafood_order_customer` SET
						`order_id` = '".$ovalue['order_id']."',
						`name` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['name'], ENT_QUOTES, 'UTF-8')) ."',
						`phone_number` = '".$ovalue['customer_details']['phone_number']."',
						`email` = '".$ovalue['customer_details']['email']."',
						`address` = '".strip_tags(html_entity_decode(  $ovalue['customer_details']['address'], ENT_QUOTES, 'UTF-8'))."',
						`delivery_area` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['delivery_area'], ENT_QUOTES, 'UTF-8')) ."',
						`address_instructions` = '".strip_tags(html_entity_decode( $ovalue['customer_details']['address_instructions'], ENT_QUOTES, 'UTF-8'))."'
				");
				

				$this->db->query("INSERT INTO `oc_werafood_order_rider` SET
						`order_id` = '".$ovalue['order_id']."',
						`is_rider_available` = '".$ovalue['rider']['is_rider_available']."',
						`arrival_time` = '".$ovalue['rider']['arrival_time']."'
						
				");

				

				foreach ($ovalue['order_items'] as $ikey => $ivalue) {
					if(isset($ivalue['cgst'])){
						$cgst = $ivalue['cgst'];
					} else {
						$cgst = 0;
					}

					if(isset($ivalue['cgst_percent'])){
						$cgst_percent = $ivalue['cgst_percent'];
					} else {
						$cgst_percent = 0;
					}

					if(isset($ivalue['sgst'])){
						$sgst = $ivalue['sgst'];
					} else {
						$sgst = 0;
					}

					if(isset($ivalue['sgst_percent'])){
						$sgst_percent = $ivalue['sgst_percent'];
					} else {
						$sgst_percent = 0;
					}


					$this->db->query("INSERT INTO `oc_werafood_order_items` SET 
								`order_id` = '".$ovalue['order_id']."',
								`wera_item_id` = '".$ivalue['wera_item_id']."',
								`item_id` = '".$ivalue['item_id']."',
								`item_name` = '".$ivalue['item_name']."',
								`item_unit_price` = '".$ivalue['item_unit_price']."',
								`subtotal` = '".$ivalue['subtotal']."',
								`item_quantity` = '".$ivalue['item_quantity']."',
								`gst` = '".$ivalue['gst']."',
								`gst_percent` = '".$ivalue['gst_percent']."',
								`packaging` = '".$ivalue['packaging']."',
								`instructions` =   '".strip_tags(html_entity_decode($ivalue['instructions'], ENT_QUOTES, 'UTF-8'))."',
								`discount` = '".$ivalue['discount']."',
								`cgst` = '".$cgst."',
								`sgst` = '".$sgst."',
								`cgst_percent` = '".$cgst_percent."',
								`sgst_percent` = '".$sgst_percent."',
								`packaging_gst` = '".$ivalue['packaging_gst']."'
					");
					if($ivalue['variants']){
						foreach ($ivalue['variants'] as $vkey => $vvalue) {
							$this->db->query("INSERT INTO `oc_werafood_order_variants` SET
										`order_id` = '".$ovalue['order_id']."',
										`order_item_id` = '".$ivalue['item_id']."',
										`size_id` = '".$vvalue['size_id']."',
										`size_name` = '".$vvalue['size_name']."',
										`price` = '".$vvalue['price']."'
							");
						}
					}

					if($ivalue['addons']){
						foreach ($ivalue['addons'] as $akey => $avalue) {
							$this->db->query("INSERT INTO `oc_werafood_order_addons` SET
										`order_id` = '".$order_id."',
										`order_item_id` = '".$ivalue['item_id']."',
										`addon_id` = '".$avalue['addon_id']."',
										`name` = '".$avalue['name']."',
										`price` = '".$avalue['price']."',
										`cgst` = '".$avalue['cgst']."',
										`sgst` = '".$avalue['sgst']."',
										`cgst_percent` = '".$avalue['cgst_percent']."',
										`sgst_percent` = '".$avalue['sgst_percent']."'
							");
						}
					}
				}
			}
			$json['order_id'] = $ovalue['order_id'];
			$json['order_from'] = $ovalue['order_from'];
		}

		curl_close($ch);

		

		if($datas['code'] == 1){
			$json['info'] = 1;
			
			$this->response->addHeader('Content-Type: application/json');

			$this->response->setOutput(json_encode($json));
			//$this->session->data['success'] = 'Items Send Successfully!';
		} else {
			$json['info'] = 0;
			$json['order_id'] = '';
			$json['order_from'] = '';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['errors'] = 'Items Not Send !';
		}
		// echo 'done';
		// exit;
		// $data['superadmin'] = $this->session->data['superadmin'];
		// $data['header'] = $this->load->controller('common/header');
		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['footer'] = $this->load->controller('common/footer');
		// // echo'<pre>';
		// // print_r($data);
		// // exit;
		// $this->response->setOutput($this->load->view('catalog/urbanpiper_order', $data));



	}

	// public function addstore() {
	// 	// echo'innn';
	// 	// exit;
	// 	$this->load->language('catalog/order');
	// 	$this->document->setTitle('Online Pending Order');
	// 	$this->load->model('catalog/order');
	// 	$json = array();

	// 	//$this->load->model('catalog/order');
	// 	$setting_value_hotel_name = $this->model_catalog_order->get_settings('HOTEL_NAME');
	// 	if (utf8_strlen($setting_value_hotel_name) == '' ) {
	// 		$json['error']['hotel_name'] = 'Please Add Hotel Name In Setting';
	// 	}
	// 	$setting_value_hotel_add = $this->model_catalog_order->get_settings('HOTEL_ADD');
	// 	if (utf8_strlen($setting_value_hotel_add) == '' ) {
	// 		$json['error']['hotel_add'] = 'Please Add Hotel Address In Setting';
	// 	}

	// 	$setting_contact = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
	// 	if (utf8_strlen($setting_contact) < 10) {
	// 		$json['error']['hotel_phone'] = 'Please Add Valid Phone Number In Setting';
	// 	}

	// 	$setting_email = $this->model_catalog_order->get_settings('EMAIL_USERNAME');
	// 	if ((utf8_strlen($setting_email) > 96) || !filter_var($setting_email, FILTER_VALIDATE_EMAIL)) {
	// 		$json['error']['hotel_email'] = 'Please Enter Valid Email In Setting';
	// 	}
	// 	if (!$json) {
	// 		$stores_array[] = array(
	// 			'city' => 'MUMBAI',
	// 			'name' => $setting_value_hotel_name,
	// 			'min_pickup_time' => 900,
 //  				'min_delivery_time' => 1800,
 //  				'contact_phone' => $setting_contact,
 //  				'notification_phones' =>  array (0 => $setting_contact),
 //  				'ref_id' => HOTEL_LOCATION_ID,
 //  				'min_order_value' => 200,
 //  				'hide_from_ui' => false,
 //  				'address' => $setting_value_hotel_add,
 //  				'notification_emails' =>   array ( 0 => $setting_email),
 //  				'zip_codes' => array(),
 //  				'geo_longitude' => '',
 //  				'active' => true,
 //  				'geo_latitude' => '',
 //  				'ordering_enabled' => true,
 //  				'translations' => array(),
 //  				'excluded_platforms' => array(),
 //  				'platform_data' => array(),
 //  				'timings' => array(),
	// 		);
	// 		$store_array = array (
	// 			'stores' =>  $stores_array
	// 		); 
	// 		echo "<pre>";print_r($store_array);exit;
	// 		$body = json_encode($store_array);

	// 		//$body = json_encode($store_array, JSON_PRETTY_PRINT);//echo "<pre>";print_r($body);exit;
			
	// 		$curl = curl_init();

	// 			curl_setopt_array($curl, array(
	// 		  CURLOPT_URL => 'https://pos-int.urbanpiper.com/external/api/v1/stores/',
	// 		  CURLOPT_RETURNTRANSFER => true,
	// 		  CURLOPT_ENCODING => '',
	// 		  CURLOPT_MAXREDIRS => 10,
	// 		  CURLOPT_TIMEOUT => 0,
	// 		  CURLOPT_FOLLOWLOCATION => true,
	// 		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 		  CURLOPT_CUSTOMREQUEST => 'POST',
	// 		  CURLOPT_POSTFIELDS => $body ,
	// 		  CURLOPT_HTTPHEADER => array(
	// 		    'Authorization:'.URBANPIPER_API_KEY,
	// 		    'Content-Type: application/json',
	// 		  ),
	// 		));

	// 		$response = curl_exec($curl);
	// 		$datas = json_decode($response,true);
	// 		echo "<pre>";print_r($datas);exit;
	// 		curl_close($curl);

	// 		//echo "<pre>";print_r( $response );exit;
		
	// 		$json['success'] = "Successfull Data Send";
	// 	}
	// 	$this->response->addHeader('Content-Department: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function addstore() {
		/*echo'innn';
		exit;*/
		sleep(10);
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Pending Order');
		$this->load->model('catalog/order');
		$json = array();
		$store_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
		$store_data = array();
		if($store_sql->num_rows > 0){
			$store_data = $store_sql->row;
			$final_noti_no = array();
			$searchForValue = ',';
			if( strpos($store_data['notification_phones'], $searchForValue) !== false ) {
			    $final_noti_no = explode(',', $store_data['notification_phones']);
			} else {
				$final_noti_no[] = $store_data['notification_phones'];
			}
			$final_noti_email = array();
			if( strpos($store_data['notification_emails'], $searchForValue) !== false ) {
			    $final_noti_email = explode(',', $store_data['notification_emails']);
			} else {
				$final_noti_email[] = $store_data['notification_emails'];
			}
			$timings_arra = array();
			$timings_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store` WHERE 1= 1");
			if($timings_sql->num_rows > 0){
				foreach ($timings_sql->rows as $tkey => $tvalue) {
					$timings_arra[] = array(
						'day' => $tvalue['day'],
						'slots' => array( '0' => array( 
							'start_time' =>  $tvalue['start_time'],
							'end_time' =>  $tvalue['end_time']
						) )
					);
				}
			}

			$included_platforms = array();
			$platform_data = array();

			$platform_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_platform` WHERE 1= 1");
			if($platform_sql->num_rows > 0){
				foreach ($platform_sql->rows as $pkey => $pvalue) {
					$included_platforms[] = $pvalue['platform_name'];
					$platform_data[] = array(
						'name' => $pvalue['platform_name'],
						'url' => ($pvalue['platform_link']),
						'platform_store_id' => $pvalue['platform_store_id'],
					);
				}
			}
			$Fulfillment_modes = array();
			if($store_data['fullfillmode'] != ''){
				$Fulfillment_modes = json_decode($store_data['fullfillmode']);
			}


			$stores_array[] = array(
				'city' => $store_data['city'],
				'name' => $store_data['name'],
				'min_pickup_time' => (int)$store_data['min_pickup_time'],
  				'min_delivery_time' => (int)$store_data['min_delivery_time'],
  				'contact_phone' => $store_data['contact_phone'],
  				'notification_phones' =>  (array)$final_noti_no,
  				'ref_id' => $store_data['ref_id'],
  				'min_order_value' => (float)$store_data['min_order_value'],
  				'hide_from_ui' => false,
  				'address' =>  $store_data['address'],
  				'notification_emails' =>   (array)$final_noti_email,
  				'zip_codes' => array( '0' => $store_data['zip_codes']),
  				'geo_longitude' => (double)$store_data['geo_longitude'],
  				'active' => (boolean) $store_data['active'],
  				'geo_latitude' => (double)$store_data['geo_latitude'],
  				'ordering_enabled' => true,
  				'translations' => array(),
  				'included_platforms' => (array)$included_platforms,
  				'platform_data' => $platform_data,
  				'timings' => $timings_arra,
  				'fulfillment_modes' => (array)$Fulfillment_modes,

			);
			$store_array = array (
				'stores' =>  $stores_array
			); 
			$body = json_encode($store_array ,JSON_UNESCAPED_SLASHES);
			//echo "<pre>";print_r($body);exit;
			//$body = json_decode($body, JSON_PRETTY_PRINT);echo "<pre>";var_dump($body);exit;
			
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

				curl_setopt_array($curl, array(
			  CURLOPT_URL => STOREAPI,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $body ,
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:'.URBANPIPER_API_KEY,
			    'Content-Type: application/json',
			  ),
			));

			$response = curl_exec($curl);
			$datas = json_decode($response,true);

			if (curl_errno($curl)) {
		    	$error_msg = curl_error($curl);
			}
			
			curl_close($curl);
			$error = 0;
			$success = 0;

			if($datas['status'] == 'success'){
				$json['success'] = "Successfull Data Send";
				$this->session->data['store_api_status'] = 'new_store_create';
				$this->db->query("TRUNCATE oc_item_urbanpiper");
				$this->db->query("TRUNCATE oc_urbanpiper_category_time");
				$this->db->query("TRUNCATE oc_allwebhook_response");
				$this->db->query("TRUNCATE oc_urbanpiper_platform_items");
				$url = Truncate_Basictable;
				$final_data_rider = array(
					'db_name' => 'db_'.$store_data['ref_id']
				);
				$data_json_rider = json_encode($final_data_rider);
				$ch_fetch = curl_init();
				curl_setopt($ch_fetch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch_fetch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch_fetch, CURLOPT_URL, $url);
				curl_setopt($ch_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
				curl_setopt($ch_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch_fetch, CURLOPT_POSTFIELDS,$data_json_rider);
				curl_setopt($ch_fetch, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch_fetch,CURLOPT_SSL_VERIFYPEER, false);
				$rider_response  = curl_exec($ch_fetch);
				$rider_results = json_decode($rider_response,true);
				curl_close($ch_fetch);
				$success = 1;

			} elseif($datas['status'] == 'error') {
				$json['errors'] = $datas['message'];
				$error = 1;
			}  else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
				$error = 1;
			}else {
				$json['info'] = 0;
				$json['errors'] = $response;
				$error = 1;
			}
			$error_plus = 0;
			$check_error = $this->db->query("SELECT `sucess`,`failure` FROM oc_urbanpiper_tazitokari_call_api_status WHERE `event` = 'store_add' ORDER BY id desc Limit 1");
			if ($check_error->num_rows > 0) {
				if($error == 1){
					//if($check_error->row['failure']){
						$error_plus = $check_error->row['failure'] + 1;
					//}
				}
				$this->db->query("UPDATE oc_urbanpiper_tazitokari_call_api_status SET
						`sucess`= '".$this->db->escape($success)."', 
						 `date`= NOW(),
						`failure` = '".$this->db->escape($error_plus)."'  
						WHERE `event` = 'store_add' ");
			} else {
				$this->db->query("INSERT INTO oc_urbanpiper_tazitokari_call_api_status SET
						`sucess`= '".$this->db->escape($success)."', 
						 `date`= NOW(),
						`failure` = '".$this->db->escape($error)."'  ,
						`event` = 'store_add'
					");
			}
		} else {
			$json['error'] = "Please Add Store";
		}
		
		
		$this->response->addHeader('Content-Department: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function order_ready(){
		//$this->load->model('catalog/order');
		//$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');

		if(isset($this->request->get['order_id'])){
			$orderid = $this->request->get['order_id'];
			// echo'inn';
			// 	exit;
			$edit = 0;
			if($orderid != ''){
				$body_data = array(
	  				'merchant_id' => MERCHANT_ID,
	  				'order_id' => $orderid,
	  			);
		
				$body = json_encode($body_data);
				//echo '<pre>'; print_r($body) ;exit;
				//$url = "https://api.werafoods.com/pos/v2/order/food-ready";
				$url = "https://api.werafoods.com/pos/v1/order/food-ready";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-Wera-Api-Key:'.WERA_API_KEY,'Accept: application/json' ));
				
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response  = curl_exec($ch);
				$datas = json_decode($response,true);
					// echo'<pre>';
					// print_r($response);
					// exit;
				if($datas['code'] == 1){
					$this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
					$json['info'] = 1;
				} elseif($datas['code'] == 2) {
					$json['info'] = 2;
					$json['reason'] = $data['msg'];
				} else {
					$json['info'] = 0;
				}
				
			}
		}

		$json['done'] = '<script>parent.closeIFrame();</script>';
		$this->response->setOutput(json_encode($json));
	}



	public function getItems() {
		// echo'innn';
		// exit;
		date_default_timezone_set("Asia/Kolkata");
		$this->load->language('catalog/order');
		$this->document->setTitle('Online Order');
		//$this->log->write('Start Orders');
		//$this->log->write(date('Y-m-d h:i:s'));

		$url = FETCH_ONLINE_URBANPIPER_DATAS;
		$final_data = array('db_name' => API_DATABASE);
		$data_json = json_encode($final_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		$response  = curl_exec($ch);
		$results = json_decode($response,true);
		curl_close($ch);
		//echo "<pre>";print_r($results);exit;
		//$this->log->write('Fetch Order');
		//$this->log->write(date('Y-m-d h:i:s'));

		$current_dates = date('Y-m-d');
		$next_day3 = date('Y-m-d', strtotime($current_dates . ' -1 day'));

		$exist_order_datas = $this->db->query("SELECT * FROM oc_orders_app WHERE order_date >= '".$next_day3."' ")->rows;
		foreach ($exist_order_datas as $ekey => $evalue) {
			$exist_order_array[$evalue['online_order_id']] = $evalue;
		}

		if (!empty($results)) {
			$all_orders = '';
			foreach ($results['online_orders'] as $result) {
				if (!isset($exist_order_array[$result['online_order_id']])) {

					$orders_app = $this->db->query("INSERT INTO oc_orders_app SET
						
						`packaging` = '".$result['packaging']."', 
						`delivery_charge` = '".$result['delivery_charge']."', 
						`total_charge` = '".$result['total_charge']."', 
						`tot_qty` = '".$result['tot_qty']."', 
						`total` = '".$result['total']."', 
						`grand_tot`  = '".$result['grand_tot']."', 
						`tot_gst` = '".$this->db->escape($result['tot_gst'])."',
						`gst_val` = '".$this->db->escape($result['gst_val'])."',
						`order_time` = '".$result['order_time']."', 
						`order_date` = '".$result['order_date']."', 
						`address_1` = '".$this->db->escape($result['address_1'])."', 
						`address_2` = '".$this->db->escape($result['address_2'])."', 
						`city` = '".$this->db->escape($result['city'], $this->conn)."',
						`postcode` = '".$this->db->escape($result['postcode'])."',
						`order_status` = '1', area = '".$this->db->escape($result['area'])."' ,
						`online_order_status` = '".$this->db->escape($result['online_order_status'])."',
						`online_order_type_placed_delie` = '".$this->db->escape($result['online_order_type_placed_delie'])."',
						`online_order_id` = '".$this->db->escape($result['online_order_id'])."',
						`online_channel` = '".$this->db->escape($result['online_channel'])."',
						`online_instruction` = '".$this->db->escape($result['online_instruction'])."',
						`online_deliverydatetime` = '".$this->db->escape($result['online_deliverydatetime'])."',
						`online_created_time` = '".$this->db->escape($result['online_created_time'])."',
						`online_store_id` = '".$this->db->escape($result['online_store_id'])."',
						`online_biz_id` = '".$this->db->escape($result['online_biz_id'])."',
						`online_payment_method` = '".$this->db->escape($result['online_payment_method'])."',
						`kot_status` = '0',
						`cust_id` = '".$this->db->escape($result['cust_id'])."',
						`firstname` =  '".$this->db->escape($result['firstname'])."',
						`email` =  '".$this->db->escape($result['email'])."',
						`telephone` =  '".$this->db->escape($result['telephone'])."',
						`roundtotal` = '".$result['roundtotal']."',
						`ftotal_discount` = '".$this->db->escape($result['ftotal_discount'])."',
						`ftotalvalue` = '".$this->db->escape($result['ftotalvalue'])."',
						`discount` = '".$this->db->escape($result['discount'])."',
						`discount_reason` = '".$this->db->escape($result['discount_reason'])."',
						`online_deliery_type` = '".$this->db->escape($result['online_deliery_type'])."',
						`zomato_order_type` = '".$this->db->escape($result['zomato_order_type'])."',
						`zomato_order_id` = '".$this->db->escape($result['zomato_order_id'])."',
						`zomato_discount` = '".$this->db->escape($result['zomato_discount'])."',
						`zomato_rider_otp` = '".$this->db->escape($result['zomato_rider_otp'])."',
						`pay_online` = '".$result['pay_online']."'
					");
					$orders_app_id = $this->db->getLastId();
					foreach ($result['online_order_item'] as $okey => $ovalue) {
						$this->db->query("INSERT INTO oc_order_item_app SET
							 
							`order_id` = '".$orders_app_id."', 
							`online_order_id` = '".$this->db->escape($ovalue['online_order_id'])."' ,
							`online_order_status` = '".$this->db->escape($ovalue['online_order_status'])."',
							`item_id` = '".$this->db->escape($ovalue['item_id'])."',
							`item` = '".$this->db->escape($ovalue['item'])."', 
							`qty` = '".$this->db->escape($ovalue['qty'])."',
							`price` = '".$this->db->escape($ovalue['price'])."',
							`total` = '".$this->db->escape($ovalue['total'])."',
							`gst` = '".$ovalue['gst']."', 
							`gst_val` = '".$ovalue['gst_val']."',
							`grand_tot` = '".$this->db->escape($ovalue['grand_tot'])."' ,
							`online_order_type_placed_delie` = '".$this->db->escape($ovalue['online_order_type_placed_delie'])."' ,
							`discount_per` = '".$this->db->escape($ovalue['discount_per'])."',
							`discount_value` = '".$this->db->escape($ovalue['discount_value'])."'
						");
					}

					$cust_details = $this->db->query("SELECT `firstname`,`email`,`telephone`,`date_added`,`customer_id` FROM oc_customer_app WHERE customer_id = '".$result['customer_details']['customer_id']."' ");
					if ($cust_details->num_rows > 0) {
						$this->db->query("UPDATE oc_customer_app SET
							`firstname` = '".$this->db->escape($result['customer_details']['firstname'])."',
							`email`= '".$this->db->escape($result['customer_details']['email'])."',
							`telephone`= '".$this->db->escape($result['customer_details']['telephone'])."', 
							`information_field` = '1' ,
							`customer_type` = 'online' , 
							`date_added` = '".$result['customer_details']['date_added']."'
							WHERE `customer_id` = '".$result['customer_details']['customer_id']."' ");
					} else {
						$this->db->query("INSERT INTO oc_customer_app SET
							`customer_id` = '".$this->db->escape($result['customer_details']['customer_id'])."',
							`firstname` = '".$this->db->escape($result['customer_details']['firstname'])."',
							`email`= '".$this->db->escape($result['customer_details']['email'])."',
							`telephone`= '".$this->db->escape($result['customer_details']['telephone'])."', 
							`information_field` = '1' ,
							`customer_type` = 'online' , 
							`date_added` = '".$result['customer_details']['date_added']."'
						");
					}

					//$add_details = $this->db->query("SELECT * FROM oc_address_app WHERE `address_1` = '".$this->db->escape($result['customer_add_details']['address_1'])."' AND `address_2` = '".$this->db->escape($result['customer_add_details']['address_2'])."' AND `city`= '".$this->db->escape($result['customer_add_details']['city'])."' AND `postcode`= '".$this->db->escape($result['customer_add_details']['postcode'])."' AND  `area`= '".$this->db->escape($result['customer_add_details']['area'])."'AND  `customer_id` = '".$this->db->escape($result['customer_add_details']['customer_id'])."' AND `sub_locality`= '".$this->db->escape($result['customer_add_details']['sub_locality'])."'  ");
					$add_details = $this->db->query("SELECT * FROM oc_address_app WHERE `online_order_id` = '".$this->db->escape($result['online_order_id'])."' AND  `customer_id` = '".$this->db->escape($result['customer_add_details']['customer_id'])."' ");


					if ($add_details->num_rows == 0) {
						//$this->db->query("DELETE FROM `oc_address_app` WHERE `customer_id` = '".$result['customer_details']['customer_id'] ."' ");
						$this->db->query("INSERT INTO `oc_address_app` SET 
						
							`customer_id` = '".$result['customer_details']['customer_id'] ."',
							 `address_1` = '".$this->db->escape($result['customer_add_details']['address_1'])."', 
							 `address_2` = '".$this->db->escape($result['customer_add_details']['address_2'])."', 
							 `city`= '".$this->db->escape($result['customer_add_details']['city'])."', 
							 `postcode`= '".$this->db->escape($result['customer_add_details']['postcode'])."',
							`area`= '".$this->db->escape($result['customer_add_details']['area'])."',
							`firstname` = '".$this->db->escape($result['customer_add_details']['firstname'])."', 
							`sub_locality`= '".$this->db->escape($result['customer_add_details']['sub_locality'])."',
							`online_order_id`= '".$this->db->escape($result['online_order_id'])."',
							`country_id` = 99 ,
							`zone_id` = 1493 
						 ");
					} 
					
					if ($orders_app == 1) {
						$all_orders .= ','.$result['online_order_id'];
					}
				}
			}

			if($all_orders != ''){
				$all_order = ltrim($all_orders,',');
				$url = CHECK_ONLINE_DATAS_URBANPIPER;
				//$final_data = $all_order;
				$final_data = array('db_name' => API_DATABASE,'online_order_id' => $all_order);
				$data_json = json_encode($final_data);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				$response  = curl_exec($ch);
				$resultsz = json_decode($response,true);
				curl_close($ch);
				//$this->log->write('Update Fetch Order');
				//$this->log->write(date('Y-m-d h:i:s'));
			}
		}

		$url = FETCH_RIDER_URBANPIPER;
		$final_data_rider = array('db_name' => API_DATABASE);
		$data_json_rider = json_encode($final_data_rider);
		$ch_fetch = curl_init();
		curl_setopt($ch_fetch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch_fetch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch_fetch, CURLOPT_URL, $url);
		curl_setopt($ch_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
		curl_setopt($ch_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch_fetch, CURLOPT_POSTFIELDS,$data_json_rider);
		curl_setopt($ch_fetch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_fetch,CURLOPT_SSL_VERIFYPEER, false);
		$rider_response  = curl_exec($ch_fetch);
		$rider_results = json_decode($rider_response,true);
		curl_close($ch_fetch);
		//$this->log->write('Fetch Rider');
		//$this->log->write(date('Y-m-d h:i:s'));
		//echo "<pre>";print_r($rider_results);exit;
		$all_rider = '';
		if (!empty($rider_results)) {
			foreach ($rider_results as $result_rdr) {
				$insert_rider = $this->db->query("INSERT INTO `oc_urbanpiper_rider` SET 
					`channel_name` = '".$this->db->escape($result_rdr['channel_name']) ."', 
					`channel_order_id` = '".$this->db->escape($result_rdr['channel_order_id']) ."', 
					`rider_name` = '".$this->db->escape($result_rdr['rider_name'])."',
					`rider_alt_phone` = '".$this->db->escape($result_rdr['rider_alt_phone'])."', 
					`rider_phone`= '".$this->db->escape($result_rdr['rider_phone'])."',
					`rider_user_id`= '".$this->db->escape($result_rdr['rider_user_id'])."', 
					`rider_status`= '".$this->db->escape($result_rdr['rider_status'])."', 
					`real_order_id` = '".$this->db->escape($result_rdr['real_order_id'])."', 
					`entry_status`= '1'
					 "
				);
				if ($insert_rider == 1) {
					$all_rider .= ','.$result_rdr['id'];
				}
			}
			if($all_rider != ''){
				$all_riders = ltrim($all_rider,',');
				$url = UPDATE_ENTRY_STATUS_RIDER_URBANPIPER;
				//$final_data_update_rider = $all_riders;
				$final_data_update_rider = array('db_name' => API_DATABASE,'rider_id' => $all_riders);
				$data_json_rider = json_encode($final_data_update_rider);
				$ch_update = curl_init();
				curl_setopt($ch_update, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch_update, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch_update, CURLOPT_URL, $url);
				curl_setopt($ch_update, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
				curl_setopt($ch_update, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch_update, CURLOPT_POSTFIELDS,$data_json_rider);
				curl_setopt($ch_update, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch_update,CURLOPT_SSL_VERIFYPEER, false);
				$updateresponse  = curl_exec($ch_update);
				$resultsz = json_decode($updateresponse,true);
				curl_close($ch_update);
				//$this->log->write('Update Fetch Rider');
				//$this->log->write(date('Y-m-d h:i:s'));
			}
		}
		$url = FETCH_UPDATE_ORDER_STATUS;
		$final_data_order_fetch = array('db_name' => API_DATABASE);
		$data_json_order_fetch = json_encode($final_data_order_fetch);
		$ch_order_fetch = curl_init();
		curl_setopt($ch_order_fetch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch_order_fetch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch_order_fetch, CURLOPT_URL, $url);
		curl_setopt($ch_order_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_order_fetch)));
		curl_setopt($ch_order_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch_order_fetch, CURLOPT_POSTFIELDS,$data_json_order_fetch);
		curl_setopt($ch_order_fetch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch_order_fetch,CURLOPT_SSL_VERIFYPEER, false);
		$response  = curl_exec($ch_order_fetch);
		$order_results = json_decode($response,true);
		curl_close($ch_order_fetch);

		//$this->log->write('Order Staus');
				//$this->log->write(date('Y-m-d h:i:s'));
		//echo "<pre>";print_r($results);exit;
		$all_order = '';
		if (!empty($order_results)) {
			foreach ($order_results as $order_result) {
				$insert_rider = $this->db->query("INSERT INTO `oc_urbunpiper_order_status` SET 
					`urbanpiper_order_id` = '".$this->db->escape($order_result['urbanpiper_order_id']) ."', 
					`prev_state` = '".$this->db->escape($order_result['prev_state']) ."', 
					`new_state` = '".$this->db->escape($order_result['new_state'])."',
					`timestamp` = '".$this->db->escape($order_result['timestamp'])."', 
					`date_added`= '".$this->db->escape($order_result['date_added'])."',
					`message` = '".$this->db->escape($order_result['message'])."', 
					`entry_status`= '1'
					 "
				);
				if ($insert_rider == 1) {
					$all_order .= ','.$order_result['urbanpiper_order_id'];
				}
			}
			if($all_order != ''){
				$all_orders = ltrim($all_order,',');
				$url = FETCH_UPDATE_ORDER_STATUS;
				//$final_data_update_order = $all_orders;
				$final_data_update_rider = array('db_name' => API_DATABASE,'order_status_id' => $all_orders);
				$data_json_rider = json_encode($final_data_update_rider);
				$ch_order_fetch = curl_init();
				curl_setopt($ch_order_fetch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch_order_fetch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch_order_fetch, CURLOPT_URL, $url);
				curl_setopt($ch_order_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
				curl_setopt($ch_order_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch_order_fetch, CURLOPT_POSTFIELDS,$data_json_rider);
				curl_setopt($ch_order_fetch, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch_order_fetch,CURLOPT_SSL_VERIFYPEER, false);
				$response  = curl_exec($ch_order_fetch);
				$resultsz = json_decode($response,true);
				curl_close($ch_order_fetch);
				//$this->log->write('Update Order Staus');
				//$this->log->write(date('Y-m-d h:i:s'));
			}
		}
		//$this->load->model('catalog/urbanpiper_order');

		if(isset($this->request->post['store_status'])) {
			$data['store_status'] = $this->request->post['store_status'];
		} else {
			$data['store_status'] = '1';
		}

		//$this->log->write('Fetch List');
		//$this->log->write(date('Y-m-d h:i:s'));
		$current_date = date('Y-m-d');
		$start_date = date('Y-m-d', strtotime($current_date . ' -1 day'));
		$data['food_alert'] = 0;
		$pending_orders = array();
		$data['pending_orders'] = array();
		$order_datas = $this->db->query("SELECT * FROM `oc_orders_app` WHERE (online_order_id != '') AND `order_date` >= '".$start_date."'  AND `order_date` <= '".$current_date."' AND `online_order_status` != 'Completed' order by CAST(online_order_id AS int) DESC");
		if($order_datas->num_rows > 0){
			$sr_no = 1;
			foreach ($order_datas->rows as $okey => $ovalue) {
				$order_time = '';
				if($ovalue['online_created_time'] != ''){
					$order_time =  date('d-m-Y H:i:s', $ovalue['online_created_time']/1000); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
				}
				$online_deliverydatetime = '';
				if($ovalue['online_deliverydatetime'] != ''){
					$online_deliverydatetime =  date('d-m-Y H:i:s', $ovalue['online_deliverydatetime']/1000); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
				}
				$online_deliverydatetime = '';
				if($ovalue['online_deliverydatetime'] != ''){
					$online_deliverydatetime =  date('d-m-Y H:i:s', $ovalue['online_deliverydatetime']/1000); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
				}
				$local_orderdate = '';
				if($ovalue['order_date'] != ''){
					$local_orderdate =  date('d-m-Y', strtotime($ovalue['order_date'])); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
				}
				if($ovalue['online_order_status'] == 'Placed'){
					$data['food_alert'] = 1;
				}
				$pending_orders[] = array(
					'sr_no' => $sr_no,
					'order_id' => $ovalue['online_order_id'],
					'zomato_order_id' => $ovalue['zomato_order_id'],
					'online_channel' => $ovalue['online_channel'],
					'payment_mode' => $ovalue['online_payment_method'],
					'order_type' => $ovalue['online_order_type_placed_delie'],
					'gross_amount' => number_format($ovalue['grand_tot'],2),
					'status' => $ovalue['online_order_status'],
					'order_time' => $order_time,
					'delivery_time' => $online_deliverydatetime,
				);
				$sr_no++;
			} 
		}

		$order_status_urban_sql = $this->db->query("SELECT new_state,urbanpiper_order_id FROM `oc_urbunpiper_order_status` WHERE CAST(date_added AS DATE) >= '".$start_date."' AND CAST(date_added AS DATE) <= '".$current_date."' ORDER BY CAST(urbanpiper_order_id AS int) DESC");
		if($order_status_urban_sql->num_rows > 0){
			$increment = 0 ;
			foreach ($order_status_urban_sql->rows as $order_status_urban_sql) {
				// if(($order_status_urban_sql['new_state'] != 'Acknowledged') && ($order_status_urban_sql['new_state'] != 'Food Ready') && ($order_status_urban_sql['new_state'] != 'Cancelled')){
				// 	$this->db->query("UPDATE " . DB_PREFIX . "orders_app SET  
				// 		`online_order_status` = '" . $this->db->escape($order_status_urban_sql['new_state']) . "'
				// 		WHERE online_order_id = '".$order_status_urban_sql['urbanpiper_order_id']."' AND online_order_status = '".$order_status_urban_sql['prev_state']."' ");
				// 	//$this->log->write("UPDATE " . DB_PREFIX . "orders_app SET  `online_order_status` = '" . $this->db->escape($order_status_urban_sql['new_state']) . "' WHERE online_order_id = '".$order_status_urban_sql['urbanpiper_order_id']."' AND online_order_status = '".$order_status_urban_sql['prev_state']."' ");
				// 	if($order_status_urban_sql['new_state'] == 'Completed'){
				// 		$this->db->query("DELETE FROM `oc_urbanpiper_tazitokari_call_api_status` WHERE `order_id` = '".$order_status_urban_sql['urbanpiper_order_id']."' ");
				// 	}
				// }
				if(($order_status_urban_sql['new_state'] == 'Cancelled')){
					$this->db->query("UPDATE " . DB_PREFIX . "orders_app SET  
						`online_order_status` = '" . $this->db->escape($order_status_urban_sql['new_state']) . "'
						WHERE online_order_id = '".$order_status_urban_sql['urbanpiper_order_id']."'  ");
					$update_cancel_sql = "SELECT `order_id` FROM `oc_order_info`  WHERE `urbanpiper_order_id` =  '".$order_status_urban_sql['urbanpiper_order_id']."' ORDER BY CAST(urbanpiper_order_id AS int) DESC LIMIT 1";
					$update_cancel_sql_dates = $this->db->query($update_cancel_sql);
					if($update_cancel_sql_dates->num_rows > 0){
						$this->db->query("UPDATE `oc_order_info` SET `cancel_status` = '1', `food_cancel` = '1' WHERE `order_id` = '".$update_cancel_sql_dates->row['order_id']."' ");
						$this->db->query("UPDATE `oc_order_items` SET `cancelstatus` = '1', `cancel_bill` = '1' WHERE `order_id` = '".$update_cancel_sql_dates->row['order_id']."' ");
					} 
				}else if($order_status_urban_sql['new_state'] == 'Completed'){
					$this->db->query("UPDATE " . DB_PREFIX . "orders_app SET  
						`online_order_status` = '" . $this->db->escape($order_status_urban_sql['new_state']) . "'
						WHERE online_order_id = '".$order_status_urban_sql['urbanpiper_order_id']."' ");
					$this->db->query("DELETE FROM `oc_urbanpiper_tazitokari_call_api_status` WHERE `order_id` = '".$order_status_urban_sql['urbanpiper_order_id']."' ");
				}
			}
		}
		
		

		$data['pending_orders'] = $pending_orders;

		//$this->log->write('Done Fetch List');
		//$this->log->write(date('Y-m-d h:i:s'));
		//$this->log->write('Update Order Staus');
				//$this->log->write(date('Y-m-d h:i:s'));
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['warning'] = $this->session->data['warning'];

			unset($this->session->data['warning']);
		} else {
			$data['warning'] = '';
		}
		//$this->log->write('pending_orders');
		//$this->log->write($pending_orders);
		// print_r(date('Y-m-d h:i:s')); 
		// echo'<pre>';
		// print_r($pending_orders); 
		// exit;
		$data['token'] = $this->session->data['token'];

		
		$data['order_page'] = $this->url->link('catalog/order', 'token=' . $this->session->data['token'], true);
		
		//$data['platform_list'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'], true);
		$data['superadmin'] = $this->session->data['superadmin'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('catalog/urbanpiper_order', $data));
	}


	// public function bill_print() {
		
	// 	if (isset($this->request->get['order_id'])) {

	// 		$orderid = $this->request->get['order_id'];
	// 		$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

	// 		$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

	// 		$order_id = $order_ids['order_id'];
	// 		$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		
	// 		$duplicate = '0';
	// 		$advance_id = '0';
	// 		$advance_amount = '0';
	// 		$grand_total = '0';
	// 		$orderno = '0';
	// 		// 	echo'<pre>';
	// 		// print_r($ans);
	// 		// exit;
	// 		if(($ans['bill_status'] == 1 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
			
	// 			date_default_timezone_set('Asia/Kolkata');
	// 			$this->load->language('customer/customer');
	// 			//$this->load->model('catalog/order');
	// 			$merge_datas = array();
	// 			$this->document->setTitle('BILL');
	// 			$te = 'BILL';
				
	// 			$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 			$last_open_dates = $this->db->query($last_open_date_sql);
	// 			if($last_open_dates->num_rows > 0){
	// 				$last_open_date = $last_open_dates->row['bill_date'];
	// 			} else {
	// 				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
	// 				$last_open_dates = $this->db->query($last_open_date_sql);
	// 				if($last_open_dates->num_rows > 0){
	// 					$last_open_date = $last_open_dates->row['bill_date'];
	// 				} else {
	// 					$last_open_date = date('Y-m-d');
	// 				}
	// 			}

	// 			$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

	// 			$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

	// 			if($last_open_dates_liq->num_rows > 0){
	// 				$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 			} else {
	// 				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

	// 				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

	// 				if($last_open_dates_liq->num_rows > 0){
	// 					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
	// 				} else {
	// 					$last_open_date_liq = date('Y-m-d');
	// 				}
	// 			}

	// 			$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
	// 			$last_open_dates_order = $this->db->query($last_open_date_sql_order);
	// 			if($last_open_dates_order->num_rows > 0){
	// 				$last_open_date_order = $last_open_dates_order->row['bill_date'];
	// 			} else {
	// 				$last_open_date_order = date('Y-m-d');
	// 			}

	// 			if($ans['order_no'] == '0'){
	// 				$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
	// 				$orderno = 1;
	// 				if(isset($orderno_q['order_no'])){
	// 					$orderno = $orderno_q['order_no'] + 1;
	// 				}

	// 				$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 				if($kotno2->num_rows > 0){
	// 					$kot_no2 = $kotno2->row['billno'];
	// 					$kotno = $kot_no2 + 1;
	// 				} else{
	// 					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
	// 					if($kotno2->num_rows > 0){
	// 						$kot_no2 = $kotno2->row['billno'];
	// 						$kotno = $kot_no2 + 1;
	// 					} else {
	// 						$kotno = 1;
	// 					}
	// 				}

	// 				$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 				if($kotno1->num_rows > 0){
	// 					$kot_no1 = $kotno1->row['billno'];
	// 					$botno = $kot_no1 + 1;
	// 				} else{
	// 					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
	// 					if($kotno1->num_rows > 0){
	// 						$kot_no1 = $kotno1->row['billno'];
	// 						$botno = $kot_no1 + 1;
	// 					} else {
	// 						$botno = 1;
	// 					}
	// 				}

	// 				$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
	// 				$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

	// 				//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 				if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
	// 					$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
	// 				} else{
	// 					$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
	// 				}
	// 				$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
	// 				if($apporder['app_order_id'] != '0'){
	// 					$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
	// 				}
	// 				$this->db->query($update_sql);
	// 			}

	// 			if($advance_id != '0'){
	// 				$this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
	// 			}
	// 			$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
	// 			// echo'<pre>';
	// 			// print_r($anss);
	// 			// exit;

	// 			$testfood = array();
	// 			$testliq = array();
	// 			$testtaxvalue1food = 0;
	// 			$testtaxvalue1liq = 0;
	// 			$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
	// 			foreach($tests as $test){
	// 				$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
	// 				$testfoods[] = array(
	// 					'tax1' => $test['tax1'],
	// 					'amt' => $amt,
	// 					'tax1_value' => $test['tax1_value']
	// 				);
	// 			}
				
	// 			$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
	// 			foreach($testss as $testa){
	// 				$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
	// 				$testliqs[] = array(
	// 					'tax1' => $testa['tax1'],
	// 					'amt' => $amts,
	// 					'tax1_value' => $testa['tax1_value']
	// 				);
	// 			}
				
	// 			$infosl = array();
	// 			$infos = array();
	// 			$flag = 0;
	// 			$totalquantityfood = 0;
	// 			$totalquantityliq = 0;
	// 			$disamtfood = 0;
	// 			$disamtliq = 0;
	// 			$modifierdatabill = array();
	// 			foreach ($anss as $lkey => $result) {
	// 				foreach($anss as $lkeys => $results){
	// 					if($lkey == $lkeys) {

	// 					} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
	// 						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 							if($result['parent'] == '0'){
	// 								$result['code'] = '';
	// 							}
	// 						}
	// 					} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
	// 						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
	// 							if($result['parent'] == '0'){
	// 								$result['qty'] = $result['qty'] + $results['qty'];
	// 								if($result['nc_kot_status'] == '0'){
	// 									$result['amt'] = $result['qty'] * $result['rate'];
	// 								}
	// 							}
	// 						}
	// 					}
	// 				}
					
	// 				if($result['code'] != ''){
	// 					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
	// 					if($decimal_mesurement->num_rows > 0){
	// 						if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
	// 							$qty = (int)$result['qty'];
	// 						} else {
	// 								$qty = $result['qty'];
	// 						}
	// 					} else {
	// 						$qty = $result['qty'];
	// 					}
	// 					if($result['is_liq']== 0){
	// 						$infos[] = array(
	// 							'id'			=> $result['id'],
	// 							'billno'		=> $result['billno'],
	// 							'name'          => $result['name'],
	// 							'rate'          => $result['rate'],
	// 							'amt'           => $result['amt'],
	// 							'qty'         	=> $qty,
	// 							'tax1'         	=> $result['tax1'],
	// 							'tax2'          => $result['tax2'],
	// 							'discount_value'=> $result['discount_value']
	// 						);
	// 						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$totalquantityfood = $totalquantityfood + $result['qty'];
	// 						$disamtfood = $disamtfood + $result['discount_value'];
	// 					} else {
	// 						$flag = 1;
	// 						$infosl[] = array(
	// 							'id'			=> $result['id'],
	// 							'billno'		=> $result['billno'],
	// 							'name'          => $result['name'],
	// 							'rate'          => $result['rate'],
	// 							'amt'           => $result['amt'],
	// 							'qty'         	=> $qty,
	// 							'tax1'         	=> $result['tax1'],
	// 							'tax2'          => $result['tax2'],
	// 							'discount_value'=> $result['discount_value']
	// 						);
	// 						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
	// 						$totalquantityliq = $totalquantityliq + $result['qty'];
	// 						$disamtliq = $disamtliq + $result['discount_value'];
	// 					}
	// 				}
	// 			}
				
	// 			$merge_datas = array();
	// 			if($orderno != '0'){
	// 				$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
	// 			}
				
	// 			if($ans['parcel_status'] == '0'){
	// 				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 					$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
	// 				} else {
	// 					$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
	// 				}
	// 			} else {
	// 				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
	// 					$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
	// 				} else {
	// 					$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
	// 				}
	// 			}

				
				
	// 			$csgst=$ans['gst']/2;
	// 			$csgsttotal = $ans['gst'];

	// 			$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
	// 			$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
	// 			$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
	// 			$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 			$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
	// 			if($ans['advance_amount'] == '0.00'){
	// 				$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			} else{
	// 				$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
	// 			}
	// 			//$gtotal = ceil($gtotal);
	// 			$gtotal = round($gtotal);

	// 			$printtype = '';
	// 			$printername = '';

	// 			if ($printtype == '' || $printername == '' ) {
	// 				$printtype = $this->user->getPrinterType();
	// 				$printername = $this->user->getPrinterName();
	// 				$bill_copy = 1;
	// 			}

	// 			if ($printtype == '' || $printername == '' ) {
	// 				$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
	// 				if($locationData->num_rows > 0){
	// 					$locationData = $locationData->row;
	// 					$printtype = $locationData['bill_printer_type'];
	// 					$printername = $locationData['bill_printer_name'];
	// 					$bill_copy = $locationData['bill_copy'];
	// 				} else{
	// 					$printtype = '';
	// 					$printername = '';
	// 					$bill_copy = 1;
	// 				}
	// 			}
	// 			// $this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
				
	// 			if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
	// 				$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
	// 			 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
	// 			}
	// 			$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
	// 			$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

	// 				if($printerModel ==0){
	// 					try {
	// 						    if($printtype == 'Network'){
	// 						 		$connector = new NetworkPrintConnector($printername, 9100);
	// 						 	} else if($printtype == 'Windows'){
	// 						 		$connector = new WindowsPrintConnector($printername);
	// 						 	} else {
	// 						 		$connector = '';
	// 						 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						 	}
	// 						 	if($connector != ''){
	// 							    $printer = new Printer($connector);
	// 							    $printer->selectPrintMode(32);

	// 							   	$printer->setEmphasis(true);
	// 							   	for($i = 1; $i <= $bill_copy; $i++){
	// 								   	$printer->setTextSize(2, 1);
	// 								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 								    $printer->feed(1);
	// 								    $printer->setTextSize(1, 1);
	// 								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 								    if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 								    	$printer->feed(1);
	// 								    	$printer->text("Duplicate Bill");
	// 								    }
	// 								    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								    $printer->feed(1);
	// 								    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 										$printer->feed(1);
	// 										$printer->text(("Mobile :".$ans['cust_contact']));
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Address : ".$ans['cust_address']));
	// 										$printer->feed(1);
	// 										$printer->text("Gst No :".$ans['gst_no']);
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Mobile :".$ans['cust_contact']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text("Mobile :".$ans['cust_contact']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Name :".$ans['cust_name']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}else{
	// 									    $printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 								   	if($infos){
									   			
	// 								   		$printer->feed(1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
	// 								   		$printer->feed(1);
											   		
	// 										/*$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 								   		$printer->feed(1);

	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->setTextSize(2, 2);
	// 								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 								   		$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);*/
	// 								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 								    	/*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
	// 								    	$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
	// 									    $printer->feed(1);*/
	// 								   		$printer->setEmphasis(false);
	// 									    $printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 										$printer->feed(1);
	// 								   	 	$printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setEmphasis(false);
	// 										$total_items_normal = 0;
	// 										$total_quantity_normal = 0;
	// 									    foreach($infos as $nkey => $nvalue){
	// 									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
	// 									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 									    	$printer->feed(1);
	// 								    	 	if($modifierdatabill != array()){
	// 											    	foreach($modifierdatabill as $key => $value){
	// 										    			$printer->setTextSize(1, 1);
	// 										    			if($key == $nvalue['id']){
	// 										    				foreach($value as $modata){
	// 										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
	// 														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 													    		$printer->feed(1);
	// 												    		}
	// 												    	}
	// 										    		}
	// 										    	}
	// 										    	$total_items_normal ++ ;
	// 									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

	// 									    }
	// 									    $printer->text("----------------------------------------------");
	// 									    $printer->feed(1);
	// 									    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
	// 									    $printer->feed(1);
	// 									    $printer->setEmphasis(false);
	// 									   	$printer->setTextSize(1, 1);
	// 									    $printer->text("----------------------------------------------");
	// 										$printer->feed(1);
	// 										/*// foreach($testfoods as $tkey => $tvalue){
	// 										// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
	// 									 //    	$printer->feed(1);
	// 										// }
	// 										$printer->text("----------------------------------------------");
	// 										$printer->feed(1);*/
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										if($ans['fdiscountper'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										} elseif($ans['discount'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										}
	// 										$printer->text(str_pad("",35)."SCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",35)."CCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",30)."Packaging :".$wera_order_datas['packaging']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
	// 										$printer->feed(1);
											
	// 										//order_ids
											
	// 										$printer->setEmphasis(true);
	// 										$printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
	// 										$printer->setEmphasis(false);
	// 									}
									   	
	// 									$printer->feed(1);
	// 									$printer->text("----------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(true);
	// 									$printer->setTextSize(2, 2);
	// 									$printer->setJustification(Printer::JUSTIFY_RIGHT);
	// 									$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
	// 									$printer->setTextSize(1, 1);
	// 									$printer->feed(1);
										
	// 									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 									if($SETTLEMENT_status == '1'){
	// 										if(isset($this->session->data['credit'])){
	// 											$credit = $this->session->data['credit'];
	// 										} else {
	// 											$credit = '0';
	// 										}
	// 										if(isset($this->session->data['cash'])){
	// 											$cash = $this->session->data['cash'];
	// 										} else {
	// 											$cash = '0';
	// 										}
	// 										if(isset($this->session->data['online'])){
	// 											$online = $this->session->data['online'];
	// 										} else {
	// 											$online ='0';
	// 										}

	// 										if(isset($this->session->data['onac'])){
	// 											$onac = $this->session->data['onac'];
	// 											$onaccontact = $this->session->data['onaccontact'];
	// 											$onacname = $this->session->data['onacname'];

	// 										} else {
	// 											$onac ='0';
	// 										}
	// 									}
	// 									if($SETTLEMENT_status=='1'){
	// 										if($credit!='0' && $credit!=''){
	// 											$printer->text("PAY BY: CARD");
	// 										}
	// 										if($online!='0' && $online!=''){
	// 											$printer->text("PAY BY: ONLINE");
	// 										}
	// 										if($cash!='0' && $cash!=''){
	// 											$printer->text("PAY BY: CASH");
	// 										}
	// 										if($onac!='0' && $onac!=''){
	// 											$printer->text("PAY BY: ON.ACCOUNT");
	// 											$printer->feed(1);
	// 											$printer->text("Name: '".$onacname."'");
	// 											$printer->feed(1);
	// 											$printer->text("Contact: '".$onaccontact."'");
	// 										}
	// 									}
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->text("----------------------------------------------");
	// 								    $printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(false);
	// 								   	$printer->setTextSize(1, 1);
									   
										
	// 									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 									$printer->feed(1);
	// 								   	/*$printer->setTextSize(2, 2);

	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
	// 									$printer->feed(1);*/
	// 								   	$printer->setTextSize(1, 1);

	// 									if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 										$printer->feed(1);
	// 									}
	// 									if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 										$printer->feed(1);
	// 									}
	// 									$printer->text("----------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 									}
	// 									$printer->feed(1);
	// 							   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 							    	$printer->setTextSize(2, 2);
	// 									$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 									$printer->feed(1);
	// 							   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 							   		$printer->feed(1);
	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
	// 									$printer->feed(2);
	// 									$printer->cut();
	// 								}
	// 								// Close printer //
	// 							    $printer->close();
	// 						    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

	// 							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 							}
	// 						} catch (Exception $e) {
	// 						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->session->data['warning'] = $printername." "."Not Working";
	// 						}
						
	// 				}
	// 				else{  // 45 space code starts from here

	// 					try {
	// 						    if($printtype == 'Network'){
	// 						 		$connector = new NetworkPrintConnector($printername, 9100);
	// 						 	} else if($printtype == 'Windows'){
	// 						 		$connector = new WindowsPrintConnector($printername);
	// 						 	} else {
	// 						 		$connector = '';
	// 						 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						 	}
	// 						 	if($connector != ''){
	// 							    $printer = new Printer($connector);
	// 							    $printer->selectPrintMode(32);

	// 							   	$printer->setEmphasis(true);
	// 							   	// $printer->text('45 Spacess');
	// 				    			$printer->feed(1);
	// 							   	for($i = 1; $i <= $bill_copy; $i++){
	// 								   	$printer->setTextSize(2, 1);
	// 								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								   	$printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
	// 								    $printer->feed(1);
	// 								    $printer->setTextSize(1, 1);
	// 								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
	// 								    if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 								    	$printer->feed(1);
	// 								    	$printer->text("Duplicate Bill");
	// 								    }
	// 								    $printer->setJustification(Printer::JUSTIFY_CENTER);
	// 								    $printer->feed(1);
	// 								    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 										$printer->feed(1);
	// 										$printer->text(("Mobile :".$ans['cust_contact']));
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Address : ".$ans['cust_address']));
	// 										$printer->feed(1);
	// 										$printer->text("Gst No :".$ans['gst_no']);
	// 										$printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text(("Name : ".$ans['cust_name']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text(("Mobile :".$ans['cust_contact']));
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
	// 									 	$printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
	// 									 	$printer->text("Mobile :".$ans['cust_contact']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Name :".$ans['cust_name']."");
	// 									    $printer->feed(1);
	// 									}
	// 									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
	// 									    $printer->text("Address : ".$ans['cust_address']."");
	// 									    $printer->feed(1);
	// 									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
	// 									    $printer->text("Gst No :".$ans['gst_no']."");
	// 									    $printer->feed(1);
	// 									}else{
	// 									    $printer->text("Name : ".$ans['cust_name']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Mobile :".$ans['cust_contact']."");
	// 									    $printer->feed(1);
	// 									    $printer->text("Address : ".$ans['cust_address']);
	// 									    $printer->feed(1);
	// 									    $printer->text("Gst No :".$ans['gst_no']);
	// 									    $printer->feed(1);
	// 									}
	// 									$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
	// 								    $printer->setEmphasis(true);
	// 								   	$printer->setTextSize(1, 1);
	// 								   	if($infos){
									   			
	// 								   		$printer->feed(1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
	// 								   		$printer->feed(1);
	// 										/*$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 								   		$printer->feed(1);

	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								   		$printer->setTextSize(2, 2);
	// 								   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 								   		$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->setJustification(Printer::JUSTIFY_LEFT);*/
	// 								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
	// 								    	/*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
	// 								    	$printer->feed(1);
	// 								   		$printer->setTextSize(1, 1);
	// 								   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
	// 									    $printer->feed(1);*/
	// 								   		$printer->setEmphasis(false);
	// 									    $printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
	// 										$printer->feed(1);
	// 								   	 	$printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										$printer->setEmphasis(false);
	// 										$total_items_normal = 0;
	// 										$total_quantity_normal = 0;
	// 									    foreach($infos as $nkey => $nvalue){
	// 									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
	// 									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
	// 									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
	// 									    	$printer->feed(1);
	// 								    	 	if($modifierdatabill != array()){
	// 											    	foreach($modifierdatabill as $key => $value){
	// 										    			$printer->setTextSize(1, 1);
	// 										    			if($key == $nvalue['id']){
	// 										    				foreach($value as $modata){
	// 										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
	// 														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
	// 														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
	// 													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
	// 													    		$printer->feed(1);
	// 												    		}
	// 												    	}
	// 										    		}
	// 										    	}
	// 										    	$total_items_normal ++ ;
	// 									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

	// 									    }
	// 									    $printer->text("------------------------------------------");
	// 									    $printer->feed(1);
	// 									    $printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
	// 									    $printer->feed(1);
	// 									    $printer->setEmphasis(false);
	// 									   	$printer->setTextSize(1, 1);
	// 									    $printer->text("------------------------------------------");
	// 										$printer->feed(1);
	// 										// foreach($testfoods as $tkey => $tvalue){
	// 										// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
	// 									 //    	$printer->feed(1);
	// 										// }
	// 										// $printer->text("------------------------------------------");
	// 										// $printer->feed(1);
	// 										$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 										if($ans['fdiscountper'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										} elseif($ans['discount'] != '0'){
	// 											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
	// 											$printer->feed(1);
	// 										}
	// 										$printer->text(str_pad("",25)."SCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",25)."CCGST :".$csgst."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging :".$wera_order_datas['packaging']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
	// 										$printer->feed(1);
	// 										$printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
	// 										$printer->feed(1);
											
	// 										$printer->setEmphasis(true);
	// 										$printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
	// 										$printer->setEmphasis(false);
	// 									}
									   	
	// 									$printer->feed(1);
	// 									$printer->text("------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(true);
	// 									if($ans['advance_amount'] != '0.00'){
	// 										$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
	// 										$printer->feed(1);
	// 									}
	// 									$printer->setTextSize(2, 2);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
	// 									$printer->setTextSize(1, 1);
	// 									$printer->feed(1);
	// 									if($ans['dtotalvalue']!=0){
	// 											$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
	// 											$printer->feed(1);
	// 										}
	// 									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
	// 									if($SETTLEMENT_status == '1'){
	// 										if(isset($this->session->data['credit'])){
	// 											$credit = $this->session->data['credit'];
	// 										} else {
	// 											$credit = '0';
	// 										}
	// 										if(isset($this->session->data['cash'])){
	// 											$cash = $this->session->data['cash'];
	// 										} else {
	// 											$cash = '0';
	// 										}
	// 										if(isset($this->session->data['online'])){
	// 											$online = $this->session->data['online'];
	// 										} else {
	// 											$online ='0';
	// 										}

	// 										if(isset($this->session->data['onac'])){
	// 											$onac = $this->session->data['onac'];
	// 											$onaccontact = $this->session->data['onaccontact'];
	// 											$onacname = $this->session->data['onacname'];

	// 										} else {
	// 											$onac ='0';
	// 										}
	// 									}
	// 									if($SETTLEMENT_status=='1'){
	// 										if($credit!='0' && $credit!=''){
	// 											$printer->text("PAY BY: CARD");
	// 										}
	// 										if($online!='0' && $online!=''){
	// 											$printer->text("PAY BY: ONLINE");
	// 										}
	// 										if($cash!='0' && $cash!=''){
	// 											$printer->text("PAY BY: CASH");
	// 										}
	// 										if($onac!='0' && $onac!=''){
	// 											$printer->text("PAY BY: ON.ACCOUNT");
	// 											$printer->feed(1);
	// 											$printer->text("Name: '".$onacname."'");
	// 											$printer->feed(1);
	// 											$printer->text("Contact: '".$onaccontact."'");
	// 										}
	// 									}
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 								    $printer->text("------------------------------------------");
	// 								    $printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 									$printer->setEmphasis(false);
	// 								   	$printer->setTextSize(1, 1);
									   
										
	// 									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
	// 									$printer->feed(1);
	// 								   /*	$printer->setTextSize(2, 2);

	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp']) );
	// 											$printer->feed(1);*/
	// 								   	$printer->setTextSize(1, 1);

	// 									if($this->model_catalog_order->get_settings('TEXT1') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
	// 										$printer->feed(1);
	// 									}
	// 									if($this->model_catalog_order->get_settings('TEXT2') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
	// 										$printer->feed(1);
	// 									}
	// 									$printer->text("------------------------------------------");
	// 									$printer->feed(1);
	// 									$printer->setJustification(Printer::JUSTIFY_CENTER);
	// 									if($this->model_catalog_order->get_settings('TEXT3') != ''){
	// 										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
	// 									}
	// 									$printer->feed(1);
	// 							   		$printer->setJustification(Printer::JUSTIFY_LEFT);
	// 							    	$printer->setTextSize(2, 2);
	// 									$printer->text("Order From: ".$wera_order_datas['order_from']."");
	// 									$printer->feed(1);
	// 							   		$printer->text("Order No: ".$wera_order_datas['order_id']."");
	// 							   		$printer->feed(1);
	// 									$printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
	// 									$printer->feed(2);
	// 									$printer->cut();
	// 								}
	// 								// Close printer //
	// 							    $printer->close();
	// 						    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

	// 							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
	// 							}
	// 						} catch (Exception $e) {
	// 						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
	// 						    $this->session->data['warning'] = $printername." "."Not Working";
	// 						}
	// 				}
	// 				if($ans['bill_status'] == 1 && $duplicate == '1'){
	// 					$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// 				} else{
	// 					$json = array();
	// 					$json = array(
	// 						'LOCAL_PRINT' => 0,
	// 						'status' => 1,
	// 					);
	// 				}
	// 				$this->response->addHeader('Content-Type: application/json');
	// 			$this->response->setOutput(json_encode($json));
				
	// 		} else {
	// 			$this->session->data['warning'] = 'Bill already printed';
	// 			$this->request->post = array();
	// 			$_POST = array();
	// 			$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// 		}
	// 	}

	// 	// $this->request->post = array();
	// 	// $_POST = array();
	// 	// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	// }

	public function bill_print() {
        
        if (isset($this->request->get['order_id'])) {

            //$orderid = $this->request->get['order_id'];

            $order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE urbanpiper_order_id = '".$this->request->get['order_id']."'")->row;
            $order_id = $order_ids['order_id'];
            //$wera_order_datas = $this->db->query("SELECT * FROM oc_werafood_orders WHERE order_id = '".$orderid."'")->row;

            /*$order_ids = $this->db->query("SELECT * FROM oc_order_info WHERE wera_order_id = '".$orderid."'")->row;

            $order_id = $order_ids['order_id'];*/
            $ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
            $zomato_order_id_datas = $this->db->query("SELECT `zomato_order_id`,`zomato_rider_otp` FROM `oc_orders_app` WHERE online_order_id = '".$this->request->get['order_id']."'")->row;
        
            $duplicate = '0';
            $advance_id = '0';
            $advance_amount = '0';
            $grand_total = '0';
            $orderno = '0';
                /*echo'<pre>';
            print_r($ans);
            exit;*/
            if(($ans['bill_status'] == 1 ) || ($ans['bill_status'] == 0)){
            
                date_default_timezone_set('Asia/Kolkata');
                $this->load->language('customer/customer');
                $this->load->model('catalog/order');
                $merge_datas = array();
                $this->document->setTitle('BILL');
                $te = 'BILL';
                
                $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                $last_open_dates = $this->db->query($last_open_date_sql);
                if($last_open_dates->num_rows > 0){
                    $last_open_date = $last_open_dates->row['bill_date'];
                } else {
                    $last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
                    $last_open_dates = $this->db->query($last_open_date_sql);
                    if($last_open_dates->num_rows > 0){
                        $last_open_date = $last_open_dates->row['bill_date'];
                    } else {
                        $last_open_date = date('Y-m-d');
                    }
                }

                $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                if($last_open_dates_liq->num_rows > 0){
                    $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                } else {
                    $last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

                    $last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

                    if($last_open_dates_liq->num_rows > 0){
                        $last_open_date_liq = $last_open_dates_liq->row['bill_date'];
                    } else {
                        $last_open_date_liq = date('Y-m-d');
                    }
                }

                $last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
                $last_open_dates_order = $this->db->query($last_open_date_sql_order);
                if($last_open_dates_order->num_rows > 0){
                    $last_open_date_order = $last_open_dates_order->row['bill_date'];
                } else {
                    $last_open_date_order = date('Y-m-d');
                }

                if($ans['order_no'] == '0'){
                    $orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
                    $orderno = 1;
                    if(isset($orderno_q['order_no'])){
                        $orderno = $orderno_q['order_no'] + 1;
                    }

                    $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                    if($kotno2->num_rows > 0){
                        $kot_no2 = $kotno2->row['billno'];
                        $kotno = $kot_no2 + 1;
                    } else{
                        $kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
                        if($kotno2->num_rows > 0){
                            $kot_no2 = $kotno2->row['billno'];
                            $kotno = $kot_no2 + 1;
                        } else {
                            $kotno = 1;
                        }
                    }

                    $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                    if($kotno1->num_rows > 0){
                        $kot_no1 = $kotno1->row['billno'];
                        $botno = $kot_no1 + 1;
                    } else{
                        $kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
                        if($kotno1->num_rows > 0){
                            $kot_no1 = $kotno1->row['billno'];
                            $botno = $kot_no1 + 1;
                        } else {
                            $botno = 1;
                        }
                    }

                    $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
                    $this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

                    //$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
                        $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    } else{
                        $update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
                    }
                    $apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
                    if($apporder['app_order_id'] != '0'){
                        $this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
                    }
                    $this->db->query($update_sql);
                }

                if($advance_id != '0'){
                    $this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
                }
                $anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
                // echo'<pre>';
                // print_r($anss);
                // exit;

                $testfood = array();
                $testliq = array();
                $testtaxvalue1food = 0;
                $testtaxvalue1liq = 0;
                $tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
                foreach($tests as $test){
                    $amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
                    $testfoods[] = array(
                        'tax1' => $test['tax1'],
                        'amt' => $amt,
                        'tax1_value' => $test['tax1_value']
                    );
                }
                
                $testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
                foreach($testss as $testa){
                    $amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
                    $testliqs[] = array(
                        'tax1' => $testa['tax1'],
                        'amt' => $amts,
                        'tax1_value' => $testa['tax1_value']
                    );
                }
                
                $infosl = array();
                $infos = array();
                $flag = 0;
                $totalquantityfood = 0;
                $totalquantityliq = 0;
                $disamtfood = 0;
                $disamtliq = 0;
                $modifierdatabill = array();
                foreach ($anss as $lkey => $result) {
                    foreach($anss as $lkeys => $results){
                        if($lkey == $lkeys) {

                        } elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['code'] = '';
                                }
                            }
                        } elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
                            if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
                                if($result['parent'] == '0'){
                                    $result['qty'] = $result['qty'] + $results['qty'];
                                    if($result['nc_kot_status'] == '0'){
                                        $result['amt'] = $result['qty'] * $result['rate'];
                                    }
                                }
                            }
                        }
                    }
                    
                    if($result['code'] != ''){
                        $decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ");
                        if($decimal_mesurement->num_rows > 0){
                            if ($decimal_mesurement->row['decimal_mesurement'] == 0) {
                                $qty = (int)$result['qty'];
                            } else {
                                    $qty = $result['qty'];
                            }
                        } else {
                            $qty = $result['qty'];
                        }
                        if($result['is_liq']== 0){
                            $infos[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityfood = $totalquantityfood + $result['qty'];
                            $disamtfood = $disamtfood + $result['discount_value'];
                        } else {
                            $flag = 1;
                            $infosl[] = array(
                                'id'            => $result['id'],
                                'billno'        => $result['billno'],
                                'name'          => $result['name'],
                                'rate'          => $result['rate'],
                                'amt'           => $result['amt'],
                                'qty'           => $qty,
                                'tax1'          => $result['tax1'],
                                'tax2'          => $result['tax2'],
                                'discount_value'=> $result['discount_value']
                            );
                            $modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
                            $totalquantityliq = $totalquantityliq + $result['qty'];
                            $disamtliq = $disamtliq + $result['discount_value'];
                        }
                    }
                }
                
                $merge_datas = array();
                if($orderno != '0'){
                    $merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
                }
                
                if($ans['parcel_status'] == '0'){
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    } else {
                        $gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
                    }
                } else {
                    if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
                        $gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    } else {
                        $gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
                    }
                }

                
                
                $csgst=$ans['gst']/2;
                $csgsttotal = $ans['gst'];

                $ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 125);
                $ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
                $ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
                $ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
                $ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
                $csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
                if($ans['advance_amount'] == '0.00'){
                    $gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
                } else{
                    $gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
                }
                //$gtotal = ceil($gtotal);
                $gtotal = round($gtotal);

                $printtype = '';
                $printername = '';

                if ($printtype == '' || $printername == '' ) {
                    $printtype = $this->user->getPrinterType();
                    $printername = $this->user->getPrinterName();
                    $bill_copy = 1;
                }

                if ($printtype == '' || $printername == '' ) {
                    $locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
                    if($locationData->num_rows > 0){
                        $locationData = $locationData->row;
                        $printtype = $locationData['bill_printer_type'];
                        $printername = $locationData['bill_printer_name'];
                        $bill_copy = $locationData['bill_copy'];
                    } else{
                        $printtype = '';
                        $printername = '';
                        $bill_copy = 1;
                    }
                }
                // $this->db->query("UPDATE oc_werafood_orders SET status = 'Order Placed' WHERE order_id = '".$orderid."'");
                
                if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
                    $printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
                    $printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
                }
                $printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
                $LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

                    if($printerModel ==0){
                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                        //if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            /*$printer->text("Gst No :".$ans['gst_no']."");
                                            $printer->feed(1);*/
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                           /* $printer->text("Gst No :".$ans['gst_no']);
                                            $printer->feed(1);*/
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                                            //$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
                                            $printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['date_added'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s', strtotime($ans['time_added']))."");
                                            $printer->feed(1);
                                                    
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);
                                            /*// foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            $printer->text("----------------------------------------------");
                                            $printer->feed(1);*/
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",35)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",35)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                            if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                           

                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",25)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            //order_ids
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",29)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_RIGHT);
                                        //$printer->text("GRAND TOTAL  :  ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL  :  ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                        /*$printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                        $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("----------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                       /* $printer->text("Order Reference: ".$ans['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        $printer->feed(1);*/
                                        if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                       	if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                        
                    }
                    else{  // 45 space code starts from here

                        try {
                                if($printtype == 'Network'){
                                    $connector = new NetworkPrintConnector($printername, 9100);
                                } else if($printtype == 'Windows'){
                                    $connector = new WindowsPrintConnector($printername);
                                } else {
                                    $connector = '';
                                    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                }
                                if($connector != ''){
                                    $printer = new Printer($connector);
                                    $printer->selectPrintMode(32);

                                    $printer->setEmphasis(true);
                                    // $printer->text('45 Spacess');
                                    $printer->feed(1);
                                    for($i = 1; $i <= $bill_copy; $i++){
                                        $printer->setTextSize(2, 1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
                                        $printer->feed(1);
                                        $printer->setTextSize(1, 1);
                                        $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
                                       // if($ans['bill_status'] == 1 && $duplicate == '1'){
                                        if($ans['bill_status'] == 1 ){
                                            $printer->feed(1);
                                            $printer->text("Duplicate Bill");
                                        }
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
                                            
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
                                            $printer->text(("Address : ".$ans['cust_address']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text(("Name : ".$ans['cust_name']));
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text(("Mobile :".$ans['cust_contact']));
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']);
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Name :".$ans['cust_name']."");
                                            $printer->feed(1);
                                        }
                                        else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                        }else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
                                            $printer->text("Address : ".$ans['cust_address']."");
                                            $printer->feed(1);
                                        }else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
                                            // $printer->text("Gst No :".$ans['gst_no']."");
                                            // $printer->feed(1);
                                        }else{
                                            $printer->text("Name : ".$ans['cust_name']);
                                            $printer->feed(1);
                                            $printer->text("Mobile :".$ans['cust_contact']."");
                                            $printer->feed(1);
                                            $printer->text("Address : ".$ans['cust_address']);
                                            $printer->feed(1);
                                            // $printer->text("Gst No :".$ans['gst_no']);
                                            // $printer->feed(1);
                                        }
                                        $printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
                                        $printer->setEmphasis(true);
                                        $printer->setTextSize(1, 1);
                                        if($infos){
                                                
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
                                            $printer->feed(1);
                                            /*$printer->text("Order Reference: ".$ans['order_id']."");
                                            $printer->feed(1);

                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->setTextSize(2, 2);
                                            $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);*/
                                            // $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
                                            /*$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
                                            $printer->feed(1);
                                            $printer->setTextSize(1, 1);
                                            $printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
                                            $printer->feed(1);*/
                                            $printer->setEmphasis(false);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
                                            $printer->feed(1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $total_items_normal = 0;
                                            $total_quantity_normal = 0;
                                            foreach($infos as $nkey => $nvalue){
                                                $nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                $nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
                                                $nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                $nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
                                                $printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
                                                $printer->feed(1);
                                                if($modifierdatabill != array()){
                                                        foreach($modifierdatabill as $key => $value){
                                                            $printer->setTextSize(1, 1);
                                                            if($key == $nvalue['id']){
                                                                foreach($value as $modata){
                                                                    $modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
                                                                    $modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
                                                                    $modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
                                                                    $printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
                                                                    $printer->feed(1);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $total_items_normal ++ ;
                                                    $total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

                                            }
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
                                            $printer->feed(1);
                                            $printer->setEmphasis(false);
                                            $printer->setTextSize(1, 1);
                                            $printer->text("------------------------------------------");
                                            $printer->feed(1);
                                            // foreach($testfoods as $tkey => $tvalue){
                                            //  $printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
                                         //     $printer->feed(1);
                                            // }
                                            // $printer->text("------------------------------------------");
                                            // $printer->feed(1);
                                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                                            // if($ans['fdiscountper'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // } elseif($ans['discount'] != '0'){
                                            //     $printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
                                            //     $printer->feed(1);
                                            // }
                                            $printer->text(str_pad("",25)."SCGST :".$csgst."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",25)."CCGST :".$csgst."");
                                            $printer->feed(1);
                                           if($ans['packaging'] > 0){
                                                 $printer->text(str_pad("",30)."Packaging :".number_format($ans['packaging'],2)."");
                                                $printer->feed(1);
                                            }

                                            if($ans['delivery_charges'] > 0){
                                                 $printer->text(str_pad("",24)."Delivery Charges :".number_format($ans['delivery_charges'],2)."");
                                                $printer->feed(1);
                                            }
                                            if($ans['discount_reason'] != ''){
                                                $printer->text(str_pad("",31)."Coupon :".$ans['discount_reason']."");
                                                $printer->feed(1);
                                            }
                                            if($ans['ftotalvalue'] > 0){
                                                $printer->text(str_pad("",31)."Discount :".number_format($ans['ftotalvalue'],2)."");
                                                $printer->feed(1);
                                            }
                                            /*$printer->text(str_pad("",20)."Packaging CGST :".$wera_order_datas['packaging_cgst']."");
                                            $printer->feed(1);
                                            $printer->text(str_pad("",20)."Packaging SGST :".$wera_order_datas['packaging_sgst']."");
                                            $printer->feed(1);*/
                                            
                                            $printer->setEmphasis(true);
                                            $printer->text(str_pad("",25)."Net total :".($ans['grand_total'])."");
                                            $printer->setEmphasis(false);
                                        }
                                        
                                        $printer->feed(1);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(true);
                                        if($ans['advance_amount'] != '0.00'){
                                            $printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
                                            $printer->feed(1);
                                        }
                                        $printer->setTextSize(2, 2);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        //$printer->text("GRAND TOTAL : ".round($ans['grand_total']));
                                        $printer->text("GRAND TOTAL : ".$ans['grand_total']);
                                        $printer->setTextSize(1, 1);
                                        $printer->feed(1);
                                        if($ans['dtotalvalue']!=0){
                                                $printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
                                                $printer->feed(1);
                                            }
                                        $SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
                                        if($SETTLEMENT_status == '1'){
                                            if(isset($this->session->data['credit'])){
                                                $credit = $this->session->data['credit'];
                                            } else {
                                                $credit = '0';
                                            }
                                            if(isset($this->session->data['cash'])){
                                                $cash = $this->session->data['cash'];
                                            } else {
                                                $cash = '0';
                                            }
                                            if(isset($this->session->data['online'])){
                                                $online = $this->session->data['online'];
                                            } else {
                                                $online ='0';
                                            }

                                            if(isset($this->session->data['onac'])){
                                                $onac = $this->session->data['onac'];
                                                $onaccontact = $this->session->data['onaccontact'];
                                                $onacname = $this->session->data['onacname'];

                                            } else {
                                                $onac ='0';
                                            }
                                        }
                                        if($SETTLEMENT_status=='1'){
                                            if($credit!='0' && $credit!=''){
                                                $printer->text("PAY BY: CARD");
                                            }
                                            if($online!='0' && $online!=''){
                                                $printer->text("PAY BY: ONLINE");
                                            }
                                            if($cash!='0' && $cash!=''){
                                                $printer->text("PAY BY: CASH");
                                            }
                                            if($onac!='0' && $onac!=''){
                                                $printer->text("PAY BY: ON.ACCOUNT");
                                                $printer->feed(1);
                                                $printer->text("Name: '".$onacname."'");
                                                $printer->feed(1);
                                                $printer->text("Contact: '".$onaccontact."'");
                                            }
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setEmphasis(false);
                                        $printer->setTextSize(1, 1);
                                       
                                        
                                        $printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
                                        $printer->feed(1);
                                       /*   $printer->setTextSize(2, 2);

                                        $printer->text("Order OTP :".($wera_order_datas['order_otp']) );
                                                $printer->feed(1);*/
                                        $printer->setTextSize(1, 1);

                                        if($this->model_catalog_order->get_settings('TEXT1') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT1'));
                                            $printer->feed(1);
                                        }
                                        if($this->model_catalog_order->get_settings('TEXT2') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT2'));
                                            $printer->feed(1);
                                        }
                                        $printer->text("------------------------------------------");
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                                        if($this->model_catalog_order->get_settings('TEXT3') != ''){
                                            $printer->text($this->model_catalog_order->get_settings('TEXT3'));
                                        }
                                        $printer->feed(1);
                                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                                        $printer->setTextSize(2, 2);
                                        /*$printer->text("Order Reference: ".$ans['order_id']."");
                                        $printer->feed(1);
                                        $printer->text("Online Order No: ".$ans['urbanpiper_order_id']."");
                                        $printer->feed(1);*/
                                         if($zomato_order_id_datas['zomato_order_id'] > 0){
                                            //$printer->text("Aggre. Order ID: ".$zomato_order_id_datas['zomato_order_id']."");
                                            $printer->text("Aggre. Order ID: ".substr($zomato_order_id_datas['zomato_order_id'], -5)."");
                                            $printer->feed(1);
                                        }
                                        if($zomato_order_id_datas['zomato_rider_otp'] != ''){
                                            $printer->text("Order OTP :".($zomato_order_id_datas['zomato_rider_otp'] ));
                                            $printer->feed(2);
                                        }
                                        // $printer->text("Order OTP :".($wera_order_datas['order_otp'] ));
                                        // $printer->feed(2);
                                        $printer->cut();
                                    }
                                    // Close printer //
                                    $printer->close();
                                    $this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

                                    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
                                }
                            } catch (Exception $e) {
                                $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
                                $this->session->data['warning'] = $printername." "."Not Working";
                            }
                    }
                    if($ans['bill_status'] == 1 && $duplicate == '1'){
                        $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
                    } else{
                        $json = array();
                        $json = array(
                            'LOCAL_PRINT' => 0,
                            'status' => 1,
                        );
                    }
                    $json['done'] = '<script>parent.closeIFrame();</script>';
                    $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                
            } else {
                $this->session->data['warning'] = 'Bill already printed';
                $this->request->post = array();
                $_POST = array();
                $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
            }
        }

        // $this->request->post = array();
        // $_POST = array();
        // $this->response->redirect($this->url->link('catalog/urbanpiper_order', 'token=' . $this->session->data['token'],true));
    }

	public function order_info_datas(){
		$json = array();

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			
			$customer_data = $this->db->query("SELECT * FROM `oc_werafood_order_customer` WHERE order_id = '".$order_id."'")->row;
			$json['customer_data'] = array(
				'name' => $customer_data['name'],
				'phone_number' => $customer_data['phone_number'],
				'email' => $customer_data['email'],
				'address' => $customer_data['address'],

			);

			$results = $this->db->query("SELECT * FROM `oc_werafood_order_items` WHERE order_id = '".$order_id."'")->rows;

			foreach ($results as $result) {
				

				$json['item_data'][] = array(
					'order_id' => $result['order_id'],
					'instructions' => $result['instructions'],
					'item_quantity' => $result['item_quantity'],
					'item_unit_price' => $result['item_unit_price'],
					'subtotal' => $result['subtotal'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		// echo'<pre>';
		// print_r($json);
		// exit;

		$sort_order = array();
		//$json['sucess'] = 1;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	function is_connected(){
	    $connected = @fsockopen("www.google.com", 80); 
	    if ($connected){
	        $is_conn = true; 
	        fclose($connected);
	    }else{
	        $is_conn = false; 
	    }
	    return $is_conn;
	}

	// public function check_transection(){
	// 	//echo "<pre>";print_r($this->request->post);exit;
	// 	if(isset($this->request->post['view'])){
	// 		$json = array();
	// 		$url = NEW_TRANSECTION_COUNT;
	// 		$final_data = array();
	// 		$data_json = json_encode($final_data);
	// 		$ch = curl_init();
	// 		curl_setopt($ch, CURLOPT_URL, $url);
	// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
	// 		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
	// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	// 		$response  = curl_exec($ch);
	// 		$results = json_decode($response,true);
	// 		//echo "<pre>";print_r($results);exit;
	// 		$json['unseen_notification'] = 0;
	// 		if (!empty($results)) {
	// 			$json['unseen_notification'] = $results;
	// 		}//echo "<pre>";print_r($json);exit;
	// 		$this->response->addHeader('Content-Type: application/json');
	// 		$this->response->setOutput(json_encode($json));
	// 	}
	// }

	// function is_connectednet(){
	// 	$json = array();
	//     $connected = @fsockopen("www.google.com", 80); 
	//     if ($connected){
	//         $is_conn = true; 
	//         fclose($connected);
	//     }else{
	//         $is_conn = false; 
	//     }
	//     $json['is_conn'] =  $is_conn;
	//     //echo "<pre>";print_r($json);exit;
	//     $this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
	// public function check_transection() {
	// 	// echo'innn';
	// 	// exit;
	// 	$json = array();
	// 	date_default_timezone_set("Asia/Kolkata");
	// 	$this->load->language('catalog/order');
	// 	$this->document->setTitle('Online Order');
	// 	$this->log->write('Start Orders');
	// 	$this->log->write(date('Y-m-d h:i:s'));

	// 	$url = FETCH_ONLINE_URBANPIPER_DATAS;
	// 	$final_data = array();
	// 	$data_json = json_encode($final_data);
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
	// 	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	// 	$response  = curl_exec($ch);
	// 	$results = json_decode($response,true);
	// 	curl_close($ch);
	// 	//echo "<pre>";print_r($results);exit;
	// 	$this->log->write('Fetch Order');
	// 	$this->log->write(date('Y-m-d h:i:s'));
	// 	$json['unseen_notification'] = 0;
	// 	if (!empty($results)) {
	// 		$json['unseen_notification'] = count($results);
	// 		$all_orders = '';
	// 		foreach ($results['online_orders'] as $result) {
	// 			$orders_app = $this->db->query("INSERT INTO oc_orders_app SET
	// 				`order_id` = '".$result['order_id']."', 
	// 				`packaging` = '".$result['packaging']."', 
	// 				`tot_qty` = '".$result['tot_qty']."', 
	// 				`total` = '".$result['total']."', 
	// 				`grand_tot`  = '".$result['grand_tot']."', 
	// 				`tot_gst` = '".$this->db->escape($result['tot_gst'])."',
	// 				`gst_val` = '".$this->db->escape($result['gst_val'])."',
	// 				`order_time` = '".$result['gst_val']."', 
	// 				`order_date` = '".$result['order_date']."', 
	// 				`address_1` = '".$this->db->escape($result['address_1'])."', 
	// 				`address_2` = '".$this->db->escape($result['address_2'])."', 
	// 				`city` = '".$this->db->escape($result['city'], $this->conn)."',
	// 				`postcode` = '".$this->db->escape($result['postcode'])."',
	// 				`order_status` = '1', area = '".$this->db->escape($result['area'])."' ,
	// 				`online_order_status` = '".$this->db->escape($result['online_order_status'])."',
	// 				`online_order_type_placed_delie` = '".$this->db->escape($result['online_order_type_placed_delie'])."',
	// 				`online_order_id` = '".$this->db->escape($result['online_order_id'])."',
	// 				`online_channel` = '".$this->db->escape($result['online_channel'])."',
	// 				`online_instruction` = '".$this->db->escape($result['online_instruction'])."',
	// 				`online_deliverydatetime` = '".$this->db->escape($result['online_deliverydatetime'])."',
	// 				`online_created_time` = '".$this->db->escape($result['online_created_time'])."',
	// 				`online_store_id` = '".$this->db->escape($result['online_store_id'])."',
	// 				`online_biz_id` = '".$this->db->escape($result['online_biz_id'])."',
	// 				`online_payment_method` = '".$this->db->escape($result['online_payment_method'])."',
	// 				`kot_status` = '0',
	// 				`cust_id` = '".$this->db->escape($result['cust_id'])."',
	// 				`roundtotal` = '".$result['roundtotal']."',
	// 				`ftotal_discount` = '".$this->db->escape($result['ftotal_discount'])."',
	// 				`ftotalvalue` = '".$this->db->escape($result['ftotalvalue'])."',
	// 				`discount` = '".$this->db->escape($result['discount'])."',
	// 				`discount_reason` = '".$this->db->escape($result['discount_reason'])."',
	// 				`online_deliery_type` = '".$this->db->escape($result['online_deliery_type'])."',
	// 				`zomato_order_type` = '".$this->db->escape($result['zomato_order_type'])."',
	// 				`zomato_order_id` = '".$this->db->escape($result['zomato_order_id'])."',
	// 				`zomato_discount` = '".$this->db->escape($result['zomato_discount'])."',
	// 				`pay_online` = '".$result['pay_online']."'
	// 			");
	// 			foreach ($result['online_order_item'] as $okey => $ovalue) {
	// 				$this->db->query("INSERT INTO oc_order_item_app SET
	// 					 
	// 					`order_id` = '".$ovalue['order_id']."', 
	// 					`online_order_id` = '".$this->db->escape($ovalue['online_order_id'])."' ,
	// 					`online_order_status` = '".$this->db->escape($ovalue['online_order_status'])."',
	// 					`item_id` = '".$this->db->escape($ovalue['item_id'])."',
	// 					`item` = '".$this->db->escape($ovalue['item'])."', 
	// 					`qty` = '".$this->db->escape($ovalue['qty'])."',
	// 					`price` = '".$this->db->escape($ovalue['price'])."',
	// 					`total` = '".$this->db->escape($ovalue['total'])."',
	// 					`gst` = '".$ovalue['gst']."', 
	// 					`gst_val` = '".$ovalue['gst_val']."',
	// 					`grand_tot` = '".$this->db->escape($ovalue['grand_tot'])."' ,
	// 					`online_order_type_placed_delie` = '".$this->db->escape($ovalue['online_order_type_placed_delie'])."' ,
	// 					`discount_per` = '".$this->db->escape($ovalue['discount_per'])."',
	// 					`discount_value` = '".$this->db->escape($ovalue['discount_value'])."'
	// 				");
	// 			}

	// 			$cust_details = $this->db->query("SELECT `firstname`,`email`,`telephone`,`date_added`,`customer_id` FROM oc_customer_app WHERE customer_id = '".$result['customer_details']['customer_id']."' ");
	// 			if ($cust_details->num_rows > 0) {
	// 				$this->db->query("UPDATE oc_customer_app SET
	// 					`firstname` = '".$this->db->escape($result['customer_details']['firstname'])."',
	// 					`email`= '".$this->db->escape($result['customer_details']['email'])."',
	// 					`telephone`= '".$this->db->escape($result['customer_details']['telephone'])."', 
	// 					`information_field` = '1' ,
	// 					`customer_type` = 'online' , 
	// 					`date_added` = '".$result['customer_details']['date_added']."'
	// 					WHERE `customer_id` = '".$result['customer_details']['customer_id']."' ");
	// 			} else {
	// 				$this->db->query("INSERT INTO oc_customer_app SET
	// 					`customer_id` = '".$this->db->escape($result['customer_details']['customer_id'])."',
	// 					`firstname` = '".$this->db->escape($result['customer_details']['firstname'])."',
	// 					`email`= '".$this->db->escape($result['customer_details']['email'])."',
	// 					`telephone`= '".$this->db->escape($result['customer_details']['telephone'])."', 
	// 					`information_field` = '1' ,
	// 					`customer_type` = 'online' , 
	// 					`date_added` = '".$result['customer_details']['date_added']."'
	// 				");
	// 			}

	// 			$add_details = $this->db->query("SELECT * FROM oc_address_app WHERE `address_1` = '".$this->db->escape($result['customer_add_details']['address_1'])."' AND `address_2` = '".$this->db->escape($result['customer_add_details']['address_2'])."' AND `city`= '".$this->db->escape($result['customer_add_details']['city'])."' AND `postcode`= '".$this->db->escape($result['customer_add_details']['postcode'])."' AND  `area`= '".$this->db->escape($result['customer_add_details']['area'])."'AND  `customer_id` = '".$this->db->escape($result['customer_add_details']['customer_id'])."' AND `sub_locality`= '".$this->db->escape($result['customer_add_details']['sub_locality'])."'  ");

	// 			if ($cust_details->num_rows == 0) {
	// 				$this->db->query("DELETE FROM `oc_address_app` WHERE `customer_id` = '".$result['customer_details']['customer_id'] ."' ");
	// 				$this->db->query("INSERT INTO `oc_address_app` SET 
	// 				
	// 					`customer_id` = '".$result['customer_details']['customer_id'] ."',
	// 					 `address_1` = '".$this->db->escape($result['customer_add_details']['address_1'])."', 
	// 					 `address_2` = '".$this->db->escape($result['customer_add_details']['address_2'])."', 
	// 					 `city`= '".$this->db->escape($result['customer_add_details']['city'])."', 
	// 					 `postcode`= '".$this->db->escape($result['customer_add_details']['postcode'])."',
	// 					`area`= '".$this->db->escape($result['customer_add_details']['area'])."',
	// 					`firstname` = '".$this->db->escape($result['customer_add_details']['firstname'])."', 
	// 					`sub_locality`= '".$this->db->escape($result['customer_add_details']['sub_locality'])."',
	// 					`country_id` = 99 ,
	// 					`zone_id` = 1493 
	// 				 ");
	// 			} 
				
	// 			if ($orders_app == 1) {
	// 				$all_orders .= ','.$result['online_order_id'];
	// 			}
	// 		}

	// 		if($all_orders != ''){
	// 			$all_order = ltrim($all_orders,',');
	// 			$url = CHECK_ONLINE_DATAS_URBANPIPER;
	// 			$final_data = $all_order;
	// 			$data_json = json_encode($final_data);
	// 			$ch = curl_init();
	// 			curl_setopt($ch, CURLOPT_URL, $url);
	// 			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
	// 			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
	// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	// 			$response  = curl_exec($ch);
	// 			$resultsz = json_decode($response,true);
	// 			curl_close($ch);
	// 			$this->log->write('Update Fetch Order');
	// 			$this->log->write(date('Y-m-d h:i:s'));
	// 		}
	// 	}

	// 	$url = FETCH_RIDER_URBANPIPER;
	// 	$final_data_rider = array();
	// 	$data_json_rider = json_encode($final_data_rider);
	// 	$ch_fetch = curl_init();
	// 	curl_setopt($ch_fetch, CURLOPT_URL, $url);
	// 	curl_setopt($ch_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
	// 	curl_setopt($ch_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 	curl_setopt($ch_fetch, CURLOPT_POSTFIELDS,$data_json_rider);
	// 	curl_setopt($ch_fetch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch_fetch,CURLOPT_SSL_VERIFYPEER, false);
	// 	$rider_response  = curl_exec($ch_fetch);
	// 	$rider_results = json_decode($rider_response,true);
	// 	curl_close($ch_fetch);
	// 	$this->log->write('Fetch Rider');
	// 	$this->log->write(date('Y-m-d h:i:s'));
	// 	//echo "<pre>";print_r($rider_results);exit;
	// 	$all_rider = '';
	// 	if (!empty($rider_results)) {
	// 		foreach ($rider_results as $result_rdr) {
	// 			$insert_rider = $this->db->query("INSERT INTO `oc_urbanpiper_rider` SET 
	// 				`channel_name` = '".$this->db->escape($result_rdr['channel_name']) ."', 
	// 				`channel_order_id` = '".$this->db->escape($result_rdr['channel_order_id']) ."', 
	// 				`rider_name` = '".$this->db->escape($result_rdr['rider_name'])."',
	// 				`rider_alt_phone` = '".$this->db->escape($result_rdr['rider_alt_phone'])."', 
	// 				`rider_phone`= '".$this->db->escape($result_rdr['rider_phone'])."',
	// 				`rider_user_id`= '".$this->db->escape($result_rdr['rider_user_id'])."', 
	// 				`rider_status`= '".$this->db->escape($result_rdr['rider_status'])."', 
	// 				`real_order_id` = '".$this->db->escape($result_rdr['real_order_id'])."', 
	// 				`entry_status`= '1'
	// 				 "
	// 			);
	// 			if ($insert_rider == 1) {
	// 				$all_rider .= ','.$result_rdr['id'];
	// 			}
	// 		}
	// 		if($all_rider != ''){
	// 			$all_riders = ltrim($all_rider,',');
	// 			$url = UPDATE_ENTRY_STATUS_RIDER_URBANPIPER;
	// 			$final_data_update_rider = $all_riders;
	// 			$data_json_rider = json_encode($final_data_update_rider);
	// 			$ch_update = curl_init();
	// 			curl_setopt($ch_update, CURLOPT_URL, $url);
	// 			curl_setopt($ch_update, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
	// 			curl_setopt($ch_update, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 			curl_setopt($ch_update, CURLOPT_POSTFIELDS,$data_json_rider);
	// 			curl_setopt($ch_update, CURLOPT_RETURNTRANSFER, true);
	// 			curl_setopt($ch_update,CURLOPT_SSL_VERIFYPEER, false);
	// 			$updateresponse  = curl_exec($ch_update);
	// 			$resultsz = json_decode($updateresponse,true);
	// 			curl_close($ch_update);
	// 			$this->log->write('Update Fetch Rider');
	// 			$this->log->write(date('Y-m-d h:i:s'));
	// 		}
	// 	}
	// 	$url = FETCH_UPDATE_ORDER_STATUS;
	// 	$final_data_order_fetch = array();
	// 	$data_json_order_fetch = json_encode($final_data_order_fetch);
	// 	$ch_order_fetch = curl_init();
	// 	curl_setopt($ch_order_fetch, CURLOPT_URL, $url);
	// 	curl_setopt($ch_order_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_order_fetch)));
	// 	curl_setopt($ch_order_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 	curl_setopt($ch_order_fetch, CURLOPT_POSTFIELDS,$data_json_order_fetch);
	// 	curl_setopt($ch_order_fetch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch_order_fetch,CURLOPT_SSL_VERIFYPEER, false);
	// 	$response  = curl_exec($ch_order_fetch);
	// 	$order_results = json_decode($response,true);
	// 	curl_close($ch_order_fetch);

	// 	$this->log->write('Order Staus');
	// 			$this->log->write(date('Y-m-d h:i:s'));
	// 	//echo "<pre>";print_r($results);exit;
	// 	$all_order = '';
	// 	if (!empty($order_results)) {
	// 		foreach ($order_results as $order_result) {
	// 			$insert_rider = $this->db->query("INSERT INTO `oc_urbunpiper_order_status` SET 
	// 				`urbanpiper_order_id` = '".$this->db->escape($order_result['urbanpiper_order_id']) ."', 
	// 				`prev_state` = '".$this->db->escape($order_result['prev_state']) ."', 
	// 				`new_state` = '".$this->db->escape($order_result['new_state'])."',
	// 				`timestamp` = '".$this->db->escape($order_result['timestamp'])."', 
	// 				`date_added`= '".$this->db->escape($order_result['date_added'])."',
	// 				`message` = '".$this->db->escape($order_result['message'])."', 
	// 				`entry_status`= '1'
	// 				 "
	// 			);
	// 			if ($insert_rider == 1) {
	// 				$all_order .= ','.$order_result['urbanpiper_order_id'];
	// 			}
	// 		}
	// 		if($all_order != ''){
	// 			$all_orders = ltrim($all_order,',');
	// 			$url = FETCH_UPDATE_ORDER_STATUS;
	// 			$final_data_update_order = $all_orders;
	// 			$data_json_rider = json_encode($final_data_update_order);
	// 			$ch_order_fetch = curl_init();
	// 			curl_setopt($ch_order_fetch, CURLOPT_URL, $url);
	// 			curl_setopt($ch_order_fetch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json_rider)));
	// 			curl_setopt($ch_order_fetch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 			curl_setopt($ch_order_fetch, CURLOPT_POSTFIELDS,$data_json_rider);
	// 			curl_setopt($ch_order_fetch, CURLOPT_RETURNTRANSFER, true);
	// 			curl_setopt($ch_order_fetch,CURLOPT_SSL_VERIFYPEER, false);
	// 			$response  = curl_exec($ch_order_fetch);
	// 			$resultsz = json_decode($response,true);
	// 			curl_close($ch_order_fetch);
	// 			$this->log->write('Update Order Staus');
	// 			$this->log->write(date('Y-m-d h:i:s'));
	// 		}
	// 	}
	// 	//$this->load->model('catalog/urbanpiper_order');

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function check_transection() {
		
 		$connected = @fsockopen("www.google.com", 80); 
 		$json['unseen_notification'] = 0;
	    if ($connected){
	        $is_conn = true; 
	        date_default_timezone_set("Asia/Kolkata");
			$this->load->language('catalog/order');
			$this->document->setTitle('Online Order');
			//$this->log->write('Start Orders');
			//$this->log->write(date('Y-m-d h:i:s'));
			//$this->log->write('Fetch Order');
			//$this->log->write(date('Y-m-d h:i:s'));

			//$url = FETCH_ONLINE_URBANPIPER_DATAS;
			$url = COUNT_NEW_ORDERS;
			$final_data = array('db_name' => API_DATABASE);
			$data_json = json_encode($final_data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
			$response  = curl_exec($ch);
			$results = json_decode($response,true);
			curl_close($ch);
			
			if (!empty($results)) {
				if(!empty($results['online_orders'])){
					$json['unseen_notification'] = count($results['online_orders']);
				}
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		    fclose($connected);
	    }else{
	        $is_conn = false; 
	        if(!isset($this->request->post['view']) ){
	        	$this->session->data['warning'] = "Please Connect On Internet For Online Orders";
				$json['redirect_warning'] = $this->url->link('catalog/order', 'token=' . $this->session->data['token'],true);
			}
	    }
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}