<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
date_default_timezone_set('Asia/Kolkata');
class ControllerCatalogfoodbarreport extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('catalog/food_bar_report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/food_bar_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_itemname'])){
			$filter_itemname = $this->request->get['filter_itemname'];
		} else {
			$filter_itemname = '';
		}

		if(isset($this->request->get['filter_itemid'])){
			$filter_itemid = $this->request->get['filter_itemid'];
		} else {
			$filter_itemid = '';
		}

		if(isset($this->request->get['store'])){
			$store_id = $this->request->get['store'];
		}
		else{
			$store_id = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}
		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}
		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/food_bar_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_itemname' => $filter_itemname,
			'filter_itemid' => $filter_itemid,
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			'store_id'			=> $store_id
			);
		
		$all_data ='';
		$final_data = array();
		$food_datas = array();
		$liq_datas = array();
		$item_data ='';
		$data['final_data'] = '';
		$stores = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$current_date = date('Y-m-d');

			if($start_date == $current_date){
				$order_info_tbl_name = 'oc_order_info';
				$order_item_tbl_name = 'oc_order_items';
			} else {
				$order_info_tbl_name = 'oc_order_info_report';
				$order_item_tbl_name = 'oc_order_items_report';
			}
		
			$sql ="SELECT * FROM ".$order_info_tbl_name." WHERE 1=1  ";
			
			if (!empty($filter_data['filter_startdate'])) {
				$sql .= " AND (`bill_date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($start_date))) . "'";
			}
			if (!empty($filter_data['filter_enddate'])) {
				$sql .= " AND (`bill_date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($end_date))) . "'";
			}
				
			$sql .= "AND `pay_method` = '1' AND `bill_status` = '1'";
			// $sql .= " AND `liq_cancel` = '0' ";
			$all_food = $this->db->query($sql)->rows;
			//$food_datas[]= array();
			foreach ($all_food as $akey => $avalue) {
				$food_billdatas = $this->db->query("SELECT `billno`,`amt` FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '0' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($food_billdatas as $bkey => $bvalue) {
					$food_datas[]= array(
						'billno' => $bvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ftotal'],
						'discount' => $avalue['ftotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'gst' => $avalue['gst'],
						'gtotal' =>$avalue['ftotal'] - $avalue['ftotalvalue'] + $avalue['gst'],
					);
					//echo'<pre>';print_r($food_billdatas);exit();
				}

				$liq_billdatas = $this->db->query("SELECT `billno`,amt FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '1' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($liq_billdatas as $lkey => $lvalue) {
					$liq_datas[]= array(
						'billno' => $lvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ltotal'],
						'discount' => $avalue['ltotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'vat' => $avalue['vat'],
						'gtotal' => $avalue['ltotal'] - $avalue['ltotalvalue'] + $avalue['vat'],
					);
				}
			}
			$final_data = array(
				'food_datas' => $food_datas,
				'liq_datas' => $liq_datas
			);
		}

		$data['final_data'] = $final_data;
		// echo "<pre>";
		// print_r($final_data);
		// exit();
		$data['stores'] = $stores;
		$data['storecount'] = $this->db->query("SELECT COUNT(*) as total FROM oc_outlet")->row;
		$data['filter_itemname'] = $filter_itemname;
		$data['filter_itemid'] = $filter_itemid;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		$data['store_id'] = $store_id;
		$data['recipestores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
		
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/food_bar_report', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/food_bar_report', $data));
	}
	public function name(){
		$json = array();

		if (isset($this->request->get['filter_itemname'])) {
			$sql = "SELECT * FROM `oc_purchase_order_items` WHERE 1=1 ";
			if(!empty($this->request->get['filter_itemname'])){
				$sql .= " AND `item_name` LIKE '%".$this->request->get['filter_itemname']."%'";
			}
			$sql .=" GROUP BY`item_id` DESC LIMIT 0,10";
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function prints(){
		$this->load->model('catalog/order');
		if(isset($this->request->get['filter_item_id'])){
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = '';
		}

		if(isset($this->request->get['filter_quantity'])){
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = '';
		}

		if(isset($this->request->get['store'])){
			$store = $this->request->get['store'];
		} else {
			$store = '';
		}

		$bomdata = array();

		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}

		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}

		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		$data['cancel'] = $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['print'] = $this->url->link('catalog/itemwisereport/prints', 'token=' . $this->session->data['token'] . $url, true);
		
		$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$current_date = date('Y-m-d');

			if($start_date == $current_date){
				$order_info_tbl_name = 'oc_order_info';
				$order_item_tbl_name = 'oc_order_items';
			} else {
				$order_info_tbl_name = 'oc_order_info_report';
				$order_item_tbl_name = 'oc_order_items_report';
			}
		
			$sql ="SELECT * FROM ".$order_info_tbl_name." WHERE 1=1  ";
			
			if (!empty($filter_data['filter_startdate'])) {
				$sql .= " AND (`bill_date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($start_date))) . "'";
			}
			if (!empty($filter_data['filter_enddate'])) {
				$sql .= " AND (`bill_date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($end_date))) . "'";
			}
				
			$sql .= "AND `pay_method` = '1' AND `bill_status` = '1'";
			// $sql .= " AND `liq_cancel` = '0' ";
			$all_food = $this->db->query($sql)->rows;
			//$food_datas[]= array();
			foreach ($all_food as $akey => $avalue) {
				$food_billdatas = $this->db->query("SELECT `billno`,`amt` FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '0' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($food_billdatas as $bkey => $bvalue) {
					$food_datas[]= array(
						'billno' => $bvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ftotal'],
						'discount' => $avalue['ftotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'gst' => $avalue['gst'],
						'gtotal' =>$avalue['ftotal'] - $avalue['ftotalvalue'] + $avalue['gst'],
					);
					//echo'<pre>';print_r($food_billdatas);exit();
				}

				$liq_billdatas = $this->db->query("SELECT `billno`,amt FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '1' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($liq_billdatas as $lkey => $lvalue) {
					$liq_datas[]= array(
						'billno' => $lvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ltotal'],
						'discount' => $avalue['ltotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'vat' => $avalue['vat'],
						'gtotal' => $avalue['ltotal'] - $avalue['ltotalvalue'] + $avalue['vat'],
					);
				}
			}
			$final_data = array(
				'food_datas' => $food_datas,
				'liq_datas' => $liq_datas
			);
		/*echo'<pre>';
		print_r($final_data);
		exit;*/

		//$data['final_data'] = $final_data;

		try {

// 		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == '1'){
// 		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
// 		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == '2'){
// 		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
// =======
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text("Food And Bar Report");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->text(str_pad("Date :".date('Y-m-d'),30)."Time :".date('H:i:s'));
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("Date",10)."".str_pad("AMT", 6)."".str_pad("Dis", 7)."".str_pad("NET", 6)." ".str_pad("GST", 7)." ".str_pad("TOT", 6)."".str_pad("B.NO", 3)."");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->text("FOOD");
		    $printer->feed(1);

		    $printer->setEmphasis(false);
			$total_discount_normal = 0;
		   	$gtotal = 0;
		   	$gtotal_liq = 0;
			$total_amount_normal = 0;
		    foreach($final_data['food_datas'] as $nkey => $nvalue){
	    	  	$nvalue['date'] = utf8_substr(html_entity_decode($nvalue['date'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$nvalue['amount'] = utf8_substr(html_entity_decode($nvalue['amount'], ENT_QUOTES, 'UTF-8'), 0, 5);
				$nvalue['discount'] = utf8_substr(html_entity_decode($nvalue['discount'], ENT_QUOTES, 'UTF-8'), 0, 6);
				$nvalue['net'] = utf8_substr(html_entity_decode($nvalue['net'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$nvalue['gst'] = utf8_substr(html_entity_decode($nvalue['gst'], ENT_QUOTES, 'UTF-8'), 0, 6);
		    	$nvalue['gtotal'] = utf8_substr(html_entity_decode($nvalue['gtotal'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$nvalue['billno'] = utf8_substr(html_entity_decode($nvalue['billno'], ENT_QUOTES, 'UTF-8'), 0, 3);
		    	
		    	$printer->text("".str_pad($nvalue['date'],10)."".str_pad($nvalue['amount'],6)."".str_pad($nvalue['discount'],7)."".str_pad($nvalue['net'],6)."".str_pad($nvalue['gst'],7)."".str_pad($nvalue['gtotal'],6)."".str_pad($nvalue['billno'],3));
		    	$printer->feed(1);
		    	
		    	$gtotal = $gtotal + $nvalue['gtotal'];
		    	$total_discount_normal = $total_discount_normal + $nvalue['discount'];

	    	}
		    $printer->feed(1);
		    $printer->text("BAR");
		    $printer->feed(1);
	    	foreach($final_data['liq_datas'] as $nkey => $nvalue){
	    	  	$nvalue['date'] = utf8_substr(html_entity_decode($nvalue['date'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$nvalue['amount'] = utf8_substr(html_entity_decode($nvalue['amount'], ENT_QUOTES, 'UTF-8'), 0, 5);
				$nvalue['discount'] = utf8_substr(html_entity_decode($nvalue['discount'], ENT_QUOTES, 'UTF-8'), 0, 6);
				$nvalue['net'] = utf8_substr(html_entity_decode($nvalue['net'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$nvalue['vat'] = utf8_substr(html_entity_decode($nvalue['vat'], ENT_QUOTES, 'UTF-8'), 0, 6);
		    	$nvalue['gtotal'] = utf8_substr(html_entity_decode($nvalue['gtotal'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$nvalue['billno'] = utf8_substr(html_entity_decode($nvalue['billno'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	
		    	$printer->text("".str_pad($nvalue['date'],10)."".str_pad($nvalue['amount'],6)."".str_pad($nvalue['discount'],7)."".str_pad($nvalue['net'],6)."".str_pad($nvalue['vat'],7)."".str_pad($nvalue['gtotal'],6)."".str_pad($nvalue['billno'],3));
		    	$printer->feed(1);
		    	
		    	$total_amount_normal = $total_amount_normal + $nvalue['amount'];
		    	$gtotal_liq = $gtotal_liq + $nvalue['gtotal'];

		    	$total_discount_normal = $total_discount_normal + $nvalue['discount'];

	    	}
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
		  	$printer->feed(1);
		  	$printer->text(str_pad("Total FOOD AMT ",20).$gtotal);
		  	$printer->feed(1);
		  	$printer->text(str_pad("Total BAR AMT ",20).$gtotal_liq);
		  	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(2);
		    $printer->cut();
			$printer->feed(1);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getList();
	}

	public function recipe(){

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		if(isset($this->request->get['filter_item_id'])){
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = '';
		}

		if(isset($this->request->get['filter_quantity'])){
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = '';
		}

		if(isset($this->request->get['store'])){
			$store = $this->request->get['store'];
		} else {
			$store = '';
		}

		$bomdata = array();

		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}

		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}

		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}

		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		$data['cancel'] = $this->url->link('catalog/itemwisereport', 'token=' . $this->session->data['token'] . $url, true);
		$data['print'] = $this->url->link('catalog/itemwisereport/prints', 'token=' . $this->session->data['token'] . $url, true);
		
		$itemDetails = $this->db->query("SELECT `item_name`,`item_id`, `item_code`,`rate_1` FROM oc_item WHERE item_id = '".$filter_item_id."'")->row;

		$bomdetails = $this->db->query("SELECT qty, item_name, unit, id, item_code, unit_id, store_id FROM oc_bom_items WHERE parent_item_code = '".$itemDetails['item_code']."' AND store_id = '".$store."'")->rows;

		$notes_data = $this->db->query("SELECT `notes` FROM `oc_purchase_order_items` WHERE item_id = '".$filter_item_id."'")->row;

		// echo'<pre>';
		// print_r($notes_data);
		// exit;
		$itemdata = array(
			'item_id' 	=> $itemDetails['item_id'],
			'item_name' => $itemDetails['item_name'],
			'item_code' => $itemDetails['item_code'],
			'purchase_price' => $itemDetails['rate_1'],
			'description' => $notes_data['notes'],
			'quantity'	=> $filter_quantity,
		);

		foreach($bomdetails as $key){
			$stock_item_type = $this->db->query("SELECT * FROM oc_stock_item WHERE item_code = '".$key['item_code']."'")->row;
			$this->load->model('catalog/purchaseentry');
			$availableqty = $this->model_catalog_purchaseentry->availableQuantity($key['unit_id'], $key['id'], 0, $key['store_id'], $stock_item_type['item_type'], $key['item_code']);
			$bomdata[] = array(
				'item_name' => $key['item_name'],
				'qty'		=> $key['qty'] * $filter_quantity,
				'unit'		=> $key['unit'],
				'avq'		=> $availableqty
			);
		}

		$data['token'] = $this->session->data['token'];
		$data['itemDetails'] = $itemdata;
		$data['bomdetails'] = $bomdata;

		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/itemwisedetails', $data));
	}

	public function export() {
		//echo "in";exit;
		$data['title'] = 'Food And Bar Day Wise Report'; 

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}
		$this->load->language('catalog/food_bar_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_itemname'])){
			$filter_itemname = $this->request->get['filter_itemname'];
		} else {
			$filter_itemname = '';
		}

		if(isset($this->request->get['filter_itemid'])){
			$filter_itemid = $this->request->get['filter_itemid'];
		} else {
			$filter_itemid = '';
		}

		if(isset($this->request->get['store'])){
			$store_id = $this->request->get['store'];
		}
		else{
			$store_id = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}
		if (isset($this->request->get['store'])) {
			$url .= '&store=' . $this->request->get['store'];
		}

		if (isset($this->request->get['filter_itemname'])) {
			$url .= '&filter_itemname=' . $this->request->get['filter_itemname'];
		}
		if (isset($this->request->get['filter_itemid'])) {
			$url .= '&filter_itemid=' . $this->request->get['filter_itemid'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/food_bar_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		$filter_data = array(
			'filter_itemname' => $filter_itemname,
			'filter_itemid' => $filter_itemid,
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			'store_id'			=> $store_id
			);
		
		$all_data ='';
		$final_data = array();
		$food_datas = array();
		$liq_datas = array();
		$item_data ='';
		$data['final_data'] = '';
		$stores = array();
		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$current_date = date('Y-m-d');

			if($start_date == $current_date){
				$order_info_tbl_name = 'oc_order_info';
				$order_item_tbl_name = 'oc_order_items';
			} else {
				$order_info_tbl_name = 'oc_order_info_report';
				$order_item_tbl_name = 'oc_order_items_report';
			}
		
			$sql ="SELECT * FROM ".$order_info_tbl_name." WHERE 1=1  ";
			
			if (!empty($filter_data['filter_startdate'])) {
				$sql .= " AND (`bill_date`) >= '" . $this->db->escape(date('Y-m-d', strtotime($start_date))) . "'";
			}
			if (!empty($filter_data['filter_enddate'])) {
				$sql .= " AND (`bill_date`) <= '" . $this->db->escape(date('Y-m-d', strtotime($end_date))) . "'";
			}
				
			$sql .= "AND `pay_method` = '1' AND `bill_status` = '1'";
			// $sql .= " AND `liq_cancel` = '0' ";
			$all_food = $this->db->query($sql)->rows;
			//$food_datas[]= array();
			foreach ($all_food as $akey => $avalue) {
				$food_billdatas = $this->db->query("SELECT `billno`,`amt` FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '0' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($food_billdatas as $bkey => $bvalue) {
					$food_datas[]= array(
						'billno' => $bvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ftotal'],
						'discount' => $avalue['ftotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'gst' => $avalue['gst'],
						'gtotal' =>$avalue['ftotal'] - $avalue['ftotalvalue'] + $avalue['gst'],
					);
					//echo'<pre>';print_r($food_billdatas);exit();
				}

				$liq_billdatas = $this->db->query("SELECT `billno`,amt FROM ".$order_item_tbl_name." WHERE `order_id` = '".$avalue['order_id']."' AND `is_liq` = '1' AND `cancel_bill` = '0' AND `cancelstatus` = '0' AND `cancelmodifier` = '0' GROUP BY `billno` ")->rows;
				foreach ($liq_billdatas as $lkey => $lvalue) {
					$liq_datas[]= array(
						'billno' => $lvalue['billno'],
						'date'	=> $avalue['date'],
						'amount'	=> $avalue['ltotal'],
						'discount' => $avalue['ltotalvalue'],
						'net' => $avalue['ftotal_discount'],
						'vat' => $avalue['vat'],
						'gtotal' => $avalue['ltotal'] - $avalue['ltotalvalue'] + $avalue['vat'],
					);
				}
			}
			$final_data = array(
				'food_datas' => $food_datas,
				'liq_datas' => $liq_datas
			);
		}

		$data['final_data'] = $final_data;

		$data['stores'] = $stores;
		$data['storecount'] = $this->db->query("SELECT COUNT(*) as total FROM oc_outlet")->row;
		$data['filter_itemname'] = $filter_itemname;
		$data['filter_itemid'] = $filter_itemid;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		
		$html = $this->load->view('catalog/food_bar_report_html.tpl', $data); // exit;
	 	$filename = "Food And Bar Report.html";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		$this->response->setOutput($this->load->view('catalog/food_bar_report', $data));
	}

}