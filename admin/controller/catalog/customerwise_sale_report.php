<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogCustomerWiseSaleReport extends Controller {

	public function index() {
		$this->load->language('catalog/customerwise_sale_report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/customerwise_sale_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/shiftclose_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';
		$data['showdata'] = array();


		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$data['showdata'] = $this->db->query("SELECT oi.cust_name, oi.cust_contact, oi.grand_total, oi.bill_date, oit.billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.order_id = oit.order_id) WHERE oi.bill_date >= '".$start_date."' AND oi.bill_date <= '".$end_date."' AND cust_contact > '0' GROUP BY oit.`order_id` ")->rows;
			// echo'<pre>';
			// print_r($data['showdata']);
			// exit;
		}

		$data['action'] = $this->url->link('catalog/customerwise_sale_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customerwise_sale_report', $data));
	}
	public function export(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/customerwise_sale_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/shiftclose_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			$data['showdata'] = $this->db->query("SELECT oi.cust_name, oi.cust_contact, oi.grand_total, oi.bill_date, oit.billno FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON(oi.order_id = oit.order_id) WHERE oi.bill_date >= '".$start_date."' AND oi.bill_date <= '".$end_date."' AND cust_contact > '0' GROUP BY oit.`order_id` ")->rows;
				
		}
		$data['action'] = $this->url->link('catalog/shiftclose_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/customerwise_sale_report_html', $data);
		
		$filename = 'customerwise_sale_report';
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;		
		// header('Content-disposition: attachment; filename=' . $filename);
		// header('Content-type: text/html');
		// echo $html;exit;

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}


	public function prints() {
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$billdatas = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' ORDER BY order_no ASC")->rows;
			}
			foreach($dates as $date){
				$showdata[$date] = $this->db->query("SELECT *,SUM(onac) as onacc, SUM(total_payment) as totalbill,SUM(pay_cash) as cash,SUM(pay_card) as card,SUM(pay_online) as online,count(*) as total FROM `oc_order_info_report` WHERE   `complimentary_status` <> '1' AND `cancel_status`<> '1' AND `shiftclose_status`<> '0' AND shift_date = '".$date."'  GROUP BY shift_id")->rows;

				$data['showdata'] = $showdata;
							
			}
			
			$grandtotal = 0;
			$totalcash  = 0;
			$totalcard  = 0;
			$totalonacc  = 0;
			

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('H:i'));
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Shift Close Report");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("Time",6)."".str_pad("Bill",6)."".str_pad("T_SALE",9)."".str_pad("CASH",7)."".str_pad("OnAcc",7)."".str_pad("CARD",5)."".str_pad("USER",8));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($showdata as $key => $value) {
			  		if($value!=array()){
			  			$printer->setJustification(Printer::JUSTIFY_CENTER);
			  			$printer->text("DATE: ".$key);
			  			$printer->feed(1);
			  			$printer->setJustification(Printer::JUSTIFY_LEFT);

					  	foreach ($value as $akey =>$data1) {
					  		$printer->text(str_pad(date('H:i',strtotime($data1['shift_time'])),6)."".str_pad($data1['total'],6)."".str_pad($data1['totalbill'],9)."".str_pad($data1['cash'],7)."".str_pad(round($data1['onacc']),7)."".str_pad($data1['card'] + $data1['online'],5)."".str_pad($data1['shift_username'],8));
					  		$printer->feed(1);
					  		$grandtotal = $grandtotal + $data1['totalbill'];
					  		$totalcash  = $totalcash + $data1['cash'];
					  		$totalonacc = $totalonacc + $data1['onacc'];
					  		$totalcard  = $totalcard + $data1['card'] + $data1['online'];
					  	}
			  		}
				}
			  	$printer->setEmphasis(false);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("Total:",11)."".str_pad($grandtotal,9)."".str_pad($totalcash,9)."".str_pad($totalonacc,9)."".str_pad($totalcard,9));
	  			$printer->feed(2);
	  			$printer->cut();
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}


}
?>