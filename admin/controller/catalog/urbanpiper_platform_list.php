<?php
class Controllercatalogurbanpiperplatformlist extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/urbanpiper_platform_list');

		//$this->document->setTitle($this->language->get('heading_title'));

		//$this->load->model('catalog/urbanpiper_platform_list');
		$this->getList();
	}

	protected function getList() {
	
	


	if (isset($this->request->get['filter_item_name'])) {
			$filter_item_name = $this->request->get['filter_item_name'];
		} else {
			$filter_item_name = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}

		if (isset($this->request->get['filter_item_code'])) {
			$filter_item_code = $this->request->get['filter_item_code'];
		} else {
			$filter_item_code = null;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}

		if (isset($this->request->get['filter_barcode'])) {
			$filter_barcode = $this->request->get['filter_barcode'];
		} else {
			$filter_barcode = null;
		}

		if (isset($this->request->get['filter_item_category'])) {
			$filter_item_category = $this->request->get['filter_item_category'];
		} else {
			$filter_category = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'item_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}


		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' =>  'Live Platform',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' =>  'Live Platform',
			'href' => $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/urbanpiper_platform_list/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['copy'] = $this->url->link('catalog/urbanpiper_platform_list/copy', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/urbanpiper_platform_list/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['back'] = $this->url->link('catalog/urbanpiperitem_show', 'token=' . $this->session->data['token'] . $url, true);
		$data['item_onoff'] = $this->url->link('catalog/urbanpiper_platform_list/item_onoff', 'token=' . $this->session->data['token'] . $url, true);

		$data['products'] = array();

		$filter_data = array(
			'filter_item_name' => $filter_item_name,
			'filter_item_id' => $filter_item_id,
			'filter_item_code' => $filter_item_code,
			'filter_department' => $filter_department,
			'filter_barcode' => $filter_barcode,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$data['zomato_link'] = $this->url->link('catalog/urbanpiper_platform_item', 'token=' . $this->session->data['token'].'&platform=zomato', true);
		$data['swiggy_link'] = $this->url->link('catalog/urbanpiper_platform_item', 'token=' . $this->session->data['token'].'&platform=swiggy', true);

		$this->load->model('tool/image');
		sleep(5);

		//if (isset($this->session->data['store_api_status']) && ($this->session->data['store_api_status'] != '')) {
		$url = Callback_Platform_LIVE;
		$final_data = array('db_name' => API_DATABASE);
		$data_json = json_encode($final_data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		$response  = curl_exec($ch);
		if (curl_errno($ch)) {
	    	$error_msg = curl_error($ch);
		}
		$item_results = json_decode($response,true);
		//echo "<pre>";print_r($item_results);exit;
		if (!empty($item_results)) {
			$this->db->query("DELETE FROM `oc_allwebhook_response` WHERE `api_for` = 'Platform_Api'");
			foreach ($item_results as $plkey => $plvalue) {
				$this->db->query("INSERT INTO `oc_allwebhook_response` SET 
					`api_for` = 'Platform_Api',  
					`array` = '".$this->db->escape(serialize($plvalue['array'])) ."' "
				);
			}
			//unset($this->session->data['item_api_status']);
		} 
		//} 
		
		$base_pproduct_info =  array();
		$allwebhook_response_query = $this->db->query("SELECT DISTINCT `array` FROM " . DB_PREFIX . "allwebhook_response WHERE  `api_for` = 'Platform_Api' ORDER BY id desc");
		if($allwebhook_response_query->num_rows > 0){
			foreach ($allwebhook_response_query->rows as $pkey => $pvalue) {
				if($pvalue['array'] != ''){
					$base_pproduct_info[] = unserialize(unserialize($pvalue['array']));
				}
			}
		} 

		$data['pproduct_info_zomato'] =  array();
		$data['pproduct_info_swiggy'] =  array();
		if(!empty($base_pproduct_info)){
			foreach ($base_pproduct_info as $bkey => $bvalue) {
				if(($bvalue['platform'] == 'zomato')){
					$data['pproduct_info_zomato'][] = $bvalue;
				}
				if(($bvalue['platform'] == 'swiggy')){
					$data['pproduct_info_swiggy'][] = $bvalue;
				}
			}
		}

		//echo "<pre>";print_r($data['pproduct_info']);exit;
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];

			unset($this->session->data['error_warning']);
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, true);
		$data['sort_model'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, true);
		$data['sort_price'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, true);
		$data['sort_quantity'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, true);
		$data['sort_order'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$product_total = 0;

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_item_name'] = $filter_item_name;
		$data['filter_item_id'] = $filter_item_id;
		$data['filter_item_code'] = $filter_item_code;
		
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/urbanpiper_platform_list', $data));
	}

	public function item_onoff() {
		
		$this->load->language('catalog/item');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/item');

		$store_sql = $this->db->query("SELECT * FROM `oc_urbanpiper_store_detail` WHERE 1= 1");
		if($store_sql->num_rows > 0){
			$store_name = $store_sql->row['ref_id'];
		} else {
			$store_name = 'ador12';
		}

  		$body_data = array(
  			// 'merchant_id' =>  MERCHANT_ID
  			'location_ref_id' => $store_name,
  			//'option_ref_ids' =>  array()
  		);
  			
  		if(isset($this->request->get['item_code'])){
  			if($this->request->get['inapp'] == 1){
			//echo '<pre>'; print_r($this->request->get) ;exit;
	  			$item_code = $this->request->get['item_code'];
	  			$item_status = $this->request->get['item_onoff'];

	  			$body_data['item_ref_ids'][] = $item_code;
	  			$body_data['option_ref_ids'] = array();
	  			if($item_status == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}

		  		$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$item_code."' ");
		  		$this->db->query("UPDATE oc_item_urbanpiper SET item_onoff_status = '".$item_onoff_status."' WHERE item_id = '".$item_code."' ");
	  			$sync_data = 1; 
	  		} else {
	  			$sync_data = 0; 
	  		}
	  	} else {
	  		$item_datas = $this->db->query("SELECT `item_id`,`item_onoff_status` FROM oc_item_urbanpiper WHERE 1 = 1 ")->rows;
	  		foreach ($item_datas as $akey => $avalue) {
		  		$body_data['item_ref_ids'][] = $avalue['item_id'];
		  		$body_data['option_ref_ids'] = array();
	  			if($avalue['item_onoff_status'] == 1){
	  				$item_onoff_status = 0;
					$body_data['action'] = 'disable';
	  			} else{
	  				$item_onoff_status = 1;
					$body_data['action'] = 'enable';
	  			}
	  			$this->db->query("UPDATE oc_item SET item_onoff_status = '".$item_onoff_status."' WHERE item_code = '".$avalue['item_id']."' ");
	  			$this->db->query("UPDATE oc_item_urbanpiper SET item_onoff_status = '".$item_onoff_status."' WHERE item_id = '".$avalue['item_id']."' ");
	  		}
	  		$sync_data = 1; 
	  	}
	  
	  	//$body_data['action'] = 'disable';
		//echo '<pre>'; print_r($body_data);exit;
		if($sync_data == 1){
			$body = json_encode($body_data);//echo '<pre>'; print_r($body);exit;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array($curl, array(
			 // CURLOPT_URL => 'https://pos-int.urbanpiper.com/hub/api/v1/items/',
			CURLOPT_URL => ITEM_ON_OFF,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>$body,
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:'.URBANPIPER_API_KEY,
			    'Content-Type: application/json',
			  ),
			));

			$response = curl_exec($curl);
			if (curl_errno($curl)) {
			    $error_msg = curl_error($curl);
			    
			}
			$datas = json_decode($response,true);
			if (isset($error_msg)) {
				
			}
			curl_close($curl);
			$datas = json_decode($response,true);
			//echo $response;exit;
			// echo'<pre>';
			// print_r($value);
			// exit;
			if($datas['status'] == 'success'){
				$this->session->data['success'] = 'Item Status Updated Successfully...!';
			} elseif($datas['status'] == 'error') {
				$this->session->data['errors'] = $response['error'];
			}else if (isset($error_msg)) {
				$this->session->data['errors'] = $error_msg;
			} else {
				$this->session->data['errors'] = $response;
			}
		} else {
			$this->session->data['errors'] = 'Item is not for Sync';

		}

		

		//$this->session->data['success'] = 'Short Name Updated Successfully';

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_item_type'])) {
			$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->redirect($this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . $url, true));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {
			$this->load->model('catalog/urbanpiper_platform_list');

			$data = array(
				'filter_item_name' => $this->request->get['filter_item_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_urbanpiper_platform_list->getItems($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['item_id'],
					'item_name'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecode() {
		$json = array();

		if (isset($this->request->get['filter_item_code'])) {
			$this->load->model('catalog/urbanpiper_platform_list');

			$data = array(
				'filter_item_code' => $this->request->get['filter_item_code'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_urbanpiper_platform_list->getItems($data);
			// echo "<pre>";
			// print_r($results);
			// exit();

			foreach ($results as $result) {
				$json[] = array(
					//'item_id' => $result['item_code'],
					'item_code'        => strip_tags(html_entity_decode($result['item_id'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_code'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_store_nme_lbl'] = $this->language->get('entry_store_nme');
		$data['entry_store_add_lbl'] = $this->language->get('entry_store_add');
		$data['entry_store_zipcode_lbl'] = $this->language->get('entry_store_zipcode');
		$data['entry_store_city_lbl'] = $this->language->get('entry_store_city');
		
		$data['entry_store_contact_phone_lbl'] = $this->language->get('entry_store_contact_phone');
		$data['entry_store_notification_no_lbl'] = $this->language->get('entry_store_notification_no');

		$data['entry_store_notification_email_lbl'] = $this->language->get('entry_store_notification_email_no');
		$data['webhook_url_lbl'] = $this->language->get('webhook_url');
		$data['entry_minimum_pick_time_lbl'] = $this->language->get('entry_minimum_pick_time');
		$data['entry_minimum_delivery_time_lbl'] = $this->language->get('entry_minimum_delivery_time');
		$data['entry_minimum_order_value_lbl'] = $this->language->get('entry_minimum_order_value');
		$data['entry_geo_longitude_lbl'] = $this->language->get('entry_geo_longitude');
		$data['entry_geo_latitude_lbl'] = $this->language->get('entry_geo_latitude');
		$data['entry_webhook_event_type_lbl'] = $this->language->get('entry_webhook_event_type');
		$data['entry_ordering_timing_lbl'] = $this->language->get('entry_ordering_timing');

		$data['entry_ordering_start_timing_lbl'] = $this->language->get('entry_ordering_start_timing');
		$data['entry_ordering_end_timing_lbl'] = $this->language->get('entry_ordering_end_timing');

		$data['entry_packaging_charge_lbl'] = $this->language->get('entry_packaging_charge');

		$data['webhook_event_array'] = array(
			'rider_status_update' => 'Rider status update',
			'order_placed' => 'Order placed',
		    'order_status_update' => 'Order status update',
		    'inventory_update' => 'Inventory update',
		    'store_creation' => 'Store creation',
		    'store_action' => 'Store action',
		    'item_state_toggle' => 'Item state toggle',
		    'catalogue_timing_grp' => 'Catalogue timing grp',
		);

		$data['retries_mode_array'] = array(
			'none' => 'None',
			'seconds' => 'Seconds',
			'minutes' => 'Minutes',
		);



		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_ean'] = $this->language->get('entry_ean');
		$data['entry_jan'] = $this->language->get('entry_jan');
		$data['entry_isbn'] = $this->language->get('entry_isbn');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_shipping'] = $this->language->get('entry_shipping');
		$data['entry_date_available'] = $this->language->get('entry_date_available');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length_class'] = $this->language->get('entry_length_class');
		$data['entry_length'] = $this->language->get('entry_length');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_additional_image'] = $this->language->get('entry_additional_image');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_recurring'] = $this->language->get('entry_recurring');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_sku'] = $this->language->get('help_sku');
		$data['help_upc'] = $this->language->get('help_upc');
		$data['help_ean'] = $this->language->get('help_ean');
		$data['help_jan'] = $this->language->get('help_jan');
		$data['help_isbn'] = $this->language->get('help_isbn');
		$data['help_mpn'] = $this->language->get('help_mpn');
		$data['help_minimum'] = $this->language->get('help_minimum');
		$data['help_manufacturer'] = $this->language->get('help_manufacturer');
		$data['help_stock_status'] = $this->language->get('help_stock_status');
		$data['help_points'] = $this->language->get('help_points');
		$data['help_category'] = $this->language->get('help_category');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_download'] = $this->language->get('help_download');
		$data['help_related'] = $this->language->get('help_related');
		$data['help_tag'] = $this->language->get('help_tag');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
		$data['button_option_add'] = $this->language->get('button_option_add');
		$data['button_option_value_add'] = $this->language->get('button_option_value_add');
		$data['button_discount_add'] = $this->language->get('button_discount_add');
		$data['button_special_add'] = $this->language->get('button_special_add');
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_recurring_add'] = $this->language->get('button_recurring_add');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_recurring'] = $this->language->get('tab_recurring');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['tab_openbay'] = $this->language->get('tab_openbay');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['error_webhook_url'])) {
			$data['error_webhook_url'] = $this->error['error_webhook_url'];
		} else {
			$data['error_webhook_url'] = '';
		}

		if (isset($this->error['error_webhook_event'])) {
			$data['error_webhook_event'] = $this->error['error_webhook_event'];
		} else {
			$data['error_webhook_event'] = '';
		}

		if (isset($this->error['error_retries_mode'])) {
			$data['error_retries_mode'] = $this->error['error_retries_mode'];
		} else {
			$data['error_retries_mode'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' =>'Live Platform',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Live Platform',
			'href' => $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['webhook_id'])) {
			$data['action'] = $this->url->link('catalog/urbanpiper_platform_list/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/urbanpiper_platform_list/edit', 'token=' . $this->session->data['token'] . '&webhook_id=' . $this->request->get['webhook_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/urbanpiper_platform_list', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['webhook_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->model_catalog_urbanpiper_platform_list->getProduct($this->request->get['webhook_id']);
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		

		if (isset($this->request->post['webhook_url'])) {
			$data['webhook_url'] = $this->request->post['webhook_url'];
		} elseif (!empty($product_info)) {
			$data['webhook_url'] = $product_info['url'];
		} else {
			$data['webhook_url'] = '';
		}

		if (isset($this->request->post['webhook_event'])) {
			$data['webhook_event'] = $this->request->post['webhook_event'];
		} elseif (!empty($product_info)) {
			$data['webhook_event'] = $product_info['event_type'];
		} else {
			$data['webhook_event'] = '';
		}

		if (isset($this->request->post['retries_mode'])) {
			$data['retries_mode'] = $this->request->post['retries_mode'];
		} elseif (!empty($product_info)) {
			$data['retries_mode'] = $product_info['retrial_interval_units'];
		} else {
			$data['retries_mode'] = '';
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/urbanpiper_platform_list_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_platform_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (($this->request->post['webhook_url'] == ' ')) {
			$this->error['error_webhook_url'] = 'Please Add Webhook Url';
		}

		if (($this->request->post['webhook_event'] == ' ')) {
			$this->error['error_webhook_event'] = 'Please Add Webhook Event';
		}

		if (($this->request->post['retries_mode'] == ' ')) {
			$this->error['error_retries_mode'] = 'Please Select Retries Mode';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_platform_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/urbanpiper_platform_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	
}
