<?php
class ControllerCatalogWaiter extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/waiter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/waiter');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/waiter');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/waiter');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_waiter->addWaiter($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/waiter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/waiter');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_waiter->editWaiter($this->request->get['waiter_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_waiter_id'])) {
				$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/waiter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/waiter');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $waiter_id) {
				$this->model_catalog_waiter->deleteWaiter($waiter_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_waiter_id'])) {
				$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_waiter_id'])) {
			$filter_waiter_id = $this->request->get['filter_waiter_id'];
		} else {
			$filter_waiter_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_waiter_id'])) {
			$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/waiter/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/waiter/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_waiter_id' => $filter_waiter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_waiter->getTotalWaiter();

		$results = $this->model_catalog_waiter->getWaiters($filter_data);
		// echo '<pre>';
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$data['waiters'][] = array(
				'waiter_id' => $result['waiter_id'],
				'name'        => $result['name'],
				'code'        => $result['code'],
				'roll'        => $result['roll'],
				'edit'        => $this->url->link('catalog/waiter/edit', 'token=' . $this->session->data['token'] . '&waiter_id=' . $result['waiter_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_waiter_id'])) {
			$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
		}

		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        $data['sort_name'] = $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_code'] = $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . '&sort=code' . $url, true);
        $data['sort_roll'] = $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . '&sort=roll' . $url, true);
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_waiter_id'])) {
			$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
		}

		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_waiter_id'] = $filter_waiter_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/waiter_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['waiter_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = array();
		}

		if (isset($this->error['department_id'])) {
			$data['error_department_id'] = $this->error['department_id'];
		} else {
			$data['error_department_id'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_waiter_id'])) {
			$url .= '&filter_waiter_id=' . $this->request->get['filter_waiter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['waiter_id'])) {
			$data['action'] = $this->url->link('catalog/waiter/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/waiter/edit', 'token=' . $this->session->data['token'] . '&waiter_id=' . $this->request->get['waiter_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/waiter', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['waiter_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$waiter_info = $this->model_catalog_waiter->getWaiter($this->request->get['waiter_id']);
		}

		$data['token'] = $this->session->data['token'];

		$i_code = $this->db->query(" SELECT code FROM oc_waiter ORDER BY code DESC limit 0,1 ");
		 if ($i_code->num_rows > 0) {
		 	$new_code = $i_code->row['code'] + 1;
		 }else { 
		 	$new_code = 1; 
		 }

		if (isset($this->request->get['code'])) {
			$data['code'] = $this->request->get['code'];
		} elseif (!empty($waiter_info)) {
			$data['code'] = $waiter_info['code'];
		} else {
			$data['code'] = $new_code;
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($waiter_info)) {
			$data['name'] = $waiter_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($waiter_info)) {
			$data['address'] = $waiter_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['refer'])) {
			$data['refer'] = $this->request->post['refer'];
		} elseif (!empty($waiter_info)) {
			$data['refer'] = $waiter_info['refer'];
		} else {
			$data['refer'] = '';
		}

		if (isset($this->request->post['place'])) {
			$data['place'] = $this->request->post['place'];
		} elseif (!empty($waiter_info)) {
			$data['place'] = $waiter_info['place'];
		} else {
			$data['place'] = '';
		}

		if (isset($this->request->post['pin'])) {
			$data['pin'] = $this->request->post['pin'];
		} elseif (!empty($waiter_info)) {
			$data['pin'] = $waiter_info['pin'];
		} else {
			$data['pin'] = '';
		}


		if (isset($this->request->post['contact'])) {
			$data['contact'] = $this->request->post['contact'];
		} elseif (!empty($waiter_info)) {
			$data['contact'] = $waiter_info['contact'];
		} else {
			$data['contact'] = '';
		}

		if (isset($this->request->post['doj'])) {
			$data['doj'] = date('Y-d-m',strtotime($this->request->post['doj']));
		} elseif (!empty($waiter_info)) {
			$data['doj'] = date('Y-d-m',strtotime($waiter_info['doj']));
		} else {
			$data['doj'] = '';
		}

		if (isset($this->request->post['commission'])) {
			$data['commission'] = $this->request->post['commission'];
		} elseif (!empty($waiter_info)) {
			$data['commission'] = $waiter_info['commission'];
		} else {
			$data['commission'] = '';
		}

		if (isset($this->request->post['activate'])) {
			$data['activate'] = $this->request->post['activate'];
		} elseif (!empty($waiter_info)) {
			$data['activate'] = $waiter_info['activate'];
		} else {
			$data['activate'] = '';
		}

		$data['rolls'] =array('Steward' => 'Steward' ,
		'Captain' => 'Captain' ,
		'Waiter' => 'Waiter',
		'Delivery Boy' => 'Delivery Boy');

		if (isset($this->request->post['roll'])) {
			$data['roll'] = $this->request->post['roll'];
		} elseif (!empty($waiter_info)) {
			$data['roll'] = $waiter_info['roll'];
		} else {
			$data['roll'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/waiter_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/waiter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Enter valid Name';
		} else {
			if(isset($this->request->get['waiter_id'])){
				$is_exist = $this->db->query("SELECT `waiter_id` FROM `oc_waiter` WHERE `code` = '".$datas['code']."' AND `waiter_id` <> '".$this->request->get['waiter_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['code'] = 'Code Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `waiter_id` FROM `oc_waiter` WHERE `code` = '".$datas['code']."' ");
				if($is_exist->num_rows > 0){
					$this->error['code'] = 'Code Already Exists';
				}
			}
		}

		// if ($this->request->post['department_id'] == '') {
		// 	$this->error['department_id'] = 'Please Enter department ID';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/waiter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		return !$this->error;
	}

	

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/waiter');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_waiter->getWaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}