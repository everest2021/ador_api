<!DOCTYPE html>
	<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
		<head>
			<meta charset="UTF-8" />
			<!-- <title><?php echo $heading_title; ?></title> -->
		</head>
		<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
			<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
  
			  <!-- <div class="col-sm-6 col-sm-offset-3">
						<center>
						  <?php echo HOTEL_NAME ?>
						  <?php echo HOTEL_ADD ?>
						</center> -->

				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Current Stock LBS Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<?php $total = 0; ?>
								<tr>
									<th style="text-align: center;">Brand Name</th>
									<?php foreach($brandsizezz as $key => $value) {  ?>
										<th style="text-align: center;"><?php echo $value['brand_size'] ?></th>
									<?php } ?>
									<th style="text-align: center;">Loose</th>
								</tr>
								<?php foreach($finaldatas as $key => $value) { //echo'<pre>';print_r($value);exit; ?>
									<tr>
										<td><?php echo $key ?></td>
										<?php foreach ($value as $skey => $svalue) { ?>
											<td><?php echo $svalue['qty'] ?></td>
										<?php } ?>
										<?php foreach ($loosezz as $lkey => $lvalue) { ?>
											<?php if ($key == $lkey) { ?>
												<td><?php echo $lvalue ?></td>
											<?php } ?>

										<?php } ?>
									</tr>
								<?php } ?>
				</table>
			<?php echo $footer; ?>
   		</div>
	</body>
</html>
