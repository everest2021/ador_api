<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class ControllerCatalogpurchaseorderreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/purchaseorderreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/purchaseorderreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/purchaseorderreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['storename'] = $this->request->post['filter_storename'];
		}
		else{
			$data['storename'] = '';
		}

		if(isset($this->request->post['filter_supplier'])){
			$data['supplier'] = $this->request->post['filter_supplier'];
		}
		else{
			$data['supplier'] = '';
		}

		if(isset($this->request->post['filter_type'])){
			$data['type'] = $this->request->post['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		$orderdatas  = array();
		$data['orderdatas'] = array();

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) && isset($this->request->post['filter_storename']) && isset($this->request->post['filter_supplier'])){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->post['filter_storename'];
			$suppliercode = $this->request->post['filter_supplier'];

			$sql = "SELECT * FROM oc_purchase_transaction WHERE `invoice_date` >= '".$start_date."' AND `invoice_date`<= '".$end_date."' AND  category = '".$this->request->post['filter_type']."'";

			if($this->request->post['filter_storename'] != ''){
				$sql .= " AND store_id = '".$storecode."'";
			}

			if($this->request->post['filter_supplier'] != ''){
				$sql .= " AND supplier_id = '".$suppliercode."'";
			}

			$orders = $this->db->query($sql)->rows;
			foreach ($orders as $order) {
				$all_order_datas = $this->db->query("SELECT description, SUM(amount) as amount, SUM(qty) as qty, unit, unit_id, description_id ,rate FROM oc_purchase_items_transaction WHERE p_id = '".$order['id']."' GROUP BY description_id, unit_id")->rows;
				foreach($all_order_datas as $key){
					$orderdatas[] = array(
										'store_name'	=> $order['store_name'],
										'invoice_date'	=> $order['invoice_date'],
										'supplier_name'	=> $order['supplier_name'],
										'invoice_no'	=> $order['invoice_no'],
										'description' 	=> $key['description'],
										'qty'         	=> $key['qty'],
										'unit'  		=> $key['unit'],
										'rate'			=> $key['rate'],
										'amount'      	=> $key['amount']
									);
				}
			}
		}

		$data['orderdatas'] = $orderdatas;
		if(isset($this->request->post['filter_type']) && $this->request->post['filter_type'] != ''){
			$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = '".$this->request->post['filter_type']."'")->rows;
		} else{
			$data['storenames'] = '';
		}
		$data['types'] = array(
							''		 => 'Please Select',
							'Food'   => 'Food',
							'Liquor' => 'Liquor'
						 );
		$supplier_data = $this->db->query("SELECT * FROM oc_supplier")->rows;
		foreach ($supplier_data as $skey => $svalue) {
			$final_data[] = array(
				'supplier_id' => $svalue['supplier_id'],
				'supplier' => $svalue['supplier']
				);
		}
		$data['suppliers'] = $final_data;
		// echo'<pre>';
		// print_r($data);
		// exit;


		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/purchaseorderreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/purchaseorderreport', $data));
	}
	public function export() {
		$this->load->language('catalog/purchaseorderreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/purchaseorderreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_storename'])){
			$data['storename'] = $this->request->get['filter_storename'];
		}
		else{
			$data['storename'] = '';
		}

		if(isset($this->request->get['filter_supplier'])){
			$data['supplier'] = $this->request->get['filter_supplier'];
			$filter_supplier= $this->request->get['filter_supplier'];
		}
		else{
			$data['supplier'] = '';
			$filter_supplier='';
		}

		if(isset($this->request->get['filter_type'])){
			$data['type'] = $this->request->get['filter_type'];
			$filter_storename=$this->request->get['filter_type'];
		}
		else{
			$data['type'] = '';
			$filter_storename='';
		}

		$orderdatas  = array();
		$data['orderdatas'] = array();
		// if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) && isset($this->request->post['filter_type'])){
		// 	echo 'in';exit;
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $filter_storename;
			$suppliercode = $filter_supplier;

			$sql = "SELECT * FROM oc_purchase_transaction WHERE `invoice_date` >= '".$start_date."' AND `invoice_date`<= '".$end_date."' AND  category = '".$this->request->get['filter_type']."'";

			if(isset($this->request->get['filter_storename'])){
				$sql .= " AND store_id = '".$storecode."'";
			}

			if(isset($this->request->get['filter_supplier'])){
				$sql .= " AND supplier_id = '".$suppliercode."'";
			}
			$orders = $this->db->query($sql)->rows;

			foreach ($orders as $order) {
				$all_order_datas = $this->db->query("SELECT description, SUM(amount) as amount, SUM(qty) as qty, unit, unit_id, description_id ,rate FROM oc_purchase_items_transaction WHERE p_id = '".$order['id']."' GROUP BY description_id, unit_id")->rows;
				foreach($all_order_datas as $key){
					$orderdatas[] = array(
										'store_name'	=> $order['store_name'],
										'invoice_date'	=> $order['invoice_date'],
										'supplier_name'	=> $order['supplier_name'],
										'invoice_no'	=> $order['invoice_no'],
										'description' 	=> $key['description'],
										'qty'         	=> $key['qty'],
										'unit'  		=> $key['unit'],
										'rate'			=> $key['rate'],
										'amount'      	=> $key['amount']
									);
				}
			}
		//}

		$data['orderdatas'] = $orderdatas;
		if(isset($this->request->get['filter_type']) && $this->request->get['filter_type'] != ''){
			$data['storenames'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = '".$this->request->get['filter_type']."'")->rows;
		} else{
			$data['storenames'] = '';
		}
		$data['types'] = array(
							''		=> 'Please Select',
							'Food'   => 'Food',
							'Liquor' => 'Liquor'
						 );
		$supplier_data = $this->db->query("SELECT * FROM oc_supplier")->rows;
		foreach ($supplier_data as $skey => $svalue) {
			$final_data[] = array(
				'supplier_id' => $svalue['supplier_id'],
				'supplier' => $svalue['supplier']
				);
		}
		$data['suppliers'] = $final_data;
		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/purchaseorderreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['base'] = HTTPS_SERVER;
		$html = $this->load->view('catalog/purchaseorderreport_html', $data);
		$filename = 'purchaseorderreport.xls';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;

		/*$html = $template->render('catalog/purchaseorderreport_html.tpl');
			//echo $html;exit;
			$filename = "purchaseorderreport";
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;*/
	}
}