<?php
class ControllerCatalogOnlinePayment extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/onlinepayment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/onlinepayment');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/onlinepayment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/onlinepayment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/
			$this->model_catalog_onlinepayment->addonlinepayment($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/onlinepayment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/onlinepayment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_onlinepayment->editonlinepayment($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			// if (isset($this->request->get['filter_department_id'])) {
			// 	$url .= '&filter_department_id=' . $this->request->get['filter_department_id'];
			// }
			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		//echo'innn';exit;
		$this->load->language('catalog/onlinepayment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/onlinepayment');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_onlinepayment->deleteonlinepayment($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
			 if (isset($this->request->get['id'])) {
			 	$url .= '&id=' . $this->request->get['id'];
			 }

			/*if (isset($this->request->get['filter_id'])) {
				$url .= '&filter_id=' . $this->request->get['filter_id'];
			}
*/
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();

	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		// if (isset($this->request->get['filter_department_id'])) {

		// 	$filter_department_id = $this->request->get['filter_department_id'];

		// } else {

		// 	$filter_department_id = null;

		// }

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';

		if (isset($this->request->get['id'])) {
			$url .= '&id=' . $this->request->get['id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/onlinepayment/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/onlinepayment/delete', 'token=' . $this->session->data['token'] . $url, true);

		$filter_data = array(
			'filter_name' => $filter_name,
			//'filter_department_id' => $filter_department_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$onlinepayment_total = $this->model_catalog_onlinepayment->getTotalonlinepayment();

		$results = $this->model_catalog_onlinepayment->getonlinepayments($filter_data);
		foreach ($results as $result) {
			$data['onlinepayment'][] = array(
				'id'	=>$result['id'],
				'payment_type'   => $result['payment_type'],
				'edit'        => $this->url->link('catalog/onlinepayment/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}
		// echo'<pre>';
		// print_r($data['onlinepayment']);
		// exit;
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_capacity'] = $this->language->get('column_capacity');
		//$data['column_rate'] = $this->language->get('column_rate');
		$data['column_action'] = $this->language->get('column_action');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {

			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

			/*if (isset($this->request->post['onlinepayment'])) {
			$data['onlinepayment'] = (array)$this->request->post['onlinepayment'];
		} else {
			$data['onlinepayment'] = '';
		}*/
		/*echo "<pre>";print_r($data['onlinepayment']);exit();*/

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];

		}


		$pagination = new Pagination();
		$pagination->total = $onlinepayment_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($onlinepayment_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($onlinepayment_total - $this->config->get('config_limit_admin'))) ? $onlinepayment_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $onlinepayment_total, ceil($onlinepayment_total / $this->config->get('config_limit_admin')));
		$data['filter_name'] = $filter_name;

		// $data['filter_department_id'] = $filter_department_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/onlinepayment_list', $data));
	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_rate'] = $this->language->get('entry_rate');
		$data['entry_capacity'] = $this->language->get('entry_capacity');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');
		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['payment_type'])) {
			$data['error_payment_type'] = $this->error['payment_type'];
		} else {
			$data['error_payment_type'] = array();
		}

		if (isset($this->error['rate'])) {
			$data['error_rate'] = $this->error['rate'];
		} else {
			$data['error_rate'] = array();
		}

		if (isset($this->error['capacity'])) {
			$data['error_capacity'] = $this->error['capacity'];
		} else {
			$data['error_capacity'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];

		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/onlinepayment', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/onlinepayment/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/onlinepayment/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/onlinepayment', 'token=' . $this->session->data['token'] . $url, true);
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$onlinepayment_info = $this->model_catalog_onlinepayment->getonlinepayment($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];
		//echo "<pre>"; print_r($onlinepayment_info);exit;


		// if (isset($this->request->post['payment_type'])) {
		// 	$data['payment_type'] = $this->request->post['payment_type'];
		// } elseif (!empty($onlinepayment_info)) {
		// 	$data['payment_type'] = $onlinepayment_info['payment_type'];
		// } else {
		// 	$data['payment_type'] = '';
		// }
             //echo "<pre>";print_r($onlinepayment_info);exit();
		if (isset($this->request->post['payment_type'])) {
			$data['payment_type'] = $this->request->post['payment_type'];
		} elseif (!empty($onlinepayment_info)) {
			$data['payment_type'] = $onlinepayment_info['payment_type'];
		} else {
			$data['payment_type'] = '';
		}
		/*echo $data['payment_type'];exit;*/
		/*if (isset($this->request->post['total_amount'])) {
			$data['total_amount'] = $this->request->post['total_amount'];
		} elseif (!empty($onlinepayment_info)) {
			$data['total_amount'] = $onlinepayment_info['total_amount'];
		} else {
			$data['total_amount'] = '';
		} 
*/


		/*if (isset($this->request->post['rate_child'])) {
			$data['rate_child'] = $this->request->post['rate_child'];
		} elseif (!empty($onlinepayment_info)) {
			$data['rate_child'] = $onlinepayment_info['rate_child'];
		} else {
			$data['rate_child'] = '';
		}

		if (isset($this->request->post['capacity'])) {
			$data['capacity'] = $this->request->post['capacity'];
		} elseif (!empty($onlinepayment_info)) {
			$data['capacity'] = $onlinepayment_info['capacity'];
		} else {
			$data['capacity'] = '';
		}
*/


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/onlinepayment_form', $data));
	}


	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/onlinepayment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['payment_type']) < 2) || (utf8_strlen($this->request->post['payment_type']) > 255)) {
			$this->error['payment_type'] = 'Please Enter  Name';
		}
		/*if ((utf8_strlen($this->request->post['rate']) < 2) || (utf8_strlen($this->request->post['rate']) > 255)) {
			$this->error['rate'] = 'Please Enter  rate';
		}*/
		/*if ((utf8_strlen($this->request->post['capacity']) < 2) || (utf8_strlen($this->request->post['capacity']) > 255)) {
			$this->error['capacity'] = 'Please Enter  capacity';
		}*/

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/onlinepayment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$this->load->model('catalog/onlinepayment');
		return !$this->error;
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/onlinepayment');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				/*'id' => $this->request->get['id'],*/
				'start'       => 0,
				'limit'       => 20

			);
			$results = $this->model_catalog_onlinepayment->getonlinepayments($filter_data);
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'payment_type' => strip_tags(html_entity_decode($result['payment_type'], ENT_QUOTES, 'UTF-8'))); /*echo "<pre>";print_r($json);exit;*/
			}
			 /*echo "<pre>";print_r($json);exit; */
		}
             /*echo "<pre>";print_r($json);exit;*/
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['payment_type'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

}