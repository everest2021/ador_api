<?php
class ControllerCatalogNotification extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/notification');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/notification');
		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/notification');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/notification');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_notification->addTeacher($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/notification');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/notification');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_notification->editTeacher($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/notification');
		$this->load->model('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/notification');

		if (isset($this->request->post['selected'])){
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_notification->deleteTeacher($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_title'])) {
			$filter_title = $this->request->get['filter_title'];
		} else {
			$filter_title = null;
		}

		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . urlencode(html_entity_decode($this->request->get['filter_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/notification/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/notification/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['teachers'] = array();

		$filter_data = array(
			'filter_title'	  => $filter_title,
			'filter_id'  => $filter_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$teacher_total = $this->model_catalog_notification->getTotalTeachers($filter_data);

		$results = $this->model_catalog_notification->getTeachers($filter_data);

		foreach ($results as $result) {
			$data['teachers'][] = array(
				'id' => $result['id'],
				'title'     => $result['title'],
				'date'     => $result['date'],
				'time'     => $result['time'],
//				'teacher_code' => $result['teacher_code'],
				'edit'     => $this->url->link('catalog/notification/edit', 'token=' . $this->session->data['token'] . '&tem_id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . urlencode(html_entity_decode($this->request->get['filter_id'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . '&sort=title' . $url, true);
		$data['sort_date'] = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . '&sort=date' . $url, true);
		$data['sort_time'] = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . '&sort=time' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . urlencode(html_entity_decode($this->request->get['filter_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $teacher_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($teacher_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($teacher_total - $this->config->get('config_limit_admin'))) ? $teacher_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $teacher_total, ceil($teacher_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['filter_title'] = $filter_title;
		$data['filter_id'] = $filter_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['token'] = $this->session->data['token'];

		$this->response->setOutput($this->load->view('catalog/notification_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_teacher'])) {
			$url .= '&filter_teacher=' . urlencode(html_entity_decode($this->request->get['filter_teacher'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . urlencode(html_entity_decode($this->request->get['filter_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/teacher', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/notification/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/notification/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/notification', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$teacher_info = $this->model_catalog_notification->getTeacher($this->request->get['id']);
		}

		$data['token'] = $this->session->data['token'];

		

		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (!empty($teacher_info)) {
			$data['description'] = $teacher_info['description'];
		} else {
			$data['description'] = '';
		}

		if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (!empty($teacher_info)) {
			$data['title'] = $teacher_info['title'];
		} else {
			$data['title'] = '';
		}

		if (isset($this->request->post['date'])) {
			$data['date'] = $this->request->post['date'];
		} elseif (!empty($teacher_info)) {
			$data['date'] = $teacher_info['date'];
		} else {
			$data['date'] = '';
		}

		if (isset($this->request->post['time'])) {
			$data['time'] = $this->request->post['time'];
		} elseif (!empty($teacher_info)) {
			$data['time'] = $teacher_info['time'];
		} else {
			$data['time'] = '';
		}

		$data['statuss']  = array('1' => 'Active', 
									'0' => 'Inactive',);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/notification_form', $data));
	}

	protected function validateForm() {
		// if (!$this->user->hasPermission('modify', 'catalog/notification')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }

		if ((utf8_strlen($this->request->post['title']) < 1) || (utf8_strlen($this->request->post['title']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		// if ((utf8_strlen($this->request->post['teacher_code']) < 1) || (utf8_strlen($this->request->post['teacher_code']) > 64)) {
		// 	$this->error['teacher_code'] = 'Please Enter Color Code!';
		// }

		// if($this->request->post['category_id'] == '0' || $this->request->post['category_id'] == ''){
		// 	$this->error['category'] = 'Please Select Valid Category!';
		// }

		return !$this->error;
	}

//	protected function validateDelete() {
//		if (!$this->user->hasPermission('modify', 'catalog/teacher')) {
//			$this->error['warning'] = $this->language->get('error_permission');
//		}

//		$this->load->model('catalog/product');

//		foreach ($this->request->post['selected'] as $teacher_id) {
//			$product_total = $this->model_catalog_product->getTotalProductsByColorId($teacher_id);

//			if ($product_total) {
//				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
//			}
//		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

//		return !$this->error;
//	}
	public function autocompletebname() {
		$json = array();
		if (isset($this->request->get['filter_title'])) {
			$this->load->model('catalog/notification');
			$data = array(
				'filter_title' => $this->request->get['filter_title'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_notification->getTeachers($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'filter_title'    => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['filter_title'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	// public function autocomplete() {
	// 	$json = array();

	// 	if (isset($this->request->get['filter_tem_name'])) {
	// 		$this->load->model('catalog/notification');
	// 		echo $this->request->get['filter_tem_name'];
	// 		$filter_data = array(
	// 			'filter_tem_name' => $this->request->get['filter_tem_name'],
	// 			'start'       => 0,
	// 			'limit'       => 5
	// 		);

	// 		$results = $this->model_catalog_notification->getTeachers($filter_data);
	// 		// echo "<pre>";
	// 		// print_r($results);
	// 		// exit;
	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'tem_id' => $result['tem_id'],
	// 				'tem_name'     => strip_tags(html_entity_decode($result['tem_name'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['tem_name'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);

	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
}