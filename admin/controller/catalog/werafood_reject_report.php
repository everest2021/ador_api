<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
require_once(DIR_SYSTEM.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');
class ControllerCatalogWerafoodRejectReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle('Online Order Reject Report');
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle("Online Reject Report" );

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "Online Food Reject Report",
			'href' => $this->url->link('catalog/werafood_reject_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}
			
		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_type'])){
			$data['type'] = $this->request->post['filter_type'];
		}
		else{
			$data['type'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';

		$data['types'] = array(
				'Online' => 'Online',
				'Zomato' => 'Zomato',
				'Swiggy' => 'Swiggy',
				'Uber'	=>'Uber',
		);

		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'] ) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$type =  $this->request->post['filter_type'];

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$sql = "SELECT * FROM `oc_werafood_orders` WHERE 1=1 ";
					if($type != ''){
						$sql .= "AND `order_from` = '".$type."' ";
					}

					$sql .= "AND `date` = '".$date."' AND status = '3' ";
				$billdata[$date] = $this->db->query($sql)->rows;
			}

				
		}
		//echo "<pre>";print_r($billdata);exit;
		$data['billdatas'] = $billdata;
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('catalog/werafood_reject_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Reject Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/werafood_reject_report', $data));
	}

	public function prints() {
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$billdatas = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$cancelamount = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$advancetotal = $advance->row['advancetotal'];
			} else{
				$advancetotal = 0;
			}
				

			$discount = 0;
			$grandtotal = 0;
			$vat = 0; 
			$gst = 0; 
			$discountvalue = 0; 
			$afterdiscount = 0; 
			$stax = 0; 
			$roundoff = 0; 
			$total = 0; 
			$advance = 0;
			$foodtotal = 0;
			$liqtotal = 0;
			$fdistotal = 0;
			$ldistotal = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Bill Wise Sales Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			  	$printer->text(str_pad("Ref No",7)."".str_pad("Food",7)."".str_pad("Bar",7)."".str_pad("Disc",7).""
				  		.str_pad("Total",7)."".str_pad("Pay By",8)."T No");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($billdata as $key => $value){
			  		$printer->setJustification(Printer::JUSTIFY_CENTER);
			  		$printer->text($key);
			  		$printer->feed(1);
			  		$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	foreach ($value as $data) {
				  		if($data['food_cancel'] == 1) {
				  			$data['ftotal'] = 0;
				  		}
				  		if($data['liq_cancel'] == 1) {
				  			$data['ltotal'] = 0;
				  		}
				  		if($data['pay_cash'] != '0') {
				  			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Card",8).$data['t_name']);
					  	}else if($data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("OnAc",8).$data['t_name']);
					  	}else if($data['mealpass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Meal Pass",8).$data['t_name']);
					  	}else if($data['pass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pass",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash+Card",8).$data['t_name']);
					  	}else{
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  			$foodtotal += round($data['ftotal']);
					  			$liqtotal  += round($data['ltotal']);
					  			$fdistotal += round($data['ftotalvalue']);
					  			$ldistotal += round($data['ltotalvalue']);
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pending",8).$data['t_name']);
					  	}
				  		$printer->feed(1);
				  		  if($data['liq_cancel'] == 1){
				  		  	$vat = 0;
				  		  } else{
				  		  	$vat = $vat + $data['vat'];
				  		  }
				  		  if($data['food_cancel'] == 1){
				  		  	$gst = 0;
				  		  } else{
				  		  	$gst = $gst + $data['gst'];
				  		  }
				  		  $stax = $stax + $data['stax'];
				  		  if($data['food_cancel'] == 1){
				  		  	$data['ftotalvalue'] = 0; 
				  		  } 
				  		  if($data['liq_cancel'] == 1){
				  		  	$data['ltotalvalue'] = 0;
				  		  }
				  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
				  		  if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
				  		  	$total = $grandtotal + $stax;
				  		  } else{
			  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
			  		  	 	$total = $grandtotal + $gst + $vat + $stax; 
				  		  }
				  		  $discountvalue = $discountvalue + $discount;
				  		  $afterdiscount = $total - $discountvalue;
				  		  $roundoff = $roundoff + $data['roundtotal'];
				  		  $advance = $advance + $data['advance_amount'];
				  	}
				}
			  	$printer->setEmphasis(false);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Bill Total :",20)."".$grandtotal);
			  	$printer->setEmphasis(false);
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("O- Chrg Amt (+) :",20)."0");
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("Vat Amt (+) :",20)."".$vat);
			  	$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("S- Chrg Amt (+) :",20)."".$stax);
	  			$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("GST Amt (+) :",20)."".$gst);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("SBC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("R-Off Amt (+) :",20)."".$roundoff/2);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Total :",20)."".($total + $roundoff));
			  	$printer->feed(1);
			  	$printer->setEmphasis(false);
			  	$printer->text(str_pad("",20)."".str_pad("Discount Amt (-) :",20)."".$discountvalue);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Cancel Bill Amt (-) :",20).$cancelamount);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Comp. Amt (-) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance. Amt (-) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Net Amount :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance Amt (+) :",20).$advancetotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Grand Total (+) :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Food Total (+) :",20)."".$foodtotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Total Food Discount (+) :",20)."". $fdistotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",10)."".str_pad("Liquar Total (+) :",20)."".$liqtotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->text(str_pad("",10)."".str_pad("Total Liquar Discount (+) :",20)."". $ldistotal);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}

	public function export(){
		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle('Online Order Reject Report');

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Online Order Reject Report',
			'href' => $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = 0 ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
				
		}
		//echo "<pre>";print_r($billdata);exit;
		$data['billdatas'] = $billdata;
		$data['action'] = $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Reject Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/billwise_report_html', $data);
		
		$filename = 'BillWiseReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');
		echo $html;exit;

	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function sendmail(){
		//require 'phpmailer/PHPMailerAutoload.php';

		$this->load->model('catalog/order');
		$this->load->language('catalog/reportbill');
		$this->document->setTitle('Online Order Reject Report');

		
		if(isset($this->request->get['filter_startdate'])){
			$data['startdate'] = $this->request->get['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$data['enddate'] = $this->request->get['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = 0 AND `complimentary_status` = '0'ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$data['cancelamount'] = $cancelbillitem['total_amt'];

			$advance = $this->db->query("SELECT SUM(advance_amt) as advancetotal FROM `oc_advance` WHERE `booking_date` >= '".$start_date."' AND `booking_date` <= '".$end_date."'");
			if($advance->num_rows > 0){
				$data['advancetotal'] = $advance->row['advancetotal'];
			} else{
				$data['advancetotal'] = 0;
			}
				
		}


		//echo "<pre>";print_r($billdata);exit;
		$data['billdatas'] = $billdata;

		//$data['action'] = $this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = 'Online Order Reject Report';

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$html = $this->load->view('sale/billwise_report_html', $data);
		//echo $html;exit;
		$filename = 'BillWiseReport.html';
		header('Content-disposition: attachment; filename=' . $filename);
		header('Content-type: text/html');

		
		$filename = 'Bill Wise Report';
		define("DOMPDF_ENABLE_REMOTE", false);
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4', 'portrait');
	    $dompdf->render();
	    $output = $dompdf->output();
	    $date = date('Y-m-d');
	    $backup_name = DIR_DOWNLOAD."BillWiseReport.pdf";
	    file_put_contents($backup_name, $output);

	    $filename = "BillWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;

	 	$to_emails = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT');
	 	$password = $this->model_catalog_order->get_settings('EMAIL_TO_REPORT_PASSWORD');
	 	$username = $this->model_catalog_order->get_settings('EMAIL_USERNAME');

		$mail = new PHPMailer();
		$message = "Bill Wise Report";
		$subject = "Bill Wise Report";

	 	
	 	// echo $from_email;
	 	// echo'<br/>';
	 	// echo $to_emails;
	 	// echo'<br/>';
	 	// echo $password;
	 	// echo'<br/>';
	 	// exit;
		// $to_emails = 'myadav00349@gmail.com';
		// $from_email = 'report@loyalrisk.in';
		// $password = '2020loyalrisk';

		if($to_emails != ''){
			$to_emails_array = explode(',', $to_emails);
			$to_emails_array[]= $to_emails;
			$to_emails_array[]= $to_emails;
		} else {
			$to_emails_array[] = $to_emails; 
			$to_emails_array[]= $to_emails;
		}

		$date = date('Y-m-d');
		$filename = "BillWiseReport.pdf";
		$file_path = DIR_DOWNLOAD.$filename;
		
		//$mail->SMTPDebug = 3;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Helo = "HELO";
		$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
		$mail->Port = '587';//$this->config->get('config_smtp_port');
		$mail->Username = $username;//$this->config->get('config_smtp_username');
		$mail->Password = $password;//$this->config->get('config_smtp_password');
		// $mail->Username = 'report@loyalrisk.in';//$this->config->get('config_smtp_username');
		// $mail->Password = '2020loyalrisk';//$this->config->get('config_smtp_password');
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($username, 'Hotel');
		$mail->Subject = $subject;
		$mail->Body = html_entity_decode($message);
		$mail->addAttachment($file_path, $filename);
		foreach($to_emails_array as $ekey => $evalue){
			//$to_name = 'Aaron Fargose';
			$mail->AddAddress($evalue);
		}
		
		if($mail->Send()) {
			//echo"send";
			$this->session->data['success'] = 'Mail Sent';
		} else {
			//echo"not send";
			$this->session->data['success'] = 'Mail Not Sent';
		}
	 	//echo 'Done';exit;
	 	//$this->getList();
		$this->response->redirect($this->url->link('catalog/reportbill', 'token=' . $this->session->data['token'], true));
	}
}