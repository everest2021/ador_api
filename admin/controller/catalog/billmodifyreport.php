<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogBillmodifyreport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->model('catalog/order');
		$this->load->language('catalog/billmodifyreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/billmodifyreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$data['final_data'] = array();
		$billdata = array();
		$final_datas = array();
		$new_itemdatas = array();
		$pre_itemdatas = array();
		$data['cancelamount'] = '';



		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);
			foreach ($dates as $date) {
				$datas = $this->db->query("SELECT `order_id` FROM  oc_order_info_report WHERE bill_modify = '1' AND `bill_date` = '".$date."'  ")->rows;
				foreach ($datas as $dkey => $dvalue) {
					$pre_datas = $this->db->query("SELECT * FROM oc_order_items_modify oit LEFT JOIN oc_order_info_report_modify oi ON (oi.`batch_id` = oit.`batch_id`) WHERE  oi.`batch_id` = '".$dvalue['order_id']."' AND oi.`bill_date` = '".$date."' AND oit.is_new <> '0'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` <> '1' GROUP BY oit.code ,oit.batch_id ORDER BY oi.batch_id")->rows;
					$batch = 1;
					foreach ($pre_datas as $pkey => $pvalue) {
						$pre_itemdatas[]= array(
							'ref_no' => $pvalue['batch_id'],
							't_name' => $pvalue['t_name'],
							'waiter_code' => $pvalue['waiter_code'],
							'bill_date' => $pvalue['bill_date'],
							'item_name' => $pvalue['name'],
							'qty' => $pvalue['qty'],
							'rate' => $pvalue['rate'],
							'discount_value' => $pvalue['discount_value'],
							'pay_cash' => $pvalue['pay_cash'],
							'pay_card' => $pvalue['pay_card'],
							'pay_online' => $pvalue['pay_online'],
							'total_payment' => $pvalue['total_payment'],
							'modify_remark' => $pvalue['modify_remark'],
						);
					}
					$new_datas = $this->db->query("SELECT * FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`order_id` = '".$dvalue['order_id']."' AND oit.`cancelstatus` <> '1' AND oi.`bill_date` = '".$date."' AND oi.bill_modify = '1' AND oit.`ismodifier` = '1' AND oit.`cancel_bill` <> '1' AND oit.`new_amt` <> '0' GROUP BY oit.code ,oit.order_id ORDER BY oi.order_id")->rows;
					foreach ($new_datas as $nkey => $nvalue) {
						$pre_itemdatas[]= array(
							'batch' => $batch,
							'ref_no' => $nvalue['order_id'],
							't_name' => $nvalue['t_name'],
							'waiter_code' => $nvalue['waiter_code'],
							'bill_date' => $nvalue['bill_date'],
							'item_name' => $nvalue['name'],
							'qty' => $nvalue['qty'],
							'rate' => $nvalue['rate'],
							'discount_value' => $nvalue['discount_value'],
							'pay_cash' => $nvalue['pay_cash'],
							'pay_card' => $nvalue['pay_card'],
							'pay_online' => $nvalue['pay_online'],
							'total_payment' => $nvalue['total_payment'],
							'modify_remark' => $nvalue['modify_remark'],
						);
					}
					$batch ++;
				}
			}
			$final_datas[] = array(
				'pre_itemdatas' => $pre_itemdatas,
				//'new_itemdatas' => $new_itemdatas,
			);
			// echo "<pre>";
			// print_r($final_datas);
			// exit;
		$data['final_data'] = $final_datas;
			
		}

		$data['action'] = $this->url->link('catalog/billmodifyreport', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/billmodifyreport_form', $data));
	}


	public function export(){
		//echo'innnnn';exit;
		$this->load->model('catalog/order');
		$this->load->language('catalog/billmodifyreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/billmodifyreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';


		//if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($data['startdate']);
			$enddate =  strtotime($data['enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$dates = $this->GetDays($start_date,$end_date);

			$billmodifydatas = $this->db->query("SELECT * FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` >= '".$start_date."' AND oi.`bill_date` >= '".$end_date."' AND oit.`bill_modify` = '1' AND oit.`ismodifier` = '1' AND oit.`cancel_bill` <> '1' ")->rows;
		
			$data['billdatas'] = $billmodifydatas;
		//}

		$data['token'] = $this->session->data['token'];

		
		$fp = fopen(DIR_DOWNLOAD . "BillModify.csv", "w"); 
		$row = array();
		$row = $data['billdatas'];
		// echo'<pre>';
		// print_r($row);
		// exit;
		$line = "";
		$comma = "";
		
		//$line .= $comma . '"' . str_replace('"', '""', "Sr. No") . '"';
		//$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Ref No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Table No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Waiter No") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Bill Date") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Item Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Quantity") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Rate") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Discount") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Pay By") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "New.Item Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "New.Quantity") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "New.Rate") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "New.Discount") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "New.Pay By") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Reason") . '"';
		$comma = ",";
		
		$comma = ",";

		$line .= "\n";
		fputs($fp, $line);
		$i ='1';
		foreach($row as $key => $value) { 
			// echo'<pre>';
			// print_r($value);
			// exit();	
			$comma = "";
			$line = "";
			//$line .= $comma . '"' . str_replace('"', '""', $i) . '"';
			
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['order_no'])) . '"';
				$comma = ",";
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['t_name'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['waiter_code'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['bill_date'])) . '"';
			if($value['new_amt'] == '0') {
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['name'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['qty'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['rate'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['discount_value'])) . '"';
				if($value['pay_cash'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash')) . '"';
				} else if($value['pay_card'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Card')) . '"';
				} else if($value['pay_online'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Online')) . '"';
				} else if($value['pay_cash'] != '0' && $value['pay_card'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash/Card')) . '"';
				} else if($value['pay_cash'] != '0' && $value['pay_online'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash/Online')) . '"';
				} else if($value['pay_card'] != '0' && $value['pay_online'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Card/Online')) . '"';
				} else {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Pending')) . '"';
				}
				
			} else {
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
			}

			if($value['new_amt'] != '0') {
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['name'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['new_qty'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['new_rate'])) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['discount_value'])) . '"';
				if($value['new_pay_cash'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash')) . '"';
				} else if($value['new_pay_card'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Card')) . '"';
				} else if($value['new_pay_online'] != '0'){
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Online')) . '"';
				} else if($value['new_pay_cash'] != '0' && $value['new_pay_card'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash/Card')) . '"';
				} else if($value['new_pay_cash'] != '0' && $value['new_pay_online'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Cash/Online')) . '"';
				} else if($value['new_pay_card'] != '0' && $value['new_pay_online'] != '0') {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Card/Online')) . '"';
				} else {
					$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('Pending')) . '"';
				}
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['modify_remark'])) . '"';
			} else {
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
				$line .= $comma . '"' . str_replace('"', '""', html_entity_decode('0')) . '"';
			}
			
			$i++;
			$line .= "\n";
			//echo $line;exit;
			fputs($fp, $line);
		}
		//echo $line;exit;
		fclose($fp);
		$filename = "BillModify.csv";
		$file_path = DIR_DOWNLOAD . $filename;
		//$file_path = '/Library/WebServer/Documents/riskmanagement/'.$filename;
		$html = file_get_contents($file_path);
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;
		exit;
	}


	public function prints() {
		$this->load->model('catalog/order');
		date_default_timezone_set("Asia/Kolkata");
		$billdatas = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) ){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);

			$startdate1 = date('d-m-Y',strtotime($start_date));
			$enddate1 = date('d-m-Y',strtotime($end_date));

			$dates = $this->GetDays($start_date,$end_date);

			foreach($dates as $date){
				$billdata[$date] = $this->db->query("SELECT * FROM `oc_order_info_report` WHERE `bill_date` = '".$date."' AND `bill_status` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' ORDER BY order_no ASC")->rows;
			}

			$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1'")->row;
			$cancelamount = $cancelbillitem['total_amt'];
				

			$discount = 0;
			$grandtotal = 0;
			$vat = 0; 
			$gst = 0; 
			$discountvalue = 0; 
			$afterdiscount = 0; 
			$stax = 0; 
			$roundoff = 0; 
			$total = 0; 
			$advance = 0;

			if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
			try {
		    // Enter the share name for your USB printer here
		    //$connector = new WindowsPrintConnector("XP-58C");
		    // Print a "Hello world" receipt" //
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
			   	$printer->setJustification(Printer::JUSTIFY_CENTER);
			    $printer->feed(1);
			   	//$printer->setFont(Printer::FONT_B);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
			    $printer->feed(1);
			    $printer->setTextSize(1, 1);
			    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
			    $printer->feed(1);
			    $printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad(date('d/m/Y'),30)."".date('h:i:sa'));
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_CENTER);
			  	$printer->text("Bill Wise Sales Report");
			  	$printer->feed(2);
			  	$printer->setJustification(Printer::JUSTIFY_LEFT);
			  	$printer->text(str_pad("From :".$startdate1,30)."To :".$enddate1);
			  	$printer->feed(2);
			  	$printer->text(str_pad("Ref No",7)."".str_pad("Food",7)."".str_pad("Bar",7)."".str_pad("Disc",7).""
				  		.str_pad("Total",7)."".str_pad("Pay By",8)."T No");
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	foreach ($billdata as $key => $value){
			  		$printer->setJustification(Printer::JUSTIFY_CENTER);
			  		$printer->text($key);
			  		$printer->feed(1);
			  		$printer->setJustification(Printer::JUSTIFY_LEFT);
				  	foreach ($value as $data) {
				  		if($data['food_cancel'] == 1) {
				  			$data['ftotal'] = 0;
				  		}
				  		if($data['liq_cancel'] == 1) {
				  			$data['ltotal'] = 0;
				  		}
				  		if($data['pay_cash'] != '0') {
				  			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Card",8).$data['t_name']);
					  	}else if($data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("OnAc",8).$data['t_name']);
					  	}else if($data['mealpass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Meal Pass",8).$data['t_name']);
					  	}else if($data['pass'] != '0') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pass",8).$data['t_name']);
					  	}else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { 
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Cash+Card",8).$data['t_name']);
					  	}else{
					  		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1) {
					  			$total = round($data['ftotal'] + $data['ltotal']);
					  		} else {
					  			$total = round($data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
					  		}
					  		$printer->text(str_pad($data['order_no'],7)."".str_pad($data['ftotal'],7)."".str_pad($data['ltotal'],7)."".str_pad($data['ftotalvalue'] + $data['ltotalvalue'],7)."".str_pad($total,7)."".str_pad("Pending",8).$data['t_name']);
					  	}
				  		$printer->feed(1);
				  		  if($data['liq_cancel'] == 1){
				  		  	$vat = 0;
				  		  } else{
				  		  	$vat = $vat + $data['vat'];
				  		  }
				  		  if($data['food_cancel'] == 1){
				  		  	$gst = 0;
				  		  } else{
				  		  	$gst = $gst + $data['gst'];
				  		  }
				  		  $stax = $stax + $data['stax'];
				  		  if($data['food_cancel'] == 1){
				  		  	$data['ftotalvalue'] = 0; 
				  		  } 
				  		  if($data['liq_cancel'] == 1){
				  		  	$data['ltotalvalue'] = 0;
				  		  }
				  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
				  		  if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
				  		  	$total = $grandtotal + $stax;
				  		  } else{
			  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
			  		  	 	$total = $grandtotal + $gst + $vat + $stax; 
				  		  }
				  		  $discountvalue = $discountvalue + $discount;
				  		  $afterdiscount = $total - $discountvalue;
				  		  $roundoff = $roundoff + $data['roundtotal'];
				  		  $advance = $advance + $data['advance_amount'];
				  	}
				}
			  	$printer->setEmphasis(false);
				$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Bill Total :",20)."".$grandtotal);
			  	$printer->setEmphasis(false);
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("O- Chrg Amt (+) :",20)."0");
	  			$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("Vat Amt (+) :",20)."".$vat);
			  	$printer->feed(1);
	  			$printer->text(str_pad("",20)."".str_pad("S- Chrg Amt (+) :",20)."".$stax);
	  			$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("GST Amt (+) :",20)."".$gst);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("KKC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("SBC Amt (+) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("R-Off Amt (+) :",20)."".$roundoff);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Total :",20)."".($total + $roundoff));
			  	$printer->feed(1);
			  	$printer->setEmphasis(false);
			  	$printer->text(str_pad("",20)."".str_pad("Discount Amt (-) :",20)."".$discountvalue);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Cancel Bill Amt (-) :",20).$cancelamount);
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Comp. Amt (-) :",20)."0");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance. Amt (-) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->setEmphasis(true);
			  	$printer->text(str_pad("",20)."".str_pad("Net Amount :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Advance Amt (+) :",20).$advance);
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
			  	$printer->text(str_pad("",20)."".str_pad("Grand Total (+) :",20)."".($afterdiscount + $roundoff));
			  	$printer->feed(1);
			  	$printer->text("------------------------------------------------");
			  	$printer->feed(1);
				$printer->cut();
			    // Close printer //
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";;
			}
			$this->getList();
		}
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}