<?php
class ControllerCatalogBillmerge extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle('Add Bill');
		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/table');
		$this->document->setTitle('Add Bill');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->db->query("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no'] . "' ");
			$this->log->write("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no'] . "' ");
			if($this->request->post['order_no_1'] != '' && $this->request->post['order_no_1'] != '0'){
				$this->db->query("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_1'] . "' ");
				$this->log->write("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_1'] . "' ");
			}
			if($this->request->post['order_no_2'] != '' && $this->request->post['order_no_2'] != '0'){
				$this->db->query("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_2'] . "' ");
				$this->log->write("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_2'] . "' ");
			}
			if($this->request->post['order_no_3'] != '' && $this->request->post['order_no_3'] != '0'){
				$this->db->query("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_3'] . "' ");
				$this->log->write("UPDATE " . DB_PREFIX . "order_info SET merge_number ='" . $this->request->post['order_no']. "' WHERE order_no = '" . $this->request->post['order_no_3'] . "' ");
			}
			//$json['done'] = '<img style="width:100%" src="'.HTTP_CATALOG.'system/storage/download/done.gif"> ';
			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$this->response->setOutput(json_encode($json));
		}
	}

	protected function getForm() {
		$data['heading_title'] = 'Bill Merge';//$this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_no'])) {
			$data['action'] = $this->url->link('catalog/billmerge/edit', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/billmerge/edit', 'token=' . $this->session->data['token'] . '&order_no=' . $this->request->get['order_no'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/billmerge', 'token=' . $this->session->data['token'] . $url . '&order_no=' . $this->request->get['order_no'], true);

		$data['token'] = $this->session->data['token'];

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		//$this->log->write(print_r($this->request->get, true));

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
			// $results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no']."' AND `date` = '".$last_open_date."' ");
			// $food_total = 0;
			// $liquor_total = 0;
			// if($results->num_rows > 0){
			// 	$result = $results->row;
			// 	$food_total = $result['ftotal'];
			// 	$liquor_total = $result['ltotal'];
			// }
			$data['food_total'] = '';//$food_total;
			$data['liquor_total'] = '';//$liquor_total;
			$data['grand_total'] = '';//$food_total + $liquor_total;
		} elseif (isset($this->request->get['order_no'])) {
			//$data['order_no'] = $this->request->get['order_no'];
			$data['order_no'] = '';
			$data['food_total'] = '';
			$data['liquor_total'] = '';
			$data['grand_total'] = '';
			// $results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no']."' AND `date` = '".$last_open_date."' ");
			// $food_total = 0;
			// $liquor_total = 0;
			// if($results->num_rows > 0){
			// 	$result = $results->rows;
			// 	$food_total = $result['ftotal'];
			// 	$liquor_total = $result['ltotal'];
			// }
			$data['food_total'] = '';//$food_total;
			$data['liquor_total'] = '';//$liquor_total;
			$data['grand_total'] = '';//$food_total + $liquor_total;
		} else {
			$data['order_no'] = '';
			$data['food_total'] = '';
			$data['liquor_total'] = '';
			$data['grand_total'] = '';
		}

		if (isset($this->request->post['order_no_1'])) {
			$data['order_no_1'] = $this->request->post['order_no_1'];
			if($data['order_no_1'] != '0' && $data['order_no_1'] != ''){
				$results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no_1']."' AND `date` = '".$last_open_date."' ");
				$food_total_1 = '';
				$liquor_total_1 = '';
				$grand_total_1 = '';
				if($results->num_rows > 0){
					$result = $results->row;
					$food_total_1 = $result['ftotal'];
					$liquor_total_1 = $result['ltotal'];
					$grand_total_1 = $result['ftotal'] + $result['ltotal'];
				}
			} else {
				$food_total_1 = '';
				$liquor_total_1 = '';
				$grand_total_1 = '';
			}
			$data['food_total_1'] = $food_total_1;
			$data['liquor_total_1'] = $liquor_total_1;
			$data['grand_total_1'] = $grand_total_1;
		} else {
			$data['order_no_1'] = '';
			$data['food_total_1'] = '';
			$data['liquor_total_1'] = '';
			$data['grand_total_1'] = '';
		}

		if (isset($this->request->post['order_no_2'])) {
			$data['order_no_2'] = $this->request->post['order_no_2'];
			if($data['order_no_2'] != '0' && $data['order_no_2'] != ''){
				$results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no_2']."' AND `date` = '".$last_open_date."' ");
				$food_total_2 = '';
				$liquor_total_2 = '';
				$grand_total_2 = '';
				if($results->num_rows > 0){
					$result = $results->row;
					$food_total_2 = $result['ftotal'];
					$liquor_total_2 = $result['ltotal'];
					$grand_total_2 = $result['ftotal'] + $result['ltotal'];
				}
			} else {
				$food_total_2 = '';
				$liquor_total_2 = '';
				$grand_total_2 = '';
			}
			$data['food_total_2'] = $food_total_2;
			$data['liquor_total_2'] = $liquor_total_2;
			$data['grand_total_2'] = $grand_total_2;
		} else {
			$data['order_no_2'] = '';
			$data['food_total_2'] = '';
			$data['liquor_total_2'] = '';
			$data['grand_total_2'] = '';
		}

		if (isset($this->request->post['order_no_3'])) {
			$data['order_no_3'] = $this->request->post['order_no_3'];
			if($data['order_no_3'] != '0' && $data['order_no_3'] != ''){
				$results = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no` FROM `oc_order_info` WHERE `order_no` = '".$data['order_no_3']."' AND `date` = '".$last_open_date."' ");
				$food_total_3 = '';
				$liquor_total_3 = '';
				$grand_total_3 = '';
				if($results->num_rows > 0){
					$result = $results->row;
					$food_total_3 = $result['ftotal'];
					$liquor_total_3 = $result['ltotal'];
					$grand_total_3 = $result['ftotal'] + $result['ltotal'];
				}
			} else {
				$food_total_3 = '';
				$liquor_total_3 = '';
				$grand_total_3 = '';
			}
			$data['food_total_3'] = $food_total_3;
			$data['liquor_total_3'] = $liquor_total_3;
			$data['grand_total_3'] = $grand_total_3;
		} else {
			$data['order_no_3'] = '';
			$data['food_total_3'] = '';
			$data['liquor_total_3'] = '';
			$data['grand_total_3'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/billmerge_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/billmerge')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// if (isset($this->request->post['order_no'])){
		// 	$this->error['order_no'] = 'Please Enter order number';
		// }
		// if (($this->request->post['table_frm']) == ($this->request->post['table_to'])) {
		// 	$this->error['table_to'] = 'both tables are same';
		// }
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/billmerge')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function getorderinfo() {
		$json = array();
		$json['status'] = 0;
		if (isset($this->request->get['order_no'])) {
			if(isset($this->request->get['order_no'])){
				$order_no = $this->request->get['order_no'];
			} else {
				$order_no = null;
			}
			// if(isset($this->request->get['master_order_no'])){
			// 	$master_order_no = $this->request->get['master_order_no'];
			// } else {
			// 	$master_order_no = null;
			// }
			$filter_data = array(
				'order_no' => $order_no,
				//'master_order_no' => $master_order_no,
			);
			// $last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
			// $last_open_dates = $this->db->query($last_open_date_sql);
			// if($last_open_dates->num_rows > 0){
			// 	$last_open_date = $last_open_dates->row['bill_date'];
			// } else {
			// 	$last_open_date = date('Y-m-d');
			// }
			//$results = $this->model_catalog_table->getTables($filter_data);
			//$results = $this->db->query("SELECT o.`ftotal`, o.`ltotal`, oi.`billno`, o.`merge_number` FROM `oc_order_info` o LEFT JOIN `oc_order_items` oi ON(oi.`order_id` = o.`order_id`) WHERE oi.`billno` = '".$filter_data['order_no']."' AND o.`year_close_status` = '0' ")->rows;
			$results = $this->db->query("SELECT o.`ftotal`, o.`ltotal`, o.`order_no`, o.`merge_number` FROM `oc_order_info` o WHERE o.`order_no` = '".$filter_data['order_no']."' AND o.`year_close_status` = '0' ")->rows;
			//$this->log->write(print_r($results, true));
			$in = 0;
			foreach ($results as $result) {
				$in = 1;
				if($result['merge_number'] == '0'){
					$json = array(
						'order_no' => $result['order_no'],
						'ftotal'   => $result['ftotal'],
						'ltotal'   => $result['ltotal'],
						'grand_total' => $result['ftotal'] + $result['ltotal'],
						'status' => 1,
					);
				} else {
					$json['status'] = 2;
				}
			}
			if($in == 1){
				// $sort_order = array();
				// foreach ($json as $key => $value) {
				// 	$sort_order[$key] = $value['order_no'];
				// }
				// array_multisort($sort_order, SORT_ASC, $json);
			}
		}
		//$this->log->write(print_r($json, true));
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}