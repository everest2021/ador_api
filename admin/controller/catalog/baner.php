<?php

class ControllerCatalogBaner extends Controller {

	private $error = array();
	public function index() {

		$this->load->language('catalog/baner');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/testimonal');

		$this->getform();
	}



	public function add() {

		$this->load->language('catalog/testimonal');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/testimonal');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_catalog_testimonal->addactivity($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}


			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/testimonal', 'token=' . $this->session->data['token'] . $url, true));

		}

		$this->getForm();

	}


	public function edit() {
		$this->load->language('catalog/testimonal');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/testimonal');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_testimonal->editactivity($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/baner', 'token=' . $this->session->data['token'] . $url, true));

		}

		$this->getForm();

	}

	public function delete() {

		$this->load->language('catalog/activity');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/activity');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_activity->deleteactivity($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_sport_type'])) {
				$url .= '&filter_sport_type=' . $this->request->get['filter_sport_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/activity', 'token=' . $this->session->data['token'] . $url, true));

		}



		$this->getList();

	}



	protected function getList() {

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)

		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/testimonal', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/testimonal/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/testimonal/delete', 'token=' . $this->session->data['token'] . $url, true);
		
		$filter_data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$activity_total = $this->model_catalog_testimonal->getTotalactivity();
		$results = $this->model_catalog_testimonal->getactivitys($filter_data);

		foreach ($results as $result) {

			$data['activitys'][] = array(
				'id'		=>$result['id'],
				'discription'        => $result['discription'],
				'customer_name'        => $result['customer_name'],
				'edit'        => $this->url->link('catalog/testimonal/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);

		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['text_confirm'] = $this->language->get('text_confirm');



		$data['column_name'] = $this->language->get('column_name');

		$data['column_rate'] = $this->language->get('column_rate');

		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');

		$data['button_edit'] = $this->language->get('button_edit');

		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/testimonal', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);

		$url = '';


		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();

		$pagination->total = $activity_total;

		$pagination->page = $page;

		$pagination->limit = $this->config->get('config_limit_admin');

		$pagination->url = $this->url->link('catalog/testimonal', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);



		$data['pagination'] = $pagination->render();



		$data['results'] = sprintf($this->language->get('text_pagination'), ($activity_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($activity_total - $this->config->get('config_limit_admin'))) ? $activity_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $activity_total, ceil($activity_total / $this->config->get('config_limit_admin')));



		$data['filter_name'] = $filter_name;

		$data['sort'] = $sort;

		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/testimonal_list', $data));

	}



	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');

		$data['entry_rate'] = $this->language->get('entry_rate');

		$data['entry_filename'] = $this->language->get('entry_filename');

		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');

		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');

		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['button_upload'] = $this->language->get('button_upload');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['rate'])) {
			$data['error_rate'] = $this->error['rate'];
		} else {
			$data['error_rate'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/baner', 'token=' . $this->session->data['token'], true)

		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/baner', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('catalog/baner/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/baner/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/baner', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['token'] = $this->session->data['token'];
		$base_url = HTTP_CATALOG.'system/storage/download/';
		$base_url1 = DIR_DOWNLOAD;
		$file_show_path = $base_url.'banerimages/';
		$file_show_path_1 = $base_url1.'banerimages/';

		$otherimages_datas = array();
		$otherimages_datas = $this->db->query("SELECT * FROM oc_image ")->rows;
		
		foreach($otherimages_datas as $okey => $ovalue){
			
			if($ovalue['image'] != '' && file_exists($file_show_path_1.$ovalue['image'])){
				$otherimages_datas[$okey]['value_proc'] = $file_show_path.$ovalue['image'];
			} else {
				$otherimages_datas[$okey]['value_proc'] = '';
			}
		}
		
		$data['otherimages_datas'] = $otherimages_datas;

		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/baner_form', $data));

	}

	



	protected function validateForm() {

		if (!$this->user->hasPermission('modify', 'catalog/activity')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		return !$this->error;
	}



	protected function validateDelete() {

		if (!$this->user->hasPermission('modify', 'catalog/testimonal')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/testimonal');
		return !$this->error;
	}

	public function uploadmulti() {
		$this->load->language('catalog/baner');

		$json = array();
		$base = HTTP_CATALOG.'system/storage/download/banerimages/';
		$file_upload_path = DIR_DOWNLOAD.'banerimages/';
		$file_show_path = $base.'banerimages/';
		$files_arr = array();
   		$image_name = array();
   		foreach ($_FILES['file']['name'] as $key => $value) {
   			$files_arr[$key]['name'] = $value;
   			$image_name[] = $value;
   		}
   		foreach ($_FILES['file']['type'] as $key => $value) {
   			$files_arr[$key]['type'] = $value;
   		}
   		foreach ($_FILES['file']['tmp_name'] as $key => $value) {
   			$files_arr[$key]['tmp_name'] = $value;
   		}
   		foreach ($_FILES['file']['error'] as $key => $value) {
   			$files_arr[$key]['error'] = $value;
   		}
   		foreach ($_FILES['file']['size'] as $key => $value) {
   			$files_arr[$key]['size'] = $value;
   		}
   		
   		$i = 0 ;
   		foreach ($files_arr as $fkey => $fvalue) {
   			$img_extension = strtolower(substr(strrchr($fvalue['name'], '.'), 1));
			$file = 'Image_'.date('Ymdhis').$i.'.'.$img_extension;
			$http_path = $base.$file;
			if($file != ''){
				$exist_path = $file_upload_path.$file;
				if(file_exists($exist_path)){
					unlink($exist_path);
				}
			}
			if (move_uploaded_file($fvalue['tmp_name'], $file_upload_path . $file)) {
				$destFile = $file_upload_path . $file;
				chmod($destFile, 0777);
			}
			$sql = "INSERT INTO `oc_image` SET `image` = '".$file."',`image_source` = '".$http_path."' ";
			$this->db->query($sql);
			
			$i++;
		}
		$json['reload_href'] = $this->url->link('catalog/baner', 'token=' . $this->session->data['token'], true);
		$json['success'] = $this->language->get('text_upload');
		$this->log->write(print_r($json,true));
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		//echo "in";exit;
		$this->load->language('catalog/baner');
		$json = array();
		//$transaction_id = $this->request->get['transaction_id'];
		$image_id = $this->request->get['image_id'];
		//$other_image_id = $this->request->get['other_image_id'];
		//$fold_name = $transaction_id;
		$file_upload_path = DIR_DOWNLOAD.'banerimages/';
		$image_datas = $this->db->query("SELECT * FROM `oc_image` WHERE `image_id` = '".$image_id."' ");
		if($image_datas->num_rows > 0){
			$image_data = $image_datas->row;
			$image_name = $image_data['image'];
			if($image_name != ''){
				$exist_path = $file_upload_path.$image_name;
				if(file_exists($exist_path)){
					unlink($exist_path);
				}
			}
			$del_sql = "DELETE FROM `oc_image` WHERE `image_id` = '".$image_id."' ";
			$this->db->query($del_sql);
		}
		$json['reload_href'] = $this->url->link('catalog/baner/edit', 'token=' . $this->session->data['token'].'&image_id='.$image_id, true);
		$json['success'] = $this->language->get('text_upload');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}



	

	public function autocomplete() {

		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/testimonal');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_testimonal->getactivitys($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'id' => $result['id'],
					'customer_name'        => strip_tags(html_entity_decode($result['customer_name'], ENT_QUOTES, 'UTF-8'))
				);

			}

		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['customer_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));

	}

}