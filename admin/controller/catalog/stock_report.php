<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStockReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stock_report');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/stock_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stock_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['store'])){
			$data['store_id'] = $this->request->get['store'];
			$store_id = $this->request->get['store'];
		}
		else{
			$data['store_id']= '';
			$store_id = '';
		}


		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['store_id'])) {
			$url .= '&store_id=' . $this->request->get['store_id'];
		}
		

		$filter_data = array(
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			'store_id' =>$store_id,
			);

		$this->load->model('catalog/stock_report');
		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = '';
		$data['grand_total_open_bal'] = '';
		$data['grand_total_avail_quantity'] = '';
		$data['grand_total_purchase_order'] = '';
		$data['grand_total_stock_transfer_to'] = '';
		$data['grand_total_stock_transfer_from'] = '';
		$data['grand_total_sale_qty'] = '';
		$data['grand_total_westage_amt'] = '';
		$data['grand_total_manufacture_order'] = '';
		$data['grand_total_stock_rate'] = '';
		$data['grand_total_final_amt'] = '';

		$grand_total_open_bal = 0;
		$grand_total_avail_quantity = 0;
		$grand_total_purchase_order = 0;
		$grand_total_stock_transfer_to = 0;
		$grand_total_stock_transfer_from = 0;
		$grand_total_sale_qty = 0;
		$grand_total_wastage_amt = 0;
		$grand_total_manufacture_order = 0;
		$grand_total_stock_rate = 0;
		$grand_total_final_amt = 0;

		$final_data = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['store_id'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$store_id = $this->request->get['store_id'];
		
		
			$sql = "SELECT * FROM oc_store_name WHERE `store_type` = 'Food' ";
			if(!empty($this->request->get['store_id'])){
				$sql .= " AND id = '".$this->request->get['store_id']."'";
			}
			$store_datas = $this->db->query($sql)->rows;
			$open_bal = 0;
			$avail_quantity = 0;
		 	$purchase_order = 0;
			$stock_transfer_to = 0;
			$stock_transfer_from = 0;
			$sale_qty = 0;
			$westage_amt = 0;

			$invoice_dates = $this->db->query("SELECT invoice_date FROM purchase_test WHERE invoice_date < '".$start_date."'  ORDER BY id DESC LIMIT 1");
			if($invoice_dates->num_rows > 0){
				$invoice_date = $invoice_dates->row['invoice_date'];
			} else {
				$invoice_date = date('Y-m-d');
			}
			$closing_sql = "SELECT item_code,store_id,invoice_date,closing_balance FROM purchase_test WHERE invoice_date = '".$invoice_date."' ";
			$closing_sqls = $this->db->query($closing_sql);
			$open_balss = array();
			if($closing_sqls->num_rows > 0 ){
				foreach($closing_sqls->rows as $akey => $avalue){
					$open_balss[$avalue['item_code'].'_'.$avalue['store_id'].'_'.$avalue['invoice_date']] = $avalue['closing_balance'];
				}
			}

			$purchase_orders = "SELECT description_code,store_id,invoice_date,SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND category = 'FOOD' GROUP BY unit_id, description_id ";
			$purchase_orderss = $this->db->query($purchase_orders);
			$purchase_orders = array();
			if($purchase_orderss->num_rows > 0 ){
				foreach($purchase_orderss->rows as $pkey => $pvalue){
					$purchase_orders[$pvalue['description_code'].'_'.$pvalue['store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}


			$manufacture_orderss = "SELECT description_code,store_id,invoice_date,SUM(qty) as qty FROM oc_stockmanufacturer_items osif LEFT JOIN oc_stockmanufacturer osf ON(osf.`id` = osif.`p_id`) WHERE  `invoice_date` = '".$start_date."'   AND category = 'FOOD' GROUP BY unit_id, description_id ";
			$manufacture_ordersss = $this->db->query($manufacture_orderss);
			$manufacture_orderss = array();
			if($manufacture_ordersss->num_rows > 0 ){
				foreach($manufacture_ordersss->rows as $pkey => $pvalue){
					$manufacture_orderss[$pvalue['description_code'].'_'.$pvalue['store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$stock_trans_sql = "SELECT description_code,from_store_id,invoice_date,SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND category = 'FOOD' GROUP BY unit_id, description_code";
			$stock_transs = $this->db->query($stock_trans_sql);
			$stock_trans = array();
			if($stock_transs->num_rows > 0 ){
				foreach($stock_transs->rows as $pkey => $pvalue){
					$stock_trans[$pvalue['description_code'].'_'.$pvalue['from_store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$stock_trans_to_sql = "SELECT description_code,to_store_id,invoice_date,SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."' AND category = 'FOOD' GROUP BY description_code,unit_id";
			$stock_trans_tos = $this->db->query($stock_trans_to_sql);
			$stock_trans_to = array();
			if($stock_trans_tos->num_rows > 0 ){
				foreach($stock_trans_tos->rows as $pkey => $pvalue){
					$stock_trans_to[$pvalue['description_code'].'_'.$pvalue['to_store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$purchase_order_looses_sql = "SELECT description_code,store_id,invoice_date,SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE category = 'FOOD'  AND loose='0' GROUP BY unit_id, description_id";
			$purchase_order_loosess = $this->db->query($purchase_order_looses_sql);
			$purchase_order_looses = array();
			if($purchase_order_loosess->num_rows > 0 ){
				foreach($purchase_order_loosess->rows as $pkey => $pvalue){
					$purchase_order_looses[$pvalue['description_code'].'_'.$pvalue['store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$purchase_order_loose_aval_query_sql = "SELECT description_code,store_id,invoice_date,SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE  category = 'FOOD' AND loose='1' GROUP BY unit_id, description_id";
			$purchase_order_loose_aval_querys = $this->db->query($purchase_order_loose_aval_query_sql);
			$purchase_order_loose_aval_query = array();
			if($purchase_order_loose_aval_querys->num_rows > 0 ){
				foreach($purchase_order_loose_aval_querys->rows as $pkey => $pvalue){
					$purchase_order_loose_aval_query[$pvalue['description_code'].'_'.$pvalue['store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$order_item_sql = "SELECT  oi.`bill_date`,`code`, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'   GROUP BY code";
			$order_items = $this->db->query($order_item_sql);
			$order_item = array();
			if($order_items->num_rows > 0 ){
				foreach($order_items->rows as $pkey => $pvalue){
					$order_item[$pvalue['code'].'_'.$pvalue['bill_date']] = $pvalue['sale_qty'];
				}
			}

			$order_item_report_sql = "SELECT  oi.`bill_date`,code, SUM(qty) as sale_qty FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'   GROUP BY code";
			$order_item_reports = $this->db->query($order_item_report_sql);
			$order_item_report = array();
			if($order_item_reports->num_rows > 0 ){
				foreach($order_item_reports->rows as $pkey => $pvalue){
					$order_item_report[$pvalue['code'].'_'.$pvalue['bill_date']] = $pvalue['sale_qty'];
				}
			}
			$west_sql = "SELECT invoice_date,from_store_id,description_code,SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  GROUP BY unit_id, description_id";
			$wests = $this->db->query($west_sql);
			$west = array();
			if($wests->num_rows > 0 ){
				foreach($wests->rows as $pkey => $pvalue){
					$west[$pvalue['description_code'].'_'.$pvalue['from_store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
				}
			}

			$stockpric_sql = "SELECT purchase_rate,item_type,uom,item_code  FROM oc_stock_item WHERE 1=1";
			$stockprics = $this->db->query($stockpric_sql);
			$stockpric = array();
			if($stockprics->num_rows > 0 ){
				foreach($stockprics->rows as $pkey => $pvalue){
					$stockpric[$pvalue['item_code']] = $pvalue;
				}
			}
			foreach($store_datas as $store_data){
				$invoice_datez = $this->db->query("SELECT invoice_date FROM purchase_test  ORDER BY id DESC LIMIT 1");
				if($invoice_datez->num_rows > 0){
					$invoice_dat = $invoice_datez->row['invoice_date'];
				} else {
					$invoice_dat = date('Y-m-d');
				}
				$item_datas = $this->db->query("SELECT * FROM purchase_test WHERE invoice_date = '".$invoice_dat."' AND store_id = '".$store_data['id']."'  ")->rows;
					// echo'<pre>';
					// print_r($invoice_dat);
					// exit;

					$all_data = array();
					$sub_total_open_bal = 0;
					$sub_total_avail_quantity = 0;
					$sub_total_purchase_order = 0;
					$sub_total_stock_transfer_to = 0;
					$sub_total_stock_transfer_from 	= 0;
					$sub_total_wastage_amt 	= 0;
					$sub_total_manufacture_order = 0;
					$sub_total_stock_rate = 0;
					$sub_total_final_amt = 0;
					$sub_total_sale_qty = 0;
					foreach($item_datas as $ikey => $value){

						$open_bal = 0;
						if(isset($open_balss[$value['item_code'].'_'.$store_data['id'].'_'.$invoice_date])){
							$open_bal = $open_balss[$value['item_code'].'_'.$store_data['id'].'_'.$invoice_date];
						}
						// $open_balss = $this->db->query("SELECT closing_balance FROM purchase_test WHERE item_code = '".$value['item_code']."' AND store_id = '".$store_data['id']."' AND invoice_date = '".$invoice_date."'");
						// if($open_balss->num_rows > 0){
						// 	$open_bal = $open_balss->row['closing_balance'];
						// } else {
						// 	$open_bal = 0;
						// }

						// echo'<pre>';
						// print_r($manufacture_order);
						// exit;
						$purchase_order = 0;
						if(isset($purchase_orders[$value['item_code'].'_'.$store_data['id'].'_'.$start_date])){
							$purchase_order = $purchase_orders[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}

						
						// $manufacture_orders = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items osif LEFT JOIN oc_stockmanufacturer osf ON(osf.`id` = osif.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_id");
						// if($manufacture_orders->num_rows > 0){
						// 	$manufacture_order = $manufacture_orders->row['qty'];
						// } else {
						// 	$manufacture_order = 0;
						// }

						$manufacture_order = 0;
						if(isset($manufacture_orderss[$value['item_code'].'_'.$store_data['id'].'_'.$start_date])){
							$manufacture_order = $manufacture_orderss[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}
						
						// $stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND from_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_code");
						// if($stock_transfer_from->num_rows > 0){
						// 	$stock_transfer_from = $stock_transfer_from->row['qty'];
						// }
						//  else{
						// 	$stock_transfer_from = 0;
						// }

						$stock_transfer_from = 0;
						if(isset($stock_trans[$value['item_code'].'_'.$store_data['id'].'_'.$start_date])){
							$stock_transfer_from = $stock_trans[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}

						// echo'<pre>';
						// print_r($store_data['id']);

						// $stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."' AND to_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY description_code,unit_id");
						// if($stock_transfer_to->num_rows > 0){
						// 	$stock_transfer_to = $stock_transfer_to->row['qty'];
						// }  else{
						// 	$stock_transfer_to = 0;
						// }


						$stock_transfer_to = 0;
						if(isset($stock_trans_to[$value['item_code'].'_'.$store_data['id'].'_'.$start_date])){
							$stock_transfer_to = $stock_trans_to[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}

						$total_purchase = 0;
						if($purchase_order > 0){
							$total_purchase = $purchase_order;
						} else {
							$total_purchase = 0;
						}

						if(isset($purchase_order_looses[$value['item_code'].'_'.$store_data['id'].'_'.$start_date]) && $purchase_order_looses[$value['item_code'].'_'.$store_data['id'].'_'.$start_date] > 0 ){
							$total_purchase = $total_purchase - $purchase_order_looses[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}

						if(isset($purchase_order_loose_aval_query[$value['item_code'].'_'.$store_data['id'].'_'.$start_date]) && $purchase_order_loose_aval_query[$value['item_code'].'_'.$store_data['id'].'_'.$start_date] > 0 ){
							$total_purchase = $total_purchase +  $purchase_order_loose_aval_query[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}
						
						if($stock_transfer_from > 0){
							$total_purchase = ($open_bal + $total_purchase) - $stock_transfer_from;
						} elseif($stock_transfer_to  > 0) {
							$total_purchase = $open_bal + $total_purchase + $stock_transfer_to;
						}


						$bom_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$value['item_code']."' AND store_id = '".$store_data['id']."'");
						if($bom_data->num_rows > 0){
							$sale_qty = 0;
							foreach($bom_data->rows as $key){
								/*$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");
								if($order_item_data->num_rows > 0){
									$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
								}*/
								//$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");

								// $order_item_data_report = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");

								$order_item_data = 0;
								if(isset($order_item[$key['parent_item_code'].'_'.$start_date])){
									$order_item_data =$order_item[$key['parent_item_code'].'_'.$start_date];
								}

								$order_item_data_report = 0;
								if(isset($order_item_report[$key['parent_item_code'].'_'.$start_date])){
									$order_item_data_report = $order_item_report[$key['parent_item_code'].'_'.$start_date];
								}

								if($order_item_data > 0 && $order_item_data_report > 0){
									$sale_qty = 0;
								} else if($order_item_data > 0 ){
									$sale_qty = $sale_qty + $key['qty'] * $order_item_data;
								} else if($order_item_data_report > 0){
									$sale_qty = $sale_qty + $key['qty'] * $order_item_data_report;
								} 
								// if($order_item_data->num_rows > 0 && $order_item_data_report->num_rows > 0){
								// 	$sale_qty = 0;
								// } else if($order_item_data->num_rows > 0 ){
								// 	$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
								// } else if($order_item_data_report->num_rows > 0){
								// 	$sale_qty = $sale_qty + $key['qty'] * $order_item_data_report->row['sale_qty'];
								// } 
							}
						} else{
							$sale_qty = 0;
						}

						// $west_sql = "SELECT invoice_date,from_store_id,description_code,SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  GROUP BY unit_id, description_id";
						// $wests = $this->db->query($west_sql);
						// $west = array();
						// if($wests->num_rows > 0 ){
						// 	foreach($wests->rows as $pkey => $pvalue){
						// 		$west[$pvalue['description_code'].'_'.$pvalue['from_store_id'].'_'.$pvalue['invoice_date']] = $pvalue['qty'];
						// 	}
						// }

						$wastage_amt = 0;
						if(isset($west[$value['item_code'].'_'.$store_data['id'].'_'.$start_date])){
							$wastage_amt = $west[$value['item_code'].'_'.$store_data['id'].'_'.$start_date];
						}
						$avail_quantity = 0;
						if($stock_transfer_from > 0 || $stock_transfer_to > 0){
							$avail_quantity = $total_purchase - ($sale_qty + abs($wastage_amt));
						} else{
							//echo'innn';exit;
							$avail_quantity = ($open_bal + $total_purchase) - ($sale_qty +  abs($wastage_amt));

						}



						// $wastage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND from_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_id");
						// if($wastage_datas->num_rows > 0){
						// 	$wastage_amt = $wastage_datas->row['qty'];
						// } else {
						// 	$wastage_amt = 0;
						// }

						// $stock_price = $this->db->query("SELECT purchase_rate  FROM oc_stock_item WHERE item_code = '".$value['item_code']."'");
						// if($stock_price->num_rows > 0){
						// 	$stock_rate = $stock_price->row['purchase_rate'];
						// } else {
						// 	$stock_rate = 0;
						// }

						// $stock_type = $this->db->query("SELECT item_type,uom FROM oc_stock_item WHERE item_code = '".$value['item_code']."'")->row;

						$stock_rate = 0;
						$stock_type = '';
						$stock_uom = '';
						if(isset($stockpric[$value['item_code']])){
							$stock_rate = $stockpric[$value['item_code']]['purchase_rate'];
							$stock_uom = $stockpric[$value['item_code']]['uom'];
							$stock_type = $stockpric[$value['item_code']]['item_type'];
						}

						// if($stock_types->num_rows > 0){
						// 	$stock_type = $stock_types->row['uom'];
						// } else {
						// 	$stock_type = 0;
						// }

						//$avail_quantity = $this->model_catalog_stock_report->availableQuantity($stock_uom, $value['item_code'], '0',$store_data['id'],'',$stock_type,$start_date);

						$final_amt = $avail_quantity * $stock_rate;
						
						$sub_total_open_bal = $sub_total_open_bal + abs($open_bal);
						$sub_total_avail_quantity = $sub_total_avail_quantity + abs($avail_quantity);
						$sub_total_purchase_order = $sub_total_purchase_order + abs($purchase_order);
						$sub_total_stock_transfer_to = $sub_total_stock_transfer_to + abs($stock_transfer_to);
						$sub_total_stock_transfer_from = $sub_total_stock_transfer_from + abs($stock_transfer_from);
						$sub_total_sale_qty = $sub_total_sale_qty + abs($sale_qty);
						$sub_total_wastage_amt = $sub_total_wastage_amt + abs($wastage_amt);
						$sub_total_manufacture_order = $sub_total_manufacture_order + abs($manufacture_order);
						$sub_total_stock_rate = $sub_total_stock_rate + abs($stock_rate);
						$sub_total_final_amt = $sub_total_final_amt + abs($final_amt);
						// echo'<pre>';
						// print_r($final_amt);
						// exit;
						$all_data[] = array(
							'item_name'     => $value['item_name'],
							'open_bal'		=> $open_bal,
							'avail_quantity' 	=> $avail_quantity,
							'purchase_order' 	=> $purchase_order,
							'stock_transfer_to' 	=> $stock_transfer_to,
							'stock_transfer_from' 	=> $stock_transfer_from,
							'sale_qty' 		=> $sale_qty,
							'wastage_amt'	 => $wastage_amt,
							'manufac_amt' 	=> $manufacture_order,
							'stock_rate' 	=> $stock_rate,
							'final_amt' => $final_amt,
						);
					}
				$grand_total_open_bal = $grand_total_open_bal + $sub_total_open_bal;
				$grand_total_avail_quantity = $grand_total_avail_quantity + $sub_total_avail_quantity;
				$grand_total_purchase_order = $grand_total_purchase_order + $sub_total_purchase_order;
				$grand_total_stock_transfer_to = $grand_total_stock_transfer_to + $sub_total_stock_transfer_to;
				$grand_total_stock_transfer_from = $grand_total_stock_transfer_from + $sub_total_stock_transfer_from;
				$grand_total_sale_qty = $grand_total_sale_qty + $sub_total_sale_qty;
				$grand_total_wastage_amt = $grand_total_wastage_amt + $sub_total_wastage_amt;
				$grand_total_manufacture_order = $grand_total_manufacture_order + $sub_total_manufacture_order;
				$grand_total_stock_rate = $grand_total_stock_rate + $sub_total_stock_rate;
				$grand_total_final_amt = $grand_total_final_amt + $sub_total_final_amt;

				$final_data[] = array(
					'store_name' => $store_data['store_name'],
					'all_data' => $all_data,
					'sub_total_open_bal' 		=> $sub_total_open_bal,
					'sub_total_avail_quantity' 	=> $sub_total_avail_quantity,
					'sub_total_purchase_order' 	=> $sub_total_purchase_order,
					'sub_total_stock_transfer_to' 		=> $sub_total_stock_transfer_to,
					'sub_total_stock_transfer_from' 	=> $sub_total_stock_transfer_from,
					'sub_total_wastage_amt' 	=> $sub_total_wastage_amt,
					'sub_total_manufacture_order' 		=> $sub_total_manufacture_order,
					'sub_total_stock_rate' 		=> $sub_total_stock_rate,
					'sub_total_final_amt' 	=> $sub_total_final_amt,
					'sub_total_sale_qty' => $sub_total_sale_qty,
				);
			}
	 	}
	 	$data['final_data'] = $final_data;
			
		$data['grand_total_open_bal'] = $grand_total_open_bal;
		$data['grand_total_avail_quantity'] = $grand_total_avail_quantity;
		$data['grand_total_purchase_order'] = $grand_total_purchase_order;
		$data['grand_total_stock_transfer_to'] = $grand_total_stock_transfer_to;
		$data['grand_total_stock_transfer_from'] = $grand_total_stock_transfer_from;
		$data['grand_total_sale_qty'] = $grand_total_sale_qty;
		$data['grand_total_wastage_amt'] = $grand_total_wastage_amt;
		$data['grand_total_manufacture_order'] = $grand_total_manufacture_order;
		$data['grand_total_stock_rate'] = $grand_total_stock_rate;
		$data['grand_total_final_amt'] = $grand_total_final_amt;

		
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Food'")->rows;
		// echo'<pre>';
		// print_r($store_id);
		// exit;
		$data['store_id'] = $store_id;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		$data['action'] = $this->url->link('catalog/stock_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stock_report', $data));
	}


	public function export(){
		$this->load->language('catalog/stock_report');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stock_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->get['filter_startdate'])){
			$filter_startdate = $this->request->get['filter_startdate'];
		} else {
			$filter_startdate = date('d-m-Y');
		}

		if(isset($this->request->get['filter_enddate'])){
			$filter_enddate = $this->request->get['filter_enddate'];
		} else {
			$filter_enddate = date('d-m-Y');
		}

		if(isset($this->request->get['store'])){
			$data['store_id'] = $this->request->get['store'];
			$store_id = $this->request->get['store'];
		}
		else{
			$data['store_id']= '';
			$store_id = '';
		}


		$url = '';

		if (isset($this->request->get['filter_startdate'])) {
			$url .= '&filter_startdate=' . $this->request->get['filter_startdate'];
		}
		if (isset($this->request->get['filter_enddate'])) {
			$url .= '&filter_enddate=' . $this->request->get['filter_enddate'];
		}

		if (isset($this->request->get['store_id'])) {
			$url .= '&store_id=' . $this->request->get['store_id'];
		}
		

		$filter_data = array(
			'filter_startdate' => $filter_startdate,
			'filter_enddate' => $filter_enddate,
			'store_id' =>$store_id,
			);

		$this->load->model('catalog/stock_report');
		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = '';
		$data['grand_total_open_bal'] = '';
		$data['grand_total_avail_quantity'] = '';
		$data['grand_total_purchase_order'] = '';
		$data['grand_total_stock_transfer_to'] = '';
		$data['grand_total_stock_transfer_from'] = '';
		$data['grand_total_sale_qty'] = '';
		$data['grand_total_westage_amt'] = '';
		$data['grand_total_manufacture_order'] = '';
		$data['grand_total_stock_rate'] = '';
		$data['grand_total_final_amt'] = '';

		$grand_total_open_bal = 0;
		$grand_total_avail_quantity = 0;
		$grand_total_purchase_order = 0;
		$grand_total_stock_transfer_to = 0;
		$grand_total_stock_transfer_from = 0;
		$grand_total_sale_qty = 0;
		$grand_total_wastage_amt = 0;
		$grand_total_manufacture_order = 0;
		$grand_total_stock_rate = 0;
		$grand_total_final_amt = 0;

		$final_data = array();

		if(isset($this->request->get['filter_startdate']) && isset($this->request->get['filter_enddate']) || isset($this->request->get['store_id'])){
			$startdate = strtotime($this->request->get['filter_startdate']);
			$enddate =  strtotime($this->request->get['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$store_id = $this->request->get['store_id'];
		
		
			$sql = "SELECT * FROM oc_store_name WHERE `store_type` = 'Food' ";
			if(!empty($this->request->get['store_id'])){
				$sql .= " AND id = '".$this->request->get['store_id']."'";
			}
			$store_datas = $this->db->query($sql)->rows;
			$open_bal = 0;
			$avail_quantity = 0;
		 	$purchase_order = 0;
			$stock_transfer_to = 0;
			$stock_transfer_from = 0;
			$sale_qty = 0;
			$westage_amt = 0;
			foreach($store_datas as $store_data){
				$invoice_datez = $this->db->query("SELECT invoice_date FROM purchase_test  ORDER BY id DESC LIMIT 1");
				if($invoice_datez->num_rows > 0){
					$invoice_dat = $invoice_datez->row['invoice_date'];
				} else {
					$invoice_dat = date('Y-m-d');
				}
				$item_datas = $this->db->query("SELECT * FROM purchase_test WHERE invoice_date = '".$invoice_dat."' AND store_id = '".$store_data['id']."' ")->rows;
					// echo'<pre>';
					// print_r($invoice_dat);
					// exit;

					$all_data = array();
					$sub_total_open_bal = 0;
					$sub_total_avail_quantity = 0;
					$sub_total_purchase_order = 0;
					$sub_total_stock_transfer_to = 0;
					$sub_total_stock_transfer_from 	= 0;
					$sub_total_wastage_amt 	= 0;
					$sub_total_manufacture_order = 0;
					$sub_total_stock_rate = 0;
					$sub_total_final_amt = 0;
					$sub_total_sale_qty = 0;
					foreach($item_datas as $ikey => $value){
						$invoice_dates = $this->db->query("SELECT invoice_date FROM purchase_test WHERE invoice_date < '".$start_date."'  ORDER BY id DESC LIMIT 1");
						if($invoice_dates->num_rows > 0){
							$invoice_date = $invoice_dates->row['invoice_date'];
						} else {
							$invoice_date = date('Y-m-d');
						}
						$open_balss = $this->db->query("SELECT closing_balance FROM purchase_test WHERE item_code = '".$value['item_code']."' AND store_id = '".$store_data['id']."' AND invoice_date = '".$invoice_date."'");
						if($open_balss->num_rows > 0){
							$open_bal = $open_balss->row['closing_balance'];
						} else {
							$open_bal = 0;
						}

				
						$purchase_orders = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."' AND store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_id");
						if($purchase_orders->num_rows > 0){
							$purchase_order = $purchase_orders->row['qty'];
						} else {
							$purchase_order = 0;
						}

						
						$manufacture_orders = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items osif LEFT JOIN oc_stockmanufacturer osf ON(osf.`id` = osif.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_id");
						if($manufacture_orders->num_rows > 0){
							$manufacture_order = $manufacture_orders->row['qty'];
						} else {
							$manufacture_order = 0;
						}

						$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND from_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_code");
						if($stock_transfer_from->num_rows > 0){
							$stock_transfer_from = $stock_transfer_from->row['qty'];
						}
						 else{
							$stock_transfer_from = 0;
						}

						// echo'<pre>';
						// print_r($store_data['id']);

						$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."' AND to_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY description_code,unit_id");
						if($stock_transfer_to->num_rows > 0){
							$stock_transfer_to = $stock_transfer_to->row['qty'];
						}  else{
							$stock_transfer_to = 0;
						}

						$bom_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$value['item_code']."' AND store_id = '".$store_data['id']."'");
						if($bom_data->num_rows > 0){
							$sale_qty = 0;
							foreach($bom_data->rows as $key){
								$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");
								if($order_item_data->num_rows > 0){
									$sale_qty = $sale_qty + $bom_data->row['qty'] * $order_item_data->row['sale_qty'];
								}
							}
						} else{
							$sale_qty = 0;
						}

						$wastage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE  `invoice_date` = '".$start_date."'  AND from_store_id = '".$store_data['id']."' AND description_code = '".$value['item_code']."' GROUP BY unit_id, description_id");
						if($wastage_datas->num_rows > 0){
							$wastage_amt = $wastage_datas->row['qty'];
						} else {
							$wastage_amt = 0;
						}

						$stock_price = $this->db->query("SELECT purchase_rate  FROM oc_stock_item WHERE item_code = '".$value['item_code']."'");
						if($stock_price->num_rows > 0){
							$stock_rate = $stock_price->row['purchase_rate'];
						} else {
							$stock_rate = 0;
						}

						$stock_types = $this->db->query("SELECT item_type,uom FROM oc_stock_item WHERE item_code = '".$value['item_code']."'");

						if($stock_types->num_rows > 0){
							$stock_type = $stock_types->row['uom'];
						} else {
							$stock_type = 0;
						}

						$avail_quantity = $this->model_catalog_stock_report->availableQuantity($stock_type, $value['item_code'], '0',$store_data['id'],'',$stock_type['item_type'],$start_date);

						$final_amt = $avail_quantity * $stock_rate;
						
						$sub_total_open_bal = $sub_total_open_bal + abs($open_bal);
						$sub_total_avail_quantity = $sub_total_avail_quantity + abs($avail_quantity);
						$sub_total_purchase_order = $sub_total_purchase_order + abs($purchase_order);
						$sub_total_stock_transfer_to = $sub_total_stock_transfer_to + abs($stock_transfer_to);
						$sub_total_stock_transfer_from = $sub_total_stock_transfer_from + abs($stock_transfer_from);
						$sub_total_sale_qty = $sub_total_sale_qty + abs($sale_qty);
						$sub_total_wastage_amt = $sub_total_wastage_amt + abs($wastage_amt);
						$sub_total_manufacture_order = $sub_total_manufacture_order + abs($manufacture_order);
						$sub_total_stock_rate = $sub_total_stock_rate + abs($stock_rate);
						$sub_total_final_amt = $sub_total_final_amt + abs($final_amt);
						// echo'<pre>';
						// print_r($final_amt);
						// exit;
						$all_data[] = array(
							'item_name'     => $value['item_name'],
							'open_bal'		=> $open_bal,
							'avail_quantity' 	=> $avail_quantity,
							'purchase_order' 	=> $purchase_order,
							'stock_transfer_to' 	=> $stock_transfer_to,
							'stock_transfer_from' 	=> $stock_transfer_from,
							'sale_qty' 		=> $sale_qty,
							'wastage_amt'	 => $wastage_amt,
							'manufac_amt' 	=> $manufacture_order,
							'stock_rate' 	=> $stock_rate,
							'final_amt' => $final_amt,
						);
					}
				$grand_total_open_bal = $grand_total_open_bal + $sub_total_open_bal;
				$grand_total_avail_quantity = $grand_total_avail_quantity + $sub_total_avail_quantity;
				$grand_total_purchase_order = $grand_total_purchase_order + $sub_total_purchase_order;
				$grand_total_stock_transfer_to = $grand_total_stock_transfer_to + $sub_total_stock_transfer_to;
				$grand_total_stock_transfer_from = $grand_total_stock_transfer_from + $sub_total_stock_transfer_from;
				$grand_total_sale_qty = $grand_total_sale_qty + $sub_total_sale_qty;
				$grand_total_wastage_amt = $grand_total_wastage_amt + $sub_total_wastage_amt;
				$grand_total_manufacture_order = $grand_total_manufacture_order + $sub_total_manufacture_order;
				$grand_total_stock_rate = $grand_total_stock_rate + $sub_total_stock_rate;
				$grand_total_final_amt = $grand_total_final_amt + $sub_total_final_amt;

				$final_data[] = array(
					'store_name' => $store_data['store_name'],
					'all_data' => $all_data,
					'sub_total_open_bal' 		=> $sub_total_open_bal,
					'sub_total_avail_quantity' 	=> $sub_total_avail_quantity,
					'sub_total_purchase_order' 	=> $sub_total_purchase_order,
					'sub_total_stock_transfer_to' 		=> $sub_total_stock_transfer_to,
					'sub_total_stock_transfer_from' 	=> $sub_total_stock_transfer_from,
					'sub_total_wastage_amt' 	=> $sub_total_wastage_amt,
					'sub_total_manufacture_order' 		=> $sub_total_manufacture_order,
					'sub_total_stock_rate' 		=> $sub_total_stock_rate,
					'sub_total_final_amt' 	=> $sub_total_final_amt,
					'sub_total_sale_qty' => $sub_total_sale_qty,
				);
			}
	 	}
	 	$data['final_data'] = $final_data;
			
		$data['grand_total_open_bal'] = $grand_total_open_bal;
		$data['grand_total_avail_quantity'] = $grand_total_avail_quantity;
		$data['grand_total_purchase_order'] = $grand_total_purchase_order;
		$data['grand_total_stock_transfer_to'] = $grand_total_stock_transfer_to;
		$data['grand_total_stock_transfer_from'] = $grand_total_stock_transfer_from;
		$data['grand_total_sale_qty'] = $grand_total_sale_qty;
		$data['grand_total_wastage_amt'] = $grand_total_wastage_amt;
		$data['grand_total_manufacture_order'] = $grand_total_manufacture_order;
		$data['grand_total_stock_rate'] = $grand_total_stock_rate;
		$data['grand_total_final_amt'] = $grand_total_final_amt;

		
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['filter_startdate'] = $filter_startdate;
		$data['filter_enddate'] = $filter_enddate;
		$data['action'] = $this->url->link('catalog/stock_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// echo "<pre>";
		// print_r($data);
		// exit;
		$html = $this->load->view('sale/stock_report_html', $data);
		$filename = 'stockreport';
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;
		
		// $filename = 'StockReport.html';
		// header('Content-disposition: attachment; filename=' . $filename);
		// header('Content-type: text/html');
		// echo $html;exit;

	}


	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	 public function autocomplete() {
	 	$json = array();
	 	if(isset($this->request->get['filter_name'])){
	 		$results = $this->db->query("SELECT type_id, brand FROM oc_brand WHERE brand LIKE '%".$this->request->get['filter_name']."%' LIMIT 0,10")->rows;
	 		foreach($results as $key){
	 			$json[] = array(
	 						'id'	=> $key['type_id'],
	 						'name'  => $key['brand']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	}

	 public function getUnit() {
	 	$json = array();
	 	if(isset($this->request->get['filter_id'])){
			$results = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$this->request->get['filter_id']."' AND loose = 0")->rows;
			$jsons[] = array(
							'id' => '',
							'name' => 'Please Select'
						);
	 		foreach($results as $key){
	 			$jsons[] = array(
	 						'id'	=> $key['id'],
	 						'name'  => $key['size']
	 					  );
	 		}
	 	}
	 	$json['units'] = $jsons;
	 	$this->response->setOutput(json_encode($json));
	}
}