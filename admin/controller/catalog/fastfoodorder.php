<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogOrder extends Controller {
	private $error = array();

	public function index() {
		$bill_date = $this->db->query("SELECT * FROM oc_order_info WHERE bill_date > '".date('Y-m-d')."' ORDER BY order_id DESC LIMIT 1");
		$notimp = DIR_ACTIVATE;
		$localfile = file_get_contents($notimp);
		$localdata = explode('|', $localfile);
		// if(file_exists($notimp)){
		// 	if(isset($localdata[2]) && strtotime($localdata[2]) >= strtotime(date('Y-m-d'))){
		// 		$computerkey = shell_exec('wmic DISKDRIVE GET SerialNumber');
		// 		$encryptcomputerkey = md5($computerkey);
		// 		$prefix = md5(1000);
		// 		$postfix = md5(2000);
		// 		$result = $prefix.$encryptcomputerkey.$postfix;
		// 		$localfile = file_get_contents(DIR_ACTIVATE);
		// 		$localdata = explode('|', $localfile);
		// 		if($localdata[0] != $result){
		// 			header('location:activate.php');
		// 		}
		// 	} else {
		// 		header('location:activate.php?expire=1');
		// 	}
		// } else {
		// 	header('location:activate.php');
		// }

		if($bill_date->num_rows > 0){
			$this->session->data['warning1'] = "Please change system date";
			$this->user->logout();
			$this->response->redirect($this->url->link('common/logout'));
		} 
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');
		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');
		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			//echo 'in add';exit;
			$order_id = $this->model_catalog_order->addOrder($this->request->post);
			// $this->prints($order_id);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if($this->request->post['nc_kot_status'] == 1){
				echo "please close this window";
				exit();
			} else {
				$this->prints($order_id);
			}
		}
		$this->getList();
	}

	public function getList() {
		// echo'innn';
		// exit;
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/order');
		if(!empty($this->session->data['tablet'])){
			unset($this->session->data['tablet']);
		}
		$data['loc'] = '';
		$data['locid'] = '';
		$data['parcel_detail'] = '';
		$data['rate_id'] = '';
		$data['cust_name'] = '';
		$data['cust_id'] = '';
		$data['cust_contact'] = '';
		$data['cust_address'] = '';
		$data['cust_email'] = '';
		$data['table'] = '';
		$data['table_id'] = '';
		$data['waiter'] = '';
		$data['waiterid'] = '';
		$data['waiter_id'] = '';
		$data['captain'] = '';
		$data['captainid'] = '';
		$data['captain_id'] = '';
		$data['person'] = '';
		$data['ftotal'] = 0;
		$data['gst'] = 0;
		$data['ltotal'] = 0;
		$data['vat'] = 0;
		$data['cess'] = '';
		$data['stax'] = '';
		$data['ftotalvalue'] = '';
		$data['fdiscountper'] = '';
		$data['discount'] = '';
		$data['ldiscount'] = '';
		$data['ldiscountper'] = '';
		$data['ltotalvalue'] = '';

		$data['dchargeper'] = '';
		$data['dcharge'] = '';
		$data['dtotalvalue'] = '';

		$data['grand_total'] = '';
		$data['advance_billno'] = '0';
		$data['advance_amount'] = '0.00';
		$data['roundtotal'] = '';
		$data['total_items'] = '';
		$data['item_quantity'] = '';
		$data['date_added'] = '';
		$data['time_added'] = '';
		$data['kot_no'] = '';

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		// echo'<pre>';
		// print_r($this->request->get['order_id']);
		// exit;

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			//$this->log->write('----------------------------Edit bill Start-----------------------------------');
			//$this->log->write('User Name : ' .$this->user->getUserName());
			//$this->log->write('Order Id : ' .$order_id);
		
			$query = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id`='".$order_id."'")->rows;
		// 	echo'<pre>';
		// print_r($query);
		// exit;
			$query1 = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id`='".$order_id."'")->rows;
			$data['orderitems'] = $query1;
			$data['loc'] = $query[0]['location'];
			$data['locid'] = $query[0]['location_id'];
			$data['parcel_detail'] = $query[0]['parcel_status'];
			$data['rate_id'] = $query[0]['rate_id'];
			$data['table'] = $query[0]['t_name'];
			$data['table_id'] = $query[0]['table_id'];
			$data['waiter'] = $query[0]['waiter'];
			$data['waiterid'] = $query[0]['waiter_code'];
			$data['waiter_id'] = $query[0]['waiter_id'];
			$data['captainid'] = $query[0]['captain_code'];
			$data['captain'] = $query[0]['captain'];
			$data['captain_id'] = $query[0]['captain_id'];
			$data['person'] = $query[0]['person'];
			$data['ftotal'] = $query[0]['ftotal'];
			$data['gst'] = $query[0]['gst'];
			$data['ltotal'] = $query[0]['ltotal'];
			$data['vat'] = $query[0]['vat'];
			$data['cess'] = $query[0]['cess'];
			$data['stax'] = $query[0]['stax'];
			$data['ftotalvalue'] = $query[0]['ftotalvalue'];
			$data['fdiscountper'] = $query[0]['fdiscountper'];
			$data['discount'] = $query[0]['discount'];
			$data['ldiscount'] = $query[0]['ldiscount'];
			$data['ldiscountper'] = $query[0]['ldiscountper'];
			$data['ltotalvalue'] = $query[0]['ltotalvalue'];

			$data['dtotalvalue'] = $query[0]['dtotalvalue'];
			$data['dchargeper'] = $query[0]['dchargeper'];
			$data['dcharge'] = $query[0]['dcharge'];


			$data['grand_total'] = $query[0]['grand_total'];
			$data['advance_billno'] = $query[0]['advance_billno'];
			$data['advance_amount'] = $query[0]['advance_amount'];
			$data['roundtotal'] = $query[0]['roundtotal'];
			$data['total_items'] = $query[0]['total_items'];
			$data['item_quantity'] = $query[0]['item_quantity'];
			$data['date_added'] = $query[0]['date_added'];
			$data['time_added'] = $query[0]['time_added'];
			$data['kot_no'] = $query[0]['kot_no'];
			$data['cust_name'] = $query[0]['cust_name'];
			$data['cust_address'] = $query[0]['cust_address'];
			$data['cust_id'] = $query[0]['cust_id'];
			$data['cust_contact'] = $query[0]['cust_contact'];
			$data['cust_email'] = $query[0]['cust_email'];
			// echo '<pre>';
			// print_r($query[0]['location']);
			// print_r($query);
			// print_r($query1);
			// exit;
		//$this->log->write('---------------------------Edit bill end----------------------------------');

		}


		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		$data['display_type'] = type;
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/order', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/order/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/order/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['orders'] = array();

		$locations =  $this->db->query("SELECT  * FROM " . DB_PREFIX . "location")->rows;
		$billdates =  $this->db->query("SELECT bill_date FROM oc_order_info WHERE day_close_status = '0' order by bill_date desc LIMIT 1 ");
		$count = $billdates->num_rows;
		if($billdates->num_rows > 0){
			if($billdates->row['bill_date'] == date('Y-m-d')){
				$data['dayclose'] = 1;	
				$data['bill_date'] = date('d-m-Y');
			} else {
				$data['dayclose'] = 0;
				$data['bill_date'] = date('d-m-Y', strtotime($billdates->row['bill_date']));
			}
		} else {
			$data['dayclose'] = 1;
			$data['bill_date'] = date('d-m-Y');
		}

		foreach ($locations as $lkey => $loc) {
			if($data['locid'] != ''){
				$data['sel'] = $data['locid'];
			} elseif($lkey == 0) {
				$data['sel'] = $loc['location_id'];
			}
			$data['locations'][] = array(
				'select' => $data['sel'],
				'name'	=> $loc['location'],
				'id'	=>	$loc['location_id']
			);
			// echo'<pre>';
			// print_r($data['locations']);
			// exit;
		}
		
		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if(isset($this->request->get['edit'])){
			$data['editorder'] = $this->request->get['edit'];
		} else{
			$data['editorder'] = '';
		}

		if(isset($this->request->get['order_id'])){
			$data['editorderid'] = $this->request->get['order_id'];
		} else{
			$data['editorderid'] = '';
		}

		if (isset($this->request->get['orderidmodify'])) {
			$data['orderidmodify'] = $this->request->get['orderidmodify'];
		} else{
			$data['orderidmodify'] = '';
		}

		if (isset($this->request->get['order_id'])) {
			$url = '&order_id=' . $this->request->get['order_id'];
		}

		if(isset($this->request->get['edit'])){
			$url .= '&edit=' . $this->request->get['edit'];
		}

		// if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/order/add', 'token=' . $this->session->data['token'] . $url, true);
		// } 

		
		/************************************** Cheking Pending Prints Start ************************************/
		$kot = array();
		$bill = array();
		/*
		$kotremanings = $this->db->query("SELECT oit.`kot_no`, table_id, total_items, oi.`order_id`, oi.`bill_date` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oit.`printstatus` <> '2' GROUP BY oit.`kot_no`");
		$billremanings = $this->db->query("SELECT oi.`grand_total`, oit.`billno`, table_id, total_items, oi.`order_id`, oi.`bill_date` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`printstatus` <> '2' GROUP BY oit.`billno`");

		echo 'aaaa';exit;

		if($kotremanings->num_rows > 0){
			foreach($kotremanings->rows as $kotremaning){
				$kot[] = array(
							'order_id' => $kotremaning['order_id'],
							'kot_no' => $kotremaning['kot_no'],
							'table_id' => $kotremaning['table_id'],
							'total_items' => $kotremaning['total_items'],
							'bill_date' => $kotremaning['bill_date']
						);
			}
		}

		if($billremanings->num_rows > 0){
			foreach($billremanings->rows as $billremaning){
				$bill[] = array(
							'billno' => $billremaning['billno'],
							'table_id' => $billremaning['table_id'],
							'order_id' => $billremaning['order_id'],
							'grand_total' => $billremaning['grand_total'],
							'bill_date' => $billremaning['bill_date']
						);
			}
		}
		*/

		$data['kots'] = $kot;
		$data['bills'] = $bill;

		/************************************** Cheking Pending Prints End ************************************/


		$data['WAITER'] = $this->model_catalog_order->get_settings('WAITER');
		$data['CAPTAIN'] = $this->model_catalog_order->get_settings('CAPTAIN');
		$data['PERSONS'] = $this->model_catalog_order->get_settings('PERSONS');
		$data['DAYCLOSE_POPUP'] = $this->model_catalog_order->get_settings('DAYCLOSE_POPUP');
		$data['PERSONS_COMPULSARY'] = $this->model_catalog_order->get_settings('PERSONS_COMPULSARY');
		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		$data['NOCATEGORY'] = $this->model_catalog_order->get_settings('NOCATEGORY');
		$data['CATEGORY'] = $this->model_catalog_order->get_settings('CATEGORY');
		$data['SKIPCATEGORY'] = $this->model_catalog_order->get_settings('SKIPCATEGORY');
		$data['SKIPSUBCATEGORY'] = $this->model_catalog_order->get_settings('SKIPSUBCATEGORY');
		$data['SKIPTABLE'] = $this->model_catalog_order->get_settings('SKIPTABLE');
		$data['TENDERING_ENABLE'] = $this->model_catalog_order->get_settings('TENDERING_ENABLE');
		$data['SETTLEMENT_ON'] = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
		$data['RATE_CHANGE'] = $this->model_catalog_order->get_settings('RATE_CHANGE');
		$data['KOT_BILL_CANCEL_REASON'] = $this->model_catalog_order->get_settings('KOT_BILL_CANCEL_REASON');
		
		$data['cancel_kot_permission'] = $this->user->getCancelKot();
		$data['PRINT_BILL'] = $this->user->getPrintBill();


		$loc_datas =  $this->db->query("SELECT *,`kot_different`, `kot_copy`, `bill_copy`, `direct_bill` FROM oc_location ");
		if($loc_datas->num_rows > 0){
			$loc_dat = $loc_datas->row;
		}

		$data['direct_bill'] = $loc_dat['direct_bill'];

		// echo'<pre>';
		// print_r($data['direct_bill']);
		// exit;
		/************** If previous data is clear but not close Start *************/
		
		//$prev_date = $this->db->query("SELECT * FROM oc_order_info WHERE day_close_status = 0 ORDER BY order_id DESC LIMIT 1");
		if($billdates->num_rows > 0){
			$previous_day_data = $this->db->query("SELECT * FROM oc_order_info WHERE day_close_status = 0 AND bill_date = '".$billdates->row['bill_date']."' AND pay_method = 0 AND cancel_status = 0");
			if($previous_day_data->num_rows > 0){
				$data['clear_previous'] = 0;
			} elseif($previous_day_data->num_rows == 0 && $billdates->row['bill_date'] != date('Y-m-d')){
				$data['clear_previous'] = 1;
				$this->session->data['warning1'] = "Please Close the day"." ".$billdates->row['bill_date'];
			} else{
				$data['clear_previous'] = 0;
			}
		} else{
			$data['clear_previous'] = 0;
		}

		$data['login_url'] = $this->url->link('common/logout');

		/************** If previous data is clear but not close End *************/
		$data['printers'] = array();
		//$data['printers'] = $this->db->query("SELECT printer, printer_type, description,code FROM oc_kotgroup")->rows;
		$data['superadmin'] = $this->session->data['superadmin'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->response->setOutput($this->load->view('catalog/order', $data));

	}
	public function loc_dats() {
		$json = array();
		$loc_datas =  $this->db->query("SELECT *,`kot_different`, `kot_copy`, `bill_copy`, `direct_bill` FROM oc_location WHERE location_id = '".$this->request->get['lid']."' ");
		if($loc_datas->num_rows > 0){
			$loc_dat = $loc_datas->row;
		}
		
		$json = array(
			'loc_dat' => $loc_dat,
		);
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	
	public function get_pending_datas(){
		$json = array();
		$kot = array();
		$bill = array();
		$bill_dates =  $this->db->query("SELECT bill_date FROM oc_order_info WHERE day_close_status = '0' order by bill_date desc LIMIT 1");
		if($bill_dates->num_rows > 0){
			$bill_date = $bill_dates->row['bill_date'];
		} else {
			$bill_date = date('Y-m-d');
		}
		$printers = $this->db->query("SELECT printer, printer_type, description,code FROM oc_kotgroup")->rows;
		$kotremanings = $this->db->query("SELECT oit.`kot_no`, table_id, total_items, oi.`order_id`, oi.`bill_date` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oit.`printstatus` <> '2' AND oi.`bill_date` = '".$bill_date."' GROUP BY oit.`kot_no`");
		if($kotremanings->num_rows > 0){
			foreach($kotremanings->rows as $kotremaning){
				$kot[] = array(
					'order_id' => $kotremaning['order_id'],
					'kot_no' => $kotremaning['kot_no'],
					'table_id' => $kotremaning['table_id'],
					'total_items' => $kotremaning['total_items'],
					'bill_date' => $kotremaning['bill_date'],
					'cancel_href' => $this->url->link('catalog/order/cancel', 'token=' . $this->session->data['token'].'&orderid=' . $kotremaning['order_id'].'&kot=1', true),
					'print_href' => $this->url->link('catalog/order/pendingkot', 'token=' . $this->session->data['token'].'&order_id=' . $kotremaning['order_id'], true),
				);
			}
		}

		$billremanings = $this->db->query("SELECT oi.`grand_total`, oit.`billno`, table_id, total_items, oi.`order_id`, oi.`order_no`, oi.`bill_date` FROM oc_order_info oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`printstatus` <> '2' AND `bill_status` = '1' AND oi.`bill_date` = '".$bill_date."' GROUP BY oit.`billno`");
		if($billremanings->num_rows > 0){
			foreach($billremanings->rows as $billremaning){
				$bill[] = array(
					'billno' => $billremaning['billno'],
					'table_id' => $billremaning['table_id'],
					'order_no' => $billremaning['order_no'],
					'order_id' => $billremaning['order_id'],
					'grand_total' => $billremaning['grand_total'],
					'bill_date' => $billremaning['bill_date'],
					'cancel_href' => $this->url->link('catalog/order/cancel', 'token=' . $this->session->data['token'].'&orderid=' . $billremaning['order_id'].'&bill=1', true),
					'print_href' => $this->url->link('catalog/order/pendingbill', 'token=' . $this->session->data['token'].'&order_id=' . $billremaning['order_id'], true),
				);
			}
		}

		$html_kot = '';
		$i = 0;
		foreach($kot as $kkey => $kvalue){
			$html_kot .= '<tr>';
				$html_kot .= '<td>';
					$html_kot .= $kvalue['bill_date'];
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= $kvalue['kot_no'];
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= $kvalue['table_id'];
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= $kvalue['total_items'];
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= '<a href="'.$kvalue['cancel_href'].'" class="btn btn-primary" onclick="return confirm(\'Are you sure\')">Cancel</a>';
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= '<select class="form-control kot" id="selectprinterkot_'.$i.'">';
						foreach($printers as $printer){
							$html_kot .= '<option value="'.$printer['code'].'">'.$printer['description'].'</option>';
						}
						$html_kot .= '<option value="default" selected="selected">Default</option>';
					$html_kot .= '</select>';
					$html_kot .= '<input type="hidden" id="selectedprinterkot_'.$i.'">';
				$html_kot .= '</td>';
				$html_kot .= '<td>';
					$html_kot .= '<a href="'.$kvalue['print_href'].'" class="btn btn-primary" id="printkot_'.$i.'" onclick="return confirm(\'Are you sure?\')">Print</a>';
				$html_kot .= '</td>';
			$html_kot .= '</tr>';
			$i ++;
		}
		
		$html_bill = '';
		$i = 0;
		foreach($bill as $kkey => $kvalue){
			$html_bill .= '<tr>';
				$html_bill .= '<td>';
					$html_bill .= $kvalue['bill_date'];
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= $kvalue['order_no'];
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= $kvalue['billno'];
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= $kvalue['table_id'];
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= $kvalue['grand_total'];
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= '<a href="'.$kvalue['cancel_href'].'" class="btn btn-primary" onclick="return confirm(\'Are you sure?\')">Cancel</a>';
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= '<select class="form-control bill" id="selectprinterbill_'.$i.'">';
						foreach($printers as $printer){
							$html_bill .= '<option value="'.$printer['code'].'">'.$printer['description'].'</option>';
						}
						$html_bill .= '<option value="default" selected="selected">Default</option>';
					$html_bill .= '</select>';
					$html_bill .= '<input type="hidden" id="selectedprinterbill_'.$i.'">';
				$html_bill .= '</td>';
				$html_bill .= '<td>';
					$html_bill .= '<a href="'.$kvalue['print_href'].'" class="btn btn-primary" id="printbill_'.$i.'" onclick="return confirm(\'Are you sure?\')">Print</a>';
				$html_bill .= '</td>';
			$html_bill .= '</tr>';
			$i ++;
		}

		$json['status'] = 1;
		$json['html_kot'] = $html_kot;
		$json['html_bill'] = $html_bill;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function cancel(){
		if(isset($this->request->get['kot'])){
			$this->db->query("UPDATE oc_order_items SET printstatus = '0' WHERE order_id = '".$this->request->get['orderid']."'");
		}
		if(isset($this->request->get['bill'])){
			$this->db->query("UPDATE oc_order_info SET printstatus = '0' WHERE order_id = '".$this->request->get['orderid']."'");
		}
		$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token']));
	}

	public function pendingkot(){
		$this->load->model('catalog/order');
		$order_id = $this->request->get['order_id'];
		$printerid = $this->request->get['printkot'];
		$anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY subcategoryid")->rows;
		$anss_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
		$anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;

		$liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
		$liqurs_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
		$liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;

		//$checkkot_total = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		if(isset($this->request->get['checkkot'])){
			$getcheckkot = $this->request->get['checkkot'];
		} else{
			$getcheckkot = '0';
		}
		$checkkot = array();
		if($getcheckkot == '1'){
			$checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
		}
		
		$infos_normal = array();
		$infos_cancel = array();
		$infos_liqurnormal = array();
		$infos_liqurcancel = array();
		$infos = array();
		$modifierdata = array();
		$allKot = array();
		$ans = $this->db->query("SELECT `order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
		//$orderitem_time = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' ORDER BY `id` DESC LIMIT 1")->row['time'];
		foreach ($ans as $resultt) {
			$infoss[] = array(
				'order_id'   => $resultt['order_id'],
				'time_added'  => $resultt['time_added'],
				'date_added'  => $resultt['date_added'],
				'location'  => $resultt['location'],
				'location_id' => $resultt['location_id'],
				't_name'    => $resultt['t_name'],
				'table_id'  => $resultt['table_id'],
				'waiter'    => $resultt['waiter'],
				'waiter_id'    => $resultt['waiter_id'],
				'captain'   => $resultt['captain'],
				'captain_id'   => $resultt['captain_id'],
				'item_quantity'   => $resultt['item_quantity'],
				'total_items'   => $resultt['total_items'],
				'login_id' => $resultt['login_id'],
				'login_name' => $resultt['login_name'],
				'person' => $resultt['person'],
				'ftotal' => $resultt['ftotal'],
				'ltotal' => $resultt['ltotal'],
				'grand_total' => $resultt['grand_total'],
				'bill_status' => $resultt['bill_status'],

			);
		}

		$kot_group_datas = array();
			$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup WHERE master_kotprint = '0'")->rows;
		
		foreach($printerinfos as $pkeys => $pvalues){
			if($pvalues['subcategory'] != ''){
				$subcategoryid_exp = explode(',', $pvalues['subcategory']);
				foreach($subcategoryid_exp as $skey => $svalue){
					$kot_group_datas[$svalue] = $pvalues;
				}
			}
		}
		
		foreach ($anss as $lkey => $result) {
			foreach($anss as $lkeys => $resultss){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['code'] = '';
						}
					}
				} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['qty'] = $result['qty'] + $resultss['qty'];
							if($result['nc_kot_status'] == '0'){
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}
			}

			if($result['code'] != ''){
				if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
					$kot_group_datas[$result['subcategoryid']]['code'] = 1;
				}
				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$result['qty'];
				} else {
						$qty = $result['qty'];
				}

				$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
					'id'				=> $result['id'],
					'name'           	=> $result['name'],
					'qty'         		=> $qty,
					'amt'				=> $result['amt'],
					'rate'				=> $result['rate'],
					'message'         	=> $result['message'],
					'subcategoryid'		=> $result['subcategoryid'],
					'kot_no'            => $result['kot_no'],
					'time_added'        => $result['time'],
				);
				$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
				$flag = 0;
			}
		}

		foreach ($checkkot as $lkey => $result) {
			foreach($checkkot as $lkeys => $resultss){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['code'] = '';
						}
					}
				} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['qty'] = $result['qty'] + $resultss['qty'];
							if($result['nc_kot_status'] == '0'){
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}
			}
			if($result['code'] != ''){
				$allKot[] = array(
					'order_id'			=> $result['order_id'],
					'id'				=> $result['id'],
					'name'           	=> $result['name'],
					'qty'         		=> $result['qty'],
					'message'         	=> $result['message'],
					'subcategoryid'		=> $result['subcategoryid'],
					'kot_no'        	=> $result['kot_no'],
					'time_added'        => $result['time'],
				);
				$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
				$flag = 0;
			}
		}
		
		$total_items_normal = 0;
		$total_quantity_normal = 0;
		foreach ($anss_1 as $result) {
			if($result['qty'] > $result['pre_qty']) {
				$tem = $result['qty'] - $result['pre_qty'];
				$ans=abs($tem);
				if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
					$kot_group_datas[$result['subcategoryid']]['code'] = 1;
				}
				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$ans;
				} else {
						$qty = $ans;
				}
				$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
					'name'          	=> $result['name'],
					'qty'         		=> $ans,
					'amt'				=> $result['amt'],
					'rate'				=> $result['rate'],
					'message'       	=> $result['message'],
					'subcategoryid'		=> $result['subcategoryid'],
					'kot_no'        	=> $result['kot_no'],
					'time_added'        => $result['time'],
				);
				$total_items_normal ++;
				$total_quantity_normal = $total_quantity_normal + $ans;
				$flag = 0;
			}
		}

		foreach ($anss_2 as $lkey => $result) {
			foreach($anss as $lkeys => $resultss){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['code'] = '';
						}
					}
				} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
					if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['qty'] = $result['qty'] + $resultss['qty'];
							if($result['nc_kot_status'] == '0'){
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}
			}

			if($result['code'] != ''){
				if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
					$kot_group_datas[$result['subcategoryid']]['code'] = 1;
				}

				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$result['qty'];
				} else {
						$qty = $result['qty'];
				}
				$infos_cancel[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
					'id'				=> $result['id'],
					'name'          	=> $result['name']." (cancel)",
					'qty'         		=> $qty,
					'rate'         		=> $result['rate'],
					'amt'				=> $result['amt'],
					'message'       	=> $result['message'],
					'subcategoryid'		=> $result['subcategoryid'],
					'kot_no'        	=> $result['kot_no'],
					'time_added'        => $result['time'],
				);
				$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
				$flag = 0;
			}
		}

		foreach ($liqurs as $lkey => $liqur) {
			foreach($liqurs as $lkeys => $liqurss){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
					if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
						if($liqur['parent'] == '0'){
							$liqur['code'] = '';
						}
					}
				} elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
					if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
						if($liqur['parent'] == '0'){
							$liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
							if($liqur['nc_kot_status'] == '0'){
								$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
							}
						}
					}
				}
			}

			if($liqur['code'] != ''){
				if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
					$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
				}
				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$liqur['qty'];
				} else {
						$qty = $liqur['qty'];
				}
				$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
					'id'				=> $liqur['id'],
					'name'           	=> $liqur['name'],
					'qty'         		=> $liqur['qty'],
					'amt'				=> $liqur['amt'],
					'rate'				=> $liqur['rate'],
					'message'         	=> $liqur['message'],
					'subcategoryid'		=> $liqur['subcategoryid'],
					'kot_no'            => $liqur['kot_no'],
					'time_added'        => $liqur['time'],
				);
				$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
				$flag = 0;
			}
		}

		foreach ($liqurs_1 as $liqur) {
			if($liqur['qty'] > $liqur['pre_qty']) {
				$tem = $liqur['qty'] - $liqur['pre_qty'];
				$ans=abs($tem);
				if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
					$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
				}

				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$ans;
				} else {
						$qty = $ans;
				}

				$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
					'name'          	=> $liqur['name'],
					'qty'         		=> $qty,
					'amt'				=> $liqur['amt'],
					'rate'				=> $liqur['rate'],
					'message'       	=> $liqur['message'],
					'subcategoryid'		=> $liqur['subcategoryid'],
					'kot_no'        	=> $liqur['kot_no'],
					'time_added'        => $liqur['time'],
				);
				$total_items_liquor_normal ++;
				$total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
				$flag = 0;
			}
		}

		$total_items_liquor_cancel = 0;
		$total_quantity_liquor_cancel = 0;
		foreach ($liqurs_2 as $lkey => $liqur) {
			foreach($liqurs_2 as $lkeys => $liqurs){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1'){
					if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
						if($liqur['parent'] == '0'){
							$liqur['code'] = '';
						}
					}
				} elseif ($liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1') {
					if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
						if($liqur['parent'] == '0'){
							$liqur['qty'] = $liqur['qty'] + $liqurs['qty'];
							if($liqur['nc_kot_status'] == '0'){
								$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
							}
						}
					}
				}
			}

			if($liqur['code'] != ''){
				if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
					$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
				}
				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$liqur['qty'];
				} else {
						$qty = $liqur['qty'];
				}
				$infos_liqurcancel[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
					'id'      			=> $liqur['id'],
					'name'          	=> $liqur['name']."(cancel)",
					'qty'         		=> $qty,
					'amt'         		=> $liqur['amt'],
					'rate'         		=> $liqur['rate'],
					'message'       	=> $liqur['message'],
					'subcategoryid'		=> $liqur['subcategoryid'],
					'kot_no'        	=> $liqur['kot_no'],
					'time_added'        => $liqur['time'],
				);
				//$text = "Cancelled Bot <br>Orderid :".$order_id.", Item Name :".$result['name'].", Qty :".$result['qty'].", Amt :".$result['amt'].", Kot_no :".$result['kot_no'];
				//echo $text;
				$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
				$total_items_liquor_cancel ++;
				$total_quantity_liquor_cancel = $liqur['qty'];
				$flag = 0;
			}
		}

		if(!isset($flag)){
			$anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
			$total_items = 0;
			$total_quantity = 0;
			foreach ($anss as $result) {
				$infos[] = array(
					'name'           	=> $result['name'],
					'qty'         	 	=> $result['qty'],
					'amt'				=> $result['amt'],
					'rate'				=> $result['rate'],
					'message'        	=> $result['message'],
					'subcategoryid'		=> $result['subcategoryid'],
					'kot_no'        	=> $result['kot_no'],
					'time_added' 		=> $result['time'],
				);
				$total_items ++;
				$total_quantity = $total_quantity + $result['qty'];
			}
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "order_items SET printstatus = '0' WHERE  order_id = '".$order_id."'");
		
		// echo '<pre>';
		// print_r($infos_normal);
		// exit;

		$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_name`, `bill_printer_type` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
		if($locationData->num_rows > 0){
			$locationData = $locationData->row;
			if($locationData['kot_different'] == 1){
				$kot_different = 1;
			} else {
				$kot_different = 0;
			}
			$kot_copy = $locationData['kot_copy'];
			$bill_copy = $locationData['bill_copy'];
			$direct_bill = $locationData['direct_bill'];
			$bill_printer_type = $locationData['bill_printer_type'];
			$bill_printer_name = $locationData['bill_printer_name'];
		} else{
			$kot_different = 0;
			$kot_copy = 1;
			$direct_bill = 0;
			$bill_printer_type = '';
			$bill_printer_name = '';
		}
		if($kot_copy > 0){
			if($infos_normal){
				foreach($infos_normal as $nkeys => $nvalues){
					$printtype = '';
				 	$printername = '';
				 	$description = '';
				 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
				 	$printername = $this->user->getPrinterName();
				 	$printertype = $this->user->getPrinterType();
				 	
				 	if($printertype != ''){
				 		$printtype = $printertype;
				 	} else {
				 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
							$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
				 		}
					}

					if ($printername != '') {
						$printname = $printername;
					} else {
						if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
							$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
						}
					}

					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
				 
					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel==0){

							try {
								if($printtype == 'Network'){
									$connector = new NetworkPrintConnector($printername, 9100);
								} else if($printtype == 'Windows'){
								 	$connector = new WindowsPrintConnector($printername);
								} else {
								 	$connector = '';
								}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	for($i =1; $i<= $kot_copy; $i++){
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->text($infoss[0]['location']);
								    $printer->feed(1);
								    $printer->text($description);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setTextSize(1, 1);
								    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->feed(1);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
								    $printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $kot_no_string = '';
								    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
								    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
								    	$printer->feed(1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
										$total_amount_normal = 0;
								    	foreach($nvalues as $keyss => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
									    	if($nvalue['message'] != ''){
									    		$printer->setTextSize(1, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
									    	}
									    	$printer->feed(1);
									    	if($modifierdata != array()){
										    	foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$value['code']."' ")->row['decimal_mesurement'];
															if ($decimal_mesurement == 1) {
																$qty = (int)$modata['qty'];
															} else {
																$qty = (int)$modata['qty'];
															}
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
							    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
							    			$kot_no_string .= $nvalue['kot_no'].",";
									    }
							    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
								    
									    $printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
								    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
							    		$printer->feed(1);
							    		$printer->setTextSize(2, 1);
							    		$printer->text("G.Total :".$total_g );
							    		$printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->cut();
										$printer->feed(2);
								    } else {
									    $printer->text("Qty     Description");
									    $printer->feed(1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($nvalues as $keyss => $valuess){
									    	$printer->setTextSize(2, 1);
									    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
									    	if($valuess['message'] != ''){
									    		$printer->setTextSize(1, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
									    	}
									    	$printer->feed(1);
								    		foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $valuess['id']){
								    				foreach($value as $modata){
											    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											    		$printer->feed(1);
										    		}
										    	}
									    	}
										    $printer->setTextSize(2, 1);
									    	$total_items_normal ++ ;
									    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
									    	$kot_no_string .= $valuess['kot_no'].",";
								    	}
								    	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
									    $printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->cut();
										$printer->feed(2);
									    // Close printer //
									}
								}
							    $printer->close();
								$kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."' ");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}

					}
					else{ // 45 space code starts from here

							try {
								if($printtype == 'Network'){
									$connector = new NetworkPrintConnector($printername, 9100);
								} else if($printtype == 'Windows'){
								 	$connector = new WindowsPrintConnector($printername);
								} else {
								 	$connector = '';
								}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	// $printer->text('45 Spacess');
					    		$printer->feed(1);
							   	for($i =1; $i<= $kot_copy; $i++){
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->text($infoss[0]['location']);
								    $printer->feed(1);
								    $printer->text($description);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setTextSize(1, 1);
								    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->feed(1);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								    $printer->text("Tbl No  KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   	$printer->text(" ".$infoss[0]['table_id']."       ".$nvalues[0]['kot_no']."      ".$infoss[0]['waiter_id']."    ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
								    $printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $kot_no_string = '';
								    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
								    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
								    	$printer->feed(1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
										$total_amount_normal = 0;
								    	foreach($nvalues as $keyss => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
									    	if($nvalue['message'] != ''){
									    		$printer->setTextSize(1, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
									    	}
									    	$printer->feed(1);
									    	if($modifierdata != array()){
										    	foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$value['code']."' ")->row['decimal_mesurement'];
															if ($decimal_mesurement == 1) {
																$qty = (int)$modata['qty'];
															} else {
																$qty = (int)$modata['qty'];
															}
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
							    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
							    			$kot_no_string .= $nvalue['kot_no'].",";
									    }
							    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
								    
									    $printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
								    	$printer->text("T.I.: ".str_pad($total_items_normal,5)."T.Q.:".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
							    		$printer->feed(1);
							    		$printer->setTextSize(2, 1);
							    		$printer->text("G.Total :".$total_g );
							    		$printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->cut();
										$printer->feed(2);
								    } else { 
									    $printer->text("Qty      Description");
									    $printer->feed(1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($nvalues as $keyss => $valuess){
									    	$printer->setTextSize(2, 1);
									    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25));
									    	if($valuess['message'] != ''){
									    		$printer->setTextSize(1, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($valuess['message'],25).")");
									    	}
									    	$printer->feed(1);
								    		foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $valuess['id']){
								    				foreach($value as $modata){
											    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25));
											    		$printer->feed(1);
										    		}
										    	}
									    	}
										    $printer->setTextSize(2, 1);
									    	$total_items_normal ++ ;
									    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
									    	$kot_no_string .= $valuess['kot_no'].",";
								    	}
								    	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
									    $printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->cut();
										$printer->feed(2);
									    // Close printer //
									}
								}
							    $printer->close();
								$kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."' ");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
					}
				}
			}

			if($infos_liqurnormal){
				foreach($infos_liqurnormal as $nkeys => $nvalues){
					$printtype = '';
				 	$printername = '';
				 	$description = '';
				 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];


				 	$printername = $this->user->getPrinterName();
				 	$printertype = $this->user->getPrinterType();
				 	
				 	if($printertype != ''){
				 		$printtype = $printertype;
				 	} else {
				 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
							$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
				 		}
					}

					if ($printername != '') {
						$printname = $printername;
					} else {
						if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
							$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
						}
					}

					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
				 
					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
			 		if($printerModel==0){


				 			try {
				 			if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    $printer = new Printer($connector);
						    $printer->selectPrintMode(32);
						   	$printer->setEmphasis(true);
						   	$printer->setTextSize(2, 1);
						   
						   	$printer->setJustification(Printer::JUSTIFY_CENTER);
						   	for($i=1; $i<= $kot_copy; $i++){
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							    $printer->text("Tbl No    BOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							    $printer->text("----------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_liquor_normal = 0;
								    $total_quantity_liquor_normal = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
								    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
								    	}
								    	$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_liquor_normal ++;
								    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								    // Close printer //
								}
							}
						    $printer->close();
						    $kot_no_string = rtrim($kot_no_string, ',');
						    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}

			 		} else{ // 45 space code starts from here
				 			try {
				 			if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    $printer = new Printer($connector);
						    $printer->selectPrintMode(32);
						   	$printer->setEmphasis(true);
						   	$printer->setTextSize(2, 1);
						   
						   	$printer->setJustification(Printer::JUSTIFY_CENTER);
						   	// $printer->text('45 Spacess');
				    		$printer->feed(1);
						   	for($i=1; $i<= $kot_copy; $i++){
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
								$printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							    $printer->text("------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],20).")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_liquor_normal = 0;
								    $total_quantity_liquor_normal = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
								    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],25).")");
								    	}
								    	$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_liquor_normal ++;
								    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								    // Close printer //
								}
							}
						    $printer->close();
						    $kot_no_string = rtrim($kot_no_string, ',');
						    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
			 		}
				}
			}

			if($infos_cancel){
				foreach($infos_cancel as $nkeys => $nvalues){
					$printtype = '';
				 	$printername = '';
				 	$description = '';
				 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];

				 	$printername = $this->user->getPrinterName();
				 	$printertype = $this->user->getPrinterType();
				 	
				 	if($printertype != ''){
				 		$printtype = $printertype;
				 	} else {
				 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
							$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
				 		}
					}

					if ($printername != '') {
						$printname = $printername;
					} else {
						if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
							$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
						}
					}

					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
				 
					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}

				 	$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel ==0){
							try {
							   	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 2);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	$printer->text("** CANCEL KOT **");
							   	$printer->feed(1);
							   	$printer->setTextSize(2, 1);
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('h:i:s', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							    $printer->text("----------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_cancel = 0;
									$total_quantity_cancel = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
								    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$qty = explode(".", $valuess['qty']);
								    	$printer->text($qty['0']." ".wordwrap($valuess['name'],20,"\n"));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
								    	}
							    		$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_cancel ++ ;
								    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								}
								// Close printer //
							    $printer->close();
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
							if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
					}
					else{// 45 space code starts from here
						try {
							   	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 2);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	// $printer->text('45 Spacess');
				    			$printer->feed(1);
							   	$printer->text("** CANCEL KOT **");
							   	$printer->feed(1);
							   	$printer->setTextSize(2, 1);
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							    $printer->text("------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],25,"\n").")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_cancel = 0;
									$total_quantity_cancel = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
								    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$qty = explode(".", $valuess['qty']);
								    	$printer->text($qty['0']." ".wordwrap($valuess['name'],25));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],25,"\n").")");
								    	}
							    		$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_cancel ++ ;
								    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								}
								// Close printer //
							    $printer->close();
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
							if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
					}
			 		
				}
			}

			if($infos_liqurcancel){
				foreach($infos_liqurcancel as $nkeys => $nvalues){
					$printtype = '';
				 	$printername = '';
				 	$description = '';
				 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];


				 	$printername = $this->user->getPrinterName();
				 	$printertype = $this->user->getPrinterType();
				 	
				 	if($printertype != ''){
				 		$printtype = $printertype;
				 	} else {
				 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
							$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
				 		}
					}

					if ($printername != '') {
						$printname = $printername;
					} else {
						if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
							$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
						}
					}

					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}
				 
					if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
						$description = $kot_group_datas[$sub_category_id_compare]['description'];
					}

					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel ==0){
							try {
					 			if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 2);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	$printer->text("** CANCEL BOT **");
							   	$printer->feed(1);
							   	$printer->setTextSize(2, 1);
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							   	$printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							    $printer->text("----------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_liquor_cancel = 0;
								    $total_quantity_liquor_cancel = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
							    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$qty = explode(".", $valuess['qty']);
								    	$printer->text($qty['0']." ".wordwrap($valuess['name'],20,"\n"));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
								    	}
							    		$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_liquor_cancel ++ ;
								    	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								}
							    // Close printer //
							    $printer->close();
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
					}
					else{ // 45 space code starts from here

						try {
					 			if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 2);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	// $printer->text('45 Spacess');
				    			$printer->feed(1);
							   	$printer->text("** CANCEL BOT **");
							   	$printer->feed(1);
							   	$printer->setTextSize(2, 1);
							    $printer->text($infoss[0]['location']);
							    $printer->feed(1);
							    $printer->text($description);
							    $printer->feed(1);
							    $printer->setJustification(Printer::JUSTIFY_LEFT);
							    $printer->setTextSize(1, 1);
							    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
								$printer->feed(1);
							    $printer->setEmphasis(true);
							   	$printer->setTextSize(1, 1);
							    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
							    $printer->feed(1);
							    $printer->setEmphasis(false);
							   	$printer->setTextSize(1, 1);
							   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('H:i', strtotime($nvalues[0]['time_added']))."");
							    $printer->feed(1);
							   	$printer->text("------------------------------------------");
							    $printer->feed(1);
							    $printer->setEmphasis(true);
							    $kot_no_string = '';
							    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
							    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
							    	$printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_normal = 0;
									$total_quantity_normal = 0;
									$total_amount_normal = 0;
							    	foreach($nvalues as $keyss => $nvalue){
								    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
								    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
								    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
								    	if($nvalue['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($nvalue['message'],25,"\n").")");
								    	}
								    	$printer->feed(1);
								    	if($modifierdata != array()){
									    	foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $nvalue['id']){
								    				foreach($value as $modata){
											    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
												    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
											    		$printer->feed(1);
										    		}
										    	}
								    		}
								    	}
								    	$total_items_normal ++ ;
						    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
						    			$kot_no_string .= $nvalue['kot_no'].",";
								    }
						    		$total_g = $checkkot_total['ftotal'] + $checkkot_total['ltotal'];
							    
								    $printer->setTextSize(1, 1);
									$printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
							    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
						    		$printer->feed(1);
						    		$printer->setTextSize(2, 1);
						    		$printer->text("G.Total :".$total_g );
						    		$printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
							    } else {
								    $printer->text("Qty     Description");
								    $printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								    $total_items_liquor_cancel = 0;
								    $total_quantity_liquor_cancel = 0;
								    foreach($nvalues as $keyss => $valuess){
								    	$printer->setTextSize(2, 1);
							    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    	$qty = explode(".", $valuess['qty']);
								    	$printer->text($qty['0']." ".wordwrap($valuess['name'],20));
								    	if($valuess['message'] != ''){
								    		$printer->setTextSize(1, 1);
								    		$printer->feed(1);
								    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
								    	}
							    		$printer->feed(1);
							    		foreach($modifierdata as $key => $value){
							    			$printer->setTextSize(1, 1);
							    			if($key == $valuess['id']){
							    				foreach($value as $modata){
										    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20));
										    		$printer->feed(1);
									    		}
									    	}
								    	}
								    	$total_items_liquor_cancel ++ ;
								    	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
								    	$kot_no_string .= $valuess['kot_no'].",";
							    	}
							    	$printer->setTextSize(1, 1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
								    $printer->feed(2);
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->cut();
									$printer->feed(2);
								}
							    // Close printer //
							    $printer->close();
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
						} catch (Exception $e) {
						    if(isset($kot_no_string)){
							    $kot_no_string = rtrim($kot_no_string, ',');
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
							} else {
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							}
						    $this->session->data['warning'] = $printername." "."Not Working";
						}

					}
				}
			}
		}

		if($direct_bill == 1 && $bill_copy > 0){
			$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
			foreach($tests as $test){
				$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
				$testfoods[] = array(
					'tax1' => $test['tax1'],
					'amt' => $amt,
					'tax1_value' => $test['tax1_value']
				);
			}
			
			$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
			foreach($testss as $testa){
				$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
				$testliqs[] = array(
					'tax1' => $testa['tax1'],
					'amt' => $amts,
					'tax1_value' => $testa['tax1_value']
				);
			}

			$infoslb = array();
			$infosb = array();
			$flag = 0;
			$totalquantityfood = 0;
			$totalquantityliq = 0;
			$disamtfood = 0;
			$disamtliq = 0;
			$modifierdatabill = array();

			foreach ($anssb as $lkey => $result) {
				foreach($anssb as $lkeys => $results){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
						if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $results['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}
				if($result['code'] != ''){
					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$result['qty'];
					} else {
							$qty = $result['qty'];
					}
					if($result['is_liq']== 0){
						$infosb[] = array(
							'id'			=> $result['id'],
							'billno'		=> $result['billno'],
							'name'          => $result['name'],
							'rate'          => $result['rate'],
							'amt'           => $result['amt'],
							'qty'         	=> $result['qty'],
							'tax1'         	=> $result['tax1'],
							'tax2'          => $result['tax2']
						);
						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
						$totalquantityfood = $totalquantityfood + $result['qty'];
						$disamtfood = $disamtfood + $result['discount_value'];
					} else {
						$flag = 1;
						$infoslb[] = array(
							'id'			=> $result['id'],
							'billno'		=> $result['billno'],
							'name'          => $result['name'],
							'rate'          => $result['rate'],
							'amt'           => $result['amt'],
							'qty'         	=> $result['qty'],
							'tax1'         	=> $result['tax1'],
							'tax2'          => $result['tax2']
						);
						$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
						$totalquantityliq = $totalquantityliq + $result['qty'];
						$disamtliq = $disamtliq + $result['discount_value'];
					}
				}
			}

			$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
			
			if($ansb['app_order_id'] != '0'){
				$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$ansb['app_order_id']."'");
			}

			if($ansb['parcel_status'] == '0'){
				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
					$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
				} else{
					$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
				}
			}else{
				if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
					$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
				} else{
					$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
				}
			}

			$csgst=$ansb['gst']/2;
			$csgsttotal = $ansb['gst'];

			$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
			$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
			$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
			$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
			//$gtotal = ceil($gtotal);
			$gtotal = round($gtotal);



			//$onac = $this->session->data['onac'];

			// echo'<pre>';
			// print_r($onac);
			// exit;
			

			$printername = $this->user->getPrinterName();
		 	$printtype = $this->user->getPrinterType();

		 	if($printtype == '' && $printername == ''){
				$printtype = $bill_printer_type;
		 		$printername = $bill_printer_name;
		 	}else{
		 		$printtype = '';
		 		$printername = '';	
		 	}
 			
			if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
				$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
			 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
			}


			$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
			if($printerModel ==0){

				try {
			    	if($printtype == 'Network'){
				 		$connector = new NetworkPrintConnector($printername, 9100);
				 	} else if($printtype == 'Windows'){
				 		$connector = new WindowsPrintConnector($printername);
				 	} else {
				 		$connector = '';
				 	}
				    $printer = new Printer($connector);
				    $printer->selectPrintMode(32);

				   	$printer->setEmphasis(true);
				   	$printer->setTextSize(2, 1);
				   	$printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->feed(1);
				    for($i =1; $i<= $bill_copy; $i++){
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
					    $printer->feed(1);
					    $printer->setTextSize(1, 1);
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	$printer->feed(1);
					    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
							
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Name :".$ansb['cust_name']."");
						    $printer->feed(1);
						}
						else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Mobile :".$ansb['cust_contact']."");
						    $printer->feed(1);
						}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Address : ".$ansb['cust_address']."");
						    $printer->feed(1);
						}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
						    $printer->text("Gst No :".$ansb['gst_no']."");
						    $printer->feed(1);
						}else{
						    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
						    $printer->feed(1);
						    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
						    $printer->feed(1);
						}
						$printer->text(str_pad("User Id :".$ansb['login_id'],30)."K.Ref.No :".$ansb['order_id']);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	if($infosb){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_CENTER);
					   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time :".date('h:i:s'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$ansb['order_no']."");
					   		$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ansb['location'],10)." ".str_pad($ansb['t_name'],10)."".str_pad($ansb['waiter_id'],10)."".str_pad($ansb['captain_id'],10)."".$ansb['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
							$printer->feed(1);
					   	 	$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
							$total_items_normal = 0;
							$total_quantity_normal = 0;
						    foreach($infosb as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
						    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
						    	$printer->feed(1);
						    	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_normal ++ ;
				    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    }
						    $printer->text("----------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$ansb['ftotal']);
							$printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							foreach($testfoods as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ansb['fdiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
								$printer->feed(1);
							} elseif($ansb['discount'] != '0'){
								$printer->text(str_pad("",22)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",33)."SCGST :".$csgst."");
							$printer->feed(1);
							$printer->text(str_pad("",33)."CCGST :".$csgst."");
							if($ansb['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",36)."SCRG :".$ansb['staxfood']."");
									$printer->feed(1);
									$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
								} else{
									$printer->text(str_pad("",36)."SCRG :".$ansb['staxfood']."");
									$printer->feed(1);
									$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$netamountfood = ($ansb['ftotal'] - ($disamtfood));
								} else{
									$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
							$printer->setEmphasis(false);
						}
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	if($infoslb){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_CENTER);
					   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time :".date('h:i:s'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$ansb['order_no']."");
					 		$printer->feed(1);
					 		$printer->setEmphasis(true);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
					    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ansb['location'],10)." ".str_pad($ansb['t_name'],10)."".str_pad($ansb['waiter_id'],10)."".str_pad($ansb['captain_id'],10)."".$ansb['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
							$printer->feed(1);
					    	$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
							$total_items_liquor_normal = 0;
				    		$total_quantity_liquor_normal = 0;
						    foreach($infoslb as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
						    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
						    	$printer->feed(1);
						    	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
				    			$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
						    }
						    $printer->text("----------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."L.Total :".$ansb['ltotal']);
							$printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("----------------------------------------------");
							$printer->feed(1);
							foreach($testliqs as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("----------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ansb['ldiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
								$printer->feed(1);
							} elseif($ansb['ldiscount'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",35)."VAT :".$ansb['vat']."");
							$printer->feed(1);
							if($ansb['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",34)."SCRG :".$ansb['staxliq']."");
									$printer->feed(1);
									$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
								} else{
									$printer->text(str_pad("",34)."SCRG :".$ansb['staxliq']."");
									$printer->feed(1);
									$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$netamountliq = ($ansb['ltotal'] - ($disamtliq));
								} else{
									$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						}
						$printer->feed(1);
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
						if($ansb['advance_amount'] != '0.00'){
							$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
							$printer->feed(1);
						}
						$printer->setTextSize(2, 2);
						$printer->setJustification(Printer::JUSTIFY_RIGHT);
						$printer->text("GRAND TOTAL  :  ".$gtotal);
						$printer->setTextSize(1, 1);
						$printer->feed(1);
						if($ansb['dtotalvalue']!=0){
								$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
								$printer->feed(1);
							}
						$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
						if($SETTLEMENT_status == '1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
							}
						}
						if($SETTLEMENT_status=='1'){
							if($credit!='0' && $credit!=''){
								$printer->text("PAY BY: CARD");
							}
							if($online!='0' && $online!=''){
								$printer->text("PAY BY: ONLINE");
							}
							if($cash!='0' && $cash!=''){
								$printer->text("PAY BY: CASH");
							}
							if($onac!='0' && $onac!=''){
								$printer->text("PAY BY: ON.ACCOUNT");
								$printer->feed(1);
								$printer->text("Name: '".$onacname."'");
								$printer->feed(1);
								$printer->text("Contact: '".$onaccontact."'");
							}
						}
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
						$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
						$printer->feed(1);
						if($this->model_catalog_order->get_settings('TEXT1') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT1'));
							$printer->feed(1);
						}
						if($this->model_catalog_order->get_settings('TEXT2') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT2'));
							$printer->feed(1);
						}
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_CENTER);
						if($this->model_catalog_order->get_settings('TEXT3') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT3'));
						}
						$printer->feed(2);
						$printer->cut();
					    // Close printer //
					}
				    $printer->close();
				    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
					$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
				} catch (Exception $e) {
					$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
					$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
					$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
				}

			}
			else{  // 45 space code starts from here

				try {
			    	if($printtype == 'Network'){
				 		$connector = new NetworkPrintConnector($printername, 9100);
				 	} else if($printtype == 'Windows'){
				 		$connector = new WindowsPrintConnector($printername);
				 	} else {
				 		$connector = '';
				 	}
				    $printer = new Printer($connector);
				    $printer->selectPrintMode(32);
				   	$printer->setEmphasis(true);
				   	$printer->setTextSize(2, 1);
				   	$printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->feed(1);
				    // $printer->text('45 Spacess');
				    $printer->feed(1);
				    for($i =1; $i<= $bill_copy; $i++){
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
					    $printer->feed(1);
					    $printer->setTextSize(1, 1);
					    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	$printer->feed(1);
					    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
							
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
						 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
						 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
						 	$printer->feed(1);
						}
						else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Name :".$ansb['cust_name']."");
						    $printer->feed(1);
						}
						else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Mobile :".$ansb['cust_contact']."");
						    $printer->feed(1);
						}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
						    $printer->text("Address : ".$ansb['cust_address']."");
						    $printer->feed(1);
						}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
						    $printer->text("Gst No :".$ansb['gst_no']."");
						    $printer->feed(1);
						}else{
						    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
						    $printer->feed(1);
						    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
						    $printer->feed(1);
						}
						$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$ansb['order_id']);
					    $printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	if($infosb){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$ansb['order_no']."");
					   		$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
							$printer->feed(1);
					   	 	$printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
							$total_items_normal = 0;
							$total_quantity_normal = 0;
						    foreach($infosb as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
						    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
						    	$printer->feed(1);
						    	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_normal ++ ;
				    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    }
						    $printer->text("------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$ansb['ftotal']);
							$printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("------------------------------------------");
							$printer->feed(1);
							foreach($testfoods as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ansb['fdiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
								$printer->feed(1);
							} elseif($ansb['discount'] != '0'){
								$printer->text(str_pad("",22)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",25)."SCGST :".$csgst."");
							$printer->feed(1);
							$printer->text(str_pad("",25)."CCGST :".$csgst."");
							if($ansb['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",36)."SCRG :".$ansb['staxfood']."");
									$printer->feed(1);
									$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
								} else{
									$printer->text(str_pad("",36)."SCRG :".$ansb['staxfood']."");
									$printer->feed(1);
									$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$netamountfood = ($ansb['ftotal'] - ($disamtfood));
								} else{
									$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
							$printer->setEmphasis(false);
						}
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
					   	$printer->setTextSize(1, 1);
					   	if($infoslb){
					   		$printer->feed(1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infoslb[0]['billno'],6)."Time:".date('H:i'));
					   		$printer->feed(1);
					   		$printer->text("Ref no: ".$ansb['order_no']."");
					 		$printer->feed(1);
					 		$printer->setEmphasis(true);
					   		$printer->setTextSize(1, 1);
					   		$printer->setJustification(Printer::JUSTIFY_LEFT);
					    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
					    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
					    	$printer->feed(1);
					   		$printer->setTextSize(1, 1);
					   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
						    $printer->feed(1);
					   		$printer->setEmphasis(false);
						    $printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
							$printer->feed(1);
					    	$printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setEmphasis(false);
							$total_items_liquor_normal = 0;
				    		$total_quantity_liquor_normal = 0;
						    foreach($infoslb as $nkey => $nvalue){
						    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
						    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
						    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
						    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
						    	$printer->feed(1);
						    	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
				    			$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
						    }
						    $printer->text("------------------------------------------");
						    $printer->feed(1);
						    $printer->setJustification(Printer::JUSTIFY_LEFT);
						    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."L.Total :".$ansb['ltotal']);
							$printer->feed(1);
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						    $printer->text("------------------------------------------");
							$printer->feed(1);
							foreach($testliqs as $tkey => $tvalue){
								$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
						    	$printer->feed(1);
							}
							$printer->text("------------------------------------------");
							$printer->feed(1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							if($ansb['ldiscountper'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
								$printer->feed(1);
							} elseif($ansb['ldiscount'] != '0'){
								$printer->text(str_pad("",25)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
								$printer->feed(1);
							}
							$printer->text(str_pad("",35)."VAT :".$ansb['vat']."");
							$printer->feed(1);
							if($ansb['parcel_status'] == '0'){
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$printer->text(str_pad("",34)."SCRG :".$ansb['staxliq']."");
									$printer->feed(1);
									$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
								} else{
									$printer->text(str_pad("",34)."SCRG :".$ansb['staxliq']."");
									$printer->feed(1);
									$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
								}
							} else{
								if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
									$netamountliq = ($ansb['ltotal'] - ($disamtliq));
								} else{
									$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
								}
							}
							$printer->setEmphasis(true);
							$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
						    $printer->setEmphasis(false);
						   	$printer->setTextSize(1, 1);
						}
						$printer->feed(1);
						$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(true);
						if($ansb['advance_amount'] != '0.00'){
							$printer->text(str_pad("Advance Amount",20).$ansb['advance_amount']."");
							$printer->feed(1);
						}
						$printer->setTextSize(2, 2);
						$printer->setJustification(Printer::JUSTIFY_RIGHT);
						$printer->text("GRAND TOTAL  :  ".$gtotal);
						$printer->setTextSize(1, 1);
						$printer->feed(1);
						if($ansb['dtotalvalue']!=0){
								$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
								$printer->feed(1);
							}
						$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
						if($SETTLEMENT_status == '1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
							}
						}
						if($SETTLEMENT_status=='1'){
							if($credit!='0' && $credit!=''){
								$printer->text("PAY BY: CARD");
							}
							if($online!='0' && $online!=''){
								$printer->text("PAY BY: ONLINE");
							}
							if($cash!='0' && $cash!=''){
								$printer->text("PAY BY: CASH");
							}
							if($onac!='0' && $onac!=''){
								$printer->text("PAY BY: ON.ACCOUNT");
								$printer->feed(1);
								$printer->text("Name: '".$onacname."'");
								$printer->feed(1);
								$printer->text("Contact: '".$onaccontact."'");
							}
						}
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
						$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
						$printer->feed(1);
						if($this->model_catalog_order->get_settings('TEXT1') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT1'));
							$printer->feed(1);
						}
						if($this->model_catalog_order->get_settings('TEXT2') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT2'));
							$printer->feed(1);
						}
						$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_CENTER);
						if($this->model_catalog_order->get_settings('TEXT3') != ''){
							$printer->text($this->model_catalog_order->get_settings('TEXT3'));
						}
						$printer->feed(2);
						$printer->cut();
					    // Close printer //
					}
				    $printer->close();
				    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
					$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
				} catch (Exception $e) {
					$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
					$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
					$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
				}
			}
		}
		$this->request->post = array();
		$_POST = array();
		$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token']));
	}

	public function pendingbill(){
		date_default_timezone_set('Asia/Kolkata');
		$this->load->language('customer/customer');
		$this->load->model('catalog/order');
		$this->document->setTitle('BILL');
		$te = 'BILL';

		$order_id = $this->request->get['order_id'];
		$printerid = $this->request->get['printbill'];

		$printtype = $printerinfos['printer_type'];
	 	$printername = $printerinfos['printer'];

		$printtype = '';
		$printername = '';

	 	if($printtype != '' || $printername != ''){
			$printtype = $this->user->getPrinterName();
		 	$printername = $this->user->getPrinterType();	
	 	}elseif($printerid != 'default'){
			$printerinfos = $this->db->query("SELECT * FROM oc_kotgroup WHERE code = '".$printerid."' AND master_kotprint = '0'")->row;
			$printtype = $printerinfos['printer_type'];
	 		$printername = $printerinfos['printer'];
		} else {
			$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
			$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
		}

		$this->db->query("UPDATE oc_order_info SET printstatus = 0 WHERE order_id = '".$order_id."'");
		$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

		$testfood = array();
		$testliq = array();
		$testtaxvalue1food = 0;
		$testtaxvalue1liq = 0;
		$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
		foreach($tests as $test){
			$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
			$testfoods[] = array(
				'tax1' => $test['tax1'],
				'amt' => $amt,
				'tax1_value' => $test['tax1_value']
			);
		}
		
		$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
		foreach($testss as $testa){
			$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
			$testliqs[] = array(
				'tax1' => $testa['tax1'],
				'amt' => $amts,
				'tax1_value' => $testa['tax1_value']
			);
		}
		
		$infosl = array();
		$infos = array();
		$flag = 0;
		$totalquantityfood = 0;
		$totalquantityliq = 0;
		$disamtfood = 0;
		$disamtliq = 0;
		$modifierdatabill = array();
		foreach ($anss as $lkey => $result) {
			foreach($anss as $lkeys => $results){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
					if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['code'] = '';
						}
					}
				} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
					if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['qty'] = $result['qty'] + $results['qty'];
							if($result['nc_kot_status'] == '0'){
								$result['amt'] = $result['qty'] * $result['rate'];
							}
						}
					}
				}
			}

			if($result['code'] != ''){
				$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
				if ($decimal_mesurement == 0) {
						$qty = (int)$result['qty'];
				} else {
						$qty = $result['qty'];
				}
				if($result['is_liq']== 0){
					$infos[] = array(
						'id'			=> $result['id'],
						'billno'		=> $result['billno'],
						'name'          => $result['name'],
						'rate'          => $result['rate'],
						'amt'           => $result['amt'],
						'qty'         	=> $qty,
						'tax1'         	=> $result['tax1'],
						'tax2'          => $result['tax2'],
						'discount_value'=> $result['discount_value']
					);
					$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$totalquantityfood = $totalquantityfood + $result['qty'];
					$disamtfood = $disamtfood + $result['discount_value'];
				} else {
					$flag = 1;
					$infosl[] = array(
						'id'			=> $result['id'],
						'billno'		=> $result['billno'],
						'name'          => $result['name'],
						'rate'          => $result['rate'],
						'amt'           => $result['amt'],
						'qty'         	=> $qty,
						'tax1'         	=> $result['tax1'],
						'tax2'          => $result['tax2'],
						'discount_value'=> $result['discount_value']
					);
					$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$totalquantityliq = $totalquantityliq + $result['qty'];
					$disamtliq = $disamtliq + $result['discount_value'];
				}
			}
		}
		$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
		if($locationData->num_rows > 0){
			$locationData = $locationData->row;
			if($locationData['kot_different'] == 1){
				$kot_different = 1;
			} else {
				$kot_different = 0;
			}
			$kot_copy = $locationData['kot_copy'];
			$bill_copy = $locationData['bill_copy'];
			$direct_bill = $locationData['direct_bill'];
		} else{
			$kot_different = 0;
			$kot_copy = 1;
			$bill_copy = 1;
			$direct_bill = 0;
		}
		
		if($ans['app_order_id'] != '0'){
			$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$ans['app_order_id']."'");
		}

		if($ans['parcel_status'] == '0'){
			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
			} else{
				$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
			}
		}else{
			if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
				$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
			} else{
				$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
			}
		}

		$csgst=$ans['gst']/2;
		$csgsttotal = $ans['gst'];

		$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
		$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
		$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
		$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
		$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
		$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
		$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
		$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
		$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
		$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
		$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
		$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
		//$gtotal = ceil($gtotal);
		$gtotal = round($gtotal);


		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
		if($printerModel ==0){

			try {
		    	if($printtype == 'Network'){
			 		$connector = new NetworkPrintConnector($printername, 9100);
			 	} else if($printtype == 'Windows'){
			 		$connector = new WindowsPrintConnector($printername);
			 	} else {
			 		$connector = '';
			 	}
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	for($i = 1; $i <= $bill_copy; $i++){
				   	$printer->setTextSize(2, 1);
				   	$printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
				    $printer->feed(1);
				    $printer->setTextSize(1, 1);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
				    $printer->feed(1);
				    $printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->setJustification(Printer::JUSTIFY_LEFT);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
					if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
						
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
						$printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
						$printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Name :".$ans['cust_name']."");
					    $printer->feed(1);
					}
					else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
					    $printer->text("Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}else{
					    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
				   	if($infos){
				   		$printer->feed(1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time :".date('H:i'));
				   		$printer->feed(1);
				   		$printer->text("Ref no: ".$ans['order_no']."");
				   		$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
				    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
				    	$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
					    $printer->feed(1);
				   		$printer->setEmphasis(false);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
						$printer->feed(1);
				   	 	$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setEmphasis(false);
						$total_items_liquor_normal = 0;
					    $total_quantity_liquor_normal = 0;
					    foreach($infos as $nkey => $nvalue){
					    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
					    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
					    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
					    	$printer->feed(1);
				    	 	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
					    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];

					    }
					    $printer->text("----------------------------------------------");
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."F.Total :".$ans['ftotal']);
					    $printer->feed(1);
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						foreach($testfoods as $tkey => $tvalue){
							$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
					    	$printer->feed(1);
						}
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						if($ans['fdiscountper'] != '0'){
							$printer->text(str_pad("",25)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
							$printer->feed(1);
						} elseif($ans['discount'] != '0'){
							$printer->text(str_pad("",22)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
							$printer->feed(1);
						}
						$printer->text(str_pad("",33)."SCGST :".$csgst."");
						$printer->feed(1);
						$printer->text(str_pad("",33)."CCGST :".$csgst."");
						$printer->feed(1);
						if($ans['parcel_status'] == '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}else{
								$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}
						} else{
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$netamountfood = ($ans['ftotal'] - ($disamtfood));
							} else{
								$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
							}
						}
						$printer->setEmphasis(true);
						$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
						$printer->setEmphasis(false);
					}
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
				   	if($infosl){
				   		$printer->feed(1);
				   		$printer->setJustification(Printer::JUSTIFY_CENTER);
				   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('H:i'));
				   		$printer->feed(1);
				   		$printer->text("Ref no: ".$ans['order_no']."");
				 		$printer->feed(1);
				 		$printer->setEmphasis(true);
				   		$printer->setTextSize(1, 1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
				    	$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
					    $printer->feed(1);
				   		$printer->setEmphasis(false);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
						$printer->feed(1);
				    	$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setEmphasis(false);
						$total_items_liquor_normal = 0;
					    $total_quantity_liquor_normal = 0;
					    foreach($infosl as $nkey => $nvalue){
					    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
					    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
					    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
					    	$printer->feed(1);
				    	 	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
								    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
					    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
					    }
					    $printer->text("----------------------------------------------");
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."L.Total :".$ans['ltotal']);
					    $printer->feed(1);
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						foreach($testliqs as $tkey => $tvalue){
							$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
					    	$printer->feed(1);
						}
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						if($ans['ldiscountper'] != '0'){
							$printer->text(str_pad("",25)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
							$printer->feed(1);
						} elseif($ans['ldiscount'] != '0'){
							$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
							$printer->feed(1);
						}
						$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
						$printer->feed(1);
						if($ans['parcel_status'] == '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
								$printer->feed(1);
								$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
							} else{
								$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
								$printer->feed(1);
								$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
							}
						} else{
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
							} else{
								$netamountliq = ($ans['ltotal'] - ($disamtliq));
							}
						}
						$printer->setEmphasis(true);
						$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					}
					$printer->feed(1);
					$printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(true);
					if($ans['advance_amount'] != '0.00'){
						$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
						$printer->feed(1);
					}
					$printer->setTextSize(2, 2);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->text("GRAND TOTAL  :  ".$gtotal);
					$printer->setTextSize(1, 1);
					$printer->feed(1);
					if($ansb['dtotalvalue']!=0){
							$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
							$printer->feed(1);
						}
					$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
					if($SETTLEMENT_status == '1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
							}
						}
						if($SETTLEMENT_status=='1'){
							if($credit!='0' && $credit!=''){
								$printer->text("PAY BY: CARD");
							}
							if($online!='0' && $online!=''){
								$printer->text("PAY BY: ONLINE");
							}
							if($cash!='0' && $cash!=''){
								$printer->text("PAY BY: CASH");
							}
							if($onac!='0' && $onac!=''){
								$printer->text("PAY BY: ON.ACCOUNT");
								$printer->feed(1);
								$printer->text("Name: '".$onacname."'");
								$printer->feed(1);
								$printer->text("Contact: '".$onaccontact."'");
							}
						}
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
				    $printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(false);
				   	$printer->setTextSize(1, 1);
					$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
					$printer->feed(1);
					if($this->model_catalog_order->get_settings('TEXT1') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT1'));
						$printer->feed(1);
					}
					if($this->model_catalog_order->get_settings('TEXT2') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT2'));
						$printer->feed(1);
					}
					$printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_CENTER);
					if($this->model_catalog_order->get_settings('TEXT3') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT3'));
					}
					$printer->feed(2);
					$printer->cut();
				}
			    // Close printer //
			    $printer->close();
			    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
			    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
			} catch (Exception $e) {
			    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
			    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
			    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
			}
			
		}
		else{  // 45 space code starts from here


			try {
		    	if($printtype == 'Network'){
			 		$connector = new NetworkPrintConnector($printername, 9100);
			 	} else if($printtype == 'Windows'){
			 		$connector = new WindowsPrintConnector($printername);
			 	} else {
			 		$connector = '';
			 	}
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);

			   	$printer->setEmphasis(true);
			   	// $printer->text('45 Spacess');
				$printer->feed(1);
			   	for($i = 1; $i <= $bill_copy; $i++){
				   	$printer->setTextSize(2, 1);
				   	$printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
				    $printer->feed(1);
				    $printer->setTextSize(1, 1);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
				    $printer->feed(1);
				    $printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->setJustification(Printer::JUSTIFY_LEFT);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
					if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
						
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
						$printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
						$printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Name :".$ans['cust_name']."");
					    $printer->feed(1);
					}
					else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
					    $printer->text("Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}else{
					    $printer->text(str_pad("Name : ".$ans['cust_name'],20)."Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					    $printer->text(str_pad("Address : ".$ans['cust_address'],20)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
				   	if($infos){
				   		$printer->feed(1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],5)."Time:".date('H:i'));
				   		$printer->feed(1);
				   		$printer->text("Ref no: ".$ans['order_no']."");
				   		$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
				    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
				    	$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
					    $printer->feed(1);
				   		$printer->setEmphasis(false);
					    $printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
						$printer->feed(1);
				   	 	$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setEmphasis(false);
						$total_items_liquor_normal = 0;
					    $total_quantity_liquor_normal = 0;
					    foreach($infos as $nkey => $nvalue){
					    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
					    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
					    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
					    	$printer->feed(1);
				    	 	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
									    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
					    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];

					    }
					    $printer->text("------------------------------------------");
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."F.Total :".$ans['ftotal']);
					    $printer->feed(1);
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					    $printer->text("------------------------------------------");
						$printer->feed(1);
						foreach($testfoods as $tkey => $tvalue){
							$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
					    	$printer->feed(1);
						}
						$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						if($ans['fdiscountper'] != '0'){
							$printer->text(str_pad("",20)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
							$printer->feed(1);
						} elseif($ans['discount'] != '0'){
							$printer->text(str_pad("",22)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
							$printer->feed(1);
						}
						$printer->text(str_pad("",25)."SCGST :".$csgst."");
						$printer->feed(1);
						$printer->text(str_pad("",25)."CCGST :".$csgst."");
						$printer->feed(1);
						if($ans['parcel_status'] == '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$printer->text(str_pad("",25)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}else{
								$printer->text(str_pad("",25)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}
						} else{
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$netamountfood = ($ans['ftotal'] - ($disamtfood));
							} else{
								$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
							}
						}
						$printer->setEmphasis(true);
						$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
						$printer->setEmphasis(false);
					}
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
				   	if($infosl){
				   		$printer->feed(1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infosl[0]['billno'],6)."Time :".date('H:i'));
				   		$printer->feed(1);
				   		$printer->text("Ref no: ".$ans['order_no']."");
				 		$printer->feed(1);
				 		$printer->setEmphasis(true);
				   		$printer->setTextSize(1, 1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
				    	$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
					    $printer->feed(1);
				   		$printer->setEmphasis(false);
					    $printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
						$printer->feed(1);
				    	$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setEmphasis(false);
						$total_items_liquor_normal = 0;
					    $total_quantity_liquor_normal = 0;
					    foreach($infosl as $nkey => $nvalue){
					    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
					    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
					    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
					    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
					    	$printer->feed(1);
				    	 	if($modifierdatabill != array()){
							    	foreach($modifierdatabill as $key => $value){
						    			$printer->setTextSize(1, 1);
						    			if($key == $nvalue['id']){
						    				foreach($value as $modata){
									    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
										    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
								    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
								    		$printer->feed(1);
								    		}
								    	}
						    		}
						    	}
						    	$total_items_liquor_normal ++;
					    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
					    }
					    $printer->text("------------------------------------------");
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T quantity :".str_pad($total_quantity_liquor_normal,5)."L.Total :".$ans['ltotal']);
					    $printer->feed(1);
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					    $printer->text("------------------------------------------");
						$printer->feed(1);
						foreach($testliqs as $tkey => $tvalue){
							$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
					    	$printer->feed(1);
						}
						$printer->text("------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						if($ans['ldiscountper'] != '0'){
							$printer->text(str_pad("",20)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
							$printer->feed(1);
						} elseif($ans['ldiscount'] != '0'){
							$printer->text(str_pad("",20)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
							$printer->feed(1);
						}
						$printer->text(str_pad("",30)."VAT :".$ans['vat']."");
						$printer->feed(1);
						if($ans['parcel_status'] == '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$printer->text(str_pad("",20)."SCRG :".$ans['staxliq']."");
								$printer->feed(1);
								$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
							} else{
								$printer->text(str_pad("",20)."SCRG :".$ans['staxliq']."");
								$printer->feed(1);
								$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
							}
						} else{
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
							} else{
								$netamountliq = ($ans['ltotal'] - ($disamtliq));
							}
						}
						$printer->setEmphasis(true);
						$printer->text(str_pad("",20)."Net total :".ceil($netamountliq)."");
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					}
					$printer->feed(1);
					$printer->text("------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(true);
					if($ans['advance_amount'] != '0.00'){
						$printer->text(str_pad("Advance Amount",33).$ans['advance_amount']."");
						$printer->feed(1);
					}
					$printer->setTextSize(2, 2);
					$printer->setJustification(Printer::JUSTIFY_RIGHT);
					$printer->text(str_pad("",25)."GRAND TOTAL  :".$gtotal);
					$printer->setTextSize(1, 1);
					$printer->feed(1);
					if($ansb['dtotalvalue']!=0){
							$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
							$printer->feed(1);
						}
					$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
					if($SETTLEMENT_status =='1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
							}
						}
						if($SETTLEMENT_status=='1'){
							if($credit!='0' && $credit!=''){
								$printer->text("PAY BY: CARD");
							}
							if($online!='0' && $online!=''){
								$printer->text("PAY BY: ONLINE");
							}
							if($cash!='0' && $cash!=''){
								$printer->text("PAY BY: CASH");
							}
							if($onac!='0' && $onac!=''){
								$printer->text("PAY BY: ON.ACCOUNT");
								$printer->feed(1);
								$printer->text("Name: '".$onacname."'");
								$printer->feed(1);
								$printer->text("Contact: '".$onaccontact."'");
							}
						}
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
				    $printer->text("------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(false);
				   	$printer->setTextSize(1, 1);
					$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
					$printer->feed(1);
					if($this->model_catalog_order->get_settings('TEXT1') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT1'));
						$printer->feed(1);
					}
					if($this->model_catalog_order->get_settings('TEXT2') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT2'));
						$printer->feed(1);
					}
					$printer->text("------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_CENTER);
					if($this->model_catalog_order->get_settings('TEXT3') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT3'));
					}
					$printer->feed(2);
					$printer->cut();
				}
			    // Close printer //
			    $printer->close();
			    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
			    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
			} catch (Exception $e) {
			    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
			    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
			    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
			}

		}
		$this->request->post = array();
		$_POST = array();
		$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}

	public function prints() {
		//$this->log->write('---------------------------Kot Print Start--------------------------');
		//$this->log->write('User Name : ' .$this->user->getUserName());
		
		// echo'<pre>';
		// print_r($this->request->post);
		// exit;
		$this->load->language('customer/customer');
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$order_id = 0;
		if(isset($this->session->data['cust_id'])){
			$cust_id = $this->session->data['cust_id'];
		} else{
			$cust_id = '';
		}
		// echo'<pre>';print_r($this->session->data);
		// exit;

		if(isset($this->session->data['msr_no'])){
			$msr_no = $this->session->data['msr_no'];
		} else{
			$msr_no = '';
		}
		// echo $msr_no;
		// exit;
		//$cust_id = $this->session->data['cust_id'];
		//$msr_no = $this->session->data['msr_no'];
		
		$msr_bal = $this->model_catalog_msr_recharge->pre_bal($msr_no,$cust_id);
		
		if($msr_bal < $this->request->post['grand_total'] && $msr_no != ''){
			//echo'PLEASE RECHARGE CARD BALANCE IS ZERO';
			$json = array();
			$json = array(
				'status' => 0,
				'msr_bal' => $msr_bal,
			);
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
				
		} else {

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && (!isset($this->request->get['checkkot']))) {
			$order_id = $this->model_catalog_order->addOrder($this->request->post);
		} else {
			$order_id = $this->request->get['order_id'];
		}
		//$this->log->write('---------------------------After Model Called!-----------------------------');
		//$this->log->write('order id'.$order_id);


		if(isset($this->session->data['compl_status'])){
			$compl_status = $this->session->data['compl_status'];
			$complimentary_reason = $this->session->data['complimentary_reason'];
			$order_id = $order_id;
		} else {
			$compl_status = 0;
			$complimentary_reason = '';
		}

		// echo'<pre>';
		// print_r($compl_status);
		// exit;
		if($compl_status == 1){
			$json = array();
			$json = array(
				'status' => 2,
				'order_id' => $order_id,
				'compl_status' => $compl_status,
				'complimentary_reason' => $complimentary_reason,
			);

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));	
		} else {

		if ($order_id || isset($this->request->get['order_id'])) {

			$te = 'BILL';
			if(isset($this->request->get['edit']) && ($this->request->get['edit'] == 1)){
				$edit = $this->request->get['edit'];
			} else{
				$edit = '0';
			}
			if(isset($this->request->get['checkkot'])){
				$getcheckkot = $this->request->get['checkkot'];
			} else{
				$getcheckkot = '0';
			}
			$checkkot = array();
			// echo '<pre>';
			// print_r($order_id);
			// exit;
			$anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY id,subcategoryid")->rows;
			$anss_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
			// echo '<pre>';
			// print_r($anss_2);
			// exit;
			$liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
			$user_id= $this->user->getId();
			$user = $this->db->query("SELECT `checkkot` FROM oc_user WHERE user_id = '".$user_id."'")->row;
			if($getcheckkot == '1' && $user['checkkot'] == '1'){
				$checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
			}
			
			$infos_normal = array();
			$infos_cancel = array();
			$infos_liqurnormal = array();
			$infos_liqurcancel = array();
			$infos = array();
			$modifierdata = array();
			$allKot = array();
			$ans = $this->db->query("SELECT `order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
			//$orderitem_time = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' ORDER BY `id` DESC LIMIT 1")->row['time'];
			foreach ($ans as $resultt) {
				$infoss[] = array(
					'order_id'   => $resultt['order_id'],
					'time_added'  => $resultt['time_added'],
					'date_added'  => $resultt['date_added'],
					'location'  => $resultt['location'],
					'location_id' => $resultt['location_id'],
					't_name'    => $resultt['t_name'],
					'table_id'  => $resultt['table_id'],
					'waiter'    => $resultt['waiter'],
					'waiter_id'    => $resultt['waiter_id'],
					'captain'   => $resultt['captain'],
					'captain_id'   => $resultt['captain_id'],
					'item_quantity'   => $resultt['item_quantity'],
					'total_items'   => $resultt['total_items'],
					'login_id' => $resultt['login_id'],
					'login_name' => $resultt['login_name'],
					'person' => $resultt['person'],
					'ftotal' => $resultt['ftotal'],
					'ltotal' => $resultt['ltotal'],
					'grand_total' => $resultt['grand_total'],
					'bill_status' => $resultt['bill_status'],

				);
			}

			$master_kot =  $this->db->query("SELECT `master_kot` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
			if($master_kot->num_rows > 0){
				$master_kot = $master_kot->row['master_kot'];
			} else {
				$master_kot = 0;
			}

			if($master_kot == 1){
				$this->log->write("Gooo Toooo Master Kot Printssss");
				$this->master_kotprint($order_id);
			} else {
				$this->log->write("back to kot printsssss");

				// echo'<pre>';
				// print_r($infoss);
				// exit();
				$kot_group_datas = array();
				$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup WHERE master_kotprint = '0'")->rows;
				

				foreach($printerinfos as $pkeys => $pvalues){
					if($pvalues['subcategory'] != ''){
						$subcategoryid_exp = explode(',', $pvalues['subcategory']);
						foreach($subcategoryid_exp as $skey => $svalue){
							$kot_group_datas[$svalue] = $pvalues;
						}
					}
				}
				// echo'<pre>';
				// print_r($kot_group_datas);
				// exit();
				
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $resultss){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $resultss['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}

					if($result['code'] != ''){
						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
						}

						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}

						
						$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
							'id'				=> $result['id'],
							'name'           	=> $result['name'],
							'qty'         		=> $qty,
							'code'         		=> $result['code'],
							'amt'				=> $result['amt'],
							'rate'				=> $result['rate'],
							'message'         	=> $result['message'],
							'subcategoryid'		=> $result['subcategoryid'],
							'kot_no'            => $result['kot_no'],
							'time_added'        => $result['time'],
						);

						// echo'<pre>';
						// print_r($result);
						// exit;
						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
						$flag = 0;
					}
				}



				foreach ($checkkot as $lkey => $result) {
					foreach($checkkot as $lkeys => $resultss){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $resultss['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					if($result['code'] != ''){
						$allKot[] = array(
							'order_id'			=> $result['order_id'],
							'id'				=> $result['id'],
							'name'           	=> $result['name'],
							'qty'         		=> $result['qty'],
							'rate'         		=> $result['rate'],
							'amt'         		=> $result['amt'],
							'subcategoryid'		=> $result['subcategoryid'],
							'message'         	=> $result['message'],
							'kot_no'            => $result['kot_no'],
							'time_added'        => $result['time'],
						);
						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
						$flag = 0;
					}
				}

				// echo'<pre>';
				// print_r($kot_group_datas);
				// echo'<br/>';
				// print_r($infos_normal);
				// exit;
				
				$total_items_normal = 0;
				$total_quantity_normal = 0;
				foreach ($anss_1 as $result) {
					if($result['qty'] > $result['pre_qty']) {
						$tem = $result['qty'] - $result['pre_qty'];
						$ans=abs($tem);
						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
						}

						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$ans;
						} else {
								$qty = $ans;
						}
						$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
							'name'          	=> $result['name'],
							'qty'         		=> $qty,
							'code'         		=> $result['code'],
							'amt'				=> $result['amt'],
							'rate'				=> $result['rate'],
							'message'       	=> $result['message'],
							'subcategoryid'		=> $result['subcategoryid'],
							'kot_no'        	=> $result['kot_no'],
							'time_added'        => $result['time'],
						);
						$total_items_normal ++;
						$total_quantity_normal = $total_quantity_normal + $ans;
						$flag = 0;
					}
				}

				foreach ($anss_2 as $lkey => $result) {
					foreach($anss as $lkeys => $resultss){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
							if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $resultss['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}

					if($result['code'] != ''){
						if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
							$kot_group_datas[$result['subcategoryid']]['code'] = 1;
						}
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}
						if($qty > 0){
							$infos_cancel[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
								'id'				=> $result['id'],
								'name'          	=> $result['name']." (cancel)",
								'qty'         		=> $result['qty'],
								'rate'         		=> $result['rate'],
								'amt'				=> $result['amt'],
								'message'       	=> $result['message'],
								'subcategoryid'		=> $result['subcategoryid'],
								'kot_no'        	=> $result['kot_no'],
								'time_added'        => $result['time'],
							);
						}
						$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
						$flag = 0;
					}
				}

				foreach ($liqurs as $lkey => $liqur) {
					foreach($liqurs as $lkeys => $liqurss){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
							if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
								if($liqur['parent'] == '0'){
									$liqur['code'] = '';
								}
							}
						} elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
							if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
								if($liqur['parent'] == '0'){
									$liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
									if($liqur['nc_kot_status'] == '0'){
										$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
									}
								}
							}
						}
					}

					if($liqur['code'] != ''){
						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
						}
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$liqur['qty'];
						} else {
								$qty = $liqur['qty'];
						}

						$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
							'id'				=> $liqur['id'],
							'name'           	=> $liqur['name'],
							'qty'         		=> $qty,
							'amt'				=> $liqur['amt'],
							'rate'				=> $liqur['rate'],
							'message'         	=> $liqur['message'],
							'subcategoryid'		=> $liqur['subcategoryid'],
							'kot_no'            => $liqur['kot_no'],
							'time_added'        => $liqur['time'],
						);
						// echo "<pre>";
						// print_r($liqur['id']);
						$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
						$flag = 0;
					}
				}
				// echo'</br>';
				// print_r($modifierdata);
				// exit;

				foreach ($liqurs_1 as $liqur) {
					if($liqur['qty'] > $liqur['pre_qty']) {
						$tem = $liqur['qty'] - $liqur['pre_qty'];
						$ans=abs($tem);
						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
						}

						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$ans;
						} else {
								$qty = $ans;
						}

						$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
							'name'          	=> $liqur['name'],
							'qty'         		=> $qty,
							'amt'				=> $liqur['amt'],
							'rate'				=> $liqur['rate'],
							'message'       	=> $liqur['message'],
							'subcategoryid'		=> $liqur['subcategoryid'],
							'kot_no'        	=> $liqur['kot_no'],
							'time_added'        => $liqur['time'],
						);
						$total_items_liquor_normal ++;
						$total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
						$flag = 0;
					}
				}

				$total_items_liquor_cancel = 0;
				$total_quantity_liquor_cancel = 0;
				foreach ($liqurs_2 as $lkey => $liqur) {
					foreach($liqurs_2 as $lkeys => $liqurs){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1'){
							if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
								if($liqur['parent'] == '0'){
									$liqur['code'] = '';
								}
							}
						} elseif ($liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1') {
							if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
								if($liqur['parent'] == '0'){
									$liqur['qty'] = $liqur['qty'] + $liqurs['qty'];
									if($liqur['nc_kot_status'] == '0'){
										$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
									}
								}
							}
						}
					}

					if($liqur['code'] != ''){
						if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
							$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
						}

						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$liqur['qty'];
						} else {
								$qty = $liqur['qty'];
						}
						$infos_liqurcancel[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
							'id'      			=> $liqur['id'],
							'name'          	=> $liqur['name']."(cancel)",
							'qty'         		=> $qty,
							'amt'         		=> $liqur['amt'],
							'rate'         		=> $liqur['rate'],
							'message'       	=> $liqur['message'],
							'subcategoryid'		=> $liqur['subcategoryid'],
							'kot_no'        	=> $liqur['kot_no'],
							'time_added'        => $liqur['time'],
						);
						//$text = "Cancelled Bot <br>Orderid :".$order_id.", Item Name :".$result['name'].", Qty :".$result['qty'].", Amt :".$result['amt'].", Kot_no :".$result['kot_no'];
						//echo $text;
						$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
						$total_items_liquor_cancel ++;
						$total_quantity_liquor_cancel = $liqur['qty'];
						$flag = 0;
					}
				}

				if(!isset($flag)){
					$anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
					$total_items = 0;
					$total_quantity = 0;
					foreach ($anss as $result) {
						$infos[] = array(
							'name'           	=> $result['name'],
							'qty'         	 	=> $result['qty'],
							'amt'				=> $result['amt'],
							'rate'				=> $result['rate'],
							'message'        	=> $result['message'],
							'subcategoryid'		=> $result['subcategoryid'],
							'kot_no'        	=> $result['kot_no'],
							'time_added' 		=> $result['time'],
						);
						$total_items ++;
						$total_quantity = $total_quantity + $result['qty'];
					}
				}
				$this->db->query("UPDATE " . DB_PREFIX . "order_items SET `kot_status` = '1', `is_new` = '1' WHERE  order_id = '".$order_id."'");
			}

			$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_type`, `bill_printer_name`,`master_kot` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
			if($locationData->num_rows > 0){
				$locationData = $locationData->row;
				if($locationData['kot_different'] == 1){
					$kot_different = 1;
				} else {
					$kot_different = 0;
				}
				$master_kot = $locationData['master_kot'];
				$kot_copy = $locationData['kot_copy'];
				$bill_copy = $locationData['bill_copy'];
				$direct_bill = $locationData['direct_bill'];
				$bill_printer_type = $locationData['bill_printer_type'];
				$bill_printer_name = $locationData['bill_printer_name'];
			} else{
				$master_kot = 0;
				$kot_different = 0;
				$kot_copy = 1;
				$direct_bill = 0;
				$bill_printer_type = '';
				$bill_printer_name = '';
			}

		
			// $cust_id = $this->session->data['c_id'];
			// $msr_no = $this->session->data['msr_no'];

			// $msr_bal = $this->model_catalog_msr_recharge->pre_bal($msr_no,$cust_id);
			// echo'<pre>';
			// print_r($msr_bal);
			// exit;
			


			$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

			if(($LOCAL_PRINT == 0 && $direct_bill == 0 && $edit == '0' && $master_kot == 0) || $kot_copy > 0){
					if($kot_copy > 0 && $LOCAL_PRINT == 0){

						if($infos_normal){
							// echo'<pre>';
							// 	print_r($infos_normal);
							// 	exit;
							foreach($infos_normal as $nkeys => $nvalues){
								// echo '<pre>';
								// print_r($nvalues);
								// echo '<br />';
								// echo '<pre>';
								// print_r($nvalues[0]['time_added']);
								// echo '<br />';
								// echo '<pre>';
								// print_r(date('H:i:s', strtotime($nvalues[0]['time_added'])));
								// echo '<br />';
								// exit;
								// echo'<pre>';
								// print_r($infos_normal);
								// exit;
							 	$printtype = '';
							 	$printername = '';
							 	$description = '';
							 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
							 	$printername = $this->user->getPrinterName();
							 	$printertype = $this->user->getPrinterType();
							 	
							 	if($printertype != ''){
							 		$printtype = $printertype;
							 	} else {
							 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
										$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
							 		}
								}

								if ($printername != '') {
									$printname = $printername;
								} else {
									if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
										$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
									}
								}

								if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
									$description = $kot_group_datas[$sub_category_id_compare]['description'];
								}

								$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								if($printerModel ==0){

									try {
								 		if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} elseif($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
									 		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
									 		if($printerModel == 0){
										 		if($kot_different == 0){
											    	//echo"innnn kot";exit;
												    $printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												    for($i = 1; $i <= $kot_copy; $i++){
												    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
														   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											    			$printer->feed(1);
											    		}
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
														$printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $kot_no_string = '';
													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

													    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													    	$printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
															$total_amount_normal = 0;
													    	foreach($nvalues as $keyss => $nvalue){
														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
														    		
														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
														    	if($nvalue['message'] != ''){
														    		$printer->setTextSize(2, 2);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
														    	}
														    	$printer->feed(1);
														    	if($modifierdata != array()){
															    	foreach($modifierdata as $key => $value){
														    			$printer->setTextSize(1, 1);
														    			if($key == $nvalue['id']){
														    				foreach($value as $modata){
																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																	    		$printer->feed(1);
																    		}
																    	}
														    		}
														    	}
														    	$total_items_normal ++ ;
												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
												    			$kot_no_string .= $nvalue['kot_no'].",";
														    }
												    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
														    $printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
													    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
												    		$printer->feed(1);
												    		$printer->setTextSize(2, 1);
												    		$printer->text("G.Total :".$total_g );
												    		$printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
													    } else {
															$printer->text("Qty     Description");
													    	$printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
														    foreach($nvalues as $keyss => $valuess){
														    	$printer->setTextSize(2, 1);
														    	//$printer->setTextSize(2, 2);
													    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
														    	if($valuess['message'] != ''){
														    		$printer->setTextSize(2, 1);
														    		//$printer->setTextSize(2, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
														    	$total_items_normal ++ ;
														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
														    	$kot_no_string .= $valuess['kot_no'].",";
													    	}

													    	$printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												} else{
													$printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												   	for($i = 1; $i <= $kot_copy; $i++){
												   		//echo'innn2';exit;
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
													    $printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $kot_no_string = '';
													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
														   	$printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
													    	$total_amount_normal = 0;
													    	foreach($nvalues as $keyss => $nvalue){
														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
														    	if($nvalue['message'] != ''){
														    		$printer->setTextSize(1, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
														    	}
														    	$printer->feed(1);
														    	if($modifierdata != array()){
															    	foreach($modifierdata as $key => $value){
														    			$printer->setTextSize(1, 1);
														    			if($key == $nvalue['id']){
														    				foreach($value as $modata){
																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																	    		$printer->feed(1);
																    		}
																    	}
														    		}
														    	}
														    	$total_items_normal ++ ;
												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
												    			$kot_no_string .= $nvalue['kot_no'].",";
														    }
													    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

														    $printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
													    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
												    		$printer->feed(1);
												    		$printer->setTextSize(2, 1);
												    		$printer->text("G.Total :".$total_g );
												    		$printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
													    } else {
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
														    foreach($nvalues as $keyss => $valuess){
														    	//echo'<pre>';print_r($valuess);exit;
														    	$printer->setTextSize(2, 1);
														    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
														    	if($valuess['message'] != ''){
														    		//$printer->setTextSize(1, 1);
														    		$printer->setTextSize(2, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
														    	$total_items_normal ++ ;
														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
														    	$kot_no_string .= $valuess['kot_no'].",";
													    	}
															$printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
														foreach($nvalues as $keyss => $valuess){
															$valuess['qty'] = round($valuess['qty']);
															for($i = 1; $i <= $valuess['qty']; $i++){
																$total_items_normal = 0;
																$total_quantity_normal = 0;
																$qtydisplay = 1;
																//echo $valuess['qty'];
																$printer = new Printer($connector);
															    $printer->selectPrintMode(32);
															   	$printer->setEmphasis(true);
															   	$printer->setTextSize(2, 1);
															   	$printer->setJustification(Printer::JUSTIFY_CENTER);
															    $printer->text($infoss[0]['location']);
															    $printer->feed(1);
															    $printer->text($description);
															    $printer->feed(1);
															    $printer->setTextSize(2, 1);
															   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
															    $printer->text("Tbl.No ".$table_id);
															    $printer->feed(1);
															    $printer->setJustification(Printer::JUSTIFY_LEFT);
															    $printer->setTextSize(1, 1);
															    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
																$printer->feed(1);
															    $printer->setEmphasis(true);
															   	$printer->setTextSize(1, 1);
															    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
															    $printer->feed(1);
															    $printer->setEmphasis(false);
															   	$printer->setTextSize(1, 1);
															   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
															    $printer->feed(1);
															    $printer->text("----------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(true);
															    $printer->text("Qty     Description");
															    $printer->feed(1);
															    $printer->text("----------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(false);
														    	$printer->setTextSize(2, 1);
													    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,30,"\n"));
														    	if($valuess['message'] != ''){
														    		$printer->feed(1);
														    		$printer->setTextSize(2, 1);
														    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
															    $printer->setTextSize(2, 1);
														    	$total_items_normal ++ ;
														    	$total_quantity_normal ++;
														    	$qtydisplay ++;
														    	$printer->setTextSize(1, 1);
															    $printer->text("----------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(true);
															    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
															    $printer->feed(2);
															    $printer->setJustification(Printer::JUSTIFY_CENTER);
															    $printer->cut();
																$printer->feed(2);
															}
														}
													}
												}
											} else {
												
											}
											// Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
											$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
									    if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
										//continue;
									}
									
								} else {  // 45 space code starts from here

									try {
								 		if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} elseif($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
										 		if($kot_different == 0){
											    	//echo"innnn kot";exit;
												    $printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												   	// $printer->text('45 Spacess');
							    					$printer->feed(1);
												    for($i = 1; $i <= $kot_copy; $i++){
												    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
														   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											    			$printer->feed(1);
											    		}
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
														$printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $kot_no_string = '';
													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

													    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													    	$printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
															$total_amount_normal = 0;
													    	foreach($nvalues as $keyss => $nvalue){
														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
														    		
														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
														    	if($nvalue['message'] != ''){
														    		$printer->setTextSize(1, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
														    	}
														    	$printer->feed(1);
														    	if($modifierdata != array()){
															    	foreach($modifierdata as $key => $value){
														    			$printer->setTextSize(1, 1);
														    			if($key == $nvalue['id']){
														    				foreach($value as $modata){
																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																	    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																	    		$printer->feed(1);
																    		}
																    	}
														    		}
														    	}
														    	$total_items_normal ++ ;
												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
												    			$kot_no_string .= $nvalue['kot_no'].",";
														    }
												    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
													    
														    $printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
													    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
												    		$printer->feed(1);
												    		$printer->setTextSize(2, 1);
												    		$printer->text("G.Total :".$total_g );
												    		$printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
													    } else {
															$printer->text("Qty     Description");
													    	$printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
														    foreach($nvalues as $keyss => $valuess){
														    	$printer->setTextSize(2, 1);

													    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
														    	if($valuess['message'] != ''){
														    		$printer->setTextSize(1, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			//$printer->setTextSize(1, 1);
													    			$printer->setTextSize(2, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
														    	$total_items_normal ++ ;
														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
														    	$kot_no_string .= $valuess['kot_no'].",";
													    	}

													    	$printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												} else{
													$printer = new Printer($connector);
												    $printer->selectPrintMode(32);
												   	$printer->setEmphasis(true);
												   	$printer->setTextSize(2, 1);
												   	$printer->setJustification(Printer::JUSTIFY_CENTER);
												   	for($i = 1; $i <= $kot_copy; $i++){
												   		//echo'innn2';exit;
													    $printer->text($infoss[0]['location']);
													    $printer->feed(1);
													    $printer->text($description);
													    $printer->feed(1);
													    $printer->setTextSize(2, 1);
													   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    $printer->text("Tbl.No ".$table_id);
													    $printer->feed(1);
													    $printer->setJustification(Printer::JUSTIFY_LEFT);
													    $printer->setTextSize(1, 1);
													    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
														$printer->feed(1);
													    $printer->setEmphasis(true);
													   	$printer->setTextSize(1, 1);
													    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													   	$printer->setTextSize(1, 1);
													   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $kot_no_string = '';
													    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
														   	$printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
													    	$total_amount_normal = 0;
													    	foreach($nvalues as $keyss => $nvalue){
														    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
														    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
														    	if($nvalue['message'] != ''){
														    		$printer->setTextSize(1, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
														    	}
														    	$printer->feed(1);
														    	if($modifierdata != array()){
															    	foreach($modifierdata as $key => $value){
														    			$printer->setTextSize(1, 1);
														    			if($key == $nvalue['id']){
														    				foreach($value as $modata){
																	    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																		    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																		    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																	    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																	    		$printer->feed(1);
																    		}
																    	}
														    		}
														    	}
														    	$total_items_normal ++ ;
												    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
												    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
												    			$kot_no_string .= $nvalue['kot_no'].",";
														    }
													    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

														    $printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
													    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
												    		$printer->feed(1);
												    		$printer->setTextSize(2, 1);
												    		$printer->text("G.Total :".$total_g );
												    		$printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
													    } else {
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														    $total_items_normal = 0;
															$total_quantity_normal = 0;
														    foreach($nvalues as $keyss => $valuess){
														    	//echo'<pre>';print_r($valuess);exit;
														    	$printer->setTextSize(2, 1);
														    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
														    	if($valuess['message'] != ''){
														    		$printer->setTextSize(1, 1);
														    		$printer->feed(1);
														    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			//$printer->setTextSize(1, 1);
													    			$printer->setTextSize(2, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
														    	$total_items_normal ++ ;
														    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
														    	$kot_no_string .= $valuess['kot_no'].",";
													    	}
															$printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
														foreach($nvalues as $keyss => $valuess){
															$valuess['qty'] = round($valuess['qty']);
															for($i = 1; $i <= $valuess['qty']; $i++){
																$total_items_normal = 0;
																$total_quantity_normal = 0;
																$qtydisplay = 1;
																//echo $valuess['qty'];
																$printer = new Printer($connector);
															    $printer->selectPrintMode(32);
															   	$printer->setEmphasis(true);
															   	$printer->setTextSize(2, 1);
															   	$printer->setJustification(Printer::JUSTIFY_CENTER);
															    $printer->text($infoss[0]['location']);
															    $printer->feed(1);
															    $printer->text($description);
															    $printer->feed(1);
															    $printer->setTextSize(2, 1);
															   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
															    $printer->text("Tbl.No ".$table_id);
															    $printer->feed(1);
															    $printer->setJustification(Printer::JUSTIFY_LEFT);
															    $printer->setTextSize(1, 1);
															    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
																$printer->feed(1);
															    $printer->setEmphasis(true);
															   	$printer->setTextSize(1, 1);
															   $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
															    $printer->feed(1);
															    $printer->setEmphasis(false);
															   	$printer->setTextSize(1, 1);
															   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
															    $printer->feed(1);
															    $printer->text("------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(true);
															    $printer->text("Qty     Description");
															    $printer->feed(1);
															    $printer->text("------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(false);
														    	$printer->setTextSize(2, 2);
													    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,20,"\n"));
														    	if($valuess['message'] != ''){
														    		$printer->feed(1);
														    		$printer->setTextSize(2, 1);
														    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
														    	}
														    	$printer->feed(1);
													    		foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $valuess['id']){
													    				foreach($value as $modata){
																    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
																    		$printer->feed(1);
															    		}
															    	}
														    	}
															    $printer->setTextSize(2, 1);
														    	$total_items_normal ++ ;
														    	$total_quantity_normal ++;
														    	$qtydisplay ++;
														    	$printer->setTextSize(1, 1);
															    $printer->text("------------------------------------------");
															    $printer->feed(1);
															    $printer->setEmphasis(true);
															    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
															    $printer->feed(2);
															    $printer->setJustification(Printer::JUSTIFY_CENTER);
															    $printer->cut();
																$printer->feed(2);
															}
														}
													}
												}
											// Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
											$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
									    if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
										//continue;
									}
								}
							}
						}

						if($allKot && $infos_normal == array() && $infos_liqurnormal == array() && $infos_liqurcancel == array() && $infos_cancel == array()){
							$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
							if($printerModel ==0){
								try {
								    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
									 	$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
								 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
								 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
								 	} else {
								 		$connector = '';
								 	}
								    if($connector != ''){
								    	//echo'inn3';exit;
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	for($i = 1; $i <= $kot_copy; $i++){
									   		if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text("Default");
										    $printer->feed(1);
								    		$printer->text("CHECK KOT");
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text(str_pad("Wtr",10)."".str_pad("Cpt",10)."".date('d/m/y', strtotime($infoss[0]['date_added'])));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(str_pad($infoss[0]['waiter_id'],10)."".str_pad($infoss[0]['captain_id'],10)."".date('H:i:s', strtotime($allKot[0]['time_added'])));
										    $printer->feed(1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($allKot as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);	
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->feed(2);
										    } else {
											    $printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
											    foreach($allKot as $keyss => $valuess){
											    	//$printer->setTextSize(2, 1);
											    	$printer->setTextSize(2, 1);
										    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		$printer->setTextSize(1, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			//$printer->setTextSize(1, 1);
										    			$printer->setTextSize(2, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
												    $printer->setTextSize(2, 1);
											    	$total_items_normal ++ ;
											    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    if($this->model_catalog_order->get_settings('CHECK_KOT_GRAND_TOTAL') == 1){
											    	$printer->feed(1);
											    	$printer->setTextSize(2, 2);
											    	$printer->text("GRAND TOTAL  :  ".$infoss[0]['grand_total']);
												}
											    $printer->feed(2);
												$printer->setJustification(Printer::JUSTIFY_CENTER);
											}
										    $printer->cut();
										    // Close printer //
										    $printer->close();
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$allKot[0]['order_id']."' ");
										}
									}
								} catch (Exception $e) {
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$allKot[0]['order_id']."' ");
								}
							} else {  // 45 space code starts from here
								try {
							    	if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
								 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
								 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
								 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
								 	} else {
								 		$connector = '';
								 	}
								    if($connector != ''){
								    	//echo'inn3';exit;
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	// $printer->text('45 Spacess');
						    			$printer->feed(1);
									   	for($i = 1; $i <= $kot_copy; $i++){
									   		if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    			$printer->feed(1);
								    		}
										    $printer->text($infoss[0]['location']);
										    $printer->feed(1);
										    $printer->text("Default");
										    $printer->feed(1);
								    		$printer->text("CHECK KOT");
										    $printer->feed(1);
										    $printer->setTextSize(2, 1);
										   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    $printer->text("Tbl.No ".$table_id);
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->setTextSize(1, 1);
										   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],8).str_pad("Persons :".$infoss[0]['person'],8)."K.Ref.No :".$infoss[0]['order_id']);
											$printer->feed(1);
										    $printer->setEmphasis(true);
										   	$printer->setTextSize(1, 1);
										    $printer->text(str_pad("Wtr",8)."".str_pad("Cpt",8)."".date('d/m/y', strtotime($infoss[0]['date_added'])));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										   	$printer->text(str_pad($infoss[0]['waiter_id'],8)."".str_pad($infoss[0]['captain_id'],8)."".date('H:i', strtotime($allKot[0]['time_added'])));
										    $printer->feed(1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
										    	$printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
												$total_amount_normal = 0;
										    	foreach($allKot as $keyss => $nvalue){
											    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
											    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
											    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
											    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
											    	if($nvalue['message'] != ''){
											    		$printer->setTextSize(1, 1);	
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
													}
											    	$printer->feed(1);
											    	if($modifierdata != array()){
												    	foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
														    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
									    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    }
									    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
										    
											    $printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
										    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
									    		$printer->feed(1);
									    		$printer->setTextSize(2, 1);
									    		$printer->text("G.Total :".$total_g );
									    		$printer->feed(2);
											    $printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->feed(2);
										    } else {
											    $printer->text("Qty     Description");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											    $total_items_normal = 0;
												$total_quantity_normal = 0;
											    foreach($allKot as $keyss => $valuess){
											    	$printer->setTextSize(2, 1);
										    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
											    	if($valuess['message'] != ''){
											    		//$printer->setTextSize(1, 1);
											    		$printer->setTextSize(2, 1);
											    		$printer->feed(1);
											    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
											    	}
											    	$printer->feed(1);
										    		foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $valuess['id']){
										    				foreach($value as $modata){
													    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
													    		$printer->feed(1);
												    		}
												    	}
											    	}
												    $printer->setTextSize(2, 1);
											    	$total_items_normal ++ ;
											    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
										    	}
										    	$printer->setTextSize(1, 1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    if($this->model_catalog_order->get_settings('CHECK_KOT_GRAND_TOTAL') == 1){
											    	$printer->feed(1);
											    	$printer->setTextSize(2, 2);
											    	$printer->text("GRAND TOTAL  :  ".$infoss[0]['grand_total']);
												}
											    $printer->feed(2);
												$printer->setJustification(Printer::JUSTIFY_CENTER);
											}
										    $printer->cut();
										    // Close printer //
										    $printer->close();
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$allKot[0]['order_id']."' ");
										}
									}
								} catch (Exception $e) {
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$allKot[0]['order_id']."' ");
								}
							}
						}

						if($infos_liqurnormal){
							foreach($infos_liqurnormal as $nkeys => $nvalues){
							 	$printtype = '';
							 	$printername = '';
							 	$description = '';
							 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];

							 	$printername = $this->user->getPrinterName();
							 	$printertype = $this->user->getPrinterType();
							 	
							 	if($printertype != ''){
							 		$printtype = $printertype;
							 	} else {
							 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
										$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
							 		}
								}

								if ($printername != '') {
									$printname = $printername;
								} else {
									if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
										$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
									}
								}

								if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
									$description = $kot_group_datas[$sub_category_id_compare]['description'];
								}

								$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								if($printerModel ==0){

									try {
							 			if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
										    if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No    Wtr    Cpt               ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_liquor_normal = 0;
													    $total_quantity_liquor_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(2, 1);
													    		//$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
												    	$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	
													    	if($nvalue['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_liquor_normal = 0;
													    $total_quantity_liquor_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".str_pad($valuess['name'],20,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
												    	$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_liquor_normal = 0;
														    $total_quantity_liquor_normal = 0;
														    $qtydisplay = 1;
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
															   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
												    			$printer->feed(1);
												    		}
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($description);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);
														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
															$printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														    $printer->text("BOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($qtydisplay." ".str_pad($valuess['name']."-".$i,20,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			//$printer->setTextSize(1, 1);
												    			$printer->setTextSize(2, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
									    if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								} else {  // 45 space code starts from here
									try {
							 			if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
										    if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	// $printer->text('45 Spacess');
						    					$printer->feed(1);		
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],8).str_pad("Persons :".$infoss[0]['person'],8)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."   ".$infoss[0]['waiter_id']."   ".$infoss[0]['captain_id']."               ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_liquor_normal = 0;
													    $total_quantity_liquor_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
												    	$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($description);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
													$printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_liquor_normal = 0;
													    $total_quantity_liquor_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".str_pad($valuess['name'],20,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
												    	$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_liquor_normal = 0;
														    $total_quantity_liquor_normal = 0;
														    $qtydisplay = 1;
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
															   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
												    			$printer->feed(1);
												    		}
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($description);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);
														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
															$printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($qtydisplay." ".str_pad($valuess['name']."-".$i,20,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],24,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_liquor_normal ++;
													    	$total_quantity_liquor_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_liquor_normal."     T Item :  ".$total_items_liquor_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
									    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
									    if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								}			
							}
						}

						if($infos_cancel){
							foreach($infos_cancel as $nkeys => $nvalues){
							 	$printtype = '';
							 	$printername = '';
							 	$description = '';
							 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
							 	
							 	if(isset($kot_group_datas[$sub_category_id_compare]['printer_type'])){
									$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
								}
								if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
									$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
								}
								if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
									$description = $kot_group_datas[$sub_category_id_compare]['description'];
								}

								$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								if($printerModel ==0){
									try {
									    if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
									 		//if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 2);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->text("** CANCEL KOT **");
											   	$printer->setTextSize(2, 1);
											   	$printer->feed(1);
											   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("----------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_cancel = 0;
													$total_quantity_cancel = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$qty = explode(".", $valuess['qty']);
												    	$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
												    	if($valuess['message'] != ''){
												    		//$printer->setTextSize(1, 1);
												    		$printer->setTextSize(2, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
												    	}
											    		$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_cancel ++ ;
												    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
											    	$printer->setTextSize(1, 1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											// } else{
											// 	foreach($nvalues as $keyss => $valuess){
											// 		$total_items_cancel = 0;
											// 		$total_quantity_cancel = 0;
											// 		$printer = new Printer($connector);
											// 	    $printer->selectPrintMode(32);
											// 	   	$printer->setEmphasis(true);
											// 	   	$printer->setTextSize(2, 1);
											// 	   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    //$printer->feed(1);
											// 	   	//$printer->setFont(Printer::FONT_B);
											// 	    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											// 	    $printer->feed(1);
										 //    		$printer->text("CANCEL KOT");
											// 	    $printer->feed(1);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setTextSize(1, 1);
											// 	    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
											// 		$printer->feed(1);
											// 		// $printer->text("User Name :".$this->user->getUserName());
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 		// $printer->text("K.Ref.No :".$infoss[0]['order_id']);
											// 		// $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setEmphasis(true);
											// 	   	$printer->setTextSize(1, 1);
											// 	    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											// 	   	$printer->setTextSize(1, 1);
											// 	   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('h:i:s a', strtotime($nvalues[0]['time_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("Qty     Description");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											//     	$printer->setTextSize(2, 1);
										 //    	  	//$valuess['name'] = utf8_substr(html_entity_decode($valuess['name'], ENT_QUOTES, 'UTF-8'), 0, 15);
											//     	//$valuess['message'] = utf8_substr(html_entity_decode($valuess['message'], ENT_QUOTES, 'UTF-8'), 0, 10);
											//     	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											//     	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
											//     	$printer->feed(1);
											//     	if($valuess['message'] != ''){
											//     		$printer->feed(1);
											//     		$printer->text("Msg :".wordwrap($valuess['message'],10,"<br>\n"));
											//     	}
										 //    		$printer->feed(1);
										 //    		foreach($modifierdata as $key => $value){
										 //    			$printer->setTextSize(1, 1);
										 //    			if($key == $valuess['id']){
										 //    				foreach($value as $modata){
											// 		    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											// 		    		$printer->feed(1);
											// 	    		}
											// 	    	}
											//     	}
											// 	    $printer->feed(1);
											//     	$total_items_cancel ++ ;
											//     	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
											//     	$printer->setTextSize(1, 1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
											// 	    $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    $printer->cut();
											// 		$printer->feed(2);
											// 	}
											// }
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
										if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								} else {  // 45 space code starts from here
									try {
									    if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
									 		//if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 2);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	// $printer->text('45 Spacess');
						    					$printer->feed(1);
											    $printer->text("** CANCEL KOT **");
											   	$printer->setTextSize(2, 1);
											   	$printer->feed(1);
											   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
												$printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_cancel = 0;
													$total_quantity_cancel = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$qty = explode(".", $valuess['qty']);
												    	$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
												    	if($valuess['message'] != ''){
												    		//$printer->setTextSize(1, 1);
												    		$printer->setTextSize(2, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
												    	}
											    		$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_cancel ++ ;
												    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
											    	$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											// } else{
											// 	foreach($nvalues as $keyss => $valuess){
											// 		$total_items_cancel = 0;
											// 		$total_quantity_cancel = 0;
											// 		$printer = new Printer($connector);
											// 	    $printer->selectPrintMode(32);
											// 	   	$printer->setEmphasis(true);
											// 	   	$printer->setTextSize(2, 1);
											// 	   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    //$printer->feed(1);
											// 	   	//$printer->setFont(Printer::FONT_B);
											// 	    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											// 	    $printer->feed(1);
										 //    		$printer->text("CANCEL KOT");
											// 	    $printer->feed(1);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setTextSize(1, 1);
											// 	    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
											// 		$printer->feed(1);
											// 		// $printer->text("User Name :".$this->user->getUserName());
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 		// $printer->text("K.Ref.No :".$infoss[0]['order_id']);
											// 		// $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setEmphasis(true);
											// 	   	$printer->setTextSize(1, 1);
											// 	    $printer->text("Tbl No    KOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											// 	   	$printer->setTextSize(1, 1);
											// 	   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('h:i:s a', strtotime($nvalues[0]['time_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("Qty     Description");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											//     	$printer->setTextSize(2, 1);
										 //    	  	//$valuess['name'] = utf8_substr(html_entity_decode($valuess['name'], ENT_QUOTES, 'UTF-8'), 0, 15);
											//     	//$valuess['message'] = utf8_substr(html_entity_decode($valuess['message'], ENT_QUOTES, 'UTF-8'), 0, 10);
											//     	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											//     	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
											//     	$printer->feed(1);
											//     	if($valuess['message'] != ''){
											//     		$printer->feed(1);
											//     		$printer->text("Msg :".wordwrap($valuess['message'],10,"<br>\n"));
											//     	}
										 //    		$printer->feed(1);
										 //    		foreach($modifierdata as $key => $value){
										 //    			$printer->setTextSize(1, 1);
										 //    			if($key == $valuess['id']){
										 //    				foreach($value as $modata){
											// 		    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											// 		    		$printer->feed(1);
											// 	    		}
											// 	    	}
											//     	}
											// 	    $printer->feed(1);
											//     	$total_items_cancel ++ ;
											//     	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
											//     	$printer->setTextSize(1, 1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
											// 	    $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    $printer->cut();
											// 		$printer->feed(2);
											// 	}
											// }
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
										if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								}
							}
						}

						if($infos_liqurcancel){
							foreach($infos_liqurcancel as $nkeys => $nvalues){
							 	$printtype = '';
							 	$printername = '';
							 	$description = '';
							 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
							 	
							 	if(isset($kot_group_datas[$sub_category_id_compare]['printer_type'])){
									$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
								}
								if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
									$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
								}
								if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
									$description = $kot_group_datas[$sub_category_id_compare]['description'];
								}

								$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								if($printerModel ==0){
									try {
									    if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
										    //if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 2);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    $printer->text("** CANCEL BOT **");
											   	$printer->feed(1);
											   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
											   	$printer->setTextSize(2, 1);
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
												$printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_liquor_cancel = 0;
												    $total_quantity_liquor_cancel = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	//$printer->setTextSize(2, 1);
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$qty = explode(".", $valuess['qty']);
														$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(2, 1);
												    		$printer->feed(1);
												    		$printer->text("(".$valuess['message'].")");
												    	}
											    		$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_liquor_cancel ++ ;
												    	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
											    	$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											// } else{
											// 	foreach($nvalues as $keyss => $valuess){
											// 		$total_items_liquor_cancel = 0;
											// 	    $total_quantity_liquor_cancel = 0;
											// 		$printer = new Printer($connector);
											// 	    $printer->selectPrintMode(32);
											// 	   	$printer->setEmphasis(true);
											// 	   	$printer->setTextSize(2, 1);
											// 	   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    //$printer->feed(1);
											// 	   	//$printer->setFont(Printer::FONT_B);
											// 	    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											// 	    $printer->feed(1);
										 //    		$printer->text("CANCEL BOT");
											// 	    $printer->feed(1);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setTextSize(1, 1);
											// 	    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
											// 		$printer->feed(1);
											// 		// $printer->text("User Name :".$this->user->getUserName());
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 		// $printer->text("K.Ref.No :".$infoss[0]['order_id']);
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setEmphasis(true);
											// 	   	$printer->setTextSize(1, 1);
											// 	    $printer->text("Tbl No    BOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											// 	   	$printer->setTextSize(1, 1);
											// 	   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('h:i:s a', strtotime($nvalues[0]['time_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("Qty     Description");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											//     	$printer->setTextSize(2, 1);
										 //    	  	//$valuess['name'] = utf8_substr(html_entity_decode($valuess['name'], ENT_QUOTES, 'UTF-8'), 0, 15);
											//     	//$valuess['message'] = utf8_substr(html_entity_decode($valuess['message'], ENT_QUOTES, 'UTF-8'), 0, 10);
											//     	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											//     	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
											//     	if($valuess['message'] != ''){
											//     		$printer->feed(1);
											//     		$printer->text("Msg :".wordwrap($valuess['message'],10,"<br>\n"));
											//     	}
										 //    		$printer->feed(1);
										 //    		foreach($modifierdata as $key => $value){
										 //    			$printer->setTextSize(1, 1);
										 //    			if($key == $valuess['id']){
										 //    				foreach($value as $modata){
											// 		    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											// 		    		$printer->feed(1);
											// 	    		}
											// 	    	}
											//     	}
											// 	    $printer->feed(1);
											//     	$total_items_liquor_cancel ++ ;
											//     	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
											//     	$printer->setTextSize(1, 1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
											// 	    $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    $printer->cut();
											// 		$printer->feed(2);
											// 	}
											// }
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
										if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								} else{  // 45 space code starts from here
									try {
									    if($printtype == 'Network'){
									 		$connector = new NetworkPrintConnector($printername, 9100);
									 	} else if($printtype == 'Windows'){
									 		$connector = new WindowsPrintConnector($printername);
									 	} else {
									 		$connector = '';
									 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
									 	}
									 	if($connector != ''){
										    //if($kot_different == 0){
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 2);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	// $printer->text('45 Spacess');
						    					$printer->feed(1);
											    $printer->text("** CANCEL BOT **");
											   	$printer->feed(1);
											   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    			$printer->feed(1);
									    		}
											   	$printer->setTextSize(2, 1);
											    $printer->text($infoss[0]['location']);
											    $printer->feed(1);
											    $printer->text($description);
											    $printer->feed(1);
											    $printer->setTextSize(2, 1);
											   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
											    $printer->text("Tbl.No ".$table_id);
											    $printer->feed(1);
											    $printer->setJustification(Printer::JUSTIFY_LEFT);
											    $printer->setTextSize(1, 1);
											   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
												$printer->feed(1);
												$printer->setEmphasis(true);
											   	$printer->setTextSize(1, 1);
											    $printer->text("BOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											    $printer->feed(1);
											    $printer->setEmphasis(false);
											   	$printer->setTextSize(1, 1);
											   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
											    $printer->feed(1);
											    $printer->text("------------------------------------------");
											    $printer->feed(1);
											    $printer->setEmphasis(true);
											    $kot_no_string = '';
											    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
											    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
											    	$printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_normal = 0;
													$total_quantity_normal = 0;
													$total_amount_normal = 0;
											    	foreach($nvalues as $keyss => $nvalue){
												    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
												    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
												    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
												    	if($nvalue['message'] != ''){
												    		$printer->setTextSize(1, 1);
												    		$printer->feed(1);
												    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
												    	}
												    	$printer->feed(1);
												    	if($modifierdata != array()){
													    	foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $nvalue['id']){
												    				foreach($value as $modata){
															    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
																    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
															    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
															    		$printer->feed(1);
														    		}
														    	}
												    		}
												    	}
												    	$total_items_normal ++ ;
										    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
										    			$kot_no_string .= $nvalue['kot_no'].",";
												    }
										    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
											    
												    $printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
											    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
										    		$printer->feed(1);
										    		$printer->setTextSize(2, 1);
										    		$printer->text("G.Total :".$total_g );
										    		$printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
											    } else {
												    $printer->text("Qty     Description");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												    $total_items_liquor_cancel = 0;
												    $total_quantity_liquor_cancel = 0;
												    foreach($nvalues as $keyss => $valuess){
												    	$printer->setTextSize(2, 1);
											    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    	$qty = explode(".", $valuess['qty']);
														$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
												    	if($valuess['message'] != ''){
												    		$printer->setTextSize(2, 1);
												    		$printer->feed(1);
												    		$printer->text("(".$valuess['message'].")");
												    	}
											    		$printer->feed(1);
											    		foreach($modifierdata as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $valuess['id']){
											    				foreach($value as $modata){
														    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
														    		$printer->feed(1);
													    		}
													    	}
												    	}
												    	$total_items_liquor_cancel ++ ;
												    	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
												    	$kot_no_string .= $valuess['kot_no'].",";
											    	}
											    	$printer->setTextSize(1, 1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
												    $printer->feed(2);
												    $printer->setJustification(Printer::JUSTIFY_CENTER);
												    $printer->cut();
													$printer->feed(2);
												}
											// } else{
											// 	foreach($nvalues as $keyss => $valuess){
											// 		$total_items_liquor_cancel = 0;
											// 	    $total_quantity_liquor_cancel = 0;
											// 		$printer = new Printer($connector);
											// 	    $printer->selectPrintMode(32);
											// 	   	$printer->setEmphasis(true);
											// 	   	$printer->setTextSize(2, 1);
											// 	   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    //$printer->feed(1);
											// 	   	//$printer->setFont(Printer::FONT_B);
											// 	    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
											// 	    $printer->feed(1);
										 //    		$printer->text("CANCEL BOT");
											// 	    $printer->feed(1);
											// 	    $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setTextSize(1, 1);
											// 	    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],30)."K.Ref.No :".$infoss[0]['order_id']);
											// 		$printer->feed(1);
											// 		// $printer->text("User Name :".$this->user->getUserName());
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 		// $printer->text("K.Ref.No :".$infoss[0]['order_id']);
											// 		// $printer->feed(2);
											// 		// $printer->setJustification(Printer::JUSTIFY_LEFT);
											// 	    $printer->setEmphasis(true);
											// 	   	$printer->setTextSize(1, 1);
											// 	    $printer->text("Tbl No    BOT No    Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											// 	   	$printer->setTextSize(1, 1);
											// 	   	$printer->text(" ".$infoss[0]['table_id']."         ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."     ".date('h:i:s a', strtotime($nvalues[0]['time_added']))."");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("Qty     Description");
											// 	    $printer->feed(1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(false);
											//     	$printer->setTextSize(2, 1);
										 //    	  	//$valuess['name'] = utf8_substr(html_entity_decode($valuess['name'], ENT_QUOTES, 'UTF-8'), 0, 15);
											//     	//$valuess['message'] = utf8_substr(html_entity_decode($valuess['message'], ENT_QUOTES, 'UTF-8'), 0, 10);
											//     	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
											//     	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
											//     	if($valuess['message'] != ''){
											//     		$printer->feed(1);
											//     		$printer->text("Msg :".wordwrap($valuess['message'],10,"<br>\n"));
											//     	}
										 //    		$printer->feed(1);
										 //    		foreach($modifierdata as $key => $value){
										 //    			$printer->setTextSize(1, 1);
										 //    			if($key == $valuess['id']){
										 //    				foreach($value as $modata){
											// 		    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											// 		    		$printer->feed(1);
											// 	    		}
											// 	    	}
											//     	}
											// 	    $printer->feed(1);
											//     	$total_items_liquor_cancel ++ ;
											//     	$total_quantity_liquor_cancel = $total_quantity_liquor_cancel + $valuess['qty'];
											//     	$printer->setTextSize(1, 1);
											// 	    $printer->text("----------------------------------------------");
											// 	    $printer->feed(1);
											// 	    $printer->setEmphasis(true);
											// 	    $printer->text("T Qty CANCEL :  ".$total_quantity_liquor_cancel."     T Item CANCEL :  ".$total_items_liquor_cancel."");
											// 	    $printer->feed(2);
											// 	    $printer->setJustification(Printer::JUSTIFY_CENTER);
											// 	    $printer->cut();
											// 		$printer->feed(2);
											// 	}
											// }
										    // Close printer //
										    $printer->close();
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										}
									} catch (Exception $e) {
										if(isset($kot_no_string)){
										    $kot_no_string = rtrim($kot_no_string, ',');
										    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
										} else {
											$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
										}
									    $this->session->data['warning'] = $printername." "."Not Working";
									    //continue;
									}
								}
							}
						}
					}
				}else {

					$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');
					$KOT_RATE_AMT = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
					$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
					$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
					$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
					$BAR_NAME =	$this->model_catalog_order->get_settings('BAR_NAME');
					$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
					$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
					$GST_NO	= $this->model_catalog_order->get_settings('GST_NO');
					$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
					$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
					$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');

					if(isset($this->session->data['credit'])){
						$credit = $this->session->data['credit'];
					} else {
						$credit = '0';
					}
					if(isset($this->session->data['cash'])){
						$cash = $this->session->data['cash'];
					} else {
						$cash = '0';
					}
					if(isset($this->session->data['online'])){
						$online = $this->session->data['online'];
					} else {
						$online ='0';
					}

					if(isset($this->session->data['onac'])){
						$onac = $this->session->data['onac'];
						$onaccontact = $this->session->data['onaccontact'];
						$onacname = $this->session->data['onacname'];

					} else {
						$onac ='0';
						$onaccontact = '';
						$onacname = '';
					}

					if(isset($testliqs)){
						$testliqs = $testliqs;
					} else {
						$testliqs =array();
					}

					if(isset($testfoods)){
						$testfoods = $testfoods;
					} else {
						$testfoods =array();
					}

					if(isset($infoss)){
						$infoss = $infoss;
					} else {
						$infoss =array();
					}

					if(isset($ansb)){
						$ansb = $ansb;
					} else {
						$ansb =array();
					}

					

					$final_datas = array();
					if($local_print == 1){
						$final_datas = array(
							'aaaaa' => 0000,
							'infoss' => $infoss,
							'infos_normal' => $infos_normal,
							'modifierdata' => $modifierdata,
							'allKot' => $allKot,
							'infos_liqurnormal' => $infos_liqurnormal,
							'infos_liqurcancel' => $infos_liqurcancel,
							'infos_cancel' => $infos_cancel,
							'kot_different' => $kot_different,
							'edit' => $edit,
							'ansb' => $ansb,
							'infosb' => $infosb,
							'infoslb' => $infoslb,
							'merge_datas' => $merge_datas,
							'modifierdatabill' => $modifierdatabill,
							'KOT_RATE_AMT' =>$KOT_RATE_AMT,
							'HOTEL_NAME' => $HOTEL_NAME,
							'HOTEL_ADD' =>$HOTEL_ADD,
							'INCLUSIVE' => $INCLUSIVE,
							'BAR_NAME' => $BAR_NAME,
							'BAR_ADD' => $BAR_ADD,
							'GST_NO' => $GST_NO,
							'SETTLEMENT_status' => $SETTLEMENT_status,
							'direct_bill' => $direct_bill,
							'bill_copy' =>$bill_copy,
							'localprint' => $local_print,
							'kot_copy' => $kot_copy,
							'testfoods' => $testfoods,
							'testliqs' => $testliqs,
						);
					}
					
					$json = array();
					$json = array(
						'status' => 0,
						'final_datas' => $final_datas,
						'LOCAL_PRINT' => $local_print,

						
					);

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($json));	

				}


			// echo '<pre>';
			// print_r($edit);
			// echo '</br>';\
			// print_r($infoss[0]['bill_status']);
			// exit;

			$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
			if($SETTLEMENT_status==1){
				if(isset($this->session->data['credit'])){
					$credit = $this->session->data['credit'];
				}
				if(isset($this->session->data['cash'])){
					$cash = $this->session->data['cash'];
				}
				if(isset($this->session->data['online'])){
					$online = $this->session->data['online'];
				}
				if(isset($this->session->data['onac'])){
					$onac = $this->session->data['onac'];
				}
			}
			

			if(($infoss[0]['bill_status'] == 0 && $edit == '0' && $master_kot == 0) || ($infoss[0]['bill_status'] != 0 && $edit == '1' && $master_kot == 0)){
				if($LOCAL_PRINT == 0 && ($direct_bill == 1 || $edit == '1')){
					
					$merge_datas = array();
					$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($edit == '0'){
						$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates = $this->db->query($last_open_date_sql);
						if($last_open_dates->num_rows > 0){
							$last_open_date = $last_open_dates->row['bill_date'];
						} else {
							$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
							$last_open_dates = $this->db->query($last_open_date_sql);
							if($last_open_dates->num_rows > 0){
								$last_open_date = $last_open_dates->row['bill_date'];
							} else {
								$last_open_date = date('Y-m-d');
							}
						}

						$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
						if($last_open_dates_liq->num_rows > 0){
							$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
						} else {
							$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
							$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
							if($last_open_dates_liq->num_rows > 0){
								$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
							} else {
								$last_open_date_liq = date('Y-m-d');
							}
						}

						$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
						$last_open_dates_order = $this->db->query($last_open_date_sql_order);
						if($last_open_dates_order->num_rows > 0){
							$last_open_date_order = $last_open_dates_order->row['bill_date'];
						} else {
							$last_open_date_order = date('Y-m-d');
						}
						
						$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
						
						if($ordernogenrated['order_no'] == '0'){
							$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
							$orderno = 1;
							if(isset($orderno_q['order_no'])){
								$orderno = $orderno_q['order_no'] + 1;
							}
							
							$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
							if($kotno2->num_rows > 0){
								$kot_no2 = $kotno2->row['billno'];
								$kotno = $kot_no2 + 1;
							} else{
								$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
								if($kotno2->num_rows > 0){
									$kot_no2 = $kotno2->row['billno'];
									$kotno = $kot_no2 + 1;
								} else {
									$kotno = 1;
								}
							}

							$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
							if($kotno1->num_rows > 0){
								$kot_no1 = $kotno1->row['billno'];
								$botno = $kot_no1 + 1;
							} else{
								$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
								if($kotno1->num_rows > 0){
									$kot_no1 = $kotno1->row['billno'];
									$botno = $kot_no1 + 1;
								}else {
									$botno = 1;
								}
							}

							$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
							$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

							// echo'<pre>';
							// print_r($this->session->data);
							// exit;
							if(isset($this->session->data['cash'])){
								$cashh = $this->session->data['cash'];
							} else{
								$cashh = '';
							}

							if(isset($this->session->data['credit'])){
								$creditt = $this->session->data['credit'];
							} else{
								$creditt = '';
							}

							if(isset($this->session->data['online'])){
								$onlinee = $this->session->data['online'];
							} else{
								$onlinee = '';
							}

							if(isset($this->session->data['onac'])){
								$onacc = $this->session->data['onac'];
							} else{
								$onacc = '';
							}

							// echo $cashh;
							// echo'<br />';
							// echo $creditt;
							// exit;
							if(isset($this->session->data['payment_type'])){
								$payment_type = $this->session->data['payment_type'];
								if($payment_type==0){
									$payment_type = '';
								}
							} else{
								$payment_type = '';
							}

							if(($onacc != '0' && $onacc != '') && ($cashh != '0' && $cashh != '')){
								$tot_pay = $onacc + $cashh;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_cash = '".$cashh."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							}elseif(($onacc != '0' && $onacc != '') && ($creditt != '0' && $creditt != '')){
								$tot_pay = $onacc + $creditt;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_card = '".$creditt."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							}elseif(($onacc != '0' && $onacc != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay = $onacc + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_online = '".$onlinee."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							
							}elseif(($cashh != '0' && $cashh != '') && ($creditt != '0' && $creditt != '')){
								$tot_pay = $cashh + $creditt;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."',pay_card = '".$creditt."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

							
							}elseif(($cashh != '0' && $cashh != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay = $cashh + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."' , pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


							}elseif(($creditt != '0' && $creditt != '') && ($onlinee != '0' && $onlinee != '')){
								$tot_pay =  $creditt + $onlinee;
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."',pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


							}elseif($cashh != '0' && $cashh != ''){

								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."', payment_status = '1', total_payment = '".$cashh."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);

							}elseif( $creditt != '0' && $creditt != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."', payment_status = '1', total_payment = '".$creditt."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							}elseif( $onlinee != '0' && $onlinee != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$onlinee."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							}elseif( $onacc != '0' && $onacc != ''){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."',onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$this->session->data['onac']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
								// unset($this->session->data['cash']);
								// unset($this->session->data['credit']);
								// unset($this->session->data['online']);
								// unset($this->session->data['onac']);
								// unset($this->session->data['onaccust']);
								// unset($this->session->data['onaccontact']);
								// unset($this->session->data['onacname']);
							

							} elseif($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							} else{
								$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
							}
							$ordernogenrated['order_no'] = $orderno;
							$this->db->query($update_sql);
							$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
							if($apporder['app_order_id'] != '0'){
								$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
							}
						} 
						if($ordernogenrated['order_no'] != '0'){
							$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
						}
					}

					$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
					$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
					foreach($tests as $test){
						$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
						$testfoods[] = array(
							'tax1' => $test['tax1'],
							'amt' => $amt,
							'tax1_value' => $test['tax1_value']
						);
					}
					
					$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
					foreach($testss as $testa){
						$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
						$testliqs[] = array(
							'tax1' => $testa['tax1'],
							'amt' => $amts,
							'tax1_value' => $testa['tax1_value']
						);
					}
					$infoslb = array();
					$infosb = array();
					$flag = 0;
					$totalquantityfood = 0;
					$totalquantityliq = 0;
					$disamtfood = 0;
					$disamtliq = 0;
					$modifierdatabill = array();
					foreach ($anssb as $lkey => $result) {
						foreach($anssb as $lkeys => $results){
							if($lkey == $lkeys) {

							} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['code'] = '';
									}
								}
							} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
								if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
									if($result['parent'] == '0'){
										$result['qty'] = $result['qty'] + $results['qty'];
										if($result['nc_kot_status'] == '0'){
											$result['amt'] = $result['qty'] * $result['rate'];
										}
									}
								}
							}
						}
						if($result['code'] != ''){
							$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
								if ($decimal_mesurement == 0) {
										$qty = (int)$result['qty'];
								} else {
										$qty = $result['qty'];
								}
							if($result['is_liq']== 0){
								$infosb[] = array(
									'billno'		=> $result['billno'],
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityfood = $totalquantityfood + $result['qty'];
								$disamtfood = $disamtfood + $result['discount_value'];
							} else {
								$flag = 1;
								$infoslb[] = array(
									'billno'		=> $result['billno'],
									'id'			=> $result['id'],
									'name'          => $result['name'],
									'rate'          => $result['rate'],
									'amt'           => $result['amt'],
									'qty'         	=> $qty,
									'tax1'         	=> $result['tax1'],
									'tax2'          => $result['tax2']
								);
								$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
								$totalquantityliq = $totalquantityliq + $result['qty'];
								$disamtliq = $disamtliq + $result['discount_value'];
							}
						}
					}
					
					if($ansb['parcel_status'] == '0'){
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
						} else{
							$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
						}
					} else {
						if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
							$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
						} else{
							$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
						}
					}

					//$onac = $this->session->data['onac'];

				// echo'<pre>';
				// print_r($this->session->data);
				// exit;
				

					$csgst=$ansb['gst'] / 2;
					$csgsttotal = $ansb['gst'];

					$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
					$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
					$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
					$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
					$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
					if($ansb['advance_amount'] == '0.00'){
						$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
					} else{
						$gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
					}
					//$gtotal = ceil($gtotal);
					$gtotal = round($gtotal);


					if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
						$locationData =  $this->db->query("SELECT `parcel_detail` FROM oc_location WHERE location_id = '".$ansb['location_id']."'");
						if($locationData->num_rows > 0){
							$parcel_detail = $locationData->row['parcel_detail'];
						} else {
							$parcel_detail = 0;
						}


						$printtype = $bill_printer_type;
			 			$printername = $bill_printer_name;
						if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
							$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
						 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
						}
					
						$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
						if($printerModel ==0){
									$this->log->write("Innnnn Bill Function ");


							try {
						    	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								   	//$printer->setFont(Printer::FONT_B);
									for($i = 1; $i <= $bill_copy; $i++){
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    //$printer->setJustification(Printer::JUSTIFY_CENTER);
									    //$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	$printer->feed(1);
									    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
											
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
											$printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
											$printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Name :".$ansb['cust_name']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infosb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infosb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			}
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											foreach($testfoods as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",30)."SCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",30)."CCGST :".$csgst."");
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",33)."SCRG :".$ansb['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												} else{
													$printer->text(str_pad("",33)."SCRG :".$ansb['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ansb['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infoslb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time :".date('h:i:s'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infoslb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											foreach($testliqs as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",32)."VAT :".$ansb['vat']."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",31)."SCRG :".$ansb['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												} else{
													$printer->text(str_pad("",31)."SCRG :".$ansb['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ansb['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansb['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("",28)."GRAND TOTAL  :  ".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ansb['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
											$printer->feed(1);
										}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										// echo $SETTLEMENT_status;
										// exit;
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: ".$onacname."");
												$printer->feed(1);
												$printer->text("Contact: ".$onaccontact."");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
										if($merge_datas){
											$printer->feed(2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("Merged Bills");
											$printer->feed(1);
											$mcount = count($merge_datas) - 1;
											foreach($merge_datas as $mkey => $mvalue){
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text("Bill Number :".$mvalue['order_no']."");
												$printer->feed(1);
												$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
												$printer->feed(1);
												$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
												$printer->feed(1);
												if($mvalue['ltotal'] > '0'){
													$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
												}
												$printer->feed(1);
												if($ansb['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													}
												}
												$printer->setJustification(Printer::JUSTIFY_RIGHT);
												$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
												$printer->feed(1);
												if($ansb['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													}
												}
												if($mkey < $mcount){ 
													$printer->text("----------------------------------------------");
													$printer->feed(1);
												}
											}
											$printer->feed(1);
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
											$printer->feed(1);
										}
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									    // Close printer //
									}
								    $printer->close();
								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
							}
							
						}
						else{  // 45 space code starts from here


							try {
						    	if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 	}
							    if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								    // $printer->text('45 Spacess');
					    			$printer->feed(1);
								   	//$printer->setFont(Printer::FONT_B);
									for($i = 1; $i <= $bill_copy; $i++){
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    //$printer->setJustification(Printer::JUSTIFY_CENTER);
									    //$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	$printer->feed(1);
									    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
											
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
											$printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
											$printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
										 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
										 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Name :".$ansb['cust_name']."");
										    $printer->feed(1);
										}
										else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
										    $printer->text("Address : ".$ansb['cust_address']."");
										    $printer->feed(1);
										}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
										    $printer->text("Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										    $printer->feed(1);
										    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infosb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infosb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			}
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											foreach($testfoods as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",25)."SCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",25)."CCGST :".$csgst."");
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												} else{
													$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ansb['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infoslb){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$ansb['order_no']."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infoslb as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
										    	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
							    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
											$printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											foreach($testliqs as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ansb['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ansb['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",21)."VAT :".$ansb['vat']."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												} else{
													$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ansb['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansb['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text("GRAND TOTAL : ".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ansb['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
											$printer->feed(1);
										}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
										if($merge_datas){
											$printer->feed(2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("Merged Bills");
											$printer->feed(1);
											$mcount = count($merge_datas) - 1;
											foreach($merge_datas as $mkey => $mvalue){
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text("Bill Number :".$mvalue['order_no']."");
												$printer->feed(1);
												$printer->text(str_pad("F Total :".$mvalue['ftotal'],20)."SGST :".$mvalue['gst']/2);
												$printer->feed(1);
												$printer->text(str_pad("",20)."CGST :".$mvalue['gst']/2);
												$printer->feed(1);
												if($mvalue['ltotal'] > '0'){
													$printer->text(str_pad("L Total :".$mvalue['ltotal'],20)."VAT :".$mvalue['vat']."");
												}
												$printer->feed(1);
												if($ansb['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													}
												}
												$printer->setJustification(Printer::JUSTIFY_RIGHT);
												$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
												$printer->feed(1);
												if($ansb['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													}
												}
												if($mkey < $mcount){ 
													$printer->text("------------------------------------------");
													$printer->feed(1);
												}
											}
											$printer->feed(1);
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text(str_pad("MERGED GRAND TOTAL",35).$gtotal."");
											$printer->feed(1);
										}
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									    // Close printer //
									}
								    $printer->close();

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
							}
						}
						if($parcel_detail == 1){
							$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
							$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
							$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
							$hote_name=str_replace(" ", "%20", $HOTEL_NAME);
							$i =1;
							$food_items = '';
							$liq_items ='';
							foreach($infosb as $nkey => $nvalue){
								$food_items .= $i.'-'.$nvalue['name'].','.'%0a';
								$i++;
							}
							$l = $i;
							foreach($infoslb as $nakey => $navalue){
								$liq_items .= $l.'-'.$navalue['name'].','.'%0a';
								$l++;
							}

							$text =  $hote_name.'%0a'.$food_items.''.$liq_items.'%0a'.'Grand Total :- '.$gtotal;
							$msg = str_replace(" ", "%20", $text);

							$link= $link_1.$ansb['cust_contact']."&message=".$msg;
							// echo'<pre>';
							// print_r($link);
							// exit;
							if($ansb['cust_contact'] != ''){
								file($link);
							}

							// if($ret[0] != 'Daily Message Limit Reached'){
							// 	$this->session->data['success'] .= "SMS Send Successfully<br>";
							// } else {
							// 	$this->session->data['success'] .= "Sub Batch ".$i." Not Send<br>";
							// }
						}
					}
					$json = array();
					$json = array(
						'direct_bill' => $direct_bill,
						'order_id' => $order_id,
						'grand_total' => $gtotal,
						'status' => 1,
						'orderidmodify' =>$order_id,
						'edit' => $edit,
					);
					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($json));
					unset($this->session->data['cash']);
					unset($this->session->data['credit']);
					unset($this->session->data['online']);
					unset($this->session->data['onac']);
					unset($this->session->data['onaccust']);
					unset($this->session->data['onaccontact']);
					unset($this->session->data['onacname']);
				} else {
					$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');
							$KOT_RATE_AMT = $this->model_catalog_order->get_settings('KOT_RATE_AMT');
							$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
							$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
							$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
							$BAR_NAME =	$this->model_catalog_order->get_settings('BAR_NAME');
							$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
							$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
							$GST_NO	= $this->model_catalog_order->get_settings('GST_NO');
							$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
							$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
							$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');

							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac ='0';
								$onaccontact = '';
								$onacname = '';
							}

							if(isset($testliqs)){
								$testliqs = $testliqs;
							} else {
								$testliqs =array();
							}

							if(isset($testfoods)){
								$testfoods = $testfoods;
							} else {
								$testfoods =array();
							}

							if(isset($ansb)){
								$ansb = $ansb;
							} else {
								$ansb =array();
							}

							if(isset($infosb)){
								$infosb = $infosb;
							} else {
								$infosb =array();
							}

							if(isset($infoslb)){
								$infoslb = $infoslb;
							} else {
								$infoslb =array();
							}

							if(isset($merge_datas)){
								$merge_datas = $merge_datas;
							} else {
								$merge_datas =array();
							}

							if(isset($modifierdatabill)){
								$modifierdatabill = $modifierdatabill;
							} else {
								$modifierdatabill =array();
							}



							$final_datas = array();
							if($local_print == 1){
								$final_datas = array(
									'infoss' => $infoss,
									'infos_normal' => $infos_normal,
									'modifierdata' => $modifierdata,
									'allKot' => $allKot,
									'infos_liqurnormal' => $infos_liqurnormal,
									'infos_liqurcancel' => $infos_liqurcancel,
									'infos_cancel' => $infos_cancel,
									'kot_different' => $kot_different,
									'edit' => $edit,
									'ansb' => $ansb,
									'infosb' => $infosb,
									'infoslb' => $infoslb,
									'merge_datas' => $merge_datas,
									'modifierdatabill' => $modifierdatabill,
									'KOT_RATE_AMT' =>$KOT_RATE_AMT,
									'HOTEL_NAME' => $HOTEL_NAME,
									'HOTEL_ADD' =>$HOTEL_ADD,
									'INCLUSIVE' => $INCLUSIVE,
									'BAR_NAME' => $BAR_NAME,
									'BAR_ADD' => $BAR_ADD,
									'GST_NO' => $GST_NO,
									'SETTLEMENT_status' => $SETTLEMENT_status,
									'direct_bill' => $direct_bill,
									'bill_copy' =>$bill_copy,
									'localprint' => $local_print,
									'kot_copy' => $kot_copy,
									'testfoods' => $testfoods,
									'testliqs' => $testliqs,
									'cash' => $cash,
									'credit' => $credit,
									'online' => $online,
									'onac' => $onac,
									'onaccontact' => $onaccontact,
									'onacname' => $onacname,

									'TEXT1' => $TEXT1,
									'TEXT2' => $TEXT2,
									'TEXT3' => $TEXT3,


								);
							}
							
							$json = array();
							$json = array(
								'status' => 0,
								'final_datas' => $final_datas,
								'LOCAL_PRINT' => $local_print,

								
							);

							$this->response->addHeader('Content-Type: application/json');
							$this->response->setOutput(json_encode($json));	
							unset($this->session->data['cash']);
							unset($this->session->data['credit']);
							unset($this->session->data['online']);
							unset($this->session->data['onac']);
							unset($this->session->data['onaccust']);
							unset($this->session->data['onaccontact']);
							unset($this->session->data['onacname']);
						}
			} else{
				$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');

				$json = array();
				$json = array(
					'status' => 0,
					'localprints' => $local_print,

				);
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				//$this->session->data['warning'] = 'Bill already printed';
			}
			
		}
	}
}
	//$this->log->write('---------------------------Kot Print End---------------------------');

		/*
		$this->request->post = array();
		$_POST = array();
		if($edit == '1'){
			$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'].'&orderidmodify='.$order_id));
		} else{
			$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token']));
		}
		*/
		
	}
	

	public function master_kotprint($order_id) {
		$this->load->language('customer/customer');
		$this->load->model('catalog/order');
		$this->load->model('catalog/msr_recharge');
		$this->load->language('catalog/order');
		$this->document->setTitle($this->language->get('heading_title'));
		$msr_bal = 0;
		$compl_status = 0;
		
		
		if ($order_id > 0) {
			$te = 'BILL';
			if(isset($this->request->get['edit']) && ($this->request->get['edit'] == 1)){
				$edit = $this->request->get['edit'];
			} else{
				$edit = '0';
			}

			if(isset($this->request->get['checkkot'])){
				$getcheckkot = $this->request->get['checkkot'];
			} else{
				$getcheckkot = '0';
			}

			$checkkot = array();
			
			$anss = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1' ORDER BY id,subcategoryid")->rows;
			$anss_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '0' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$anss_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '0' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
			
			$liqurs = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_1 = array();//$this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '1' AND is_liq = '1' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
			$liqurs_2 = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND is_new = '0' AND is_liq = '1' AND cancelstatus = '1' AND ismodifier = '1'")->rows;
			$user_id= $this->user->getId();
			$user = $this->db->query("SELECT `checkkot` FROM oc_user WHERE user_id = '".$user_id."'")->row;
			if($getcheckkot == '1' && $user['checkkot'] == '1'){
				$checkkot = $this->db->query("SELECT `id`, `order_id`, `billno`, `nc_kot_status`, `code`, `name`, `qty`, `rate`, `ismodifier`, `parent`, `parent_id`, `amt`, `subcategoryid`, `message`, `is_liq`, `kot_status`, `pre_qty`, `prefix`, `is_new`, `kot_no`, `cancelstatus`, `login_id`, `login_name`, `time`, `date`, `captain_id`, `waiter_id` FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND (kot_no <> '' AND kot_no <> '0')")->rows;
			}
			
			$infos_normal = array();
			$infos_cancel = array();
			$infos_liqurnormal = array();
			$infos_liqurcancel = array();
			$infos = array();
			$modifierdata = array();
			$allKot = array();
			$ans = $this->db->query("SELECT `order_id`, `time_added`, `date_added`, `location`, `location_id`, `t_name`, `table_id`, `waiter`, `waiter_id`, `captain`, `captain_id`, `item_quantity`, `total_items`, `login_id`, `login_name`, `person`, `ftotal`, `ltotal`, `grand_total`, `bill_status` FROM oc_order_info WHERE order_id = '".$order_id."'")->rows;
			//$orderitem_time = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' ORDER BY `id` DESC LIMIT 1")->row['time'];
			foreach ($ans as $resultt) {
				$infoss[] = array(
					'order_id'   => $resultt['order_id'],
					'time_added'  => $resultt['time_added'],
					'date_added'  => $resultt['date_added'],
					'location'  => $resultt['location'],
					'location_id' => $resultt['location_id'],
					't_name'    => $resultt['t_name'],
					'table_id'  => $resultt['table_id'],
					'waiter'    => $resultt['waiter'],
					'waiter_id'    => $resultt['waiter_id'],
					'captain'   => $resultt['captain'],
					'captain_id'   => $resultt['captain_id'],
					'item_quantity'   => $resultt['item_quantity'],
					'total_items'   => $resultt['total_items'],
					'login_id' => $resultt['login_id'],
					'login_name' => $resultt['login_name'],
					'person' => $resultt['person'],
					'ftotal' => $resultt['ftotal'],
					'ltotal' => $resultt['ltotal'],
					'grand_total' => $resultt['grand_total'],
					'bill_status' => $resultt['bill_status'],

				);
			}

			
			// echo'<pre>';
			// print_r($infoss);
			// exit();
			$kot_group_datas = array();
			$printerinfos = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup WHERE master_kotprint = '1' ")->rows;
			

			foreach($printerinfos as $pkeys => $pvalues){
				if($pvalues['subcategory'] != ''){
					$subcategoryid_exp = explode(',', $pvalues['subcategory']);
					foreach($subcategoryid_exp as $skey => $svalue){
						$kot_group_datas[$svalue] = $pvalues;
					}
				}
			}
			// echo'<pre>';
			// print_r($kot_group_datas);
			// exit();
			
			foreach ($anss as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}

				if($result['code'] != ''){
					if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
						$kot_group_datas[$result['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$result['qty'];
					} else {
							$qty = $result['qty'];
					}

					
					$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
						'id'				=> $result['id'],
						'name'           	=> $result['name'],
						'qty'         		=> $qty,
						'code'         		=> $result['code'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'         	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'            => $result['kot_no'],
						'time_added'        => $result['time'],
					);

					// echo'<pre>';
					// print_r($result);
					// exit;
					$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}



			foreach ($checkkot as $lkey => $result) {
				foreach($checkkot as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '0' && $result['cancelstatus'] == '0' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}
				if($result['code'] != ''){
					$allKot[] = array(
						'order_id'			=> $result['order_id'],
						'id'				=> $result['id'],
						'name'           	=> $result['name'],
						'qty'         		=> $result['qty'],
						'rate'         		=> $result['rate'],
						'amt'         		=> $result['amt'],
						'subcategoryid'		=> $result['subcategoryid'],
						'message'         	=> $result['message'],
						'kot_no'            => $result['kot_no'],
						'time_added'        => $result['time'],
					);
					$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}

			// echo'<pre>';
			// print_r($kot_group_datas);
			// echo'<br/>';
			// print_r($infos_normal);
			// exit;
			
			$total_items_normal = 0;
			$total_quantity_normal = 0;
			foreach ($anss_1 as $result) {
				if($result['qty'] > $result['pre_qty']) {
					$tem = $result['qty'] - $result['pre_qty'];
					$ans=abs($tem);
					if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
						$kot_group_datas[$result['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$ans;
					} else {
							$qty = $ans;
					}
					$infos_normal[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
						'name'          	=> $result['name'],
						'qty'         		=> $qty,
						'code'         		=> $result['code'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'       	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'        	=> $result['kot_no'],
						'time_added'        => $result['time'],
					);
					$total_items_normal ++;
					$total_quantity_normal = $total_quantity_normal + $ans;
					$flag = 0;
				}
			}

			foreach ($anss_2 as $lkey => $result) {
				foreach($anss as $lkeys => $resultss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1'){
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['code'] = '';
							}
						}
					} elseif ($result['code'] == $resultss['code'] && $result['rate'] == $resultss['rate'] && $result['message'] == $resultss['message'] && $resultss['cancelstatus'] == '1' && $result['cancelstatus'] == '1' && $result['ismodifier'] == '1' && $resultss['ismodifier'] == '1') {
						if(($result['amt'] == $resultss['amt']) || ($resultss['amt'] != '0' && $result['amt'] != '0')){
							if($result['parent'] == '0'){
								$result['qty'] = $result['qty'] + $resultss['qty'];
								if($result['nc_kot_status'] == '0'){
									$result['amt'] = $result['qty'] * $result['rate'];
								}
							}
						}
					}
				}

				if($result['code'] != ''){
					if(!isset($kot_group_datas[$result['subcategoryid']]['code'])){
						$kot_group_datas[$result['subcategoryid']]['code'] = 1;
					}
					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$result['qty'];
					} else {
							$qty = $result['qty'];
					}
					if($qty > 0){
						$infos_cancel[$kot_group_datas[$result['subcategoryid']]['code']][] = array(
							'id'				=> $result['id'],
							'name'          	=> $result['name']." (cancel)",
							'qty'         		=> $result['qty'],
							'rate'         		=> $result['rate'],
							'amt'				=> $result['amt'],
							'message'       	=> $result['message'],
							'subcategoryid'		=> $result['subcategoryid'],
							'kot_no'        	=> $result['kot_no'],
							'time_added'        => $result['time'],
						);
					}
					$modifierdata[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}

			foreach ($liqurs as $lkey => $liqur) {
				foreach($liqurs as $lkeys => $liqurss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1'){
						if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['code'] = '';
							}
						}
					} elseif ($liqur['code'] == $liqurss['code'] && $liqur['rate'] == $liqurss['rate'] && $liqur['message'] == $liqurss['message'] && $liqurss['cancelstatus'] == '0' && $liqur['cancelstatus'] == '0' && $liqur['ismodifier'] == '1' && $liqurss['ismodifier'] == '1') {
						if(($liqur['amt'] == $liqurss['amt']) || ($liqurss['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['qty'] = $liqur['qty'] + $liqurss['qty'];
								if($liqur['nc_kot_status'] == '0'){
									$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
								}
							}
						}
					}
				}

				if($liqur['code'] != ''){
					if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
						$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
					}
					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$liqur['qty'];
					} else {
							$qty = $liqur['qty'];
					}

					$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
						'id'				=> $liqur['id'],
						'name'           	=> $liqur['name'],
						'qty'         		=> $qty,
						'amt'				=> $liqur['amt'],
						'rate'				=> $liqur['rate'],
						'message'         	=> $liqur['message'],
						'subcategoryid'		=> $liqur['subcategoryid'],
						'kot_no'            => $liqur['kot_no'],
						'time_added'        => $liqur['time'],
					);
					// echo "<pre>";
					// print_r($liqur['id']);
					$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
					$flag = 0;
				}
			}
			// echo'</br>';
			// print_r($modifierdata);
			// exit;

			foreach ($liqurs_1 as $liqur) {
				if($liqur['qty'] > $liqur['pre_qty']) {
					$tem = $liqur['qty'] - $liqur['pre_qty'];
					$ans=abs($tem);
					if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
						$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$ans;
					} else {
							$qty = $ans;
					}

					$infos_liqurnormal[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
						'name'          	=> $liqur['name'],
						'qty'         		=> $qty,
						'amt'				=> $liqur['amt'],
						'rate'				=> $liqur['rate'],
						'message'       	=> $liqur['message'],
						'subcategoryid'		=> $liqur['subcategoryid'],
						'kot_no'        	=> $liqur['kot_no'],
						'time_added'        => $liqur['time'],
					);
					$total_items_liquor_normal ++;
					$total_quantity_liquor_normal = $total_quantity_liquor_normal + $ans;
					$flag = 0;
				}
			}

			$total_items_liquor_cancel = 0;
			$total_quantity_liquor_cancel = 0;
			foreach ($liqurs_2 as $lkey => $liqur) {
				foreach($liqurs_2 as $lkeys => $liqurs){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1'){
						if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['code'] = '';
							}
						}
					} elseif ($liqur['code'] == $liqurs['code'] && $liqur['rate'] == $liqurs['rate'] && $liqur['message'] == $liqurs['message'] && $liqurs['cancelstatus'] == '1' && $liqur['cancelstatus'] == '1' && $liqur['ismodifier'] == '1' && $liqurs['ismodifier'] == '1') {
						if(($liqur['amt'] == $liqurs['amt']) || ($liqurs['amt'] != '0' && $liqur['amt'] != '0')){
							if($liqur['parent'] == '0'){
								$liqur['qty'] = $liqur['qty'] + $liqurs['qty'];
								if($liqur['nc_kot_status'] == '0'){
									$liqur['amt'] = $liqur['qty'] * $liqur['rate'];
								}
							}
						}
					}
				}

				if($liqur['code'] != ''){
					if(!isset($kot_group_datas[$liqur['subcategoryid']]['code'])){
						$kot_group_datas[$liqur['subcategoryid']]['code'] = 1;
					}

					$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$liqur['code']."' ")->row['decimal_mesurement'];
					if ($decimal_mesurement == 0) {
							$qty = (int)$liqur['qty'];
					} else {
							$qty = $liqur['qty'];
					}
					$infos_liqurcancel[$kot_group_datas[$liqur['subcategoryid']]['code']][] = array(
						'id'      			=> $liqur['id'],
						'name'          	=> $liqur['name']."(cancel)",
						'qty'         		=> $qty,
						'amt'         		=> $liqur['amt'],
						'rate'         		=> $liqur['rate'],
						'message'       	=> $liqur['message'],
						'subcategoryid'		=> $liqur['subcategoryid'],
						'kot_no'        	=> $liqur['kot_no'],
						'time_added'        => $liqur['time'],
					);
					//$text = "Cancelled Bot <br>Orderid :".$order_id.", Item Name :".$result['name'].", Qty :".$result['qty'].", Amt :".$result['amt'].", Kot_no :".$result['kot_no'];
					//echo $text;
					$modifierdata[$liqur['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$liqur['id']."' AND ismodifier = '0'")->rows;
					$total_items_liquor_cancel ++;
					$total_quantity_liquor_cancel = $liqur['qty'];
					$flag = 0;
				}
			}

			if(!isset($flag)){
				$anss= $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."'")->rows;
				$total_items = 0;
				$total_quantity = 0;
				foreach ($anss as $result) {
					$infos[] = array(
						'name'           	=> $result['name'],
						'qty'         	 	=> $result['qty'],
						'amt'				=> $result['amt'],
						'rate'				=> $result['rate'],
						'message'        	=> $result['message'],
						'subcategoryid'		=> $result['subcategoryid'],
						'kot_no'        	=> $result['kot_no'],
						'time_added' 		=> $result['time'],
					);
					$total_items ++;
					$total_quantity = $total_quantity + $result['qty'];
				}
			}
			$this->db->query("UPDATE " . DB_PREFIX . "order_items SET `kot_status` = '1', `is_new` = '1' WHERE  order_id = '".$order_id."'");
		}

		$locationData =  $this->db->query("SELECT `kot_different`, `kot_copy`, `bill_copy`, `direct_bill`, `bill_printer_type`, `bill_printer_name`,`master_kot` FROM oc_location WHERE location_id = '".$infoss[0]['location_id']."'");
		if($locationData->num_rows > 0){
			$locationData = $locationData->row;
			if($locationData['kot_different'] == 1){
				$kot_different = 1;
			} else {
				$kot_different = 0;
			}
			$kot_copy = $locationData['kot_copy'];
			$bill_copy = $locationData['bill_copy'];
			$direct_bill = $locationData['direct_bill'];
			$bill_printer_type = $locationData['bill_printer_type'];
			$bill_printer_name = $locationData['bill_printer_name'];
		} else{
			$kot_different = 0;
			$kot_copy = 1;
			$direct_bill = 0;
			$bill_printer_type = '';
			$bill_printer_name = '';
		}

		$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
		if(($LOCAL_PRINT == 0 && $direct_bill == 0 && $edit == '0') || $kot_copy > 0){
			if($kot_copy > 0 && $LOCAL_PRINT == 0){
				if($infos_normal){
					foreach($infos_normal as $nkeys => $nvalues){
								
					 	$printtype = '';
					 	$printername = '';
					 	$description = '';
					 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
					 	$printername = $this->user->getPrinterName();
					 	$printertype = $this->user->getPrinterType();
					 	
					 	if($printertype != ''){
					 		$printtype = $printertype;
					 	} else {
					 		if (isset($kot_group_datas[$sub_category_id_compare]['printer_type'])) {
								$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
					 		}
						}

						if ($printername != '') {
							$printname = $printername;
						} else {
							if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
								$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
							}
						}

						if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
							$description = $kot_group_datas[$sub_category_id_compare]['description'];
						}

						$printerinfoszz = $this->db->query("SELECT `subcategory`, `printer_type`, `printer`, `description`, `code` FROM oc_kotgroup WHERE master_kotprint = '1' ")->rows;

						//foreach($printerinfoszz as $pkeys => $pvalues){
							// if($pvalues['subcategory'] != ''){
							//  	$subcategoryid_exp = explode(',', $pvalues['subcategory']);
							// 	foreach($subcategoryid_exp as $skey => $svalue){
							// 		foreach($nvalues as $keyss => $nvalue){
							// 			if($svalue == $nvalue['subcategoryid']){
							// 				$printer_name[] = array(
							// 					'printername' => $pvalues['printer'],
							// 					'printertype' => $pvalues['printer_type'],
							// 					'description' => $pvalues['description'],

							// 				);
							// 			}
							// 	 	}
							// 	}
							// }
						// 	echo'<pre>';
						// print_r($pvalues);
						//}

						
						//exit;
						//$prints_names = array_unique($printer_name);
						// $final_array = array();
						// foreach ($printer_name as  $value) {
						// 	if(!in_array($value, $final_array)){
						// 		$final_array[] = $value;
						// 	}
						// }

						foreach ($printerinfoszz as $qkey => $qvalue) {
							$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
							if($printerModel ==0){
								try {
							 		if($qvalue['printer_type'] == 'Network'){
								 		$connector = new NetworkPrintConnector($qvalue['printer'], 9100);
								 	} elseif($qvalue['printer_type'] == 'Windows'){
								 		$connector = new WindowsPrintConnector($qvalue['printer']);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
								 	}
								 	if($connector != ''){
								 		$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
								 		if($printerModel == 0){
									 		if($kot_different == 0){
										    	//echo"innnn kot";exit;
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($qvalue['description']);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
													$printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    		
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(2, 2);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
												    	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);
													    	//$printer->setTextSize(2, 2);
												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(2, 1);
													    		//$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}

												    	$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	for($i = 1; $i <= $kot_copy; $i++){
											   		//echo'innn2';exit;
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($qvalue['description']);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("----------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													   	$printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
												    	$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
												    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

													    $printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	//echo'<pre>';print_r($valuess);exit;
													    	$printer->setTextSize(2, 1);
													    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],30,"\n"));
													    	if($valuess['message'] != ''){
													    		//$printer->setTextSize(1, 1);
													    		$printer->setTextSize(2, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
														$printer->setTextSize(1, 1);
													    $printer->text("----------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_normal = 0;
															$total_quantity_normal = 0;
															$qtydisplay = 1;
															//echo $valuess['qty'];
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($qvalue['description']);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);
														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 1);
												    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,30,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->feed(1);
													    		$printer->setTextSize(2, 1);
													    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
														    $printer->setTextSize(2, 1);
													    	$total_items_normal ++ ;
													    	$total_quantity_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("----------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										} 
										// Close printer //
									    $printer->close();
									    $kot_no_string = rtrim($kot_no_string, ',');
										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									}
								} catch (Exception $e) {
								    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
								    if(isset($kot_no_string)){
									    $kot_no_string = rtrim($kot_no_string, ',');
									    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									} else {
										$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
									}
								    $this->session->data['warning'] = $printername." "."Not Working";
									//continue;
								}
							} else {  // 45 space code starts from here
								try {
							 		if($qvalue['printer_type'] == 'Network'){
								 		$connector = new NetworkPrintConnector($qvalue['printer'], 9100);
								 	} elseif($qvalue['printer_type'] == 'Windows'){
								 		$connector = new WindowsPrintConnector($qvalue['printer']);
								 	} else {
								 		$connector = '';
								 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
								 	}
								 	if($connector != ''){
									 		if($kot_different == 0){
										    	//echo"innnn kot";exit;
											    $printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	// $printer->text('45 Spacess');
						    					$printer->feed(1);
											    for($i = 1; $i <= $kot_copy; $i++){
											    	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
													   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
										    			$printer->feed(1);
										    		}
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($qvalue['description']);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
													$printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],12).str_pad("Persons :".$infoss[0]['person'],12)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){

												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
														$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    		
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
											    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
												    
													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
														$printer->text("Qty     Description");
												    	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	$printer->setTextSize(2, 1);

												    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],25,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],30,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			//$printer->setTextSize(1, 1);
												    			$printer->setTextSize(2, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],25,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}

												    	$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
												}
											} else{
												$printer = new Printer($connector);
											    $printer->selectPrintMode(32);
											   	$printer->setEmphasis(true);
											   	$printer->setTextSize(2, 1);
											   	$printer->setJustification(Printer::JUSTIFY_CENTER);
											   	for($i = 1; $i <= $kot_copy; $i++){
											   		//echo'innn2';exit;
												    $printer->text($infoss[0]['location']);
												    $printer->feed(1);
												    $printer->text($qvalue['description']);
												    $printer->feed(1);
												    $printer->setTextSize(2, 1);
												   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
												    $printer->text("Tbl.No ".$table_id);
												    $printer->feed(1);
												    $printer->setJustification(Printer::JUSTIFY_LEFT);
												    $printer->setTextSize(1, 1);
												    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],10).str_pad("Persons :".$infoss[0]['person'],10)."K.Ref.No :".$infoss[0]['order_id']);
													$printer->feed(1);
												    $printer->setEmphasis(true);
												   	$printer->setTextSize(1, 1);
												    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
												    $printer->feed(1);
												    $printer->setEmphasis(false);
												   	$printer->setTextSize(1, 1);
												   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
												    $printer->feed(1);
												    $printer->text("------------------------------------------");
												    $printer->feed(1);
												    $printer->setEmphasis(true);
												    $kot_no_string = '';
												    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
												    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
													   	$printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
												    	$total_amount_normal = 0;
												    	foreach($nvalues as $keyss => $nvalue){
													    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
													    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
													    	if($nvalue['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
													    	}
													    	$printer->feed(1);
													    	if($modifierdata != array()){
														    	foreach($modifierdata as $key => $value){
													    			$printer->setTextSize(1, 1);
													    			if($key == $nvalue['id']){
													    				foreach($value as $modata){
																    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
																	    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
																	    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
																    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
																    		$printer->feed(1);
															    		}
															    	}
													    		}
													    	}
													    	$total_items_normal ++ ;
											    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
											    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
											    			$kot_no_string .= $nvalue['kot_no'].",";
													    }
												    	$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];

													    $printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
												    	$printer->text("T.I. : ".str_pad($total_items_normal,5)."T.Q. :".str_pad($total_quantity_normal,5)."F.T. :".$total_amount_normal);
											    		$printer->feed(1);
											    		$printer->setTextSize(2, 1);
											    		$printer->text("G.Total :".$total_g );
											    		$printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
												    } else {
													    $printer->text("Qty     Description");
													    $printer->feed(1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(false);
													    $total_items_normal = 0;
														$total_quantity_normal = 0;
													    foreach($nvalues as $keyss => $valuess){
													    	//echo'<pre>';print_r($valuess);exit;
													    	$printer->setTextSize(2, 1);
													    	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
													    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->setTextSize(1, 1);
													    		$printer->feed(1);
													    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			//$printer->setTextSize(1, 1);
												    			$printer->setTextSize(2, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
													    	$total_items_normal ++ ;
													    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
													    	$kot_no_string .= $valuess['kot_no'].",";
												    	}
														$printer->setTextSize(1, 1);
													    $printer->text("------------------------------------------");
													    $printer->feed(1);
													    $printer->setEmphasis(true);
													    $printer->text("T. Q. :  ".$total_quantity_normal."     T. I. :  ".$total_items_normal."");
													    $printer->feed(2);
													    $printer->setJustification(Printer::JUSTIFY_CENTER);
													    $printer->cut();
														$printer->feed(2);
													}
													foreach($nvalues as $keyss => $valuess){
														$valuess['qty'] = round($valuess['qty']);
														for($i = 1; $i <= $valuess['qty']; $i++){
															$total_items_normal = 0;
															$total_quantity_normal = 0;
															$qtydisplay = 1;
															//echo $valuess['qty'];
															$printer = new Printer($connector);
														    $printer->selectPrintMode(32);
														   	$printer->setEmphasis(true);
														   	$printer->setTextSize(2, 1);
														   	$printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->text($infoss[0]['location']);
														    $printer->feed(1);
														    $printer->text($qvalue['description']);
														    $printer->feed(1);
														    $printer->setTextSize(2, 1);
														   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
														    $printer->text("Tbl.No ".$table_id);
														    $printer->feed(1);
														    $printer->setJustification(Printer::JUSTIFY_LEFT);
														    $printer->setTextSize(1, 1);
														    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
															$printer->feed(1);
														    $printer->setEmphasis(true);
														   	$printer->setTextSize(1, 1);
														   $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
														   	$printer->setTextSize(1, 1);
														   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("Qty     Description");
														    $printer->feed(1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(false);
													    	$printer->setTextSize(2, 2);
												    	  	$printer->text($qtydisplay." ".wordwrap($valuess['name']."-".$i,20,"\n"));
													    	if($valuess['message'] != ''){
													    		$printer->feed(1);
													    		$printer->setTextSize(2, 1);
													    		$printer->text("(".wordwrap($valuess['message'],10,"<br>\n").")");
													    	}
													    	$printer->feed(1);
												    		foreach($modifierdata as $key => $value){
												    			$printer->setTextSize(1, 1);
												    			if($key == $valuess['id']){
												    				foreach($value as $modata){
															    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
															    		$printer->feed(1);
														    		}
														    	}
													    	}
														    $printer->setTextSize(2, 1);
													    	$total_items_normal ++ ;
													    	$total_quantity_normal ++;
													    	$qtydisplay ++;
													    	$printer->setTextSize(1, 1);
														    $printer->text("------------------------------------------");
														    $printer->feed(1);
														    $printer->setEmphasis(true);
														    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
														    $printer->feed(2);
														    $printer->setJustification(Printer::JUSTIFY_CENTER);
														    $printer->cut();
															$printer->feed(2);
														}
													}
												}
											}
										// Close printer //
									    $printer->close();
									    $kot_no_string = rtrim($kot_no_string, ',');
										$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									}
								} catch (Exception $e) {
								    //echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
								    if(isset($kot_no_string)){
									    $kot_no_string = rtrim($kot_no_string, ',');
									    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
									} else {
										$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
									}
								    $this->session->data['warning'] = $printername." "."Not Working";
									//continue;
								}
							}
						}
					}
				}

				if($allKot && $infos_normal == array() && $infos_liqurnormal == array() && $infos_liqurcancel == array() && $infos_cancel == array()){
					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel ==0){
						try {
						    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
							 	$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
						 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
						 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
						    	//echo'inn3';exit;
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	for($i = 1; $i <= $kot_copy; $i++){
							   		if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
									   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
						    			$printer->feed(1);
						    		}
								    $printer->text($infoss[0]['location']);
								    $printer->feed(1);
								    $printer->text("Default");
								    $printer->feed(1);
						    		$printer->text("CHECK KOT");
								    $printer->feed(1);
								    $printer->setTextSize(2, 1);
								   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    $printer->text("Tbl.No ".$table_id);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setTextSize(1, 1);
								   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->feed(1);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								    $printer->text(str_pad("Wtr",10)."".str_pad("Cpt",10)."".date('d/m/y', strtotime($infoss[0]['date_added'])));
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   	$printer->text(str_pad($infoss[0]['waiter_id'],10)."".str_pad($infoss[0]['captain_id'],10)."".date('H:i:s', strtotime($allKot[0]['time_added'])));
								    $printer->feed(1);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
								    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
								    	$printer->feed(1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
										$total_amount_normal = 0;
								    	foreach($allKot as $keyss => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
									    	if($nvalue['message'] != ''){
									    		$printer->setTextSize(1, 1);	
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
											}
									    	$printer->feed(1);
									    	if($modifierdata != array()){
										    	foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
							    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    }
							    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
								    
									    $printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
								    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
							    		$printer->feed(1);
							    		$printer->setTextSize(2, 1);
							    		$printer->text("G.Total :".$total_g );
							    		$printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(2);
								    } else {
									    $printer->text("Qty     Description");
									    $printer->feed(1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($allKot as $keyss => $valuess){
									    	//$printer->setTextSize(2, 1);
									    	$printer->setTextSize(2, 1);
								    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
									    	if($valuess['message'] != ''){
									    		$printer->setTextSize(1, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
									    	}
									    	$printer->feed(1);
								    		foreach($modifierdata as $key => $value){
								    			//$printer->setTextSize(1, 1);
								    			$printer->setTextSize(2, 1);
								    			if($key == $valuess['id']){
								    				foreach($value as $modata){
											    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
											    		$printer->feed(1);
										    		}
										    	}
									    	}
										    $printer->setTextSize(2, 1);
									    	$total_items_normal ++ ;
									    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
								    	}
								    	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
									    $printer->feed(1);
									    $printer->text("----------------------------------------------");
									    if($this->model_catalog_order->get_settings('CHECK_KOT_GRAND_TOTAL') == 1){
									    	$printer->feed(1);
									    	$printer->setTextSize(2, 2);
									    	$printer->text("GRAND TOTAL  :  ".$infoss[0]['grand_total']);
										}
									    $printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
									}
								    $printer->cut();
								    // Close printer //
								    $printer->close();
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$allKot[0]['order_id']."' ");
								}
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$allKot[0]['order_id']."' ");
						}
					} else {  // 45 space code starts from here
						try {
					    	if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
						 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
						 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
						 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
						    	//echo'inn3';exit;
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);
							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							   	// $printer->text('45 Spacess');
				    			$printer->feed(1);
							   	for($i = 1; $i <= $kot_copy; $i++){
							   		if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
									   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
						    			$printer->feed(1);
						    		}
								    $printer->text($infoss[0]['location']);
								    $printer->feed(1);
								    $printer->text("Default");
								    $printer->feed(1);
						    		$printer->text("CHECK KOT");
								    $printer->feed(1);
								    $printer->setTextSize(2, 1);
								   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
								    $printer->text("Tbl.No ".$table_id);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setTextSize(1, 1);
								   	$printer->text(str_pad("User Id :".$infoss[0]['login_id'],8).str_pad("Persons :".$infoss[0]['person'],8)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->feed(1);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								    $printer->text(str_pad("Wtr",8)."".str_pad("Cpt",8)."".date('d/m/y', strtotime($infoss[0]['date_added'])));
								    $printer->feed(1);
								    $printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   	$printer->text(str_pad($infoss[0]['waiter_id'],8)."".str_pad($infoss[0]['captain_id'],8)."".date('H:i', strtotime($allKot[0]['time_added'])));
								    $printer->feed(1);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
								    $printer->setEmphasis(true);
								    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
								    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
								    	$printer->feed(1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
										$total_amount_normal = 0;
								    	foreach($allKot as $keyss => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
									    	if($nvalue['message'] != ''){
									    		$printer->setTextSize(1, 1);	
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($nvalue['message'],20,"\n").")");
											}
									    	$printer->feed(1);
									    	if($modifierdata != array()){
										    	foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
							    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
							    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
									    }
							    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
								    
									    $printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
								    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
							    		$printer->feed(1);
							    		$printer->setTextSize(2, 1);
							    		$printer->text("G.Total :".$total_g );
							    		$printer->feed(2);
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(2);
								    } else {
									    $printer->text("Qty     Description");
									    $printer->feed(1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									    $total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($allKot as $keyss => $valuess){
									    	$printer->setTextSize(2, 1);
								    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    	$printer->text($valuess['qty']." ".wordwrap($valuess['name'],20,"\n"));
									    	if($valuess['message'] != ''){
									    		//$printer->setTextSize(1, 1);
									    		$printer->setTextSize(2, 1);
									    		$printer->feed(1);
									    		$printer->text("(".wordwrap($valuess['message'],20,"\n").")");
									    	}
									    	$printer->feed(1);
								    		foreach($modifierdata as $key => $value){
								    			$printer->setTextSize(1, 1);
								    			if($key == $valuess['id']){
								    				foreach($value as $modata){
											    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],20,"\n"));
											    		$printer->feed(1);
										    		}
										    	}
									    	}
										    $printer->setTextSize(2, 1);
									    	$total_items_normal ++ ;
									    	$total_quantity_normal = $total_quantity_normal + $valuess['qty'];
								    	}
								    	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
									    $printer->feed(1);
									    $printer->text("------------------------------------------");
									    if($this->model_catalog_order->get_settings('CHECK_KOT_GRAND_TOTAL') == 1){
									    	$printer->feed(1);
									    	$printer->setTextSize(2, 2);
									    	$printer->text("GRAND TOTAL  :  ".$infoss[0]['grand_total']);
										}
									    $printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
									}
								    $printer->cut();
								    // Close printer //
								    $printer->close();
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$allKot[0]['order_id']."' ");
								}
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$allKot[0]['order_id']."' ");
						}
					}
				}
				if($infos_cancel){
					foreach($infos_cancel as $nkeys => $nvalues){
					 	$printtype = '';
					 	$printername = '';
					 	$description = '';
					 	$sub_category_id_compare = $nvalues[0]['subcategoryid'];
					 	
					 	if(isset($kot_group_datas[$sub_category_id_compare]['printer_type'])){
							$printtype = $kot_group_datas[$sub_category_id_compare]['printer_type'];
						}
						if(isset($kot_group_datas[$sub_category_id_compare]['printer'])){
							$printername = $kot_group_datas[$sub_category_id_compare]['printer'];
						}
						if(isset($kot_group_datas[$sub_category_id_compare]['description'])){
							$description = $kot_group_datas[$sub_category_id_compare]['description'];
						}

						$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
						if($printerModel ==0){
							try {
							    if($qvalue['printer_type']  == 'Network'){
							 		$connector = new NetworkPrintConnector($qvalue['printer'], 9100);
							 	} else if($qvalue['printer_type'] == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
							 		//if($kot_different == 0){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 2);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->text("** CANCEL KOT **");
									   	$printer->setTextSize(2, 1);
									   	$printer->feed(1);
									   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
							    			$printer->feed(1);
							    		}
									    $printer->text($infoss[0]['location']);
									    $printer->feed(1);
									    $printer->text($qvalue['description']);
									    $printer->feed(1);
									    $printer->setTextSize(2, 1);
									   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    $printer->text("Tbl.No ".$table_id);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setTextSize(1, 1);
									    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									    $printer->text("KOT No    Wtr    Cpt              ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   	$printer->text(" ".$nvalues[0]['kot_no']."         ".$infoss[0]['waiter_id']."      ".$infoss[0]['captain_id']."               ".date('H:i:s', strtotime($nvalues[0]['time_added']))."");
									    $printer->feed(1);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $kot_no_string = '';
									    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
									    	$printer->text(str_pad("Name",24)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
									    	$printer->feed(1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										    $total_items_normal = 0;
											$total_quantity_normal = 0;
											$total_amount_normal = 0;
									    	foreach($nvalues as $keyss => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
										    	if($nvalue['message'] != ''){
										    		$printer->setTextSize(1, 1);
										    		$printer->feed(1);
										    		$printer->text("(".wordwrap($nvalue['message'],50,"\n").")");
										    	}
										    	$printer->feed(1);
										    	if($modifierdata != array()){
											    	foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
								    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
								    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
								    			$kot_no_string .= $nvalue['kot_no'].",";
										    }
								    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
									    
										    $printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
									    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
								    		$printer->feed(1);
								    		$printer->setTextSize(2, 1);
								    		$printer->text("G.Total :".$total_g );
								    		$printer->feed(2);
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    $printer->cut();
											$printer->feed(2);
									    } else {
										    $printer->text("Qty     Description");
										    $printer->feed(1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										    $total_items_cancel = 0;
											$total_quantity_cancel = 0;
										    foreach($nvalues as $keyss => $valuess){
										    	$printer->setTextSize(2, 1);
									    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    	$qty = explode(".", $valuess['qty']);
										    	$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
										    	if($valuess['message'] != ''){
										    		//$printer->setTextSize(1, 1);
										    		$printer->setTextSize(2, 1);
										    		$printer->feed(1);
										    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
										    	}
									    		$printer->feed(1);
									    		foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $valuess['id']){
									    				foreach($value as $modata){
												    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
												    		$printer->feed(1);
											    		}
											    	}
										    	}
										    	$total_items_cancel ++ ;
										    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
										    	$kot_no_string .= $valuess['kot_no'].",";
									    	}
									    	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
										    $printer->feed(2);
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    $printer->cut();
											$printer->feed(2);
										}
									$printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
								if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
							    //continue;
							}
						} else {  // 45 space code starts from here
							try {
							    if($qvalue['printer_type'] == 'Network'){
							 		$connector = new NetworkPrintConnector($qvalue['printer'], 9100);
							 	} else if($qvalue['printer_type'] == 'Windows'){
							 		$connector = new WindowsPrintConnector($qvalue['printer']);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no = '".$nvalues[0]['kot_no']."' AND order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
							 		//if($kot_different == 0){
									    $printer = new Printer($connector);
									    $printer->selectPrintMode(32);
									   	$printer->setEmphasis(true);
									   	$printer->setTextSize(2, 2);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	// $printer->text('45 Spacess');
				    					$printer->feed(1);
									    $printer->text("** CANCEL KOT **");
									   	$printer->setTextSize(2, 1);
									   	$printer->feed(1);
									   	if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
										   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
							    			$printer->feed(1);
							    		}
									    $printer->text($infoss[0]['location']);
									    $printer->feed(1);
									    $printer->text($description);
									    $printer->feed(1);
									    $printer->setTextSize(2, 1);
									   	$table_id = utf8_substr(html_entity_decode($infoss[0]['table_id'], ENT_QUOTES, 'UTF-8'), 0, 4);
									    $printer->text("Tbl.No ".$table_id);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setTextSize(1, 1);
									    $printer->text(str_pad("User Id :".$infoss[0]['login_id'],15).str_pad("Persons :".$infoss[0]['person'],15)."K.Ref.No :".$infoss[0]['order_id']);
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									    $printer->text("KOT No  Wtr  Cpt   ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   	$printer->text(" ".$nvalues[0]['kot_no']."     ".$infoss[0]['waiter_id']."     ".$infoss[0]['captain_id']."    ".date('H:i', strtotime($nvalues[0]['time_added']))."");
									    $printer->feed(1);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setEmphasis(true);
									    $kot_no_string = '';
									    if($this->model_catalog_order->get_settings('KOT_RATE_AMT') == 1){
									    	$printer->text(str_pad("Name",20)." ".str_pad("Qty",8)."".str_pad("Rate",7)."".str_pad("Amt",8));
									    	$printer->feed(1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										    $total_items_normal = 0;
											$total_quantity_normal = 0;
											$total_amount_normal = 0;
									    	foreach($nvalues as $keyss => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['qty'],8)."".str_pad($nvalue['rate'],7)."".str_pad($nvalue['amt'],8));
										    	if($nvalue['message'] != ''){
										    		$printer->setTextSize(1, 1);
										    		$printer->feed(1);
										    		$printer->text("(".wordwrap($nvalue['message'],30,"\n").")");
										    	}
										    	$printer->feed(1);
										    	if($modifierdata != array()){
											    	foreach($modifierdata as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
													    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['qty'],8)."".str_pad($modata['rate'],7)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
								    			$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
								    			$total_amount_normal = $total_amount_normal + $nvalue['amt'];
								    			$kot_no_string .= $nvalue['kot_no'].",";
										    }
								    		$total_g = $infoss[0]['ftotal'] + $infoss[0]['ltotal'];
									    
										    $printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
									    	$printer->text("T Items: ".str_pad($total_items_normal,5)."T quantity :".str_pad($total_quantity_normal,5)."F.Total :".$total_amount_normal);
								    		$printer->feed(1);
								    		$printer->setTextSize(2, 1);
								    		$printer->text("G.Total :".$total_g );
								    		$printer->feed(2);
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    $printer->cut();
											$printer->feed(2);
									    } else {
										    $printer->text("Qty     Description");
										    $printer->feed(1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										    $total_items_cancel = 0;
											$total_quantity_cancel = 0;
										    foreach($nvalues as $keyss => $valuess){
										    	$printer->setTextSize(2, 1);
									    	  	$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
										    	$qty = explode(".", $valuess['qty']);
										    	$printer->text($qty['0']." ".str_pad($valuess['name'],17,"\n"));
										    	if($valuess['message'] != ''){
										    		//$printer->setTextSize(1, 1);
										    		$printer->setTextSize(2, 1);
										    		$printer->feed(1);
										    		$printer->text("(".wordwrap($valuess['message'],50,"\n").")");
										    	}
									    		$printer->feed(1);
									    		foreach($modifierdata as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $valuess['id']){
									    				foreach($value as $modata){
												    		$printer->text(str_pad("",5)."".$modata['qty']." ".wordwrap($modata['name'],30,"\n"));
												    		$printer->feed(1);
											    		}
											    	}
										    	}
										    	$total_items_cancel ++ ;
										    	$total_quantity_cancel = $total_quantity_cancel + $valuess['qty'];
										    	$kot_no_string .= $valuess['kot_no'].",";
									    	}
									    	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setEmphasis(true);
										    $printer->text("T Qty CANCEL :  ".$total_quantity_cancel."     T Item CANCEL :  ".$total_items_cancel."");
										    $printer->feed(2);
										    $printer->setJustification(Printer::JUSTIFY_CENTER);
										    $printer->cut();
											$printer->feed(2);
										}
									$printer->close();
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
								if(isset($kot_no_string)){
								    $kot_no_string = rtrim($kot_no_string, ',');
								    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE kot_no IN (".$kot_no_string.") AND order_id = '".$order_id."'");
								} else {
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
								}
							    $this->session->data['warning'] = $printername." "."Not Working";
							    //continue;
							}
						}
					}
				}
			}
		}else {
			$final_datas = array();
			$json = array();
			$json = array(
				'status' => 0,
				'final_datas' => $final_datas,
				'LOCAL_PRINT' =>0,

				
			);

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));	

		}


		// echo '<pre>';
		// print_r($edit);
		// echo '</br>';\
		// print_r($infoss[0]['bill_status']);
		// exit;

		$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
		if($SETTLEMENT_status==1){
			if(isset($this->session->data['credit'])){
				$credit = $this->session->data['credit'];
			}
			if(isset($this->session->data['cash'])){
				$cash = $this->session->data['cash'];
			}
			if(isset($this->session->data['online'])){
				$online = $this->session->data['online'];
			}
			if(isset($this->session->data['onac'])){
				$onac = $this->session->data['onac'];
			}
		}
	

		if(($infoss[0]['bill_status'] == 0 && $edit == '0') || ($infoss[0]['bill_status'] != 0 && $edit == '1')){
			if($LOCAL_PRINT == 0 && ($direct_bill == 1 || $edit == '1')){
				
				$merge_datas = array();
				$ansb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
				if($edit == '0'){
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates = $this->db->query($last_open_date_sql);
						if($last_open_dates->num_rows > 0){
							$last_open_date = $last_open_dates->row['bill_date'];
						} else {
							$last_open_date = date('Y-m-d');
						}
					}

					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
						$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
						if($last_open_dates_liq->num_rows > 0){
							$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
						} else {
							$last_open_date_liq = date('Y-m-d');
						}
					}

					$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
					$last_open_dates_order = $this->db->query($last_open_date_sql_order);
					if($last_open_dates_order->num_rows > 0){
						$last_open_date_order = $last_open_dates_order->row['bill_date'];
					} else {
						$last_open_date_order = date('Y-m-d');
					}
					
					$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					
					if($ordernogenrated['order_no'] == '0'){
						$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
						$orderno = 1;
						if(isset($orderno_q['order_no'])){
							$orderno = $orderno_q['order_no'] + 1;
						}
						
						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else{
							$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
							if($kotno2->num_rows > 0){
								$kot_no2 = $kotno2->row['billno'];
								$kotno = $kot_no2 + 1;
							} else {
								$kotno = 1;
							}
						}

						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else{
							$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
							if($kotno1->num_rows > 0){
								$kot_no1 = $kotno1->row['billno'];
								$botno = $kot_no1 + 1;
							}else {
								$botno = 1;
							}
						}

						$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");
						$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0'");

						// echo'<pre>';
						// print_r($this->session->data);
						// exit;
						if(isset($this->session->data['cash'])){
							$cashh = $this->session->data['cash'];
						} else{
							$cashh = '';
						}

						if(isset($this->session->data['credit'])){
							$creditt = $this->session->data['credit'];
						} else{
							$creditt = '';
						}

						if(isset($this->session->data['online'])){
							$onlinee = $this->session->data['online'];
						} else{
							$onlinee = '';
						}

						if(isset($this->session->data['onac'])){
							$onacc = $this->session->data['onac'];
						} else{
							$onacc = '';
						}

						// echo $cashh;
						// echo'<br />';
						// echo $creditt;
						// exit;
						if(isset($this->session->data['payment_type'])){
							$payment_type = $this->session->data['payment_type'];
							if($payment_type==0){
								$payment_type = '';
							}
						} else{
							$payment_type = '';
						}

						if(($onacc != '0' && $onacc != '') && ($cashh != '0' && $cashh != '')){
							$tot_pay = $onacc + $cashh;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_cash = '".$cashh."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

						}elseif(($onacc != '0' && $onacc != '') && ($creditt != '0' && $creditt != '')){
							$tot_pay = $onacc + $creditt;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_card = '".$creditt."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

						}elseif(($onacc != '0' && $onacc != '') && ($onlinee != '0' && $onlinee != '')){
							$tot_pay = $onacc + $onlinee;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."', pay_online = '".$onlinee."', onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
						
						}elseif(($cashh != '0' && $cashh != '') && ($creditt != '0' && $creditt != '')){
							$tot_pay = $cashh + $creditt;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."',pay_card = '".$creditt."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";

						
						}elseif(($cashh != '0' && $cashh != '') && ($onlinee != '0' && $onlinee != '')){
							$tot_pay = $cashh + $onlinee;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."' , pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


						}elseif(($creditt != '0' && $creditt != '') && ($onlinee != '0' && $onlinee != '')){
							$tot_pay =  $creditt + $onlinee;
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."',pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$tot_pay."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";


						}elseif($cashh != '0' && $cashh != ''){

							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$cashh."', payment_status = '1', total_payment = '".$cashh."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							// unset($this->session->data['cash']);
							// unset($this->session->data['credit']);
							// unset($this->session->data['online']);
							// unset($this->session->data['onac']);
							// unset($this->session->data['onaccust']);
							// unset($this->session->data['onaccontact']);
							// unset($this->session->data['onacname']);

						}elseif( $creditt != '0' && $creditt != ''){
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_card = '".$creditt."', payment_status = '1', total_payment = '".$creditt."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							// unset($this->session->data['cash']);
							// unset($this->session->data['credit']);
							// unset($this->session->data['online']);
							// unset($this->session->data['onac']);
							// unset($this->session->data['onaccust']);
							// unset($this->session->data['onaccontact']);
							// unset($this->session->data['onacname']);
						}elseif( $onlinee != '0' && $onlinee != ''){
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_online = '".$onlinee."',payment_type='".$payment_type."', payment_status = '1', total_payment = '".$onlinee."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							// unset($this->session->data['cash']);
							// unset($this->session->data['credit']);
							// unset($this->session->data['online']);
							// unset($this->session->data['onac']);
							// unset($this->session->data['onaccust']);
							// unset($this->session->data['onaccontact']);
							// unset($this->session->data['onacname']);
						}elseif( $onacc != '0' && $onacc != ''){
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', onac = '".$onacc."',onaccust = '".$this->session->data['onaccust']."', onaccontact = '".$this->session->data['onaccontact']."', onacname = '".$this->session->data['onacname']."', payment_status = '1', total_payment = '".$this->session->data['onac']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
							// unset($this->session->data['cash']);
							// unset($this->session->data['credit']);
							// unset($this->session->data['online']);
							// unset($this->session->data['onac']);
							// unset($this->session->data['onaccust']);
							// unset($this->session->data['onaccontact']);
							// unset($this->session->data['onacname']);
						

						} elseif($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ansb['grand_total']."', payment_status = '1', total_payment = '".$ansb['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
						} else{
							$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
						}
						$ordernogenrated['order_no'] = $orderno;
						$this->db->query($update_sql);
						$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
						if($apporder['app_order_id'] != '0'){
							$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
						}
					} 
					if($ordernogenrated['order_no'] != '0'){
						$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$ordernogenrated['order_no']."' AND `order_no` <> '".$ordernogenrated['order_no']."' ")->rows;
					}
				}

				$anssb = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				$infoslb = array();
				$infosb = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();
				foreach ($anssb as $lkey => $result) {
					foreach($anssb as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
							if ($decimal_mesurement == 0) {
									$qty = (int)$result['qty'];
							} else {
									$qty = $result['qty'];
							}
						if($result['is_liq']== 0){
							$infosb[] = array(
								'billno'		=> $result['billno'],
								'id'			=> $result['id'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infoslb[] = array(
								'billno'		=> $result['billno'],
								'id'			=> $result['id'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}
				
				if($ansb['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
					} else{
						$gtotal = ($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']) + ($ansb['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ansb['ftotal']+$ansb['ltotal'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
					} else{
						$gtotal =($ansb['ftotal']+$ansb['ltotal']+$ansb['gst']+$ansb['vat'])-($ansb['ftotalvalue'] + $ansb['ltotalvalue']);
					}
				}

				//$onac = $this->session->data['onac'];

			// echo'<pre>';
			// print_r($this->session->data);
			// exit;
			

				$csgst=$ansb['gst'] / 2;
				$csgsttotal = $ansb['gst'];

				$ansb['cust_name'] = utf8_substr(html_entity_decode($ansb['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['cust_address'] = utf8_substr(html_entity_decode($ansb['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['location'] = utf8_substr(html_entity_decode($ansb['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['waiter'] = utf8_substr(html_entity_decode($ansb['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ansb['ftotal'] = utf8_substr(html_entity_decode($ansb['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['ltotal'] = utf8_substr(html_entity_decode($ansb['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['cust_contact'] = utf8_substr(html_entity_decode($ansb['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ansb['t_name'] = utf8_substr(html_entity_decode($ansb['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['captain'] = utf8_substr(html_entity_decode($ansb['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ansb['vat'] = utf8_substr(html_entity_decode($ansb['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				if($ansb['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ansb['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = round($gtotal);


				if((($infos_cancel == array() && $infos_liqurcancel == array()) || $edit == '1') && $bill_copy > 0){
					$locationData =  $this->db->query("SELECT `parcel_detail` FROM oc_location WHERE location_id = '".$ansb['location_id']."'");
					if($locationData->num_rows > 0){
						$parcel_detail = $locationData->row['parcel_detail'];
					} else {
						$parcel_detail = 0;
					}


					$printtype = $bill_printer_type;
		 			$printername = $bill_printer_name;
					if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
						$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
					 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
					}

					$this->log->write("Innnnn Bill Function How MAny Times");

					//echo'innnn';
					// echo'<pre>';
					// print_r($bill_copy);
					// exit;
				
					$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
					if($printerModel ==0){

						try {
					    	if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							    $printer->feed(1);
							   	//$printer->setFont(Printer::FONT_B);
								for($i = 1; $i <= $bill_copy; $i++){
									$this->log->write("Innnnn Bill Function How MAny Times".$bill_copy);

								    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    //$printer->setJustification(Printer::JUSTIFY_CENTER);
								    //$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	$printer->feed(1);
								    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
										
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Name :".$ansb['cust_name']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
									    $printer->text("Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infosb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),11)."Bill no: ".str_pad($infosb[0]['billno'],6)."Time:".date('h:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infosb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
												    		$printer->text(str_pad(".".$modata['name'],29)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			}
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
											$printer->feed(1);
										} elseif($ansb['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",30)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",30)."CCGST :".$csgst."");
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",33)."SCRG :".$ansb['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											} else{
												$printer->text(str_pad("",33)."SCRG :".$ansb['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ansb['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
										$printer->setEmphasis(false);
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infoslb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time :".date('h:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infoslb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    }
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
											$printer->feed(1);
										} elseif($ansb['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",32)."VAT :".$ansb['vat']."");
										$printer->feed(1);
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",31)."SCRG :".$ansb['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											} else{
												$printer->text(str_pad("",31)."SCRG :".$ansb['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ansb['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									if($ansb['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_RIGHT);
									$printer->text(str_pad("",28)."GRAND TOTAL  :  ".$gtotal);
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ansb['dtotalvalue']!=0){
										$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
										$printer->feed(1);
									}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									// echo $SETTLEMENT_status;
									// exit;
									if($SETTLEMENT_status == '1'){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: ".$onacname."");
											$printer->feed(1);
											$printer->text("Contact: ".$onaccontact."");
										}
									}
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number :".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
											$printer->feed(1);
											$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
											}
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("----------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								    // Close printer //
								}
							    $printer->close();
							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
						}
						
					}
					else{  // 45 space code starts from here


						try {
					    	if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	}
						    if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	$printer->setTextSize(2, 1);
							   	$printer->setJustification(Printer::JUSTIFY_CENTER);
							    $printer->feed(1);
							    // $printer->text('45 Spacess');
				    			$printer->feed(1);
							   	//$printer->setFont(Printer::FONT_B);
								for($i = 1; $i <= $bill_copy; $i++){
									$this->log->write("Innnnn Bill Function How MAny Time2222222s".$bill_copy);

								    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    //$printer->setJustification(Printer::JUSTIFY_CENTER);
								    //$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	$printer->feed(1);
								    if($ansb['cust_contact'] == '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' &&  $ansb['gst_no'] == ''){
										
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
										$printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] == '' && $ansb['cust_contact'] != '' && $ansb['cust_address'] != '' && $ansb['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ansb['cust_contact'],25)."Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_name'] != '' && $ansb['cust_contact'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Name :".$ansb['cust_name']."");
									    $printer->feed(1);
									}
									else if($ansb['cust_contact'] != '' && $ansb['cust_name'] == '' && $ansb['cust_address'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									}else if($ansb['cust_address'] != '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == '' && $ansb['gst_no'] == ''){
									    $printer->text("Address : ".$ansb['cust_address']."");
									    $printer->feed(1);
									}else if( $ansb['gst_no'] != '' && $ansb['cust_address'] == '' && $ansb['cust_name'] == '' && $ansb['cust_contact'] == ''){
									    $printer->text("Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ansb['cust_name'],25)."Mobile :".$ansb['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ansb['cust_address'],25)."Gst No :".$ansb['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ansb['login_id'],20)."K.Ref.No :".$infoss[0]['order_id']);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infosb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infosb[0]['billno'],5)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)." ".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infosb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
						    			}
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty:".str_pad($total_quantity_normal,7)."F.Total :".$ansb['ftotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['fdiscountper']."%) :".$ansb['ftotalvalue']."");
											$printer->feed(1);
										} elseif($ansb['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ftotalvalue']."rs):".$ansb['ftotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",25)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",25)."CCGST :".$csgst."");
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											} else{
												$printer->text(str_pad("",25)."SCRG :".$ansb['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ansb['ftotal']) - ($disamtfood)) + ($ansb['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ansb['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ansb['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
										$printer->setEmphasis(false);
									}
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infoslb){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ansb['bill_date'])),13)."Bill no: ".str_pad($infoslb[0]['billno'],5)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$ansb['order_no']."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)."".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ansb['location'],8)."".str_pad($ansb['t_name'],8)."".str_pad($ansb['waiter_id'],8)."".str_pad($ansb['captain_id'],8)."".$ansb['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",7)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infoslb as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8');
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
									    	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
												    		$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
													    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
													    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}
									    	$total_items_normal ++ ;
						    				$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
									    }
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T QTY :".str_pad($total_quantity_normal,7)."L.Total :".$ansb['ltotal']);
										$printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ansb['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ldiscountper']."%) :".$ansb['ltotalvalue']."");
											$printer->feed(1);
										} elseif($ansb['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ansb['ltotalvalue']."rs) :".$ansb['ltotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",21)."VAT :".$ansb['vat']."");
										$printer->feed(1);
										if($ansb['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											} else{
												$printer->text(str_pad("",21)."SCRG :".$ansb['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ansb['vat'] + $ansb['ltotal']) - ($disamtliq)) + ($ansb['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ansb['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ansb['vat'] + $ansb['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									if($ansb['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",38).$ansb['advance_amount']."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->text("GRAND TOTAL : ".$gtotal);
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ansb['dtotalvalue']!=0){
										$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
										$printer->feed(1);
									}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									if($SETTLEMENT_status == '1'){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: '".$onacname."'");
											$printer->feed(1);
											$printer->text("Contact: '".$onaccontact."'");
										}
									}
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number :".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],20)."SGST :".$mvalue['gst']/2);
											$printer->feed(1);
											$printer->text(str_pad("",20)."CGST :".$mvalue['gst']/2);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],20)."VAT :".$mvalue['vat']."");
											}
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
											$printer->feed(1);
											if($ansb['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",35).$gtotal."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								    // Close printer //
								}
							    $printer->close();

							    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							$this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
						}
					}
					if($parcel_detail == 1){
						$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
						$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
						$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
						$hote_name=str_replace(" ", "%20", $HOTEL_NAME);
						$i =1;
						$food_items = '';
						$liq_items ='';
						foreach($infosb as $nkey => $nvalue){
							$food_items .= $i.'-'.$nvalue['name'].','.'%0a';
							$i++;
						}
						$l = $i;
						foreach($infoslb as $nakey => $navalue){
							$liq_items .= $l.'-'.$navalue['name'].','.'%0a';
							$l++;
						}

						$text =  $hote_name.'%0a'.$food_items.''.$liq_items.'%0a'.'Grand Total :- '.$gtotal;
						$msg = str_replace(" ", "%20", $text);

						$link= $link_1.$ansb['cust_contact']."&message=".$msg;
						// echo'<pre>';
						// print_r($link);
						// exit;
						if($ansb['cust_contact'] != ''){
							file($link);
						}
					}
				}
				$json = array();
				$json = array(
					'direct_bill' => $direct_bill,
					'order_id' => $order_id,
					'grand_total' => $gtotal,
					'status' => 1,
					'orderidmodify' =>$order_id,
					'edit' => $edit,
				);
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				unset($this->session->data['cash']);
				unset($this->session->data['credit']);
				unset($this->session->data['online']);
				unset($this->session->data['onac']);
				unset($this->session->data['onaccust']);
				unset($this->session->data['onaccontact']);
				unset($this->session->data['onacname']);
			} else {
				$local_print = $this->model_catalog_order->get_settings('LOCAL_PRINT');

				$final_datas = array();
				$json = array();
				$json = array(
					'status' => 0,
					'final_datas' => $final_datas,
					'LOCAL_PRINT' => $local_print,
				);

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));	
				unset($this->session->data['cash']);
				unset($this->session->data['credit']);
				unset($this->session->data['online']);
				unset($this->session->data['onac']);
				unset($this->session->data['onaccust']);
				unset($this->session->data['onaccontact']);
				unset($this->session->data['onacname']);
			}
		} else{
			$json = array();
			$json = array(
				'status' => 0,
				'localprints' => $local_print,

			);
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			//$this->session->data['warning'] = 'Bill already printed';
		}
		
	}

	public function printsb() {
		// $this->log->write("---------------------------Print Bill Start-------------------------");
		// $this->log->write('User Name :' .$this->user->getUserName());
		// $this->log->write("order_id : " .$this->request->get['order_id']);

		// echo'inn';
		// exit();

		$this->log->write("printsb");

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		// 	echo'<pre>';
		// print_r($ans);
		// exit;
			if(isset($this->request->get['duplicate'])){
				$duplicate = $this->request->get['duplicate'];
			} else{
				$duplicate = '0';
			}

			if(isset($this->request->get['advance_id'])){
				$advance_id = $this->request->get['advance_id'];
			} else{
				$advance_id = '0';
			}

			if(isset($this->request->get['advance_amount'])){
				$advance_amount = $this->request->get['advance_amount'];
			} else{
				$advance_amount = '0';
			}

			// echo'<pre>';
			// print_r( $this->request->get['grand_total']);
			// exit;

			if(isset($this->request->get['grand_total'])){
				$grand_total = $this->request->get['grand_total'];
			} else{
				$grand_total = '0';
			}
			$orderno = '0';

			if(($ans['bill_status'] == 0 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
				date_default_timezone_set('Asia/Kolkata');
				$this->load->language('customer/customer');
				$this->load->model('catalog/order');
				$merge_datas = array();
				$this->document->setTitle('BILL');
				$te = 'BILL';
				
				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}
				}

				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

				if($last_open_dates_liq->num_rows > 0){
					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
				} else {
					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";

					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);

					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq = date('Y-m-d');
					}
				}

				$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
				$last_open_dates_order = $this->db->query($last_open_date_sql_order);
				if($last_open_dates_order->num_rows > 0){
					$last_open_date_order = $last_open_dates_order->row['bill_date'];
				} else {
					$last_open_date_order = date('Y-m-d');
				}

				if($ans['order_no'] == '0'){
					$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
					$orderno = 1;
					if(isset($orderno_q['order_no'])){
						$orderno = $orderno_q['order_no'] + 1;
					}

					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
					if($kotno2->num_rows > 0){
						$kot_no2 = $kotno2->row['billno'];
						$kotno = $kot_no2 + 1;
					} else{
						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else {
							$kotno = 1;
						}
					}

					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
					if($kotno1->num_rows > 0){
						$kot_no1 = $kotno1->row['billno'];
						$botno = $kot_no1 + 1;
					} else{
						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else {
							$botno = 1;
						}
					}

					$this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
					$this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

					//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
					} else{
						$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
					}
					$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					if($apporder['app_order_id'] != '0'){
						$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
					}
					$this->db->query($update_sql);
				}


				if($advance_id != '0'){
					$this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
				}
				$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

				$testfood = array();
				$testliq = array();
				$testtaxvalue1food = 0;
				$testtaxvalue1liq = 0;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				
				$infosl = array();
				$infos = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();

				// echo'<pre>';
				// print_r($anss);
				// echo'<br>';
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}
						if($result['is_liq']== 0){
							$infos[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infosl[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}

				
				
				$merge_datas = array();
				if($orderno != '0'){
					$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
				}
				
				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else {
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else {
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}

				$l_datasss =  $this->db->query("SELECT `parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
				if($l_datasss->num_rows > 0){
					$p_detail = $l_datasss->row['parcel_detail'];
				} else {
					$p_detail = 0;
				}

				if($duplicate == '1'){
					$this->db->query("UPDATE `oc_order_info` SET duplicate = '1', duplicate_time = '".date('h:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE `order_id` = '".$order_id."'");
					$duplicatebilldata = $this->db->query("SELECT `order_no`, grand_total, item_quantity, `login_name`, `duplicate_time` FROM `oc_order_info` WHERE `duplicate` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND day_close_status = '0' AND order_id = '".$order_id."'");
					$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
					$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
					$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
					if ($setting_value_link_1 != '') {
						$text = "Duplicate%20Bill%20-%20Ref%20No%20:".$duplicatebilldata->row['order_no'].",%20Qty%20:".$duplicatebilldata->row['item_quantity'].",%20Amt%20:".$duplicatebilldata->row['grand_total'].",%20login%20name%20:".$duplicatebilldata->row['login_name'].",%20Time%20:".$duplicatebilldata->row['duplicate_time'];
						$link = $link_1."&phone=".$setting_value_number."&text=".$text;
						//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
						//echo $link;exit;
						$ret = file($link);
					} 
				}
				
				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				// echo'<pre>';
				// print_r($ans);
				// exit;
				$ansz = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;

				if($ansz['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ansz['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = round($gtotal);

				$printtype = '';
				$printername = '';

				if ($printtype == '' || $printername == '' ) {
					$printtype = $this->user->getPrinterType();
					$printername = $this->user->getPrinterName();
					$bill_copy = 1;
				}

				if ($printtype == '' || $printername == '' ) {
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name`,`parcel_detail` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$printtype = $locationData['bill_printer_type'];
						$printername = $locationData['bill_printer_name'];
						$bill_copy = $locationData['bill_copy'];
					} else{
						$printtype = '';
						$printername = '';
						$bill_copy = 1;
					}
				}
				
				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}
				$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
				$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');

				if($LOCAL_PRINT == 0){

					if($printerModel ==0){
				// 		echo'<pre>';
				// print_r($infos);
				// exit;
						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	for($i = 1; $i <= $bill_copy; $i++){
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ans['login_id'],10)."    ".str_pad("Ref no: ".$orderno,10)."     ".str_pad("K.Ref.No :".$order_id,10));
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
									   	 	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											// foreach($testfoods as $tkey => $tvalue){
											// 	$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										 //    	$printer->feed(1);
											// }
											// $printer->text("----------------------------------------------");
											// $printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("",23)."SCGST (2.5) :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",23)."CCGST (2.5) :".$csgst."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													// $printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
													// $printer->feed(1);
													$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}else{
													// $printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
													// $printer->feed(1);
													$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ans['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
								   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);	
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infosl){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_CENTER);
									   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('H:i:s'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$orderno."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_liquor_normal = 0;
										    $total_quantity_liquor_normal = 0;
										    foreach($infosl as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
										    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
															    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}

										    	$total_items_liquor_normal ++;
										    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
										    }
										    $printer->text("----------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("----------------------------------------------");
											$printer->feed(1);
											foreach($testliqs as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ans['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													// $printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
													// $printer->feed(1);
													$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												} else{
													// $printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
													// $printer->feed(1);
													$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ans['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansz['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text("GRAND TOTAL  :  ".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ans['dtotalvalue']!=0){
												$printer->text("Delivery Charge:".$ans['dtotalvalue']);
												$printer->feed(1);
											}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
												$printer->feed(1);
											}

										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   
										if($merge_datas){
											$printer->feed(2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("Merged Bills");
											$printer->feed(1);
											$mcount = count($merge_datas) - 1;
											foreach($merge_datas as $mkey => $mvalue){
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text("Bill Number :".$mvalue['order_no']."");
												$printer->feed(1);
												$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
												$printer->feed(1);
												$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
												$printer->feed(1);
												if($mvalue['ltotal'] > '0'){
													$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
												}
												$printer->feed(1);
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													}
												}
												$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
												$printer->feed(1);
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													}
												}
												if($mkey < $mcount){ 
													$printer->text("----------------------------------------------");
													$printer->feed(1);
												}
											}
											$printer->feed(1);
											$printer->text("----------------------------------------------");
											$printer->feed(1);
											$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
											$printer->feed(1);
										}
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
						
					}
					else{  // 45 space code starts from here

						try {
							    if($printtype == 'Network'){
							 		$connector = new NetworkPrintConnector($printername, 9100);
							 	} else if($printtype == 'Windows'){
							 		$connector = new WindowsPrintConnector($printername);
							 	} else {
							 		$connector = '';
							 		$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							 	}
							 	if($connector != ''){
								    $printer = new Printer($connector);
								    $printer->selectPrintMode(32);

								   	$printer->setEmphasis(true);
								   	// $printer->text('45 Spacess');
					    			$printer->feed(1);
								   	for($i = 1; $i <= $bill_copy; $i++){
									   	$printer->setTextSize(2, 1);
									   	$printer->setJustification(Printer::JUSTIFY_CENTER);
									   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
									    $printer->feed(1);
									    $printer->setTextSize(1, 1);
									    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
									    if($ans['bill_status'] == 1 && $duplicate == '1'){
									    	$printer->feed(1);
									    	$printer->text("Duplicate Bill");
									    }
									    $printer->setJustification(Printer::JUSTIFY_CENTER);
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
										if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
											
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
											$printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
											$printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
										 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
										 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}
										else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Name :".$ans['cust_name']."");
										    $printer->feed(1);
										}
										else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
										    $printer->text("Address : ".$ans['cust_address']."");
										    $printer->feed(1);
										}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
										    $printer->text("Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}else{
										    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
										    $printer->feed(1);
										    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
										    $printer->feed(1);
										}
										$printer->text(str_pad("User Id :".$ans['login_id'],20)."K.Ref.No :".$order_id);
									    $printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
									   	if($infos){
									   			
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$orderno."");
									   		$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									   	 	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_normal = 0;
											$total_quantity_normal = 0;
										    foreach($infos as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
												    	foreach($modifierdatabill as $key => $value){
											    			$printer->setTextSize(1, 1);
											    			if($key == $nvalue['id']){
											    				foreach($value as $modata){
											    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
														    		$printer->feed(1);
													    		}
													    	}
											    		}
											    	}
											    	$total_items_normal ++ ;
										    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											foreach($testfoods as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['fdiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
												$printer->feed(1);
											} elseif($ans['discount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",25)."SCGST :".$csgst."");
											$printer->feed(1);
											$printer->text(str_pad("",25)."CCGST :".$csgst."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}else{
													$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
													$printer->feed(1);
													$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountfood = ($ans['ftotal'] - ($disamtfood));
												} else{
													$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
											$printer->setEmphasis(false);
										}
										
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
									   	$printer->setTextSize(1, 1);
								   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
								   			$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->feed(1);	
											$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
											if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
												$printer->feed(1);				   		
										   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
									   		}
									   		$printer->feed(1);	
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   	}
									   	if($infosl){
									   		$printer->feed(1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infosl[0]['billno'],6)."Time:".date('H:i'));
									   		$printer->feed(1);
									   		$printer->text("Ref no: ".$orderno."");
									 		$printer->feed(1);
									 		$printer->setEmphasis(true);
									   		$printer->setTextSize(1, 1);
									   		$printer->setJustification(Printer::JUSTIFY_LEFT);
									    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
									    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
									    	$printer->feed(1);
									   		$printer->setTextSize(1, 1);
									   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
										    $printer->feed(1);
									   		$printer->setEmphasis(false);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
											$printer->feed(1);
									    	$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setEmphasis(false);
											$total_items_liquor_normal = 0;
										    $total_quantity_liquor_normal = 0;
										    foreach($infosl as $nkey => $nvalue){
										    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
										    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
										    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
										    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
										    	$printer->feed(1);
									    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
															    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
															    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}

										    	$total_items_liquor_normal ++;
										    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
										    }
										    $printer->text("------------------------------------------");
										    $printer->feed(1);
										    $printer->setJustification(Printer::JUSTIFY_LEFT);
										    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
										    $printer->feed(1);
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										    $printer->text("------------------------------------------");
											$printer->feed(1);
											foreach($testliqs as $tkey => $tvalue){
												$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
										    	$printer->feed(1);
											}
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											if($ans['ldiscountper'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											} elseif($ans['ldiscount'] != '0'){
												$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
												$printer->feed(1);
											}
											$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												} else{
													$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
													$printer->feed(1);
													$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
												} else{
													$netamountliq = ($ans['ltotal'] - ($disamtliq));
												}
											}
											$printer->setEmphasis(true);
											$printer->text(str_pad("",25)."Net total :".ceil($netamountliq)."");
										    $printer->setEmphasis(false);
										   	$printer->setTextSize(1, 1);
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(true);
										if($ansz['advance_amount'] != '0.00'){
											$printer->text(str_pad("Advance Amount",38).$ansz['advance_amount']."");
											$printer->feed(1);
										}
										$printer->setTextSize(2, 2);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text("GRAND TOTAL : ".$gtotal);
										$printer->setTextSize(1, 1);
										$printer->feed(1);
										if($ans['dtotalvalue']!=0){
												$printer->text(str_pad("",20)."Delivery Charge:".$ans['dtotalvalue']);
												$printer->feed(1);
											}
										$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
										if($SETTLEMENT_status == '1'){
											if(isset($this->session->data['credit'])){
												$credit = $this->session->data['credit'];
											} else {
												$credit = '0';
											}
											if(isset($this->session->data['cash'])){
												$cash = $this->session->data['cash'];
											} else {
												$cash = '0';
											}
											if(isset($this->session->data['online'])){
												$online = $this->session->data['online'];
											} else {
												$online ='0';
											}

											if(isset($this->session->data['onac'])){
												$onac = $this->session->data['onac'];
												$onaccontact = $this->session->data['onaccontact'];
												$onacname = $this->session->data['onacname'];

											} else {
												$onac ='0';
											}
										}
										if($SETTLEMENT_status=='1'){
											if($credit!='0' && $credit!=''){
												$printer->text("PAY BY: CARD");
											}
											if($online!='0' && $online!=''){
												$printer->text("PAY BY: ONLINE");
											}
											if($cash!='0' && $cash!=''){
												$printer->text("PAY BY: CASH");
											}
											if($onac!='0' && $onac!=''){
												$printer->text("PAY BY: ON.ACCOUNT");
												$printer->feed(1);
												$printer->text("Name: '".$onacname."'");
												$printer->feed(1);
												$printer->text("Contact: '".$onaccontact."'");
											}
										}
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									   
										if($merge_datas){
											$printer->feed(2);
											$printer->setJustification(Printer::JUSTIFY_CENTER);
											$printer->text("Merged Bills");
											$printer->feed(1);
											$mcount = count($merge_datas) - 1;
											foreach($merge_datas as $mkey => $mvalue){
												$printer->setJustification(Printer::JUSTIFY_LEFT);
												$printer->text("Bill Number :".$mvalue['order_no']."");
												$printer->feed(1);
												$printer->text(str_pad("F Total :".$mvalue['ftotal'],24)."SGST :".$mvalue['gst']/2);
												$printer->feed(1);
												$printer->text(str_pad("",24)."CGST :".$mvalue['gst']/2);
												$printer->feed(1);
												if($mvalue['ltotal'] > '0'){
													$printer->text(str_pad("L Total :".$mvalue['ltotal'],24)."VAT :".$mvalue['vat']."");
												}
												$printer->feed(1);
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													} else{
														$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
													}
												}
												$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
												$printer->feed(1);
												if($ans['parcel_status'] == '0'){
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
													}
												} else{
													if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													} else{
														$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
													}
												}
												if($mkey < $mcount){ 
													$printer->text("------------------------------------------");
													$printer->feed(1);
												}
											}
											$printer->feed(1);
											$printer->text("------------------------------------------");
											$printer->feed(1);
											$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
											$printer->feed(1);
										}
										$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
										$printer->feed(1);
										if($this->model_catalog_order->get_settings('TEXT1') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT1'));
											$printer->feed(1);
										}
										if($this->model_catalog_order->get_settings('TEXT2') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT2'));
											$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										if($this->model_catalog_order->get_settings('TEXT3') != ''){
											$printer->text($this->model_catalog_order->get_settings('TEXT3'));
										}
										$printer->feed(2);
										$printer->cut();
									}
									// Close printer //
								    $printer->close();
							    	$this->db->query("UPDATE oc_order_info SET shift_id = 0 WHERE order_id = '".$order_id."'");

								    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
									$this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
								}
							} catch (Exception $e) {
							    $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
							    $this->session->data['warning'] = $printername." "."Not Working";
							}
					}

					if($p_detail == 1){
						$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
						$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
						$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
						$hote_name=str_replace(" ", "%20", $HOTEL_NAME);
						$i =1;
						$food_items = '';
						$liq_items ='';
						foreach($infos as $nkey => $nvalue){
							$food_items .= $i.'-'.$nvalue['name'].','.'%0a';
							$i++;
						}
						$l = $i;
						foreach($infosl as $nakey => $navalue){
							$liq_items .= $l.'-'.$navalue['name'].','.'%0a';
							$l++;
						}

						$text =  $hote_name.'%0a'.$food_items.''.$liq_items.'%0a'.'Grand Total :- '.$gtotal;
						$msg = str_replace(" ", "%20", $text);

						$link= $link_1.$ans['cust_contact']."&message=".$msg;
						// echo'<pre>';
						// print_r($link);
						// exit;
						if($ans['cust_contact'] != ''){
							file($link);
						}

						// if($ret[0] != 'Daily Message Limit Reached'){
						// 	$this->session->data['success'] .= "SMS Send Successfully<br>";
						// } else {
						// 	$this->session->data['success'] .= "Sub Batch ".$i." Not Send<br>";
						// }
					}

					if($ans['bill_status'] == 1 && $duplicate == '1'){
						$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
					} else{
						$json = array();
						$json = array(
							'LOCAL_PRINT' => 0,
							'status' => 1,
						);
					}
				}else {
					$json = array();
						$final_datas = array();
						$HOTEL_NAME = $this->model_catalog_order->get_settings('HOTEL_NAME');
						$HOTEL_ADD = $this->model_catalog_order->get_settings('HOTEL_ADD');
						$INCLUSIVE = $this->model_catalog_order->get_settings('INCLUSIVE');
						$BAR_NAME = $this->model_catalog_order->get_settings('BAR_NAME');
						$BAR_ADD = $this->model_catalog_order->get_settings('BAR_ADD');
						$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
						$GST_NO = $this->model_catalog_order->get_settings('GST_NO');
						$TEXT1 = $this->model_catalog_order->get_settings('TEXT1');
						$TEXT2 = $this->model_catalog_order->get_settings('TEXT2');
						$TEXT3 = $this->model_catalog_order->get_settings('TEXT3');
						$LOCAL_PRINT = $this->model_catalog_order->get_settings('LOCAL_PRINT');
						if(isset($testfoods)){
							$testfoods = $testfoods;
						} else {
							$testfoods =array();
						}

						if(isset($infosl)){
							$infosl = $infosl;
						} else {
							$infosl =array();
						}

						if(isset($testliqs)){
							$testliqs = $testliqs;
						} else {
							$testliqs =array();
						}
						$cash = 0;
						$credit = 0;
						$online = 0;
						$onac =0;
						$onaccontact = '';
						$onacname = '';
						if($SETTLEMENT_status == '1'){
							if(isset($this->session->data['credit'])){
								$credit = $this->session->data['credit'];
							} else {
								$credit = '0';
							}
							if(isset($this->session->data['cash'])){
								$cash = $this->session->data['cash'];
							} else {
								$cash = '0';
							}
							if(isset($this->session->data['online'])){
								$online = $this->session->data['online'];
							} else {
								$online ='0';
							}

							if(isset($this->session->data['onac'])){
								$onac = $this->session->data['onac'];
								$onaccontact = $this->session->data['onaccontact'];
								$onacname = $this->session->data['onacname'];

							} else {
								$onac =0;
								$onaccontact = '';
								$onacname = '';
							}
						}
										


						$final_datas = array(
							'ans' => $ans,
							'infos' => $infos,
							'modifierdatabill' => $modifierdatabill,
							'testfoods' => $testfoods,
							'infosl' => $infosl,
							'testliqs' => $testliqs,
							'merge_datas' => $merge_datas,
							'GST_NO' => $GST_NO,
							'TEXT1' => $TEXT1,
							'TEXT2' => $TEXT2,
							'TEXT3' => $TEXT3,
							'bill_copy' => $bill_copy,
							'cash' =>$cash,
							'card' =>$credit,
							'online' =>$online,
							'onac' =>$onac,
							'onaccontact' =>$onaccontact,
							'onacname' =>$onacname,

							'SETTLEMENT_status' => $SETTLEMENT_status,
							'HOTEL_NAME' => $HOTEL_NAME,
							'HOTEL_ADD' => $HOTEL_ADD,
							'INCLUSIVE' => $INCLUSIVE,
							'BAR_NAME' => $BAR_NAME,
							'orderno' => $orderno,
							'order_id' => $order_id,
							'duplicate' => $duplicate,
							'csgst' => $csgst,
							'csgsttotal' => $csgsttotal,
							'gtotal' => $gtotal,
							'status' => 1,
						);


						$json = array(
							'LOCAL_PRINT' => $LOCAL_PRINT,
							'final_datas' => $final_datas,

							'status' => 1,
						);
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			} else {
				$this->session->data['warning'] = 'Bill already printed';
				$this->request->post = array();
				$_POST = array();
				$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
			}
		}
		//$this->log->write("---------------------------Print Bill End------------------------------");

		// $this->request->post = array();
		// $_POST = array();
		// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}

	public function autocomplete5() {
		
		$json = array();
		$modifieritemdata = array();
		$json['status'] = 0;
		if(isset($this->request->get['filter_name'])) {

			$this->load->model('catalog/item');
			$filter_data = array(
				'filter_item_code' => $this->request->get['filter_name'],
				'filter_rate_id' => $this->request->get['filter_rate_id'],
				'start'       => 0,
				'limit'       => 10
			);
			$rates = array(
				'1' => 'rate_1',
				'2' => 'rate_2',
				'3' => 'rate_3',
				'4' => 'rate_4',
				'5' => 'rate_5',
				'6' => 'rate_6',
			);
			$results = $this->model_catalog_item->getItems1($filter_data);

		// 	echo'<pre>';
		// print_r($results);
		// exit;
			foreach ($results as $result) {
				if(isset($rates[$filter_data['filter_rate_id']])){
					$rate_key = $rates[$filter_data['filter_rate_id']];
				} else {
					$rate_key = 'rate_1';
				}

				$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['vat']."'");
				if($tax1->num_rows > 0){
					$taxvalue1 = $tax1->row['tax_value'];
				} else{
					$taxvalue1 = '0';
				}

				$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['tax2']."'");
				if($tax2->num_rows > 0){
					$taxvalue2 = $tax2->row['tax_value'];
				} else{
					$taxvalue2 = '0';
				}

				$modifieritems = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$result['modifier_group']."'")->rows;
				$i=0;
				foreach($modifieritems as $modifieritem){
					$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$modifieritem['modifier_item_code']."'")->row;
					$modifieritemdata[] = array(
											'modifieritem' => $modifieritem['modifier_item'],
											'itemid' => $modifieritem['modifier_item_id'],
											'itemcode' => $modifieritem['modifier_item_code'],
											'subcategoryid' => $itemdata['item_sub_category_id'],
											'default_item' => $modifieritem['default_item'],
											'modifierrate' => $itemdata[$rate_key],
											'kitchen_display'	=> $modifieritem['kitchen_dispaly'],
											'is_liq' => $itemdata['is_liq']
										);
					$i++;
				}
				// $testingnn = array(
				// 				'testitemdata' => $modifieritemdata
				// 			);
				// print_r($modifieritemdata);
				// exit();

				// $totalmodifiers = $this->db->query("SELECT COUNT(*) as count FROM oc_modifier_items WHERE modifier_id = '".$result['modifier_group']."'")->row;
				$rate = $result[$rate_key];
				if($result['modifier_group'] != '0'){
					$result['modifier_group'] = 1;
				} else{
					$result['modifier_group'] = 0;
				}
				$json = array(
					'status' => 1,
					'item_code' => $result['item_code'],
					'is_liq' => $result['is_liq'],
					'purchase_price' 	=> $rate,
					'taxvalue1'       	=> $taxvalue1,
					'taxvalue2'       	=> $taxvalue2,
					'subcategoryid'		=> $result['item_sub_category_id'],
					'subcategoryname'   => $result['item_sub_category'],
					'kitchen_display'	=> $result['kitchen_dispaly'],
					'modifier_group'   	=> $result['modifier_group'],
					'modifier_qty'   	=> $result['modifier_qty'],
					'modifieritems'   	=> $modifieritemdata,
					'totalmodifiers'   	=> $i,
					'item_name'       	=> strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['item_name'];
		// }
		// array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecustomer() {
		$json = array();

		if (isset($this->request->get['filter_contact'])) {
			$this->load->model('catalog/customer');

			$filter_data = array(
				'filter_contact' => $this->request->get['filter_contact'],
				'start'       => 0,
				'limit'       => 10
			);
			// echo'<pre>';
			// print_r($filter_data);
			// exit;
			$results = $this->model_catalog_customer->getCustomers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'cust_contact' => $result['contact'],
					'cust_name' => $result['name'],
					'cust_id' => $result['c_id'],
					'cust_address' => $result['address'],
					'cust_email' => $result['email'],
					// 'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['cust_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function autocompletecustomer1() {
		$json = array();

		if (isset($this->request->get['filter_id'])) {
			$this->load->model('catalog/customer');

			$filter_data = array(
				'filter_c_id' => $this->request->get['filter_id'],
				
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_customer->getCustomers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'cust_contact' => $result['contact'],
					'cust_name' => $result['name'],
					'cust_id' => $result['c_id'],
					'cust_address' => $result['address'],
					'cust_email' => $result['email'],
					// 'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['cust_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function autocompletename() {
		$json = array();
		$modifieritemdata = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/item');

			$filter_data = array(
				'filter_item_name' => $this->request->get['filter_name'],
				'filter_rate_id' => $this->request->get['filter_rate_id'],
				'start'       => 0,
				'limit'       => 100
			);
			$rates = array(
				'1' => 'rate_1',
				'2' => 'rate_2',
				'3' => 'rate_3',
				'4' => 'rate_4',
				'5' => 'rate_5',
				'6' => 'rate_6',
			);
			$results = $this->model_catalog_item->getItems1($filter_data);
			// echo'<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $result) {
				if(isset($rates[$filter_data['filter_rate_id']])){
					$rate_key = $rates[$filter_data['filter_rate_id']];
				} else {
					$rate_key = 'rate_1';
				}

				$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['vat']."'");
				if($tax1->num_rows > 0){
					$taxvalue1 = $tax1->row['tax_value'];
				} else{
					$taxvalue1 = '0';
				}

				$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['tax2']."'");
				if($tax2->num_rows > 0){
					$taxvalue2 = $tax2->row['tax_value'];
				} else{
					$taxvalue2 = '0';
				}

				$modifieritems = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$result['modifier_group']."'")->rows;
				$i=0;
				// echo'<pre>';
				// 	print_r($modifieritems);
				// 	exit;
				foreach($modifieritems as $modifieritem){
					$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$modifieritem['modifier_item_code']."'")->row;
					
					$modifieritemdata[] = array(
											'modifieritem' => $modifieritem['modifier_item'],
											'itemid' => $modifieritem['modifier_item_id'],
											'itemcode' => $modifieritem['modifier_item_code'],
											'subcategoryid' => $itemdata['item_sub_category_id'],
											'default_item' => $modifieritem['default_item'],
											'kitchen_display' => $modifieritem['kitchen_dispaly'],

											'modifierrate' => $itemdata[$rate_key],
											'is_liq' => $itemdata['is_liq']
										);
					$i++;
				}

				$rate = $result[$rate_key];
				if($result['modifier_group'] != '0'){
					$result['modifier_group'] = 1;
				} else{
					$result['modifier_group'] = 0;
				}
				$json[] = array(
					'item_code' 		=> $result['item_code'],
					'is_liq'			=> $result['is_liq'],
					'shortname' 		=> $result['short_name'],
					'purchase_price' 	=> $rate,
					'taxvalue1'       	=> $taxvalue1,
					'taxvalue2'       	=> $taxvalue2,
					'kitchen_display'	=> $result['kitchen_dispaly'],
					'subcategoryid'		=> $result['item_sub_category_id'],
					'subcategoryname'   => $result['item_sub_category'],
					'modifier_group'   	=> $result['modifier_group'],
					'modifieritems'   	=> $modifieritemdata,
					'totalmodifiers'   	=> $i,
					'item_name'        	=> strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function autocompletenamecode() {
		$json = array();
		$modifieritemdata = array();
		if (isset($this->request->get['filter_id'])) {
			$this->load->model('catalog/item');
			
			$filter_data = array(
				'filter_id' => $this->request->get['filter_id'],
				'filter_rate_id' => $this->request->get['filter_rate_id'],
				'start'       => 0,
				'limit'       => 100
			);
			$rates = array(
				'1' => 'rate_1',
				'2' => 'rate_2',
				'3' => 'rate_3',
				'4' => 'rate_4',
				'5' => 'rate_5',
				'6' => 'rate_6',
			);
			$results = $this->model_catalog_item->getItemscode($filter_data);
			foreach ($results as $result) {
				if(isset($rates[$filter_data['filter_rate_id']])){
					$rate_key = $rates[$filter_data['filter_rate_id']];
				} else {
					$rate_key = 'rate_1';
				}

				$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['vat']."'");
				if($tax1->num_rows > 0){
					$taxvalue1 = $tax1->row['tax_value'];
				} else{
					$taxvalue1 = '0';
				}

				$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['tax2']."'");
				if($tax2->num_rows > 0){
					$taxvalue2 = $tax2->row['tax_value'];
				} else{
					$taxvalue2 = '0';
				}

				$modifieritems = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$result['modifier_group']."'")->rows;
				$i=0;
				foreach($modifieritems as $modifieritem){
					$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$modifieritem['modifier_item_code']."'")->row;
					$modifieritemdata[] = array(
											'modifieritem' => $modifieritem['modifier_item'],
											'itemid' => $modifieritem['modifier_item_id'],
											'itemcode' => $modifieritem['modifier_item_code'],
											'subcategoryid' => $itemdata['item_sub_category_id'],
											'default_item' => $modifieritem['default_item'],
											'modifierrate' => $itemdata[$rate_key],
											'kitchen_display' => $modifieritem['kitchen_dispaly'],

											'is_liq' => $itemdata['is_liq']
										);
					$i++;
				}

				$rate = $result[$rate_key];
				if($result['modifier_group'] != '0'){
					$result['modifier_group'] = 1;
				} else{
					$result['modifier_group'] = 0;
				}
				$json[] = array(
					'item_code' 		=> $result['item_code'],
					'is_liq'			=> $result['is_liq'],
					'shortname' 		=> $result['short_name'],
					'purchase_price' 	=> $rate,
					'taxvalue1'       	=> $taxvalue1,
					'taxvalue2'       	=> $taxvalue2,
					'kitchen_display'	=> $result['kitchen_dispaly'],
					'subcategoryid'		=> $result['item_sub_category_id'],
					'subcategoryname'   => $result['item_sub_category'],
					'modifier_group'   	=> $result['modifier_group'],
					'modifieritems'   	=> $modifieritemdata,
					'totalmodifiers'   	=> $i,
					'item_name'        	=> strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function autocompletekotmsg() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * from oc_kotmsg WHERE message LIKE '%" . $this->request->get['filter_name'] . "%' limit 0,15 ")->rows;

			foreach ($results as $result) {
				$json[] = array(
					'msg_code' => strip_tags(html_entity_decode($result['msg_code'], ENT_QUOTES, 'UTF-8')),
					'message'  => strip_tags(html_entity_decode($result['message'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {  
			$sort_order[$key] = $value['message'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function tableinfo() {
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;
		// $this->log->write("---------------------------Table Info  Start------------------------------------");
		// $this->log->write("User Name :".$this->user->getUserName());

		// $this->log->write($this->request->get);
		
		$this->load->model('catalog/order');
		$tid = $this->request->get['tid'];
		$lid = $this->request->get['lid'];
		$edit = $this->request->get['edit'];
		$editorderid = $this->request->get['editorderid'];
		$tab_index = $this->request->get['tab_index'];
		$parentdatas = array();

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}
		// $this->log->write("Last Date");
		
		// $this->log->write($last_open_date);


		/*
		$waiter_pre_datas =  $this->db->query("SELECT * FROM oc_order_info WHERE table_id = '".$tid."' AND `date` = '".$last_open_date."' order by waiter_id desc ")->rows;
		foreach ($waiter_pre_datas as $wkey => $waiter) {
			$data = array(
				'waiter_id' => $waiter['waiter_id'],
				'waiter_code'	=> $waiter['waiter_code'],
				'waiter'	=>	$waiter['waiter'],
				'captain_id' => $waiter['captain_id'],
				'captain_code'	=> $waiter['captain_code'],
				'captain'	=>	$waiter['captain'],
			);
		}
		*/

		$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$tid."' ";
		$running_datas = $this->db->query($running_sql);
		if($running_datas->num_rows > 0){
			$data['status'] = 2;
		} else {
			if($this->model_catalog_order->get_settings('SKIPTABLE') == 0) {
				date_default_timezone_set("Asia/Kolkata");
				$sql = "INSERT INTO `oc_running_table` SET `table_id` = '".$tid."', `date_added` = '".date('Y-m-d')."', `time_added` = '".date('H:i:s')."' ";
				$this->db->query($sql);
				$this->session->data['table_id'] = $tid;
				$data['status'] = 1;
			}	
		}
		//$this->log->write("Status");
		
		//$this->log->write($data['status']);
		
		
		$display_type = type;
		if($this->model_catalog_order->get_settings('SETTLEMENT') == '1'){
			if($edit == '1'){
				$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND 	`bill_date` = '" . $this->db->escape($last_open_date) . "'  AND order_id = '".$editorderid."'");
			} else{
				$tableinfo =  $this->db->query("SELECT * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND 	`bill_date` = '" . $this->db->escape($last_open_date) . "' AND pay_method = '0' AND cancel_status <> '1' ORDER BY order_id DESC LIMIT 1 ");
			}
		} else{
			$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND 	`bill_date` = '" . $this->db->escape($last_open_date) . "' AND bill_status = '0' ORDER BY order_id DESC LIMIT 1 ");
		}
		// $this->log->write("Table Data In Order Info");
		// $this->log->write($tableinfo);
		$data['html'] = '';
		if($tableinfo->num_rows>0) {
			$order =	$tableinfo->row['order_id'];
			$apporderid =	$tableinfo->row['app_order_id'];
			$staxfood = $tableinfo->row['staxfood'];
			$staxliq = $tableinfo->row['staxliq'];
			$order_no =	$tableinfo->row['order_no'];
			$data['order'] = $order;
			$tableitems = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' AND ismodifier = '1' ORDER BY id,`code` ")->rows;
			// $data['i']=1;
			
			// $this->log->write("Html Data In Order items");
			// $this->log->write($tableitems);
		
		 	foreach ($tableinfo->row as $tkey => $tinfo) {
				$data[$tkey] = $tinfo;
			}
		// $this->log->write("Table Data ");

		// $this->log->write($data);

		 	$html = '<tr>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;width: 5%;display:none;">No.</td>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;width: 15%;font-weight: bold;font-size: 20px;">Code</td>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;width: 30%;font-weight: bold;font-size: 20px;">Name</td>';
				$html .= '<td style="width: 8%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Qty</td>';
				$html .= '<td style="width: 13%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Rate</td>';
				$html .= '<td style="width: 13%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Amt</td>';
				$html .= '<td style="width: 25%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">KOT</td>';
				$html .= '<td style="padding-top: 2px;padding-bottom: 2px;width: 2%;font-weight: bold;font-size: 20px;">R</td>';
			$html .= '</tr>';
			
		 	$liq_tax_per = 0;
		 	$liq_tax_per2 = 0;
		 	$food_tax_per = 0;
		 	$food_tax_per2 = 0;
		 	$data['i']=1;
		 	foreach ($tableitems as $lkey => $items) {
		 		//$tabletest = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' AND parent_id = '".$items['id']."'")->row;
		 		// print_r($tabletest);
				$item_datas = $this->db->query("SELECT `vat`, `tax2` FROM `oc_item` WHERE `item_code` = '".$items['code']."' ");
		 		$taxvalue1 = 0;
		 		$taxvalue2 = 0;
		 		if($item_datas->num_rows > 0){
			 		$item_data = $item_datas->row;
			 		$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['vat']."'");
					if($tax1->num_rows > 0){
						$taxvalue1 = $tax1->row['tax_value'];
					} else{
						$taxvalue1 = '0';
					}

					$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$item_data['tax2']."'");
					if($tax2->num_rows > 0){
						$taxvalue2 = $tax2->row['tax_value'];
					} else{
						$taxvalue2 = '0';
					}
				}

				$parentdatas = $this->db->query("SELECT * FROM oc_order_items WHERE parent_id = '".$items['id']."'")->rows;
				$datai = 1;

				foreach($tableitems as $lkeys => $itemss){
					if($lkey == $lkeys) {

					} elseif($lkey > $lkeys && $items['code'] == $itemss['code'] && $items['rate'] == $itemss['rate'] && $items['message'] == $itemss['message'] && $itemss['cancelstatus'] == '0' && $items['cancelstatus'] == '0' && $items['ismodifier'] == '1' && $itemss['ismodifier'] == '1'){
						if(($items['amt'] == $itemss['amt']) || ($itemss['amt'] != '0' && $items['amt'] != '0')){
							if($items['parent'] == '0'){
								$items['code'] = '';
							}
						}
					} elseif ($items['code'] == $itemss['code'] && $items['rate'] == $itemss['rate'] && $items['message'] == $itemss['message'] && $itemss['cancelstatus'] == '0' && $items['cancelstatus'] == '0' && $items['ismodifier'] == '1' && $itemss['ismodifier'] == '1') {
						if(($items['amt'] == $itemss['amt']) || ($itemss['amt'] != '0' && $items['amt'] != '0')){
							if($items['parent'] == '0'){
								$items['qty'] = $items['qty'] + $itemss['qty'];
								if($itemss['nc_kot_status'] == 0){
									$items['amt'] = $items['qty'] * $items['rate'];
								}
							}
						}
					}
				}

				if($items['cancelstatus'] == '1' || $items['ismodifier'] == '0'){
					$display='none';
				} elseif($items['cancelstatus'] == '1' && $items['ismodifier'] == '0'){
					$display='none';
				} else{
					$display='';
				}
				// $this->log->write("code");

				// $this->log->write($items['code']);

				if($items['code'] != ''){
					$html .= '<tr id="re_'.$data['i'].'" style="display:'.$display.'">';
						$html .= '<td class="r_'.$data['i'].'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">';
							$html .= ''.$data['i'].'<input type="hidden" name="po_datas['.$data['i'].'][kot_status]" value="'.$items['kot_status'].'" id="kot_status'.$data['i'].'" />
							<input type="hidden" name="po_datas['.$data['i'].'][pre_qty]" value="'.$items['qty'].'" id="pre_qty'.$data['i'].'" />
							<input type="hidden" name="po_datas['.$data['i'].'][kot_no]" value="'.$items['kot_no'].'" id="kot_no'.$data['i'].'" />
							<input type="hidden" name="po_datas['.$data['i'].'][is_new]" value="'.$items['is_new'].'" id="is_new'.$data['i'].'" />
							<input type="hidden" name="po_datas['.$data['i'].'][is_set]" value="1" id="is_set'.$data['i'].'" />
							<input type="hidden" name="po_datas['.$data['i'].'][cancelstatus]" value="'.$items['cancelstatus'].'" id="cancelstatus'.$data['i'].'" />';
						
						$html .= '</td>';
						$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:15%">';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas['.$data['i'].'][code]" value="'.$items['code'].'" id="code_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas['.$data['i'].'][subcategoryid]" value="'.$items['subcategoryid'].'" id="subcategoryid_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas['.$data['i'].'][tax1]" value="'.$items['tax1'].'" id="tax1_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas['.$data['i'].'][tax2]" value="'.$items['tax2'].'" id="tax2_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas['.$data['i'].'][tax1_value]" value="'.$items['tax1_value'].'" id="tax1_value_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas['.$data['i'].'][tax2_value]" value="'.$items['tax2_value'].'" id="tax2_value_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas['.$data['i'].'][discount_per]" value="'.$items['discount_per'].'" id="discount_per_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas['.$data['i'].'][discount_value]" value="'.$items['discount_value'].'" id="discount_value_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas['.$data['i'].'][billno]" value="'.$items['billno'].'" id="billno_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" name="po_datas['.$data['i'].'][ismodifier]" value="'.$items['ismodifier'].'" id="ismodifier_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas['.$data['i'].'][parent_id]" value="'.$items['parent_id'].'" id="parent_id_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" name="po_datas['.$data['i'].'][cancelmodifier]" value="'.$items['cancelmodifier'].'" id="cancelmodifier_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" name="po_datas['.$data['i'].'][nc_kot_status]" value="'.$items['nc_kot_status'].'" id="nc_kot_status_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" name="po_datas['.$data['i'].'][nc_kot_reason]" value="'.$items['nc_kot_reason'].'" id="nc_kot_reason_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" name="po_datas['.$data['i'].'][transfer_qty]" value="'.$items['transfer_qty'].'" id="transfer_qty_'.$data['i'].'" />';
							$html .= '<input type="hidden" name="po_datas['.$data['i'].'][referparent]" value="'.$data['i'].'" id="referparent_'.$data['i'].'" />';
							$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas['.$data['i'].'][parent]" value="'.$items['parent'].'" id="parent_'.$data['i'].'" />';
							$tab_index ++;
						$html .= '</td>';
					 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
					 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas['.$data['i'].'][name]" value="'.$items['name'].'" id="name_'.$data['i'].'" />';
					 		$html .= '<input style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas['.$data['i'].'][id]" value="'.$items['id'].'" id="id_'.$data['i'].'" />';
					 		$tab_index ++;
					 	$html .= '</td>';
					 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:8%;">';
						 	if($display_type == '1'){
						 		if($data['bill_status'] == '0' || $edit == '1'){
						 			$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="text" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control" id="qty_'.$data['i'].'"  autocomplete="off"/>';
						 		} else {
						 			$html .= '<input readonly="readonly" tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="text" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control" id="qty_'.$data['i'].'"  autocomplete="off"/>';	
						 		}
						 	} else {
								if($data['bill_status'] == '0' || $edit == '1'){ 		
						 			$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('.$data['i'].')" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control screenquantity" id="qty_'.$data['i'].'" />';
						 		} else {
						 			$html .= '<input readonly="readonly" tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('.$data['i'].')" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty form-control screenquantity" id="qty_'.$data['i'].'" />';
						 		}
						 	}
						 	$tab_index ++;
					 	$html .= '</td>';
					 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:13%">';
						 	if($display_type == '1'){
						 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="number" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control" id="rate_'.$data['i'].'" />';
						 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['.$data['i'].'][pre_rate]" value="'.$items['rate'].'" class="inputs pre_rate form-control" id="pre_rate_'.$data['i'].'" />';
						 	} else {
						 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('.$data['i'].')" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs rate form-control" id="rate_'.$data['i'].'" />';
						 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="hidden" onclick="select_rate('.$data['i'].')" name="po_datas['.$data['i'].'][pre_rate]" value="'.$items['rate'].'" class="inputs pre_rate form-control" id="pre_rate_'.$data['i'].'" />';
						 	}
					 		$tab_index ++;
					 	$html .= '</td>';
					 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width: 13%">';
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" readonly="readonly" name="po_datas['.$data['i'].'][amt]" value="'.$items['amt'].'" class="inputs form-control" id="amt_'.$data['i'].'" />';
					 		$tab_index ++;
					 	$html .= '</td>';		
					 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
					 		$html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;font-size: 16px;" type="text" name="po_datas['.$data['i'].'][message]" autocomplete="off" value="'.$items['message'].'" class="inputs lst form-control" id="message_'.$data['i'].'" /><input style="display:none" type="text" name="po_datas['.$data['i'].'][is_liq]" value="'.$items['is_liq'].'" class="inputs lst" id="is_liq_'.$data['i'].'" />';
				 		// $html .= '<input tabindex="'.$tab_index.'" readonly="readonly" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" name="po_datas['.$data['i'].'][cancelmodifier]" value="'.$items['cancelmodifier'].'" id="cancelmodifier_'.$data['i'].'" />';
					 		$tab_index ++;
					 	$html .= '</td>';
					 	$html .= '<td class="r_'.$data['i'].'" style="text-align: left;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">';
							$html .= '-';
						$html .= '</td>';
					$html .= '</tr>';
					if($parentdatas != array()){
						foreach($parentdatas as $parentdata){
							$html .= '<tr id="re_'.$data['i']."_".$datai.'" class="re_'.$data['i'].'" style="display:none">';
								$html .= '<td class="r_'.$data['i'].'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">';
									$html .= ''.$data['i'].'<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][kot_status]" value="'.$parentdata['kot_status'].'" id="kot_status'.$data['i']."_".$datai.'" />
									<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][pre_qty]" value="'.$parentdata['qty'].'" id="pre_qty'.$data['i']."_".$datai.'" />
									<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][kot_no]" value="'.$parentdata['kot_no'].'" id="kot_no'.$data['i']."_".$datai.'" />
									<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][is_new]" value="'.$parentdata['is_new'].'" id="is_new'.$data['i']."_".$datai.'" />
									<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][is_set]" value="1" id="is_set'.$data['i']."_".$datai.'" />
									<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][cancelstatus]" value="'.$parentdata['cancelstatus'].'" id="cancelstatus'.$data['i']."_".$datai.'" />';
								$html .= '</td>';
								$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:15%">';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs code form-control" name="po_datas['.$data['i']."_".$datai.'][code]" value="'.$parentdata['code'].'" id="code_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas['.$data['i']."_".$datai.'][subcategoryid]" value="'.$parentdata['subcategoryid'].'" id="subcategoryid_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas['.$data['i']."_".$datai.'][tax1]" value="'.$parentdata['tax1'].'" id="tax1_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas['.$data['i']."_".$datai.'][tax2]" value="'.$parentdata['tax2'].'" id="tax2_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas['.$data['i']."_".$datai.'][tax1_value]" value="'.$parentdata['tax1_value'].'" id="tax1_value_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas['.$data['i']."_".$datai.'][tax2_value]" value="'.$parentdata['tax2_value'].'" id="tax2_value_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas['.$data['i']."_".$datai.'][discount_per]" value="'.$parentdata['discount_per'].'" id="discount_per_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas['.$data['i']."_".$datai.'][discount_value]" value="'.$parentdata['discount_value'].'" id="discount_value_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas['.$data['i']."_".$datai.'][billno]" value="'.$parentdata['billno'].'" id="billno_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" name="po_datas['.$data['i']."_".$datai.'][ismodifier]" value="'.$parentdata['ismodifier'].'" id="ismodifier_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nckotstatus form-control" name="po_datas['.$data['i']."_".$datai.'][nc_kot_status]" value="'.$parentdata['nc_kot_status'].'" id="nckotstatus_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nckotreason form-control" name="po_datas['.$data['i']."_".$datai.'][nc_kot_reason]" value="'.$parentdata['nc_kot_reason'].'" id="nckotreason_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transferqty form-control" name="po_datas['.$data['i']."_".$datai.'][transfer_qty]" value="'.$parentdata['transfer_qty'].'" id="transferqty_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas['.$data['i']."_".$datai.'][parent_id]" value="'.$parentdata['parent_id'].'" id="parent_id_'.$data['i']."_".$datai.'" />';
									$html .= '<input type="hidden" name="po_datas['.$data['i']."_".$datai.'][referparent]" value="'.$data['i'].'" id="referparent_'.$data['i']."_".$datai.'" />';
									$html .= '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas['.$data['i']."_".$datai.'][parent]" value="'.$parentdata['parent'].'" id="parent_'.$data['i']."_".$datai.'" />';
									$tab_index ++;
								$html .= '</td>';
							 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
							 		$html .= '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" class="inputs names form-control" name="po_datas['.$data['i']."_".$datai.'][name]" value="'.$parentdata['name'].'" id="name_'.$data['i']."_".$datai.'" />';
							 		$html .= '<input style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas['.$data['i']."_".$datai.'][id]" value="'.$parentdata['id'].'" id="id_'.$data['i']."_".$datai.'" />';
							 		$tab_index ++;
							 	$html .= '</td>';
							 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:8%;">';
						 			$html .= '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['.$data['i']."_".$datai.'][qty]" value="'.$parentdata['qty'].'" id="qty_'.$data['i']."_".$datai.'" />';
							 	$html .= '</td>';
							 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:13%">';
							 		$html .= '<input style="padding: 0px 1px;font-size: 16px;" type="number" name="po_datas['.$data['i']."_".$datai.'][rate]" value="'.$parentdata['rate'].'" id="rate_'.$data['i']."_".$datai.'" />';
							 	$html .= '</td>';
							 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width: 13%">';
							 		$html .= '<input style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="hidden" name="po_datas['.$data['i']."_".$datai.'][amt]" value="'.$parentdata['amt'].'" id="amt_'.$data['i']."_".$datai.'" />';
							 	$html .= '</td>';		
							 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
							 		$html .= '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['.$data['i']."_".$datai.'][message]" value="'.$parentdata['message'].'" autocomplete="off" id="message_'.$data['i']."_".$datai.'" /><input style="display:none" type="hidden" name="po_datas['.$data['i']."_".$datai.'][is_liq]" value="'.$parentdata['is_liq'].'" class="inputs lst" id="is_liq_'.$data['i']."_".$datai.'" />';
							 		$html .= '<input style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="hidden" name="po_datas['.$data['i']."_".$datai.'][cancelmodifier]" value="'.$parentdata['cancelmodifier'].'" id="cancelmodifier_'.$data['i']."_".$datai.'" />';
							 	$html .= '</td>';
							$html .= '</tr>';
							$datai++;
						}
					}
				}
				if($items['is_liq'] == 1){
					$liq_tax_per = $taxvalue1;
					$liq_tax_per2 = $taxvalue2;
				}
				if($items['is_liq'] == 0){
					$food_tax_per = $taxvalue1;
					$food_tax_per2 = $taxvalue2;
				}

				if($items['code'] != ''){
					$data['i'] ++;
				}

			}
			//$this->log->write($data['bill_status']);
			if(($data['bill_status'] == '0') || ($data['bill_status'] != '0' && $edit == '1')){
			 	$html .= '<tr id="re_'.$data['i'].'">';
			 		$html .= '<td class="r_'.$data['i'].'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">';
			 			$html .= $data['i'].'<input type="hidden" name="po_datas['.$data['i'].'][kot_status]" value="'.$items['kot_status'].'" id="kot_status'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][pre_qty]" value="0" id="pre_qty'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][kot_no]" value="0" id="kot_no'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][is_new]" value="0" id="is_new'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][is_set]" value="0" id="is_set'.$data['i'].'" /><input type="hidden" name="po_datas['.$data['i'].'][cancelstatus]" value="0" id="cancelstatus'.$data['i'].'" />';
			 		$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:15%">';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas['.$data['i'].'][code]" value="" id="code_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas['.$data['i'].'][subcategoryid]" value="" id="subcategoryid_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas['.$data['i'].'][tax1]" value="" id="tax1_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas['.$data['i'].'][tax2]" value="" id="tax2_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas['.$data['i'].'][tax1_value]" value="" id="tax1_value_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas['.$data['i'].'][tax2_value]" value="" id="tax2_value_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas['.$data['i'].'][discount_per]" value="" id="discount_per_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas['.$data['i'].'][discount_value]" value="" id="discount_value_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas['.$data['i'].'][billno]" value="" id="billno_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" value="1" name="po_datas['.$data['i'].'][ismodifier]" id="ismodifier_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" value="0" name="po_datas['.$data['i'].'][nc_kot_status]" id="nc_kot_status_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" value="" name="po_datas['.$data['i'].'][nc_kot_reason]" id="nc_kot_reason_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" value="0" name="po_datas['.$data['i'].'][transfer_qty]" id="transfer_qty_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas['.$data['i'].'][parent_id]" value="0" id="parent_id_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['.$data['i'].'][referparent]" value="0" id="referparent_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['.$data['i'].'][parent]" value="0" id="parent_'.$data['i'].'" />';
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas['.$data['i'].'][name]" value="" id="name_'.$data['i'].'" />';
				 		$html .= '<input style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas['.$data['i'].'][id]" value="" id="id_'.$data['i'].'" />';
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:8%;">';
					 	if($display_type == '1'){
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="text" name="po_datas['.$data['i'].'][qty]" value="" class="inputs qty form-control" id="qty_'.$data['i'].'" autocomplete="off"/>';
					 	} else {
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('.$data['i'].')" name="po_datas['.$data['i'].'][qty]" value="" class="inputs qty form-control screenquantity" id="qty_'.$data['i'].'" />';
					 	}
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width:13%">';
					 	if($display_type == '1'){
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="number" name="po_datas['.$data['i'].'][rate]" value="" class="inputs rate form-control" id="rate_'.$data['i'].'" />';
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['.$data['i'].'][pre_rate]" value="" class="inputs pre_rate form-control" id="pre_rate_'.$data['i'].'" />';
					 		
					 	} else {
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('.$data['i'].')" name="po_datas['.$data['i'].'][rate]" value="" class="inputs rate form-control" id="rate_'.$data['i'].'" />';
					 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('.$data['i'].')" name="po_datas['.$data['i'].'][pre_rate]" value="" class="inputs pre_rate form-control" id="pre_rate_'.$data['i'].'" />';
					 		
					 	}
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;width: 13%">';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" readonly="readonly" name="po_datas['.$data['i'].'][amt]" value="" class="inputs form-control" id="amt_'.$data['i'].'" />';
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="padding-top: 2px;padding-bottom: 2px;">';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;font-size: 16px;" type="text" name="po_datas['.$data['i'].'][message]" value="" autocomplete="off" class="inputs lst form-control" id="message_'.$data['i'].'" /><input style="display:none" type="text" name="po_datas['.$data['i'].'][is_liq]"  value="" id="is_liq_'.$data['i'].'" />';
				 		$html .= '<input style="padding: 0px 1px;" type="hidden" name="po_datas['.$data['i'].'][usermsg]" value="" class="inputs usermsg form-control" id="message_'.$data['i'].'" />';
				 		$html .= '<input tabindex="'.$tab_index.'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" name="po_datas['.$data['i'].'][cancelmodifier]" value="0" id="cancelmodifier_'.$data['i'].'" />';
			 			
			 			$html .= '<input type="hidden" name="liq_tax_per" value="'.$liq_tax_per.'"  id="liq_tax_per" />';
			 			$html .= '<input type="hidden" name="liq_tax_per2" value="'.$liq_tax_per2.'"  id="liq_tax_per2" />';
						$html .= '<input type="hidden" name="food_tax_per" value="'.$food_tax_per.'"  id="food_tax_per" />';
						$html .= '<input type="hidden" name="food_tax_per2" value="'.$food_tax_per2.'"  id="food_tax_per2" />';
						// $html .= '<input type="hidden" name="foodcount" value="'.$foodcount.'"  id="foodcount" />';
						// $html .= '<input type="hidden" name="liqcount" value="'.$liqcount.'"  id="liqcount" />';
				 		$tab_index ++;
				 	$html .= '</td>';
				 	$html .= '<td class="r_'.$data['i'].'" style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
						$html .= '<a tabindex="'.$tab_index.'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('.$data['i'].')" class="button inputs remove" id="remove_'.$data['i'].'" ><i class="fa fa-trash-o"></i></a>';
						$tab_index ++;
					$html .= '</td>';
				$html .= '</tr>';
			}
			$html .= '<input type="hidden" name="orderid" value="'.$order.'"  id="orderid" />';
			$html .= '<input type="hidden" name="apporderid" value="'.$apporderid.'"  id="apporderid" />';
			$html .= '<input type="hidden" name="order_no" value="'.$order_no.'"  id="orderno" />';
			$html .= '<input type="hidden" name="bill_status" value="'.$data['bill_status'].'"  id="bill_status" />';
			$html .= '<input type="hidden" name="staxfood" value="'.$staxfood.'"  id="staxfood" />';
			$html .= '<input type="hidden" name="staxliq" value="'.$staxliq.'"  id="staxliq" />';
			$data['html'] = $html;
			$data['tab_index'] = $tab_index;
		} 
		//$this->log->write("Html Get Datas ".print_r($data['html'],true));
		//$this->log->write("---------------------------Table Info End---------------------------");
		
		
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function tables() {


		$this->load->model('catalog/order');
		$lname =$this->request->get['lname'];
		$id=$this->request->get['id'];
		$locname= "'".$lname ."'";
		$locationss =  $this->db->query("SELECT  * FROM oc_location WHERE location_id ='".$id."' ")->row;
		$locations = array();
		$service_charge = 0;
		$rate_id = 1;
		$table_id_string = '';
		if(isset($locationss['table_to']) && $locationss['table_to'] > 0){
			$service_charge = $locationss['service_charge'];
			$rate_id = $locationss['rate_id'];
			for($i=$locationss['table_from']; $i<=$locationss['table_to']; $i++){
				$locations[] = array(
					'table_id' => $i,
					'name' => $i,
					'parcel_detail' => $locationss['parcel_detail'],
					'service_charge' => $locationss['service_charge']
				);
				$table_id_string .= $i.',';	
			}
			$table_id_string = rtrim($table_id_string, ',');
		}

		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$table_id_string = "'" . str_replace(",", "','", html_entity_decode($table_id_string)) . "'";
		//echo "SELECT `order_id`, `bill_status`, `pay_method`, `cancel_status`, `person`, `time_added`, `grand_total` FROM oc_order_info WHERE `location_id` ='".$id."' AND `table_id` IN (".$table_id_string.") AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND (`pay_method` = '0' OR `cancel_status` = '1') ORDER BY order_id DESC";exit;
		$tableinfoss =  $this->db->query("SELECT `table_id`, `order_id`, `bill_status`, `pay_method`, `cancel_status`, `person`, `time_added`, `grand_total` FROM oc_order_info WHERE `location_id` ='".$id."' AND `table_id` IN (".$table_id_string.") AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY order_id DESC")->rows;
		foreach ($tableinfoss as $tkey => $tvalue) {
			$tableinfos[$tvalue['table_id']] = $tvalue;
		}
		$i=1;
		$data['html'] = '';
		$html = '<div class="row" style="xwidth: 100%;height: 100px;margin-left: 1%;margin-top: 1%;">';
		foreach ($locations as $lkey => $loc) {
			$tname= "'".$loc['name']."'";
			//$tableinfo =  $this->db->query("SELECT `bill_status`, `pay_method`, `cancel_status`, `person`, `time_added`, `grand_total` FROM oc_order_info WHERE `location_id` ='".$id."' AND `table_id` = '".$loc['table_id']."' AND `bill_date` = '" . $this->db->escape($last_open_date) . "' ORDER BY order_id DESC LIMIT 1 ");
			if(strlen($loc['name']) > 27){
				$font_size = '12px';
			} elseif(strlen($loc['name']) > 18){
				$font_size = '18px';
			} elseif(strlen($loc['name']) > 9){
				$font_size = '24px';
			} else {
				$font_size = '30px';
			}
			
			if($i % 5 == 0){
				$i = 1;
				$html .= '</div>';
				$html .= '<div class="row" style="xwidth: 100%;height: 100px;margin-left: 1%;margin-top: 1%;">';
			}
			
			if(isset($tableinfos[$loc['name']])){
			//if($tableinfo->num_rows > 0) {
				//$html .= '<div style="margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color:red;border-color:black;width: 22%;padding-top: 3%;padding-bottom: 3%;display: inline-block;text-align: center;height:	15%;">';
				$loc['colour'] = '';
				if($tableinfos[$loc['name']]['bill_status'] == 0){
					$loc['colour'] = 'lime';
				}
				if($tableinfos[$loc['name']]['bill_status'] == 1){
					$loc['colour'] = 'red';
				}
				if($tableinfos[$loc['name']]['pay_method'] == 1 || $tableinfos[$loc['name']]['cancel_status'] == 1){
					$loc['colour'] = 'orange';
					$html .= '<div class="col-sm-3" style="background-color: '.$loc['colour'].';border-color: black;text-align: center;height: 100%;margin-right: 1%;margin-top: 1%;width: 190px;display: table;padding: 0px !important;">';
					if($this->model_catalog_order->get_settings('NOCATEGORY') == 0){
						$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';" data-toggle="tooltip" href="javascript:cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.','.$loc['parcel_detail'].',1)" title='.$loc['name'].' class="">'.$loc['name'].'</a>';
					} else{
						$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:subcat('.$id.','.$locname.','.$tname.','.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$loc['parcel_detail'].',1)"  class="">'.$loc['name'].'</a>';
					}
					$html .= '</div>';
				} else{
					$html .= '<div class="col-sm-3" style="background-color: '.$loc['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;margin-top: 1%;width: 190px;display: table;padding: 0px !important;">';
						if($this->model_catalog_order->get_settings('NOCATEGORY') == 0){
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;" data-toggle="tooltip" href="javascript:cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.','.$loc['parcel_detail'].',1)" title='.$loc['name'].' class=""><b>'.$loc['name'].'</b><br>';
						} else{
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;" data-toggle="tooltip" href="javascript:subcat('.$id.','.$locname.','.$tname.','.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$loc['parcel_detail'].',1)"  class="">'.$loc['name'].'</b><br>';
						}
						$html .= '<span style="font-size:12px;">';
						$html .= 'Persons : <b>'.$tableinfos[$loc['name']]['person'].'</b><br>';
						$html .= 'Time : <b>'.date('h:i',strtotime($tableinfos[$loc['name']]['time_added'])).'</b><br>';
						$html .= 'Amount : <b>'.$tableinfos[$loc['name']]['grand_total'].'</b><br>';
						$html .= '</span></a>';
					$html .= '</div>';
				}
			} else {
				if($this->model_catalog_order->get_settings('SKIPTABLE') == 0){
					$loc['colour'] = 'orange';
					//$html .= '<div style="margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$loc['colour'].';border-color: black;width: 22%;padding-top: 3%;padding-bottom: 3%;display: inline-block;text-align: center;height: 15%;">';
					$html .= '<div class="col-sm-3" style="background-color: '.$loc['colour'].';border-color: black;text-align: center;height: 100%;margin-right: 1%;margin-top: 1%;width: 190px;display: table;padding: 0px !important;">';
						if($this->model_catalog_order->get_settings('NOCATEGORY') == 1){
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';" data-toggle="tooltip" href="javascript:subcat('.$id.','.$locname.','.$tname.','.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$loc['parcel_detail'].',1)" class="">'.$loc['name'].'</a>';
						} else {
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';" data-toggle="tooltip" href="javascript:cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.','.$loc['parcel_detail'].',1)" title='.$loc['name'].' class="">'.$loc['name'].'</a>';
						}
					$html .= '</div>';
				} elseif($this->model_catalog_order->get_settings('SKIPSUBCATEGORY') == 1 && $this->model_catalog_order->get_settings('SKIPTABLE') == 1 && $this->model_catalog_order->get_settings('NOCATEGORY') == 1){
					$items =  $this->db->query("SELECT  * FROM oc_item WHERE item_sub_category_id = '1' AND ismodifier = 0")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($items as $sckey => $scat) {
						if(strlen($scat['item_name']) > 27){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 18){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($scat['colour'] == ''){
							$scat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}
						$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding:0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" class="add" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="">'.$scat['item_name'].'</a>';
						$html .= '</div>';
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="btn btn-primary" >'.$scat['item_name'].'</a>';
					}

				} elseif($this->model_catalog_order->get_settings('SKIPSUBCATEGORY') == 1 && $this->model_catalog_order->get_settings('NOCATEGORY') == 1){
					$loc['colour'] = 'orange';
					//$html .= '<div style="margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$loc['colour'].';border-color: black;width: 22%;padding-top: 3%;padding-bottom: 3%;display: inline-block;text-align: center;height: 15%;">';
					$html .= '<div class="col-sm-3" style="background-color: '.$loc['colour'].';border-color: black;text-align: center;height: 100%;margin-right: 1%;margin-top: 1%;width: 190px;display: table;padding: 0px !important;">';
						$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';" data-toggle="tooltip" href="javascript:subcat('.$id.','.$locname.','.$tname.','.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$loc['parcel_detail'].',1)" class="">'.$loc['name'].'</a>';
					$html .= '</div>';
					
				} elseif($this->model_catalog_order->get_settings('NOCATEGORY') == 1 && $this->model_catalog_order->get_settings('SKIPTABLE') == 1){
					//echo "iiiiiii";exit;
					$subcategorys =  $this->db->query("SELECT  * FROM oc_subcategory WHERE parent_id ='".$this->model_catalog_order->get_settings('CATEGORY')."'")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($subcategorys as $sckey => $scat) {
						// echo'<pre>';
						// print_r($scat);
						// exit;
						if(strlen($scat['name']) > 27){
							$font_size = '22px';
						} elseif(strlen($scat['name']) > 18){
							$font_size = '22px';
						} elseif(strlen($scat['name']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($scat['colour'] == ''){
							$scat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}
						$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding: 0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:items('.$id.','.$locname.',1,'.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$scat['category_id'].')" title='.$scat['name'].' class="">'.$scat['name'].'</a>';
						$html .= '</div>';
						// echo'<pre>';
						// print_r($html);
						// exit;
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')" title='.$scat['name'].' class="btn btn-primary" >'.$scat['name'].'</a>';
					}

				} elseif($this->model_catalog_order->get_settings('SKIPSUBCATEGORY') == 1 && $this->model_catalog_order->get_settings('SKIPTABLE') == 1){
					$items =  $this->db->query("SELECT  * FROM oc_item WHERE item_sub_category_id = '1' AND ismodifier = 0")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($items as $sckey => $scat) {
						if(strlen($scat['item_name']) > 27){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 18){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($scat['colour'] == ''){
							$scat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}
						$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding:0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" class="add" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="">'.$scat['item_name'].'</a>';
						$html .= '</div>';
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="btn btn-primary" >'.$scat['item_name'].'</a>';
					}
				} elseif($this->model_catalog_order->get_settings('SKIPTABLE') == 1){
					//echo'11111';
					$categorys =  $this->db->query("SELECT  * FROM oc_category")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($categorys as $ckey => $cat) {
						if(strlen($cat['category']) > 27){
							$font_size = '22px';
						} elseif(strlen($cat['category']) > 18){
							$font_size = '22px';
						} elseif(strlen($cat['category']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($cat['colour'] == ''){
							$cat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}

						$html .= '<div class="col-sm-3" style="background-color: '.$cat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding: 0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:subcat('.$id.','.$locname.',1,'.$tname.','.$cat['category_id'].',0,0)" title='.$cat['category'].' class="">'.$cat['category'].'</a>';
						$html .= '</div>';
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$cat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$cat['category_id'].')" title='.$cat['category'].' class="btn btn-primary" >'.$cat['category'].'</a>';
					}
				} elseif($this->model_catalog_order->get_settings('NOCATEGORY') == 1 ){
					//echo "iiiiiii";
					$subcategorys =  $this->db->query("SELECT  * FROM oc_subcategory WHERE parent_id ='".$this->model_catalog_order->get_settings('CATEGORY')."'")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($subcategorys as $sckey => $scat) {
						if(strlen($scat['name']) > 27){
							$font_size = '22px';
						} elseif(strlen($scat['name']) > 18){
							$font_size = '22px';
						} elseif(strlen($scat['name']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($scat['colour'] == ''){
							$scat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}
						$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding: 0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:items('.$id.','.$locname.',1,'.$tname.','.$this->model_catalog_order->get_settings('CATEGORY').','.$scat['category_id'].')" title='.$scat['name'].' class="">'.$scat['name'].'</a>';
						$html .= '</div>';
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')" title='.$scat['name'].' class="btn btn-primary" >'.$scat['name'].'</a>';
					}
				} elseif($this->model_catalog_order->get_settings('SKIPSUBCATEGORY') == 1 ){
					$items =  $this->db->query("SELECT  * FROM oc_item WHERE item_sub_category_id = '1' AND ismodifier = 0")->rows;
					$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
					foreach ($items as $sckey => $scat) {
						if(strlen($scat['item_name']) > 27){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 18){
							$font_size = '22px';
						} elseif(strlen($scat['item_name']) > 9){
							$font_size = '22px';
						} else {
							$font_size = '22px';
						}
						if($scat['colour'] == ''){
							$scat['colour'] = '#ff008c';
						}
						if($i % 5 == 0){
							$html .= '</div>';
							$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
						}
						$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding:0px !important;">';
							$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" class="add" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="">'.$scat['item_name'].'</a>';
						$html .= '</div>';
						//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="btn btn-primary" >'.$scat['item_name'].'</a>';
					}
				} else {
					$loc['colour'] = 'orange';
					//$html .= '<div style="margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$loc['colour'].';border-color: black;width: 22%;padding-top: 3%;padding-bottom: 3%;display: inline-block;text-align: center;height: 15%;">';
					$html .= '<div class="col-sm-3" style="background-color: '.$loc['colour'].';border-color: black;text-align: center;height: 100%;margin-right: 1%;margin-top: 1%;width: 190px;display: table;padding: 0px !important;">';
						$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';" data-toggle="tooltip" href="javascript:cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.','.$loc['parcel_detail'].',1)" title='.$loc['name'].' class="">'.$loc['name'].'</a>';
					$html .= '</div>';
				}
			}
			$i++;
			//$this->log->write($loc['name']);
		}
		$html .= '</div>';
		$data['html'] .= $html;

		/*
		$location_datas = $this->db->query("SELECT `rate_id` FROM `oc_location` WHERE `location_id` = '".$id."' ");
		$rate_id = 1;
		if($location_datas->num_rows > 0){
			$location_data = $location_datas->row;
			$rate_id = $location_data['rate_id'];
		}
		*/
		$data['rate_id'] = $rate_id;
		$data['NOCATEGORY'] = $this->model_catalog_order->get_settings('NOCATEGORY');
		$data['CATEGORY'] = $this->model_catalog_order->get_settings('CATEGORY');
		$data['SKIPSUBCATEGORY'] = $this->model_catalog_order->get_settings('SKIPSUBCATEGORY');
		$data['SKIPTABLE'] = $this->model_catalog_order->get_settings('SKIPTABLE');
		$data['service_charge'] = $service_charge;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	
	public function checkcustomers() {
		$contacno =$this->request->get['contactno'];
		$custs =  $this->db->query("SELECT  * FROM oc_customerinfo WHERE contact ='".$contacno."'");
		if($custs->num_rows > 0) {
			$data['contactno'] = $custs->row['contact']	;
			$data['c_id'] = $custs->row['c_id']	;
			$data['name'] = $custs->row['name']	;
			$data['address'] = $custs->row['address']	;
			$data['email'] = $custs->row['email']	;
			$data['html'] ='<h4 style = "margin-top:10%" >'.$data['name'].'</h4>';
		} else {
			$data['html'] ='<a data-toggle="tooltip" onclick="newreg()" title="new" class="btn btn-primary" style="margin-top: 10%">Register</a>';
			$data['contactno'] = $contacno;
		}
		// 	$data['html'] = '<a  data-toggle="tooltip" title="" class="btn btn-primary" onClick="addwaiter('.$loc['waiter_id'].','.$wname.')"  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;padding:5%;" data-original-title="'.$loc['name'].'">'.$loc['name'].'</a>';
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function waitersel() {
		$waiters =  $this->db->query("SELECT  * FROM oc_waiter WHERE roll = 'Waiter' ")->rows;
		$i=1;
		$data['html'] = '';
		foreach ($waiters as $lkey => $loc) {
			$wname= "'".$loc['name']."'";
			// $data['html'] .= '<a  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;background-color:red;border-color:red;font-size: 280%;width: 10%;" data-toggle="tooltip" onClick="cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.',1)" title='.$loc['name'].' class="btn btn-primary" >'.$i.'</a>';
			$data['html'] .= '<a  data-toggle="tooltip" title="" class="btn btn-primary" onClick="addwaiter('.$loc['waiter_id'].','.$wname.')"  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;padding:5%;" data-original-title="'.$loc['name'].'">'.$loc['name'].'</a>';
			$i++;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function waiterselid() {
		$waiters =  $this->db->query("SELECT  * FROM oc_waiter WHERE roll = 'Waiter' ")->rows;
		$i=1;
		$data['html'] = '';
		foreach ($waiters as $lkey => $loc) {
			$wcode= "'".$loc['code']."'";
			// $data['html'] .= '<a  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;background-color:red;border-color:red;font-size: 280%;width: 10%;" data-toggle="tooltip" onClick="cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.',1)" title='.$loc['name'].' class="btn btn-primary" >'.$i.'</a>';
			$data['html'] .= '<a  data-toggle="tooltip" title="" class="btn btn-primary" onClick="addwaiter('.$loc['waiter_id'].','.$wcode.')"  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;padding:5%;" data-original-title="'.$loc['code'].'">'.$loc['code'].'</a>';
			$i++;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function captainsel() {
		$captains =  $this->db->query("SELECT  * FROM oc_waiter WHERE roll = 'Captain' ")->rows;
		$i=1;
		$data['html'] = '';
		foreach ($captains as $lkey => $loc) {
			$cname= "'".$loc['name']."'";
			// $data['html'] .= '<a  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;background-color:red;border-color:red;font-size: 280%;width: 10%;" data-toggle="tooltip" onClick="cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.',1)" title='.$loc['name'].' class="btn btn-primary" >'.$i.'</a>';
			$data['html'] .= '<a  data-toggle="tooltip" title="" class="btn btn-primary" onClick="addcaptain('.$loc['waiter_id'].','.$cname.')"  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;padding:5%;" data-original-title="'.$loc['name'].'">'.$loc['name'].'</a>';
			$i++;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function captainselid() {
		$captains =  $this->db->query("SELECT  * FROM oc_waiter WHERE roll = 'Captain' ")->rows;
		$i=1;
		$data['html'] = '';
		foreach ($captains as $lkey => $loc) {
			$cname= "'".$loc['code']."'";
			// $data['html'] .= '<a  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;background-color:red;border-color:red;font-size: 280%;width: 10%;" data-toggle="tooltip" onClick="cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.',1)" title='.$loc['name'].' class="btn btn-primary" >'.$i.'</a>';
			$data['html'] .= '<a  data-toggle="tooltip" title="" class="btn btn-primary" onClick="addcaptain('.$loc['waiter_id'].','.$cname.')"  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;padding:5%;" data-original-title="'.$loc['code'].'">'.$loc['code'].'</a>';
			$i++;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	
	public function additem() {
		$itemid = $this->request->get['itemid'];
		$modifieritemdata = array();
		$filter_rate_id = $this->request->get['filter_rate_id'];
		$iteminfo =  $this->db->query("SELECT  * FROM oc_item WHERE item_id ='".$itemid."'")->row;
		$rates = array(
			'1' => 'rate_1',
			'2' => 'rate_2',
			'3' => 'rate_3',
			'4' => 'rate_4',
			'5' => 'rate_5',
			'6' => 'rate_6',
		);
		if(isset($rates[$filter_rate_id])){
			$rate_key = $rates[$filter_rate_id];
		} else {
			$rate_key = 'rate_1';
		}

		$modifieritems = $this->db->query("SELECT * FROM oc_modifier_items WHERE modifier_id = '".$iteminfo['modifier_group']."'")->rows;
		$i=0;
		foreach($modifieritems as $modifieritem){
			$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$modifieritem['modifier_item_code']."'")->row;
			$modifieritemdata[] = array(
									'modifieritem' => $modifieritem['modifier_item'],
									'itemid' => $modifieritem['modifier_item_id'],
									'itemcode' => $modifieritem['modifier_item_code'],
									'subcategoryid' => $itemdata['item_sub_category_id'],
									'default_item' => $modifieritem['default_item'],
									'kitchen_display' => $modifieritem['kitchen_dispaly'],
									'modifierrate' => $itemdata[$rate_key],
									'is_liq' => $itemdata['is_liq']
								);
			$i++;
		}

		$rate = $iteminfo[$rate_key];

		if($iteminfo['modifier_group'] != '0'){
			$iteminfo['modifier_group'] = 1;
		} else{
			$iteminfo['modifier_group'] = 0;
		}

		$data['item_code'] = $iteminfo['item_code'];
		$data['is_liq'] = $iteminfo['is_liq'];
		$data['purchase_price'] = $rate;
		$data['item_name'] = $iteminfo['item_name'];
		$data['kitchen_display'] = $iteminfo['kitchen_dispaly'];
		$data['item_sub_category_id'] = $iteminfo['item_sub_category_id'];
		$data['subcategoryname'] = $iteminfo['item_sub_category'];
		$data['modifier_group'] = $iteminfo['modifier_group'];
		$data['modifier_qty'] = $iteminfo['modifier_qty'];
		$data['modifieritems'] = $modifieritemdata;
		$data['totalmodifiers'] = $i;
		$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$iteminfo['vat']."'");
		if($tax1->num_rows > 0){
			$taxvalue1 = $tax1->row['tax_value'];
		} else{
			$taxvalue1 = '0';
		}
		$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$iteminfo['tax2']."'");
		if($tax2->num_rows > 0){
			$taxvalue2 = $tax2->row['tax_value'];
		} else{
			$taxvalue2 = '0';
		}
		$data['taxvalue1'] = $taxvalue1;
		$data['taxvalue2'] = $taxvalue2;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function cat() {
		$lname =$this->request->get['lname'];
		$lname1 = "'".$lname."'";
		$lid=$this->request->get['lid'];
		$tname =$this->request->get['tname'];
		$tname1 = "'".$tname."'";
		$tid=$this->request->get['tid'];
		// echo'<pre>';
		// print_r($tid);
		// exit;
		$categorys =  $this->db->query("SELECT  * FROM oc_category")->rows;
		/*	
		$i=1;
		//$data['html'] = '<a  style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="table('.$lid.','.$lname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="table('.$lid.','.$lname1.')"> <img  src="'.HTTP_CATALOG.'system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		foreach ($categorys as $ckey => $cat) {
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    			<div class="product-thumb transition">
      				<div class="image">
                  		<a onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$cat['category_id'].')">
            				<img src="'.$cat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$cat['category'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          				</a>
              		</div>
      				<div class="caption">
        				<h5 style="text-align: center;">
        					'.$cat['category'].'
        				</h5>
      				</div>
    			</div>
  			</div>';
			$i++;
		}
		*/
		$i=1;
		$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
		foreach ($categorys as $ckey => $cat) {
			if(strlen($cat['category']) > 27){
				$font_size = '22px';
			} elseif(strlen($cat['category']) > 18){
				$font_size = '22px';
			} elseif(strlen($cat['category']) > 9){
				$font_size = '22px';
			} else {
				$font_size = '22px';
			}
			if($cat['colour'] == ''){
				$cat['colour'] = '#ff008c';
			}
			if($i % 5 == 0){
				$html .= '</div>';
				$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
			}
			$html .= '<div class="col-sm-3" style="background-color: '.$cat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding: 0px !important;">';
				$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$cat['category_id'].',0,0)" title='.$cat['category'].' class="">'.$cat['category'].'</a>';
			$html .= '</div>';
			//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$cat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$cat['category_id'].')" title='.$cat['category'].' class="btn btn-primary" >'.$cat['category'].'</a>';
		}
		$html .= '</div>';
		$data['html'] = $html;		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function subcat() {
		$lname =$this->request->get['lname'];
		$lname1 = "'".$lname."'";
		$lid=$this->request->get['lid'];
		$tname =$this->request->get['tname'];
		$tname1 = "'".$tname."'";
		$tid=$this->request->get['tid'];
		$catid=$this->request->get['catid'];
		$subcategorys =  $this->db->query("SELECT  * FROM oc_subcategory WHERE parent_id ='".$catid."'")->rows;
		/*
		$i=1;
		//$data['html'] = '<a style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a><br>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.',0)"> <img  src="'.HTTP_CATALOG.'system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		foreach ($subcategorys as $sckey => $scat) {
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    			<div class="product-thumb transition">
      				<div class="image">
                  		<a onClick="items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')">
            				<img src="'.$scat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$scat['name'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          				</a>
              		</div>
      				<div class="caption">
        				<h5 style="text-align: center;">
          					'.$scat['name'].'
        				</h5>
      				</div>
    			</div>
  			</div>';
			$i++;
		}
		*/
		$i=1;
		$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
		foreach ($subcategorys as $sckey => $scat) {
			if(strlen($scat['name']) > 27){
				$font_size = '22px';
			} elseif(strlen($scat['name']) > 18){
				$font_size = '22px';
			} elseif(strlen($scat['name']) > 9){
				$font_size = '22px';
			} else {
				$font_size = '22px';
			}
			if($scat['colour'] == ''){
				$scat['colour'] = '#ff008c';
			}
			if($i % 5 == 0){
				$html .= '</div>';
				$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
			}
			$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding: 0px !important;">';
				$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" href="javascript:items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')" title='.$scat['name'].' class="">'.$scat['name'].'</a>';
			$html .= '</div>';
			//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')" title='.$scat['name'].' class="btn btn-primary" >'.$scat['name'].'</a>';
		}
		$html .= '</div>';
		$data['html'] = $html;		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function pendingtableinfo() {
		$table_infos =  $this->db->query("SELECT  * FROM oc_order_info WHERE day_close_status = '0' AND bill_status = '0' AND cancel_status = '0'")->rows;
		$token =$this->session->data['token'];
		$html = '<table class="table table-bordered table-hover" style="text-align: center;">';
		$html .=  '<tr>';
		$html .=  '<th style="text-align: center;">Table Name</th>';
		$html .=  '<th style="text-align: center;">Order No</th>';
		$html .=  '<th style="text-align: center;">In Time</th>';
		$html .=  '<th style="text-align: center;">GRAND TOTAL</th>';
		$html .=  '</tr>';
		foreach ($table_infos as $info) {
			$html .=  '<tr>';
			$html .=  '<td>'.$info['t_name'].'</td>';
			$html .=  '<td>'.$info['order_no'].'</td>';
			$html .=  '<td>'.$info['time_added'].'</td>';  
			$html .=  '<td>'.$info['grand_total'].'</td>'; 
			$html .=  '</tr>';
		}
		$html .= '</table>';
		$html .='<a class="btn btn-info" href="index.php?route=catalog/order/releasetable&token='.$token.'">Release Table</a>';
		$data['table_info'] = $html;		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}


	public function currentsaleinfo() {
		$table_datas = $this->db->query("SELECT `name`, `rate`, SUM(`qty`) as quantity, SUM(`amt`) as amount FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`day_close_status` = '0' GROUP BY oit.`code` ")->rows;
		$html = '<table class="table table-bordered table-hover" style="text-align: center;">';
			$html .=  '<tr>';
				$html .=  '<th style="text-align: center;">Name</th>';
				$html .=  '<th style="text-align: center;">Rate</th>';
				$html .=  '<th style="text-align: center;">Quantity</th>';
				$html .=  '<th style="text-align: center;">Total</th>';
			$html .=  '</tr>';
			foreach ($table_datas as $info) {
				$html .=  '<tr>';
					$html .=  '<td>'.$info['name'].'</td>';
					$html .=  '<td>'.$info['rate'].'</td>';  
					$html .=  '<td>'.$info['quantity'].'</td>'; 
					$html .=  '<td>'.$info['amount'].'</td>'; 
				$html .=  '</tr>';
			}
		$html .= '</table>';
		$data['sale_info'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	
	public function items() {
		$lname =$this->request->get['lname'];
		$lname1 = "'".$lname."'";
		$lid=$this->request->get['lid'];
		$tname =$this->request->get['tname'];
		$tname1 = "'".$tname."'";
		$tid=$this->request->get['tid'];
		$catid=$this->request->get['catid'];
		$scatid=$this->request->get['scatid'];
		$items =  $this->db->query("SELECT  * FROM oc_item WHERE item_sub_category_id ='".$scatid."' AND ismodifier = 0")->rows;
		/*	
		$i=1;
		//$data['html'] = '<a  style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a><br>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.')"> <img  src="'.HTTP_CATALOG.'system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		foreach ($items as $sckey => $scat) {
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			// if($scat['is_liq'] == 0){
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    			<div class="product-thumb transition">
      				<div class="image">
                  		<a onClick="add('.$scat['item_id'].')">
            				<img  src="'.$scat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$scat['item_name'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          				</a>
              		</div>
      				<div class="caption">
        				<h5 style="text-align: center;">
          					'.$scat['item_name'].'
        				</h5>
      				</div>
    			</div>
  			</div>';
			$i++;
		}
		*/
		$i=1;
		$html = '<div class="row" style="xwidth: 650px;height: 100px;margin-left: 1%;margin-top: 1%;">';
		foreach ($items as $sckey => $scat) {
			if(strlen($scat['item_name']) > 27){
				$font_size = '22px';
			} elseif(strlen($scat['item_name']) > 18){
				$font_size = '22px';
			} elseif(strlen($scat['item_name']) > 9){
				$font_size = '22px';
			} else {
				$font_size = '22px';
			}
			if($scat['colour'] == ''){
				$scat['colour'] = '#ff008c';
			}
			if($i % 5 == 0){
				$html .= '</div>';
				$html .= '<div class="row" style="width: 100%;height: 20%;margin-left: 1%;margin-top: 1%;">';
			}
			$html .= '<div class="col-sm-3" style="background-color: '.$scat['colour'].';border-color:black;text-align: center;height: 100%;margin-right: 1%;width: 190px;display: table;line-height:1.5em;margin-top:1%;padding:0px !important;">';
				$html .= '<a style="cursor: pointer;color: #000;vertical-align: middle;display: table-cell;width: 100%;height: 100%;font-size: '.$font_size.';;max-width: 141px;word-wrap: break-word;" data-toggle="tooltip" class="add" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="">'.$scat['item_name'].'</a>';
			$html .= '</div>';
			//$data['html'] .= '<a style="cursor: pointer;margin-bottom: 2%;margin-right: 1%;margin-left: 1%;background-color: '.$scat['colour'].';border-color: black;font-size: 30px;width: 22%;padding-top: 3%;padding-bottom: 3%;" data-toggle="tooltip" onClick="add('.$scat['item_id'].')" title='.$scat['item_name'].' class="btn btn-primary" >'.$scat['item_name'].'</a>';
		}
		$html .= '</div>';
		$data['html'] = $html;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function autocomplete4() {
		$json = array();

		if (isset($this->request->get['filter_cname'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_cname' => $this->request->get['filter_cname'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getcaptains($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletecaptainid() {
		$json = array();

		if (isset($this->request->get['filter_ccode'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_ccode' => $this->request->get['filter_ccode'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getcaptainsid($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['code'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocompletecname() {
		$json = array();

		if (isset($this->request->get['filter_ccode'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_ccode' => $this->request->get['filter_ccode'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getcaptaindata($filter_data);

			foreach ($results as $result) {
				$json = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		//$sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['code'];
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function autocomplete3() {
		$json = array();

		if (isset($this->request->get['filter_wname'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_wname' => $this->request->get['filter_wname'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getwaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletewaiterid() {
		$json = array();

		if (isset($this->request->get['filter_wcode'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_wcode' => $this->request->get['filter_wcode'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getwaitersid($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['code'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompletewname() {
		$json = array();

		if (isset($this->request->get['filter_wcode'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_wcode' => $this->request->get['filter_wcode'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getwaitersid($filter_data);

			foreach ($results as $result) {
				$json = array(
					'waiter_id' => $result['waiter_id'],
					'code'        => strip_tags(html_entity_decode($result['code'], ENT_QUOTES, 'UTF-8')),
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		//$sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['code'];
		// }

		// array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function findtable() {
		//echo'innn';
		// echo'<pre>';
		// print_r($this->user->getUserName());
		// exit;
		// $this->log->write('-----------------------Find Table Start------------------------');
		// $this->log->write('User Name : '.$this->user->getUserName());

		$json = array();
		if (isset($this->request->get['filter_tname'])) {
			$this->load->model('catalog/order');
			$filter_data = array(
				'filter_name' => $this->request->get['filter_tname'],
			);
			//$results = $this->model_catalog_order->gettables($filter_data);
		
			$results = $this->model_catalog_order->gettables_data($filter_data);
			if(isset($results['location_id'])){
				// $location_datas = $this->db->query("SELECT `rate_id` FROM `oc_location` WHERE `location_id` = '".$results['location_id']."' ");
				// $rate_id = 1;
				// if($location_datas->num_rows > 0){
				// 	$location_data = $location_datas->row;
				// 	$rate_id = $location_data['rate_id'];
				// }
				// $json['table_id'] = $results['table_id'];
				//unset($this->session->data['c_id']);
				if(isset($this->session->data['c_id'])){
					$results['parcel_detail'] = 0;
				}

				/*
				$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$filter_data['filter_name']."' ";
				$running_datas = $this->db->query($running_sql);
				if($running_datas->num_rows > 0){
					$json['status'] = 2;
				} else {
					$sql = "INSERT INTO `oc_running_table` SET `table_id` = '".$filter_data['filter_name']."' ";
					$this->db->query($sql);
					$this->session->data['table_id'] = $filter_data['filter_name'];
					$json['status'] = 1;	
				}
				*/

				$json['table_id'] = $filter_data['filter_name'];
				$json['loc_id'] = $results['location_id'];
				$json['parcel_detail'] = $results['parcel_detail'];
				$json['rate_id'] = $results['rate_id'];
				$json['service_charge'] = $results['service_charge'];
				$json['loc_name'] = strip_tags(html_entity_decode($results['location'], ENT_QUOTES, 'UTF-8'));
				$json['name'] = strip_tags(html_entity_decode($filter_data['filter_name'], ENT_QUOTES, 'UTF-8'));
				$json['status'] = 1;	
			} else {
				$json['status'] = 0;
			}
		}

		// $this->log->write('Json Datas : '.print_r($json,true));
		// $this->log->write('------------------------------Find Table End-------------------------------');

		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		// array_multisort($sort_order, SORT_ASC, $json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function autocomplete1() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getlocations($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'location_id' => $result['location_id'],
					'location'        => strip_tags(html_entity_decode($result['location'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['location'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/order');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_order->getOrders($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'order_id' => $result['order_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteadvance() {
		$json = array();

		if (isset($this->request->get['advance_id'])) {
			$advanceAmount = $this->db->query("SELECT advance_amt,advance_billno FROM oc_advance WHERE advance_billno = '".$this->request->get['advance_id']."' AND status = '0'")->row;
				$json = array(
					'advance_amt' => $advanceAmount['advance_amt']
				);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/order')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/order')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		// foreach ($this->request->post['selected'] as $order_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsByOrderId($order_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }

		return !$this->error;
	}

	public function upload() {
		$this->load->language('account/order');
		$json = array();
		
		$file_show_path = HTTP_SERVER.'system/storage/download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				//$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function printcancel() {
		// echo'inn';
		// exit();
		if (isset($this->request->get['orderid'])) {
			$order_id = $this->request->get['orderid'];
			$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		// 	echo'<pre>';
		// print_r($ans);
		// exit;
			if(isset($this->request->get['cancel'])){
				$duplicate = $this->request->get['cancel'];
			} else{
				$duplicate = '0';
			}

			if(isset($this->request->get['advance_id'])){
				$advance_id = $this->request->get['advance_id'];
			} else{
				$advance_id = '0';
			}

			if(isset($this->request->get['advance_amount'])){
				$advance_amount = $this->request->get['advance_amount'];
			} else{
				$advance_amount = '0';
			}

			if(isset($this->request->get['grand_total'])){
				$grand_total = $this->request->get['grand_total'];
			} else{
				$grand_total = '0';
			}
			$orderno = '0';

			if(($ans['bill_status'] == 0 && $duplicate == '0') || ($ans['bill_status'] == 1 && $duplicate == '1')){
				date_default_timezone_set('Asia/Kolkata');
				$this->load->language('customer/customer');
				$this->load->model('catalog/order');
				$merge_datas = array();
				$this->document->setTitle('BILL');
				$te = 'BILL';
				
				$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '0' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates = $this->db->query($last_open_date_sql);
					if($last_open_dates->num_rows > 0){
						$last_open_date = $last_open_dates->row['bill_date'];
					} else {
						$last_open_date = date('Y-m-d');
					}
				}

				$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
				$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
				if($last_open_dates_liq->num_rows > 0){
					$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
				} else {
					$last_open_date_liq_sql = "SELECT oi.`bill_date` FROM `oc_order_info_report` oi LEFT JOIN oc_order_items_report oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`year_close_status` = '0' AND oi.`order_no` <> '0' AND oit.`is_liq` = '1' ORDER BY oi.`bill_date` DESC LIMIT 1";
					$last_open_dates_liq = $this->db->query($last_open_date_liq_sql);
					if($last_open_dates_liq->num_rows > 0){
						$last_open_date_liq = $last_open_dates_liq->row['bill_date'];
					} else {
						$last_open_date_liq = date('Y-m-d');
					}
				}

				$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
				$last_open_dates_order = $this->db->query($last_open_date_sql_order);
				if($last_open_dates_order->num_rows > 0){
					$last_open_date_order = $last_open_dates_order->row['bill_date'];
				} else {
					$last_open_date_order = date('Y-m-d');
				}

				if($ans['order_no'] == '0'){
					$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
					$orderno = 1;
					if(isset($orderno_q['order_no'])){
						$orderno = $orderno_q['order_no'] + 1;
					}

					$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
					if($kotno2->num_rows > 0){
						$kot_no2 = $kotno2->row['billno'];
						$kotno = $kot_no2 + 1;
					} else{
						$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_items_report` oit WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");
						if($kotno2->num_rows > 0){
							$kot_no2 = $kotno2->row['billno'];
							$kotno = $kot_no2 + 1;
						} else {
							$kotno = 1;
						}
					}

					$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
					if($kotno1->num_rows > 0){
						$kot_no1 = $kotno1->row['billno'];
						$botno = $kot_no1 + 1;
					} else{
						$kotno1 = $this->db->query("SELECT `billno` FROM `oc_order_items` oit WHERE `bill_date` = '".$last_open_date_liq."' AND is_liq = 1 order by `billno` DESC LIMIT 1");
						if($kotno1->num_rows > 0){
							$kot_no1 = $kotno1->row['billno'];
							$botno = $kot_no1 + 1;
						} else {
							$botno = 1;
						}
					}

					// $this->db->query("UPDATE oc_order_items SET billno = '".$kotno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0' ");
					// $this->db->query("UPDATE oc_order_items SET billno = '".$botno."' WHERE is_liq = 1 AND order_id = '".$order_id."' AND cancelstatus = '0' ");

					//$ansbb = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					// if($this->model_catalog_order->get_settings('SETTLEMENT_ON') == 1){
					// 	$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."', pay_cash = '".$ans['grand_total']."', payment_status = '1', total_payment = '".$ans['grand_total']."', pay_method = '1' WHERE `order_id` = '".$order_id."' ";
					// } else{
					// 	$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
					// }
					$apporder = $this->db->query("SELECT app_order_id FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
					// if($apporder['app_order_id'] != '0'){
					// 	$this->db->query("UPDATE oc_order_app SET status = 3 WHERE order_id = '".$apporder['app_order_id']."'");
					// }
					//$this->db->query($update_sql);
				}

				// if($advance_id != '0'){
				// 	$this->db->query("UPDATE oc_order_info SET advance_billno = '".$advance_id."', advance_amount = '".$advance_amount."', grand_total = '".$grand_total."' WHERE order_id = '".$order_id."'");
				// }
				$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;

				$testfood = array();
				$testliq = array();
				$testtaxvalue1food = 0;
				$testtaxvalue1liq = 0;
				$tests = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '0' GROUP BY tax1")->rows;
				foreach($tests as $test){
					$amt = ($test['amt'] + $test['stax']) - $test['discount_value'];
					$testfoods[] = array(
						'tax1' => $test['tax1'],
						'amt' => $amt,
						'tax1_value' => $test['tax1_value']
					);
				}
				
				$testss = $this->db->query("SELECT SUM(amt) as amt, SUM(stax) as stax, SUM(tax1_value) as tax1_value, SUM(discount_value) as discount_value, is_liq, tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' AND is_liq = '1' GROUP BY tax1")->rows;
				foreach($testss as $testa){
					$amts = ($testa['amt'] + $testa['stax']) - $testa['discount_value'];
					$testliqs[] = array(
						'tax1' => $testa['tax1'],
						'amt' => $amts,
						'tax1_value' => $testa['tax1_value']
					);
				}
				
				$infosl = array();
				$infos = array();
				$flag = 0;
				$totalquantityfood = 0;
				$totalquantityliq = 0;
				$disamtfood = 0;
				$disamtliq = 0;
				$modifierdatabill = array();
				foreach ($anss as $lkey => $result) {
					foreach($anss as $lkeys => $results){
						if($lkey == $lkeys) {

						} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['code'] = '';
								}
							}
						} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
							if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
								if($result['parent'] == '0'){
									$result['qty'] = $result['qty'] + $results['qty'];
									if($result['nc_kot_status'] == '0'){
										$result['amt'] = $result['qty'] * $result['rate'];
									}
								}
							}
						}
					}
					
					if($result['code'] != ''){
						$decimal_mesurement= $this->db->query("SELECT `decimal_mesurement` FROM oc_item WHERE item_code = '".$result['code']."' ")->row['decimal_mesurement'];
						if ($decimal_mesurement == 0) {
								$qty = (int)$result['qty'];
						} else {
								$qty = $result['qty'];
						}
						if($result['is_liq']== 0){
							$infos[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityfood = $totalquantityfood + $result['qty'];
							$disamtfood = $disamtfood + $result['discount_value'];
						} else {
							$flag = 1;
							$infosl[] = array(
								'id'			=> $result['id'],
								'billno'		=> $result['billno'],
								'name'          => $result['name'],
								'rate'          => $result['rate'],
								'amt'           => $result['amt'],
								'qty'         	=> $qty,
								'tax1'         	=> $result['tax1'],
								'tax2'          => $result['tax2'],
								'discount_value'=> $result['discount_value']
							);
							$modifierdatabill[$result['id']] = $this->db->query("SELECT `id`, `code`, `name`, `rate`, `qty`, `amt` FROM oc_order_items WHERE parent_id = '".$result['id']."' AND ismodifier = '0'")->rows;
							$totalquantityliq = $totalquantityliq + $result['qty'];
							$disamtliq = $disamtliq + $result['discount_value'];
						}
					}
				}
				
				$merge_datas = array();
				if($orderno != '0'){
					$merge_datas = $this->db->query("SELECT `ftotal`, `ltotal`, `order_no`, `gst`, `vat`, `ftotalvalue`, `discount`,`ldiscount`,`ltotalvalue` FROM oc_order_info WHERE merge_number = '".$orderno."' AND `order_no` <> '".$orderno."' ")->rows;
				}
				
				if($ans['parcel_status'] == '0'){
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal = ($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					} else {
						$gtotal = ($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']) + ($ans['stax']);
					}
				} else {
					if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
						$gtotal =($ans['ftotal']+$ans['ltotal'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					} else {
						$gtotal =($ans['ftotal']+$ans['ltotal']+$ans['gst']+$ans['vat'])-($ans['ftotalvalue'] + $ans['ltotalvalue']);
					}
				}

				if($duplicate == '1'){
					$this->db->query("UPDATE `oc_order_info` SET duplicate = '1', duplicate_time = '".date('h:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE `order_id` = '".$order_id."'");
					$duplicatebilldata = $this->db->query("SELECT `order_no`, grand_total, item_quantity, `login_name`, `duplicate_time` FROM `oc_order_info` WHERE `duplicate` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND day_close_status = '0' AND order_id = '".$order_id."'");
					$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
					$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
					$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
					if ($setting_value_link_1 != '') {
						$text = "Duplicate%20Bill%20-%20Ref%20No%20:".$duplicatebilldata->row['order_no'].",%20Qty%20:".$duplicatebilldata->row['item_quantity'].",%20Amt%20:".$duplicatebilldata->row['grand_total'].",%20login%20name%20:".$duplicatebilldata->row['login_name'].",%20Time%20:".$duplicatebilldata->row['duplicate_time'];
						$link = $link_1."&phone=".$setting_value_number."&text=".$text;
						//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
						//echo $link;exit;
						$ret = file($link);
					} 
				}
				
				$csgst=$ans['gst']/2;
				$csgsttotal = $ans['gst'];

				$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
				$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
				$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
				$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
				$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
				if($ans['advance_amount'] == '0.00'){
					$gtotal = utf8_substr(html_entity_decode($gtotal, ENT_QUOTES, 'UTF-8'), 0, 9);
				} else{
					$gtotal = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
				}
				//$gtotal = ceil($gtotal);
				$gtotal = round($gtotal);


				$printtype = '';
				$printername = '';

				if ($printtype == '' || $printername == '' ) {
					$printtype = $this->user->getPrinterType();
					$printername = $this->user->getPrinterName();
					$bill_copy = 1;
				}

				if ($printtype == '' || $printername == '' ) {
					$locationData =  $this->db->query("SELECT `bill_copy`, `bill_printer_type`, `bill_printer_name` FROM oc_location WHERE location_id = '".$ans['location_id']."'");
					if($locationData->num_rows > 0){
						$locationData = $locationData->row;
						$printtype = $locationData['bill_printer_type'];
						$printername = $locationData['bill_printer_name'];
						$bill_copy = $locationData['bill_copy'];
					} else{
						$printtype = '';
						$printername = '';
						$bill_copy = 1;
					}
				}
				
				if(($printtype == 'Please Select' || $printtype == '') && $printername == ''){
					$printtype = $this->model_catalog_order->get_settings('PRINTER_TYPE');
				 	$printername = $this->model_catalog_order->get_settings('PRINTER_NAME');
				}

				$printerModel = $this->model_catalog_order->get_settings('PRINTER_MODEL');
				if($printerModel ==0){


						try {
						    if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	// 	$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
						 	}
						 	if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	for($i = 1; $i <= $bill_copy; $i++){
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    if($ans['bill_status'] == 1 && $duplicate == '1'){
								    	$printer->feed(1);
								    	$printer->text("canceled Bill");
								    }
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
										
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
										$printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
										$printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Name :".$ans['cust_name']."");
									    $printer->feed(1);
									}
									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Mobile :".$ans['cust_contact']."");
									    $printer->feed(1);
									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
									    $printer->text("Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infos){
								   			
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infos[0]['billno'],5)."Time :".date('H:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$orderno."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",24)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infos as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
								    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

									    }
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ans['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
											$printer->feed(1);
										} elseif($ans['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",25)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",25)."CCGST :".$csgst."");
										$printer->feed(1);
										if($ans['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",25)."SCRG :".$ans['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											}else{
												$printer->text(str_pad("",25)."SCRG :".$ans['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ans['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
										$printer->setEmphasis(false);
									}
									
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
							   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
							   			$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);	
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infosl){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_CENTER);
								   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time :".date('H:i:s'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$orderno."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",10)." ".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ans['location'],10)." ".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",24)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_liquor_normal = 0;
									    $total_quantity_liquor_normal = 0;
									    foreach($infosl as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
									    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
									    	$printer->feed(1);
								    	 	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
									    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 24);
														    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],24)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}

									    	$total_items_liquor_normal ++;
									    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
									    }
									    $printer->text("----------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("----------------------------------------------");
										$printer->feed(1);
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ans['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
											$printer->feed(1);
										} elseif($ans['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
										$printer->feed(1);
										if($ans['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											} else{
												$printer->text(str_pad("",34)."SCRG :".$ans['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ans['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",29)."Net total :".ceil($netamountliq)."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									if($ans['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_RIGHT);
									$printer->text("GRAND TOTAL  :  ".$gtotal);
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ans['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ans['dtotalvalue']);
											$printer->feed(1);
										}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									if($SETTLEMENT_status == '1'){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: '".$onacname."'");
											$printer->feed(1);
											$printer->text("Contact: '".$onaccontact."'");
										}
									}
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("----------------------------------------------");
								    $printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number :".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],32)."SGST :".$mvalue['gst']/2);
											$printer->feed(1);
											$printer->text(str_pad("",32)."CGST :".$mvalue['gst']/2);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],32)."VAT :".$mvalue['vat']."");
											}
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("----------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("----------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("----------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								}
								// Close printer //
							    $printer->close();
							 //    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    // $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
						    // $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
					
				}
				else{  // 45 space code starts from here


						try {
						    if($printtype == 'Network'){
						 		$connector = new NetworkPrintConnector($printername, 9100);
						 	} else if($printtype == 'Windows'){
						 		$connector = new WindowsPrintConnector($printername);
						 	} else {
						 		$connector = '';
						 	// 	$this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
						 	}
						 	if($connector != ''){
							    $printer = new Printer($connector);
							    $printer->selectPrintMode(32);

							   	$printer->setEmphasis(true);
							   	// $printer->text('45 Spacess');
				    			$printer->feed(1);
							   	for($i = 1; $i <= $bill_copy; $i++){
								   	$printer->setTextSize(2, 1);
								   	$printer->setJustification(Printer::JUSTIFY_CENTER);
								   	$printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
								    $printer->feed(1);
								    $printer->setTextSize(1, 1);
								    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
								    if($ans['bill_status'] == 1 && $duplicate == '1'){
								    	$printer->feed(1);
								    	$printer->text("canceled Bill");
								    }
								    $printer->setJustification(Printer::JUSTIFY_CENTER);
								    $printer->feed(1);
								    $printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
									if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
										
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
										$printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
										$printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
									 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
									 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}
									else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Name :".$ans['cust_name']."");
									    $printer->feed(1);
									}
									else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Mobile :".$ans['cust_contact']."");
									    $printer->feed(1);
									}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
									    $printer->text("Address : ".$ans['cust_address']."");
									    $printer->feed(1);
									}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
									    $printer->text("Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}else{
									    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
									    $printer->feed(1);
									    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
									    $printer->feed(1);
									}
									$printer->text(str_pad("User Id :".$ans['login_id'],26)."K.Ref.No :".$order_id);
								    $printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
								   	if($infos){
								   			
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infos[0]['billno'],6)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$orderno."");
								   		$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",8)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								   	 	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_normal = 0;
										$total_quantity_normal = 0;
									    foreach($infos as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0,7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],20)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".str_pad($nvalue['amt'],8));
									    	$printer->feed(1);
								    	 	if($modifierdatabill != array()){
											    	foreach($modifierdatabill as $key => $value){
										    			$printer->setTextSize(1, 1);
										    			if($key == $nvalue['id']){
										    				foreach($value as $modata){
										    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] =html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8');
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 7);
													    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],8)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
													    		$printer->feed(1);
												    		}
												    	}
										    		}
										    	}
										    	$total_items_normal ++ ;
									    		$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];

									    }
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_normal,5)."T Qty :".str_pad($total_quantity_normal,7)."F.Total :".$ans['ftotal']);
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										foreach($testfoods as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ans['fdiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
											$printer->feed(1);
										} elseif($ans['discount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",25)."SCGST :".$csgst."");
										$printer->feed(1);
										$printer->text(str_pad("",25)."CCGST :".$csgst."");
										$printer->feed(1);
										if($ans['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											}else{
												$printer->text(str_pad("",28)."SCRG :".$ans['staxfood']."");
												$printer->feed(1);
												$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountfood = ($ans['ftotal'] - ($disamtfood));
											} else{
												$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",25)."Net total :".ceil($netamountfood)."");
										$printer->setEmphasis(false);
									}
									
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
								   	$printer->setTextSize(1, 1);
							   		if($this->model_catalog_order->get_settings('BAR_NAME') != ''){
							   			$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->feed(1);	
										$printer->text($this->model_catalog_order->get_settings('BAR_NAME'));
										if($this->model_catalog_order->get_settings('BAR_ADD') != ''){
											$printer->feed(1);				   		
									   		$printer->text($this->model_catalog_order->get_settings('BAR_ADD'));
								   		}
								   		$printer->feed(1);	
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   	}
								   	if($infosl){
								   		$printer->feed(1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								   		$printer->text("Date:".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),11)."Bill no: ".str_pad($infosl[0]['billno'],5)."Time:".date('H:i'));
								   		$printer->feed(1);
								   		$printer->text("Ref no: ".$orderno."");
								 		$printer->feed(1);
								 		$printer->setEmphasis(true);
								   		$printer->setTextSize(1, 1);
								   		$printer->setJustification(Printer::JUSTIFY_LEFT);
								    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
								    	$printer->text(str_pad("Loc",8)." ".str_pad("Tbl No",8)."".str_pad("Wtr",8)."".str_pad("Cpt",8)."Person");
								    	$printer->feed(1);
								   		$printer->setTextSize(1, 1);
								   		$printer->text(str_pad($ans['location'],8)." ".str_pad($ans['t_name'],8)."".str_pad($ans['waiter_id'],8)."".str_pad($ans['captain_id'],8)."".$ans['person']."");
									    $printer->feed(1);
								   		$printer->setEmphasis(false);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										$printer->text(str_pad("Name",20)." ".str_pad("Rate",7)."".str_pad("Qty",8)."".str_pad("Amt",8));
										$printer->feed(1);
								    	$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setEmphasis(false);
										$total_items_liquor_normal = 0;
									    $total_quantity_liquor_normal = 0;
									    foreach($infosl as $nkey => $nvalue){
									    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
									    	$nvalue['rate'] =utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'),0, 7);
									    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 8);
									    	$printer->text("".str_pad($nvalue['name'],24)." ".str_pad($nvalue['rate'],7)."".str_pad($nvalue['qty'],8)."".$nvalue['amt'],8);
									    	$printer->feed(1);
								    	 	if($modifierdatabill != array()){
										    	foreach($modifierdatabill as $key => $value){
									    			$printer->setTextSize(1, 1);
									    			if($key == $nvalue['id']){
									    				foreach($value as $modata){
									    					$modata['name'] = utf8_substr(html_entity_decode($modata['name'], ENT_QUOTES, 'UTF-8'), 0, 20);
														    	$modata['rate'] = utf8_substr(html_entity_decode($modata['rate'], ENT_QUOTES, 'UTF-8'), 0, 7);
														    	$modata['qty'] = utf8_substr(html_entity_decode($modata['qty'], ENT_QUOTES, 'UTF-8'), 0, 8);
												    		$printer->text(str_pad(".".$modata['name'],20)." ".str_pad($modata['rate'],7)."".str_pad($modata['qty'],8)."".str_pad($modata['amt'],8));
												    		$printer->feed(1);
											    		}
											    	}
									    		}
									    	}

									    	$total_items_liquor_normal ++;
									    	$total_quantity_liquor_normal = $total_quantity_liquor_normal + $nvalue['qty'];
									    }
									    $printer->text("------------------------------------------");
									    $printer->feed(1);
									    $printer->setJustification(Printer::JUSTIFY_LEFT);
									    $printer->text("T Items: ".str_pad($total_items_liquor_normal,5)."T Qty :".str_pad($total_quantity_liquor_normal,7)."L.Total :".str_pad($ans['ltotal'],7));
									    $printer->feed(1);
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									    $printer->text("------------------------------------------");
										$printer->feed(1);
										foreach($testliqs as $tkey => $tvalue){
											$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
									    	$printer->feed(1);
										}
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_LEFT);
										if($ans['ldiscountper'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ldiscountper']."%) :".$ans['ltotalvalue']."");
											$printer->feed(1);
										} elseif($ans['ldiscount'] != '0'){
											$printer->text(str_pad("",21)."Discount(".$ans['ltotalvalue']."rs) :".$ans['ltotalvalue']."");
											$printer->feed(1);
										}
										$printer->text(str_pad("",35)."VAT :".$ans['vat']."");
										$printer->feed(1);
										if($ans['parcel_status'] == '0'){
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$printer->text(str_pad("",28)."SCRG :".$ans['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											} else{
												$printer->text(str_pad("",28)."SCRG :".$ans['staxliq']."");
												$printer->feed(1);
												$netamountliq = (($ans['vat'] + $ans['ltotal']) - ($disamtliq)) + ($ans['staxliq']);
											}
										} else{
											if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
												$netamountliq = ($ans['vat'] + $ans['ltotal'] - ($disamtliq));
											} else{
												$netamountliq = ($ans['ltotal'] - ($disamtliq));
											}
										}
										$printer->setEmphasis(true);
										$printer->text(str_pad("",28)."Net total :".ceil($netamountliq)."");
									    $printer->setEmphasis(false);
									   	$printer->setTextSize(1, 1);
									}
									$printer->feed(1);
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(true);
									if($ans['advance_amount'] != '0.00'){
										$printer->text(str_pad("Advance Amount",28).$ans['advance_amount']."");
										$printer->feed(1);
									}
									$printer->setTextSize(2, 2);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
									$printer->setTextSize(1, 1);
									$printer->feed(1);
									if($ansb['dtotalvalue']!=0){
											$printer->text("Delivery Charge:".$ansb['dtotalvalue']);
											$printer->feed(1);
										}
									$SETTLEMENT_status = $this->model_catalog_order->get_settings('SETTLEMENT_ON');
									if($SETTLEMENT_status =='1'){
										if(isset($this->session->data['credit'])){
											$credit = $this->session->data['credit'];
										} else {
											$credit = '0';
										}
										if(isset($this->session->data['cash'])){
											$cash = $this->session->data['cash'];
										} else {
											$cash = '0';
										}
										if(isset($this->session->data['online'])){
											$online = $this->session->data['online'];
										} else {
											$online ='0';
										}

										if(isset($this->session->data['onac'])){
											$onac = $this->session->data['onac'];
											$onaccontact = $this->session->data['onaccontact'];
											$onacname = $this->session->data['onacname'];

										} else {
											$onac ='0';
										}
									}
									if($SETTLEMENT_status=='1'){
										if($credit!='0' && $credit!=''){
											$printer->text("PAY BY: CARD");
										}
										if($online!='0' && $online!=''){
											$printer->text("PAY BY: ONLINE");
										}
										if($cash!='0' && $cash!=''){
											$printer->text("PAY BY: CASH");
										}
										if($onac!='0' && $onac!=''){
											$printer->text("PAY BY: ON.ACCOUNT");
											$printer->feed(1);
											$printer->text("Name: '".$onacname."'");
											$printer->feed(1);
											$printer->text("Contact: '".$onaccontact."'");
										}
									}
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
								    $printer->text("------------------------------------------");
								    $printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_LEFT);
									$printer->setEmphasis(false);
								   	$printer->setTextSize(1, 1);
								   
									if($merge_datas){
										$printer->feed(2);
										$printer->setJustification(Printer::JUSTIFY_CENTER);
										$printer->text("Merged Bills");
										$printer->feed(1);
										$mcount = count($merge_datas) - 1;
										foreach($merge_datas as $mkey => $mvalue){
											$printer->setJustification(Printer::JUSTIFY_LEFT);
											$printer->text("Bill Number:".$mvalue['order_no']."");
											$printer->feed(1);
											$printer->text(str_pad("F Total :".$mvalue['ftotal'],28)."SGST :".$mvalue['gst']/2);
											$printer->feed(1);
											$printer->text(str_pad("",28)."CGST :".$mvalue['gst']/2);
											$printer->feed(1);
											if($mvalue['ltotal'] > '0'){
												$printer->text(str_pad("L Total :".$mvalue['ltotal'],28)."VAT :".$mvalue['vat']."");
											}
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']);
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												} else{
													$sub_gtotal = ($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']);
												}
											}
											$printer->setJustification(Printer::JUSTIFY_RIGHT);
											$printer->text("GRAND TOTAL  :  ".$sub_gtotal."");
											$printer->feed(1);
											if($ans['parcel_status'] == '0'){
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']) + ($mvalue['stax']));
												}
											} else{
												if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												} else{
													$gtotal = $gtotal + (($mvalue['ltotal'] + $mvalue['ftotal'] + $mvalue['gst'] + $mvalue['vat']) - ($mvalue['ftotalvalue'] + $mvalue['ltotalvalue']));
												}
											}
											if($mkey < $mcount){ 
												$printer->text("------------------------------------------");
												$printer->feed(1);
											}
										}
										$printer->feed(1);
										$printer->text("------------------------------------------");
										$printer->feed(1);
										$printer->setJustification(Printer::JUSTIFY_RIGHT);
										$printer->text(str_pad("MERGED GRAND TOTAL",38).$gtotal."");
										$printer->feed(1);
									}
									$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
									$printer->feed(1);
									if($this->model_catalog_order->get_settings('TEXT1') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT1'));
										$printer->feed(1);
									}
									if($this->model_catalog_order->get_settings('TEXT2') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT2'));
										$printer->feed(1);
									}
									$printer->text("------------------------------------------");
									$printer->feed(1);
									$printer->setJustification(Printer::JUSTIFY_CENTER);
									if($this->model_catalog_order->get_settings('TEXT3') != ''){
										$printer->text($this->model_catalog_order->get_settings('TEXT3'));
									}
									$printer->feed(2);
									$printer->cut();
								}
								// Close printer //
							    $printer->close();
							 //    $this->db->query("UPDATE oc_order_info SET printstatus = 2 WHERE order_id = '".$order_id."'");
								// $this->db->query("UPDATE oc_order_items SET printstatus = 2 WHERE order_id = '".$order_id."'");
							}
						} catch (Exception $e) {
						    // $this->db->query("UPDATE oc_order_info SET printstatus = 1 WHERE order_id = '".$order_id."'");
						    // $this->db->query("UPDATE oc_order_items SET printstatus = 1 WHERE order_id = '".$order_id."'");
						    $this->session->data['warning'] = $printername." "."Not Working";
						}
				}
				 

				if($ans['bill_status'] == 1 && $duplicate == '1'){
					//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
					$json = array(
						'status' => 1,
					);
				//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				} else{
					$json = array();
					$json = array(
						'status' => 1,
					);
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			} else {
				$this->session->data['warning'] = 'Bill already printed';
				$this->request->post = array();
				$_POST = array();
				$json = array(
						'status' => 1,
					);
				//$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
		}
		// $this->request->post = array();
		// $_POST = array();
		// $this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}

	public function releasetable(){
		$this->db->query("TRUNCATE TABLE oc_running_table");
		$this->response->redirect($this->url->link('catalog/order', 'token=' . $this->session->data['token'],true));
	}

}