<?php
class ControllerCatalogTablechange extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/item');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getForm();
	}

	public function edit() {
		//echo'innn edit';exit;
		$this->log->write("---------------------------Table Chnage Start-----------------");
		$this->log->write('User Name : ' .$this->user->getUserName());
		$this->log->write('Table transfer From : ' .$this->request->post['table_frm']);
		$this->log->write('Table transfer To  : ' .$this->request->post['table_to']);




		$this->load->language('catalog/table');
		$this->document->setTitle($this->language->get('heading_title'));
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->session->data['success'] = $this->language->get('text_success');
			
			$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
			$last_open_dates = $this->db->query($last_open_date_sql);
			if($last_open_dates->num_rows > 0){
				$last_open_date = $last_open_dates->row['bill_date'];
			} else {
				$last_open_date = date('Y-m-d');
			}

			$table_from = $this->request->post['table_frm'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$table_from);
			$sql = "SELECT `location_id`, `location` FROM " . DB_PREFIX . "location WHERE 1=1 ";
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		 	$is_exist_tables_from = $this->db->query($sql);
		 	$from_location = '';
		 	$from_location_id = 0;
		 	if($is_exist_tables_from->num_rows > 0){
		 		$is_exist_table_from = $is_exist_tables_from->row;
		 		$from_location = $is_exist_table_from['location'];
		 		$from_location_id = $is_exist_table_from['location_id'];
			}
			
			$table_to = $this->request->post['table_to'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$table_to);
			$sql = "SELECT `location_id`, `location` FROM " . DB_PREFIX . "location WHERE 1=1 ";
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		 	$is_exist_tables_to = $this->db->query($sql);
		 	$to_location = '';
		 	$to_location_id = 0;
		 	if($is_exist_tables_to->num_rows > 0){
		 		$is_exist_table_to = $is_exist_tables_to->row;
		 		$to_location = $is_exist_table_to['location'];
		 		$to_location_id = $is_exist_table_to['location_id'];
			}

			$order_info_datas_from = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND `table_id` = '".$this->request->post['table_frm']."' ORDER BY `order_id` ");
		 	$order_info_data_from = array();
		 	$order_id_from = 0;
		 	if($order_info_datas_from->num_rows > 0){
		 		$order_info_data_from = $order_info_datas_from->row;
	 			$order_id_from = $order_info_data_from['order_id'];
		 	}

			$order_info_datas_to = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND `table_id` = '".$this->request->post['table_to']."' ORDER BY `order_id` ");
		 	$order_info_data_to = array();
		 	$order_id_to = 0;
		 	if($order_info_datas_to->num_rows > 0){
		 		$order_info_data_to = $order_info_datas_to->row;
	 			$order_id_to = $order_info_data_to['order_id'];
		 	}
			
			$from_order_items_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_items WHERE `order_id` = '".$order_id_from."' ");
			$from_order_items_data = array();
			if($from_order_items_datas->num_rows > 0){
				$from_order_items_data = $from_order_items_datas->rows;
			}

			$to_order_items_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_items WHERE `order_id` = '".$order_id_to."' ");
			$to_order_items_data = array();
			if($to_order_items_datas->num_rows > 0){
				$to_order_items_data = $to_order_items_datas->rows;
			}

			if($to_order_items_data){
				foreach($to_order_items_data as $tkeys => $tvalues){
					foreach($from_order_items_data as $fkeys => $fvalues){
						if($tvalues['code'] == $fvalues['code'] && $tvalues['rate'] == $fvalues['rate'] && $tvalues['message'] == $fvalues['message'] && $tvalues['cancelstatus'] == '0' && $tvalues['ismodifier'] == '1' && $tvalues['parent'] == '0'){
							$new_quantity = $tvalues['qty'] + $fvalues['qty'];
							$new_amount = $tvalues['rate'] * $new_quantity;
							$sql = "UPDATE `oc_order_items` SET `qty` = '".$new_quantity."', `amt` = '".$new_amount."' WHERE `id` = '".$tvalues['id']."'";
							$this->db->query($sql);
							unset($from_order_items_data[$fkeys]);
							break;
						}
					}
				}
				if($from_order_items_data){
					foreach($from_order_items_data as $fkeys => $fvalues){
						if($fvalues['ismodifier'] == '0' && $fvalues['cancelstatus'] == '0'){
							$fvalues['parent_id'] = $autoincrementparentid;
						}
						$sql = "INSERT INTO `oc_order_items` SET 
								`order_id` = '".$order_id_to."', 
								`code` = '".$fvalues['code']."', 
								`name` = '".$fvalues['name']."', 
								`qty` = '".$fvalues['qty']."', 
								`rate` = '".$fvalues['rate']."', 
								`ismodifier` = '".$fvalues['ismodifier']."',
								`parent` = '".$fvalues['parent']."',
								`parent_id` = '".$fvalues['parent_id']."',
								`amt` = '".$fvalues['amt']."', 
								`subcategoryid` = '".$fvalues['subcategoryid']."', 
								`message` = '".$fvalues['message']."', 
								`is_liq` = '".$fvalues['is_liq']."', 
								`kot_status` = '".$fvalues['kot_status']."', 
								`pre_qty` = '".$fvalues['pre_qty']."', 
								`prefix` = '".$fvalues['prefix']."', 
								`is_new` = '".$fvalues['is_new']."', 
								`kot_no` = '".$fvalues['kot_no']."', 
								`reason` = '".$fvalues['reason']."', 
								`billno` = '".$fvalues['billno']."', 
								`tax1` = '".$fvalues['tax1']."', 
								`tax2` = '".$fvalues['tax2']."', 
								`time` = '".$fvalues['time']."',
								`date` = '".$fvalues['date']."',
								`bill_date` = '".$fvalues['bill_date']."',
								`login_id` = '".$fvalues['login_id']."',
								`login_name`= '".$fvalues['login_name']."',
								`cancelstatus` = '".$fvalues['cancelstatus']."',
								`transfer_qty` = '".$fvalues['transfer_qty']."', 
								`nc_kot_status` = '".$fvalues['nc_kot_status']."', 
								`nc_kot_reason` = '".$fvalues['nc_kot_reason']."' "; 
						// echo $sql;
						// echo '<br />';
						$this->db->query($sql);
						$orderitemid = $this->db->getLastId();
						if($fvalues['ismodifier'] == '1' && $fvalues['cancelstatus'] == '0'){
							$autoincrementparentid = $orderitemid;
						}
						//$this->log->write($sql);
					}
				}
				$sql = "DELETE FROM `oc_order_info` WHERE `order_id` = '".$order_id_from."' ";
				$this->db->query($sql);
				//$this->log->write($sql);

				$sql = "DELETE FROM `oc_order_items` WHERE `order_id` = '".$order_id_from."' ";
				$this->db->query($sql);
				//$this->log->write($sql);
				
				$this->updateorderinfo($order_id_to);
			} else {
				$insertdata = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id_from."'")->row;
				$this->db->query("INSERT " . DB_PREFIX . "order_info SET 
							t_name = '" . $this->request->post['table_to']. "',
							table_id = '" . $this->request->post['table_to']. "',
							location = '". $to_location. "',
							location_id = '". $to_location_id. "',
							`parcel_status` = '" . $this->db->escape($insertdata['parcel_status']) ."',
							`cust_name` = '" . $this->db->escape($insertdata['cust_name']) ."', 
							`cust_id` = '" . $this->db->escape($insertdata['cust_id']) ."',
							`cust_contact` = '" . $this->db->escape($insertdata['cust_contact']) ."',
							`cust_address` = '" . $this->db->escape($insertdata['cust_address']) ."',
							`cust_email` = '" . $this->db->escape($insertdata['cust_email']) ."',
							`gst_no` = '" . $this->db->escape($insertdata['gst_no']) ."',
							`waiter_code` = '" . $this->db->escape($insertdata['waiter_code']) . "', 
							`waiter` = '" . $this->db->escape($insertdata['waiter']) . "',  
							`waiter_id` = '" . $this->db->escape($insertdata['waiter_id']) . "', 
							`captain_code` = '" . $this->db->escape($insertdata['captain_code']) . "',   
							`captain` = '" . $this->db->escape($insertdata['captain']) . "',  
							`captain_id` = '" . $this->db->escape($insertdata['captain_id']) . "',
							`person` = '" . $this->db->escape($insertdata['person']) . "',
							`ftotal` = '" . $this->db->escape($insertdata['ftotal']) . "',
							`ftotal_discount` = '" . $this->db->escape($insertdata['ftotal_discount']) . "',
							`gst` = '" . $this->db->escape($insertdata['gst']) . "',
							`ltotal` = '" . $this->db->escape($insertdata['ltotal']) . "',
							`ltotal_discount` = '" . $this->db->escape($insertdata['ltotal_discount']) . "',
							`vat` = '" . $this->db->escape($insertdata['vat']) . "',
							`cess` = '" . $this->db->escape($insertdata['cess']) . "',
							`staxfood` = '" . $this->db->escape($insertdata['staxfood']) . "',
							`staxliq` = '" . $this->db->escape($insertdata['staxliq']) . "',
							`stax` = '" . $this->db->escape($insertdata['stax']) . "',
							`ftotalvalue` = '" . $this->db->escape($insertdata['ftotalvalue']) . "',
							`fdiscountper` = '" . $this->db->escape($insertdata['fdiscountper']) . "',
							`discount` = '" . $this->db->escape($insertdata['discount']) . "',
							`ldiscount` = '" . $this->db->escape($insertdata['ldiscount']) . "',
							`ldiscountper` = '" . $this->db->escape($insertdata['ldiscountper']) . "',
							`ltotalvalue` = '" . $this->db->escape($insertdata['ltotalvalue']) . "',
							`advance_billno` = '" . $this->db->escape($insertdata['advance_billno']) . "',
							`advance_amount` = '" . $this->db->escape($insertdata['advance_amount']) . "',
							`grand_total` = '" . $this->db->escape($insertdata['grand_total']) . "',
							`roundtotal` = '" . $this->db->escape($insertdata['roundtotal']) . "',
							`total_items` = '" . $this->db->escape($insertdata['total_items']) . "',
							`item_quantity` = '" . $this->db->escape($insertdata['item_quantity']) . "',
							`nc_kot_status` = '" . $this->db->escape($insertdata['nc_kot_status']) . "',
							`rate_id` = '" . $this->db->escape($insertdata['rate_id']) . "',
							`bill_date` = '" . $this->db->escape($insertdata['bill_date']) . "',
							`date_added` = '" . $this->db->escape($insertdata['date_added']) . "',
							`time_added` = '" . $this->db->escape($insertdata['time_added']) . "',
							`kot_no` = '" . $this->db->escape($insertdata['kot_no']) . "',
							`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
							`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'
							");	
				$oid = $this->db->getLastId();
				$this->db->query("UPDATE oc_order_items SET order_id = '".$oid."' WHERE order_id = '".$order_id_from."'");
				$this->db->query("DELETE FROM oc_order_info WHERE order_id = '".$order_id_from."'");
				$this->updateorderinfo($oid);
			}
			$json['done'] = '<script>parent.closeIFrame();</script>';
			$json['info'] = 1;
			$json['status'] = 0;
			$this->response->setOutput(json_encode($json));
		} else {
			$json['info'] = 0;
			$json['status'] = $this->error['status'];
			$json['message'] = 'From and To Table are Same';
			$this->response->setOutput(json_encode($json));
		}
		$this->log->write("---------------------------Table Chnage end-----------------------------");

	}

	public function updateorderinfo($orderid){
		//echo'inn';exit;
		$this->log->write("---------------------------Table Chnage update info Start------------------");
		$this->log->write('User Name : ' .$this->user->getUserName());
		$this->log->write('Order id : ' .$orderid);
		

		$this->load->model('catalog/order');
		$ftotal = 0;
		$ltotal = 0;
		$gst = 0;
		$vat = 0;
		$grand_total = 0;
		$ftotalvalue = 0;
		$ftotal_discount = 0;
		$ltotalvalue = 0;
		$ltotal_discount = 0;
		$gsttaxamt = 0;
		$staxfood = 0;
		$staxliq = 0;
		$vattaxamt = 0;
		$totalamtfood = 0;
		$totalamtliq = 0;

		$fromtotable = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '0' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		$fromtotableliquir = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id` = '".$orderid."' AND `is_liq` = '1' AND ismodifier = '1' AND cancelstatus = '0'")->rows;
		$order_fromtotable = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id` = '".$orderid."'")->row;
		$service_charge = $this->db->query("SELECT `service_charge` FROM oc_location WHERE `location_id` = '".$order_fromtotable['location_id']."'")->row['service_charge'];

		$tax_food1 = 0;
		foreach ($fromtotable as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
					$valtax2 = $taxes->row['tax2'];
				} else{
					$vattax = '0';
					$valtax2 = '0';
				}
				$taxfood1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxfood1->num_rows > 0){
					$tax_food1 = $taxfood1->row['tax_value'];
				} else{
					$tax_food1 = '0';
				}
				$taxfood2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$valtax2."'");
				if($taxfood2->num_rows > 0){
					$tax_food2 = $taxfood2->row['tax_value'];
				} else{
					$tax_food2 = '0';
				}
			}
			$ftotal = $ftotal + $value['amt'];
		}
		$servicechargefood = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$staxfood = 0;
		foreach ($fromtotable as $key => $value) {
			if($order_fromtotable['fdiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['fdiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);		
					}else{
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
		  		$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
		  		$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['fdiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}elseif($order_fromtotable['discount'] > 0){
				$discount_per = (($order_fromtotable['discount'])/$ftotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $afterdiscountamt*($servicechargefood/100);		
					}else{
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($afterdiscountamt + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
		  		$ftotalvalue = $ftotalvalue + $discount_value;
				$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}else{
				$staxfood_1 = 0;
				if($servicechargefood > 0){
					if ($service_charge == 1) {
						$staxfood_1 = $value['amt'] * ($servicechargefood/100);		
					}else{
						$staxfood_1 = 0;
					}
				}
				$staxfood = $staxfood + $staxfood_1;
				$tax1_value = ($value['amt'] + $staxfood_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxfood_1) * ($value['tax2'] / 100);
				$gsttaxamt = $gsttaxamt + $tax1_value + $tax2_value;
				$ftotal_discount = $ftotal_discount + $ftotal; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxfood_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_f = $staxfood;
		$gst = $gsttaxamt;
		
		$tax_liq1 = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($key == 0){
				$taxes = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$value['code']."' ");
				if($taxes->num_rows > 0){
					$vattax = $taxes->row['vat'];
				} else{
					$vattax = '0';
				}
				$taxliq1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$vattax."'");
				if($taxliq1->num_rows > 0){
					$tax_liq1 = $taxliq1->row['tax_value'];
				} else{
					$tax_liq1 = '0';
				}
			}
			$ltotal = $ltotal + $value['amt'];
		}

		$servicechargeliq = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		$staxliq = 0;
		foreach ($fromtotableliquir as $key => $value) {
			if($order_fromtotable['ldiscountper'] > 0){
				$discount_value = $value['amt']*($order_fromtotable['ldiscountper']/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$order_fromtotable['ldiscountper']."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} elseif($order_fromtotable['ldiscount'] > 0){
				$discount_per = ($order_fromtotable['ldiscount']/$ltotal)*100;
				$discount_value = $value['amt']*($discount_per/100);
				$afterdiscountamt = $value['amt'] - $discount_value;
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $afterdiscountamt * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($afterdiscountamt + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotalvalue = $ltotalvalue + $discount_value;
		  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '".$discount_per."',
				 							discount_value = '".$discount_value."',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			} else{
				$staxliq_1 = 0;
				if($servicechargeliq > 0){
					if ($service_charge == 1) {
						$staxliq_1 = $value['amt'] * ($servicechargeliq/100);		
					}else{
						$staxliq_1 = 0;
					}
				}
				$staxliq = $staxliq + $staxliq_1;
				$tax1_value = ($value['amt'] + $staxliq_1) * ($value['tax1'] / 100);
		  		$tax2_value = ($value['amt'] + $staxliq_1) * ($value['tax2'] / 100);
				$vattaxamt = $vattaxamt + $tax1_value + $tax2_value;
		  		$ltotal_discount = $ltotal_discount + $value['amt']; 
		  		$this->db->query("UPDATE oc_order_items
				 						SET discount_per = '0.00',
				 							discount_value = '0.00',
				 							tax1_value = '".$tax1_value."',
				 							tax2_value = '".$tax2_value."',
				 							stax = '" . $this->db->escape($staxliq_1) . "',
				 							login_id = '".$this->user->getId()."',
											login_name = '".$this->user->getUserName()."'
				 							WHERE id = '".$value['id']."' ");
			}
		}
		$stax_l = $staxliq;
		$vat = $vattaxamt;
		$stax = $stax_l + $stax_f;
		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
			$grand_total = ($ftotal + $ltotal + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		} else{
			$grand_total = ($ftotal + $ltotal + $gst + $vat + $stax_f + $stax_l) - ($ftotalvalue + $ltotalvalue);
		}
		$grand_totals = number_format($grand_total,2,'.','');
		$grand_total_explode = explode('.', $grand_total);
		$roundtotal = 0;
		if(isset($grand_total_explode[1])){
			if($grand_total_explode[1] >= 50){
				$roundtotals = 100 - $grand_total_explode[1];
				$roundtotal = ($roundtotals / 100); 
			} else {
				$roundtotal = -($grand_total_explode[1] / 100);
			}
		}
		$dtotalvalue = 0;
		if($order_fromtotable['dchargeper'] > 0){
			$dtotalvalue = $grand_total * ($order_fromtotable['dchargeper'] / 100);
		} elseif($order_fromtotable['dcharge'] > 0){
			$dtotalvalue = $order_fromtotable['dcharge'];
		}
		$totalitemandqty = $this->db->query("SELECT SUM(qty) as totalqty, COUNT(*) as totalitem FROM oc_order_items WHERE order_id = '".$orderid."' AND cancelstatus = '0' AND ismodifier = '1'")->row;
		//$totalvalue = ($order_fromtotable['ltotalvalue'] + $order_fromtotable['ftotalvalue'] + $order_fromtotable['discount'] + $order_fromtotable['ldiscount']);
		//$grand_total = $grand_total - $totalvalue;
		$this->db->query("UPDATE `oc_order_info` 
									SET `ftotal` = '".$ftotal."', 
									`ltotal` = '".$ltotal."', 
									`gst` = '".$gst."', 
									`vat` = '".$vat."', 
									`grand_total` = '".$grand_total."', 
									`dtotalvalue` = '".$dtotalvalue."', 
									`ftotalvalue` = '".$ftotalvalue."', 
									`ftotal_discount` = '".$ftotal_discount."', 
									`ltotalvalue` = '".$ltotalvalue."', 
									`ltotal_discount` = '".$ltotal_discount."', 
									`staxfood` = '".$stax_f."', 
									`staxliq` = '".$stax_l."', 
									`stax` = '".$stax."',
									`roundtotal` = '".$roundtotal."',
									`total_items` = '".$totalitemandqty['totalitem']."',
									`item_quantity` = '".$totalitemandqty['totalqty']."',
									`login_id` = '".$this->user->getId()."',
									`login_name` = '".$this->user->getUserName()."',
									`cancel_status` = '0'
									WHERE `order_id` = '".$orderid."'");

	   	$test = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '0'")->rows;
		$testmain = $this->db->query("SELECT * FROM oc_order_items WHERE ismodifier = '1'")->rows;
		foreach($testmain as $testmaindata){
			foreach($test as $testdata){
				if($testdata['parent_id'] == $testmaindata['id']){
					$testdata['qty'] = $testmaindata['qty'];
					$testdata['amt'] = $testdata['qty'] * $testdata['rate'];
					$this->db->query("UPDATE oc_order_items SET qty = '".$testdata['qty']."', amt = '".$testdata['amt']."' WHERE parent_id ='".$testmaindata['id']."' AND id='".$testdata['id']."'");
				}
			}
		}
		$this->log->write("---------------------------Table Chnage update info end------------------");

	}

	protected function getForm() {
		//$this->log->write("Table Change Load");

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['table_to'])) {
			$data['table_to'] = $this->error['table_to'];
		} else {
			$data['table_to'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_table_id'])) {
			$url .= '&filter_table_id=' . $this->request->get['filter_table_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$table_froms = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND cancel_status <> 1 ORDER BY `order_id` ")->rows;
		
		$data['tables_from'] = array();
		foreach ($table_froms as $dvalue) {
			$data['tables_from'][$dvalue['table_id']]= $dvalue['t_name'];
		}
		//$this->log->write(print_r($this->request->get, true));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/table', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_no'])) {
			$data['action'] = $this->url->link('catalog/tablechange/edit', 'token=' . $this->session->data['token'] .  $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/tablechange/edit', 'token=' . $this->session->data['token'] . '&order_no=' . $this->request->get['order_no'] . '&table_frm=' . $this->request->get['table_frm'] . '&table_frm1=' . $this->request->get['table_frm1'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/tablechange', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->post['order_no'])) {
			$data['order_no'] = $this->request->post['order_no'];
		} elseif (isset($this->request->get['order_no'])) {
			$data['order_no'] = $this->request->get['order_no'];
		} else {
			$data['order_no'] = '';
		}

		if (isset($this->request->post['table_frm'])) {
			$data['table_frm'] = $this->request->post['table_frm'];
		} elseif (isset($this->request->get['table_frm'])) {
			$data['table_frm'] = $this->request->get['table_frm'];
		} else {
			$data['table_frm'] = '';
		}

		if (isset($this->request->post['table_frm1'])) {
			$data['table_frm1'] = $this->request->post['table_frm1'];
		} if (isset($this->request->get['table_frm1'])) {
			$data['table_frm1'] = $this->request->get['table_frm1'];
		} else {
			$data['table_frm1'] = '';
		}

		if (isset($this->request->post['table_to'])) {
			$data['table_to'] = $this->request->post['table_to'];
		} else {
			$data['table_to'] = '';
		}

		if (isset($this->request->post['table_to1'])) {
			$data['table_to1'] = $this->request->post['table_to1'];
		} else {
			$data['table_to1'] = '';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/tablechange_form', $data));

		if (isset($this->request->get['table_frm'])) {
			$table_old = $this->request->get['table_frm'];
		} else {
			$table_old = '';
		}

		$this->load->model('catalog/order');
		if($this->model_catalog_order->get_settings('SKIPTABLE') == 0) {
			date_default_timezone_set("Asia/Kolkata");
			$sql = "INSERT INTO `oc_running_table` SET `table_id` = '".$table_old."', `date_added` = '".date('Y-m-d')."', `time_added` = '".date('H:i:s')."' ";
			$this->db->query($sql);
			$this->session->data['table_id'] = $table_old;
			// $data['status'] = 1;
		}	
		//$this->log->write("Table Change Load end");

		
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/table')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$running_sql = "SELECT `status` FROM `oc_running_table` WHERE `table_id` = '".$this->request->post['table_to']."' ";
		$running_datas = $this->db->query($running_sql);
		if($running_datas->num_rows > 0){
			$this->error['table_to'] = 'Table Is Allready Running!';
			$this->error['status'] = 3;
		}

		// $tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND `bill_date` = '" . $this->db->escape($last_open_date) . "' AND cancel_status <> '1' ORDER BY order_id DESC LIMIT 1 ");
		// if($tableinfo->num_rows > 0){
		// 	$this->error['table_frm'] = 'Table Is Allready Running!';
		// 	$this->error['status'] = 5;
		// }

		if (!isset($this->request->post['table_frm']) || $this->request->post['table_frm'] == '' || $this->request->post['table_frm'] == '0'){
			$this->error['table_frm'] = 'Please Select Table From';
			$this->error['status'] = 4;	
		}

		if ($this->request->post['table_frm'] == $this->request->post['table_to']) {
			$this->error['table_to'] = 'From and To Table are Same';
			$this->error['status'] = 1;
		} else {
			$table_to = $this->request->post['table_to'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$table_to);
			$sql = "SELECT `location_id` FROM " . DB_PREFIX . "location WHERE 1=1 ";
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		 	$is_exist_tables = $this->db->query($sql);
		 	if($is_exist_tables->num_rows > 0){
		 		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
				$last_open_dates = $this->db->query($last_open_date_sql);
				if($last_open_dates->num_rows > 0){
					$last_open_date = $last_open_dates->row['bill_date'];
				} else {
					$last_open_date = date('Y-m-d');
				}

				$order_info_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' AND payment_status = '0' AND `table_id` = '".$table_to."' ORDER BY `order_id` ");
			 	if($order_info_datas->num_rows > 0){
			 		//$this->error['status'] = 3;		
			 	}
		 	} else {
		 		$this->error['status'] = 2;	
		 	}

		 	$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
			$last_open_dates = $this->db->query($last_open_date_sql);
			if($last_open_dates->num_rows > 0){
				$last_open_date = $last_open_dates->row['bill_date'];
			} else {
				$last_open_date = date('Y-m-d');
			}
		 	$order_info_datas = $this->db->query("SELECT * FROM ".DB_PREFIX."order_info WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '1' AND payment_status = '0' AND `table_id` = '".$table_to."' ORDER BY `order_id` ");
		 	if($order_info_datas->num_rows > 0){
		 		$this->error['status'] = 5;		
		 	}
		}
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/table')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/table');

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/table');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/table')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/table');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_table->getTables($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'table_id' => $result['table_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}