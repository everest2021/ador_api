<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogMsrledgerReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/msr_ledger');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/msr_ledger');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/msr_ledger_report', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('m/d/Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		}
		else{
			$data['enddate'] = date('m/d/Y');
		}

		if(isset($this->request->post['creditcust'])){
			$data['creditcust'] = $this->request->post['creditcust'];
		}
		else{
			$data['creditcust'] = '';
		}

		if(isset($this->request->post['creditid'])){
			$data['creditid'] = $this->request->post['creditid'];
		}
		else{
			$data['creditid'] = '';
		}

		$data['billdatas'] = array();
		$billdata = array();
		$data['cancelamount'] = '';
		$creditdata = array();
		$credittotal = 0;
		$allcredits = array();
		$data['allcredits'] = array();
		//echo "<pre>";print_r($this->request->post);exit;
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate']) ){
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);

			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$this->load->model('catalog/msr_recharge');

			///$dates = $this->GetDays($start_date,$end_date);
			//echo "<pre>";print_r($dates);exit;

			$finaldata = array();
			$custdata = array();
 			//$custss = $this->db->query("SELECT * FROM oc_msr_recharge_transaction GROUP BY cust_id ")->rows;

 			$sql = "SELECT * FROM oc_msr_recharge_transaction WHERE 1=1 ";

 			if($data['creditcust'] != ''){
 				$sql .= "AND cust_name = '".$data['creditcust']."'";

 			}

 			$sql .= "GROUP BY cust_id ";
 			//echo $sql;exit;
			$custss = $this->db->query($sql)->rows;

			//echo "<pre>";print_r($custss);exit;
			$rechargedata = array();
			$sr_no = 1;
			foreach ($custss as $ckey => $cvalue) {
				
				$dates = $this->GetDays($start_date,$end_date);

				foreach ($dates as $key => $value) {
					
					$recharge_data = $this->db->query("SELECT sum(amount) as recharge , sum(extra_amt) as extra_amt FROM oc_msr_recharge_transaction WHERE `date` = '".$value."' AND cust_id = '".$cvalue['cust_id']."' AND `type`= '+' ")->row;
					$refund_data = $this->db->query("SELECT sum(amount) as refund FROM oc_msr_recharge_transaction WHERE `date` = '".$value."' AND cust_id = '".$cvalue['cust_id']."' AND `type` = '-' AND `trans_status` = '1'")->row;
					$redeem_data = $this->db->query("SELECT sum(amount) as redeem FROM oc_msr_recharge_transaction WHERE `date` = '".$value."' AND cust_id = '".$cvalue['cust_id']."' AND `type` = '-' AND `trans_status` = '2'")->row;
					
					$pre_bal = $this->model_catalog_msr_recharge->pre_bal($cvalue['msr_no'], $cvalue['cust_id']);
					
					$order_datass = $this->db->query("SELECT oi.`msrcard`,oit.`billno`,oi.`grand_total`,oi.`bill_date` FROM `oc_order_info` oi LEFT JOIN oc_order_items oit ON(oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date` = '".$value."' AND oi.`msr_cust_id` = '".$cvalue['cust_id']."'  GROUP BY oit.order_id ASC")->rows;
					//echo "<pre>";print_r($order_datass);];
					$rechargedata[$value]  = array('date' => $value,
											'reacharge' => $recharge_data['recharge'] +  $recharge_data['extra_amt'],
											'refund' => $refund_data['refund'],
											'redeem' => $redeem_data['redeem'],
											'sr_no' => $sr_no,
											'order_data' => $order_datass
										);
				}
				$sr_no++;
				$custdata[] = array('cust_name' =>$cvalue['cust_name'],
												'contact' =>$cvalue['cust_mob_no'],
												'msr_no' =>$cvalue['msr_no'],
												'rechargedata' => $rechargedata,		
												'pre_bal' => $pre_bal		
					 						);
			}
			//exit;

			//$finaldata  = array('custdata' => $custdata ,
			//'rechargedata' =>$rechargedata);

			//echo "<pre>";print_r($custdata);exit;
			

			$data['finaldata'] = $custdata;
				
		}
	
		$data['billdatas'] = $creditdata;

		// echo "<pre>";
		// print_r($creditdata);
		// exit();

		$data['action'] = $this->url->link('catalog/msr_ledger_report', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/msr_ledger_report', $data));
	}

	public function name(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * FROM oc_customerinfo WHERE name LIKE '%".$this->request->get['filter_name']."%' OR contact LIKE '%".$this->request->get['filter_name']."%' ORDER BY c_id DESC LIMIT 1")->rows;
			foreach ($results as $result) {
				$json[] = array(
					'c_id' => $result['c_id'],
					'contact'        => $result['contact'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),

				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}