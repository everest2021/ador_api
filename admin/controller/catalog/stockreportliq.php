<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStockReportLiq extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/stockreportliq');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['item_name'])){
			$data['item_name'] = $this->request->post['item_name'];
		}
		else{
			$data['item_name'] = '';
		}

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		}
		else{
			$data['startdate'] = date('Y-m-d');
		}

		if(isset($this->request->post['item_id'])){
			$data['item_id'] = $this->request->post['item_id'];
		}
		else{
			$data['item_id'] = '';
		}

		if(isset($this->request->post['unit_id'])){
			$data['unit_id'] = $this->request->post['unit_id'];
		}
		else{
			$data['unit_id'] = '';
		}

		if(isset($this->request->post['store'])){
			$data['store_id'] = $this->request->post['store'];
			$store_id = $this->request->post['store'];
		}
		else{
			$data['store_id'] = '';
			$store_id = '';
		}

		$this->load->model('catalog/purchaseentry');

		$data['final_data'] = '';
		// echo '<pre>';
		// print_r($data);
		// exit;
		$final_data = array();
		//echo $store_id;
		if(!empty($this->request->post['store'])){
			 $startdate = date('Y-m-d',strtotime($data['startdate']));
			$sql = "SELECT * FROM oc_brand WHERE 1=1";

			if(!empty($this->request->post['item_id']) && !empty($this->request->post['item_name'])){
				$sql .= " AND type_id = '".$this->request->post['item_id']."' AND brand = '".$this->request->post['item_name']."'";
			} 

			$brand_datas = $this->db->query($sql)->rows;

			foreach($brand_datas as $brand_data){
				$sql1 = "SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 0";

				if(!empty($this->request->post['unit_id'])){
					$sql1 .= " AND id = '".$this->request->post['unit_id']."'";
				}

				$brand_types = $this->db->query($sql1)->rows;

				foreach($brand_types as $brand_type){
					$avail_quantity = $this->model_catalog_purchaseentry->availableQuantity($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$opening_d = $this->model_catalog_purchaseentry->availableQuantity_report($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$total_purchase = $this->model_catalog_purchaseentry->total_purchase($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0,$startdate);
					$stock_tran_f = $this->model_catalog_purchaseentry->stock_transfer_f($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'', 0, $startdate);
					$stock_tran_t = $this->model_catalog_purchaseentry->stock_transfer_t($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$sale = $this->model_catalog_purchaseentry->stock_sale($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0,$startdate);
					$loose = $this->model_catalog_purchaseentry->loose_stock($brand_type['id'], $brand_data['brand_id'], 1,$store_id,'',0 ,$startdate);
					
					// echo'<pre>';
					// print_r($loose);
					// exit;
					if($avail_quantity != 0 || $opening_d != 0 || $total_purchase != 0 || $stock_tran_f != 0 || $stock_tran_t != 0 || $sale != 0 || $loose != 0){
						$final_data[] = array(
							'item_name'			=> $brand_data['brand'],
							'unit'				=> $brand_type['size'],
							'avail_quantity' 	=> $avail_quantity,
							'opening_bal' 	=> $opening_d,
							'total_purchase' 	=> $total_purchase,
							's_t_f' 	=> $stock_tran_f,
							's_t_t' 	=> $stock_tran_t,
							'sale' => $sale,
							'loose' => $loose,


						);
					}
				}
			
			}
		}
		$data['final_data'] = $final_data;
		$data['stores'] = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = 'Liquor'")->rows;
		// echo'<pre>';
		// print_r($final_data);
		// exit;
		$data['action'] = $this->url->link('catalog/stockreportliq', 'token=' . $this->session->data['token'] . $url, true);
		$data['heading_title'] = $this->language->get('heading_title');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stockreportliq', $data));
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	 public function autocomplete() {
	 	$json = array();
	 	if(isset($this->request->get['filter_name'])){
	 		$results = $this->db->query("SELECT type_id, brand FROM oc_brand WHERE brand LIKE '%".$this->request->get['filter_name']."%' LIMIT 0,10")->rows;
	 		foreach($results as $key){
	 			$json[] = array(
	 						'id'	=> $key['type_id'],
	 						'name'  => $key['brand']
	 					  );
	 		}
	 	}
	 	$this->response->setOutput(json_encode($json));
	}

	 public function getUnit() {
	 	$json = array();
	 	if(isset($this->request->get['filter_id'])){
			$results = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$this->request->get['filter_id']."' AND loose = 0")->rows;
			$jsons[] = array(
							'id' => '',
							'name' => 'Please Select'
						);
	 		foreach($results as $key){
	 			$jsons[] = array(
	 						'id'	=> $key['id'],
	 						'name'  => $key['size']
	 					  );
	 		}
	 	}
	 	$json['units'] = $jsons;
	 	$this->response->setOutput(json_encode($json));
	}
}