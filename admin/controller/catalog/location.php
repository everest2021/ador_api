<?php
class ControllerCatalogLocation extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/location');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/location');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/location');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			/*echo '<pre>';
		print_r($this->request->post);
		exit;*/

			$this->model_catalog_location->addLocation($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_location_id'])) {
				$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
			}


			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/location');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/location');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_location->editLocation($this->request->get['location_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_location_id'])) {
				$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/department');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/location');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $department_id) {
				$this->model_catalog_location->deleteLocation($department_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_location_id'])) {
				$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = null;
		}

		if (isset($this->request->get['filter_location_id'])) {
		 	$filter_location_id = $this->request->get['filter_location_id'];
		} else {
			$filter_location_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/location/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/location/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		$filter_data = array(
			'filter_location' => $filter_location,
			'filter_location_id' => $filter_location_id,
			
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$sport_total = $this->model_catalog_location->getTotalLocation();

		$results = $this->model_catalog_location->getLocations($filter_data);
		
		$rates = array(
			'1' => 'Rate 1',
			'2' => 'Rate 2',
			'3' => 'Rate 3',
			'4' => 'Rate 4',
			'5' => 'Rate 5',
			'6' => 'Rate 6',
		);



		foreach ($results as $result) {
			if(isset($rates[$result['rate_id']])){
				$rate = $rates[$result['rate_id']];
			} else {
				$rate = '';
			}
			$data['locations'][] = array(
				'location_id' => $result['location_id'],
				'location'        => $result['location'],
				'table_to' => $result['table_to'],
				'table_from' => $result['table_from'],
				'rate'        => $rate,
				'edit'        => $this->url->link('catalog/location/edit', 'token=' . $this->session->data['token'] . '&location_id=' . $result['location_id'] . $url, true)
			);
			
		}

		$data['token'] = $this->session->data['token'];

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}

		if (isset($this->request->get['filter_location_id'])) {
		 	$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		 }

		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        $data['sort_name'] = $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . '&sort=location' . $url, true);
        $data['sort_table_to'] = $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . '&sort=table_to' . $url, true);
        $data['sort_table_from'] = $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . '&sort=table_from' . $url, true);

		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sport_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/sport', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sport_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sport_total - $this->config->get('config_limit_admin'))) ? $sport_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sport_total, ceil($sport_total / $this->config->get('config_limit_admin')));

		$data['filter_location'] = $filter_location;
		$data['filter_location_id'] = $filter_location_id;
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/location_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['location_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['location'])) {
			$data['error_location'] = $this->error['location'];
		} else {
			$data['error_location'] = array();
		}

		if (isset($this->error['table_from'])) {
			$data['error_table_from'] = $this->error['table_from'];
		} else {
			$data['error_table_from'] = '';
		}

		if (isset($this->error['table_to'])) {
			$data['error_table_to'] = $this->error['table_to'];
		} else {
			$data['error_table_to'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}

		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['location_id'])) {
			$data['action'] = $this->url->link('catalog/location/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/location/edit', 'token=' . $this->session->data['token'] . '&location_id=' . $this->request->get['location_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/location', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['location_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$department_info = $this->model_catalog_location->getLocation($this->request->get['location_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['location_id'])) {
			$data['location_id'] = $this->request->get['location_id'];
		} elseif (!empty($department_info)) {
			$data['location_id'] = $department_info['location_id'];
		} else {
			$data['location_id'] = '';
		}

		if (isset($this->request->post['location'])) {
			$data['location'] = $this->request->post['location'];
		} elseif (!empty($department_info)) {
			$data['location'] = $department_info['location'];
		} else {
			$data['location'] = '';
		}

		if (isset($this->request->post['table_to'])) {
			$data['table_to'] = $this->request->post['table_to'];
		} elseif (!empty($department_info)) {
			$data['table_to'] = $department_info['table_to'];
		} else {
			$data['table_to'] = '';
		}

		if (isset($this->request->post['table_from'])) {
			$data['table_from'] = $this->request->post['table_from'];
		} elseif (!empty($department_info)) {
			$data['table_from'] = $department_info['table_from'];
		} else {
			$data['table_from'] = '';
		}

		if (isset($this->request->post['bill_copy'])) {
			$data['bill_copy'] = $this->request->post['bill_copy'];
		} elseif (!empty($department_info)) {
			$data['bill_copy'] = $department_info['bill_copy'];
		} else {
			$data['bill_copy'] = '';
		}

		if (isset($this->request->post['master_kot'])) {
			$data['master_kot'] = $this->request->post['master_kot'];
		} elseif (!empty($department_info)) {
			$data['master_kot'] = $department_info['master_kot'];
		} else {
			$data['master_kot'] = '';
		}


		if (isset($this->request->post['kot_copy'])) {
			$data['kot_copy'] = $this->request->post['kot_copy'];
		} elseif (!empty($department_info)) {
			$data['kot_copy'] = $department_info['kot_copy'];
		} else {
			$data['kot_copy'] = '';
		}

		if (isset($this->request->post['bot_copy'])) {
			$data['bot_copy'] = $this->request->post['bot_copy'];
		} elseif (!empty($department_info)) {
			$data['bot_copy'] = $department_info['bot_copy'];
		} else {
			$data['bot_copy'] = '0';
		}

		if (isset($this->request->post['parcel_detail'])) {
			$data['parcel_detail'] = $this->request->post['parcel_detail'];
		} elseif (!empty($department_info)) {
			$data['parcel_detail'] = $department_info['parcel_detail'];
		} else {
			$data['parcel_detail'] = '';
		}

		if (isset($this->request->post['dboy_detail'])) {
			$data['dboy_detail'] = $this->request->post['dboy_detail'];
		} elseif (!empty($department_info)) {
			$data['dboy_detail'] = $department_info['dboy_detail'];
		} else {
			$data['dboy_detail'] = '';
		}

		if (isset($this->request->post['direct_bill'])) {
			$data['direct_bill'] = $this->request->post['direct_bill'];
		} elseif (!empty($department_info)) {
			$data['direct_bill'] = $department_info['direct_bill'];
		} else {
			$data['direct_bill'] = '';
		}

		if (isset($this->request->post['is_app'])) {
			$data['is_app'] = $this->request->post['is_app'];
		} elseif (!empty($department_info)) {
			$data['is_app'] = $department_info['is_app'];
		} else {
			$data['is_app'] = '';
		}

		if (isset($this->request->post['is_advance'])) {
			$data['is_advance'] = $this->request->post['is_advance'];
		} elseif (!empty($department_info)) {
			$data['is_advance'] = $department_info['is_advance'];
		} else {
			$data['is_advance'] = '';
		}



		if (isset($this->request->post['a_to_z'])) {
			$data['a_to_z'] = $this->request->post['a_to_z'];
		} elseif (!empty($department_info)) {
			$data['a_to_z'] = $department_info['a_to_z'];
		} else {
			$data['a_to_z'] = '';
		}

		if (isset($this->request->post['service_charge'])) {
			$data['service_charge'] = $this->request->post['service_charge'];
		} elseif (!empty($department_info)) {
			$data['service_charge'] = $department_info['service_charge'];
		} else {
			$data['service_charge'] = '';
		}

		if (isset($this->request->post['kot_different'])) {
			$data['kot_different'] = $this->request->post['kot_different'];
		} elseif (!empty($department_info)) {
			$data['kot_different'] = $department_info['kot_different'];
		} else {
			$data['kot_different'] = '';
		}


		if (isset($this->request->post['bill_printer_name'])) {
			$data['bill_printer_name'] = $this->request->post['bill_printer_name'];
		} elseif (!empty($department_info)) {
			$data['bill_printer_name'] = $department_info['bill_printer_name'];
		} else {
			$data['bill_printer_name'] = '';
		}

		if (isset($this->request->post['bill_printer_type'])) {
			$data['bill_printer_type'] = $this->request->post['bill_printer_type'];
		} elseif (!empty($department_info)) {
			$data['bill_printer_type'] = $department_info['bill_printer_type'];
		} else {
			$data['bill_printer_type'] = '';
		}

		if (isset($this->request->post['store_name'])) {
			$data['store_name'] = $this->request->post['store_name'];
		} elseif (!empty($department_info)) {
			$data['store_name'] = $department_info['store_name'];
		} else {
			$data['store_name'] = '';
		}

		$data['bill_printer_types'] = array(
			'Network' => 'Network',
			'Windows' => 'Windows'
		);

		$data['rates'] = array(
			'1' => 'Rate 1',
			'2' => 'Rate 2',
			'3' => 'Rate 3',
			'4' => 'Rate 4',
			'5' => 'Rate 5',
			'6' => 'Rate 6',
		);

		$data['stores'] = $this->db->query(" SELECT * FROM oc_store_name ")->rows;
		//echo "<pre>";print_r($data['stores']);exit;

		if (isset($this->request->post['rate_id'])) {
			$data['rate_id'] = $this->request->post['rate_id'];
		} elseif (!empty($department_info)) {
			$data['rate_id'] = $department_info['rate_id'];
		} else {
			$data['rate_id'] = '';
		}

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/location_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/location')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$datas = $this->request->post;
		if ((utf8_strlen($this->request->post['location']) < 2) || (utf8_strlen($this->request->post['location']) > 255)) {
			$this->error['location'] = 'Please Enter Location Name';
		} else {
			if(isset($this->request->get['location_id'])){
				$is_exist = $this->db->query("SELECT `location_id` FROM `oc_location` WHERE `location` = '".$datas['location']."' AND `location_id` <> '".$this->request->get['location_id']."' ");
				if($is_exist->num_rows > 0){
					$this->error['location'] = 'Location Name Already Exists';
				}
			} else {
				$is_exist = $this->db->query("SELECT `location_id` FROM `oc_location` WHERE `location` = '".$datas['location']."' ");
				if($is_exist->num_rows > 0){
					$this->error['location'] = 'Location Name Already Exists';
				}
			}
		}

		if(!isset($this->request->get['location_id'])){
			$is_exist = $this->db->query("SELECT * FROM oc_location WHERE ".$datas['table_from']." BETWEEN `table_from` AND `table_to` ");
			if($is_exist->num_rows > 0){
				$this->error['table_from'] = 'Table Number Range Already Present';
			}

			$is_exist = $this->db->query("SELECT * FROM oc_location WHERE ".$datas['table_to']." BETWEEN `table_from` AND `table_to` ");
			if($is_exist->num_rows > 0){
				$this->error['table_to'] = 'Table Number Range Already Present';
			}
		} else {
			$is_exist = $this->db->query("SELECT * FROM oc_location WHERE ".$datas['table_from']." BETWEEN `table_from` AND `table_to` AND `location_id` <> '".$this->request->get['location_id']."' ");
			if($is_exist->num_rows > 0){
				$this->error['table_from'] = 'Table Number Range Already Present';
			}

			$is_exist = $this->db->query("SELECT * FROM oc_location WHERE ".$datas['table_to']." BETWEEN `table_from` AND `table_to` AND `location_id` <> '".$this->request->get['location_id']."' ");
			if($is_exist->num_rows > 0){
				$this->error['table_to'] = 'Table Number Range Already Present';
			}
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/location')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/location');

		return !$this->error;
	}

	public function upload() {
		$this->load->language('catalog/location');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'catalog/location')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);

			$json['filename'] = $file;
			$json['mask'] = $filename;

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// public function autocomplete() {
	// 	$json = array();

	// 	if (isset($this->request->get['filter_location'])) {
	// 		$this->load->model('catalog/location');

	// 		$data = array(
	// 			'filter_location' => $this->request->get['filter_location'],
	// 			'start'       => 0,
	// 			'limit'       => 20
	// 		);

	// 		$results = $this->model_catalog_location->getLocations($data);

	// 		foreach ($results as $result) {
	// 			$json[] = array(
	// 				'location_id' => $result['location_id'],
	// 				'location'        => strip_tags(html_entity_decode($result['location'], ENT_QUOTES, 'UTF-8'))
	// 			);
	// 		}
	// 	}

	// 	$sort_order = array();

	// 	foreach ($json as $key => $value) {
	// 		$sort_order[$key] = $value['location'];
	// 	}

	// 	array_multisort($sort_order, SORT_ASC, $json);
	// 	$this->response->setOutput(json_encode($json));
	// }

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_location'])) {
			$this->load->model('catalog/location');
			$data = array(
				'filter_location' => $this->request->get['filter_location'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_location->getLocations($data);
			// echo "<pre>";
			// print_r($results);
			// exit();
			foreach ($results as $result) {
				$json[] = array(
					'location_id' => $result['location_id'],
					'location'    => strip_tags(html_entity_decode($result['location'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['location'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}