<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogStockTransfer extends Controller {
	private $error = array();
	
	public function index() {

		$this->load->language('catalog/stocktransfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stocktransfer');

		$this->getForm();
	}

	public function add() {
		$this->load->language('catalog/stocktransfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stocktransfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$id = $this->model_catalog_stocktransfer->addpurchaseentry($this->request->post);
			$this->prints($id);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['year'])) {
				$url .= '&year=' . $this->request->get['year'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//$url .= '&id='.$id;.'&submit_fun=1'
			$this->response->redirect($this->url->link('catalog/stocktransfer/getForm', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/stocktransfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stocktransfer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$id = $this->model_catalog_stocktransfer->editpurchaseentry($this->request->post, $this->request->get['id']);
			$this->prints($id);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['year'])) {
				$url .= '&year=' . $this->request->get['year'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$url .= '&id='.$id;

			$this->response->redirect($this->url->link('catalog/stocktransfer/getForm', 'token=' . $this->session->data['token'] . $url.'&submit_fun=1', true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/stocktransfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stocktransfer');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $po_numbers) {
				$po_numbers_exp = explode('/', $po_numbers);
				
				$po_number = $po_numbers_exp[0];
				$year = $po_numbers_exp[1];
				$pre_po = $po_numbers_exp[2];
				$this->model_catalog_stocktransfer->deletepurchaseentry($po_number, $year , $pre_po);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_number'])) {
				$url .= '&filter_number=' . $this->request->get['filter_number'];
			}

			if (isset($this->request->get['filter_year'])) {
				$url .= '&filter_year=' . $this->request->get['filter_year'];
			}

			if (isset($this->request->get['filter_mname'])) {
				$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));;
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stocktransfer', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function prints($id){
		$this->load->model('catalog/order');
		$transfer = $this->db->query("SELECT * FROM oc_stocktransfer WHERE id = '".$id."'")->row;
		$transfer_items = $this->db->query("SELECT *, SUM(qty) as qty FROM oc_stocktransfer_items WHERE p_id = '".$id."' GROUP BY unit_id, description_id")->rows;
		foreach($transfer_items as $transfer_item){
			$transfer_data[] = array(
				'name'			=> $transfer_item['description'],
				'qty'         	=> $transfer_item['qty'],
				'rate'          => $transfer_item['rate'],
				'amt'         	=> $transfer_item['amount']
			);
		}
		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->text("Stock Transfer");
		    $printer->feed(1);
		    $printer->text("Issue Slip");
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->feed(1);
		   	$printer->text("Store From :".$transfer['from_store_name']);
		   	$printer->feed(1);
		   	$printer->text("Store To :".$transfer['to_store_name']);
		   	$printer->feed(1);
		   	$printer->text("Slip No :".$transfer['invoice_no']);
		   	$printer->feed(1);
		   	$printer->text("Date:".$transfer['invoice_date']);
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("Description",16)."".str_pad("Qty", 10)."".str_pad("Rate", 10)."Amount");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $total_items_normal = 0;
			$total_quantity_normal = 0;
		    foreach($transfer_data as $nkey => $nvalue){
	    	  	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 16);
				$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 5);
		    	$printer->text("".str_pad($nvalue['name'],16)."".str_pad($nvalue['qty'],10)."".str_pad($nvalue['rate'],10)."".$nvalue['amt']);
		    	$printer->feed(1);
		    	$total_items_normal ++ ;
		    	$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	    	}
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(2);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getForm();
	}

	public function getForm() {
		$this->load->language('catalog/stocktransfer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stocktransfer');
		$this->load->model('catalog/purchaseentry');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');

		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['po_datas'])) {
			$data['error_po_datas'] = $this->error['po_datas'];
		} else {
			$data['error_po_datas'] = '';
		}

		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
			if (isset($this->session->data['warning'])) {
				$data['error_warning'] = $this->session->data['warning'];
				unset($this->session->data['warning']);
			}
		} else {
			$data['success'] = '';
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_number'])) {
			$url .= '&filter_number=' . $this->request->get['filter_number'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_mname'])) {
			$url .= '&filter_mname=' . urlencode(html_entity_decode($this->request->get['filter_mname'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('catalog/stocktransfer', 'token=' . $this->session->data['token'] . $url, true);

		$purchaseentry_info = array();
		if((isset($this->request->get['filter_invoice_no'])) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$purchaseentry_info = $this->model_catalog_stocktransfer->getPurchaseentryby_supp_invoice($this->request->get['filter_invoice_no']);
		} elseif (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$purchaseentry_info = $this->model_catalog_stocktransfer->getPurchaseentry($this->request->get['id']);
		}

		// echo '<pre>';
		// print_r($purchaseentry_info);
		// exit;

		if(isset($purchaseentry_info[0]['id'])){
			$data['edit'] = 0;
		} else {
			$data['edit'] = 1;
		}

		$data['redirect_url'] =  $this->url->link('catalog/stocktransfer/add', 'token=' . $this->session->data['token'], true);
		if (!isset($this->request->get['id']) && !isset($purchaseentry_info[0]['id'])) {
			$data['action'] = $this->url->link('catalog/stocktransfer/add', 'token=' . $this->session->data['token'] . $url, true);
			$data['action1'] = $this->url->link('catalog/stocktransfer/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			if(isset($this->request->get['id'])){
				$data['action'] = $this->url->link('catalog/stocktransfer/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
				$data['action1'] = $this->url->link('catalog/stocktransfer/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
			} else {
				if(isset($purchaseentry_info[0]['id'])){
					$id = $purchaseentry_info[0]['id'];
					$data['action'] = $this->url->link('catalog/stocktransfer/edit', 'token=' . $this->session->data['token'] . '&id=' . $id . $url, true);
					$data['action1'] = $this->url->link('catalog/stocktransfer/edit', 'token=' . $this->session->data['token'] . '&id=' . $id . $url, true);
				} else {
					$data['action'] = $this->url->link('catalog/stocktransfer/add', 'token=' . $this->session->data['token'] . $url, true);
					$data['action1'] = $this->url->link('catalog/stocktransfer/add', 'token=' . $this->session->data['token'] . $url, true);	
				}
			}
		}
		//exit;

		$data['token'] = $this->session->data['token'];
		
		if (isset($this->request->post['id'])) {
			$data['id'] = $this->request->post['id'];
		} elseif (isset($purchaseentry_info[0]['id'])) {
			$data['id'] = $purchaseentry_info[0]['id'];
		} else {
			$data['id'] = '0';
		}	

		if (isset($this->request->post['filter_invoice_no'])) {
			$data['filter_invoice_no'] = $this->request->post['filter_invoice_no'];
		}  elseif(isset($this->request->get['filter_invoice_no'])){
			$data['filter_invoice_no'] = $this->request->get['filter_invoice_no'];
		} else {
			$data['filter_invoice_no'] = '';
		}

		if (isset($this->request->post['invoice_date'])) {
			$data['invoice_date'] = $this->request->post['invoice_date'];
		} elseif (isset($purchaseentry_info[0]['invoice_date'])) {
			if($purchaseentry_info[0]['invoice_date'] != '0000-00-00' && $purchaseentry_info[0]['invoice_date'] != '1970-01-01'){
				$data['invoice_date'] = date('d-m-Y', strtotime($purchaseentry_info[0]['invoice_date']));
			} else {
				$data['invoice_date'] = '';
			}
		} else {
			$data['invoice_date'] = date('d-m-Y');
		}

		if (isset($this->request->post['invoice_no'])) {
			$data['invoice_no'] = $this->request->post['invoice_no'];
		} elseif (isset($purchaseentry_info[0]['invoice_no'])) {
			$data['invoice_no'] = $purchaseentry_info[0]['invoice_no'];
		} else {
			$invoice_no = $this->db->query("SELECT invoice_no FROM oc_stocktransfer ORDER BY invoice_no DESC LIMIT 1");
			if($invoice_no->num_rows > 0){
				$data['invoice_no'] = $invoice_no->row['invoice_no'] + 1;
			} else{
				$data['invoice_no'] = 1;
			}
		}

		$stores_datas = $this->db->query("SELECT `id`, `store_name` FROM `oc_store_name` WHERE store_type = 'Food' ORDER BY `store_name` ")->rows;
		$data['stores'] = array();
		foreach($stores_datas as $skey => $svalue){
			$data['stores'][$svalue['id']] = $svalue['store_name'];
		}

		if (isset($this->request->post['from_store_id'])) {
			$data['from_store_name'] = $this->request->post['from_store_name'];
			$data['from_store_id'] = $this->request->post['from_store_id'];
		} elseif (isset($purchaseentry_info[0]['from_store_name'])) {
			$data['from_store_name'] = $purchaseentry_info[0]['from_store_name'];
			$data['from_store_id'] = $purchaseentry_info[0]['from_store_id'];
		} else {
			$data['from_store_name'] = '';
			$data['from_store_id'] = '0';
		}

		if (isset($this->request->post['to_store_id'])) {
			$data['to_store_name'] = $this->request->post['to_store_name'];
			$data['to_store_id'] = $this->request->post['to_store_id'];
		} elseif (isset($purchaseentry_info[0]['to_store_name'])) {
			$data['to_store_name'] = $purchaseentry_info[0]['to_store_name'];
			$data['to_store_id'] = $purchaseentry_info[0]['to_store_id'];
		} else {
			$data['to_store_name'] = '';
			$data['to_store_id'] = '0';
		}

		if (isset($this->request->post['narration'])) {
			$data['narration'] = $this->request->post['narration'];
		} elseif (isset($purchaseentry_info[0]['narration'])) {
			$data['narration'] = $purchaseentry_info[0]['narration'];
		} else {
			$data['narration'] = '';
		}

		if (isset($this->request->post['category'])) {
			$data['category'] = $this->request->post['category'];
		} elseif (isset($purchaseentry_info[0]['category'])) {
			$data['category'] = $purchaseentry_info[0]['category'];
		} else {
			$data['category'] = '';
		}

		if (isset($this->request->post['total_qty'])) {
			$data['total_qty'] = $this->request->post['total_qty'];
		} elseif (isset($purchaseentry_info[0]['total_qty'])) {
			$data['total_qty'] = $purchaseentry_info[0]['total_qty'];
		} else {
			$data['total_qty'] = '';
		}

		if (isset($this->request->post['total_amt'])) {
			$data['total_amt'] = $this->request->post['total_amt'];
		} elseif (isset($purchaseentry_info[0]['total_amt'])) {
			$data['total_amt'] = $purchaseentry_info[0]['total_amt'];
		} else {
			$data['total_amt'] = '';
		}

		if (isset($this->request->post['total_pay'])) {
			$data['total_pay'] = $this->request->post['total_pay'];
		} elseif (isset($purchaseentry_info[0]['total_pay'])) {
			$data['total_pay'] = $purchaseentry_info[0]['total_pay'];
		} else {
			$data['total_pay'] = '';
		}

		$units = array();
		if (isset($this->request->post['po_datas'])) {
			$data['po_datas'] = $this->request->post['po_datas'];
		} elseif(isset($purchaseentry_info[0]['id'])){
			$total = 0;
			$purchaseentry_item_info = $this->db->query("SELECT * FROM `oc_stocktransfer_items` WHERE `p_id` = '".$purchaseentry_info[0]['id']."' ")->rows;
			foreach ($purchaseentry_item_info as $tkey => $tvalue) {
				$data['po_datas'][$tkey]['id'] = $tvalue['id'];
				$data['po_datas'][$tkey]['p_id'] = $tvalue['p_id'];
				$data['po_datas'][$tkey]['description'] = $tvalue['description'];
				$data['po_datas'][$tkey]['description_id'] = $tvalue['description_id'];
				$data['po_datas'][$tkey]['description_code'] = $tvalue['description_code'];
				$data['po_datas'][$tkey]['description_barcode'] = $tvalue['description_barcode'];
				$data['po_datas'][$tkey]['description_code_search'] = $tvalue['description_code_search'];
				$data['po_datas'][$tkey]['qty'] = $tvalue['qty'];

				$stock_type = $this->db->query("SELECT item_type FROM oc_stock_item WHERE item_code = '".$tvalue['description_code']."'")->row;
				$avq = $this->model_catalog_purchaseentry->availableQuantity($tvalue['unit_id'],$tvalue['description_id'],'0', $purchaseentry_info[0]['from_store_id'], $stock_type['item_type'], $tvalue['description_code']);

				$data['po_datas'][$tkey]['avq'] = $avq;
				$data['po_datas'][$tkey]['type_id'] = $tvalue['type'];

				$unitss = $this->db->query("SELECT * FROM `oc_unit`")->rows;
				$units = array();
				foreach($unitss as $ukey => $uvalue){
					$units[$uvalue['unit_id']] = $uvalue['unit'];
				}

				$data['po_datas'][$tkey]['units'] = $units;
				
				$data['po_datas'][$tkey]['unit_id'] = $tvalue['unit_id'];
				$data['po_datas'][$tkey]['unit'] = $tvalue['unit'];
				$data['po_datas'][$tkey]['rate'] = $tvalue['rate'];
				$data['po_datas'][$tkey]['amount'] = $tvalue['amount'];
			}
		} else {	
			$data['po_datas'] = array();
		}
		$data['units'] = $units;
		$unitfood = $this->db->query("SELECT * FROM oc_unit")->rows;
		$data['units1'] = json_encode($unitfood);

		$taxess = $this->db->query("SELECT * FROM `oc_tax` ")->rows;
		$taxes = array();
		foreach($taxess as $ukey => $uvalue){
			$taxes[$uvalue['tax_value']] = $uvalue['tax_name'];
		}
		$data['taxes'] = $taxes;

		if(isset($this->request->get['submit_fun'])){
			$data['submit_fun'] = '1';
		} else {
			$data['submit_fun'] = '0';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stocktransfer', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/stocktransfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach($this->request->post['po_datas'] as $pkeys => $pvalues){
			if($pvalues['qty'] == '0'){
				$this->error['po_datas'][$pkeys]['qty'] = 'Please Enter the Quantity';			
			}		
		}

		if(empty($this->request->post['invoice_no'])){
			$this->error['warning'] = "Enter invoice number";
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/stocktransfer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function autocomplete_description(){
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filter_name' => $this->request->get['filter_name']
			);
			$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1 ";
			if($filter_data['filter_name'] != ''){
				$sql .= " AND (`item_name` LIKE '%".$this->db->escape($filter_data['filter_name'])."%') ";
			}
			$sql .= " AND item_category = 'Food'";
			//$this->log->write($sql);
			$results = $this->db->query($sql)->rows;
			foreach ($results as $result) {
				if($result['uom'] != 'Please Select'){
					$units[$result['uom']]['unit_id'] = $result['uom'];
					$units[$result['uom']]['unit_name'] = $result['unit_name'];
				} else{
					$sql = "SELECT * FROM `oc_unit` WHERE 1=1 ";
					$unitss = $this->db->query($sql)->rows;
					$units = array();
					foreach($unitss as $ukey => $uvalue){
						$units[$uvalue['unit_id']]['unit_id'] = $uvalue['unit_id'];
						$units[$uvalue['unit_id']]['unit_name'] = $uvalue['unit'];
					}
				}
				$json[] = array(
					'id' 			=> $result['id'],
					'item_code' 	=> $result['item_code'],
					'stock_type'	=> $result['item_type'],
					'bar_code' 		=> $result['bar_code'],
					'tax' 			=> $result['tax'],
					'tax_value' 	=> $result['tax_value'],
					'item_category' => $result['item_category'],
					'uom' 			=> $result['uom'],
					'unit_name' 	=> $result['unit_name'],
					'units' 		=> $units,
					'item_name'     => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
					'rate'          => $result['purchase_rate']
				);
			}
			$sort_order = array();
			foreach ($json as $key => $value) {
				$sort_order[$key] = $value['item_name'];
			}
			array_multisort($sort_order, SORT_ASC, $json);
		}
		if (isset($this->request->get['filter_name_1'])) {
			$filter_data = array(
				'filter_name_1' => $this->request->get['filter_name_1'],
				'start'       => 0,
				'limit'       => 5
			);
			if($filter_data['filter_name_1'] != ''){
				$sql = "SELECT * FROM `oc_stock_item` WHERE 1=1 ";
				
					$sql .= " AND (`item_code` = '".$this->db->escape($filter_data['filter_name_1'])."' OR `bar_code` = '".$this->db->escape($filter_data['filter_name_1'])."') ";
				
					$sql .= " AND item_category = 'Food'";
				//$this->log->write($sql);
				$result = $this->db->query($sql)->row;
			}
				if(isset($result['id'])){
					if($result['uom'] != 'Please Select'){
						$units[$result['uom']]['unit_id'] = $result['uom'];
						$units[$result['uom']]['unit_name'] = $result['unit_name'];
					} else{
						$sql = "SELECT * FROM `oc_unit` WHERE 1=1 ";
						$unitss = $this->db->query($sql)->rows;
						$units = array();
						foreach($unitss as $ukey => $uvalue){
							$units[$uvalue['unit_id']]['unit_id'] = $uvalue['unit_id'];
							$units[$uvalue['unit_id']]['unit_name'] = $uvalue['unit'];
						}
					}
					$json = array(
						'status' 		=> 1,
						'id' 			=> $result['id'],
						'item_code' 	=> $result['item_code'],
						'stock_type'	=> $result['item_type'],
						'bar_code' 		=> $result['bar_code'],
						'tax' 			=> $result['tax'],
						'tax_value' 	=> $result['tax_value'],
						'item_category' => $result['item_category'],
						'uom' 			=> $result['uom'],
						'units' 		=> $units,
						'unit_name' 	=> $result['unit_name'],
						'item_name'     => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8')),
						'rate'          => $result['purchase_rate']
					);
				} else {
					$json = array(
						'status' => 0
					);
				}
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		
	}

	public function getAvailableQuantity(){
		$this->load->model('catalog/purchaseentry');
		$json = array();
		if (isset($this->request->get['unit_id']) && isset($this->request->get['description_id']) && isset($this->request->get['description_code']) && isset($this->request->get['store_id']) && isset($this->request->get['stock_type'])) {
			$avq = $this->model_catalog_purchaseentry->availableQuantity($this->request->get['unit_id'],$this->request->get['description_id'],'0', $this->request->get['store_id'], $this->request->get['stock_type'], $this->request->get['description_code']);
			$json = array(
				'avq' 			=> $avq
			);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// public function storetype(){
	// 	$json = array();
	// 	if (!empty($this->request->get['store_id'])) {
	// 		$storetype = $this->db->query("SELECT * FROM oc_store_name WHERE id = '".$this->request->get['store_id']."'")->row;
	// 		$tostores = $this->db->query("SELECT * FROM oc_store_name WHERE store_type = '".$storetype['store_type']."'")->rows;
	// 		$jsons[] = array(
	// 						'id' => '',
	// 						'store' => 'Please Select'
	// 					);
	// 		foreach ($tostores as $result) {
	// 			$jsons[] = array(
	// 				'id' => $result['id'],
	// 				'store' => $result['store_name']
	// 			);
	// 		}
	// 		$json1 = $storetype['store_type'];
	// 	} else{
	// 		$stores = $this->db->query("SELECT * FROM oc_store_name")->rows;
	// 		$jsons[] = array(
	// 						'id' => '',
	// 						'store' => 'Please Select'
	// 					);
	// 		foreach ($stores as $result) {
	// 			$jsons[] = array(
	// 				'id' => $result['id'],
	// 				'store' => $result['store_name']
	// 			);
	// 		}
	// 		$json1 = array();
	// 	}
	// 	$json['to_stores'] = $jsons;
	// 	$json['category'] = $json1;
	// 	$this->response->addHeader('Content-Type: application/json');
	// 	$this->response->setOutput(json_encode($json));
	// }
}