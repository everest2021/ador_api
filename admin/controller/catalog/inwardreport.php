<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
class ControllerCatalogInwardreport extends Controller {
	private $error = array();
	// echo'<pre>';
	// print_r($controller);exit;

	public function index() {
		$this->load->language('catalog/inwardreport');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/inwardreport');
		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/inwardreport', 'token=' . $this->session->data['token'] . $url, true)
		);

		if(isset($this->request->post['filter_startdate'])){
			$data['startdate'] = $this->request->post['filter_startdate'];
		} else {
			$data['startdate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_enddate'])){
			$data['enddate'] = $this->request->post['filter_enddate'];
		} else {
			$data['enddate'] = date('d-m-Y');
		}

		if(isset($this->request->post['filter_storename'])){
			$data['store_name'] = $this->request->post['filter_storename'];
			$data['store_id'] = $this->request->post['filter_storeid'];
		} else {
			$stores = $this->db->query("SELECT * FROM oc_outlet WHERE outlet_id = '".$this->user->getUserStore()."'")->row;
			$data['store_name'] = $stores['outlet_name'];
			$data['store_id'] = $stores['outlet_id'];
		}	

		$data['final_data'] =array();
		$final_data = array();
		if(isset($this->request->post['filter_startdate']) && isset($this->request->post['filter_enddate'])){
			//echo'aaa';exit;
			$startdate = strtotime($this->request->post['filter_startdate']);
			$enddate =  strtotime($this->request->post['filter_enddate']);
			$start_date = date('Y-m-d', $startdate);
			$end_date = date('Y-m-d', $enddate);
			$storecode = $this->request->post['filter_storeid'];
				// echo'<pre>';
				// print_r($this->request->post);
				// exit;

			$sql = "SELECT * FROM `oc_outward_order` WHERE `ow_date` >= '".$start_date."' AND `ow_date` <= '".$end_date."' ";	
			if($storecode != ''){
				$sql .= " AND outlet_id = '".$storecode."'";
			}
			$sql .=" GROUP BY `outlet_id`";
			//echo $sql;exit;
			$store_data = $this->db->query($sql)->rows;
			foreach ($store_data as $skey => $svalue) {
				$item_data = array();
				$i =1;
				$out_name =$this->db->query("SELECT `outlet_name` FROM `oc_outlet` WHERE `outlet_id` = '".$svalue['outlet_id']."' ")->row;
				$all_data =$this->db->query("SELECT * FROM `oc_outward_order` ow LEFT JOIN `oc_outward_order_items` owi ON(ow.`id` = owi.`ow_id`) WHERE ow.`outlet_id` = '".$svalue['outlet_id']."' ")->rows;			
				foreach ($all_data as $akey => $avalue) {
					$item_data[] =array(
						'i' =>$i,
						'po_number' => $avalue['ow_prefix'].'-'.$avalue['ow_number'],
						'item_name' =>$avalue['item_name'],
						'quantity' =>$avalue['quantity'],
						'notes' =>$avalue['notes'],
					);
					$i ++;
				}
				$final_data[] = array(
					'outlet_name' =>$out_name['outlet_name'],
					'item_data' => $item_data,
				);
			}

		}
		$data['final_data'] = $final_data;

		$sql = "SELECT * FROM `oc_outlet` WHERE 1=1 ";
		if($this->user->getuserstore() != 0){
			$sql .="AND outlet_id = '".$this->user->getuserstore()."'";
		}
		$outlet_datas = $this->db->query($sql)->rows;
		if($this->user->getuserstore() == 0){
		$outlet_data[''] = 'All';
		}
		foreach ($outlet_datas as $dkey => $dvalue) {

			$outlet_data[$dvalue['outlet_id']] = $dvalue['outlet_name'];
		}
		$data['outlet_data'] = $outlet_data;
		
				// echo'<pre>';
				// print_r($data['final_data']);
				// exit;

		$data['token'] = $this->session->data['token'];
		$data['action'] = $this->url->link('catalog/inwardreport', 'token=' . $this->session->data['token'] . $url, true);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/inwardreport', $data));
	}
}