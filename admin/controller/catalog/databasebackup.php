<?php

class ControllerCatalogDatabasebackup extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/databasebackup');

		$this->document->setTitle($this->language->get('heading_title'));

		

		$this->getList();
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}


	protected function getList() {
		//echo $this->request->get['export'];exit;
		if (isset($this->request->get['export'])) {
			$export = $this->request->get['export'];
		} else {
			$export = '';
		}

		$url = '';

		

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/advance/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/advance/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		//echo $export;exit;
		//ENTER THE RELEVANT INFO BELOW
		// if ($this->request->get['export'] == 'export') {
		// 	$name ='db_ador2';
		// 	$user ='root';
		// 	$pass ='';
		// 	$host ='localhost';
		// 	//Export the database and output the status to the page
		// 	$mysqli = new mysqli($host,$user,$pass,$name); 
	 //        $mysqli->select_db($name); 
	 //        $mysqli->query("SET NAMES 'utf8'");
	 //        $queryTables = $mysqli->query('SHOW TABLES'); 
	 //        while($row = $queryTables->fetch_row()) { 
	 //        	$target_tables_all[] = $row[0]; 
	 //        }
	 //        //echo "<pre>";print_r($target_tables_all);exit;
	        
	 //        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
	 //        //$backup_name = DIR_DOWNLOAD.'db_ador_bk.sql';
	 //        //file_put_contents($backup_name);
	 //        //echo 'Done';exit;

	 //        foreach($target_tables_all as $table) {
	 //            $result         =   $mysqli->query('SELECT * FROM '.$table);  
	 //            $fields_amount  =   $result->field_count;  
	 //            $rows_num=$mysqli->affected_rows;     
	 //            $res            =   $mysqli->query('SHOW CREATE TABLE '.$table); 
	 //            $TableMLine     =   $res->fetch_row();
	 //            $content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine[1].";\n\n";

	 //            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
	 //                while($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
	 //                    if ($st_counter%100 == 0 || $st_counter == 0 )  {
	 //                            $content .= "\nINSERT INTO ".$table." VALUES";
	 //                    }
	 //                    $content .= "\n(";
	 //                    for($j=0; $j<$fields_amount; $j++)  { 
	 //                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
	 //                        if (isset($row[$j])){
	 //                            $content .= '"'.$row[$j].'"' ; 
	 //                        } else {   
	 //                            $content .= '""';
	 //                        }     
	 //                        if ($j<($fields_amount-1)) {
	 //                            $content.= ',';
	 //                        }      
	 //                    }
	 //                    $content .=")";
	 //                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
	 //                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {   
	 //                        $content .= ";";
	 //                    } else {
	 //                        $content .= ",";
	 //                    } 
	 //                    $st_counter=$st_counter+1;
	 //                }
	 //            } $content .="\n\n\n";
	 //        }
	 //        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
	 //        $backup_name = DIR_DOWNLOAD.'db_ador_bk.sql';
	 //        //echo $backup_name;exit;
	 //        file_put_contents($backup_name, $content);

	 //        $filename = "db_ador.sql";
		// 	$file_path = $backup_name;
		// 	$html = file_get_contents($file_path);
		// 	Header('Content-type: application/octet-stream');
		// 	header('Content-Disposition: attachment; filename='.$filename);

	 //        $html = file_get_contents($backup_name,$content);
		// 	header('Content-type: text/html');
		// 	header('Content-Disposition: attachment; filename='.$filename);
		// 	echo $html;
		// 	exit;
		// }


		// $datas = $this->db->query("SELECT count(*) as order_info from oc_order_info where bk_status = '0' AND bill_date = '2019-11-16' " )->row;
		// $datass = $this->db->query("SELECT count(*) as order_items from oc_order_items where bk_status = '0' AND bill_date = '2019-11-16'")->row;

		$datas = $this->db->query("SELECT count(*) as order_info from oc_order_info_report where bk_status = '0'  " )->row;
		$datass = $this->db->query("SELECT count(*) as order_items from oc_order_items_report where bk_status = '0'")->row;

		//echo $datas['order_info'];exit;
		$data['order_info'] = $datas['order_info'];
		$data['order_items'] = $datass['order_items'];


		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['advance'])) {
			$data['error_advance'] = $this->error['advance'];
		} else {
			$data['error_advance'] = '';
		}

		
		if (isset($_SESSION['warning_send'])) {
			$data['warning_send'] = $_SESSION['warning_send'];
			unset($_SESSION['warning_send']);
		} else {
			$data['warning_send'] = '';
		}


		if (isset($_SESSION['success_send'])) {
			$data['success_send'] = $_SESSION['success_send'];
			unset($_SESSION['success_send']);
		} else {
			$data['success_send'] = '';
		}

		// echo '<pre>';
		// print_r($data);
		// exit;

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
			if (isset($this->session->data['warning'])) {
				$data['error_warning'] = $this->session->data['warning'];
				unset($this->session->data['warning']);
			}
		} else {
			$data['success'] = '';
			$data['error_warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_advance_billno'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . '&sort=advance_billno' . $url, true);
		$data['sort_booking_date'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . '&sort=booking_date' . $url, true);
		$data['sort_customer_name'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . '&sort=customer_name' . $url, true);
		$data['sort_contact'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . '&sort=sort_contact' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . '&sort=sort_status' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		
		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/databasebackup', $data));
	}

	public function Export(){
			$name ='db_ador2';
			$user ='root';
			$pass ='';
			$host ='localhost';
			//Export the database and output the status to the page
			$mysqli = new mysqli($host,$user,$pass,$name); 
	        $mysqli->select_db($name); 
	        $mysqli->query("SET NAMES 'utf8'");
	        $queryTables = $mysqli->query('SHOW TABLES'); 
	        while($row = $queryTables->fetch_row()) { 
	        	$target_tables_all[] = $row[0]; 
	        }
	        //echo "<pre>";print_r($target_tables_all);exit;
	        
	        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
	        //$backup_name = DIR_DOWNLOAD.'db_ador_bk.sql';
	        //file_put_contents($backup_name);
	        //echo 'Done';exit;

	        foreach($target_tables_all as $table) {
	            $result         =   $mysqli->query('SELECT * FROM '.$table);  
	            $fields_amount  =   $result->field_count;  
	            $rows_num=$mysqli->affected_rows;     
	            $res            =   $mysqli->query('SHOW CREATE TABLE '.$table); 
	            $TableMLine     =   $res->fetch_row();
	            $content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine[1].";\n\n";

	            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
	                while($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
	                    if ($st_counter%100 == 0 || $st_counter == 0 )  {
	                            $content .= "\nINSERT INTO ".$table." VALUES";
	                    }
	                    $content .= "\n(";
	                    for($j=0; $j<$fields_amount; $j++)  { 
	                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
	                        if (isset($row[$j])){
	                            $content .= '"'.$row[$j].'"' ; 
	                        } else {   
	                            $content .= '""';
	                        }     
	                        if ($j<($fields_amount-1)) {
	                            $content.= ',';
	                        }      
	                    }
	                    $content .=")";
	                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
	                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {   
	                        $content .= ";";
	                    } else {
	                        $content .= ",";
	                    } 
	                    $st_counter=$st_counter+1;
	                }
	            } $content .="\n\n\n";
	        }
	        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
	        $backup_name = DIR_DOWNLOAD.'db_ador_bk.sql';
	        //echo $backup_name;exit;
	        file_put_contents($backup_name, $content);

	        $filename = "db_ador.sql";
			$file_path = $backup_name;
			$html = file_get_contents($file_path);
			Header('Content-type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$filename);

	        $html = file_get_contents($backup_name,$content);
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;
	}



	protected function validateForm() { 
    	if (!$this->user->hasPermission('modify', 'catalog/advance')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	// if ((utf8_strlen($this->request->post['advance']) < 0) || (utf8_strlen($this->request->post['advance']) > 64)) {
     //  		$this->error['advance'] = $this->language->get('error_advance');
    	// }
  //   	echo $this->request->post['advance'];
		// echo "im";exit;
		if ( $this->request->post['advance'] <= '0'  ) {
      		$this->error['advance'] = " Please enter Advance Amount";
    	}

    	if ( $this->request->post['c_id'] == ''  ) {
      		$this->error['warning'] = "This is new customer so please make entry of it";
    	}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
					
    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}

	protected function getForm() {
		$this->load->model('catalog/order');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['advance'])) {
			$data['error_advance'] = $this->error['advance'];
		} else {
			$data['error_advance'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['item_id'])) {
			$data['action'] = $this->url->link('catalog/advance/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/advance/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $this->request->get['item_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/advance', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_advance->getItem($this->request->get['item_id']);
		}

		if (isset($this->request->post['c_id'])) {
			$data['c_id'] = $this->request->post['c_id'];
		} elseif (!empty($item_info)) {
			$data['c_id'] = $item_info['c_id'];
		} else {
			$data['c_id'] = '';
		}

		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}

		if (isset($this->request->post['contact'])) {
			$data['contact'] = $this->request->post['contact'];
		} elseif (!empty($item_info)) {
			$data['contact'] = $item_info['contact'];
		} else {
			$data['contact'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($item_info)) {
			$data['address'] = $item_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($item_info)) {
			$data['email'] = $item_info['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['gst_no'])) {
			$data['gst_no'] = $this->request->post['gst_no'];
		} elseif (!empty($item_info)) {
			$data['gst_no'] = $item_info['gst_no'];
		} else {
			$data['gst_no'] = '';
		}

		// if (isset($this->request->post['booking_date'])) {
		// 	$data['booking_date'] = $this->request->post['booking_date'];
		// } elseif (!empty($item_info)) {
		// 	$data['booking_date'] = $item_info['booking_date'];
		// } else {
		// 	$data['booking_date'] = '';
		// }

		if (isset($this->request->post['delivery_date'])) {
			$data['delivery_date'] = $this->request->post['delivery_date'];
		} elseif (!empty($item_info)) {
			$data['delivery_date'] = $item_info['delivery_date'];
		} else {
			$data['delivery_date'] = '';
		}

		if (isset($this->request->post['delivery_time'])) {
			$data['delivery_time'] = $this->request->post['delivery_time'];
		} elseif (!empty($item_info)) {
			$data['delivery_time'] = $item_info['delivery_time'];
		} else {
			$data['delivery_time'] = '';
		}

		if (isset($this->request->post['total'])) {
			$data['total'] = $this->request->post['total'];
		} elseif (!empty($item_info)) {
			$data['total'] = $item_info['total'];
		} else {
			$data['total'] = '';
		}

		if (isset($this->request->post['gst'])) {
			$data['gst'] = $this->request->post['gst'];
		} elseif (!empty($item_info)) {
			$data['gst'] = $item_info['gst'];
		} else {
			$data['gst'] = '';
		}

		if (isset($this->request->post['sc'])) {
			$data['sc'] = $this->request->post['sc'];
		} elseif (!empty($item_info)) {
			$data['sc'] = $item_info['stax'];
		} else {
			$data['sc'] = '';
		}

		if (isset($this->request->post['grandtotal'])) {
			$data['grandtotal'] = $this->request->post['grandtotal'];
		} elseif (!empty($item_info)) {
			$data['grandtotal'] = $item_info['grand_total'];
		} else {
			$data['grandtotal'] = '';
		}

		if (isset($this->request->post['advance'])) {
			$data['advance'] = $this->request->post['advance'];
		} elseif (!empty($item_info)) {
			$data['advance'] = $item_info['advance_amt'];
		} else {
			$data['advance'] = '';
		}

		if (isset($this->request->post['balance'])) {
			$data['balance'] = $this->request->post['balance'];
		} elseif (!empty($item_info)) {
			$data['balance'] = $item_info['balance'];
		} else {
			$data['balance'] = '';
		}


		if (isset($this->request->get['item_id'])){
			$data['testdatas'] = $this->db->query("SELECT * FROM oc_advance_item WHERE advance_id = '".$this->request->get['item_id']."'")->rows;
		} else{
			$data['testdatas'] = array();
		}

		$data['add_customer'] = $this->url->link('catalog/customer/add', 'token=' . $this->session->data['token'] . $url.'&refer=1', true);

		$data['INCLUSIVE'] = $this->model_catalog_order->get_settings('INCLUSIVE');
		$data['SERVICE_CHARGE_FOOD'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$data['SERVICE_CHARGE_LIQ'] = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/advance_form', $data));
	}

	public function prints($advance_id){
		$this->load->model('catalog/order');
		$advance = $this->db->query("SELECT * FROM oc_advance WHERE id = '".$advance_id."'")->row;
		$advance_items = $this->db->query("SELECT * FROM oc_advance_item WHERE advance_id = '".$advance_id."'")->rows;
		foreach($advance_items as $advance_item){
			$advancedata[] = array(
				'name'			=> $advance_item['item_name'],
				'rate'          => $advance_item['rate'],
				'amt'           => $advance_item['amt'],
				'qty'         	=> $advance_item['qty'],
				'msg'			=> $advance_item['msg']
			);
		}
		// print_r($advancedata);
		// exit();
		try {
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	} else {
		 		$connector = '';
		 	}
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->text("Advance Receipt");
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->text("Name :".str_pad($advance['customer_name'],20).""."Contact :".$advance['contact']);
		   	$printer->feed(1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		   	$printer->text("Address :".wordwrap($advance['address'],15,"\n"));
		   	$printer->setJustification(Printer::JUSTIFY_LEFT);
		   	$printer->feed(1);
		   	$printer->text("Advance Receipt No:".$advance['advance_billno']);
		   	$printer->feed(1);
		   	$printer->text("Booking Date:".$advance['booking_date']);
		   	$printer->feed(1);
		   	$printer->text("Delivery Date:".$advance['delivery_date']);
		   	$printer->feed(1);
		   	$printer->text("Delivery Time:".$advance['delivery_time']);
		   	$printer->feed(1);
		   	$printer->text("----------------------------------------------");
		   	$printer->feed(1);
		   	$printer->text(str_pad("Name",16)."".str_pad("Qty", 10)."".str_pad("Rate", 10)."Amount");
		   	$printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(false);
		    $total_items_normal = 0;
			$total_quantity_normal = 0;
		    foreach($advancedata as $nkey => $nvalue){
	    	  	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 11);
				//$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0, 4);
				$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
				//$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 4);
		    	//$valuess['qty'] = utf8_substr(html_entity_decode($valuess['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
		    	//$printer->text($valuess['qty']." ".wordwrap($valuess['name'],15,"\n"));
		    	//$printer->feed(1);
		    	$printer->text("".str_pad($nvalue['name'],16)."".str_pad($nvalue['rate'],10)."".str_pad($nvalue['qty'],10)."".$nvalue['amt']);
		    	if($nvalue['msg'] != ''){
		    		$printer->feed(1);
		    		$printer->text("Msg :".wordwrap($nvalue['msg'],10,"\n"));
				}
		    	$printer->feed(1);
		    	$total_items_normal ++ ;
		    	$total_quantity_normal = $total_quantity_normal + $nvalue['qty'];
	    	}
	    	$printer->setTextSize(1, 1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setEmphasis(true);
		    $printer->text("T Qty :  ".$total_quantity_normal."     T Item :  ".$total_items_normal."");
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->text(str_pad("Total",35).$advance['total']."");
		    $printer->feed(1);
		    $printer->text(str_pad("GST",35).$advance['gst']."");
		    $printer->feed(1);
		    $printer->text(str_pad("Grand Total",35).$advance['grand_total']."");
		    $printer->feed(1);
		    $printer->text(str_pad("Advance",35).$advance['advance_amt']."");
		    $printer->feed(1);
		    $printer->text(str_pad("Balance",35).$advance['balance']."");
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text($this->model_catalog_order->get_settings('ADVANCE_NOTE'));
		    $printer->feed(1);
		    $printer->text("----------------------------------------------");
		    $printer->feed(2);
		    $printer->cut();
			$printer->feed(2);
		    // Close printer //
		    $printer->close();
		} catch (Exception $e) {
		    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
		}
		$this->getList();
	}

	public function makebill(){
		$this->load->model('catalog/advance');
		$this->load->model('catalog/order');
		$order_id = $this->model_catalog_advance->makeBill($this->request->get['item_id']);

		$anss = $this->db->query("SELECT * FROM oc_order_items WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'")->rows;
		$testfood = array();
		$testtaxvalue1food = 0;

		$tests = $this->db->query("SELECT SUM(amt) as amt ,SUM(tax1_value) as tax1_value ,is_liq,tax1 FROM `oc_order_items` WHERE order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1' GROUP BY tax1")->rows;
		$this->log->write("SELECT SUM(amt) as amt,SUM(tax1_value) as tax1_value,is_liq,tax1 FROM `oc_order_items` GROUP BY tax1");
		foreach($tests as $test){

			if($test['is_liq'] == '0'){
				$testfoods[] = array(
					'tax1' => $test['tax1'],
					'amt' => $test['amt'],
					'tax1_value' => $test['tax1_value']
					);

			} else{
				$testliqs[] = array(
					'tax1' => $test['tax1'],
					'amt' => $test['amt'],
					'tax1_value' => $test['tax1_value']
					);
			}

		}

		$countfood = $this->db->query("SELECT count(*) as id FROM oc_order_items WHERE is_liq = '0' AND order_id = '".$order_id."' AND cancelstatus = '0' AND ismodifier = '1'");
		$totalitemsfood = $countfood->row['id'];

		$infos = array();
		$totalquantityfood = 0;
		$disamtfood = 0;

		foreach ($anss as $lkey => $result) {

			foreach($anss as $lkeys => $results){
				if($lkey == $lkeys) {

				} elseif($lkey > $lkeys && $result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']){
					if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['code'] = '';
						}
					}
				} elseif ($result['code'] == $results['code'] && $result['rate'] == $results['rate'] && $result['message'] == $results['message']) {
					if(($result['amt'] == $results['amt']) || ($results['amt'] != '0' && $result['amt'] != '0')){
						if($result['parent'] == '0'){
							$result['qty'] = $result['qty'] + $results['qty'];
							$result['amt'] = $result['qty'] * $result['rate'];
						}
					}
				}
			}

			if($result['code'] != ''){
				$infos[] = array(
					'id'			=> $result['id'],
					'name'          => $result['name'],
					'rate'          => $result['rate'],
					'amt'           => $result['amt'],
					'qty'         	=> $result['qty'],
					'tax1'         	=> $result['tax1'],
					'tax2'          => $result['tax2'],
					'discount_value'=> $result['discount_value']
				);
				$totalquantityfood = $totalquantityfood + $result['qty'];
				$disamtfood = $disamtfood + $result['discount_value'];
			}
		}

			$ans = $this->db->query("SELECT * FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
			$ansfood = $this->db->query("SELECT billno FROM oc_order_items WHERE is_liq = '0' AND order_id = '".$order_id."' AND cancelstatus = '0'")->row;

			$csgst=$ans['gst']/2;
			$csgsttotal = $ans['gst'];

			$ans['cust_name'] = utf8_substr(html_entity_decode($ans['cust_name'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ans['cust_address'] = utf8_substr(html_entity_decode($ans['cust_address'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ans['location'] = utf8_substr(html_entity_decode($ans['location'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ans['waiter'] = utf8_substr(html_entity_decode($ans['waiter'], ENT_QUOTES, 'UTF-8'), 0, 15);
			$ans['ftotal'] = utf8_substr(html_entity_decode($ans['ftotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ans['ltotal'] = utf8_substr(html_entity_decode($ans['ltotal'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ans['cust_contact'] = utf8_substr(html_entity_decode($ans['cust_contact'], ENT_QUOTES, 'UTF-8'), 0, 10);
			$ans['t_name'] = utf8_substr(html_entity_decode($ans['t_name'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ans['captain'] = utf8_substr(html_entity_decode($ans['captain'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ans['vat'] = utf8_substr(html_entity_decode($ans['vat'], ENT_QUOTES, 'UTF-8'), 0, 8);
			$csgst = utf8_substr(html_entity_decode($csgst, ENT_QUOTES, 'UTF-8'), 0, 8);
			$ans['grand_total']  = utf8_substr(html_entity_decode($ans['grand_total'], ENT_QUOTES, 'UTF-8'), 0, 9);
			$ans['grand_total'] = ceil($ans['grand_total']);
			
			try {
				    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
				 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
				 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
				 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
				 	} else {
				 		$connector = '';
				 	}
				    $printer = new Printer($connector);
				    $printer->selectPrintMode(32);

				   	$printer->setEmphasis(true);
				   	$printer->setTextSize(2, 1);
				   	$printer->setJustification(Printer::JUSTIFY_CENTER);
				    //$printer->feed(1);
				   	//$printer->setFont(Printer::FONT_B);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_NAME'));
				    $printer->feed(1);
				    $printer->setTextSize(1, 1);
				    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
				    $printer->feed(1);
				    $printer->text("Advance No :".$ans['advance_billno']);
				    $printer->feed(1);
				    $printer->setJustification(Printer::JUSTIFY_CENTER);
				    $printer->setJustification(Printer::JUSTIFY_LEFT);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);

				    if($ans['cust_contact'] == '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' &&  $ans['gst_no'] == ''){
						
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Address : ".$ans['cust_address']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Gst No :".$ans['gst_no']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] != ''){
					 	$printer->text(str_pad("Name : ".$ans['cust_name'],25)."Gst No :".$ans['gst_no']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] == '' && $ans['cust_contact'] != '' && $ans['cust_address'] != '' && $ans['gst_no'] == ''){
					 	$printer->text(str_pad("Mobile :".$ans['cust_contact'],25)."Address : ".$ans['cust_address']."");
					 	$printer->feed(1);
					}
					else if($ans['cust_name'] != '' && $ans['cust_contact'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Name :".$ans['cust_name']."");
					    $printer->feed(1);
					}
					else if($ans['cust_contact'] != '' && $ans['cust_name'] == '' && $ans['cust_address'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					}else if($ans['cust_address'] != '' && $ans['cust_name'] == '' && $ans['cust_contact'] == '' && $ans['gst_no'] == ''){
					    $printer->text("Address : ".$ans['cust_address']."");
					    $printer->feed(1);
					}else if( $ans['gst_no'] != '' && $ans['cust_address'] == '' && $ans['cust_name'] == '' && $ans['cust_contact'] == ''){
					    $printer->text("Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}else{
					    $printer->text(str_pad("Name : ".$ans['cust_name'],25)."Mobile :".$ans['cust_contact']."");
					    $printer->feed(1);
					    $printer->text(str_pad("Address : ".$ans['cust_address'],25)."Gst No :".$ans['gst_no']."");
					    $printer->feed(1);
					}
					$printer->text(str_pad("User Id :".$ans['login_id'],30)."K.Ref.No :".$order_id);
				    $printer->setEmphasis(true);
				   	$printer->setTextSize(1, 1);
				   	if($infos){
				   		$printer->feed(1);
				   		$printer->setJustification(Printer::JUSTIFY_CENTER);
				   		$printer->text("Date :".str_pad(date('d-m-Y',strtotime($ans['bill_date'])),13)."Bill no: ".str_pad($ansfood['billno'],5)."Time :".date('h:i:s'));
				   		$printer->feed(1);
				   		$printer->text("Ref no: ".$ans['order_no']."");
				   		$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->setJustification(Printer::JUSTIFY_LEFT);
				    	// $printer->text("Tbl No        Wtr    Cpt    ".date('d/m/y', strtotime($infoss[0]['date_added']))."");
				    	$printer->text(str_pad("Loc",10)."".str_pad("Tbl No",10)."".str_pad("Wtr",10)."".str_pad("Cpt",10)."Person");
				    	$printer->feed(1);
				   		$printer->setTextSize(1, 1);
				   		$printer->text(str_pad($ans['location'],10)."".str_pad($ans['t_name'],10)."".str_pad($ans['waiter_id'],10)."".str_pad($ans['captain_id'],10)."".$ans['person']."");
					    $printer->feed(1);
				   		$printer->setEmphasis(false);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						$printer->text(str_pad("Name",22)."".str_pad("Rate",10)."".str_pad("Qty",10).""."Amt");
						$printer->feed(1);
				   	 	$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setEmphasis(false);
					    foreach($infos as $nkey => $nvalue){
					    	$nvalue['name'] = utf8_substr(html_entity_decode($nvalue['name'], ENT_QUOTES, 'UTF-8'), 0, 15);
					    	$nvalue['rate'] = utf8_substr(html_entity_decode($nvalue['rate'], ENT_QUOTES, 'UTF-8'), 0, 4);
					    	$nvalue['qty'] = utf8_substr(html_entity_decode($nvalue['qty'], ENT_QUOTES, 'UTF-8'), 0, 4);
					    	//$nvalue['amt'] = utf8_substr(html_entity_decode($nvalue['amt'], ENT_QUOTES, 'UTF-8'), 0, 4);
					    	$printer->text("".str_pad($nvalue['name'],22)."".str_pad($nvalue['rate'],10)."".str_pad($nvalue['qty'],10)."".$nvalue['amt']);
					    	$printer->feed(1);
					    }
					    $printer->text("----------------------------------------------");
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_LEFT);
					    $printer->text("Total Items: ".str_pad($totalitemsfood,10)."Total quantity :".$totalquantityfood."");
					    $printer->feed(1);
					    $printer->setEmphasis(false);
					   	$printer->setTextSize(1, 1);
					    $printer->text("----------------------------------------------");
						$printer->feed(1);
						foreach($testfoods as $tkey => $tvalue){
							$printer->text($tvalue['tax1']."% On ".$tvalue['amt']." is ".$tvalue['tax1_value']);
					    	$printer->feed(1);
						}
						$printer->text("----------------------------------------------");
						$printer->feed(1);
						$printer->setJustification(Printer::JUSTIFY_LEFT);
						if($ans['fdiscountper'] != '0'){
							$printer->text(str_pad("F.Total :".$ans['ftotal'],25)."Discount(".$ans['fdiscountper']."%) :".$ans['ftotalvalue']."");
							$printer->feed(1);
						} elseif($ans['discount'] != '0'){
							$printer->text(str_pad("F.Total :".$ans['ftotal'],22)."Discount(".$ans['ftotalvalue']."rs):".$ans['ftotalvalue']."");
							$printer->feed(1);
						} elseif ($ans['fdiscountper'] == '0' && $ans['discount'] == '0') {
							$printer->text("F.Total :".$ans['ftotal']);
							$printer->feed(1);
						}
						$printer->text(str_pad("",33)."SCGST :".$csgst."");
						$printer->feed(1);
						$printer->text(str_pad("",33)."CCGST :".$csgst."");
						$printer->feed(1);
						if($ans['parcel_status'] == '0'){
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}else{
								$printer->text(str_pad("",34)."SCRG :".$ans['staxfood']."");
								$printer->feed(1);
								$netamountfood = (($csgsttotal + $ans['ftotal']) - ($disamtfood)) + ($ans['staxfood']);
							}
						} else{
							if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
								$netamountfood = ($ans['ftotal'] - ($disamtfood));
							} else{
								$netamountfood = ($csgsttotal + $ans['ftotal'] - ($disamtfood));
							}
						}
						$printer->setEmphasis(true);
						$printer->text(str_pad("",29)."Net total :".ceil($netamountfood)."");
						$printer->setEmphasis(false);
					}				   	
					$printer->feed(1);
					$printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(true);
					if($ans['advance_amount'] != '0.00'){
						$printer->text(str_pad("Advance Amount",38).$ans['advance_amount']."");
						$printer->feed(1);
					}
					$printer->text(str_pad("Grand Total",38).$ans['grand_total']."");
					$printer->feed(1);
				    $printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_LEFT);
					$printer->setEmphasis(false);
				   	$printer->setTextSize(1, 1);
					$printer->text("GST NO.".$this->model_catalog_order->get_settings('GST_NO'));
					$printer->feed(1);
					if($this->model_catalog_order->get_settings('TEXT1') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT1'));
						$printer->feed(1);
					}
					if($this->model_catalog_order->get_settings('TEXT2') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT2'));
						$printer->feed(1);
					}
					$printer->text("----------------------------------------------");
					$printer->feed(1);
					$printer->setJustification(Printer::JUSTIFY_CENTER);
					if($this->model_catalog_order->get_settings('TEXT3') != ''){
						$printer->text($this->model_catalog_order->get_settings('TEXT3'));
					}
					$printer->feed(2);
					$printer->cut();
				    // Close printer //
				    $printer->close();
				} catch (Exception $e) {
				    $this->session->data['warning'] = $this->model_catalog_order->get_settings('PRINTER_NAME')." "."Not Working";
				}
		$this->response->redirect($this->url->link('catalog/advance', 'token=' . $this->session->data['token'] .'&order_id='.$order_id, true));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/advance')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/advance');

		return !$this->error;
	}

	public function autocompletecustname() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {

			$results = $this->db->query("SELECT email,gst_no,name,address,contact,c_id FROM oc_customerinfo WHERE name LIKE '%" .$this->request->get['filter_item_name']. "%' LIMIT 0,15")->rows;

			foreach ($results as $result) {
				$json[] = array(
					'name' => $result['name'],
					'address' => $result['address'],
					'contact' => $result['contact'],
					'c_id' => $result['c_id'],
					'email' => $result['email'],
					'gst_no' => $result['gst_no']
					//'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteitemname() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {

			$results = $this->db->query("SELECT * FROM oc_item WHERE item_name LIKE '%" .$this->request->get['filter_item_name']. "%' AND is_liq = '0' LIMIT 0,5")->rows;

			foreach ($results as $result) {

				$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['vat']."'");
				if($tax1->num_rows > 0){
					$taxvalue1 = $tax1->row['tax_value'];
				} else{
					$taxvalue1 = '0';
				}

				$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$result['tax2']."'");
				if($tax2->num_rows > 0){
					$taxvalue2 = $tax2->row['tax_value'];
				} else{
					$taxvalue2 = '0';
				}

				$json[] = array(
					'item_id' => $result['item_id'],
					'item_code' => $result['item_code'],
					'rate' => $result['rate_1'],
					'tax1'       	=> $taxvalue1,
					'tax2'       	=> $taxvalue2,
					'subcategoryid' => $result['item_sub_category_id'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}


	public function autocompletekotmsg() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$results = $this->db->query("SELECT * from oc_kotmsg WHERE message LIKE '%" . $this->request->get['filter_name'] . "%' limit 0,10 ")->rows;

			foreach ($results as $result) {
				$json[] = array(
					'msg_code' => strip_tags(html_entity_decode($result['msg_code'], ENT_QUOTES, 'UTF-8')),
					'message'  => strip_tags(html_entity_decode($result['message'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {  
			$sort_order[$key] = $value['message'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_customername'])) {
			$this->load->model('catalog/advance');

			$data = array(
				'filter_customername' => $this->request->get['filter_customername'],
				'filter_status' => '',
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_advance->getItems($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['id'],
					'contact' => $result['contact'],
					'customername'  => $result['customer_name']
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['customername'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}