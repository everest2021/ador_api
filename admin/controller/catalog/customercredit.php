<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ControllerCatalogCustomerCredit extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('catalog/customercredit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customercredit');

		$this->getList();
	}
	public function add() {
		$this->load->language('catalog/customercredit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customercredit');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$creditid = $this->model_catalog_customercredit->addCredit($this->request->post);
			$this->printCredit($creditid);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function printCredit($creditid){
		$this->load->model('catalog/order');
		$creditDetails = $this->db->query("SELECT * FROM oc_customercredit WHERE id = '".$creditid."'")->row;
		//echo "<pre>";
		//print_r($creditDetails);
		//exit();
		$printername = $this->user->getPrinterName();
		$printtype = $this->user->getPrinterType();
		try {

			if($printtype == 'Network'){
							$connector = new NetworkPrintConnector($printername, 9100);
						} else if($printtype == 'Windows'){
						 	$connector = new WindowsPrintConnector($printername);
						} else {
						 	$connector = '';
						}
		    if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Network'){
		 		$connector = new NetworkPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'), 9100);
		 	} else if($this->model_catalog_order->get_settings('PRINTER_TYPE') == 'Windows'){
		 		$connector = new WindowsPrintConnector($this->model_catalog_order->get_settings('PRINTER_NAME'));
		 	}
		 	//echo var_dump($connector);
		 	//exit;
		    $printer = new Printer($connector);
		    $printer->selectPrintMode(32);

		   	$printer->setEmphasis(true);
		   	$printer->setTextSize(2, 1);
		   	$printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->text(html_entity_decode($this->model_catalog_order->get_settings('HOTEL_NAME'), ENT_QUOTES, 'UTF-8'));
		    $printer->feed(1);
		    $printer->setTextSize(1, 1);
		    $printer->text($this->model_catalog_order->get_settings('HOTEL_ADD'));
		    $printer->feed(1);
		    $printer->setJustification(Printer::JUSTIFY_CENTER);
		    $printer->setJustification(Printer::JUSTIFY_LEFT);
		    $printer->setEmphasis(true);
		   	$printer->setTextSize(1, 1);
		   	$printer->feed(1);
			$printer->text("----------------------------------------------");
			$printer->feed(1);
			$printer->text(str_pad("Date",13).": ".$creditDetails['date']);
			$printer->feed(1);
			$printer->text(str_pad("Customer Name",13).": ".$creditDetails['customer_name']);
			$printer->feed(1);
			$printer->text(str_pad("Contact",13).": ".$creditDetails['contact']);
			$printer->feed(1);
			$printer->text(str_pad("Credit",13).": ".$creditDetails['credit']);
			$printer->feed(1);
			$printer->text(str_pad("MSR NO.",13).": ".$creditDetails['msr_no']);
			$printer->feed(1);
			$printer->text("----------------------------------------------");
			$printer->feed(2);
			$printer->cut();
		    $printer->close();
		} catch (Exception $e) {
		     echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}exit;
		$this->getList();
	}

	public function edit() {
		$this->load->language('catalog/customercredit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customercredit');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_catalog_customercredit->editCredit($this->request->get['item_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_item_type'])) {
				$url .= '&filter_item_type=' . $this->request->get['filter_item_type'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/customercredit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/customercredit');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $item_id) {
				$this->model_catalog_customercredit->deleteCredit($item_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_customername'])) {
			$filter_customername = $this->request->get['filter_customername'];
		} else {
			$filter_customername = null;
		}

		if (isset($this->request->get['filter_item_id'])) {
			$filter_item_id = $this->request->get['filter_item_id'];
		} else {
			$filter_item_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'customer_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/customercredit/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/customercredit/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['items'] = array();

		$filter_data = array(
			'filter_item_id' => $filter_item_id,
			'filter_customername' => $filter_customername,
			'order' => $order,
			'sort' => $sort,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$item_total = $this->model_catalog_customercredit->getTotalCredits($filter_data);

		$results = $this->model_catalog_customercredit->getCredits($filter_data);
		// echo "<pre>";
		// print_r($results);
		// exit();

		foreach ($results as $result) {
			$customer = $this->db->query("SELECT * FROM oc_customercredit WHERE id = '".$result['id']."'")->row;
			$orderdata = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$customer['c_id']."'")->row;
			$balance = $result['credit'] - $orderdata['onac'];
			$data['items'][] = array(
				'item_id' 			=> $result['id'],
				'customername'  	=> $result['customer_name'],
				'contact'			=> $result['contact'],
				'credit'       		=> $result['credit'],
				'msr_no'       		=> $result['msr_no'],
				'balance'			=> $balance,
				'edit'        		=> $this->url->link('catalog/customercredit/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $result['id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_customer_name'] = $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . '&sort=customer_name' . $url, true);
		$data['sort_contact'] = $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . '&sort=contact' . $url, true);
		$data['sort_credit'] = $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . '&sort=credit' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['filter_item_id'])) {
			$url .= '&filter_item_id=' . $this->request->get['filter_item_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $item_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($item_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($item_total - $this->config->get('config_limit_admin'))) ? $item_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $item_total, ceil($item_total / $this->config->get('config_limit_admin')));

		$data['filter_customername'] = $filter_customername;
		$data['filter_item_id'] = $filter_item_id;
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/customercreditlist', $data));
	}

	protected function getForm() {
		

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['item_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['item_name'])) {
			$data['error_item_name'] = $this->error['item_name'];
		} else {
			$data['error_item_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_customername'])) {
			$url .= '&filter_customername=' . $this->request->get['filter_customername'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['item_id'])) {
			$data['action'] = $this->url->link('catalog/customercredit/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/customercredit/edit', 'token=' . $this->session->data['token'] . '&item_id=' . $this->request->get['item_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/customercredit', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['item_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$item_info = $this->model_catalog_customercredit->getCredit($this->request->get['item_id']);
		}

		if (isset($this->request->post['c_id'])) {
			$data['c_id'] = $this->request->post['c_id'];
		} elseif (!empty($item_info)) {
			$data['c_id'] = $item_info['c_id'];
		} else {
			$data['c_id'] = '';
		}

		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}

		if (isset($this->request->post['customername'])) {
			$data['customername'] = $this->request->post['customername'];
		} elseif (!empty($item_info)) {
			$data['customername'] = $item_info['customer_name'];
		} else {
			$data['customername'] = '';
		}

		if (isset($this->request->post['contact'])) {
			$data['contact'] = $this->request->post['contact'];
		} elseif (!empty($item_info)) {
			$data['contact'] = $item_info['contact'];
		} else {
			$data['contact'] = '';
		}

		if (isset($this->request->post['msr_no'])) {
			$data['msr_no'] = $this->request->post['msr_no'];
		} elseif (!empty($item_info)) {
			$data['msr_no'] = $item_info['msr_no'];
		} else {
			$data['msr_no'] = '';
		}

		//$data['add_customer'] = $this->url->link('catalog/customer/add', 'token=' . $this->session->data['token'] . $url.'&refer=1', true);

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo $data['inc_rate_1'];
		//exit();

		$this->response->setOutput($this->load->view('catalog/customercreditform', $data));
	}


	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/customercredit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/customercredit');

		return !$this->error;
	}

	public function autocompletecustname() {
		$json = array();

		if (isset($this->request->get['filter_item_name'])) {

			$results = $this->db->query("SELECT name,contact,c_id FROM oc_customerinfo WHERE name LIKE '%" .$this->request->get['filter_item_name']. "%' OR contact LIKE '%" .$this->request->get['filter_item_name']. "%' LIMIT 0,15")->rows;

			foreach ($results as $result) {
				$customer = $this->db->query("SELECT * FROM oc_customerinfo WHERE c_id = '".$result['c_id']."'")->row;
				$customercredit = $this->db->query("SELECT SUM(credit) as credit FROM oc_customercredit WHERE c_id = '".$customer['c_id']."'");
				$orderdata = $this->db->query("SELECT SUM(onac) as onac FROM oc_order_info WHERE onaccust = '".$customer['c_id']."'");

				if($customercredit->num_rows > 0){
					$credit = $customercredit->row['credit'];
				} else{
					$credit = 0;
				}

				if($orderdata->num_rows > 0){
					$debit = $orderdata->row['onac'];
				} else{
					$debit = 0;
				}

				$balance = $credit - $debit;
				$json[] = array(
					'name' => $result['name'],
					'contact' => $result['contact'],
					'c_id' => $result['c_id'],
					'credit' => $credit,
					'balance' => $balance
					//'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_customername'])) {
			$this->load->model('catalog/customercredit');

			$data = array(
				'filter_customername' => $this->request->get['filter_customername'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_customercredit->getCredits($data);

			foreach ($results as $result) {
				$json[] = array(
					'item_id' => $result['id'],
					'customername'  => $result['customer_name'],
					'contact'  => $result['contact'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['customername'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}