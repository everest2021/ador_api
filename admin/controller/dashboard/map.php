<?php
class ControllerDashboardMap extends Controller {
	public function index() {
		$this->load->language('dashboard/map');

		$data['heading_title'] = 'एकूण गेम / Total Games';//$this->language->get('heading_title');

		$data['text_view'] = 'View Detail';//$this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
		$total_sports = $this->db->query($sql)->num_rows;
		$data['total_sports'] = $total_sports;
		$data['total_link'] = $this->url->link('catalog/sport', 'token=' . $this->session->data['token'], true);

		$sql = "SELECT * FROM `oc_sport` WHERE `sport_type_id` = '1' ";
		$kala_sports = $this->db->query($sql)->num_rows;
		$data['kala_sports'] = $kala_sports;
		$data['kala_link'] = $this->url->link('catalog/sport', 'token=' . $this->session->data['token'].'&filter_sport_type=1', true);

		$sql = "SELECT * FROM `oc_sport` WHERE `sport_type_id` = '2' ";
		$krida_sports = $this->db->query($sql)->num_rows;
		$data['krida_sports'] = $krida_sports;
		$data['krida_link'] = $this->url->link('catalog/sport', 'token=' . $this->session->data['token'].'&filter_sport_type=2', true);
		
		return $this->load->view('dashboard/map', $data);
	}
}