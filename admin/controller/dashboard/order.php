<?php
class ControllerDashboardOrder extends Controller {
	public function index() {
		$this->load->language('dashboard/order');

		$data['heading_title'] = 'शाळा नोंदणीकृत / Schools Registered';//$this->language->get('heading_title');

		$data['text_view'] = 'View Detail';//$this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('sale/order');

		$sql = "SELECT * FROM `oc_customer` GROUP BY `school_id` ";
		$school_count = $this->db->query($sql)->num_rows;
		$data['count'] = $school_count;

		$data['url'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/order', $data);
	}
}