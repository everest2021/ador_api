<?php
class ControllerDashboardSale extends Controller {
	public function index() {
		$this->load->language('dashboard/sale');

		$data['heading_title'] = 'कला वैयक्तिक सहभागी / Kala Individual Participants';//$this->language->get('heading_title');

		$data['text_view'] = 'View Detail';//$this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		$sql = "SELECT * FROM `oc_customer` WHERE `sport_type_id` = '1' AND `participant_type_id` = '1' ";
		$count = $this->db->query($sql)->num_rows;
		$data['count'] = $count;

		$data['url'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'].'&filter_sport_type=1&filter_participant_type=1', true);

		return $this->load->view('dashboard/sale', $data);
	}
}
