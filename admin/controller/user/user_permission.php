<?php
class ControllerUserUserPermission extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		$this->getList();
	}

	public function add() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->addUserGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->editUserGroup($this->request->get['user_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $user_group_id) {
				$this->model_user_user_group->deleteUserGroup($user_group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('user/user_permission/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('user/user_permission/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['user_groups'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$user_group_total = $this->model_user_user_group->getTotalUserGroups();

		$results = $this->model_user_user_group->getUserGroups($filter_data);

		foreach ($results as $result) {
			$data['user_groups'][] = array(
				'user_group_id' => $result['user_group_id'],
				'name'          => $result['name'],
				'edit'          => $this->url->link('user/user_permission/edit', 'token=' . $this->session->data['token'] . '&user_group_id=' . $result['user_group_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($user_group_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($user_group_total - $this->config->get('config_limit_admin'))) ? $user_group_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $user_group_total, ceil($user_group_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/user_group_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['user_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_access'] = $this->language->get('entry_access');
		$data['entry_modify'] = $this->language->get('entry_modify');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['user_group_id'])) {
			$data['action'] = $this->url->link('user/user_permission/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('user/user_permission/edit', 'token=' . $this->session->data['token'] . '&user_group_id=' . $this->request->get['user_group_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
		}

		//echo "<pre>";print_r($user_group_info);exit;

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($user_group_info)) {
			$data['name'] = $user_group_info['name'];
		} else {
			$data['name'] = '';
		}

		// $ignore = array(
		// 	'common/dashboard',
		// 	'common/startup',
		// 	'common/login',
		// 	'common/logout',
		// 	'common/forgotten',
		// 	'common/reset',			
		// 	'common/footer',
		// 	'common/header',
		// 	'error/not_found',
		// 	'error/permission',
		// 	'dashboard/order',
		// 	'dashboard/sale',
		// 	'dashboard/customer',
		// 	'dashboard/online',
		// 	'dashboard/map',
		// 	'dashboard/activity',
		// 	'dashboard/chart',
		// 	'dashboard/recent'
		// );

		$data['permissions'] = array();
		// $files = array();

		// // Make path into an array
		// $path = array(DIR_APPLICATION . 'controller/*');

		// // While the path array is still populated keep looping through
		// while (count($path) != 0) {
		// 	$next = array_shift($path);

		// 	foreach (glob($next) as $file) {
		// 		// If directory add to path array
		// 		if (is_dir($file)) {
		// 			$path[] = $file . '/*';
		// 		}

		// 		// Add the file to the files to be deleted array
		// 		if (is_file($file)) {
		// 			$files[] = $file;
		// 		}
		// 	}
		// }

		// // Sort the file array
		// sort($files);
					
		// foreach ($files as $file) {
		// 	$controller = substr($file, strlen(DIR_APPLICATION . 'controller/'));

		// 	$permission = substr($controller, 0, strrpos($controller, '.'));

		// 	if (!in_array($permission, $ignore)) {
		// 		$data['permissions'][] = $permission;
		// 	}
		// }
		$data['permissions']['catalog/werafood_itemdata'] = 'Wera Food Item ';

		$data['permissions']['catalog/werafood_order_report'] = 'Wera Food Order Report ';
		$data['permissions']['catalog/werafood_reject_report'] = 'Wera Food Reject Report ';
		$data['permissions']['catalog/store_onoff'] = 'Store ON/OFF Report ';
		$data['permissions']['catalog/werapending_order'] = 'Wera Pending Order ';

		$data['permissions']['catalog/customerwise_item_report'] = 'Customer Wise Item Report';
		$data['permissions']['catalog/customerwise_sale_report'] = 'Customer Wise Sale Report ';

		$data['permissions']['catalog/repair'] = 'repair ';


		$data['permissions']['catalog/dailybill'] = 'Daily Bill Wise';
		$data['permissions']['catalog/dailybillwiseitem_report'] = 'Daily Bill wise Item Report';
		$data['permissions']['catalog/dailyreportstock'] = 'Daily Item Wise Report';
		$data['permissions']['catalog/dailydaysummaryreport'] = 'Daily Summary Report';
		$data['permissions']['catalog/dailyminidaysummaryreport'] = 'Mini Day Summary Report';

		$data['permissions']['catalog/dailyshiftclose_report'] = 'Daily Shift Close Report';

		$data['permissions']['catalog/events'] = 'events';
		$data['permissions']['catalog/msr_redeem'] = 'MSR Redeem';
		$data['permissions']['catalog/msr_ledger_report'] = 'MSR Ledger Report';
		$data['permissions']['catalog/msr_redeem_report'] = 'MSR Redeem Report';

		$data['permissions']['catalog/onlinepayment'] = 'onlinepayment';
		$data['permissions']['catalog/stock_invoice'] = 'Stock print';
		$data['permissions']['catalog/reception'] = 'Reception';
		$data['permissions']['catalog/stockclear'] = 'stockclear';
		$data['permissions']['catalog/expensetrans'] = 'Expense Transaction';
		$data['permissions']['catalog/accountexpense'] = 'Account Expense';
		$data['permissions']['catalog/databasebackup'] = 'databasebackup';
		$data['permissions']['catalog/orderprocesss'] = 'Orderprocesss';
		$data['permissions']['catalog/kitchen_display'] = 'Kitchen Display';
		$data['permissions']['catalog/feedback'] = 'Feedback Form';
		$data['permissions']['catalog/feedback_report'] = 'Feedback Report';
		$data['permissions']['catalog/feedbackreport'] = 'Feedback Report';
		$data['permissions']['catalog/msr_recharge'] = 'MSR Recharge';
		$data['permissions']['catalog/msr_refund'] = 'MSR Refund';
		$data['permissions']['catalog/msr_recharge_report'] = 'MSR Recharge Report';
		$data['permissions']['catalog/msr_refund_report'] = 'MSR Refund Report';
		$data['permissions']['catalog/msr_ledger'] = 'MSR Ledger Report';
		$data['permissions']['catalog/sms_report'] = 'SMS Report';
		$data['permissions']['catalog/shiftclose_report'] = 'Shift Close Report';
		$data['permissions']['catalog/billwiseitem_report'] = 'Bill Wise Item Report';
		$data['permissions']['catalog/prebill_report'] = 'Previous Bill Print Report';
		$data['permissions']['catalog/salepromotion'] = 'Sale Promotion';
		$data['permissions']['catalog/sync_to'] = 'Sync To';
		$data['permissions']['catalog/purchaseentryloose'] = 'Purchaseentryloose';
		$data['permissions']['catalog/sms'] = 'SMS';
		$data['permissions']['catalog/templet'] = 'Templet';
		$data['permissions']['catalog/venue'] = 'Venue';
		$data['permissions']['catalog/meal'] = 'Meal';
		$data['permissions']['catalog/activity'] = 'Activity';
		$data['permissions']['catalog/item'] = 'Item';
		$data['permissions']['catalog/modifier'] = 'Modifier';
		$data['permissions']['catalog/kotgroup'] = 'Kot Group';
		$data['permissions']['catalog/itembulk'] = 'Bulk Item Update';
		$data['permissions']['catalog/department'] = 'Department';
		$data['permissions']['catalog/location'] = 'Table Master';
		$data['permissions']['catalog/testimonal'] = 'Testimonal';
		$data['permissions']['catalog/baner'] = 'Baner';
		$data['permissions']['catalog/notification'] = 'notification';
		//$data['permissions']['catalog/table'] = 'Table';
		$data['permissions']['catalog/barcode'] = 'Barcode';
		$data['permissions']['catalog/category'] = 'Category';
		$data['permissions']['catalog/subcategory'] = 'SubCategory';
		$data['permissions']['catalog/onlinepayment'] = 'OnlinePayment';
		$data['permissions']['catalog/brand'] = 'Brand';
		$data['permissions']['catalog/customer'] = 'Customer';
		$data['permissions']['catalog/customercredit'] = 'Customer Credit';
		$data['permissions']['catalog/waiter'] = 'Employee';
		$data['permissions']['catalog/kotmsg'] = 'KOT Message';
		$data['permissions']['catalog/kotmsgitem'] = 'KOT Message Item';
		$data['permissions']['catalog/kotmsgcat'] = 'KOT Message Cat';
		$data['permissions']['catalog/tax'] = 'Tax Master';
		$data['permissions']['catalog/hotellocation'] = 'Location';
		$data['permissions']['catalog/order'] = 'Order';
		$data['permissions']['catalog/orderqwerty'] = 'Order Qwerty';
		$data['permissions']['catalog/ordertab'] = 'Order Tablet';
		$data['permissions']['catalog/ordertouch'] = 'Order Touch';
		$data['permissions']['catalog/billview'] = 'Bill View';
		$data['permissions']['catalog/dayclose'] = 'Day Close';
		$data['permissions']['catalog/shiftclose'] = 'Shift Close';
		$data['permissions']['catalog/advance'] = 'Advance';
		$data['permissions']['catalog/billmerge'] = 'Bill Merge';
		$data['permissions']['catalog/currentsale'] = 'Current Sale';
		$data['permissions']['catalog/salestat'] = ' Sale Statistics';
		$data['permissions']['catalog/kottransfer'] = 'KOT Transfer';
		$data['permissions']['catalog/order_undo'] = 'Order Undo';
		$data['permissions']['catalog/settlement'] = 'Settlement';
		$data['permissions']['catalog/tablechange'] = 'Table Change';
		$data['permissions']['catalog/stock_report'] = 'Stock Report';
		$data['permissions']['catalog/shiftclose_report'] = 'shiftclose_report';
		$data['permissions']['catalog/delete_report'] = 'delete_report';
		$data['permissions']['catalog/complimentary'] = 'Complimentary';
		$data['permissions']['catalog/monthlyminireport'] = 'Monthly Mini Day Summary Report';
		$data['permissions']['catalog/tendering'] = 'Tendering';
		$data['permissions']['catalog/billmodifyreport'] = 'Bill Modify Report';
		$data['permissions']['catalog/complimentaryreport'] = 'Complimentary Report';
		$data['permissions']['catalog/reportbill'] = 'Bill wise Report';
		$data['permissions']['catalog/gstbill'] = 'GST Report';
		$data['permissions']['catalog/reportstock'] = 'Item Wise Report';
		$data['permissions']['catalog/reportdaywise'] = 'Day Wise Sales Report';
		$data['permissions']['catalog/cancelkot'] = 'Cancelled KOT Report';
		$data['permissions']['catalog/cancelbot'] = 'Cancelled BOT Report';
		$data['permissions']['catalog/cancelbill'] = 'Cancelled Bill Report';
		$data['permissions']['catalog/duplicatebill'] = 'Duplicate Bill Report';
		$data['permissions']['catalog/daysummaryreport'] = 'Day Summary Report';
		$data['permissions']['catalog/creditreport'] = 'Credit Report';
		$data['permissions']['catalog/advancereport'] = 'Advance Report';
		$data['permissions']['catalog/currentstockreport'] = 'Current Stock Report';
		$data['permissions']['catalog/currentstockreport_lsb'] = 'Current Stock Report LBS';
		$data['permissions']['catalog/purchaseorderreport'] = 'Purchase Order Report';
		$data['permissions']['catalog/purchasereportloose'] = 'Purchase Order Report Loose';
		$data['permissions']['catalog/purchaseorder'] = 'Purchase Order';
		$data['permissions']['catalog/purchaseoutward'] = 'Purchase Outward';
		$data['permissions']['catalog/stocktransfer'] = 'Stock Transfer';
		$data['permissions']['catalog/stocktransferliq'] = 'Stock Transfer Liquor';
		$data['permissions']['catalog/stocktransferloose'] = 'Stock Transfer Loose';
		$data['permissions']['catalog/stockmanufacturer'] = 'Manufacturer';
		$data['permissions']['catalog/stockreportliq'] = 'Stock Report Liquor';
		$data['permissions']['catalog/stockreportloose'] = 'Stock Report Loose';
		$data['permissions']['catalog/stockreportfood'] = 'Stock Report Food';
		$data['permissions']['catalog/storewisereport'] = 'Store Wise Report';
		$data['permissions']['catalog/itemwisereport'] = 'Item Wise Report';
		$data['permissions']['catalog/inwardreport'] = 'Inward Report';
		$data['permissions']['catalog/setting'] = 'Setting';
		$data['permissions']['catalog/trancatedata'] = 'Empty data';
		$data['permissions']['catalog/nckotreport'] = 'Nc Kot Report';
		$data['permissions']['catalog/westage_adjustment'] = 'Westage / Adjustment';
		$data['permissions']['catalog/captain_and_waiter_sale'] = 'Captain And Waiter Report';
		$data['permissions']['catalog/captain_and_waiter'] = 'Captain And Waiter Report';
		$data['permissions']['catalog/food_bar_report'] = 'Food Bar Report';
		$data['permissions']['catalog/selling_report'] = 'Selling Report';
		$data['permissions']['catalog/bom'] = 'BOM';
		$data['permissions']['catalog/subbom'] = 'Sub BOM';
		$data['permissions']['catalog/bomlazer'] = 'BOM Lazer';
		$data['permissions']['user/user'] = 'User';
		$data['permissions']['user/user_permission'] = 'User Permission';
		$data['permissions']['catalog/stockcategory'] = 'Stock Category';
		$data['permissions']['catalog/store_name'] = 'Store Name';
		$data['permissions']['catalog/stock_item'] = 'Stock Item';
		$data['permissions']['catalog/supplier'] = 'Supplier';
		$data['permissions']['catalog/purchaseentry'] = 'Purchase Entry';
		$data['permissions']['catalog/purchaseentryfood'] = 'Purchase Entry Food';
		$data['permissions']['catalog/orderapp'] = 'Order App';
		$data['permissions']['catalog/swiggyorder'] = 'Swiggy Order';
		$data['permissions']['catalog/bookingapp'] = 'Booking App';
		$data['permissions']['catalog/orderprocess'] = 'Order Entry Screen';
		$data['permissions']['catalog/orderprocess1'] = 'Order Display Screen';
		$data['permissions']['catalog/point_distribution'] = 'Point Distribution';
		$data['permissions']['catalog/expensereport'] = 'Expense Report';
		$data['permissions']['catalog/urbanpiper_order'] = 'Urbanpiper Order';
		$data['permissions']['catalog/urbanpiper_itemdata'] = 'Urbanpiper Order Item';
		$data['permissions']['catalog/urbanpier_store'] = 'Urbanpiper Store';
		$data['permissions']['catalog/test_print'] = 'Test print';

		
		$data['permissions']['catalog/nckot'] = 'NC KOT';

		$data['permissions']['catalog/webhook'] = 'Webhook';
		$data['permissions']['catalog/urbanpiperitem_show'] = 'Urbanpiperitem Show';
		$data['permissions']['catalog/urbanpiper_store_list'] = 'Urbanpiperitem Store Live view';
		$data['permissions']['catalog/urbanpiper_platform_list'] = 'Urbanpiper Platform Live view';
		$data['permissions']['catalog/urbanpiper_platform_item'] = 'Urbanpiper Platform Item status';



		if (isset($this->request->post['permission']['access'])) {
			$data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$data['access'] = $user_group_info['permission']['access'];
		} else {
			$data['access'] = array();
		}

		if (isset($this->request->post['permission']['modify'])) {
			$data['modify'] = $this->request->post['permission']['modify'];
		} elseif (isset($user_group_info['permission']['modify'])) {
			$data['modify'] = $user_group_info['permission']['modify'];
		} else {
			$data['modify'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/user_group_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('user/user');

		foreach ($this->request->post['selected'] as $user_group_id) {
			$user_total = $this->model_user_user->getTotalUsersByGroupId($user_group_id);

			if ($user_total) {
				$this->error['warning'] = sprintf($this->language->get('error_user'), $user_total);
			}
		}

		return !$this->error;
	}
}