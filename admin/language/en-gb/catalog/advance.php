<?php
// Heading
$_['heading_title']     = 'Advance order';

// Text
$_['text_success']      = 'Success: You have modified advance item!';
$_['text_list']         = 'Advance order List';
$_['text_add']          = 'Add Advance item';
$_['text_edit']         = 'Edit Advance Item';

// Column
$_['column_name']       = 'Advance Name';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';


// Error
$_['error_permission']  = 'Warning: You do not have permission to modify sports!';
$_['error_name']        = 'Sport Name must be between 3 and 64 characters!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport cannot be deleted as it is currently assigned to %s products!';