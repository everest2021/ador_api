<?php
// Heading
$_['heading_title']     = 'Notification Master';

// Text
$_['text_success']      = 'Success: You have modified Notification Master!';
$_['text_list']         = 'Notification List';
$_['text_add']          = 'Add Notification';
$_['text_edit']         = 'Edit Notification';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']       = 'Notification Name';
$_['column_rate']       = 'Rate';
$_['column_capacity']   = 'Capacity';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Notification Name';
$_['entry_rate']        = 'Rate';
$_['entry_capacity']        = 'Capacity';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the sport directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your Department.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Venue!';
$_['error_name']        = 'Notification Name must be between 3 and 64 characters!';
$_['error_rate']        = 'Enter Rate!';
$_['error_capacity']        = 'Enter Capacity!';

$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport cannot be deleted as it is currently assigned to %s products!';