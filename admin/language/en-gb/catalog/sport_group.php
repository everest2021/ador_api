<?php
// Heading
$_['heading_title']     = 'Sport Groups';

// Text
$_['text_success']      = 'Success: You have modified sport groups!';
$_['text_list']         = 'Sport Group List';
$_['text_add']          = 'Add Sport Group';
$_['text_edit']         = 'Edit Sport Group';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']       = 'Sport Group Name';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Sport Group Name';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the sport group directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your sport groups.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify sport groups!';
$_['error_name']        = 'Sport Group Name must be between 3 and 64 characters!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This sport group cannot be deleted as it is currently assigned to %s products!';