<?php
// Heading
$_['heading_title']          = 'Store';

// Text
$_['text_success']           = 'Success: You have modified Store!';
$_['text_list']              = 'Store List';
$_['text_add']               = 'Add Store';
$_['text_edit']              = 'Edit Store';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Store Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Store Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_store_nme']            = 'Store name';
$_['entry_store_add']              = 'Address <br>(use same address which are you included in setting)';
$_['entry_store_city']              = 'City';
$_['entry_store_zipcode']              = 'Zipcode';
$_['entry_store_contact_phone']              = 'Contact No.';
$_['entry_store_notification_no']              = 'Orders Notification Mobile No. <br>(use (,) comma for multiple notification)';
$_['entry_store_notification_email_no']              = 'Orders Notification Gmail <br>(use (,) comma for multiple notification)';
$_['entry_store_ref_id']              = 'Hotel Ref. ID';
$_['entry_minimum_pick_time']              = 'Minimum Pickup Time';
$_['entry_minimum_delivery_time']              = 'Minimum Delivery Time';
$_['entry_minimum_order_value']              = 'Minimum Order Value';
$_['entry_geo_longitude']              = 'Geo Longitude';
$_['entry_geo_latitude']              = 'Geo latitude';
$_['entry_ordering_enabled']              = 'Ordering Status';
$_['entry_ordering_timing']              = 'Day';
$_['entry_ordering_start_timing']              = 'Start Time';
$_['entry_ordering_end_timing']              = 'End Time';

$_['entry_packaging_charge']              = 'Packaging Charge';


$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Location';
$_['entry_shipping']         = 'Requires Shipping';
$_['entry_manufacturer']     = 'Manufacturer';
$_['entry_store']            = 'Stores';
$_['entry_date_available']   = 'Date Available';
$_['entry_quantity']         = 'Quantity';
$_['entry_minimum']          = 'Minimum Quantity';
$_['entry_stock_status']     = 'Out Of Stock Status';
$_['entry_price']            = 'Price';
$_['entry_tax_class']        = 'Tax Class';
$_['entry_points']           = 'Points';
$_['entry_option_points']    = 'Points';
$_['entry_subtract']         = 'Subtract Stock';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_customer_group']   = 'Customer Group';
$_['entry_date_start']       = 'Date Start';
$_['entry_date_end']         = 'Date End';
$_['entry_priority']         = 'Priority';
$_['entry_attribute']        = 'Attribute';
$_['entry_attribute_group']  = 'Attribute Group';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option Value';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_filter']           = 'Filters';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Related Store';
$_['entry_tag']          	 = 'Store Tags';
$_['entry_reward']           = 'Reward Points';
$_['entry_layout']           = 'Layout Override';
$_['entry_recurring']        = 'Recurring Profile';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Store Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Autocomplete)';
$_['help_minimum']           = 'Force a minimum ordered amount';
$_['help_stock_status']      = 'Status shown when a Store is out of stock';
$_['help_points']            = 'Number of points needed to buy this item. If you don\'t want this Store to be purchased with points leave as 0.';
$_['help_category']          = '(Autocomplete)';
$_['help_filter']            = '(Autocomplete)';
$_['help_download']          = '(Autocomplete)';
$_['help_related']           = '(Autocomplete)';
$_['help_tag']              = 'Comma separated';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Store!';
$_['error_name']             = 'Store Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Store Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
