<?php
// Heading
$_['heading_title']      = 'Reception';

// Text
$_['text_success']       = 'Thanks For Your Reception We Consider In Further!';
$_['text_list']          = 'Reception List';
$_['text_add']           = 'Add Reception';
$_['text_edit']          = 'Reception';
$_['text_default']       = 'Default';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';

// Column
$_['column_name']        = 'Reception Name';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Reception Name';
$_['entry_store']        = 'Stores';
$_['entry_keyword']      = 'SEO URL';
$_['entry_image']        = 'Image';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_type']         = 'Type';

// Help
$_['help_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify division!';
$_['error_name']         = 'Reception Name must be between 2 and 64 characters!';
$_['error_keyword']      = 'SEO URL already in use!';
$_['error_product']      = 'Warning: This Reception cannot be deleted as it is currently assigned to %s products!';
