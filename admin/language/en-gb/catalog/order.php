<?php
// Heading
$_['heading_title']     = 'Order';

// Text
$_['text_success']      = 'Success: You have modified order!';
$_['text_list']         = 'Order List';
$_['text_add']          = 'Add Order';
$_['text_edit']         = 'Edit Order';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Column
$_['column_name']       = 'Order Name';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Order Name';
$_['entry_filename']    = 'Filename';
$_['entry_mask']        = 'Mask';

// Help
$_['help_filename']     = 'You can upload via the upload button or use FTP to upload to the order directory and enter the details below.';
$_['help_mask']         = 'It is recommended that the filename and the mask are different to stop people trying to directly link to your orders.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify orders!';
$_['error_name']        = 'Order Name must be between 3 and 64 characters!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 128 characters!';
$_['error_exists']      = 'File does not exist!';
$_['error_mask']        = 'Mask must be between 3 and 128 characters!';
$_['error_filetype']    = 'Invalid file type!';
$_['error_product']     = 'Warning: This order cannot be deleted as it is currently assigned to %s products!';