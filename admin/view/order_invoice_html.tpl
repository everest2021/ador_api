<!DOCTYPE html>
<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
<head>
<meta charset="UTF-8" />
<!-- <title><?php echo $heading_title; ?></title> -->
</head>
<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
	<h3><br><?php echo date('d/m/Y'); ?></h3>
	<?php date_default_timezone_set("Asia/Kolkata");?>
	<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
	<center><h2><b>Day Summary Report</b></h2></center>
	<h3>From : <?php echo $startdate; ?></h3>
	<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
		<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
		<tr>
			<th style="text-align: center;">Login Summary</th>
			<th style="text-align: right;" colspan="4">Amount</th>
		</tr>
		<?php $usertotal = 0; ?>
		<?php if($orderdataarrays != array()){ ?>
			<?php foreach($orderdataarrays as $key => $value) { ?>
				<tr>
					<td><b>User :<?php echo $value['orderdata']['login_id']?></b></td>
					<td><b><?php echo $value['orderdata']['login_name']?></b></td>
					<td>T.Sale</td>
					<td><?php echo $value['orderdata']['netsale'] ?></td>
				</tr>
				<tr>
					<td>Usettle Bill Amount</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['pendingamount'] ?></td>
				</tr>
				<tr>
					<td>Cash</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['total_cash']?></td>
				</tr>
				<tr>
					<td>Credit Card</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['total_card']?></td>
				</tr>
				<tr>
					<td>Online</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['total_pay_online']?></td>
				</tr>
				<tr>
					<td>on/Ac</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['onac']?></td>
				</tr>
				<tr>
					<td>Meal Pass</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['total_mealpass']?></td>
				</tr>
				<tr>
					<td>Pass</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['total_pass']?></td>
				</tr>
				<!-- <tr>
					<td>Pending table</td>
					<td></td>
					<td><?php echo $value['orderdata']['pendingbill'] ?></td>
					<td><?php echo $value['orderdata']['pendingamount'] ?></td>
				</tr> -->
				<tr>
					<td>Complement Bill</td>
					<td></td>
					<td><?php echo $value['orderdata']['complimentarycount'] ?></td>
					<td><?php echo $value['orderdata']['complimentarybill'] ?></td>
				</tr>
				<tr>
					<td>Canceled Bill</td>
					<td></td>
					<td><?php echo $value['orderdata']['cancelledcount'] ?></td>
					<td><?php echo $value['orderdata']['cancelledbill'] ?></td>
				</tr>
				<tr>
					<td>Canceled Kot</td>
					<td></td>
					<td><?php echo $value['orderdata']['cancelkot'] ?></td>
					<td><?php echo $value['orderdata']['cancelamountkot'] ?></td>
				</tr>
				<tr>
					<td>Canceled Bot</td>
					<td></td>
					<td><?php echo $value['orderdata']['cancelbot'] ?></td>
					<td><?php echo $value['orderdata']['cancelamountbot'] ?></td>
				</tr>
				<tr>
					<td>NC KOT</td>
					<td></td>
					<td><?php echo $value['orderdata']['totalnc'] ?></td>
					<td><?php echo $value['orderdata']['totalamountnc'] ?></td>
				</tr>
				<tr>
					<td>DUPLICATE BILL</td>
					<td></td>
					<td><?php echo $value['orderdata']['duplicatecount'] ?></td>
					<td><?php echo $value['orderdata']['duplicate'] ?></td>
				</tr>
				<tr>
					<td>Disc Before Bill</td>
					<td></td>
					<td><?php echo $value['orderdata']['discountbillcount'] ?></td>
					<td><?php echo $value['orderdata']['discountamount'] ?></td>
				</tr>
				<tr>
					<td>DUPLICATE KOT</td>
					<td></td>
					<td></td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>Disc After Bill</td>
					<td></td>
					<td></td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>Advance Amount (-)</td>
					<td></td>
					<td></td>
					<td><?php echo $value['orderdata']['advanceamount'] ?></td>
				</tr>
				<?php
					$usertotal = $usertotal + $value['orderdata']['total_cash'] + $value['orderdata']['total_card']+ $value['orderdata']['total_pay_online'] + $value['orderdata']['onac'] + $value['orderdata']['pendingamount'] + $value['orderdata']['advanceamount'];
				?>
			<?php } ?>
			<tr>
				<td><b>All User Totals</b></td>
				<td></td>
				<td></td>
				<td><b><?php echo $usertotal ?></b></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td><b>Table Group</b></td>
				<td></td>
				<td></td>
				<td><b>Amount</b></td>
			</tr>
			<?php foreach($orderlocdatas as $locdata){ ?>
				<tr>
					<td><?php echo $locdata['location'] ?></td>
					<td></td>
					<td></td>
					<td><?php echo $locdata['netsale'] ?></td>
				</tr>
			<?php } ?>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Sub Total</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totalnetsale'] ?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>KKC Amt(+)</td>
				<td></td>
				<td></td>
				<td>0.00</td>
			</tr>
			<tr>
				<td>SBC Amt(+)</td>
				<td></td>
				<td></td>
				<td>0.00</td>
			</tr>
			<tr>
				<td>S-Chrg Amt(+)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totalscharge'] ?></td>
			</tr>
			<tr>
				<td>Vat(+)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totalvat'] ?></td>
			</tr>
			<tr>
				<td>Gst(+)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totalgst'] ?></td>
			</tr>
			<tr>
				<td>R-Off Amt(+)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totalroundoff'] ?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Discount Amt(-)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['totaldiscount'] ?></td>
			</tr>
			<tr>
				<td>P-Discount Amt(-)</td>
				<td></td>
				<td></td>
				<td>0.00</td>
			</tr>
			<tr>
				<td>Cancel Bill Amt(-)</td>
				<td></td>
				<td></td>
				<td>0.00</td>
			</tr>
			<tr>
				<td>Advance. Amt(-)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['advance'] ?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Gross Total</td>
				<td></td>
				<td></td>
				<td><?php echo $orderlocdatasamt['nettotalamt'] ?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td><b>Payment Summary</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Cash</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['totalcash']?></td>
			</tr>
			<tr>
				<td>Credit Card</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['totalcard']?></td>
			</tr>
			<tr>
				<td>Online</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['totalpayonline']?></td>
			</tr>
			<tr>
				<td>On A/c</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['onac']?></td>
			</tr>
			<tr>
				<td>Meal Pass</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['mealpass']?></td>
			</tr>
			<tr>
				<td>Pass</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['pass']?></td>
			</tr>
			<tr>
				<td>Room Service</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['room']?></td>
			</tr>
			<tr>
				<td>MSR Card</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['msr']?></td>
			</tr>
			<tr>
				<td>Advance Amount (-)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['advanceamt']?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Total</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['total']?></td>
			</tr>
			<tr>
				<td>Card tip</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['tip_amount']?></td>
			</tr>
			<tr>
				<td>Advance Amt(+)</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['advance']?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td><b>Grand Total</b></td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['totalpaysumm']?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td><b>Online Payment Summary</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				<tr>
					<td>Swiggy</td>
					<td></td>
					<td></td>
					<td><?php echo $onlineorderamt['swiggy_total']?></td>
				</tr>

				<tr>
					<td>Zomato</td>
					<td></td>
					<td></td>
					<td><?php echo $onlineorderamt['zomato_total']?></td>
				</tr>

				<tr>
					<td>Online</td>
					<td></td>
					<td></td>
					<td><?php echo $onlineorderamt['online_total']?></td>
				</tr>

				<tr>
					<td>Uber</td>
					<td></td>
					<td></td>
					<td><?php echo $onlineorderamt['uber_total']?></td>
				</tr>

				<tr>
					<td><b>Grand Total</b></td>
					<td></td>
					<td></td>
					<td><b><?php echo $onlineorderamt['g_tot']?></b></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td><b>Tax Details</b></td>
				<td></td>
				<td><b>Net Amt</b></td>
				<td><b>Tax Amt</b></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<?php foreach($ordertaxamt['liqtax'] as $value) { ?>
				<tr>
					<td>Vat</td>
					<td><?php echo $value['tax1'] ?> % On</td>
					<td><?php echo $value['amt'] ?> is</td>
					<td><?php echo $value['tax1_value'] ?></td>
				</tr>
			<?php } ?>
				<tr>
					<td><b>Total Vat</b></td>
					<td></td>
					<td><b><?php echo $ordertaxamt['liqnetamt'] ?></b></td>
					<td><b><?php echo $ordertaxamt['taxvalliq'] ?></b></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<?php foreach($ordertaxamt['foodtax'] as $value) { ?>
				<tr>
					<td>GST</td>
					<td><?php echo $value['tax1'] ?> % On</td>
					<td><?php echo $value['amt'] ?> is</td>
					<td><?php echo $value['tax1_value'] ?></td>
				</tr>
			<?php } ?>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td>CGST</td>
					<td></td>
					<td></td>
					<td><?php echo $ordertaxamt['taxvalfood']/2 ?></td>
				</tr>
				<tr>
					<td>SGST</td>
					<td></td>
					<td></td>
					<td><?php echo $ordertaxamt['taxvalfood']/2 ?></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td><b>Total Gst</b></td>
					<td></td>
					<td><b><?php echo $ordertaxamt['foodnetamt'] ?></b></td>
					<td><b><?php echo $ordertaxamt['taxvalfood'] ?></b></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td><b>Category</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td>Food</td>
					<td></td>
					<td></td>
					<td><?php echo $catsubs['foodcat']?></td>
				</tr>
				<tr>
					<td>Liquor</td>
					<td></td>
					<td></td>
					<td><?php echo $catsubs['liqcat'] ?></td>
				</tr>
				<tr>
					<td>Total</td>
					<td></td>
					<td></td>
					<td><?php echo $catsubs['foodcat'] + $catsubs['liqcat'] ?></td>
				</tr>
				<tr>
					<td>Discount</td>
					<td></td>
					<td></td>
					<td><?php echo $catsubs['cat_fdis'] + $catsubs['cat_ldis'] ?></td>
				</tr>
				<tr>
					<td><b>Category Net Total</b></td>
					<td></td>
					<td></td>
					<td><b><?php echo ($catsubs['foodcat'] + $catsubs['liqcat']) - ($catsubs['cat_fdis'] + $catsubs['cat_ldis']) ?></b></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<tr>
					<td><b>Sub Category</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
				<?php foreach($catsubs['subcat'] as $key => $value) { ?>
					<tr>
						<td><?php echo $value['name'] ?></td>
						<td></td>
						<td></td>
						<td><?php echo $value['amount'] ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td><b>Total<b></td>
					<td></td>
					<td></td>
					<td><?php echo $catsubs['totalamt']?></td>
				</tr>
				<tr>
					<td>Discount</td>
					<td></td>
					<td></td>
					<td><?php echo  $catsubs['discountamt'] ?></td>
				</tr>
				<tr>
					<td><b>Sub Category Net Total</b></td>
					<td></td>
					<td></td>
					<td><b><?php echo $catsubs['totalamt'] - $catsubs['discountamt']?></b></td>
				</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>Last Bill No</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['lastbill'] ?></td>
				<td></td>
			</tr>
			<tr>
				<td>Last Kot No</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['lastkot'] ?></td>
				<td></td>
			</tr>
			<tr>
				<td>Last Bot No</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['lastbot'] ?></td>
				<td></td>
			</tr>
			<tr>
				<td>Unsettle Bill Amt</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['pendingbill'] ?></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['pendingamount'] ?></td>
			</tr>
			<tr>
				<td>Duplicate Bill</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['duplicatecount'] ?></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['duplicate'] ?></td>
			</tr>
			<tr>
				<td>Pending Table</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['pendingbill'] ?></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['pendingamount'] ?></td>
			</tr>
			<tr>
				<td>Compliment Bill</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['complimentarycount'] ?></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['complimentarybill'] ?></td>
			</tr>
			<tr>
				<td>Cancelled Bill</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelledcount'] ?></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelledbill'] ?></td>
			</tr>
			<tr>
				<td>Cancelled Kot</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelkot'] ?></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountkot'] ?></td>
			</tr>
			<tr>
				<td>Cancelled Bot</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelbot'] ?></td>
				<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountbot'] ?></td>
			</tr>
			<tr>
				<td>NC KOT</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['totalnc'] ?></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['totalamountnc'] ?></td>
			</tr>
			<tr>
				<td>CHANGE BILL</td>
				<td></td>
				<td><?php echo $NumberOfbillmodify ?></td>
				<td><?php echo $totalbillamount ?></td>
			</tr>
			<tr>
				<td>Disc Before Bill</td>
				<td></td>
				<td>0.00</td>
				<td>0.00</td>
			</tr>
			<tr>
				<td>Disc after Bill</td>
				<td></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['discountbillcount'] ?></td>
				<td><?php echo $orderdatalastarray['orderdatalast']['discountamount'] ?></td>
			</tr>
			<tr><td colspan="5" height="35"></td></tr>
			<tr>
				<td>CASH DROVEREST</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>OPENING BALANCE</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>CASH SALE</td>
				<td></td>
				<td></td>
				<td><?php echo $orderpaymentamt['totalcash']?></td>
			</tr>
			<tr>
				<td>PAID IN</td>
				<td></td>
				<td></td>
				<td>0</td>
			</tr>
			<tr>
				<td>PAID OUT</td>
				<td></td>
				<td></td>
				<td>0</td>
			</tr>
			<tr>
				<td>TOTAL BALANCE IN DROVER</td>
				<td></td>
				<td></td>
				<td>0</td>
			</tr>
			<tr>
				<td>TIP</td>
				<td></td>
				<td></td>
				<td><?php echo $tip ?></td>
			</tr>
			<tr>
				<td>No. Of Persons</td>
				<td></td>
				<td></td>
				<td><?php echo $person ?></td>
			</tr>
		</table>
		<br>
	<?php } ?>
<center><h3><b>End of Report</b></h3></center>
</div></body></html>