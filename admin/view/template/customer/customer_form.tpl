<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="row">
                <div style="display: none;" class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="address">
                    <li class="active"><a href="#tab-customer" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                  </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab-customer">
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-school"><?php echo 'शाळेचे नाव / School Name'; ?></label>
                        <div class="col-sm-10">
                          <select name="school" id="input-school" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($school_names as $skey => $svalue) { ?>
                              <?php if ($skey == $school) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_school) { ?>
                          <div class="text-danger"><?php echo $error_school; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-sport_type"><?php echo 'खेळ प्रकार / Sport Type'; ?></label>
                        <div class="col-sm-10">
                          <select name="sport_type" id="input-sport_type" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($sport_types as $skey => $svalue) { ?>
                              <?php if ($skey == $sport_type) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_sport_type) { ?>
                          <div class="text-danger"><?php echo $error_sport_type; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-participant_type"><?php echo 'सहभागी प्रकार / Participant Type'; ?></label>
                        <div class="col-sm-10">
                          <select name="participant_type" id="input-participant_type" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($participant_types as $skey => $svalue) { ?>
                              <?php if ($skey == $participant_type) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_participant_type) { ?>
                          <div class="text-danger"><?php echo $error_participant_type; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required individual_class">
                        <label class="col-sm-2 control-label" for="input-firstname"><?php echo 'पहिले नाव / First Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                          <?php if ($error_firstname) { ?>
                          <div class="text-danger"><?php echo $error_firstname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group individual_class">
                        <label class="col-sm-2 control-label" for="input-firstname"><?php echo 'मधले नाव / Middle Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="middlename" value="<?php echo $middlename; ?>" placeholder="<?php echo 'Middle Name'; ?>" id="input-middlename" class="form-control" />
                          <?php if ($error_middlename) { ?>
                          <div class="text-danger"><?php echo $error_middlename; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required individual_class">
                        <label class="col-sm-2 control-label" for="input-lastname"><?php echo 'आडनाव / Last Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                          <?php if ($error_lastname) { ?>
                          <div class="text-danger"><?php echo $error_lastname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group individual_class required">
                        <label class="col-sm-2 control-label" for="input-gender"><?php echo 'लिंग / Gender'; ?></label>
                        <div class="col-sm-10">
                          <select name="gender" id="input-gender" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($genders as $skey => $svalue) { ?>
                              <?php if ($skey == $gender) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_gender) { ?>
                          <div class="text-danger"><?php echo $error_gender; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                       <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-telephone"><?php echo 'मोबाईल / Mobile'; ?></label>
                        <div class="col-sm-10">
                          <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo 'Mobile'; ?>" id="input-telephone" class="form-control" />
                          <?php if ($error_telephone) { ?>
                          <div class="text-danger"><?php echo $error_telephone; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group email_div">
                        <label class="col-sm-2 control-label" for="input-email"><?php echo 'ईमेल / Email'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo 'Email'; ?>" id="input-email" class="form-control" />
                          <?php if ($error_email) { ?>
                          <div class="text-danger"><?php echo $error_email; ?></div>
                          <?php } ?>
                          <div class="" id="error_email" style="display: none;"></div>
                        </div>
                      </div>
                      <div class="form-group required aadhar_card_number_div">
                        <label class="col-sm-2 control-label" for="input-aadhar_card_number"><?php echo 'आधार कार्ड क्रमांक / Aadhar Card Number'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="aadhar_card_number" value="<?php echo $aadhar_card_number; ?>" placeholder="<?php echo 'Aadhar Card Number'; ?>" id="input-aadhar_card_number" class="form-control" />
                          <?php if ($error_aadhar_card_number) { ?>
                          <div class="text-danger"><?php echo $error_aadhar_card_number; ?></div>
                          <?php } ?>
                          <div class="" id="error_aadhar_card_number" style="display: none;"></div>
                        </div>
                      </div>
                      <div class="form-group required individual_class">
                        <label class="col-sm-2 control-label" for="input-dob"><?php echo 'जन्म तारीख / Date Of Birth'; ?></label>
                        <div class="col-sm-2">
                          दिवस / Day
                          <select name="day" id="input-day" class="form-control age_class">
                            <option value=""><?php echo '---Select Day---'; ?></option>
                            <?php foreach ($days as $skey => $svalue) { ?>
                              <?php if ($skey == $day) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_day) { ?>
                          <div class="text-danger"><?php echo $error_day; ?></div>
                          <?php } ?>
                          <!-- <input type="text" name="dob" value="<?php echo $dob; ?>" placeholder="<?php echo 'Date Of Birth'; ?>" id="input-dob" class="form-control date" /> -->
                          <!-- <?php /* if ($error_dob) { ?>
                          <div class="text-danger"><?php echo $error_dob; ?></div>
                          <?php } */ ?> -->
                        </div>
                        <div class="col-sm-2">
                          महिना / Month
                          <select name="month" id="input-month" class="form-control age_class">
                            <option value=""><?php echo '---Select Month---'; ?></option>
                            <?php foreach ($months as $skey => $svalue) { ?>
                              <?php if ($skey == $month) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_month) { ?>
                          <div class="text-danger"><?php echo $error_month; ?></div>
                          <?php } ?>
                        </div>
                        <div class="col-sm-2">
                          वर्ष / Year
                          <select name="year" id="input-year" class="form-control age_class">
                            <option value=""><?php echo '---Select Year---'; ?></option>
                            <?php foreach ($years as $skey => $svalue) { ?>
                              <?php if ($skey == $year) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_year) { ?>
                          <div class="text-danger"><?php echo $error_year; ?></div>
                          <?php } ?>
                        </div>
                        <div class="col-sm-2">
                          वय / Age
                          <input readonly="readonly" type="text" name="age" value="<?php echo $age; ?>" placeholder="<?php echo 'Age'; ?>" id="input-age" class="form-control" />
                          <?php if ($error_age) { ?>
                          <div class="text-danger"><?php echo $error_age; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div style="display: none;" class="form-group required">
                        <label class="col-sm-2 control-label" for="input-standard"><?php echo 'Standrard'; ?></label>
                        <div class="col-sm-10">
                          <select name="standard" id="input-standard" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($standards as $skey => $svalue) { ?>
                              <?php if ($skey == $standard) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_standard) { ?>
                          <div class="text-danger"><?php echo $error_standard; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-group_name"><?php echo 'गटाचे नाव / Group Name'; ?></label>
                        <div class="col-sm-10">
                          <?php if($participant_type == 1 || $participant_type == ''){ ?>
                            <select disabled="disabled" name="group_id" id="input-group_id" class="form-control">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($sports_groups as $skey => $svalue) { ?>
                                <?php if ($skey == $group_id) { ?>
                                  <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                            <input type="hidden" name="group_name" value="<?php echo $group_name; ?>" placeholder="<?php echo 'Group Name'; ?>" id="input-group_name" class="form-control" />
                            <input type="hidden" name="group_id" value="<?php echo $group_id; ?>" id="input-text_group_id" class="form-control" />
                          <?php } else { ?>
                            <select name="group_id" id="input-group_id" class="form-control">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($sports_groups as $skey => $svalue) { ?>
                                <?php if ($skey == $group_id) { ?>
                                  <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                            <input readonly="readonly" type="hidden" name="group_name" value="<?php echo $group_name; ?>" placeholder="<?php echo 'Group Name'; ?>" id="input-group_name" class="form-control" />
                            <input type="hidden" name="group_id" value="<?php echo $group_id; ?>" id="input-text_group_id" class="form-control" />
                          <?php } ?>
                          <?php if ($error_group_name) { ?>
                          <div class="text-danger"><?php echo $error_group_name; ?></div>
                          <?php } ?>
                          <div class="" id="error_group_name" style="display: none;"></div>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-sport"><?php echo 'खेळ / Sports'; ?></label>
                        <div class="col-sm-10">
                          <?php /* if($sport_type == '1'){ ?>
                            <select name="sport" id="input-sport" class="form-control">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($kala_sports as $skey => $svalue) { ?>
                                <?php if ($skey == $sport) { ?>
                                  <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                          <?php } elseif($sport_type == '2') { ?>
                            <select name="sport" id="input-sport" class="form-control">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($krida_sports as $skey => $svalue) { ?>
                                <?php if ($skey == $sport) { ?>
                                  <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                          <?php } else { */ ?>
                            <select name="sport" id="input-sport" class="form-control">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($sports as $skey => $svalue) { ?>
                                <?php if ($skey == $sport) { ?>
                                  <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                          <?php //} ?>
                          <input type="hidden" name="partner_required" value="<?php echo $partner_required; ?>" id="input-partner_required" class="form-control" />
                          <?php if ($error_sport) { ?>
                          <div class="text-danger"><?php echo $error_sport; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required group_class">
                        <label class="col-sm-2 control-label" for="input-weight"><?php echo 'जास्तीत जास्त वजन / Max Weight'; ?></label>
                        <div class="col-sm-10">
                          <input readonly="readonly" type="text" name="weight" value="<?php echo $weight; ?>" placeholder="<?php echo 'Max Weight'; ?>" id="input-weight" class="form-control" />
                          <?php if ($error_weight) { ?>
                          <div class="text-danger"><?php echo $error_weight; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required partner_individual_class" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-partner_firstname"><?php echo 'पहिले नाव / Partner First Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="partner_firstname" value="<?php echo $partner_firstname; ?>" placeholder="<?php echo 'Partner First Name'; ?>" id="input-partner_firstname" class="form-control" />
                          <?php if ($error_partner_firstname) { ?>
                          <div class="text-danger"><?php echo $error_partner_firstname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group partner_individual_class" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-partner_middlename"><?php echo 'मधले नाव / Partner Middle Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="partner_middlename" value="<?php echo $partner_middlename; ?>" placeholder="<?php echo 'Partner Middle Name'; ?>" id="input-partner_middlename" class="form-control" />
                          <?php if ($error_partner_middlename) { ?>
                          <div class="text-danger"><?php echo $error_partner_middlename; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required partner_individual_class" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-partner_lastname"><?php echo 'आडनाव / Partner Last Name'; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="partner_lastname" value="<?php echo $partner_lastname; ?>" placeholder="<?php echo 'Partner Last Name'; ?>" id="input-partner_lastname" class="form-control" />
                          <?php if ($error_partner_lastname) { ?>
                          <div class="text-danger"><?php echo $error_partner_lastname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required partner_individual_class" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-dob"><?php echo 'जन्म तारीख / Partner Date Of Birth'; ?></label>
                        <div class="col-sm-2">
                          दिवस / Day
                          <select name="partner_day" id="input-partner_day" class="form-control partner_age_class">
                            <option value=""><?php echo '---Select Day---'; ?></option>
                            <?php foreach ($days as $skey => $svalue) { ?>
                              <?php if ($skey == $partner_day) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_partner_day) { ?>
                          <div class="text-danger"><?php echo $error_partner_day; ?></div>
                          <?php } ?>
                          <!-- <input type="text" name="dob" value="<?php echo $dob; ?>" placeholder="<?php echo 'Date Of Birth'; ?>" id="input-dob" class="form-control date" /> -->
                          <!-- <?php /* if ($error_dob) { ?>
                          <div class="text-danger"><?php echo $error_dob; ?></div>
                          <?php } */ ?> -->
                        </div>
                        <div class="col-sm-2">
                          महिना / Month
                          <select name="partner_month" id="input-partner_month" class="form-control partner_age_class">
                            <option value=""><?php echo '---Select Month---'; ?></option>
                            <?php foreach ($months as $skey => $svalue) { ?>
                              <?php if ($skey == $partner_month) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_partner_month) { ?>
                          <div class="text-danger"><?php echo $error_partner_month; ?></div>
                          <?php } ?>
                        </div>
                        <div class="col-sm-2">
                          वर्ष / Year
                          <select name="partner_year" id="input-partner_year" class="form-control partner_age_class">
                            <option value=""><?php echo '---Select Year---'; ?></option>
                            <?php foreach ($years as $skey => $svalue) { ?>
                              <?php if ($skey == $partner_year) { ?>
                                <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                          <?php if ($error_partner_year) { ?>
                          <div class="text-danger"><?php echo $error_partner_year; ?></div>
                          <?php } ?>
                        </div>
                        <div class="col-sm-2">
                          वय / Age
                          <input readonly="readonly" type="text" name="partner_age" value="<?php echo $partner_age; ?>" placeholder="<?php echo 'Partner Age'; ?>" id="input-partner_age" class="form-control" />
                          <?php if ($error_partner_age) { ?>
                          <div class="text-danger"><?php echo $error_partner_age; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group group_class">
                        <label class="col-sm-10 control-label" for="input-participant_data" style="margin-left:30px;">
                          <span data-toggle="tooltip" title="<?php echo 'Participant Data'; ?>" style="float: left;">
                            <?php echo 'खेळाडू डेटा / Participant Data'; ?>
                          </span>
                          <?php if (isset($error_po_datas['warning'])) { ?>
                            <div class="text-danger" style="text-align: center;font-size: 15px;font-weight: bold;"><?php echo $error_po_datas['warning']; ?></div> 
                          <?php } ?>
                        </label>
                      </div>
                      <div class="form-group group_class">
                        <table style="width:100%;margin-left:3%;margin-top:18px;" class="table table-bordered table-hover" id="po_content">
                          <thead>
                            <tr>
                              <td style="text-align: center;width:10%;" colspan="3">खेळाडू नाव / Participant Name</td>
                              <td style="text-align: center;width:5%;">आधार कार्ड क्रमांक / Aadhar Card Number</td>
                              <td style="text-align: center;width:26%;" colspan="3">जन्म तारीख / Date Of Birth</td>
                              <td style="text-align: center;width:5%;">वय / Age</td>
                              <td style="text-align: center;width:4%;">वजन / Weight</td>
                            </tr>
                            <tr>
                              <td style="text-align: center;">पहिले नाव / First Name</td>
                              <td style="text-align: center;">मधले नाव / Middle Name</td>
                              <td style="text-align: center;">आडनाव / Last Name</td>
                              <td style="text-align: center;"></td>
                              <td style="text-align: center;">दिवस / Day</td>
                              <td style="text-align: center;">महिना / Month</td>
                              <td style="text-align: center;">वर्ष / Year</td>
                              <td style="text-align: center;"></td>
                              <td style="text-align: center;"></td>
                            </tr>
                          </thead>
                          <?php $extra_field_row = 0; ?>
                          <?php 
                            $total_weight = 0;
                          ?>
                          <?php foreach ($po_datas as $pkey => $po_data) { ?>
                            <tbody id="po_contents_row<?php echo $extra_field_row; ?>" class="po_contents_row">  
                              <tr>
                                <td style="text-align: left;">
                                  <input type="text" id="input_firstname-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][firstname]" value="<?php echo $po_data['firstname']; ?>"  />
                                  <?php if (isset($error_po_datas[$extra_field_row]['firstname'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['firstname']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <input type="text" id="input_middlename-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][middlename]" value="<?php echo $po_data['middlename']; ?>"  />
                                  <?php if (isset($error_po_datas[$extra_field_row]['middlename'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['middlename']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <input type="text" id="input_lastname-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][lastname]" value="<?php echo $po_data['lastname']; ?>"  />
                                  <?php if (isset($error_po_datas[$extra_field_row]['lastname'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['lastname']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <input type="text" id="input_aadhar_card_number-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][aadhar_card_number]" value="<?php echo $po_data['aadhar_card_number']; ?>"  />
                                  <?php if (isset($error_po_datas[$extra_field_row]['aadhar_card_number'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['aadhar_card_number']; ?></div>
                                  <?php } ?>
                                  <div class="" id="aadhar_card_number-<?php echo $extra_field_row; ?>" style="display: none;"></div>
                                </td>
                                <td style="text-align: left;">
                                  <select name="po_datas[<?php echo $extra_field_row ?>][day]" id="input_day-<?php echo $extra_field_row; ?>" class="form-control group_age_class">
                                    <option value=""><?php echo '---Select Day---'; ?></option>
                                    <?php foreach ($days as $skey => $svalue) { ?>
                                      <?php if (isset($po_data['day']) && $skey == $po_data['day']) { ?>
                                        <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                      <?php } else { ?>
                                        <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <?php if (isset($error_po_datas[$extra_field_row]['day'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['day']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <select name="po_datas[<?php echo $extra_field_row ?>][month]" id="input_month-<?php echo $extra_field_row; ?>" class="form-control group_age_class">
                                    <option value=""><?php echo '---Select Month---'; ?></option>
                                    <?php foreach ($months as $skey => $svalue) { ?>
                                      <?php if (isset($po_data['month']) && $skey == $po_data['month']) { ?>
                                        <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                      <?php } else { ?>
                                        <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <?php if (isset($error_po_datas[$extra_field_row]['month'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['month']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <select name="po_datas[<?php echo $extra_field_row ?>][year]" id="input_year-<?php echo $extra_field_row; ?>" class="form-control group_age_class">
                                    <option value=""><?php echo '---Select Year---'; ?></option>
                                    <?php foreach ($years as $skey => $svalue) { ?>
                                      <?php if (isset($po_data['year']) && $skey == $po_data['year']) { ?>
                                        <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                                      <?php } else { ?>
                                        <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <?php if (isset($error_po_datas[$extra_field_row]['year'])) { ?>
                                    <div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['year']; ?></div>
                                  <?php } ?>
                                </td>
                                <td style="text-align: left;">
                                  <input readonly="readonly" type="text" id="input_age-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][age]" value="<?php echo $po_data['age']; ?>" class="form-control" />
                                </td>
                                <td style="text-align: left;">
                                  <input type="text" id="input_weight-<?php echo $extra_field_row; ?>" name="po_datas[<?php echo $extra_field_row ?>][weight]" value="<?php echo $po_data['weight']; ?>" class="form-control group_weight_class" />
                                </td>
                              </tr>
                            </tbody>
                            <?php
                              $total_weight = $total_weight + $po_data['weight'];
                            ?>
                          <?php $extra_field_row++; ?>
                          <?php } ?>
                          <tfoot>
                            <tr> 
                              <td style="text-align: right;" colspan="7">Total</td>
                              <td style="text-align: right;" id="total_weight_td">
                                <?php echo $total_weight; ?>
                                <?php if ($error_weight) { ?>
                                  <div class="text-danger"><?php echo $error_weight; ?></div>
                                <?php } ?>
                              </td>
                            </tr>
                          </tfoot>
                          <input type="hidden" id="extra_field_row" name="extra_field_row" value="<?php echo $extra_field_row; ?>" />
                          <input type="hidden" id="total_weight" name="total_weight" value="<?php echo $total_weight; ?>" />
                        </table>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo 'स्थिती / ' . $entry_status; ?></label>
                        <div class="col-sm-10">
                          <select name="status" id="input-status" class="form-control">
                            <?php if ($status == '1') { ?>
                            <option value="1" selected="selected"><?php echo 'Active'; ?></option>
                            <option value="2"><?php echo 'Inactive'; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo 'Active'; ?></option>
                            <option value="2" selected="selected"><?php echo 'Inactive'; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $('.age_class').on('change', function() {
      year = $('#input-year').val();
      month = $('#input-month').val();
      day = $('#input-day').val();
      dob = day+'/'+month+'/'+year;
      if(year != '' && month != '' && year != ''){
        var now = new Date();
        var birthdate = dob.split("/");
        var born = new Date(birthdate[2], birthdate[1]-1, birthdate[0]);
        age=get_age(born,now);
        $('#input-age').val(age)
      }
    });

    $('.partner_age_class').on('change', function() {
      year = $('#input-partner_year').val();
      month = $('#input-partner_month').val();
      day = $('#input-partner_day').val();
      dob = day+'/'+month+'/'+year;
      if(year != '' && month != '' && year != ''){
        var now = new Date();
        var birthdate = dob.split("/");
        var born = new Date(birthdate[2], birthdate[1]-1, birthdate[0]);
        age=get_age(born,now);
        $('#input-partner_age').val(age)
      }
    });

    $('.group_age_class').on('change', function() {
      idss = $(this).attr('id');
      s_id = idss.split('-');
      id = s_id[1];

      year = $('#input_year-'+id).val();
      month = $('#input_month-'+id).val();
      day = $('#input_day-'+id).val();
      dob = day+'/'+month+'/'+year;
      if(year != '' && month != '' && year != ''){
        var now = new Date();
        var birthdate = dob.split("/");
        var born = new Date(birthdate[2], birthdate[1]-1, birthdate[0]);
        age=get_age(born,now);
        $('#input_age-'+id).val(age)
      }
    });

    $('.group_weight_class').on('change', function() {
      total_weight = 0;
      for(i=0;i<$('.po_contents_row').length;i++){
        if($('#po_contents_row'+i).length){
          weight = parseFloat($('#input_weight-'+i).val());
          if(weight != '' && weight != '0' && !isNaN(weight)){
            total_weight = total_weight + weight;
          }
        }
      }
      $('#total_weight_td').html(total_weight);
      $('#total_weight').val(total_weight);
    });

    $('#input-group_id').on('change', function() {
      group_name = $('#input-group_id option:selected').text();
      group_id = $('#input-group_id').val();
      $('#input-group_name').val(group_name);
      $('#input-text_group_id').val(group_id);
    });

    function get_age(born, now) {
      var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
      if (now >= birthday) {
        return now.getFullYear() - born.getFullYear();
      } else {
        return now.getFullYear() - born.getFullYear() - 1;
      }
    }

    $('#input-sport_type, #input-participant_type').on('change', function() {
      filter_sport_type = $('#input-sport_type').val();
      filter_participant_type = $('#input-participant_type').val();
      filter_partner_required = $('#input-partner_required').val();
      day = $('#input-day').val();
      month = $('#input-month').val();
      year = $('#input-year').val();
      filter_dob = '';
      if(day != '' && month != '' && year != ''){
        filter_dob = year+'-'+month+'-'+day;
      }
      $.ajax({
        url: 'index.php?route=customer/customer/getsports&token=<?php echo $token; ?>&filter_sport_type=' +  encodeURIComponent(filter_sport_type)+'&filter_participant_type=' +  encodeURIComponent(filter_participant_type)+'&filter_dob=' +  encodeURIComponent(filter_dob),
        dataType: 'json',
        success: function(json) {   
          $('#input-sport').find('option').remove();
          if(json['sport_datas']){
            $.each(json['sport_datas'], function (i, item) {
              $('#input-sport').append($('<option>', { 
                  value: item.sport_id,
                  text : item.sport_name 
              }));
              //$('#input-group_name').val(item.group_name);
            });
          }
          if(filter_participant_type == '2'){
            $('.individual_class, .partner_individual_class').hide();
            $('.group_class').show();
            //$('#input-group_name').val('');
            $('.email_div').addClass('required');
            $('.aadhar_card_number_div').hide();
            $('#input-group_id').prop("disabled", false); 
          } else {
            if(partner_required == '1'){
              $('.individual_class, .partner_individual_class').show();
            } else {
              $('.individual_class').show();
            }
            $('.group_class').hide();
            $('.email_div').removeClass('required');
            $('.aadhar_card_number_div').show();
            $('#input-group_id').prop("disabled", true);
          }
        }
      });
    });

    $('#input-day, #input-month, #input-year').on('change', function() {
      filter_sport_type = $('#input-sport_type').val();
      filter_participant_type = $('#input-participant_type').val();
      day = $('#input-day').val();
      month = $('#input-month').val();
      year = $('#input-year').val();
      filter_dob = '';
      if(day != '' && month != '' && year != ''){
        filter_dob = year+'-'+month+'-'+day;
      }
      filter_year = $('#input-year').val();
      $.ajax({
        url: 'index.php?route=customer/customer/getsports&token=<?php echo $token; ?>&filter_sport_type=' +  encodeURIComponent(filter_sport_type)+'&filter_participant_type=' +  encodeURIComponent(filter_participant_type)+'&filter_dob=' +  encodeURIComponent(filter_dob),
        dataType: 'json',
        success: function(json) {   
          if(filter_participant_type == '1' || filter_participant_type == '') {
            $('#input-sport').find('option').remove();
            if(json['sport_datas']){
              $.each(json['sport_datas'], function (i, item) {
                $('#input-sport').append($('<option>', { 
                    value: item.sport_id,
                    text : item.sport_name 
                }));
                //$('#input-group_name').val(item.group_name);
                //$('#input-group_id').val(item.group_id);
              });
            }
            $('#input-group_id').find('option').remove();
            if(json['sports_groups']){
              $.each(json['sports_groups'], function (i, item) {
                $('#input-group_id').append($('<option>', { 
                    value: item.sport_group_id,
                    text : item.sport_group 
                }));
                //$('#input-group_id').val(item.group_id);
              });
              $('#input-group_id').prop("disabled", true);
              $('#input-group_name').val(json['selected_group_name']);   
              $('#input-text_group_id').val(json['selected_group_id']);   
            }
          } else {
            $('#input-sport').find('option').remove();
            if(json['sport_datas']){
              $.each(json['sport_datas'], function (i, item) {
                $('#input-sport').append($('<option>', { 
                    value: item.sport_id,
                    text : item.sport_name 
                }));
                //$('#input-group_name').val(item.group_name);
                //$('#input-group_id').val(item.group_id);
              });
            }
            $('#input-group_id').find('option').remove();
            if(json['sports_groups']){
              $.each(json['sports_groups'], function (i, item) {
                $('#input-group_id').append($('<option>', { 
                    value: item.sport_group_id,
                    text : item.sport_group 
                }));
                //$('#input-group_id').val(item.group_id);
              });   
              $('#input-group_id').prop("disabled", false);
              $('#input-group_name').val(json['selected_group_name']);
              $('#input-text_group_id').val(json['selected_group_id']);
            }
          }
        }
      });
    });

    $('#input-sport').on('change', function() {
      filter_sport = $('#input-sport').val();
      filter_sport_type = $('#input-sport_type').val();
      filter_participant_type = $('#input-participant_type').val();
      $.ajax({
        url: 'index.php?route=account/register/getsportsdata&filter_sport_type=' +  encodeURIComponent(filter_sport_type)+'&filter_participant_type=' +  encodeURIComponent(filter_participant_type)+'&filter_sport=' +  encodeURIComponent(filter_sport),
        dataType: 'json',
        success: function(json) {   
          if(json['partner_required'] == '1'){
            $('.partner_individual_class').show();
          } else {
            $('.partner_individual_class').hide();
          }
          $('#input-partner_required').val(json['partner_required']);
          $('#input-weight').val(json['weight']);
          if(filter_participant_type == '2'){
            //$('#input-group_name').val(json['group_name']);
            //$('#input-group_id').val(json['group_id']);
            $('#input-group_id').find('option').remove();
            if(json['sports_groups']){
              $.each(json['sports_groups'], function (i, item) {
                $('#input-group_id').append($('<option>', { 
                    value: item.sport_group_id,
                    text : item.sport_group 
                }));
                //$('#input-group_id').val(item.group_id);
              });
              $('#input-group_id').prop("disabled", false);
              $('#input-group_name').val(json['selected_group_name']);   
              $('#input-text_group_id').val(json['selected_group_id']);   
            }
          } 
          if(json['hide_birth_date'] == '1'){
            $('.group_age_class').attr("disabled", "disabled");
            $('.group_weight_class').removeAttr("readonly");
          } else {
            $('.group_weight_class').attr("readonly", "readonly");
            $('.group_age_class').removeAttr("disabled");
          }
        }
      });
    });

    $( document ).ready(function() {
      partner_required = '<?php echo $partner_required; ?>';
      if(partner_required == '1'){
        $('.partner_individual_class').show();
      } else {
        $('.partner_individual_class').hide();
      }
      participant_type = '<?php echo $participant_type; ?>';
      sport_type = '<?php echo $sport_type; ?>';
      edit = '<?php echo $edit; ?>';
      hide_birth_date = '<?php echo $hide_birth_date; ?>';
      if(participant_type == '2'){
        $('.individual_class, .partner_individual_class').hide();
        $('.group_class').show();
        $('.aadhar_card_number_div').hide();
        if(edit == '1'){
          if(hide_birth_date == '1'){
            $('.group_age_class').attr("disabled", "disabled");
            $('.group_weight_class').removeAttr("readonly");
          } else {
            $('.group_weight_class').attr("readonly", "readonly");
            $('.group_age_class').removeAttr("disabled");
          }
        }
        $('.email_div').addClass('required');
      } else {
        $('.aadhar_card_number_div').show();
        if(partner_required == '1'){
          $('.individual_class, .partner_individual_class').show();
        } else {
          $('.individual_class').show();
        }
        $('.group_class').hide();
        $('.email_div').removeClass('required');
      }
    });
  </script>
</div>
<?php echo $footer; ?>