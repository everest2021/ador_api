<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $title; ?><br />
  </h1>
  <table class="product" style="width:100% !important;">
    <?php $count = 0; ?>
    <?php if($final_datas) { ?>
    <thead>
      <tr>
        <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Sr. No</td>
        <td class="left" style="padding: 0px 9px;font-size:11px;font-weight: bold;">School Name</td>
        <td class="left" style="padding: 0px 9px;font-size:11px;font-weight: bold;">Participant Name</td>
        <td class="left" style="padding: 0px 9px;font-size:11px;font-weight: bold;">Sport Type</td>
        <td class="left" style="padding: 0px 9px;font-size:11px;font-weight: bold;">Chest Number</td>
      </tr>
    </thead>
    <?php } ?>
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td style="padding: 0px 9px;font-size: 10px;text-align:left;"><?php echo $final_data['Sr.No'] ?></td>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $final_data['School Name']; ?></td>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $final_data['Participant Name']; ?></td>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $final_data['Sport Type']; ?></td>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $final_data['Chest Number']; ?></td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan = "5"><?php echo 'No Records Found'; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div></body></html>