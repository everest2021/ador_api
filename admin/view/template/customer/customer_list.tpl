<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>&nbsp;
        <a href="<?php echo $generate_code ?>" id="button-clear" class="btn btn-primary"><?php echo 'Generate Chest Code'; ?></a>&nbsp;
        <a href="<?php echo $export_code ?>" id="button-clear" class="btn btn-primary"><?php echo 'Export Chest Code'; ?></a>&nbsp;
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-customer').submit() : false;"><i class="fa fa-trash-o"></i>
      </button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo 'सहभागी नाव / Participant Name'; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo 'Participant Name'; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-filter_sport"><?php echo 'खेळ / Sport'; ?></label>
                <select name="filter_sport" id="input-filter_sport" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($sports as $skey =>  $svalue) { ?>
                    <?php if ($skey == $filter_sport) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-filter_school"><?php echo 'शाळेचे नाव / School Name'; ?></label>
                <select name="filter_school" id="input-filter_school" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($school_names as $skey =>  $svalue) { ?>
                    <?php if ($skey == $filter_school) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo 'स्थिती / ' . $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="">All</option>
                  <?php if ($filter_status == '1') { ?>
                  <option value="1" selected="selected"><?php echo 'सक्रिय / Active'; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo 'सक्रिय / Active'; ?></option>
                  <?php } ?>
                  <?php if ($filter_status == '2') { ?>
                  <option value="2" selected="selected"><?php echo 'निष्क्रिय / Inactive'; ?></option>
                  <?php } else { ?>
                  <option value="2"><?php echo 'निष्क्रिय / Inactive'; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-filter_sport_type"><?php echo 'खेळ प्रकार / Sport Type'; ?></label>
                <select name="filter_sport_type" id="input-filter_sport_type" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($sport_types as $skey =>  $svalue) { ?>
                    <?php if ($skey == $filter_sport_type) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group" style="display: none;">
                <label class="control-label" for="input-filter_standard"><?php echo 'Standard'; ?></label>
                <select name="filter_standard" id="input-filter_standard" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($standards as $skey =>  $svalue) { ?>
                    <?php if ($skey == $filter_standard) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo 'जोडलेली तारीख / ' . $entry_date_added; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-filter_participant_type"><?php echo 'सहभागी प्रकार / Participant Type'; ?></label>
                <select name="filter_participant_type" id="input-filter_participant_type" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($participant_types as $skey =>  $svalue) { ?>
                    <?php if ($skey == $filter_participant_type) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-customer">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'सहभागी प्रकार / Participant Name'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo 'सहभागी प्रकार / Participant Name'; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.school') { ?>
                    <a href="<?php echo $sort_school; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'शाळेचे नाव / School Name'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_school; ?>"><?php echo 'शाळेचे नाव / School Name'; ?></a>
                    <?php } ?></td>
                  <td class="text-left" style="display: none;"><?php if ($sort == 'c.standard') { ?>
                    <a href="<?php echo $sort_standard; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Standard'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_standard; ?>"><?php echo 'Standard'; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.sport_type') { ?>
                    <a href="<?php echo $sort_sport_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'खेळ प्रकार / Sport Type'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sport_type; ?>"><?php echo 'खेळ प्रकार / Sport Type'; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.participant_type') { ?>
                    <a href="<?php echo $sort_participant_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'सहभागी प्रकार / Participant Type'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_participant_type; ?>"><?php echo 'सहभागी प्रकार / Participant Type'; ?></a>
                    <?php } ?></td>
                    <td class="text-left"><?php if ($sort == 'c.sport') { ?>
                    <a href="<?php echo $sort_sport; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'खेळ / Sport'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sport; ?>"><?php echo 'खेळ / Sport'; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'स्थिती / ' . $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo 'स्थिती / ' . $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'c.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'जोडलेली तारीख / ' . $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo 'जोडलेली तारीख / ' . $column_date_added; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo 'क्रिया / ' . $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($customers) { ?>
                <?php foreach ($customers as $customer) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($customer['customer_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $customer['customer_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $customer['name']; ?></td>
                  <td class="text-left"><?php echo $customer['school']; ?></td>
                  <td class="text-left" style="display: none;"><?php echo $customer['standard']; ?></td>
                  <td class="text-left"><?php echo $customer['sport_type']; ?></td>
                  <td class="text-left"><?php echo $customer['participant_type']; ?></td>
                  <td class="text-left"><?php echo $customer['sport']; ?></td>
                  <td class="text-left"><?php echo $customer['status']; ?></td>
                  <td class="text-left"><?php echo $customer['date_added']; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $customer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=customer/customer&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').val();
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

  var filter_school = $('select[name=\'filter_school\']').val();
  
  if (filter_school) {
    url += '&filter_school=' + encodeURIComponent(filter_school);
  }

  var filter_standard = $('select[name=\'filter_standard\']').val();
  
  if (filter_standard) {
    url += '&filter_standard=' + encodeURIComponent(filter_standard);
  }

  var filter_sport_type = $('select[name=\'filter_sport_type\']').val();
  
  if (filter_sport_type) {
    url += '&filter_sport_type=' + encodeURIComponent(filter_sport_type);
  }

  var filter_participant_type = $('select[name=\'filter_participant_type\']').val();
  
  if (filter_participant_type) {
    url += '&filter_participant_type=' + encodeURIComponent(filter_participant_type);
  }

  var filter_sport = $('select[name=\'filter_sport\']').val();
  
  if (filter_sport) {
    url += '&filter_sport=' + encodeURIComponent(filter_sport);
  }
	
	var filter_status = $('select[name=\'filter_status\']').val();
	//if (filter_status) {
		url += '&filter_status=' + encodeURIComponent(filter_status); 
	//}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').val();
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}	
});
//--></script> 
<script type="text/javascript"><!--
$('#input-filter_sport_type, #input-filter_participant_type, #input-filter_standard').on('change', function() {
  filter_sport_type = $('#input-filter_sport_type').val();
  filter_participant_type = $('#input-filter_participant_type').val();
  $.ajax({
    url: 'index.php?route=customer/customer/getsports1&token=<?php echo $token; ?>&filter_sport_type=' +  encodeURIComponent(filter_sport_type)+'&filter_participant_type=' +  encodeURIComponent(filter_participant_type),
    dataType: 'json',
    success: function(json) {   
      $('#input-filter_sport').find('option').remove();
      if(json['sport_datas']){
        $.each(json['sport_datas'], function (i, item) {
          $('#input-filter_sport').append($('<option>', { 
              value: item.sport_id,
              text : item.sport_name 
          }));
        });
      }
    }
  });
});
$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
});
//--></script></div>
<?php echo $footer; ?> 
