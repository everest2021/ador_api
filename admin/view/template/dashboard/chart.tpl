<div class="tile" style="background-color: #8cc152;">
  <div class="tile-heading" style="display: none;"><?php echo $heading_title; ?></div>
  <div class="tile-body" style="min-height: 92px;">
    <h2 class="pull-left" style="">
      <div style="font-size: 15px;"><?php echo $heading_title; ?></div>
      <div style="font-size: 30px;"><?php echo $count; ?></div>
    </h2>
    <h2 class="pull-right" style="display: none;">
      <?php echo $count; ?>
    </h2>
  </div>
  <div class="tile-footer" style="background-color: #006400;"><a href="<?php echo $url; ?>"><?php echo $text_view; ?></a></div>
</div>