<!DOCTYPE html>
<html dir="<?php echo ''; ?>" lang="<?php echo ''; ?>">
<head>
<meta charset="UTF-8" />
<!-- <title><?php echo $heading_title; ?></title> -->
</head>
<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
	<h3><br><?php echo date('d/m/Y'); ?></h3>
	<?php date_default_timezone_set("Asia/Kolkata");?>
	<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
	<center><h2><b>Mini Day Summary Report</b></h2></center>
	<h3>From : <?php echo $startdate; ?></h3>
	<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
		<table border="1" class="table" style="margin-top: 0px;border: 1px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
			<?php if($orderdataarrays != array()){ ?>
								
				<tr>
					<td><b>Table Group</b></td>
					<td></td>
					<td><b>Amount</b></td>
				</tr>
				<?php foreach($orderlocdatas as $locdata){ ?>
					<tr>
						<td><?php echo $locdata['location'] ?></td>
						<td></td>
						<td><?php echo $locdata['netsale'] ?></td>
					</tr>
				<?php } ?>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Sub Total</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalnetsale'] ?></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>KKC Amt(+)</td>
					<td></td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>SBC Amt(+)</td>
					<td></td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>S-Chrg Amt(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalscharge'] ?></td>
				</tr>
				<tr>
					<td>Vat(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalvat'] ?></td>
				</tr>
				<tr>
					<td>Gst(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalgst'] ?></td>
				</tr>
				<tr>
					<td>Packaging(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalpackaging']; ?></td>
				</tr>

				<tr>
					<td>Packaging CGST(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalpackaging_cgst']; ?></td>
				</tr>

				<tr>
					<td>Packaging SGST(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalpackaging_sgst']; ?></td>
				</tr>

				<tr>
					<td>R-Off Amt(+)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totalroundoff']; ?></td>
				</tr>

				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Discount Amt(-)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['totaldiscount'] ?></td>
				</tr>
				<tr>
					<td>P-Discount Amt(-)</td>
					<td></td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>Cancel Bill Amt(-)</td>
					<td></td>
					<td>0.00</td>
				</tr>

				<tr>
					<td>Advance. Amt(-)</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['advance'] ?></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Gross Total</td>
					<td></td>
					<td><?php echo $orderlocdatasamt['nettotalamt'] ?></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td><b>Payment Summary</b></td>
					<td></td>
					<td></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Cash</td>
					<td></td>
					<td><?php echo $orderpaymentamt['totalcash']?></td>
				</tr>
				<tr>
					<td>Credit Card</td>
					<td></td>
					<td><?php echo $orderpaymentamt['totalcard']?></td>
				</tr>
				<tr>
					<td>Online</td>
					<td></td>
					<td><?php echo $orderpaymentamt['totalpayonline']?></td>
				</tr>
				<tr>
					<td>On A/c</td>
					<td></td>
					<td><?php echo $orderpaymentamt['onac']?></td>
				</tr>
				<tr>
					<td>Meal Pass</td>
					<td></td>
					<td><?php echo $orderpaymentamt['mealpass']?></td>
				</tr>
				<tr>
					<td>Pass</td>
					<td></td>
					<td><?php echo $orderpaymentamt['pass']?></td>
				</tr>
				<tr>
					<td>Room Service</td>
					<td></td>
					<td><?php echo $orderpaymentamt['room']?></td>
				</tr>
				<tr>
					<td>MSR Card</td>
					<td></td>
					<td><?php echo $orderpaymentamt['msr']?></td>
				</tr>
				<tr>
					<td>Advance Amount (-)</td>
					<td></td>
					<td><?php echo $orderpaymentamt['advanceamt']?></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Total</td>
					<td></td>
					<td><?php echo $orderpaymentamt['total']?></td>
				</tr>
				<tr>
					<td>Card tip</td>
					<td></td>
					<td><?php echo $orderpaymentamt['tip_amount']?></td>
				</tr>
				<tr>
					<td>Advance Amt(+)</td>
					<td></td>
					<td><?php echo $orderpaymentamt['advance']?></td>
				</tr>
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td><b>Grand Total</b></td>
					<td></td>
					<td><?php echo $orderpaymentamt['totalpaysumm']?></td>
				</tr>

				
				<tr><td colspan="3" height="35"></td></tr>
				<tr>
					<td>Last Bill No</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['lastbill'] ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Bill Printed Count</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['bill_printed'] ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Last Kot No</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['lastkot'] ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Last Bot No</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['lastbot'] ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Unsettle Bill Amt</td>
					<td><?php echo $pendingtablecount ?></td>
					<td><?php echo $pendingtable_amt ?></td>
				</tr>
				<tr>
					<td>Duplicate Bill</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['duplicatecount'] ?></td>
					<td><?php echo $orderdatalastarray['orderdatalast']['duplicate'] ?></td>
				</tr>
				<tr>
					<td>Pending Table</td>
					<td><?php echo $pendingtablecount ?></td>
					<td><?php echo $pendingtable_amt ?></td>
				</tr>
				<tr>
					<td>Compliment Bill</td>
					<td><?php echo $orderdatalastarray['orderitemlast']['complimentarycount'] ?></td>
					<td><?php echo $orderdatalastarray['orderitemlast']['complimentarybill'] ?></td>
				</tr>
				<tr>
					<td>Cancelled Bill</td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelledcount'] ?></td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelledbill'] ?></td>
				</tr>
				<tr>
					<td>Cancelled Kot</td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelkot'] ?></td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountkot'] ?></td>
				</tr>
				<tr>
					<td>Cancelled Bot</td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelbot'] ?></td>
					<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountbot'] ?></td>
				</tr>
				<tr>
					<td>NC KOT</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['totalnc'] ?></td>
					<td><?php echo $orderdatalastarray['orderdatalast']['totalamountnc'] ?></td>
				</tr>
				<tr>
					<td>CHANGE BILL</td>
					<td><?php echo $NumberOfbillmodify ?></td>
					<td><?php echo $totalbillamount ?></td>
				</tr>
				<tr>
					<td>Disc Before Bill</td>
					<td>0.00</td>
					<td>0.00</td>
				</tr>
				<tr>
					<td>Disc after Bill</td>
					<td><?php echo $orderdatalastarray['orderdatalast']['discountbillcount'] ?></td>
					<td><?php echo $orderdatalastarray['orderdatalast']['discountamount'] ?></td>
				</tr>
				
			</table>
			<br>
		<?php } ?>
<center><h3><b>End of Report</b></h3></center>
</div></body></html>
<style type="text/css">
	
</style>