<!DOCTYPE html>
	<html dir="<?php echo ""; ?>" lang="<?php echo ""; ?>">
		<head>
			<meta charset="UTF-8" />
			<!-- <title><?php echo $heading_title; ?></title> -->
		</head>
		<body style="height: 100%;margin: 0;padding: 0;font-family: 'Open Sans', sans-serif !important;font-size: 12px;color: #666666; text-rendering: optimizeLegibility;">
			<div class="container" style="width: 725px;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
  
			  <!-- <div class="col-sm-6 col-sm-offset-3">
						<center>
						  <?php echo HOTEL_NAME ?>
						  <?php echo HOTEL_ADD ?>
						</center> -->

				<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
		  		<?php date_default_timezone_set("Asia/Kolkata");?>
		  		<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
		  		<center><h5><b>Billwise Sales Report</b></h5></center>
		  		<h5>From : <?php echo $startdate; ?></h5>
		  		<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
				<table border="1" class="table" style="margin-top: 0px;border: 0px solid black;width: 100%;max-width: 100%;margin-bottom: 5px;background-color: transparent;border-collapse: collapse;padding: 0px !important;font-size: 14px;">
					<tr>
							<th style="text-align: center;">Ref No</th>
							<th style="text-align: center;">Food</th>
							<th style="text-align: center;">GST</th>
							<th style="text-align: center;">F.Disc</th>
							<th style="text-align: center;">Bar</th>
							<th style="text-align: center;">VAT</th>
							<th style="text-align: center;">S-Chrg</th>
							<th style="text-align: center;">Packing-Chrg</th>
							<th style="text-align: center;">L.Disc</th>
							<th style="text-align: center;">Total</th>
							<th style="text-align: center;">Pay By</th>
							<th style="text-align: center;">Pay By Cash</th>
							<th style="text-align: center;">Pay By Card</th>
							<th style="text-align: center;">Pay By Online</th>
							<th style="text-align: center;">On Account</th>
							<th style="text-align: center;">T No</th>
							<th style="text-align: center;">Time</th>

						</tr>
					
			  			<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0; $packaging = 0;$packaging_cgst = 0;$packaging_sgst = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="17"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sdiscount = 0; $sgrandtotal = 0; $svat = 0; $sgst = 0; $sfoodtotal = 0; $sfdistotal = 0; $sliqtotal = 0;$sldistotal = 0; $sdiscountvalue = 0; $safterdiscount = 0; $sstax = 0; $sroundoff = 0; $stotal = 0; $sadvance = 0; $spackaging = 0;$spackaging_cgst = 0;$spackaging_sgst = 0;?>
								  	<?php foreach ($value as $data) { //echo'<pre>';print_r($data);exit;?>
								  		<tr >
									  		<td><?php echo $data['order_no'] ?></td>
									  		<?php if($data['food_cancel'] == 1) { ?>
									  			<td><?php echo $data['ftotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ftotal']; $foodtotal += $data['ftotal'];?></td>
									  		<?php } ?>
									  		<td><?php echo $data['gst'] ?></td>
									  		<td><?php echo $data['ftotalvalue']; $fdistotal += $data['ftotalvalue'] ?></td>
									  		<?php if($data['liq_cancel'] == 1) { ?>
									  			<td><?php echo $data['ltotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ltotal']; $liqtotal +=$data['ltotal'];  ?></td>
									  		<?php } ?>
									  		<td><?php echo $data['vat'] ?></td>
									  		<td><?php echo $data['stax'] ?></td>
									  		<td><?php echo $data['packaging'] ?></td>
									  		<td><?php echo $data['ltotalvalue'];$ldistotal +=$data['ltotalvalue']; ?></td>
									  		<?php if($INCLUSIVE == 1) { ?>
									  			<td><?php echo round($data['ftotal'] + $data['ltotal'])?></td>
									  		<?php } else { ?>
									  			<td><?php echo round($data['grand_total'] ); ?></td>
									  		<?php } ?>
									  		<?php if($data['pay_cash'] != '0') { ?>
									  			<td>Cash</td>
									  		<?php } else if($data['urbanpiper_order_id'] != '0' || $data['urbanpiper_order_id'] != '') { ?>
									  			<td><?php echo $data['order_from'] ?></td>
									  		<?php } else if($data['pay_card'] != '0') { ?>
									  			<td>Card</td>
									  		<?php } else if($data['pay_online'] != '0') { ?>
									  			<td>Online(<?php echo $data['payment_type'] ?>)</td>
									  		<?php } else if($data['onac'] != '0.00') { ?>
									  			<td>OnAc</td>
									  		<?php } else if($data['mealpass'] != '0') { ?>
									  			<td>Meal Pass</td>
									  		<?php } else if($data['pass'] != '0') { ?>
									  			<td>Pass</td>
									  		<?php } else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { ?>
									  			<td>Cash+Card</td>
									  		<?php } else if($data['wera_order_id'] != '0' || $data['wera_order_id'] != '') { ?>
									  			<td><?php echo $data['order_from'] ?></td>
									  		<?php } else { ?>
									  			<td>Pending</td>
									  		<?php } ?>

									  		<td><?php echo $data['pay_cash'] ?></td>
									  		<td><?php echo $data['pay_card'] ?></td>
									  		<td><?php echo $data['pay_online'] ?></td>
									  		<td><?php echo $data['onac'] ?></td>

									  		<td><?php echo $data['t_name'] ?></td>
									  		<td><?php echo $data['out_time'] ?></td>
									  	</tr>
									  	<?php 
									  	 	  if($data['liq_cancel'] == 1){
									  		  	$vat = 0;
									  		  	$svat = 0;
									  		  } else{
									  		  	$vat = $vat + $data['vat'];
									  		  	$svat = $svat + $data['vat'];

									  		  }
									  		  if($data['food_cancel'] == 1){
									  		  	$gst = 0;
									  		  	$sgst = 0;
									  		  } else{
									  		  	$gst = $gst + $data['gst'];
									  		  	$sgst = $sgst + $data['gst'];
									  		  }
									  		  $packaging = $packaging + $data['packaging'];
										  		$packaging_cgst = $packaging_cgst + $data['packaging_cgst'];
										  		$packaging_sgst = $packaging_sgst + $data['packaging_sgst'];
									  		  $stax = $stax + $data['stax'];
									  		  if($data['food_cancel'] == 1){
									  		  	$data['ftotalvalue'] = 0; 
									  		  } 
									  		  if($data['liq_cancel'] == 1){
									  		  	$data['ltotalvalue'] = 0;
									  		  }
									  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
									  		  if($INCLUSIVE == 1){
									  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
									  		  	$sgrandtotal = round($sgrandtotal + $data['ftotal'] + $data['ltotal']); 
									  		  	$total = $grandtotal + $stax;
									  		  	$stotal = $sgrandtotal + $sstax;


									  		  } else{
								  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']  + $data['packaging']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
								  		  	 	$sgrandtotal = round($sgrandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']  + $data['packaging']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
								  		  	 	$total = $grandtotal + $gst + $vat + $stax + $packaging;
								  		  	 	$stotal = $sgrandtotal + $sgst + $svat + $sstax;


									  		  }
									  		  $discountvalue = $discountvalue + $discount;
									  		  $sdiscountvalue = $sdiscountvalue + $discount;
									  		  $afterdiscount = $total - $discountvalue;
									  		   $safterdiscount = $stotal - $sdiscountvalue;
									  		  $roundoff = $roundoff + $data['roundtotal'];
									  		  $sroundoff = $sroundoff + $data['roundtotal'];
									  		  $advance = $advance + $data['advance_amount'];
									  	?>
									<?php } ?>
									<!-- <tr>
								  		<td colspan="10"><b>Bill Total :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php echo $sgrandtotal ?></b></td>
								  	</tr>

								  	<tr>
								  		<td colspan="10">GST Amt (+) :</td>
								  		<td colspan="5" style="text-align: left;"><?php echo $sgst ?></td>
							  		</tr>

							  		<tr>
								  		<td colspan="10"><b>Total :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php echo ($stotal + $sroundoff) ?></b></td>
							  		</tr>

								  	<tr>
								  		<td colspan="10">Discount Amt (-) :</td>
								  		<td colspan="5" style="text-align: left;"><?php echo $sdiscountvalue ?></td>
							  		</tr>

							  		<tr>
								  		<td colspan="10"><b>Net Amount :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php echo ($safterdiscount + $sroundoff) ?></b></td>
							  		</tr> -->
							  	<?php }  ?>
							  	<tr>
							  		<td colspan="17"><b style="font-size: 25px;">All Total </b></td>
							  		
							  	</tr>
							  	<tr>
							  		<td colspan="10"><b>Bill Total :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $grandtotal ?></b></td>
							  	</tr>
							  	<tr>
							  		<td colspan="10">O- Chrg Amt (+) : </td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Vat Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $vat ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">S- Chrg Amt (+) : </td>
							  		<td colspan="7" style="text-align: left;"><?php echo $stax ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">GST Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $gst ?></td>
						  		</tr>
						  			<tr>
							  		<td colspan="10">KKC Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  			<tr>
							  		<td colspan="10">SBC Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">R-Off Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $roundoff / 2; ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Packaging (+)</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $packaging ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Total :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($total + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Discount Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $discountvalue ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Cancel BIll Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $cancelamount ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Comp. Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Advance. Amt (-):</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $advance ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Net Amount :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Advance. Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $advancetotal ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Grand Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>NC KOT :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $nc_kot_amt ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Food Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $foodtotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Total Food Discount (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $fdistotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Liquar Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $liqtotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Total Liquar Discount (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $ldistotal ?></b></td>
						  		</tr>

						  		<!-- <tr>
							  		<td colspan="10"><b>Packaging (+) :</td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo $packaging ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Packaging CGST  (+) :</td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo $packaging_cgst ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Packaging SGST (+) :</td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo $packaging_sgst ?></b></td>
						  		</tr> -->
				</table>
			<?php echo $footer; ?>
   		</div>
	</body>
</html>
