<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
		<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
		<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
		<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
		<title>Print Bill</title>
		<script type="text/javascript">
		$(document).ready(function() {
			setTimeout(print_fun, 1000);
		});
		function close_fun(){
			window.close();
		}
		function  print_fun(){
			window.print();
			var redirect_page = '<?php echo $redirect_page; ?>';
			redirect_page = redirect_page.replace("&amp;", "&");
			window.location.href = redirect_page;
			setTimeout(close_fun, 100);
		}
		</script>
	</head>
	<body>
		<div id="MC" style="overflow:hidden; width:340px;">
			<div align="center" style="display: none;"><img src="view/template/sale/logo.icon" height="100px" width="90px" style="height:100px;"/></div>
			
			<div id="inside-container">
				<table style="text-align: center;">
					<tr>
						<td style="text-transform: uppercase;width:340px;font-weight:bold;font-size: 12px;"><?php echo $hotel_name ?></td>
					</tr>
					<tr >
						<td style="text-transform: uppercase;width:340px;font-weight:bold;font-size: 12px;"><?php echo $hotel_addss ?> </td>
					</tr>
				</table> 
				
				<div style=" width:340px;border-bottom:1px solid;">
					<br />
					
					<span style="font-size:20px;text-align:center;margin-left:115px;margin-bottom:5px;font-weight: 100;font-weight:bold;">
						MSR Recharge
					</span><br />
					
					<span style="text-align: left;font-size: 18px;font-weight:bold;">Date: <?php echo date('d-m-Y', strtotime($current_date)); ?></span><br />
				   <br />
					<span style="text-align: left;font-size: 18px;font-weight:bold;">MSR NO:- <?php echo $final_datas['msr_no'] ;?></span><br />
					
				</div>
				<div id="actual">
					<table cellpadding="1" cellspacing="1" style="width:340px;font-size:12px;margin-top:5px;margin-bottom:5px;border-bottom:1px dotted;">
						<tbody>
							<div>
								<tr style=" width:350px;border-bottom:1px dotted;">
									<th><span style="width:150px; padding-left: 0px;font-size: 14px;font-weight:bold;">Customer Name</span></th>
									<th><span style="margin-left: 5px;font-weight: 100;font-size: 14px; font-weight:bold;">Mobile No</span></th>
									<th><span style="margin-left: 5px;font-weight: 100;font-size: 14px; font-weight:bold;">Recharge Amt</span></th>
								</tr>
								<?php $total_amt = 0;?>
								<?php $save_amt = 0;?>
								<tr>
									<td style="width:150px;font-size: 13px;font-weight:bold;"><?php echo $final_datas["cust_name"]; ?><br/></td>
									<td style="padding-left: 10px;font-size: 13px;font-weight:bold;"><?php echo $final_datas["cust_mob_no"]; ?></td>
									<td style="padding-left: 10px;font-size: 13px;font-weight:bold;"><?php echo $final_datas["amount"]; ?></td>
								</tr>
											
							</div> 
						</tbody> 
					</table>
					<br />
					<div style="border-bottom:1px dotted;">
					</div>
					<br />
					
				</div>
			</div>
		</div>
	</body>
</html>	