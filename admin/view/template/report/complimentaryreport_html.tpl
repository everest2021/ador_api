<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo 'Complimentary Report'; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
  <div style="page-break-after: always;">
	<h1 style="text-align:center;">
	  <?php echo 'Complimentary'; ?><br />
	  <p style="display:inline;font-size:15px;"><?php echo 'Date : '. $filter_startdate; ?></p>
	</h1>
	<table class="product" style="width:100% !important;">
		
		<tr>
			<th style="text-align: center;">Ref.No</th>
			<th style="text-align: center;">Table No</th>
			<th style="text-align: center;">Bill Date</th>
			<th style="text-align: center;">Item Name</th>
			<th style="text-align: center;">Qty.</th>
			<th style="text-align: center;">Rate</th>
			<th style="text-align: center;">Total Amt.</th>
			<th style="text-align: center;">Grand Total</th>
			<th style="text-align: center;">Resion</th>
		</tr>
	 
	  <tbody>
	  	
		<?php foreach($results as $data) { ?>
			<tr>
				<td><?php echo $data['ref_no'] ?></td>
				<td><?php echo $data['table_no'] ?></td>
				<td><?php echo $data['bill_date'] ?></td>
				<td><?php echo $data['item_name'] ?></td>
				<td><?php echo $data['qty'] ?></td>
				<td><?php echo $data['rate'] ?></td>
				<td><?php echo $data['amt'] ?></td>
				<td><?php echo $data['grand_total'] ?></td>
				<td><?php echo $data['resion'] ?></td>
			</tr>
		<?php } ?>
	
	  </tbody>
	</table>
  </div>
</body>
</html>
