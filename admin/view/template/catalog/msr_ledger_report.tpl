<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" id="creditcust" name="creditcust" value="<?php echo $creditcust ?>" class="form-control" placeholder="Customer name">
									    	<input type="hidden" id="creditid" name="creditid" value="<?php echo $creditid ?>" class="form-control">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10" style="display: none;">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div>
					<div class="col-sm-8 col-sm-offset-2">
					<center style = "display:none;">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>MSR Ledger Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<?php if(isset($_POST['submit'])){ ?>

							<?php foreach ($finaldata as $ckey => $cvalue) { ?>
								<tr>
									<td colspan="2"><strong> MSR No </strong></td>
									<td colspan="2"><strong> customer Name</strong></td>		
									<td><strong> Contact</strong></td>		
								</tr>
									<td colspan="2"> <?php echo $cvalue['msr_no']?></td>
									<td colspan="2"> <?php echo $cvalue['cust_name']?></td>		
									<td> <?php echo $cvalue['contact']?></td>		
								<tr>
									<td><strong>Sr No</strong></td>		

									<td><strong>Date</strong></td>		
									<td style="text-align: right;"><strong >Reacharge</strong></td>		
									<td style="text-align: right;"><strong >Redeem</strong></td>
									<td style="text-align: right;"><strong >Refund</strong></td>
								</tr>
								<?php $total = 0; ?>
								<?php foreach ($cvalue['rechargedata'] as $rkey => $rvalue) { ?>
									<tr>
										<td> <?php echo $rvalue['sr_no'] ?></td>		

										<td> <?php echo $rvalue['date'] ?></td>		
										<td style="text-align: right;"> <?php echo $rvalue['reacharge'] ?></td>		
										<td style="text-align: right;"> <?php echo $rvalue['redeem']?></td>
										<td style="text-align: right;"> <?php echo $rvalue['refund']?></td>

									</tr>
									<?php $total += $rvalue['reacharge']- $rvalue['refund'] - $rvalue['redeem']?>
								<?php }  ?>	

								<?php if (count($rvalue['order_data']) > '0') { ?>
										<td><strong> Date</strong></td>		
										<td><strong> Amount</strong></td>		
										<td><strong> Bill No </strong></td>
								<?php } ?>
									<?php foreach ($rvalue['order_data'] as $okey => $ovalue) { ?>
										<tr>
											<td> <?php echo $ovalue['bill_date']?></td>
											<td> <?php echo $ovalue['msrcard'] ?></td>		
											<td> <?php echo $ovalue['billno'] ?></td>		
										</tr>
										<?php $total = $total - $ovalue['msrcard'];?>		

									<?php }  ?>
									<tr>
										<td colspan="4" style="text-align: right;">Balance</td>
										<td colspan="1" style="text-align: right;"> <?php echo $total;?></td>
									</tr>
							<?php //echo "<pre>"; print_r($finaldata); ?>
						<?php } ?>
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/msr_ledger_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}

	$('#creditcust').autocomplete({
	  	delay: 500,
	  	source: function(request, response) {
			$.ajax({
		  		url: 'index.php?route=catalog/creditreport/name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  		dataType: 'json',
		  		success: function(json) {   
					response($.map(json, function(item) {
			  			return {
							label: item.name + "-" + item.contact,
							value: item.name,
							c_id: item.c_id
			  			}
					}));
		  		}
			});
	  	}, 
	  	select: function(event, ui) {
	  		$('#creditid').val(ui.item.c_id);
	  	}
	});
	</script>
</div>
<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
<?php echo $footer; ?>