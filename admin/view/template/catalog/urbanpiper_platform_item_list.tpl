<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $back; ?>" data-toggle="tooltip" style="background-color: #f33333;border-color:#f33333;" class="btn btn-warning">Back</a>
         <!-- &nbsp;&nbsp;<a href="<?php echo $back; ?>"  class="btn btn-warning" style="background-color: #f33333;border-color:#f33333;position: absolute !important;bottom: 2px !important;right: 5px !important;width: 55px !important;  ">Back </a> -->

      </div>
      <h1><?php echo $platform; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $platform.' '.'List'; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                    <td class="text-left" colspan="7"><?php echo $platform; ?></td>
                </tr>
                <tr>
                    <td class="text-left">Sr no </td>
                    <td class="text-left">ILPA ID</td>
                    <td class="text-left">Item Code</td>
                    <td class="text-left">Title</td>
                    <td class="text-left">Stocked-in </td>
                    <td class="text-left">Status </td>
                     <td class="text-right">Date </td>
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($final_data)) { $inc = 1;?>
                    <?php foreach ($final_data as $product) { 
                         ?>
                        <tr > 
                            <td class="text-left"><?php echo $inc++ ; ?></td>
                            <td class="text-left"><?php echo $product['ilpa_id']; ?></td>
                            <td class="text-left"><?php echo $product['item_id']; ?></b></td>
                            <td class="text-left"><?php echo $product['item_name']; ?></b></td>
                            <td class="text-left"><?php if ($product['action'] == 'stock-in') { ?> <b style="color:green"><?php } elseif($product['action'] == 'stock-out') { ?> <b style="color:red">   <?php } ?> <?php echo ucwords($product['action']); ?></td>
                            <td class="text-left"><?php if ($product['status'] == 'success') { ?> <b style="color:#2196f3"><?php } elseif($product['status'] == 'failure') { ?> <b style="color:red">   <?php } ?> <?php echo ucwords($product['status']); ?></td>
                            <td class="text-right"> <?php if ($product['ts_utc'] != '') { $order_time =  date('d-m-Y H:i:s', $product['ts_utc']/1000); }?><?php echo $order_time; ?></td>
                        </tr>
                    <?php  } ?>
                <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
         
        </form>
      </div>
    </div>
  </div>

  <style type="text/css">
    .notification-message-unread {
        color:white;
        background-color: red;
      animation: blinker 7s linear infinite;
    }

    @keyframes blinker {
     0%, 49% {
        color:white;
        background-color:green;
      }
      50%, 100% {
        background-color: blue;
         color:white;
            }
    }
    .myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;
    }
</style>
  <script type="text/javascript"><!--

$('#button-filter').on('click', function() {
    var url = 'index.php?route=catalog/urbanpiperitem_show&token=<?php echo $token; ?>';

    var filter_item_name = $('input[name=\'filter_item_name\']').val();
    if (filter_item_name) {
        var filter_item_id = $('input[name=\'filter_item_id\']').val();
        if (filter_item_id) {
            url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
            url += '&filter_item_name=' + encodeURIComponent(filter_item_name);
        }
    }

    var filter_item_code = $('input[name=\'filter_item_code\']').val();
    if (filter_item_code) {
        url += '&filter_item_code=' + encodeURIComponent(filter_item_code);
    }

    var filter_barcode = $('input[name=\'filter_barcode\']').val();
    if (filter_barcode) {
        url += '&filter_barcode=' + encodeURIComponent(filter_barcode);
    }
    
    location = url;
});

$('input[name=\'filter_item_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/urbanpiperitem_show/autocomplete&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_name,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_name\']').val(ui.item.label);
    $('input[name=\'filter_item_id\']').val(ui.item.value);
        
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_item_code\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/urbanpiperitem_show/autocompletecode&token=<?php echo $token; ?>&filter_item_code=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_code\']').val(ui.item.label);
        
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
//--></script></div>
<?php echo $footer; ?>