<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="input-brand"><?php echo 'Brand Name'; ?></label>
								<input type="text" name="filter_brand" value="<?php echo $filter_brand; ?>" placeholder="<?php echo 'Brand'; ?>" id="filter_brand" class="form-control" />
								<input type="hidden" name="filter_brand_id" value="<?php echo $filter_brand_id; ?>" id="filter_brand_id" class="form-control" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="input-subcategory"><?php echo 'SubCategory'; ?></label>
								<input type="text" name="filter_subcategory" value="<?php echo $filter_subcategory; ?>" placeholder="<?php echo 'SubCategory'; ?>" id="input-filter_subcategory" class="form-control" />
								<input type="hidden" name="filter_subcategory_id" value="<?php echo $filter_subcategory_id; ?>" id="input-filter_subcategory_id" class="form-control" />
								<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
							</div>
						</div>
					</div>
				</div>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<td class="text-left">
										<?php echo "Sr.No"; ?>
									</td>
									<td class="text-right">
										<?php if ($sort == 'brand') { ?>
										<a href="<?php echo $brand; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Brand"; ?></a>
										<?php } else { ?>
										<a href="<?php echo $brand; ?>"><?php echo "Brand"; ?></a>
										<?php } ?></td>
									<td class="text-right">
										<?php if ($sort == 'brand') { ?>
										<a href="<?php echo $brand; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Subcategory"; ?></a>
										<?php } else { ?>
										<a href="<?php echo $brand; ?>"><?php echo "Subcategory"; ?></a>
										<?php } ?></td> 
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($tables) { ?>
								<?php $i = '1';?>
								<?php foreach ($tables as $brand) { ?>
								<tr>
									<td class="text-center">
										<?php if (in_array($brand['brand_id'], $selected)) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $brand['brand_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $brand['brand_id']; ?>" />
										<?php } ?></td>
									<td class="text-left"><?php echo $i; ?></td>
									<td class="text-right"><?php echo $brand['brand']; ?></td>
									<td class="text-right"><?php echo $brand['subcategory']; ?></td>
									<td class="text-right"><a href="<?php echo $brand['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
								</tr>
								<?php $i++?>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/brand&token=<?php echo $token; ?>';

	var filter_brand = $('input[name=\'filter_brand\']').val();
	if (filter_brand) {
		var filter_brand_id = $('input[name=\'filter_brand_id\']').val();
		if (filter_brand_id) {
			url += '&filter_brand_id=' + encodeURIComponent(filter_brand_id);
			}
			url += '&filter_brand=' + encodeURIComponent(filter_brand);
	}

	var filter_subcategory = $('input[name=\'filter_subcategory\']').val();
	if (filter_brand) {
		var filter_brand_id = $('input[name=\'filter_brand_id\']').val();
		if (filter_subcategory) {
			url += '&filter_brand_id=' + encodeURIComponent(filter_brand_id);
			}
			url += '&filter_subcategory=' + encodeURIComponent(filter_subcategory);
	}

	location = url;
});

$('input[name=\'filter_brand\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/brand/autocompletebname&token=<?php echo $token; ?>&filter_brand=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.brand,
            value: item.brand_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_brand\']').val(ui.item.label);
    $('input[name=\'filter_brand_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_subcategory\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/brand/autocompletesubcategory&token=<?php echo $token; ?>&filter_subcategory=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.subcategory,
            value: item.brand_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_subcategory\']').val(ui.item.label);
    $('input[name=\'filter_brand_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});



</script></div>
<?php echo $footer; ?>