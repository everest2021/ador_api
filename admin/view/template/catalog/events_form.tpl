<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
			   <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			   <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			   <h1><?php echo $heading_title; ?></h1>
			   <ul class="breadcrumb">
				   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
			   </ul>
		</div>
  </div>

  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
		<?php } ?>
	   <div class="panel panel-default">
		   <div class="panel-heading">
			   <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Event Name'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="event_name" value="<?php echo $event_name; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
						<?php if ($error_name ) { ?>
						<div class="text-danger"><?php echo $error_name ; ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group ">
						<label class="col-sm-3 control-label"><?php echo 'SubCategory'; ?></label>
						<div class="col-sm-3">
							<select name="event_type" id="input-event_type" class="form-control">
								<?php foreach($event_types as $skey => $svalue){ ?>
									<?php if($skey == $event_type){ ?>
										<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
									<?php } else { ?>
										<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Front Image'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="photo" value="<?php echo $photo; ?>" placeholder="<?php echo 'Front Image'; ?>" id="input-photo" class="form-control" />
							<input type="hidden" name="photo_source" value="<?php echo $photo_source; ?>" id="input-photo_source" class="form-control" />
							<br>
							<span class="input-group-btn">
								<button type="button" id="button-photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
							</span>
							<?php if($photo_source != ''){ ?>
								<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source" class="thumbnail" href="<?php echo $photo_source; ?>">View Image</a>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Additional Image 1'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="photo1" value="<?php echo $photo1; ?>" placeholder="<?php echo 'Additional Image 1'; ?>" id="input-photo1" class="form-control" />
							<input type="hidden" name="photo_source1" value="<?php echo $photo_source1; ?>" id="input-photo_source1" class="form-control" />
							<br>
							<span class="input-group-btn">
								<button type="button" id="button-photo1" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
							</span>
							<?php if($photo_source1 != ''){ ?>
								<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source1" class="thumbnail" href="<?php echo $photo_source1; ?>">View Image</a>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Additional Image 2'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="photo2" value="<?php echo $photo2; ?>" placeholder="<?php echo 'Additional Image 2'; ?>" id="input-photo2" class="form-control" />
							<input type="hidden" name="photo_source2" value="<?php echo $photo_source2; ?>" id="input-photo_source2" class="form-control" />
							<br>
							<span class="input-group-btn">
								<button type="button" id="button-photo2" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
							</span>
							<?php if($photo_source2 != ''){ ?>
								<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source2" class="thumbnail" href="<?php echo $photo_source2; ?>">View Image</a>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Additional Image 3'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="photo3" value="<?php echo $photo3; ?>" placeholder="<?php echo 'Additional Image 3'; ?>" id="input-photo3" class="form-control" />
							<input type="hidden" name="photo_source3" value="<?php echo $photo_source3; ?>" id="input-photo_source3" class="form-control" />
							<br>
							<span class="input-group-btn">
								<button type="button" id="button-photo3" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
							</span>
							<?php if($photo_source3 != ''){ ?>
								<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source3" class="thumbnail" href="<?php echo $photo_source3; ?>">View Image</a>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Additional Image 4'; ?></label>
						<div class="col-sm-3">
							<input readonly="readonly" type="text" name="photo4" value="<?php echo $photo4; ?>" placeholder="<?php echo 'Additional Image 4'; ?>" id="input-photo4" class="form-control" />
							<input type="hidden" name="photo_source4" value="<?php echo $photo_source4; ?>" id="input-photo_source4" class="form-control" />
							<br>
							<span class="input-group-btn">
								<button type="button" id="button-photo4" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
							</span>
							<?php if($photo_source4 != ''){ ?>
								<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source4" class="thumbnail" href="<?php echo $photo_source4; ?>">View Image</a>
							<?php } ?>
						</div>
					</div>
			</form>
		</div>
	  </div>
	</div>
  </div>
  <script>

  $('#colorSelector').ColorPicker({

	color: '#0000ff',

	onShow: function (colpkr) {

		$(colpkr).fadeIn(500);

		return false;

	},

	onHide: function (colpkr) {

		$(colpkr).fadeOut(500);

		return false;

	},

	onChange: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	},

	onSubmit: function (hsb, hex, rgb) {

		$('#colorSelector').css('backgroundColor', '#' + hex);

		$('#colorSelector').val('#' + hex);

	}

});

  $('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo\']').attr('value', json['filename']);
			$('input[name=\'photo_source\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source').remove();
			$('#button-photo').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

  $('#button-photo1').on('click', function() {
  $('#form-photo1').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo1" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo1 input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo1 input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo1')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo1\']').attr('value', json['filename']);
			$('input[name=\'photo_source1\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source1" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source1').remove();
			$('#button-photo1').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});	

 $('#button-photo2').on('click', function() {
  $('#form-photo2').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo2" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo2 input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo2 input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo2')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo2\']').attr('value', json['filename']);
			$('input[name=\'photo_source2\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source2" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source2').remove();
			$('#button-photo2').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});	

 $('#button-photo3').on('click', function() {
  $('#form-photo3').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo3" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo3 input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo3 input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo3')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo3\']').attr('value', json['filename']);
			$('input[name=\'photo_source3\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source3" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source3').remove();
			$('#button-photo3').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});	

 $('#button-photo4').on('click', function() {
  $('#form-photo4').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo4" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo4 input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo4 input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo4')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo4\']').attr('value', json['filename']);
			$('input[name=\'photo_source4\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source4" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source4').remove();
			$('#button-photo4').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});

 $('#button-photo5').on('click', function() {
  $('#form-photo5').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo5" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo5 input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo5 input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/item/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo5')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'photo5\']').attr('value', json['filename']);
			$('input[name=\'photo_source5\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source5" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source5').remove();
			$('#button-photo5').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});	
</script>

</div> 

<?php echo $footer; ?>