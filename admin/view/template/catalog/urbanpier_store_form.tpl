<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <?php if (isset($success) && $success != '') { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-data" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-data">

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_ref_id_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_store_ref_id" value="<?php echo $entry_store_ref_id; ?>" placeholder="<?php echo $entry_store_ref_id_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_store_ref_id) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_ref_id; ?></div>
                      <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_nme_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_store_nme" value="<?php echo $entry_store_nme; ?>" placeholder="<?php echo $entry_store_nme_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_store_nme) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_nme; ?></div>
                      <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_store_add_lbl; ?></label>
                    <div class="col-sm-10">
                      <textarea name="entry_store_add" rows="5" placeholder=""  class="form-control"><?php echo $entry_store_add; ?></textarea>
                    </div>
                    <?php if ($error_entry_store_add) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_add; ?></div>
                      <?php } ?>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_city_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_store_city" value="<?php echo $entry_store_city; ?>" placeholder="<?php echo $entry_store_city_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_store_city) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_city; ?></div>
                      <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_zipcode_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_store_zipcode" value="<?php echo $entry_store_zipcode; ?>" placeholder="<?php echo $entry_store_zipcode_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_store_zipcode) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_zipcode; ?></div>
                      <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_store_contact_phone_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_store_contact_phone" value="<?php echo $entry_store_contact_phone; ?>" placeholder="<?php echo $entry_store_contact_phone_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_store_contact_phone_lbl) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_contact_phone_lbl; ?></div>
                      <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" ><?php echo $entry_store_notification_no_lbl; ?></label>
                    <div class="col-sm-4">
                      <textarea name="entry_store_notification_no" rows="5"   class="form-control"><?php echo $entry_store_notification_no; ?></textarea>
                    </div>
                    <?php if ($error_entry_store_notification_no) { ?>
                      <div class="text-danger"><?php echo $error_entry_store_notification_no; ?></div>
                      <?php } ?>

                    <label class="col-sm-1 control-label">Fulfillment modes </label>
                        <div class="col-sm-5">
                          <div class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($fullfillment_mode_array as $fullfilled => $fullfilledvaluel) { ?>
                            <div class="checkbox">
                              <label>
                                <?php if (in_array($fullfilled, $fullfillmode)) { ?>
                                <input type="checkbox" name="fullfillmode[]" value="<?php echo $fullfilledvaluel; ?>" checked="checked" />
                                <?php echo ucwords($fullfilledvaluel); ?>
                                <?php } else { ?>
                                <input type="checkbox" name="fullfillmode[]" value="<?php echo $fullfilledvaluel; ?>" />
                                <?php echo ucwords($fullfilledvaluel); ?>
                                <?php } ?>
                              </label>
                            </div>
                            <?php } ?>
                          </div>
                          <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" ><?php echo $entry_store_notification_email_lbl; ?></label>
                    <div class="col-sm-10">
                      <textarea name="notification_emails" rows="5"   class="form-control"><?php echo $notification_emails; ?></textarea>
                    </div>
                    <?php if ($error_notification_emails) { ?>
                      <div class="text-danger"><?php echo $error_notification_emails; ?></div>
                      <?php } ?>
                </div>

                 <div class="form-group required">
                    <label class="col-sm-2 control-label" ><?php echo $entry_packaging_charge_lbl; ?></label>
                    <div class="col-sm-4">
                      <input type="text" name="entry_packaging_charge" value="<?php echo $entry_packaging_charge; ?>" placeholder="<?php echo $entry_packaging_charge_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_packaging_charge) { ?>
                      <div class="text-danger"><?php echo $error_entry_packaging_charge; ?></div>
                      <?php } ?>
                    </div>
                     <label class="col-sm-2 control-label" >Delivery Charge</label>
                    <div class="col-sm-4">
                      <input type="text" name="entry_delivery_charge" value="<?php echo $entry_delivery_charge; ?>" placeholder="Delivery Charge"  class="form-control" />
                    </div>
                </div>



                <div class="form-group required">
                    <label class="col-sm-2 control-label" ><?php echo $entry_minimum_pick_time_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_minimum_pick_time" value="<?php echo $entry_minimum_pick_time; ?>" placeholder="<?php echo $entry_minimum_pick_time_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_minimum_pick_time) { ?>
                      <div class="text-danger"><?php echo $error_entry_minimum_pick_time; ?></div>
                      <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_minimum_delivery_time_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_minimum_delivery_time" value="<?php echo $entry_minimum_delivery_time; ?>" placeholder="<?php echo $entry_minimum_delivery_time_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_minimum_delivery_time) { ?>
                      <div class="text-danger"><?php echo $error_entry_minimum_delivery_time; ?></div>
                      <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_minimum_order_value_lbl; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="entry_minimum_order_value" value="<?php echo $entry_minimum_order_value; ?>" placeholder="<?php echo $entry_minimum_order_value_lbl; ?>" id="input-model" class="form-control" />
                      <?php if ($error_entry_minimum_order_value) { ?>
                      <div class="text-danger"><?php echo $error_entry_minimum_order_value; ?></div>
                      <?php } ?>
                    </div>
                </div>


                <div class="form-group ">
                    <div class="col-sm-12">
                         <label style="margin-left: 40%;margin-right: 10%;" ><?php  echo ucwords('(For Geo Longitude and Geo latitude please <a target= "blank" href = "https://www.latlong.net/ ">Click here </a> )') ?> </label>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_geo_longitude_lbl; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="entry_geo_longitude" value="<?php echo $entry_geo_longitude; ?>" placeholder="<?php echo $entry_geo_longitude_lbl; ?>" id="input-model" class="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12" style="margin-top: 3%;">
                        <label class="col-sm-2 control-label" for="input-model" ><?php echo $entry_geo_latitude_lbl; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="entry_geo_latitude" value="<?php echo $entry_geo_latitude; ?>" placeholder="<?php echo $entry_geo_latitude_lbl; ?>" id="input-model" class="form-control" />
                        </div>
                    </div>
                </div>




                <div class="form-group">
                     <div class="col-sm-12">
                         <label style="margin-left: 40%;margin-right: 10%;" ><?php  echo ucwords('(If you this feature please insert all day and time)') ?> </label>
                      </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label" ><?php  echo $entry_ordering_timing_lbl;; ?></label>
                        <div class="col-sm-2">
                            <select name="day_open_store" id="day_open_store" class="form-control"  tabindex="15">
                                <?php foreach ($days_name as $daykey =>  $dayvalue) { ?>
                                    <option value="<?php echo $daykey ?>" selected="selected"><?php echo $dayvalue; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label" ><?php echo $entry_ordering_start_timing_lbl; ?></label>
                        <div class="col-sm-2">
                            <input type="text" name="store_intime" id="store_intime"  size="6" class="time form-control"/>
                        </div>
                        <label class="col-sm-2 control-label" ><?php echo $entry_ordering_end_timing_lbl; ?></label>
                        <div class="col-sm-2">
                            <input type="text" name="store_outtime" id="store_outtime"  size="6" class="time form-control"  />
                        </div>
                        <a onclick="addtimes()" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                    </div>

                    <div class="col-sm-12" style="margin-top: 5%;">
                        <table id="tbcharge" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                  <td class="text-center"><?php echo "Days"; ?></td>
                                  <td class="text-center"><?php echo "Start Time"; ?></td>
                                  <td class="text-center"><?php echo "End Time"; ?></td>
                                  <td class="text-center">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($store_all) { ?>

                                    <?php foreach($store_all as $store_timingkey => $store_timing) {  ?>
                                         <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][id]"  value="<?php echo $store_timing['id']; ?>">
                                        <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][day]"  value="<?php echo $store_timing['day']; ?>">
                                        <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][start_time]"  value="<?php echo $store_timing['end_time']; ?>">
                                        <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][end_time]"  value="<?php echo $store_timing['end_time']; ?>">

                                        <tr>
                                            <td class="text-center"><?php echo $store_timing['day']; ?></td>
                                            <td class="text-center"><?php echo $store_timing['start_time']; ?></td>
                                            <td class="text-center"><?php echo $store_timing['end_time']; ?></td>
                                            <td>
                                                <a type="button" class="btn btn-danger" onclick='delete_store_day(<?php echo $store_timing['id'] ?>);'><i class="fa fa-minus-circle"></i></a>
                                            </td>
                                           
                                           
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">No Result Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                 <div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label" >Platform</label>
                        <div class="col-sm-2">
                            <select name="platform_name" id="platform_name" class="form-control"  tabindex="15">
                                <?php foreach ($platform_datas_array as $pkey =>  $pvalue) { ?>
                                    <option value="<?php echo $pkey ?>"><?php echo $pvalue; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label" >Platform Registeration Link</label>
                        <div class="col-sm-4">
                          <!--  <input type="text" name="platform_link" id="platform_link"  size="10" class="form-control"/> -->
                             <textarea name="platform_link" rows="5"   id="platform_link" class="form-control"></textarea>
                        </div>
                      
                    </div>
                     <div class="col-sm-12">
                        <div class="col-sm-2 pull-right">
                            <a onclick="addplatform()" class="btn btn-primary "><i class="fa fa-plus"></i></a>
                        </div>
                          <label class="col-sm-2 control-label" >Platform Store ID</label>
                            <div class="col-sm-2">
                                <input type="text" name="platform_store_id" id="platform_store_id"  size="3" class="form-control"  />
                            </div>
                        
                    </div>

                    <div class="col-sm-12" style="margin-top: 5%;">
                        <table id="tblplatfrom" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                  <td class="text-center"><?php echo "Platform"; ?></td>
                                  <td class="text-center"><?php echo "Registeration Link"; ?></td>
                                  <td class="text-center"><?php echo "Store Id"; ?></td>
                                  <td class="text-center">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($platform_datas) { ?>

                                    <?php foreach($platform_datas as $plkey => $plvalue) {  ?>
                                         <input type= "hidden"  name="platform_datas[<?php echo $plkey ?>][id]"  value="<?php echo $plvalue['id']; ?>">
                                        <input type= "hidden"  name="platform_datas[<?php echo $plkey ?>][platform_name]"  value="<?php echo $plvalue['platform_name']; ?>">
                                        <input type= "hidden"  name="platform_datas[<?php echo $plkey ?>][platform_link]"  value="<?php echo $plvalue['platform_link']; ?>">
                                        <input type= "hidden"  name="platform_datas[<?php echo $plkey ?>][platform_store_id]"  value="<?php echo $plvalue['platform_store_id']; ?>">

                                        <tr>
                                            <td class="text-center"><?php echo $plvalue['platform_name']; ?></td>
                                            <td class="text-center"><?php echo $plvalue['platform_link']; ?></td>
                                            <td class="text-center"><?php echo $plvalue['platform_store_id']; ?></td>
                                            <td>
                                                <a type="button" class="btn btn-danger" onclick='delete_platform(<?php echo $plvalue['id'] ?>);'><i class="fa fa-minus-circle"></i></a>
                                            </td>
                                           
                                           
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">No Result Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_ordering_enabled_lbl; ?></label>
                    <div class="col-sm-10">
                      <select name="entry_ordering_enabled" id="input-status" class="form-control">
                        <?php if ($entry_ordering_enabled== 'true') { ?>
                            <option value="true" selected="selected"><?php echo $text_enabled; ?></option>
                             <option value="false" ><?php echo $text_disabled; ?></option>
                        <?php } elseif($entry_ordering_enabled == 'false') { ?>
                             <option value="true" ><?php echo $text_enabled; ?></option>
                            <option value="false" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="true" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="false" ><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
  <script type="text/javascript"><!--
// Manufacturer
 $('.time').timepicker({timeFormat: 'hh:mm:ss'});

 $.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

    function addtimes(){
        var day_open_store = $('#day_open_store').val() || '';
        var store_intime = $('#store_intime').val() || '';
        var store_outtime = $('#store_outtime').val() || ''; 
         if(day_open_store == '' || store_intime == '' || store_outtime == ''){
            alert('Please fill all data');
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/urbanpier_store/gettiming&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                day_open_store: day_open_store,
                store_intime: store_intime,
                store_outtime: store_outtime,
            },
            success: function(json) {
                if (json.html != '' ) {
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                    $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                } 
                
            }
        });
    }


   function delete_store_day(id){
        if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
            $.ajax({
                url:'index.php?route=catalog/urbanpier_store/deleterecord&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {   
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                     $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
             return false;
        }
    }

    function addplatform(){
        
        var platform_name = $('#platform_name').val() || '';
        var platform_link = $('#platform_link').val() || '';
        var platform_store_id = $('#platform_store_id').val() || ''; 
       /* console.log(platform_name);
        console.log(platform_link);
        console.log(platform_store_id);*/

         if(platform_name == '' || platform_link == '' || platform_store_id == ''){
            alert('Please fill all data');
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/urbanpier_store/getplatform&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                platform_name: platform_name,
                platform_link: platform_link,
                platform_store_id: platform_store_id,
            },
            success: function(json) {
                if (json.html != '' ) {
                     $('#tblplatfrom tbody').html('');
                    $('#tblplatfrom tbody').append(json.html);
                    $('#platform_name').val('');
                    $('#platform_link').val('');
                    $('#platform_store_id').val('');
                    return false;
                } 
                
            }
        });
    }


   function delete_platform(id){
            if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
                $.ajax({
                    url:'index.php?route=catalog/urbanpier_store/deleteplatform&token=<?php echo $token; ?>'+'&id='+id,
                    method: "POST",
                    dataType: 'json',
                    success: function(json)
                    {   
                         $('#tblplatfrom tbody').html('');
                        $('#tblplatfrom tbody').append(json.html);
                         $('#platform_name').val('');
                        $('#platform_link').val('');
                        $('#platform_store_id').val('');
                        return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                 return false;
            }
    }

$('input[name=\'manufacturer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					manufacturer_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['manufacturer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'manufacturer\']').val(item['label']);
		$('input[name=\'manufacturer_id\']').val(item['value']);
	}
});

// Category
$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');

		$('#product-category' + item['value']).remove();

		$('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Filter
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#product-filter' + item['value']).remove();

		$('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Downloads
$('input[name=\'download\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['download_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'download\']').val('');

		$('#product-download' + item['value']).remove();

		$('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-download').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Related
$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');

		$('#product-related' + item['value']).remove();

		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-related').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script>






  
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>
