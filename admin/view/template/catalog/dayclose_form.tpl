<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
	<div class="page-header">
		<div class="container-fluid">
			<div id="overlay">
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>
			</div>
			<div class="pull-right sav">
				<button id="savsp" type="submit" form="form-bill_merge" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo 'Day Close'; ?></h1>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="alert alert-danger" style="display: none;" id="warning_div">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Unbilled Tables Present. Do you Want to Force Close'; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="alert alert-danger" style="display: none;" id="warning_div_1">
			<i class="fa fa-exclamation-circle"></i> <?php echo 'Warning: Cannot Close Current Date'; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-dayclose" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'Date to Close'; ?></label>
						<div class="col-sm-3">
							<input tabindex="1" readonly="readonly" type="text" name="open_date" id="open_date" value="<?php echo $open_date ?>" class="form-control" />
							<input type="hidden" name="input_warning" id="input_warning" value="0" />
							<div class="" style="display: none;" id="dayclose_error"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
<progress value="0" max="100" id="prog" style="display: none;"></progress>
<!-- <div id="here"></div> -->
<style>
#savsp{
	display:block;
	border:solid #ccc 1px;
  	cursor: pointer;
}

#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}
</style>

<script type="text/javascript">
$(document).on('keypress','input,select', function (e) {
//$('input,select').on('keypress', function (e) {
	if (e.which == 13) {
		e.preventDefault();
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		if (!$next.length) {
			$next = $('[tabIndex=1]');
		}
		$next.focus();
	}
});

$(document).on('keyup','#save_button', function (e) {
	if (e.keyCode == 33) {
		input_warning = $('#input_warning').val();
		if(input_warning == 0){
			save();
		} else {
			save1();
		}
	}
});

$(document).ready(function() {
	open_date = $('#open_date').val();
	if(open_date != '' && open_date != '0' && open_date != undefined){
		$('.sav').html('');
		html = '<a tabindex="2" id="save_button" onclick="save()" title="Update" class="btn btn-primary">Update</a>';
		$('.sav').append(html);


	}
	$('#save_button').focus();
});

// function test(argument) {
	
// 	$.ajax({
// 		type: "POST",
// 		url: 'index.php?route=catalog/order/get_pending_datas&token=<?php echo $token; ?>',
// 		dataType: 'json',
// 		data: {"data":"check"},
// 		beforeSend: function() {
// 	        // setting a timeout
// 	        $('"prog').show();
// 	    },
// 		success: function(data){
// }

function save(){
	$('#save_button').attr('onclick','').unbind('click');
	action = '<?php echo $action; ?>';
	action = action.replace("&amp;", "&");
	action = action.replace("&amp;", "&");
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});
	$.post(action, $('#form-dayclose').serialize()).done(function(data) {
		var parsed = JSON.parse(data);
		
		if(parsed.info == 1){
			$('#save_button').prop("onclick", null).off("click");
			$('#warning_div').hide();
			$('.sucess').html('');
			$('.sucess').append(parsed.done);
			$('#input_warning').val(0);
			
		} else {
			if(parsed.info == 2){
				$('#warning_div_1').show();
				//html = '<a tabindex="2" id="save_button" onclick="save1()" title="Yes" class="btn btn-primary">Yes</a>';
				//$('.sav').html('');
				//$('.sav').append(html);
				$('#save_button').hide();	
				//$('#input_warning').val(1);
			} else {
				$('#warning_div').show();
				html = '<a tabindex="2" id="save_button" onclick="save1()" title="Yes" class="btn btn-primary">Yes</a>';
				$('.sav').html('');
				$('.sav').append(html);
				$('#save_button').focus();	
				$('#input_warning').val(1);
			}
		}

	}).done(function() {
		setTimeout(function(){
			$("#overlay").fadeOut(300);
		},500);
	});
}

function save1(){
	$('#save_button').attr('onclick','').unbind('click');
	action = '<?php echo $action1; ?>';
	action = action.replace("&amp;", "&");
	action = action.replace("&amp;", "&");
	$(document).ajaxSend(function() {
		$("#overlay").fadeIn(300);　
	});
	$.post(action, $('#form-dayclose').serialize()).done(function(data) {
		var parsed = JSON.parse(data);
		if(parsed.info == 1){
			$('#warning_div').hide();
			$('.sucess').html('');
			$('.sucess').append(parsed.done);
		}
	}).done(function() {
		setTimeout(function(){
			$("#overlay").fadeOut(300);
		},500);
	});
}
</script>