<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div  class="col-sm-3">
								       		<label>Bill No</label>
									    	<input type="text" id="filter_billno" name="filter_billno" value="<?php echo $billno ?>" class="form-control" placeholder=" Bill NO">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10" style="display:none;">
				 		<button  id="print" type="button" class="btn btn-primary">Print</button>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 		<button id="excel" type="button" class="btn btn-primary">Export Excel</button>
				 	</div>
					<div class="col-md-12 col-md-offset">
					<!-- <h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4> -->
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Billwise Item Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						
						<tr>
			  				<th>REF.NO.</th>
			  				<th>F.B.No.</th>
			  				<th>F.TOT</th>
			  				<th>GST</th>
			  				<th>Packaging</th>
			  				<th>DIS</th>
			  				<th>Net Total</th>
			  				<th>L.B.No.</th>
			  				<th>L.TOT</th>
			  				<th>VAT</th>
			  				<th>DIS</th>
			  				<th>Net Total</th>
			  				<th>F&B Total</th>
			  				<th>Pay By</th>
			  				<th>Waiter</th>
			  				<th>Captain</th>
			  				<th>Tbl No.</th>
			  				<th>In</th>
			  				<th>Out</th>
			  				<th>B.Date</th>
			  				<th>Action</th>
			  			</tr>
			  			<?php foreach($order_datas as $order_data) { ?>
			  				<tr>
			  					<td><?php echo $order_data['order_no'] ?></td>
			  					<td><?php echo $order_data['food_bill_no'] ?></td>
			  					<td><?php echo $order_data['ftotal'] ?></td>
			  					<td><?php echo $order_data['gst'] ?></td>
			  					<td><?php echo $order_data['packaging'] ?></td>
			  					<td><?php echo $order_data['ftotalvalue']?></td>
			  					<?php if($INCLUSIVE == 1){ ?>
			  						<td><?php echo $order_data['ftotal']?></td>
			  					<?php } else { ?>
			  						<td><?php echo $order_data['ftotal'] + $order_data['gst'] ?></td>
			  					<?php } ?>
			  					<td><?php echo $order_data['liq_bill_no'] ?></td>
			  					<td><?php echo $order_data['ltotal'] ?></td>
			  					<td><?php echo $order_data['vat'] ?></td>
			  					<td><?php echo $order_data['ltotalvalue']?></td>
			  					<?php if($INCLUSIVE == 1){ ?>
			  						<td><?php echo $order_data['ltotal']?></td>
			  					<?php } else { ?>
			  						<td><?php echo $order_data['ltotal'] + $order_data['vat'] ?></td>
			  					<?php } ?>
			  					<td><?php echo $order_data['grand_total']?></td>
			  					<?php if($order_data['pay_card'] != 0) { ?>
			  						<td>Card</td>
			  					<?php } else if($order_data['pay_cash'] != 0) { ?>
			  						<td>Cash</td>
			  					<?php } else if($order_data['pay_online'] != 0) { ?>
			  						<td>Online</td>
			  					<?php } else if($order_data['pay_cash'] != 0 && $order_data['pay_card'] != 0) { ?>
			  						<td>Cash+Card</td>
			  					<?php } else { ?>
			  						<td>Pending</td>
			  					<?php } ?>
			  					<td><?php echo $order_data['waiter'] ?></td>
			  					<td><?php echo $order_data['captain'] ?></td>
			  					<td><?php echo $order_data['t_name'] ?></td>
			  					<td><?php echo $order_data['time_added'] ?></td>
			  					<td><?php echo $order_data['out_time'] ?></td>
			  					<td><?php echo $order_data['bill_date'] ?></td>
			  					<td> 
		  						
		  							<a href="<?php echo $order_data['href'] ?>" class="btn btn-primary" >Duplicate</a>
				  					
			  					</td>
			  					<td style="display: none"><?php echo $order_data['order_id'] ?></td>
			  				</tr>
			  			<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/billwiseitem_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/prebill_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#excel').on('click', function() {
			var url = 'index.php?route=catalog/prebill_report/excel&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	

</div>
<?php echo $footer; ?>





























