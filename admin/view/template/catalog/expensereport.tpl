<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<div class="col-sm-offset-1">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>Paid Type</label>
								    	  	<select class="form-control" name="filter_paidtype" id="input-filter_paidtype">
									         	<option selected="selected" value="">All</option>
									          	<?php foreach($paidtype as $key => $value){?>
									          		<?php if($filter_paidtype == $key && $filter_paidtype != ''){ ?>
									          			<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
									       		   	<?php } else { ?>
									          		<option value="<?php echo $key ?>"><?php echo $value?></option>
									          	<?php } ?>
									          <?php } ?>
								         	</select>
									    </div>
									     <div class="col-sm-3">
									    	<label>Type</label>
									    	   	<select class="form-control" name="filter_type" id="input-filter_type">
										        	<option selected="selected" value="">All</option>
										         	 	<?php foreach($type as $key => $value){?>
												          	<?php if($filter_type == $key && $filter_type != ''){ ?>
												          		<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
												          	<?php } else { ?>
										          	<option value="<?php echo $key ?>"><?php echo $value?></option>
										          	<?php } ?>
										          <?php } ?>
									          </select>
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div>
					<div class="col-sm-6 col-sm-offset-3">
					<center style = "display:none;">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Expense Report</b></h3></center>
					<!--<h5>From : <?php //echo $startdate; ?></h5>
					<h5 style="text-align: right;margin-top: -20px;">To : <?php //echo $enddate; ?></h5><br>-->
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;" colspan="3">Expense Opening:</th>
							<th style="text-align: center;" colspan="3"><?php echo $opening_bal ;?></th>
						</tr>
						<tr>
							<td><?php echo "Date" ;?></td>
							<td><?php echo "Exp.No" ;?></td>
							<td><?php echo "Account" ;?></td>
							<td><?php echo "Paid IN" ;?></td>
							<td><?php echo "Paid OUT" ;?></td>
							<td><?php echo "Remarks" ;?></td>
						</tr>
						<?php if($expensedatas){ ?>	
							<?php foreach($expensedatas as $expensedata) {?>
								<tr>
									<td><?php echo $expensedata['date'] ;?></td>
									<td><?php echo  $expensedata['exp_no'] ;?></td>
									<td><?php echo  $expensedata['account_name'] ;?></td>
									<td><?php echo  $expensedata['paid_in_amt'] ;?></td>
									<td><?php echo  $expensedata['paid_out_amt'] ;?></td>
									<td><?php echo  $expensedata['remarks'] ;?></td>	
								</tr>
							<?php } ?>
							<tr>
								<td colspan="3"><?php echo 'Total Opening' ;?></td>
								<td colspan="3"><?php echo $opening_bal ;?></td>
							</tr>
							<tr>
								<td colspan="3"><?php echo 'Total Paid IN' ;?></td>
								<td colspan="3"><?php echo $expensedata['total_paid_in'] ;?></td>
							</tr>
							<tr>
								<td colspan="3"><?php echo 'Net Total' ;?></td>
								<td colspan="3"><?php echo $expensedata['net_total'] ;?></td>
							</tr>
							<tr>
								<td colspan="3"><?php echo 'Total Paid OUT' ;?></td>
								<td colspan="3"><?php echo $expensedata['total_paid_out'] ;?></td>
							</tr>
							<tr>
								<td colspan="3"><?php echo 'Total Closing' ;?></td>
								<td colspan="3"><?php echo $expensedata['closing_total'] ;?></td>
							</tr>	
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/expensereport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_type = $('select[name=\'filter_type\']').val();
		  var filter_paidtype = $('select[name=\'filter_paidtype\']').val();
		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_type) {
			  url += '&filter_type=' + encodeURIComponent(filter_type);
		  }

		  if (filter_paidtype) {
			  url += '&filter_paidtype=' + encodeURIComponent(filter_paidtype);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>