<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

    <div class="page-header">

	    <div class="container-fluid">

	        <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" 

	             class="btn btn-primary"><i class="fa fa-plus"></i></a>

		         <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>

	        </div>

	        <h1><?php echo $heading_title; ?></h1>

	        <ul class="breadcrumb">

		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

		        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

		        <?php } ?>

	        </ul>

	    </div>

  	</div>

  	<div class="container-fluid">

	    <?php if ($error_warning) { ?>

	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

	    <button type="button" class="close" data-dismiss="alert">&times;</button>

	</div>

	    <?php } ?>

	    <?php if ($success) { ?>

	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

	    <button type="button" class="close" data-dismiss="alert">&times;</button>

	</div>

	    <?php } ?>

	<div class="panel panel-default">

	<div class="panel-heading">

		<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>

	</div>

	<div class="panel-body">

		<div class="well">

		    <div class="row">

			    <div class="col-sm-4">

			        <div class="form-group">

				        <label class="control-label" for="input-name"><?php echo 'Customer Name'; ?></label>

				        <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo ' Name'; ?>" id="input-filter_name" class="form-control" />

				        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>

			         </div>

			    </div>

		    </div>

		</div>

		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">

		  	<div class="table-responsive">

				<table class="table table-bordered table-hover">

				  	<thead>

						<tr>

						  	<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').

						  		prop('checked', this.checked);" /></td>

						  	<td class="text-left">

								<?php if ($sort == 'name') { ?>

									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>

								<?php } else { ?>

									<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>

								<?php } ?>

							</td>

							<td>

								<a href="<?php echo "" ?>"><?php echo 'Discription'; ?></a>

						 	</td>

						 	

						  	<td class="text-right"><?php echo $column_action; ?></td>

						</tr>

			  		</thead>

			  	<tbody>

				<?php if ($activitys) { ?>

				<?php foreach ($activitys as $activity) { ?>

				<tr>

				  	<td class="text-center">

					<?php if (in_array($activity['id'], $selected)) { ?>

						<input type="checkbox" name="selected[]" value="<?php echo $activity['id']; ?>" checked="checked" />

					<?php } else { ?>

						<input type="checkbox" name="selected[]" value="<?php echo $activity['id']; ?>" />

					<?php } ?></td>

				  	<td class="text-left"><?php echo $activity['customer_name']; ?></td>

				  	<td class="text-left"><?php echo $activity['discription']; ?></td>

				  	<td class="text-right">

				  		<a href="<?php echo $activity['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>

				</tr>

				<?php } ?>

				<?php } else { ?>

				<tr>

				  	<td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>

				</tr>

				<?php } ?>

				</tbody>

			</table>

		</div>

	</form>

		<div class="row">

		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>

		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>

		</div>

	  </div>

	</div>

  </div>

</div>

<script type="text/javascript">

$('#button-filter').on('click', function() {

  var url = 'index.php?route=catalog/testimonal&token=<?php echo $token; ?>';



  var filter_name = $('input[name=\'filter_name\']').val();

  if (filter_name) {

	var filter_name_id = $('input[name=\'filter_name_id\']').val();

	if (filter_name_id) {

	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);

	  }

	  url += '&filter_name=' + encodeURIComponent(filter_name);

  }



  



  location = url;

});



$('input[name=\'filter_name\']').autocomplete({

  delay: 500,

  source: function(request, response) {

    $.ajax({

      url: 'index.php?route=catalog/testimonal/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),

      dataType: 'json',

      success: function(json) {   

        response($.map(json, function(item) {

          return {

            label: item.customer_name,

            value: item.id

          }

        }));

      }

    });

  }, 

  select: function(event, ui) {

    $('input[name=\'filter_name\']').val(ui.item.label);

    $('input[name=\'filter_department_id\']').val(ui.item.value);

		

    return false;

  },

  focus: function(event, ui) {



    return false;

  }

});

</script></div>

<?php echo $footer; ?>