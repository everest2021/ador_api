<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
			   <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			   <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			   <h1><?php echo $heading_title; ?></h1>
			   <ul class="breadcrumb">
				   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
			   </ul>
		</div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
		<?php } ?>
	   <div class="panel panel-default">
		   <div class="panel-heading">
			   <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		   		<div class="form-group">
				   <label class="col-sm-3 control-label"><?php echo 'Code'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="code" readonly="readonly" value="<?php echo $code; ?>" placeholder="<?php echo 'Code'; ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Description'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="description" value="<?php echo $description; ?>" placeholder="<?php echo 'Description'; ?>" class="form-control" />
						<?php if ($error_name ) { ?>
						<div class="text-danger"><?php echo $error_name ; ?></div>
						<?php } ?>
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Printer Name'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="printer" value="<?php echo $printer; ?>" placeholder="<?php echo 'Printer'; ?>" class="form-control" />
						<?php if ($error_name ) { ?>
						<div class="text-danger"><?php echo $error_name ; ?></div>
						<?php } ?>
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Printer Type'; ?></label>
					<div class="col-sm-3">
					<select name="printertype" class="form-control">
					<?php foreach($printertypes as $key => $value) { ?>
						<?php if($key == $printertype) { ?>
							<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
						<?php } else { ?>
							<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php } ?>
					<?php } ?>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Master Kot'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="master_kotprint" value="<?php echo $master_kotprint; ?>" placeholder="<?php echo 'Master Kot'; ?>" class="form-control" />
					</div>
				</div> 

				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Subcategory'; ?></label>
					<div class="col-sm-5">
				    	<div class="well well-sm" style="height: 150px; overflow: auto;">
			                <?php foreach ($subcategorys as $skey => $svalue) { ?>
				                <div class="checkbox">
				                  	<label>
				                    	<?php if (in_array($svalue['category_id'], $subcategory)) { ?>
				                    		<input type="checkbox" name="subcategory[]" value="<?php echo $svalue['category_id']; ?>" checked="checked" />
				                    		<?php echo $svalue['name']; ?>
				                    	<?php } else { ?>
				                    		<input type="checkbox" name="subcategory[]" value="<?php echo $svalue['category_id']; ?>" />
				                    		<?php echo $svalue['name']; ?>
				                    	<?php } ?>
				                  	</label>
				                </div>
			                <?php } ?>
			            </div>
			            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo 'Un Select All'; ?></a></div>
			    	</div>
				</div>  
		  	</form>
		</div>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>