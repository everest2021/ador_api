<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-3">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>Store</label>
								     	<input type="text" readonly="readonly" name="filter_storename" value="<?php echo $store_name ?>" class="form-control">
								     	<input type="hidden" name="filter_storeid" value="<?php echo $store_id ?>">
								    </div>
									<div class="col-sm-12">
										<br>
										<center><input type="submit" class="btn btn-primary" value="Show"></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
						<h5 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h5>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h5 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h5>
						<center><h5><b> Inward Report</b></h5></center>
						<h5>From : <?php echo $startdate; ?></h5>
						<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
						<?php if ($final_data) {?>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
							<thead>
								<tr>
									<td style="text-align: left;"><?php echo 'Sr.No'; ?></td>
									<td style="text-align: left;"><?php echo 'Inward Number'; ?></td>
									<td style="text-align: left;"><?php echo 'Item Name'; ?></td>
									<td style="text-align: left;"><?php echo 'Quantity'; ?></td>
									<td style="text-align: left;"><?php echo 'Notes'; ?></td>		            
								</tr>
							</thead>
							<tbody>
								<?php foreach($final_data as $fkey =>$fvalue) { ?>
									<tr>
										<td class="left" colspan="7"><strong>Store Name:<?php echo $fvalue['outlet_name']; ?></strong></td>
									</tr>
									<?php foreach($fvalue['item_data'] as $fkey =>$result ) { ?>
										<tr>			          
											<td class="left"><?php echo $result['i']; ?>
											</td>
											<td class="left"><?php echo $result['po_number']; ?>                 
											</td>
											<td class="left"><?php echo $result['item_name']; ?>
											</td>
											<td class="left"><?php echo $result['quantity']; ?>
											</td>
											<td class="left"><?php echo $result['notes']; ?>
											</td>
										</tr>
									<?php } ?>			            
										<tr colspan='7'; style="border-top:2px solid #000000;padding-top:#000000;">
											<td colspan='7'; style="border-bottom:2px solid #000000;padding-bottom:#000000;" ></td>
										</tr>
								<?php }  ?>
							</tbody>
						</table>
						<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportstock/itemwisereport&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_storename = $('select[name=\'filter_storename\']').val();
		 

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_storename) {
			  url += '&filter_storename=' + encodeURIComponent(filter_storename);
		  }


		  location = url;
		  setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#type').on('change', function() {
			$('#form').submit();
		});
	</script>
</div>
<?php echo $footer; ?>