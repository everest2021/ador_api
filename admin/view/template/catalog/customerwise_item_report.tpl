<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<div class="col-sm-offset-2" >
					       		<center><h4><b>Select Date</b></h4></center><br>
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div  class="col-sm-3">
								       		<label>Customer Name</label>
									    	<input type="text" id="filter_cust_name" name="filter_cust_name" value="<?php echo $filter_cust_name ?>" class="form-control" placeholder=" Customer Name">
									    	<input type="hidden" id="filter_cust_id" name="filter_cust_id" value="<?php echo $filter_cust_id ?>" class="form-control" placeholder=" Customer Id">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10">
				 		<button style="display: none;" id="print" type="button" class="btn btn-primary">Print</button>
				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-8 col-sm-offset-2">
					<!-- <h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4> -->
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Customer wise Sale Report</b></h3></center>
					<h3>From : <?php echo date('d/m/Y',strtotime($startdate)); ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo date('d/m/Y',strtotime($enddate)); ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<?php $grandtotal=0;  ?>
						<tr>
							<th style="text-align: center;">Bill No</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;" colspan="2">Customer Name</th>
							<th style="text-align: center;" colspan="2">Customer Address</th>
							<th style="text-align: center;">Contact No</th>
							<th style="text-align: right;">Grand Total</th>
						</tr>
						<tr>
							<th style="text-align: center;">Sr No</th>
							<th style="text-align: center;">Kot No</th>
							<th style="text-align: center;" colspan="3">Name</th>
							<th style="text-align: center;">rate</th>
							<th style="text-align: right;">Qty</th>
							<th style="text-align: right;">Amt</th>
						</tr>
						  	<?php foreach ($final_datass as $key => $value) { //echo'<pre>';print_r($value); exit;?>
						<tr >
							<th style="text-align: center;border-right: none;border-left: none;" colspan="8"></th>
						</tr>
						
					  		<tr>
						  		<td><?php echo $value['billno'] ?></td>
						  		<td><?php echo date('d-m-Y',strtotime($value['bill_date'])); ?></td>
						  		<td colspan="2"><?php echo $value['cust_name']; ?></td>
						  		<td colspan="2"><?php echo $value['cust_address']; ?></td>
						  		<td><?php echo $value['cust_contact'] ?></td>
						  		<td style="text-align: right;"><?php echo $value['grand_total']; ?></td>
						  	</tr> 
				  			<?php $grandtotal = $grandtotal + $value['grand_total']; ?>
				  			
					  			<?php foreach ($value['sub_data'] as $skey => $svalue) { //echo'<pre>';print_r($svalue); exit;?>
					  			<tr>
							  		<td><?php echo $svalue['sr_no']; ?></td>
							  		<td><?php echo $svalue['kot_no']; ?></td>
							  		<td colspan="3"><?php echo $svalue['name']; ?></td>
							  		<td><?php echo $svalue['rate']; ?></td>
							  		<td style="text-align: right;"><?php echo $svalue['qty'] ?></td>
							  		<td style="text-align: right;"><?php echo $svalue['amt'] ?></td>
							  	</tr>
							  	<?php } ?>
						  	<?php } ?>
						  	<tr>
							  	<td style="text-align: right;" colspan="7"><?php echo "Grand Total" ?></td>

							  	<td style="text-align: right;"><?php echo $grandtotal; ?></td>
							</tr> 
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/customerwise_item_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_cust_id = $('input[name=\'filter_cust_id\']').val();
		  var filter_cust_name = $('input[name=\'filter_cust_name\']').val();
		  //alert(filter_cust_name);

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_cust_id) {
			  url += '&filter_cust_id=' + encodeURIComponent(filter_cust_id);
		  }

		  //alert(url);
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/customerwise_item_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#filter_cust_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		//filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/customerwise_item_report/autocomplete_name&token=<?php echo $token; ?>&filter_cust_name=' +  encodeURIComponent(request.term),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_cust_id').val(ui.item.value);
		  		$('#filter_cust_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>