<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 <!-- 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
					<center style = "display:none;">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Cancelled Kot Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">K.Ref No</th>
							<th style="text-align: center;">Kot No.</th>
							<th style="text-align: center;">Item</th>
							<th style="text-align: center;">Quantity</th>
							<th style="text-align: center;">Amount</th>
							<th style="text-align: center;">User</th>
							<th style="text-align: center;">Time</th>
							<th style="text-align: center;">Cancel Reason</th>

						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php $totalqty = 0; $totalamt = 0; ?>
							<?php foreach($cancelkotdatas as $cancelkotdata => $value) { ?>
								<tr>
									<td colspan="8"><b><?php echo $cancelkotdata ?></b></td>
								</tr>
								<?php foreach($value as $data) { ?>
									<tr>
										<td><?php echo $data['order_id'] ?></td>
										<td><?php echo $data['kot_no'] ?></td>
										<td><?php echo $data['name'] ?></td>
										<td><?php echo $data['qty'] ?></td>
										<td><?php echo $data['amt'] ?></td>
										<td><?php echo $data['login_name'] ?></td>
										<td><?php echo $data['time'] ?></td>
										<td><?php echo $data['cancel_kot_reason'] ?></td>
									</tr>
									<?php $totalqty = $totalqty + $data['qty']; $totalamt = $totalamt + $data['amt']; ?> 
								<?php } ?>
							<?php } ?>
							<tr>
								<td colspan="3">Total</td>
								<td><?php echo $totalqty;?></td>
								<td><?php echo $totalamt;?></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>