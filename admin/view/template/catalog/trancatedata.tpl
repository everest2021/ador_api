<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
					<?php if ($success != '') { ?>
				 		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

						<button type="button" class="close" data-dismiss="alert">&times;</button>

					</div>
				 	<?php }?>
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-4">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $filter_enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								   	<div style = "display:none"; class="col-sm-1">
								   		<label>Type</label>
								    	<select class="form-control" name="filter_type" id="filter_type">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($types as $key => $value) { ?>
								    			<?php if($key == $filter_type) { ?>
								    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
								    			<?php } else { ?>
								    				<option value="<?php echo $key ?>"><?php echo $value?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								   	<div style = "display:none";  class="col-sm-2">
								       	<label>Name</label>
									    <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name ?>" class="form-control" placeholder=" name">
									    <input type="hidden" id="filter_id" name="filter_id" value="<?php echo $filter_id ?>" class="form-control">
									</div>
									<div class="col-sm-12">
										<br>
										<center><a id ="filter" class="btn btn-primary" value="Show">Submit</a>
										<a style = "display:none"; id="export" type="button" class="btn btn-primary">Export</a></center>

									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
				 	
				 	
					
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/trancatedata&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_name = $('input[name=\'filter_name\']').val();
		 	var filter_id = $('input[name=\'filter_id\']').val();
		 	var filter_type = $('select[name=\'filter_type\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_name) {
				if (filter_id) {
				  	url += '&filter_id=' + encodeURIComponent(filter_id);
					url += '&filter_name=' + encodeURIComponent(filter_name);
			  	}
		  	}
		  	if (filter_type) {
				url += '&filter_type=' + encodeURIComponent(filter_type);
		  	}
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  	var url = 'index.php?route=catalog/captain_and_waiter/export&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_name = $('input[name=\'filter_name\']').val();
		 	var filter_id = $('input[name=\'filter_id\']').val();
		 	var filter_type = $('select[name=\'filter_type\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_name) {
				if (filter_id) {
				  	url += '&filter_id=' + encodeURIComponent(filter_id);
					url += '&filter_name=' + encodeURIComponent(filter_name);
			  	}
		  	}
		  	if (filter_type) {
				url += '&filter_type=' + encodeURIComponent(filter_type);
		  	}
		  	location = url;
		});

		
		$('#type').on('change', function() {
			$('#form').submit();
		});

		$('#filter_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/captain_and_waiter/name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_type=' +  encodeURIComponent(filter_type),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_id').val(ui.item.value);
		  		$('#filter_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>
</div>
<?php echo $footer; ?>