<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	           <h1><?php echo $heading_title; ?></h1>
	           <ul class="breadcrumb">
		           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		            <?php } ?>
	           </ul>
	    </div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	       <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
	    <?php } ?>
	   <div class="panel panel-default">
	       <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	        </div>
	   <div class="panel-body">
		   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		   	<div class="form-group ">
			   <label class="col-sm-3 control-label"><?php echo 'Code'; ?></label>
			    <div class="col-sm-3">
			        <input type="text" name="code" value="<?php echo $code; ?>" placeholder="<?php echo 'Code'; ?>" class="form-control" />
		            <?php if ($error_code) { ?>
		            	<div class="text-danger"><?php echo $error_code; ?></div>
		            <?php } ?>
			    </div>
			    <label class="col-sm-3 control-label"><?php echo 'Supplier'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="supplier" value="<?php echo $supplier; ?>" placeholder="<?php echo 'Supplier Name'; ?>" class="form-control" />
		            <?php if ($error_name) { ?>
		            	<div class="text-danger"><?php echo $error_name; ?></div>
		             <?php } ?>
		        </div>
			    
		    </div> 
	        <div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'Vat Tin No'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="vat_no" value="<?php echo $vat_no; ?>" placeholder="<?php echo 'Vat'; ?>" class="form-control" />
		        </div>
		        <label class="col-sm-3 control-label"><?php echo 'GST No'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="gst_no" value="<?php echo $gst_no; ?>" placeholder="<?php echo 'GST No'; ?>" class="form-control" />
		        </div>
	        </div>
	        <div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'City'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo 'City'; ?>" class="form-control" />
		        </div>
		        <label class="col-sm-3 control-label"><?php echo 'Pin No'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="pinno" value="<?php echo $pinno; ?>" placeholder="<?php echo 'PIN No'; ?>" class="form-control" />
		        </div>
	        </div>
	        <div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'Contact No'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="contactno" value="<?php echo $contactno; ?>" placeholder="<?php echo 'Contact No'; ?>" class="form-control" />
		        </div>
		        <label class="col-sm-3 control-label"><?php echo 'Mobile No'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="mobileno" value="<?php echo $mobileno; ?>" placeholder="<?php echo 'Mobile No'; ?>" class="form-control" />
		        </div>
	        </div>
	        <div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'Address'; ?></label>
		        <div class="col-sm-3">
		        	<textarea  name="address"><?php echo $address; ?></textarea>
		        </div>
		        
	        </div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>
<script type="text/javascript">

$('#category_id').on('change', function() {
  department_name = $('#category_id option:selected').text();
  $('#category').val(department_name);
});
  </script>