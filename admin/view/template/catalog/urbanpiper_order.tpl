<?php echo $header; ?>
<div id="content" style="padding-bottom: 0px;">
  <div class="container-fluid" style="padding: 0px;">
  	<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>

  	<div class="table-wrap" style="font-family: 'Open Sans';">
  		<br>
  		<div class="col-sm-12" >
  				<h1 style="display:inline;margin-left: 2%;font-weight: 600;">API</h1> 
  				<a style="float: right;background-color: #e72b0b;margin-right: 1%;border-color:#e72b0b;" onclick="backOrder()" data-toggle="tooltip" title="<?php echo 'Your Local Order Page'; ?>" class="btn btn-primary"> Back </a>&nbsp;&nbsp;
	  			
  				<!-- <a style="float: right;margin-right: 1%;" onclick="pullorder()" class="btn btn-success" data-toggle="tooltip" title="<?php echo 'Here will get new orders'; ?>" >Pull Orders</a> -->
  				<!-- <a style="float: right;background-color: #1a6662;margin-right: 1%;border-color:#1a6662;" onclick="check_live_store()" class="btn btn-success"></i>Live Store</a>&nbsp;&nbsp; -->
  				<!-- <a style="float: right;background-color: #1a6662;margin-right: 1%;border-color:#1a6662;" onclick="addstore()" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i>Send Store</a>&nbsp;&nbsp;
  				<a style="float: right;background-color: #a37c70;margin-right: 1%;border-color:#a37c70; " onclick="link_add_store_form()"  class="btn btn-success"><i class="fa fa-home" aria-hidden="true"></i>  Adding/Updating stores</a> -->
  			
  		</div>
  		<br>
  		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
  		<table class="table" style="background-color: #fff;width: 96%;margin-left: 2%;margin-top: 3%;font-size: 100%;cursor: default;">
		  	<thead  style="background-color: #e72b0b; color: #fff; font-family: inherit; font-size: 105%;">
		    	<tr>
		      		<th scope="col">Sr.No</th>
		      		<th scope="col">Order No</th>
		      		<th scope="col">Agg. Order ID</th>
		      		<th scope="col">Date</th>
		      		<th scope="col">Delivery Time</th>
		      		<th scope="col">Channal</th>
		      		<th scope="col">Payment</th>
		      		<th scope="col">Type</th>
		      		<th scope="col" colspan="2">Grand Total</th>
		    	</tr>
		  	</thead>
  			<tbody>
  				<?php if(!empty($pending_orders)) { ?>
	  				<?php foreach ($pending_orders as $pkey => $pvalue) { ?>
	  					<?php if($pvalue['status'] == 'Acknowledged') { ?>
	  						<?php 	$colors = '#2690a1'; 
	  							$font_color = '#fff';
	  						?>
	  					<?php } elseif($pvalue['status'] == 'Food Ready') {$colors = '#40d158'; $font_color = '#000'; ?>
	  					<?php } elseif($pvalue['status'] == 'Cancelled') { $colors = '#a61022'; $font_color = '#fff';?>
	  					<?php } elseif($pvalue['status'] == 'Dispatched') {$colors = 'green'; $font_color = '#fff'; ?>
	  					<?php } elseif($pvalue['status'] == 'Completed') {$colors = '#083408'; $font_color = '#fff'; ?>

	  					<?php } else { ?>
	  						<?php 	$colors = 'white'; $font_color = '#000';?>
	  					<?php } ?>
						    <tr class="dialog"  style="background-color: <?php echo $colors; ?>;color:<?php echo $font_color; ?>;font-family: inherit;font-size: 120%;cursor: pointer;">
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['sr_no'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_id'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['zomato_order_id'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_time'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['delivery_time'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['online_channel'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['payment_mode'] ?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_type'].' '.'('. $pvalue['status'] .')'?></td>
						      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['gross_amount'] ?></td>
						      	<?php if($pvalue['status'] == 'Acknowledged' || $pvalue['status'] == 'Dispatched') { ?>
						      		<td >
						      			<a onclick="order_ready(<?php echo $pvalue['order_id'] ?>)" class="btn btn" style="background-color: #1a413d;color: #fff;font-size: 13px;"> Order Ready</a>&nbsp;&nbsp;&nbsp;&nbsp;
						      			<a onclick="bill_print(<?php echo $pvalue['order_id'] ?>)"  class="btn btn" style="background-color: #5ab8c2;color: #fff;font-size: 13px;"> Print Bill</a>
						      		</td>
						      	<?php } elseif($pvalue['status'] == 'Food Ready') { ?>
									<td >&nbsp;&nbsp;&nbsp;&nbsp;
						      			<a onclick="bill_print(<?php echo $pvalue['order_id'] ?>)"  class="btn btn" style="background-color: #3c8d49;color:#fff;font-size: 13px;"> Print Bill</a>
						      		</td>
								<?php } elseif($pvalue['status'] == 'Cancelled') { ?>
									<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )">Cancelled</td>
						      	<?php } else { ?>
						      		<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['status'] ?></td>
						      	<?php } ?>
						    </tr>
	  				<?php } ?>
  				<?php } ?>
  			</tbody>
		</table>
		<div id="dialog" title="Accept Screen" style="top: 50;left:  50;">
			
		</div>
		<div id="dialog_storeonoff" title="Store ON / OFF">
			
		</div>
	</div>
	</div>
</div>
<input type="hidden"  value="<?php echo $food_alert ?>"  class="form-control food_alerts"/>
<audio id="sound_tag" src="ring_foods.mp3" preload="auto" ></audio>
<script type="text/javascript">
    $(document).ready(function(){
    	food_alerts =  $(".food_alerts").val() || 0;
    	if(food_alerts == 1){
        	$('#sound_tag')[0].play();
    	}
    });
</script>
<style type="text/css">
	.myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;

    }

    .myclass {
        box-shadow: 0 0 8px 2px #8f7474;
    }

    .ui-button.cancelButton {
	    border: 1px solid #aaaaaa
	    ;
	    color: #FF0000
	    ;
	}

	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}






.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 40px;
}

input:checked + .slider {
  background-color: #00891d;
}

input:focus + .slider {
  box-shadow: 0 0 1px #00891d;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


</style>


<script type="text/javascript">
	function order_ready(order_id){
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/urbanpiper_itemdata/order_ready&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				if(json.success){
					alert(json.success);
					$('.sucess').html('');
					$('.sucess').append(json.done);
				} else if(json.errors) {
					alert(json.errors);
				} else {
					alert("somthing is wrong");
				}
				location.reload();
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}


	function bill_print(order_id){
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/urbanpiper_order/bill_print&token=<?php echo $token; ?>&order_id='+order_id,
				dataType: 'json',
				data: {"data":"check"},
				success: function(json){
						//console.log(data); 
					location.reload();
				}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
		}

	function order_dat(order_id){
		//alert(order_id);
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);
		clearTimeout(auto_refresh);
		htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/urbanpiper_itemdata&token=<?php echo $token; ?>&order_id='+order_id+'" width="100%" height="100%"></iframe>';
			dialog.dialog("open");
			$('#dialog').html(htmlc)
   
	}

	

	function backOrder() {
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);

		order_page1 = '<?php echo $order_page; ?>';
		order_page1 = order_page1.replace("&amp;", "&");

		window.location = order_page1; 
	}

	

	//function callaudio

	function pullorder(){
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);
		location.reload();
	}


	function store_onoff(){
		//load_unseen_notification("closed_re");
		clearTimeout(auto_refresh);
		htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/store_onoff&token=<?php echo $token; ?>&status='+status+'" width="100%" height="100%"></iframe>';
		dialog_storeonoff.dialog("open");
		$('#dialog_storeonoff').html(htmlc);
		//location.reload();
	}

  dialog = $("#dialog").dialog({
	autoOpen: false,
	height: 600,
	width: 1295,
	// height: 600,
	// width: 1089,
	//top: 74,
	//left: 76
	modal: true,
	dialogClass: 'myTitleClass myclass',
	buttons: {

	},
	close: function(event, ui) {
		$("#overlay").fadeIn(200);　
          location.reload();
     },
    
});

  dialog_storeonoff = $("#dialog_storeonoff").dialog({
	autoOpen: false,
	height: 600,
	width: 900,
	modal: true,
	dialogClass: 'myTitleClass',
	buttons: {

	},
	close: function(event, ui) {
		$("#overlay").fadeIn(200);　
        location.reload();
    },

});

  function closeIFrame(){
  		//load_unseen_notification("closed_re");
  		clearTimeout(auto_refresh);
  		localStorage.removeItem("closed_re");
      	location.replace('index.php?route=catalog/urbanpiper_order&token=<?php echo $token; ?>');
      	window.location.reload(); 	
	}
	
	// var auto_refresh = setInterval(function(){ 
	// 	//if(ajax_my == 0){
	// 		$("#overlay").fadeIn(200);　//comment by me for some time
	// 		load_unseen_notification(); 	//comment by me for some time
	// 	//}
		
	// 	 }, 5000);

	
	var check_getqueue_ignore = false;
	var auto_refresh = setInterval(function(){ 
		load_unseen_notification(); 	
	}, 15000); 
	
	function load_unseen_notification(closed_re = ''){
		 	if (check_getqueue_ignore) {
	    		return false;
	    	}
		 	check_getqueue_ignore = true;
		 	var req =  $.ajax({
		   url:'index.php?route=catalog/urbanpiper_order/check_transection&token=<?php echo $token; ?>',
		   method:"POST",
		   dataType:"json",
		   beforeSend: function () {
              $("#overlay").fadeIn(200);　
            },
		   success:function(json)
		   	{ 
		   		check_getqueue_ignore = false;
			   	if(json.redirect_warning){
			   		window.location = json.redirect_warning; 
			   	} 
		   }
		  });
		  req.done(function(){
		  		$("#overlay").fadeIn(200);　
		  		window.location.reload();
		  		
            });
	}

		 
	$("#overlay").fadeOut(50);
		/*$(document).click(function() {
   			alert('inn');
		});
*/
</script>
<?php echo $footer; ?>