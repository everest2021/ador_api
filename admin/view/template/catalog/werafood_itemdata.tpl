<?php echo $header; ?>
<div id="content" class="sucess">
	<div class="page-header">
		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
		<form action="<?php echo $action; ?>" id="payform" method="post">
			<div class="container-fluid">
				<table class="table" style="background-color: #fff;width: 96%;margin-left: 2%;margin-top: 3%;font-size: 149%;cursor: default;">
				  	<input type="hidden" id="order_id" name="order_id" value="<?php echo $order_id ?>">
				  	<input type="hidden" name="order_from" id="order_from" value="<?php echo $order_from ?>">
				  	
				  	<div class="col-sm-7">
				  		<label style="font-size: 20px;color: black;">Customer Name: <?php echo $final_datas['customer_data']['name']?></label>
				  	</div>
				  	<div class="col-sm-5">
				  		<label style="font-size: 20px; color: black;">Mobile No: <?php echo $final_datas['customer_data']['phone_number']?></label>
				  	</div>
				  	<div class="col-sm-7">
				  		<label style="font-size: 20px;color: black;">Delivery : <?php echo $enable_delivery ?></label>
				  	</div>
				  	<!-- <div class="col-sm-5">
				  		<label style="font-size: 20px; color: black;">Order OTP: <?php echo $order_otp ?></label>
				  	</div> -->
				  	<thead  style="background-color: #e72b0b; color: #fff; font-family: monospace; font-size: 130%;">
				    	<tr>
				      		<th scope="col">Order No</th>
				      		<th scope="col">Item Name</th>
				      		<th scope="col">Qty</th>
				      		<th scope="col">Rate</th>
				      		<th scope="col">Amt</th>
				      		<th scope="col">GST</th>
				      		<th scope="col">GST Value</th>
				      		<th scope="col">Total</th>
				      		
				      		
				    	</tr>
				  	</thead>
		  			<tbody>
		  				<?php foreach ($final_datas['item_data'] as $pkey => $pvalue) { ?>
						    <tr  style="background-color: #fff;color: #000;font-family: monospace;font-size: 120%;cursor: pointer;">
						      	<td><?php echo $pvalue['order_id'] ?></td>
						      	<td><?php echo $pvalue['item_name'] ?></td>
						      	<td><?php echo $pvalue['item_quantity'] ?></td>
						      	<td><?php echo $pvalue['item_unit_price'] ?></td>
						      	<td><?php echo $pvalue['subtotal'] ?></td>
						      	<td><?php echo $pvalue['gst'] ?></td>
						      	<td><?php echo $pvalue['gst_val'] ?></td>
						      	<td><?php echo $pvalue['grand_tot'] ?></td>
						      	
						    </tr>

		  				<?php } ?>
		  				<tr>
		  					<td style="color: black;">T.Items</td>
		  					<td style="color: black;"> <?php echo $total_item; ?> </td>
		  					<td style="color: black;">T.Qty</td>
		  					<td style="color: black;" ><?php echo $total_qty; ?></td>
		  					<td  style="color: black;text-align: right;">G.Total</td>
		  					<td style="color: black;text-align: right;" ><?php echo round($grand_total) ?></td>
						</tr>
		  			</tbody>
				</table>
				<?php if ($order_instructions != '') { ?>
					<div class="col-sm-12">
				  		<label style="font-size: 20px; color: black;">Order Instruction: <?php echo $order_instructions ?></label>
				  	</div>
				<?php } ?>  	

				<?php if($status != 'Food Ready') {?>
					<div class="col-sm-offset-0">
						<div class="col-sm-6">
							<a  onclick="accept_btn()" id="accept-input"  class="btn btn-block" style="background: green;color: #fff;font-size: 25px;">Accept</a>
						</div>
						<div class="col-sm-6">
							<a  onclick="reject_btn()" id="reject-input"  class="btn btn-block" style="background: #e72b0b;color: #fff;font-size: 25px;">Reject</a>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-sm-2" style="float: right;">
						<a  onclick="bill_print(<?php echo $order_id ?>)" id="bill_print"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Bill Print</a>
					</div>

					<div class="col-sm-3" style="float: right;">
						<a  onclick="order_ready(<?php echo $order_id ?>)" id="order_ready"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Order Ready</a>
					</div>
				<?php } ?>
				
				<br>
				
				<div class="col-sm-12" id="prep_time" style="margin-top: 3%; ">
					<div class="col-sm-4">
			      		<label for="pre_time" style="color: #000;font-size: 25px;">Preparation Time(Minutes):</label>
			      	</div>
					<div class="col-sm-3" >
			      		<select class="form-control" id="pre_timess" name="prep_time" style="font-size: 22px;height: 50px;">
				    		<option value='' selected="selected"><?php echo "Please Select" ?></option>
				    		<?php foreach($foodready_time as $key => $value) { ?>
					    		<option value="<?php echo $key ?>" style="font-size: 20px;"><?php echo $value?></option>
				    		<?php } ?>
			      		</select>
			      	</div>
			  	</div>

			  	<div class="col-sm-12" id="reject_reason" style="margin-top: 3%; ">
					<div class="col-sm-4">
			      		<label for="reject_re" style="color: #000;font-size: 25px;">Reject Reason:</label>
			      	</div>
					<div class="col-sm-5" >
			      		<select class="form-control" id="reject_res" name="reject_reason" style="font-size: 22px;height: 50px;">
				    		<option value='' selected="selected"><?php echo "Please Select" ?></option>
				    		
				    		<?php foreach($reject_reason as $rkey => $rvalue) { ?>
					    		<option value="<?php echo $rkey ?>" style="font-size: 20px;"><?php echo $rvalue?></option>
				    		<?php } ?>
			      		</select>
			      	</div>
			  	</div>
			  	<div class="col-sm-offset-0" id="ok_btns" >
				  	<div class="col-sm-3" style="margin-top: 3%;">
						<a  onclick="ok_btn()" id="ok-input"  class="btn btn-block" style="background: green;color: #fff;font-size: 25px;">OK</a>
					</div>
				</div>
				<br />
				<!-- <?php if($msggg == ''){ ?>
					<div class="col-sm-12">
					  	<label style="font-size: 20px;color: black;">Rider Name: <?php echo $final_datas['rider_data']['rider_name']?></label>
					</div>
				  	<div class="col-sm-12">
				  		<label style="font-size: 20px; color: black;">Rider No: <?php echo $final_datas['rider_data']['rider_number']?></label>
				  	</div>
				  	<div class="col-sm-12">
					  	<label style="font-size: 20px;color: black;">Status: <?php echo $final_datas['rider_data']['rider_status']?></label>
					</div>
				  	<div class="col-sm-12">
				  		<label style="font-size: 20px; color: black;">Arrival Time: <?php echo $final_datas['rider_data']['time_to_arrive']?></label>
				  	</div>
				<?php } else { ?>
					<br />
				  	<div class="col-sm-12">
					  	<label style="font-size: 20px;color: black;">Rider: <?php echo $msggg ?></label>
					</div>
				<?php } ?> -->
			</div>
		</form>
	</div>
<style type="text/css">
	.myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;
    }

    .ui-button.cancelButton {
	    border: 1px solid #aaaaaa
	    ;
	    color: #FF0000
	    ;
	}

	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>	
	<script type="text/javascript">
	$(document).on('keydown', '#cash', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
			save();
		  }
		//console.log(code);
		//alert(code);
	});

	$(document).ready(function() {
		$('#prep_time').hide();
		$('#reject_reason').hide();
		$('#ok_btns').hide();


	});

	function bill_print(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/werafood_itemdata/bill_print&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				if(json.info == 1){
					$('.sucess').html('');
					$('.sucess').append(json.done);
				}
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}

	function order_ready(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/werafood_itemdata/order_ready&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				if(json.info == 1){
					$('.sucess').html('');
					$('.sucess').append(json.done);
				} else if(json.info == 2) {
					alert(json.reason);
				} else {
					alert('somthing is wrong !!!');
				}
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}

	function accept_btn(){
		$('#prep_time').show();
		$('#ok_btns').show();
		$('#reject_reason').hide();
		$('#reject_re').html('');


		//return false;
	}

	function reject_btn(){
		$('#reject_reason').show();
		$('#ok_btns').show();
		$('#prep_time').hide();
		//return false;
	}

	function accept_btn(){
		order_id = $('#order_id').val();
		order_from = $('#order_from').val();
		pre_timess = $('#pre_timess').val();
		reject_re = $('#reject_res').val();
		//alert(reject_re);

		if(reject_re == ''){
			if(pre_timess != ''){
				// var conn = confirm('Do You want to Print KOT ?');				
				// if(conn != false){
					$.ajax({
						type: "POST",
					  	url: 'index.php?route=catalog/werafood_itemdata/update_order&token=<?php echo $token; ?>&order_id=' +order_id+'&prepare_time='+pre_timess+'&reject_reason='+reject_re,
					  	dataType: 'json',
					  	success: function(json) {
					  		//alert('sucess');
					  		//console.log(json);
					  		if(json.info == 1){
								$('.sucess').html('');
								$('.sucess').append(json.done);
							} else if( json.info == 2 ) {
								alert(json.reason);
							} else {
								alert("somthing is wrong");
							}
					  	}
					});
				// } else {
				// 	$.ajax({
				// 		type: "POST",
				// 	  	url: 'index.php?route=catalog/werafood_itemdata/insert_datas&token=<?php echo $token; ?>&order_id=' +order_id+'&prepare_time='+pre_timess+'&reject_reason='+reject_re,
				// 	  	dataType: 'json',
				// 	  	success: function(json) {
				// 	  		//alert('sucess');
				// 	  		if(json.info == 1){
				// 				$('.sucess').html('');
				// 				$('.sucess').append(json.done);
				// 			} else {
				// 				alert('somthing is wrong')
				// 			}
				// 	  	}
				// 	});
				// }
			}else {
				alert("Please Select Preparation Time");
			}
		} else {
			if(reject_reason != ''){
				console.log(order_from);
				if(order_from == 'Swiggy'){
					urll = 'index.php?route=catalog/werafood_itemdata/reject_order_swiggy&token=<?php echo $token; ?>&order_id=' +order_id+'&reject_reason='+reject_re;
				} else {
					urll = 'index.php?route=catalog/werafood_itemdata/reject_order&token=<?php echo $token; ?>&order_id=' +order_id+'&reject_reason='+reject_re;
				}
				$.ajax({
					type: "POST",
				  	url: urll,
				  	dataType: 'json',
				  	success: function(json) {
				  		
				  		//alert('sucess');
				  		if(json.info == 1){
							$('.sucess').html('');
							$('.sucess').append(json.done);
						} else if(json.info == 2) {
							alert(json.reason);
						} else {
							alert('somthing is wrong !!!');
						}
				  	}
				});
			}else {
				alert("Please Select Reason For Cancel Order")
			}
		}
		
		// $.ajax({
		// 	type: "POST",
		// 	url: 'index.php?route=catalog/werafood_itemdata/update_order&token=<?php echo $token; ?>&order_id='+order_id+'&prepare_time='+pre_timess+'&reject_reason='+reject_re,
		// 	dataType: 'json',
		// 	success: function(data){
		// 		if(data.info == 1){
		// 			$('.sucess').html('');
		// 			$('.sucess').append(data.done);
		// 		}
				
		// 	}
		// });

		//return false;
	}

	

	
	</script>
</div>
	