<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Modifier Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="modifiername" value="<?php echo $modifiername ?>" placeholder="<?php echo 'Name'; ?>" class="form-control" />
						</div>
							<label class="col-sm-1 control-label"><?php echo 'Maximum Quantity'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="max_quantity" value="<?php echo $max_quantity ?>" placeholder="<?php echo 'Maximum Quantity'; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Select Item'; ?></label>
						<div class="col-sm-3">
							<input type="text" id="item_name" placeholder="<?php echo 'Select Item'; ?>" class="form-control" />
						</div>
					</div>
					<table id="data" class="table table-bordered">
						<tr>
							<th>Item Name</th>
							<th>Default Item</th>
							<th>Remove</th>
						</tr>
						<?php $i=0; ?>
						<?php if($testdatas) { ?>
							<?php foreach($testdatas as $testdata) { ?>
								<tr id="re_<?php echo $i ?>">
									<td><input type="text" name="po_datas[<?php echo $i ?>][item]" value="<?php echo $testdata['modifier_item'] ?>" class="form-control"/></td>
									<td>
									<?php if($testdata['default_item'] == '1'){ ?>
										<input type="checkbox" name="po_datas[<?php echo $i ?>][default_item]" id="default_item_<?php echo $i ?>" value="1" checked="checked" class="defaultitem form-control" style="display:inline-block"/>&nbsp;&nbsp;Yes
									<?php } else { ?>
										<input type="checkbox" name="po_datas[<?php echo $i ?>][default_item]" id="default_item_<?php echo $i ?>" value="1" class="defaultitem form-control" style="display:inline-block"/>&nbsp;&nbsp;Yes
									<?php } ?>
									</td>
									<input type="hidden" name="po_datas[<?php echo $i ?>][itemid]" value="<?php echo $testdata['modifier_item_id'] ?>" class="form-control"/>

									<input type="hidden" name="po_datas[<?php echo $i ?>][itemcode]" value="<?php echo $testdata['modifier_item_code'] ?>" class="form-control"/>
									<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder(<?php echo $i ?>)" class="button inputs remove " id="remove_<?php echo $i ?>" ><i class="fa fa-trash-o"></i></a></td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						<?php } ?>
					</table>
				</form>
	  		</div>
		</div>
  	</div>	
  	<script type="text/javascript">
  	var i = '<?php echo $i ?>';
  	// var arr = new Array();
  	// var test = new Array();
  	//var testing;
  	$('#item_name').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/modifier/autocompletename&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.item_name,
	            value: item.item_id,
	            code : item.item_code,
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
		/*var $tr = $('<tr id="r_'+i+'">').append(
					$("<td><input type=\"text\" value=\"" + ui.item.label + "\" id=\"item_" +i+ "\" class=\"form-control\" />"),
					$("<td style='display:none'><input type=\"hidden\" value=\"" + ui.item.value + "\"  id=\"itemid_" +i+ "\" />"),
					$("<td style='display:none'><input type=\"hidden\" value=\"" + ui.item.code + "\"  id=\"itemcode_" +i+ "\" \"/>"),
					$("<td><input type=\"text\" id=\"qty_" +i+ "\" class=\"form-control\" />")
				);
		$('#data').append($tr);*/
		html = '<tr id="re_'+i+'">'; 
	      // html += '<td id="'+i+'">';
	      html += '<td><input type="text" name="po_datas[' + i + '][item]" value="'+ui.item.label+'"  class="form-control"/></td>';
	      html += '<td><input type="checkbox" name="po_datas[' + i + '][default_item]" id="default_item_'+i+'" value="1" style="display:inline-block" class="defaultitem form-control"/>&nbsp;&nbsp;Yes</td>';
	      html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+ui.item.value+'"  class="form-control"/>';
	      html += '<input type="hidden" name="po_datas[' + i + '][itemcode]" value="'+ui.item.code+'"  class="form-control"/>';
	      html += '<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a></td>';
	      // html += '</td>';
      	html += '</tr>';
      	$('#data').append(html);
	      // var object = {i:value};
	      // alert(object);
	      //test1(i);
		i++;
	    return false;
	  },
	  focus: function(event, ui) {

	    return false;
	  }
	});

	// $(document).on('change', '.qty', function(e) {
	//   	idss = $(this).attr('id');
	//   	s_id = idss.split('_');
	//   	qty = parseFloat($('#qty_'+s_id[1]).val());
	//   	$('#qty_'+s_id[1]).val(qty);
	//   });

	$(document).on('change', '.defaultitem', function(e) {
	  	idss = $(this).attr('id');
	  	s_id = idss.split('_');
	  	default_item = parseFloat($('#default_item_'+s_id[1]).val());
	  	$('#default_item_'+s_id[1]).val(default_item);
	  });

	function remove_folder(c) {
		$('#re_'+c).remove();
	}

  	// function test1(i){
  	// 		var value = [$('#item_'+i).val(),$('#item_id_'+i).val(),$('#item_code_'+i).val(),$('#qty_'+i).val()];
		 //   testing = {i:value};
		 //   test[i] = testing.i;
  	// }
  	/*$('#save').click(function(){
  		 var arr = [];
  		 var arr1 = [];
  		$.each(new Array(i), function(n){
  			//alert(n);
			var $cells1 = $("#data"),$cells2 = $cells1.find("#r_"+n),$cells = $cells2.find("td"),
	        $item = $cells.find('#item_'+n);
	        $itemid = $cells.find('#itemid_'+n);
	        $itemcode = $cells.find('#itemcode_'+n);
	        $qty = $cells.find('#qty_'+n);
	        arr[n] = {'item':$item.val(),'itemid':$itemid.val(),'itemcode':$itemcode.val(),'qty':$qty.val()};
	        // arr1[0] = arr[0];
	        // arr1[1] = arr[1];
	        // test = {n:arr[n]};
	        // alert($inputs.val());
	        // alert($inputs1.val());
       		}
		);

		// location = 'index.php?route=catalog/modifier/add&token=<?php echo $token;?>&modifieritems='+ encodeURIComponent(btoa(JSON.stringify(arr)));
		$('input[name=\'itemdata\']').val(JSON.stringify(arr));
  			//alert (JSON.stringify(arr));
			// var $cells1 = $("#data"),$cells2 = $cells1.find("#r_"+j),$cells = $cells2.find("td"),
	  //       $inputs = $cells.find('input');
	  //   }
	    // $.each($inputs, function(){
	    //     var value = $(this).val();
	    //     alert(value);
	    // });
    });*/

  	</script>
</div>
<?php echo $footer; ?>