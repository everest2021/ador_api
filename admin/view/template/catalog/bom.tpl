<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="bom">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select BOM data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-4">
								    	<center><label>BOM Type</center></label></center>
								    	<select class="form-control" id="filter_bomtype" name="filter_bomtype">
								    		<option value='' ><?php echo "All" ?></option>
								    		<?php foreach($bomtypes as $key => $value) { ?>
								    			<?php if($key == $bomtype) { ?>
								    				<option value='<?php echo $key ?>' selected="selected"><?php echo $value ?></option>
								    			<?php } else { ?>
								    			 	<option value='<?php echo $key ?>' ><?php echo $value ?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								    <div class="col-sm-2" id="filter_bomcategory">
								    	<center><label>Category</label></center>
								    	<select class="form-control" name="filter_bomcategory">
								    		<option value='' ><?php echo "All" ?></option>
								    		<?php foreach($categorys as $key => $value) { ?>
								    			<?php if($value['category_id'] == $bomcategory) { ?>
								    				<option value="<?php echo $value['category_id']  ?>" selected="selected"><?php echo $value['category'] ?></option>
								    			<?php } else { ?>
								    			 	<option value="<?php echo $value['category_id'] ?>" ><?php echo $value['category'] ?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
									<div class="col-sm-12">
										<br>
										<center><input type="submit" class="btn btn-primary" value="Show"></center>
									</div>
								</div>
							</div>
					    </div>
				 	</div>
				 	<div class="col-sm-12">
				 		<table class="table table-bordered" id="table">
				 			<tr>
				 				<th style="text-align: center;">Code</th>
				 				<th style="text-align: center;">Name</th>
				 			</tr>
				 			<?php foreach($itemdatas as $itemdata) { ?>
					 			<tr>
					 				<td style="text-align: center;"><?php echo $itemdata['item_code'] ?></td>
					 				<td style="text-align: center;"><?php echo $itemdata['item_name'] ?></td>
					 			</tr>
				 			<?php } ?>
				 		</table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function () {
    		$("#filter_bomtype").change(function () {
    			$("#bom").submit();
    	});
	});

	$("#table tr").click(function(){
	   var code=$(this).find('td:first').html();
	   var name=$(this).find('td:last').html();
	   var filter_bomtype = $('select[name=\'filter_bomtype\']').val();
	   var filter_bomcategory = $('select[name=\'filter_bomcategory\']').val();

	   var url = 'index.php?route=catalog/subbom&token=<?php echo $token; ?>';

		  if (filter_bomtype) {
			  url += '&filter_bomtype=' + encodeURIComponent(filter_bomtype);
		  }


		  if (filter_bomcategory) {
			  url += '&filter_bomcategory=' + encodeURIComponent(filter_bomcategory);
		  }

		  if (code) {
			  url += '&code=' + encodeURIComponent(code);
		  }

		  if (name) {
			  url += '&name=' + encodeURIComponent(name);
		  }

	   window.location = url;
	   
	   alert(value);  
	   alert(value2); 
	   alert(filter_bomtype);
	   alert(filter_bomcategory); 
	});
	</script>
</div>
<?php echo $footer; ?>