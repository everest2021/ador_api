<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 		<button style="height: 33px; background-color: blue;"><a style="padding: 10px 19px;height: 10px;width: 10px;color: white;" href="<?php echo $export; ?>" id="filter_export"><?php echo 'Export'; ?></a></button>
				 	</div>
					<div class="col-sm-6 col-sm-offset-3">
					<?php if($startdate != '' && $enddate != ''){ ?>
						<center><h3><b>Complimentary Report</b></h3></center>
						<h3>From : <?php echo $startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
						  <table class="table table-bordered table-hover" style="text-align: center;">
								<?php $total = 0; ?>
								<?php foreach($finaldatas as $finaldata) { ?>
									<tr>
										<th style="text-align: center;">Ref.No</th>
										<th style="text-align: center;">Table No</th>
										<th style="text-align: center;">Bill Date</th>
										<th style="text-align: center;">Item Name</th>
										<th style="text-align: center;">Qty.</th>
										<th style="text-align: center;">Rate</th>
										<th style="text-align: center;">Total Amt.</th>
										<th style="text-align: center;">Grand Total</th>
										<th style="text-align: center;">Reason</th>
									</tr>
									<?php foreach($finaldata['date_array'] as $data) { ?>
										<tr>
											<td><?php echo $data['ref_no'] ?></td>
											<td><?php echo $data['table_no'] ?></td>
											<td><?php echo $data['bill_date'] ?></td>
											<td><?php echo $data['item_name'] ?></td>
											<td><?php echo $data['qty'] ?></td>
											<td><?php echo $data['rate'] ?></td>
											<td><?php echo $data['amt'] ?></td>
											<td><?php echo $data['grand_total'] ?></td>
											<td><?php echo $data['resion'] ?></td>
										</tr>
									<?php } ?>
								<?php } ?>
						  	</table>
					  <?php } ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/complimentaryreport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>