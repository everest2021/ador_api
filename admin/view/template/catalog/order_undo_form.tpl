<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  <div class="page-header">
  </div>
  <div class="container-fluid">
	<?php $tab_index = 1; ?>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-order" class="form-horizontal">
			
			<div class="panel-body" style="padding-bottom: 0%;">
				<div style="display:inline-block; width:100%;float:left;">
			  		<label style="text-align: center;font-size: 20px;">Undo Order List</label>
			  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 5%;overflow-y: auto;height:300px;">
						<table style="" class="table table-bordered table-hover ordertab">
					  		<tr>
								<td style="padding-top: 2px;padding-bottom: 2px;width:10%;">Order Id</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Table Id</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;text-align: right;">Total Items</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;text-align: right;">Total Qty</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;text-align: right;">Grand Total</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">Bill Date</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">Time</td>
								<td style="width:30%;padding-top: 2px;padding-bottom: 2px;">Action</td>
					  		</tr>
					  		<?php  if($order_undo_list) { ?>
						  		<tbody>
						  			<?php foreach ($order_undo_list as $key => $value) { ?>
							  			<tr>
							  				<td>
							  					<?php echo $value['order_id'] ?>
							  				</td>
							  				<td>
							  					<?php echo $value['table_id'] ?>
							  				</td>
							  				<td style="text-align: right;">
							  					<?php echo $value['total_items'] ?>
							  				</td>
							  				<td style="text-align: right;">
							  					<?php echo $value['item_quantity'] ?>
							  				</td>
							  				<td style="text-align: right;">
							  					<?php echo $value['grand_total'] ?>
							  				</td>
							  				<td>
							  					<?php echo $value['bill_date'] ?>
							  				</td>
							  				<td>
							  					<?php echo $value['time'] ?>
							  				</td>
							  				<?php if($value['tbl_status'] == 0) { ?>
								  				<td>
								  					<a  class="btn btn-primary"onclick="orderupdate_fun(<?php echo $value['order_id']?>);">Undo Order</a> &nbsp;
								  				</td>
								  			<?php  } else { ?>
								  				<td>
								  					<label style="color: red;">Table is Running</label>
								  				</td>
								  			<?php } ?>
							  			</tr>
							  		<?php } ?>
						  		</tbody>
							 <?php } ?>
						</table>
			  		</div>
				</div>
			</div>
	    </form>
	</div>
	<div id="dialog-form_select_qty" title="Select Quantity">
	</div>
</div>

<script type="text/javascript">
function orderupdate_fun(order_id){
	var reason = confirm("Are you sure? ");
	if (reason == true) {
		$.ajax({
	   		type: "GET",
	   		url: 'index.php?route=catalog/order_undo/order_update&token=<?php echo $token;?>&orderid=' + encodeURIComponent(order_id) ,
	   		dataType: 'json',
	   		success: function(data){
	   			
	   		//console.log(data.done);
	   			$('.sucess').html('');
				$('.sucess').append(data.done);
	   		}
	  	})
	} 

}
</script>
<script type="text/javascript">

$(document).ready(function() {
	$("#save_btn").hide();
  $('#input-t_number').focus();
});
</script>

<?php echo $footer; ?>