<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div class="pull-right">
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		    </div>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			       <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Customer Name'; ?></label>
				          <input type="text" name="filter_customername" value="<?php echo $filter_customername; ?>" placeholder="<?php echo 'Customer Name'; ?>" id="input-filter_customername" class="form-control" />
				          <input type="hidden" name="filter_item_id" value="<?php echo $filter_item_id; ?>" id="input-filter_item_id" class="form-control" />
				        </div>
			       </div>
			        <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Status'; ?></label>
				          <select class="form-control" name="filter_status" id="input-filter_status">
				          	<option selected="selected" value="All">All</option>
					          <?php foreach($status as $key => $value){?>
					          	<?php if($filter_status == $key && $filter_status != ''){ ?>
					          		<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
					          	<?php } else { ?>
					          		<option value="<?php echo $key ?>"><?php echo $value?></option>
					          	<?php } ?>
					          <?php } ?>
				          </select>
				        </div>
				       	<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			       </div>
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  	<td><?php if ($sort == 'advance_billno') { ?>
					<a href="<?php echo $sort_advance_billno; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Bill No'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_advance_billno; ?>"><?php echo 'Bill No'; ?></a>
					<?php } ?></td>
					<td><?php if ($sort == 'booking_date') { ?>
					<a href="<?php echo $sort_booking_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Booking Date'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_booking_date; ?>"><?php echo 'Booking Date'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'customer_name') { ?>
					<a href="<?php echo $sort_customer_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Customer Name'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_customer_name; ?>"><?php echo 'Customer Name'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'contact') { ?>
					<a href="<?php echo $sort_contact; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Contact'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_contact; ?>"><?php echo 'Contact'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'status') { ?>
					<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_status; ?>"><?php echo 'Status'; ?></a>
					<?php } ?></td>

				  <td class="text-right"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
				<?php foreach ($items as $item) { ?>
				<tr>
				  <td><?php if (in_array($item['item_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" />
					<?php } ?></td>
				  <td><?php echo $item['advance_billno']; ?></td>
				  <td><?php echo $item['booking_date']; ?></td>
				  <td><?php echo $item['customername']; ?></td>
				  <td><?php echo $item['contact']; ?></td>
				  <?php if($item['status'] == '0') { ?>
				  	<td>Pending</td>
				  <?php } else { ?>
				  	<td>Done</td>
				  <?php } ?>
				  <td class="text-right">
				  <?php if($item['status'] == '0') { ?>
				  	<a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
				  	<a href="<?php echo $item['makebill']; ?>" data-toggle="tooltip" title="<?php echo 'Make Bill'; ?>" class="btn btn-primary">Make Bill</a>
				  <?php } else { ?>
				  	<span class="glyphicon glyphicon-minus"></span>
				  <?php } ?>
				  </td>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<div id="dialog-form_paymentmode" title = "Payment Mode">
</div>	
<script type="text/javascript">
dialog_paymentmode = $("#dialog-form_paymentmode").dialog({
	closeOnEscape: false,
	autoOpen: false,
	height: 730,
	width: 1000,
	modal: true,
	open: function(event, ui) {
    	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
	}
});

$(document).ready(function(){
	orderid = '<?php echo $order_id ?>';	
	if(orderid != '' && orderid != '0'){
		htmlc = '<iframe id="paymentmode" style="border: 0px;" src="index.php?route=catalog/settlement&token=<?php echo $token; ?>&order_id='+orderid+'" width="100%" height="100%"></iframe>';
		dialog_paymentmode.dialog("open");
		$('#dialog-form_paymentmode').html(htmlc)
	}
});

function closeIFrame(){
      location.replace('index.php?route=catalog/advance&token=<?php echo $token; ?>');
}

$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/advance&token=<?php echo $token; ?>';

  	var filter_customername = $('input[name=\'filter_customername\']').val();
  	if (filter_customername) {
		var filter_item_id = $('input[name=\'filter_item_id\']').val();
		if (filter_item_id) {
	  		url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
	  		url += '&filter_customername=' + encodeURIComponent(filter_customername);
	  	}
  	}

  	var filter_status = $('select[name=\'filter_status\']').val();
  	if (filter_status) {
  		url += '&filter_status=' + encodeURIComponent(filter_status);
  	}

  	location = url;
});

$('input[name=\'filter_customername\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/advance/autocomplete&token=<?php echo $token; ?>&filter_customername=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.customername+" - "+item.contact,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_customername\']').val((ui.item.label).substring(0, ui.item.label.indexOf(' - ')));
    $('input[name=\'filter_item_id\']').val(ui.item.value);	
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
</script>
<?php echo $footer; ?>