<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
	  	<div class="container-fluid">
			<div class="pull-right">
		       <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		  	</div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			    <?php } ?>
		    </ul>
	    </div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
	  		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
	  		</div>
		<?php } ?>
	  	<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  	</div>
	  	<div class="panel-body">
		  	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
		      	<div class="form-group required">
			      	<label class="col-sm-3 control-label"><?php echo 'Location Name'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="location" value="<?php echo $location; ?>" placeholder="<?php echo 'Location'; ?>" class="form-control" />
			          	<?php if ($error_location) { ?>
			            	<div class="text-danger"><?php echo $error_location; ?></div>
			          	<?php } ?>
			        </div>
		       	</div>
		       	<div class="form-group required">
			      	<label class="col-sm-3 control-label"><?php echo 'Rate'; ?></label>
			      	<div class="col-sm-3">
			          	<select name="rate_id" id="input-rate_id" class="form-control">
			              	<option ><?php echo "Please Select" ?></option>
			          		<?php foreach($rates as $rkey => $rvalue){ ?>
			          			<?php if($rkey == $rate_id){ ?>
				      				<option value="<?php echo $rkey ?>" selected="selected"><?php echo $rvalue; ?></option>
			          			<?php } else { ?>
				      				<option value="<?php echo $rkey ?>"><?php echo $rvalue ?></option>
			          			<?php } ?>
			          		<?php } ?>
			        	</select>
			        </div>
		       	</div>
		       	<div class="form-group required">
			      	<label class="col-sm-3 control-label"><?php echo 'Stores'; ?></label>
			      	<div class="col-sm-3">
			          	<select name="store_name" id="input-store_name" class="form-control">
			              	<option ><?php echo "Please Select" ?></option>
			          		<?php foreach($stores as $rkey => $rvalue){ ?>
			          			<?php if($rvalue['id'] == $store_name){ ?>
				      				<option value="<?php echo $rvalue['id'] ?>" selected="selected"><?php echo $rvalue['store_name']; ?></option>
			          			<?php } else { ?>
				      				<option value="<?php echo $rvalue['id'] ?>"><?php echo $rvalue['store_name'] ?></option>
			          			<?php } ?>
			          		<?php } ?>
			        	</select>
			        </div>
		       	</div>
		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'Table From'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="table_from" value="<?php echo $table_from; ?>" placeholder="<?php echo 'Table From'; ?>" class="form-control" />
			        	<?php if ($error_table_from) { ?>
			            	<div class="text-danger"><?php echo $error_table_from; ?></div>
			          	<?php } ?>
			        </div>
		       	</div>
		       	<div class="form-group ">
			      	<label class="col-sm-3 control-label"><?php echo 'Table To'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="table_to" value="<?php echo $table_to; ?>" placeholder="<?php echo 'Table To'; ?>" class="form-control" />
			        	<?php if ($error_table_to) { ?>
			            	<div class="text-danger"><?php echo $error_table_to; ?></div>
			          	<?php } ?>
			        </div>
		       	</div>
		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'Bill Copy'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="bill_copy" value="<?php echo $bill_copy; ?>" placeholder="<?php echo 'Bill copy'; ?>" class="form-control" />
			        </div>
		       	</div>
		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'Kot Copy'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="kot_copy" value="<?php echo $kot_copy; ?>" placeholder="<?php echo 'Kot copy'; ?>" class="form-control" />
			        </div>
		       	</div>

		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'BOT Copy'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="bot_copy" value="<?php echo $bot_copy; ?>" placeholder="<?php echo 'Bot copy'; ?>" class="form-control" />
			        </div>
		       	</div>

		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'Master Kot'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="master_kot" value="<?php echo $master_kot; ?>" placeholder="<?php echo 'Master Kot'; ?>" class="form-control" />
			        </div>
		       	</div>


		       	<div class="form-group">
			      	<?php if($parcel_detail == '1'){ ?>
			      				<label class="col-sm-3 control-label"><?php echo 'Parcel Detail'; ?></label>
			      			<div class="col-sm-3">
								<input type="checkbox" name="parcel_detail" value="1" checked ="checked"  style="margin-right:8%"><br>
							</div>
							<?php } else { ?>
								<label class="col-sm-3 control-label"><?php echo 'Parcel Detail'; ?></label>
							<div class="col-sm-3">
								<input type="checkbox" id="parcel_detail" name="parcel_detail" value="1" style="margin-right:8%;">  <br />
							</div>
							<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($dboy_detail == '1'){ ?>
			      				<label class="col-sm-3 control-label"><?php echo 'Delivry Boy Detail'; ?></label>
			      			<div class="col-sm-3">
								<input  type="checkbox" name="dboy_detail" value="1" checked ="checked"  style="margin-right:8%"><br>
							</div>
							<?php } else { ?>
								<label class="col-sm-3 control-label"><?php echo 'Delivry Boy Detail'; ?></label>
							<div class="col-sm-3">
								<input type="checkbox" id="dboy_detail" name="dboy_detail" value="1" style="margin-right:8%;">  <br />
							</div>
							<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($a_to_z == '1'){ ?>
			      				<label class="col-sm-3 control-label"><?php echo 'A To Z'; ?></label>
			      			<div class="col-sm-3">
								<input type="checkbox" name="a_to_z" value="1" checked ="checked"  style="margin-right:8%"> <br>
							</div>
							<?php } else { ?>
								<label class="col-sm-3 control-label"><?php echo 'A To Z'; ?></label>
							<div class="col-sm-3">
								<input type="checkbox" id="a_to_z" name="a_to_z" value="1" style="margin-right:8%;"> <br />
							</div>
							<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($direct_bill == '1'){ ?>
			      				<label class="col-sm-3 control-label"><?php echo 'Direct Bill'; ?></label>
			      			<div class="col-sm-3">
								<input type="checkbox" name="direct_bill" value="1" checked ="checked"  style="margin-right:8%"> <br>
							</div>
							<?php } else { ?>
								<label class="col-sm-3 control-label"><?php echo 'Direct Bill'; ?></label>
							<div class="col-sm-3">
								<input type="checkbox" id="direct_bill" name="direct_bill" value="1" style="margin-right:8%;">  <br />
							</div>
							<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($is_app == '1'){ ?>
		      			<label class="col-sm-3 control-label"><?php echo 'App'; ?></label>
		      			<div class="col-sm-3">
							<input type="checkbox" name="is_app" value="1" checked ="checked"  style="margin-right:8%"> <br>
						</div>
						<?php } else { ?>
							<label class="col-sm-3 control-label"><?php echo 'App'; ?></label>
						<div class="col-sm-3">
							<input type="checkbox" id="is_app" name="is_app" value="1" style="margin-right:8%;">  <br />
						</div>
					<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($is_advance == '1'){ ?>
		      			<label class="col-sm-3 control-label"><?php echo 'Advance Order'; ?></label>
		      			<div class="col-sm-3">
							<input type="checkbox" name="is_advance" value="1" checked ="checked"  style="margin-right:8%"> <br>
						</div>
						<?php } else { ?>
							<label class="col-sm-3 control-label"><?php echo 'Advance order'; ?></label>
						<div class="col-sm-3">
							<input type="checkbox" id="is_advance" name="is_advance" value="1" style="margin-right:8%;">  <br />
						</div>
					<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($kot_different == '1'){ ?>
		      			<label class="col-sm-3 control-label"><?php echo 'Kot Diffrent'; ?></label>
		      			<div class="col-sm-3">
							<input type="checkbox" name="kot_different" value="1" checked ="checked"  style="margin-right:8%"> <br>
						</div>
						<?php } else { ?>
							<label class="col-sm-3 control-label"><?php echo 'Kot Diffrent'; ?></label>
						<div class="col-sm-3">
							<input type="checkbox" id="kot_different" name="kot_different" value="1" style="margin-right:8%;">  <br />
						</div>
					<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<?php if($service_charge == '1'){ ?>
		      			<label class="col-sm-3 control-label"><?php echo 'Service charge'; ?></label>
		      			<div class="col-sm-3">
							<input type="checkbox" name="service_charge" value="1" checked ="checked"  style="margin-right:8%"> <br>
						</div>
						<?php } else { ?>
							<label class="col-sm-3 control-label"><?php echo 'Service charge'; ?></label>
						<div class="col-sm-3">
							<input type="checkbox" id="service_charge" name="service_charge" value="1" style="margin-right:8%;">  <br />
						</div>
					<?php } ?>
		       	</div>
		       	<div class="form-group">
			      	<label class="col-sm-3 control-label"><?php echo 'Bill Printer Type'; ?></label>
			      	<div class="col-sm-3">
			          	<select name="bill_printer_type" id="input-bill_printer_type" class="form-control">
			              	<option ><?php echo "Please Select" ?></option>
			          		<?php foreach($bill_printer_types as $rkey => $rvalue){ ?>
			          			<?php if($rkey == $bill_printer_type){ ?>
				      				<option value="<?php echo $rkey ?>" selected="selected"><?php echo $rvalue; ?></option>
			          			<?php } else { ?>
				      				<option value="<?php echo $rkey ?>"><?php echo $rvalue ?></option>
			          			<?php } ?>
			          		<?php } ?>
			        	</select>
			        </div>
		       	</div>
		       	<div class="form-group ">
			      	<label class="col-sm-3 control-label"><?php echo 'Bill Printer Name'; ?></label>
			      	<div class="col-sm-3">
			          	<input type="text" name="bill_printer_name" value="<?php echo $bill_printer_name; ?>" placeholder="<?php echo 'printer Name'; ?>" class="form-control" />
			        </div>
		       	</div>
		  	</form>
	  	</div>
	</div>
</div> 
<?php echo $footer; ?>