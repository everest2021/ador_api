<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right" style="display: none;">
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<form action= "<?php echo $action ?>" method="get" enctype="multipart/form-data" id="status">
				<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
                <span>
                	<select class="pull-right" name="order_status"   id="input-order_status" style="padding: 5px;padding-right: 15px;" class="">
                    	<option  value=""><?php echo "ALL" ?></option>
                    	<?php foreach($statuss as $skey => $svalue){ ?>
                      		<?php if($skey == $order_status){ ?>
                      			<option value="<?php echo $skey ?>" selected = "selected"><?php echo $svalue; ?></option>
                      		<?php } else { ?>
                      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                      		<?php } ?>
                    	<?php } ?>
                	</select>
              	</span>
				<label class="pull-right" style="text-align:left;font-size: 15px;padding-right:15px;"><?php echo 'Order Status'; ?></label>
			</form>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row" >
					<?php foreach($orderarray as $test => $value) { ?>
						<div class="col-md-4" style="border: 1px #000 solid;height: 220px;">
				  			<div class="col-md-2" style="width:40%;margin-top: 10px">
					  			<?php if (!empty($value['kot_no'])) { ?>
						  			<div>
						  				<label style="width:50%;" ><?php echo 'Kot No'; ?></label><span ><?php echo $value['kot_no']?></span>
								  	</div>
							  	<?php }?>
						  		<?php if (!empty($value['bill_no'])){ ?>
								  	<div>
						  				<label style="width:50%;" ><?php echo 'Bill No'; ?></label><span class="orderappmargin"><?php echo $value['bill_no']?></span>
								  	</div>
							  	<?php } ?>
						  		<?php if (!empty($value['table_id'])){ ?>
								  	<div>
						  				<label style="width:50%;" ><?php echo 'Table No'; ?></label><span class="orderappmargin"><?php echo $value['table_id']?></span>
								  	</div>
						  		<?php } ?>
					  			<div>
					  				<label style="width:50%;"><?php echo 'Order No'; ?></label><span class="orderappmargin"><?php echo $value['order_no']?></span>
							  	</div>
					  			<div>
					  				<label style="width:50%;" ><?php echo 'Name'; ?></label><span class="orderappmargin"><?php echo $value['cus_name'] ?></span>
							  	</div>
							  	<div>
					  				<label  style="width:50%;"><?php echo 'Phone no'; ?></label><span class="orderappmargin"><?php echo $value['contact'] ?></span>
					  			</div>
						  		<div>
						  			<label style="width:50%;"><?php echo 'Add'; ?></label><span class="orderappmargin"><?php echo $value['delivery_address']?></span>
						  		</div>
						  		<div>
						  			<a href="<?php echo $value['view']; ?>"  class="btn btn-primary">View</a>
						  		</div>
						  		<!-- <div>
						  			<a href="<?php echo $value['except']; ?>"  class="btn btn-primary">Accept</a>
						  		</div> -->
						  		<div>
						  			<!-- <a href="<?php echo $value['cancle']; ?>"  class="btn btn-primary">Cancel</a> -->
						  		</div>
							</div>
					  		<div class="col-md-2" style="width:60%;margin-top: 10px">
					  			<div>
						  			<label style="margin-right:100px "><?php echo 'Item'; ?></label><span class="orderappmargin">Quantity</span>
						  		</div>
					  			<?php 
					  				$qty = 0;
					  			 	$item = 0;
					  			 	$tot= 0;
					  			?>
					  			<?php foreach($value['itemarray'] as $key) {  ?>
						  			<div>
							  			<label style="width:50%;"><?php echo $key['item_name']; ?></label>
							  			<span class="orderappmargin"><?php echo $key['quantity']?></span>
							  		</div>
					  				<?php $qty = $qty + $key['quantity'];$item++;$tot = $tot + $key['total']; ?>
					  			<?php } ?>
						  		<div>
						  			<label style="width:50%"><?php echo 'Grand Total'; ?></label><span class="orderappmargin"><?php echo $tot?></span>
						  		</div>
						  		<div>
						  			<label style="width:50%"><?php echo 'Total Item'; ?></label><span class="orderappmargin"><?php echo $item?></span>
						  		</div>
						  		<div>
						  			<label style="width:50%"><?php echo 'Total Qutntity'; ?></label><span class="orderappmargin"><?php echo $qty ?></span>
						  		</div>
						  		<div>
						  			<label style="width:50%"><?php echo 'Time'; ?></label><span class="orderappmargin"><?php echo "11.20" ?></span>
						  		</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">	
	// $(document).ready(function () {
 //    		$("#input-order_status").change(function () {
 //    			$("#status").submit();
 //    	});
	// });	

	$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/orderapp&token=<?php echo $token; ?>';

	var order_status = $('select[name=\'order_status\']').val();
	if (order_status) {
			url += '&order_status=' + encodeURIComponent(order_status);
	}
    
	location = url;
});
</script>
<?php echo $footer; ?>