<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		<div class="pull-right">
			<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li>
					<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				</li>
			<?php } ?>
			</ul>
		</div>
  	</div>
  	<div class="container-fluid">
	   	<?php if ($error_warning) { ?>
	   	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   	<button type="button" class="close" data-dismiss="alert">&times;</button>
	   	</div>
		<?php } ?>
	   	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
	   	<div class="panel-body">
		   	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Meal Name'; ?></label>
				<div class="col-sm-3">
					<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_meal_name; ?>" class="form-control" />
				<?php if ($error_name ) { ?>
				<div class="text-danger"><?php echo $error_name ; ?></div>
				<?php } ?>
				</div>
				</div> 
				<div style="display: none" class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Venue Name'; ?></label>
				<div class="col-sm-3"  >
              		<select name="venue_id" id="input-venue_id" class="form-control" >
                  		<option value="0">Please Select </option>
                  		<?php foreach($meal_datas as $skey => $svalue) { ?>
                    	<?php if ($skey == $venue_id) { ?>
                      		<option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    	<?php } else { ?>
                      		<option value="<?php echo $skey; ?>" ><?php echo $svalue; ?></option>
                    	<?php } ?>
                		<?php } ?>
               		</select>
            	</div>
            	</div>
            	<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo 'Select Item'; ?></label>
					<div class="col-sm-3">
						<input type="text" id="item_name" placeholder="<?php echo 'Select Item'; ?>" class="form-control" />
					</div>
				</div>
					<table id="data" class="table table-bordered">
						<tr>
							<th>Item Name</th>
							<th style="display:none">Default Item</th>
							<th>Remove</th>
						</tr>
						<?php $i=0; ?>
						<?php if($testdatas) { ?>
							<?php foreach($testdatas as $testdata) { ?>
								<tr id="re_<?php echo $i ?>">
									<td><input type="text" name="po_datas[<?php echo $i ?>][item]" value="<?php echo $testdata['combo_item'] ?>" class="form-control"/></td>
									<td style="display:none">
									<?php if($testdata['default_item'] == '1'){ ?>
										<input type="checkbox" name="po_datas[<?php echo $i ?>][default_item]" id="default_item_<?php echo $i ?>" value="1" checked="checked" class="defaultitem form-control" style="display:inline-block"/>&nbsp;&nbsp;Yes
									<?php } else { ?>
										<input type="checkbox" name="po_datas[<?php echo $i ?>][default_item]" id="default_item_<?php echo $i ?>" value="1" class="defaultitem form-control" style="display:inline-block"/>&nbsp;&nbsp;Yes
									<?php } ?>
									</td>
									<input type="hidden" name="po_datas[<?php echo $i ?>][itemid]" value="<?php echo $testdata['combo_item_id'] ?>" class="form-control"/>
									<input type="hidden" name="po_datas[<?php echo $i ?>][itemcode]" value="<?php echo $testdata['combo_item_code'] ?>" class="form-control"/>
									<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder(<?php echo $i ?>)" class="button inputs remove " id="remove_<?php echo $i ?>" ><i class="fa fa-trash-o"></i></a></td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						<?php } ?>
					</table>

            	<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Rate Adult'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="rate" value="<?php echo $rate; ?>" placeholder="<?php echo $entry_rate; ?>" class="form-control" />
					<?php if ($error_rate ) { ?>
					<div class="text-danger"><?php echo $error_rate ; ?></div>
					<?php } ?>
					</div>
				</div>
				<div class="form-group required">
					<label class="col-sm-3 control-label"><?php echo 'Rate Child'; ?></label>
					<div class="col-sm-3">
						<input type="text" name="rate_child" value="<?php echo $rate_child; ?>" placeholder="<?php echo 'Rate Child'; ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
						<label class="col-sm-3 control-label" ><?php echo 'Image'; ?></label>
					    <div class="col-sm-3">
					  		<input readonly="readonly" type="text" name="image" value="<?php echo $image; ?>" placeholder="<?php echo 'Image'; ?>" id="input-photo" class="form-control" />
					  		<input type="hidden" name="image_source" value="<?php echo $image_source; ?>" id="input-photo_source" class="form-control" />
					  		<br>
					  		<span class="input-group-btn">
								<button type="button" id="button-photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
					  		</span>
							<?php if($image_source != ''){ ?>
					  			<br/ ><a target="_blank" style="cursor: pointer;" id="photo_source" class="thumbnail" href="<?php echo $image_source; ?>">View Image</a>
							<?php } ?>
						</div>
			    	</div>
		  	</form>
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  	var i = '<?php echo $i ?>';
  	$('#item_name').autocomplete({
	  	delay: 500,
	  	source: function(request, response) {
	    	$.ajax({
	      		url: 'index.php?route=catalog/meal/autocompletename&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      		dataType: 'json',
	      		success: function(json) {   
	        		response($.map(json, function(item) {
	          			return {
	            			label: item.item_name,
	            			value: item.item_id,
	            			code : item.item_code,
	          			}
	        		}));
	      		}
	   		});
	  	}, 
	  	select: function(event, ui) {
		html = '<tr id="re_'+i+'">' 
	      // html += '<td id="'+i+'">';
	      	html += '<td><input type="text" name="po_datas[' + i + '][item]" value="'+ui.item.label+'"  class="form-control"/></td>'
	      	html += '<td style="display:none"><input type="checkbox" name="po_datas[' + i + '][default_item]" id="default_item_'+i+'" value="1" style="display:inline-block" class="defaultitem form-control"/>&nbsp;&nbsp;Yes</td>'
	      	html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+ui.item.value+'"  class="form-control"/>'
	      	html += '<input type="hidden" name="po_datas[' + i + '][itemcode]" value="'+ui.item.code+'"  class="form-control"/>'
	      	html += '<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a></td>'
	      	// html += '</td>';
      	html += '</tr>'
      	$('#data').append(html);
	      
		i++;
	    return false;
	  	},
	  	focus: function(event, ui) {

	    return false;
	  	}
	});

	
	$(document).on('change', '.defaultitem', function(e) {
	  	idss = $(this).attr('id');
	  	s_id = idss.split('_');
	  	default_item = parseFloat($('#default_item_'+s_id[1]).val());
	  	$('#default_item_'+s_id[1]).val(default_item);
	});

	function remove_folder(c) {
		$('#re_'+c).remove();
	}

  $('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
	  clearInterval(timer);
  }
  timer = setInterval(function() {
	if ($('#form-photo input[name=\'file\']').val() != '') {
	  clearInterval(timer); 
	  image_name = 'item';  
	  $.ajax({
		url: 'index.php?route=catalog/meal/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
		type: 'post',   
		dataType: 'json',
		data: new FormData($('#form-photo')[0]),
		cache: false,
		contentType: false,
		processData: false,   
		beforeSend: function() {
		  $('#button-upload').button('loading');
		},
		complete: function() {
		  $('#button-upload').button('reset');
		},  
		success: function(json) {
		  if (json['error']) {
			alert(json['error']);
		  }
		  if (json['success']) {
			alert(json['success']);
			console.log(json);
			$('input[name=\'image\']').attr('value', json['filename']);
			$('input[name=\'image_source\']').attr('value', json['link_href']);
			d = new Date();
			var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
			$('#photo_source').remove();
			$('#button-photo').after(previewHtml);
		  }
		},      
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	}
  }, 500);
});


  </script>
  	<script>
  	$('#colorSelector').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#colorSelector').css('backgroundColor', '#' + hex);
		$('#colorSelector').val('#' + hex);
	},
	onSubmit: function (hsb, hex, rgb) {
		$('#colorSelector').css('backgroundColor', '#' + hex);
		$('#colorSelector').val('#' + hex);
	}
});
</script>
</div> 
<?php echo $footer; ?>