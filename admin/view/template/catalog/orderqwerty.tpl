<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" style="padding-bottom: 0px;">
  <div class="container-fluid" style="padding: 0px;">
	<?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	<?php } ?>
	<?php if ($success) { ?>
	<!--   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div> -->
	<?php } ?>
	<?php if ($warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	<?php } ?>
	<?php $tab_index = 1; ?>
	<div class="panel panel-default" style="border:none;">
		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
		<div class="panel-body" style="padding-bottom: 0%;font-size: 15px;">
	 		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-order" class="form-horizontal">
				<div style="display:inline-block; width:80%;float:left;border-right: solid black 1px;">
			  		<div style="width:100%;height:20%;padding-top: 1%;">
				  		<div class="col-sm-2">
				  			<div class="form-row">
    							<div class="form-group col-sm-9" style="border-top:none;padding-top:0px;padding-left:0px;padding-right: 5px;">
							  		<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" autocomplete="off" type="text" name="table" value="<?php echo $table ?>" placeholder="<?php echo 'Table'; ?>" id="input-t_number" class="form-control inputs" />
							  		<?php $tab_index ++; ?>
							  		<input type="hidden" name="table_id" value="<?php echo $table_id ?>" id="input-t_number_id" class="form-control inputs" />
							  		
							  		<input tabindex="<?php echo $tab_index; ?>" type="text" readonly="readonly" name="location" value="<?php echo $loc ?>" placeholder="<?php echo 'location'; ?>" id="input-location" class="form-control inputs list" style="display: none"/>
							  		<input type="hidden" name="location_id" value="<?php echo $locid ?>" id="input-location_id" class="form-control" />
							  		<input type="hidden" name="parcel_status" value="<?php echo $parcel_detail ?>" id="parcel_status" class="form-control" />
							  		<input type="hidden" name="rate_id" value="<?php echo $rate_id ?>" id="input-rate_id" class="form-control" />
							  		<input type="hidden" name="page_no" value="1" id="input-page_no" class="form-control" />
							  		<input type="hidden" name="nc_kot_status" value="0" id="input-nc_kot_status" class="form-control" />
							  		<input type="hidden" name="service_charge" value="0" id="input-service_charge" class="form-control" />
							  		<input type="hidden" name="category_id" value="" id="input-category_id" class="form-control" />
							  		<input type="hidden" name="date_added" value="<?php echo $date_added; ?>" id="input-date_added" class="form-control" />
							  		<input type="hidden" name="time_added" value="<?php echo $time_added; ?>" id="input-time_added" class="form-control" />
							  		<input type="hidden" name="kot_no" value="<?php echo $kot_no; ?>" id="input-kot_no" class="form-control" />
							  	</div>
							</div>
						</div>
					  	<?php if( $WAITER == '1') { ?>
							<div class="col-sm-3">
						  		<?php if($display_type == '1') { ?>
						  			<div class="form-row" style="margin-left: -10px;">
    									<div class="form-group col-sm-4" style="border-top:none;padding-top:0px;padding-left:0px;padding-right: 5px;">
											<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="waiterid"  value="<?php echo $waiterid ?>" placeholder="<?php echo 'id'; ?>" id="input-waiterid" class="form-control inputs" />
											<?php $tab_index ++; ?>
										</div>
										<div class="form-group col-sm-8" style="border-top:none;padding-top:0px;margin-left: -5px;">
											<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="waiter"  value="<?php echo $waiter ?>" placeholder="<?php echo 'Waiter'; ?>" id="input-waiter" class="form-control inputs" />
										</div>
									</div>
								<?php } else { ?>
									<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="waiterid" onclick="waiterselid()" value="<?php echo $waiterid ?>" placeholder="<?php echo 'id'; ?>" id="input-waiterid" class="form-control inputs" />
									<?php $tab_index ++; ?>
						  			<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="waiter" onclick="waitersel()" value="<?php echo $waiter ?>" placeholder="<?php echo 'Waiter'; ?>" id="input-waiter" class="form-control inputs" />
						  		<?php } ?>
						  		<?php $tab_index ++; ?>
						  		<input type="hidden" name="waiter_id" value="<?php echo $waiter_id ?>" id="input-waiter_id" class="form-control inputs" />
							</div>
					  	<?php } ?>
						<?php if( $CAPTAIN == '1') { ?>
							<div class="col-sm-3">
								<?php if($display_type == '1') { ?>
								<div class="form-row" style="margin-left: -10px;">
    								<div class="form-group col-sm-4" style="border-top:none;padding-top:0px;padding-left:0px;padding-right: 5px;">
										<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="captainid"  value="<?php echo $captainid ?>" placeholder="<?php echo 'id'; ?>" id="input-captainid" class="form-control inputs" />
										<?php $tab_index ++; ?>
									</div>
									<div class="form-group col-sm-8" style="border-top:none;padding-top:0px;margin-left: -5px;">
										<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="captain"  value="<?php echo $captain ?>" placeholder="<?php echo 'Captain'; ?>" id="input-captain" class="form-control inputs" />
									</div>
								</div>	
								<?php } else { ?>
									<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="captainid"  onclick="captainselid()" value="<?php echo $captainid ?>" placeholder="<?php echo 'id'; ?>" id="input-captainid" class="form-control inputs" />
									<?php $tab_index ++; ?>
						  			<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="text" name="captain" onclick="captainsel()" value="<?php echo $captain ?>" placeholder="<?php echo 'Captain'; ?>" id="input-captain" class="form-control inputs" />
						  		<?php } ?>
						  		<?php $tab_index ++; ?>
						  		<input type="hidden"  value="<?php echo $captain_id ?>" name="captain_id" id="input-captain_id" class="form-control inputs" />
							</div>
						<?php } ?>
						<?php if( $PERSONS == '1') { ?>	
							<div class="col-sm-2" style="margin-left: -30px;">
								<?php if($display_type == '1') { ?>
								<div class="form-row">
	    							<div class="form-group col-sm-9" style="border-top:none;padding-top:0px;padding-left:0px;padding-right: 5px;">
								  		<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" type="number"  value="<?php echo $person ?>"  name="person"  placeholder="<?php echo 'Person'; ?>" id="input-person" class="form-control inputs" />
								  	</div>
								</div>
						  		<?php } else { ?>
						  		<input style="font-weight: bold;color: #000000 !important;font-size: 20px !important;" tabindex="<?php echo $tab_index; ?>" onclick="select_persons()" value="<?php echo $person ?>" name="person"  placeholder="<?php echo 'Person'; ?>" id="input-person" class="form-control inputs" />
								<?php } ?>
								<?php $tab_index ++; ?>
							</div>
						<?php } ?>
						<?php if($WAITER == '0') { ?>
							<div class="col-sm-3">
								<a href="javascript:history.go(0)" class="btn btn-primary">Refresh</a>
							</div>
						<?php } else { ?>
							<div class="col-sm-1">
								<a href="javascript:history.go(0)" class="btn btn-primary">Refresh</a>
							</div>
						<?php } ?>
			  		</div>
			  		<div style="width:100%; float:left;overflow-y: auto;height:500px;">
						<table style="" class="table table-bordered table-hover ordertab" id="ordertab">
					  		<tr>
								<td style="width:15%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Code</td>
								<td style="width:30%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Name</td>
								<td style="width:8%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Qty</td>
								<td style="width:13%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Rate</td>
								<td style="width:13%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">Amt</td>
								<td style="width:25%;padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;">KOT</td>
								<td style="font-weight: bold;font-size: 20px;">R</td>
					  		</tr>
					  		<?php $extra_field_row = 1; ?>
						</table>
			  		</div>
			  		<?php $tab_index_1 = 500; ?>
				  	<div style="width: 100%;float:left;border-top: solid black 1px;">
				  	<br>
						<div class="row" style="margin-left: 0%;">	
							<div style="width:15%;float:left;margin-top:0%;margin-left:0%">
						  		<label  class="control-label" for="input-ftotal" style="font-size:15px">Food Total</label>
						  		<input type="hidden" name="ids" value="<?php echo $extra_field_row ?>" id="input-ids" />
							</div>
							<div class="col-sm-3" style="margin-top:0%">
						  		<input type="number" readonly="readonly" name="ftotal" value="<?php echo $ftotal ?>"  placeholder="<?php echo 'Food Total'; ?>" id="input-ftotal" class="form-control inputs" />
						  		<input type="hidden" readonly="readonly" name="ftotal_discount" value=""  id="ftotal_discount" class="form-control" />
							</div>
							<div style="width:15%;float:left;padding-left:10%;margin-top:0%">
						  		<label  class="control-label" for="input-gst" style="font-size:15px">GST</label>
							</div>
							<label>Advance No</label>
							<div class="col-sm-3" style="margin-right:0%;margin-top:0%;">
						  		<input type="number"  readonly="readonly" name="gst" value="<?php echo $gst ?>" placeholder="<?php echo 'GST'; ?>" id="input-gst" class="form-control inputs" />
							</div>
						</div>
						<div class="row" style="margin-left: 0%;">
							<div style="width:15%;float:left;padding-top: 0%;margin-left:0%">
						  		<label  class="control-label" for="input-ltotal" style="font-size:15px">LIQ. Total </label>
							</div>
							<div class="col-sm-3" style="padding-top: 0%;">
						  		<input type="number"  readonly="readonly" name="ltotal" value="<?php echo $ltotal ?>" placeholder="<?php echo 'LIQ.Total'; ?>" id="input-ltotal" class="form-control inputs" />
								<input type="hidden" readonly="readonly" name="ltotal_discount" value="" id="ltotal_discount" class="form-control" />
							</div>
							<div style="width:15%;float:left;padding-left:10%;padding-top: 0%;">
						  		<label  class="control-label" for="input-vat" style="font-size:15px">VAT</label>
							</div>
							<div class="col-sm-3" style="margin-right:0%;padding-top: 0%;">
						  		<input type="number"  readonly="readonly" name="vat" value="<?php echo $vat ?>" placeholder="<?php echo 'VAT'; ?>" id="input-vat" class="form-control inputs" />
							</div>
							<div class="col-sm-2" style="padding-left:5px;padding-top: 0%;">
						  		<input type="text" name="advance_id" placeholder="<?php echo 'Advance'; ?>" value="<?php echo $advance_billno ?>" id="advance_id" class="form-control inputs" />
							</div>
						</div>
						<div class="row" style="margin-left: 0%;">
							<div style="width:15%;float:left;padding-top: 0%;margin-left:0%">
						  		<label  class="control-label" for="input-cess" style="font-size:15px">Cess </label>
							</div>
							<div class="col-sm-3" style="padding-top: 0%;">
						  		<input tabindex="<?php echo $tab_index_1; ?>" type="number" name="cess" value="<?php echo $cess ?>"  placeholder="<?php echo 'Cess'; ?>" id="input-cess" class="form-control inputs" />
								<?php $tab_index_1 ++; ?>
							</div>
							<div style="width:15%;float:left;padding-left:0%;padding-top: 0%;">
						  		<label  class="control-label" for="input-stax" style="font-size:15px;margin-left:50px;">Service Charge</label>
							</div>
							<div class="col-sm-3" style="margin-right:0%;padding-top: 0%;">
						 		<input tabindex="<?php echo $tab_index_1; ?>" type="number" name="stax" value="<?php echo $stax ?>"  placeholder="<?php echo 'Service Tax'; ?>" id="input-stax" class="form-control inputs" />
								<?php $tab_index_1 ++; ?>
							</div>
							<div class="col-sm-2" style="padding-left:5px;padding-top: 0%;">
						  		<!-- <input type="text" name="advance_id" placeholder="<?php echo 'Advance'; ?>" value="<?php echo $advance_billno ?>" id="advance_id" class="form-control inputs" /> -->
						  		<input type="text" name="advance_amount" readonly="readonly" id="advance_amount" value="<?php echo $advance_amount ?>" class="form-control inputs"/>
							</div>
						</div>
						<div class="row" style="margin-left: 0%;">
							<div style="width:15%;float:left;padding-top: 0%;margin-left:0%;">
						  		<label  class="control-label" for="input-discount" style="font-size:15px">F.Discount </label>
						  		<label  class="control-label" style="font-size:15px">Delivery Charge </label>
							</div>
							<div class="col-sm-3" style="padding-top: 0%;">
								<input tabindex="<?php echo $tab_index_1; ?>" type="number" name = "fdiscountper" value="<?php echo $fdiscountper ?>" placeholder="%" id="input-fdiscountper" class="form-control inputs" style="width: 60px;float: left"/>
								<?php $tab_index_1 ++; ?>
						  		<input tabindex="<?php echo $tab_index_1; ?>" type="number" name="discount" value="<?php echo $discount ?>"  placeholder="Rs" id="input-fdiscount" class="form-control inputs" style="width: 60px;float: left;"/>
						  		<input type="hidden" name="ftotalvalue" value="<?php echo $ftotalvalue ?>" id="input-ftotalvalue" class="form-control inputs" />
						  		<?php $tab_index_1 ++; ?>
								<h5 id="fdisval" style="float: left;margin-left: 9px;margin-top: 9px;"></h5>
								<br><br>
								<input tabindex="<?php echo $tab_index_1; ?>" type="number" name = "dchargeper" value="<?php echo $dchargeper ?>" placeholder="%" id="input-dchargeper" class="form-control inputs" style="width: 60px;float: left"/>
								<?php $tab_index_1 ++; ?>
						  		<input tabindex="<?php echo $tab_index_1; ?>" type="number" name="dcharge" value="<?php echo $dcharge ?>"  placeholder="Rs" id="input-dcharge" class="form-control inputs" style="width: 60px;float: left;"/>
						  		<input type="hidden" name="dtotalvalue" value="<?php echo $dtotalvalue ?>" id="input-dtotalvalue" class="form-control inputs" />
						  		<?php $tab_index_1 ++; ?>
								<h5 id="dchargeval" style="float: left;margin-left: 9px;margin-top: 9px;"></h5>
							</div>
							<div style="width:15%;float:left;padding-left:2%;padding-top: 0%;">
						  		<label class="control-label" for="input-stax" style="font-size:15px;margin-left:50px;">L.Discount</label>
							</div>
							<div class="col-sm-3" style="margin-right:0%;padding-top: 0%;">
								<input tabindex="<?php echo $tab_index_1; ?>" type="number" placeholder="%" name = "ldiscountper" value="<?php echo $ldiscountper ?>"  id="input-ldiscountper" class="form-control inputs" style="width: 60px;float: left"/>
								<?php $tab_index_1 ++; ?>
						  		<input tabindex="<?php echo $tab_index_1; ?>" type="number" name="ldiscount" value="<?php echo $ldiscount ?>"  placeholder="Rs" id="input-ldiscount" class="form-control inputs" style="width: 60px;float: left"/>
						  		<input type="hidden" name="ltotalvalue" value="<?php echo $ltotalvalue ?>" id="input-ltotalvalue" class="form-control inputs" />
								<?php $tab_index_1 ++; ?>
								<span id="ldisval" style="float: left;margin-left: 9px;margin-top: 9px;"></span>
							</div>
							<div id="divdayclose" style="width:15%;float:left;padding-top: 0%;margin-top: 10px;text-align: center;">
						  		<span id="dayclose" style="white-space: pre-wrap;xfloat: left;xmargin-left: 9px;xmargin-top: 9px;"></span>
							</div>
						</div>
						<div class="col-sm-12" style="margin-left: 0%;background-color: black;color: white">
							<div style="float:left;padding-top: 0%;margin-left:0%;margin-bottom: 0%;">
						  		<label  class="control-label" for="input-grand_total" style="font-size:20px;font-weight: bold;">Grand Total : </label> &nbsp;&nbsp;
								<span class="grand_total_span" style="font-size: 20px;font-weight: bold;"><?php echo $grand_total ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
						  		<input type="hidden" readonly="readonly" name="grand_total" value="<?php echo $grand_total ?>" id="input-grand_total" class="form-control inputs" />
						  		<input type="hidden" id="oldgrand" class="form-control inputs" />
						  		<input type="hidden" name="roundtotal" value="<?php echo $roundtotal ?>" id="input-roundtotal"/>

								<label  class="control-label" for="input-total_items" style="font-size:20px;font-weight: bold;">Total Items : </label> &nbsp;&nbsp;
								<span class="total_items_span"  style="font-size: 20px;font-weight: bold;"><?php echo $total_items ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
								<input type="hidden" readonly="total_items" name="total_items" value="<?php echo $total_items ?>" id="input-total_items" class="form-control inputs" />

								<label  class="control-label" for="input-item_quantity" style="font-size:20px;font-weight: bold;">Item Quantity : </label> &nbsp;&nbsp;
								<span class="item_quantity_span"  style="font-size: 20px;font-weight: bold;"><?php echo $item_quantity ?></span> &nbsp;&nbsp;
						  		<input type="hidden" name="item_quantity" value="<?php echo $item_quantity ?>" id="input-item_quantity" class="form-control inputs" />
							</div>
						</div>
					</div>
				</div>
				<div class="btn btn-info" onclick="check_kot()" style="float: left; font-size: 13px;padding-top: 7px;margin-top: 0%;margin-left: 5px;padding-bottom: 7px;height: 38px;">CHECK KOT</div>
				<div class="col-sm-2 main "style="float:right;font-size: 10px;margin-top: 0% !important;">
					<a onClick="moreoption()" data-toggle="tooltip" title="<?php echo 'MORE OPTIONS' ?>" class="btn btn-info sub">MORE OPTIONS</a>
				</div>
				<div style="display:inline-block;overflow:auto; ;height:600px; width:20%;float:right;">
	  				<div class="row" style="margin-left:20%">
						<!-- <div class="col-sm-3 main">
							<a id="kot-input" onclick="kot_fun()" data-toggle="tooltip" title="<?php echo 'KOT'; ?>" class="btn btn-info sub" >KOT</a>
						</div>
						<div class="col-sm-3 main">
							<a data-toggle="tooltip" title="<?php echo 'BILL' ?>" class="btn btn-info sub" onclick="bill()">BILL</a>
						</div>
						<div class="col-sm-3 main">
							<a onclick="paymentmode()" data-toggle="tooltip" title="<?php echo 'SETTLEMENT' ?>" class="btn btn-info sub">SETTLEMENT</a>
						</div>
						<div class="col-sm-3 main">
							<a class="btn btn-info sub" onclick="check_kot()">CHECK KOT</a>
						</div>
						<div class="col-sm-3 main">
						<a class="btn btn-info sub" id="billview">BILL VIEW</a>
						</div>
						<div class="col-sm-3 main">
							<a class="btn btn-info sub" onclick="pendingtable_fun()">PENDING TABLE</a>
						</div> -->
						<!-- <div class="col-sm-3 main">
							<a onClick="moreoption()" data-toggle="tooltip" title="<?php echo 'MORE OPTIONS' ?>" class="btn btn-info sub">MORE OPTIONS</a>
						</div> -->
	  				</div>
	  				<br>
	  				<table class="table table-bordered" id="runningtable"  style="font-size: 12px; padding:3px;">
					</table>
						<input type="hidden" name="kot_print_status" id="kot_print_status" value="0"/>

				</div>
				
			</form>
		</div>
	</div>
</div>
<div id="dialog-form" title="Select">
</div>
<div id="dialog-form1" title="Register">
</div>
<div id="dialog-form_merge" title="Add Bill">
</div>
<div id="dialog-form_changetable" title="Change Table">
</div>
<div id="dialog-form_dayclose" title="Day Close">
</div>
<div id="dialog-form_moreoption" title="More Options">
</div>
<div id="dialog-form_pendinginfo" title="Pending Info">
</div>
<div id="dialog-form_currentsaleinfo" title="Current Sale Info">
</div>
<div id="dialog-form_help" title="Help">
</div>
<div id="dialog-form_select_qty" title="Select Quantity">
</div>
<div id="dialog-form_select_rate" title="Select Rate">
</div>
<div id="dialog-form_select_persons" title="Select Number of Persons">
</div>
<div id="dialog-form_kottransfer" title="KOT Transfer">
</div>
<div id="dialog-form_nckot" title="NC KOT">
</div>
<div id="dialog-form_paymentmode" title = "Payment Mode">
</div>
<div id="dialog-form_tendering" title = "Tendering">
</div>
<div id="dialog-form_parceldetail" title="Parcel Detail">
</div>
<div id="dialog-form_complimentary" title="Complimentary">
</div>
<div id="myModalModifier" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
		  <div class="modal-header" id="modifierheader">
		  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		  </div>
		  <div class="modal-body" style="height: 200px;overflow-y: auto;" id="modifierbody">
		  </div>
		  <div class="modal-footer">
		  	<button id="savemodifier" onclick="savemodifier(this.id)" class="btn btn-width bkgrnd-cyan save-details" type="button" name="save-details">Save</button>
		  	<button type="button" id="closemodifier" class="btn btn-primary" onclick="savemodifier(this.id)" >Close</button>
		  </div>
	  </div>
	 </div>
</div>
<div id="myModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
		  <div class="modal-header">
		  	<button type="button" class="close" id="logout" data-dismiss="modal" aria-hidden="true">&times;</button>
		  </div>
		  <div class="modal-body" style="height: 500px;">
		    <iframe style="border: 0px; " src="index.php?route=catalog/dayclose&token=<?php echo $token; ?>&dayclose=1" width="100%" height="100%"></iframe>
		  </div>
	  </div>
	 </div>
</div>
<div id="myModalPendingPrints" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content" style="width:650px;">
		  <div class="modal-header">
		  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		  </div>
		  <div class="modal-body" style="height: 500px;">
		    <div id="exTab2" class="container-fluid">	
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs">
							<li class="active">
					    		<a href="#1" data-toggle="tab">Pending Kot</a>
							</li>
							<li>
								<a href="#2" data-toggle="tab">Pending Bill</a>
							</li>
						</ul>
						<div class="tab-content">
						  	<div style="overflow:auto; ;height:400px;" class="tab-pane active" id="1">
					  			<table class="table table-bordered" id="tablefood" style="cursor: pointer;">
						  			<thead>
							  			<tr>
							  				<th>Bill Date</th>
							  				<th>Kot No.</th>
							  				<th>Table No.</th>
							  				<th>Total Items</th>
							  				<th>Delete</th>
							  				<th>Select Printer</th>
							  				<th>Print</th>
							  			</tr>
							  		</thead>
						  			<tbody id="pendingkot_tbody">
						  			</tbody>
					  			</table>
							</div>
							<div  style="overflow:auto; ;height:400px;" class="tab-pane" id="2">
					  			<table class="table table-bordered" id="tableliq" style="cursor: pointer;">
						  			<thead>
							  			<tr>
							  				<th>Bill Date</th>
							  				<th>Ref No.</th>
							  				<th>Bill No.</th>
							  				<th>Table No.</th>
							  				<th>Amount</th>
							  				<th>Delete</th>
							  				<th>Select Printer</th>
							  				<th>Print</th>
							  			</tr>
						  			</thead>
						  			<tbody id="pendingbill_tbody">
						  			</tbody>
					  			</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
		  	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		  </div>
	  </div>
	</div>
<style>
#billcall{
	display:block;
	border:solid #ccc 1px;
  	cursor: pointer;
}
#kot-input{
	display:block;
	border:solid #ccc 1px;
  	cursor: pointer;
}
#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
</div>

<script type="text/javascript">

$(document).ready(function(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/tables&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			for(var i in data){
				html = "<tr style='text-align:center;color:black' bgcolor="+data[i].color+">";
					html += '<td>'+data[i].table_no+'</td>';
					html += '<td>'+data[i].amount+'</td>';
					html += '<td>'+data[i].time_added+'</td>';
				html += '<tr>';
				$('#runningtable').append(html); 
			}
		}
  	});
});

$(document).on('click','#logout',function(e){
	clear_previous = '<?php echo $clear_previous ?>';
	login_url = '<?php echo $login_url ?>';
	if(clear_previous == 1){
		$('#logout').removeAttr('data-dismiss','modal');
		location = login_url;
	} else{
		$('#logout').attr('data-dismiss','modal');
	}
});

function pendingPrints(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/get_pending_datas&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			if(data.status == 1){
				$('#tablefood tbody').html('');
				$('#tablefood tbody').append(data.html_kot);
				$('#tableliq tbody').html('');
				$('#tableliq tbody').append(data.html_bill);
				$('#myModalPendingPrints').modal({show:true});

				$('.kot').each (function() {
					id = $(this).attr('id');
				  	s_id = id.split('_');
					selectid = s_id[1];
					var printurl = $('#printkot_'+selectid).attr('href') + '&printkot=default';
					$('#printkot_'+selectid).attr('href',printurl);
				});

				$('.bill').each (function() {
					id = $(this).attr('id');
				  	s_id = id.split('_');
					selectid = s_id[1];
					var printurl = $('#printbill_'+selectid).attr('href') + '&printbill=default';
					$('#printbill_'+selectid).attr('href',printurl);
				});

				$('.kot').change(function(){
					// var re = new RegExp("&printkot=\\d+");
					// console.log(newUrl.replace(/([&\?]printkot=default*$|printkot=default&|[?&]printkot=default(?=#))/, ''));
					// console.log(newUrl.replace(re, ''));
					id = $(this).attr('id');
				  	s_id = id.split('_');
					selectid = s_id[1];

					var newUrl = $('#printkot_'+selectid).attr('href');

					test = newUrl.split('&');
					url = newUrl.replace(test[3],'');

					$('#selectedprinterkot_'+selectid).val($('#selectprinterkot_'+selectid).val());

					var printid = $('#selectedprinterkot_'+selectid).val();

					var printurl = url + 'printkot=' + printid;
					$('#printkot_'+selectid).attr('href',printurl);
				});

				$('.bill').change(function(){
					// var re = new RegExp("&printkot=\\d+");
					// console.log(newUrl.replace(/([&\?]printkot=default*$|printkot=default&|[?&]printkot=default(?=#))/, ''));
					// console.log(newUrl.replace(re, ''));
					id = $(this).attr('id');
				  	s_id = id.split('_');
					selectid = s_id[1];

					var newUrl = $('#printbill_'+selectid).attr('href');

					test = newUrl.split('&');
					url = newUrl.replace(test[3],'');

					$('#selectedprinterbill_'+selectid).val($('#selectprinterbill_'+selectid).val());

					var printid = $('#selectedprinterbill_'+selectid).val();

					var printurl = url + 'printbill=' + printid;
					
					$('#printbill_'+selectid).attr('href',printurl);
				});
			}
		}
	});
}

$('.kot').change(function(){
	id = $(this).attr('id');
  	s_id = id.split('_');
	selectid = s_id[1];

	var newUrl = $('#printkot_'+selectid).attr('href');

	test = newUrl.split('&');
	url = newUrl.replace(test[3],'');

	$('#selectedprinterkot_'+selectid).val($('#selectprinterkot_'+selectid).val());

	var printid = $('#selectedprinterkot_'+selectid).val();

	var printurl = url + 'printkot=' + printid;
	$('#printkot_'+selectid).attr('href',printurl);
});

$('.bill').change(function(){
	id = $(this).attr('id');
  	s_id = id.split('_');
	selectid = s_id[1];

	var newUrl = $('#printbill_'+selectid).attr('href');

	test = newUrl.split('&');
	url = newUrl.replace(test[3],'');

	$('#selectedprinterbill_'+selectid).val($('#selectprinterbill_'+selectid).val());

	var printid = $('#selectedprinterbill_'+selectid).val();

	var printurl = url + 'printbill=' + printid;
	$('#printbill_'+selectid).attr('href',printurl);
});

$(document).ready(function(){
	$('.kot').each (function() {
		id = $(this).attr('id');
	  	s_id = id.split('_');
		selectid = s_id[1];
		var printurl = $('#printkot_'+selectid).attr('href') + '&printkot=default';
		$('#printkot_'+selectid).attr('href',printurl);
	});

	$('.bill').each (function() {
		id = $(this).attr('id');
	  	s_id = id.split('_');
		selectid = s_id[1];
		var printurl = $('#printbill_'+selectid).attr('href') + '&printbill=default';
		$('#printbill_'+selectid).attr('href',printurl);
	});
});

var dayclosestatus = '<?php echo $dayclose ?>';
var DAYCLOSE_POPUP = '<?php echo $DAYCLOSE_POPUP?>';
var billdate = '<?php echo $bill_date ?>';
$( document ).ready(function() {
	clear_previous = '<?php echo $clear_previous ?>';
	if(DAYCLOSE_POPUP == 1){
		if(clear_previous == 1){
			$('#myModal').modal({show:true});
		}
		if(dayclosestatus == '0' && document.cookie.indexOf('visited=true') == -1){
			$('#myModal').modal({show:true});
			var now = new Date();
			var expires = new Date();
			expires.setFullYear(now.getFullYear());
			expires.setMonth(now.getMonth());
			expires.setDate(now.getDate()+1);
			expires.setHours(0);
			expires.setMinutes(0);
			expires.setSeconds(0);
		    document.cookie = "visited=true;expires=" + expires.toUTCString();	
		}
	}
	skip_table = '<?php echo $SKIPTABLE; ?>';
	if(skip_table == 1){
		$('#input-t_number').val(1);
		$('#input-t_number_id').val(1);
		findtable();
	}
}); 
if(dayclosestatus == '0'){
	$('#divdayclose').css('background-color','red');
	$('#dayclose').text("Day not closed" + " \n " + billdate);
	$('#dayclose').css({
					   'color' : 'white',
					   'text-align' : 'center'
					});
}else{
	$('#divdayclose').css('background-color','green');
	$('#dayclose').text("Today's date" + " \n " + billdate);
	$('#dayclose').css({
					   'color' : 'white',
					   'text-align' : 'center'
					});
}


$('#form-order').keydown(function(e) {
	if (e.keyCode == 33) {
		paymentmode();
	}
});

$('#advance_id').keydown(function(e){
	old = parseFloat($('#oldgrand').val()); 
	if (e.keyCode == 13) {
		var advance_id = $('#advance_id').val();
		displayed_grand_total = parseFloat($('#input-grand_total').val());
		$.ajax({
			url: 'index.php?route=catalog/orderqwerty/autocompleteadvance&token=<?php echo $token; ?>&advance_id=' + advance_id,
			type:"POST",
			dataType:"json",
			success: function(json) {  
				old = parseFloat($('#oldgrand').val()); 
		  		$('#advance_amount').val(json.advance_amt);
		  		grand_total = displayed_grand_total - json.advance_amt;
		  		advanceamount = parseFloat($('#advance_amount').val());
		  		if(advanceamount != '' || advanceamount != '0' || !isNaN(advanceamount)){
			  		$('#input-grand_total').val(grand_total);
			  		$('.grand_total_span').html(grand_total);
		  		}
			}
		});
		if(advance_id == ''){
			$('#advance_id').val('0');
			$('#advance_amount').val('0.00');
	  		$('#input-grand_total').val(old);
	  		$('.grand_total_span').html(old);
		}
	}
	if($('#advance_amount').val() != '0.00'){
		$('#advance_id').val('0');
		$('#advance_amount').val('0.00');
  		$('#input-grand_total').val(old);
  		$('.grand_total_span').html(old);
	}
});
</script>
<script type="text/javascript">
$(document).on('click', '.qty', function(e) {
	idss = $(this).attr('id');
  	s_id = idss.split('_');
	$('#qty_'+s_id[1]).select();
});

$(document).on('keyup', '.code', function(e) {
	if(	$(this).val() < 0 ){
		$(this).val(1);
	}
});

</script>
<script type="text/javascript"><!--
var tab_index = '<?php echo $tab_index; ?>';
function kot_fun(){
	id_length = $('#code_1').length;
	orderid = $('#orderid').val();
	bill_status = $('#bill_status').val();
	var edit = '<?php echo $editorder ?>';
	if(id_length > 0){
		var personss = $('#input-person').val();
		//console.log(personss);
		var person_compulsary = '<?php echo $PERSONS_COMPULSARY; ?>';
		if((personss == '' || personss == undefined || personss <= '0' || personss <= 0) && person_compulsary == 1){
			alert("Minimum person should be one");
			$('#input-person').val(0);
			
		} else {
			code_value = $('#code_1').val();
			if(code_value != '' ){
				if((bill_status == 0) || (bill_status != '0' && edit == '1')){
					var count = 0;
					$('.qty').each(function( index ) {
						if($(this).val() != ''){
							count ++;
						}
					});
					count ++;
					$('#name_'+count).removeClass("names");
					$('#kot-input').attr('onclick','').unbind('click');
					/*
					action1 = '<?php echo $action; ?>';
					action1 = action1.replace("&amp;", "&");
					action1 = action1.replace("&amp;", "&");
					if(edit == '1'){
						action1 = action1.replace("&amp;", "&");
					}
					//$('#form-order').attr("target","_blank");
					$('#form-order').attr("action", action1); 
					$('#form-order').submit();
					return false;
					//setTimeout(close_fun, 50);
					*/
					$(document).ajaxSend(function() {
						$("#overlay").fadeIn(300);　
					});
					var data = $('form').serialize();
					$.ajax({
						type: "POST",
					  	url: 'index.php?route=catalog/orderqwerty/prints&token=<?php echo $token; ?>',
					  	data: data,
					  	dataType: 'json',
					  	success: function(json) {
			  				if(json.status == 1){
			  					direct_bill = json.direct_bill;
				  				orderid = json.order_id;
				  				grand_total = json.grand_total;
				  				orderidmodify = json.orderidmodify;
					  			edit = json.edit;

					  			if(edit == 1){
									htmlc = '<iframe id="paymentmode" style="border: 0px;" src="index.php?route=catalog/settlement&token=<?php echo $token; ?>&order_id='+orderid+'&orderidmodify='+orderidmodify+'" width="100%" height="100%"></iframe>';
									dialog_paymentmode.dialog("open");
									$('#dialog-form_paymentmode').html(htmlc)
									//location = url;
								} else {
					  				tendering_status = '<?php echo $TENDERING_ENABLE; ?>';
									if(tendering_status == 1 && direct_bill == 1){
										htmlc = '<iframe id="tendering" style="border: 0px;" src="index.php?route=catalog/tendering&token=<?php echo $token; ?>&order_id='+orderid+'&grand_total='+grand_total+'" width="100%" height="100%"></iframe>';
										dialog_tendering.dialog("open");
										$('#dialog-form_tendering').html(htmlc)
										//location = url;
									} else {
										window.location.reload();
									}
								}
							} else {
								window.location.reload();
							}
						
				  		}
					}).done(function() {
						setTimeout(function(){
							$("#overlay").fadeOut(300);
						},500);
					});
				} else {
					alert('Table Already Billed');
					return false;	
				}
			} else {
				alert('Please Select Data To Save');
				return false;
			}
		}
	} else {
		alert('Please Select Data To Save');
		return false;
	}
}

function check_kot(){
	id_length = $('#code_1').length;
	orderid = $('#orderid').val();
	bill_status = $('#bill_status').val();
	var edit = '<?php echo $editorder ?>';


	if(id_length > 0){
		code_value = $('#code_1').val();
		if(code_value != '' ){
			if((bill_status == 0) || (bill_status != '0' && edit == '1')){
				$('#kot-input').attr('onclick','').unbind('click');
				action1 = '<?php echo $action; ?>';
				action1 = action1.replace("&amp;", "&");
				// action1 = action1.replace("&amp;", "&");
				// if(edit == '1'){
				// 	action1 = action1.replace("&amp;", "&");
				// }
				// action1 = action1+'&checkkot='+1;
				// alert(action1);
				// $('#form-order').attr("action", action1); 
				// $('#form-order').submit();
				// return false;

				$(document).ajaxSend(function() {
						$("#overlay").fadeIn(300);　
					});
					var data = $('form').serialize();
					$.ajax({
						type: "POST",
					  	url: 'index.php?route=catalog/orderqwerty/prints&token=<?php echo $token; ?>&order_id=' + orderid+'&checkkot=' +1,
					  	data: data,
					  	dataType: 'json',
					  	success: function(json) {
							window.location.reload();
				  		}
					})
					.done(function() {
						setTimeout(function(){
							$("#overlay").fadeOut(30);
						},10);
					});
			} else {
				alert('Table Already Billed');
				return false;	
			}
		} else {
			alert('Please Select Data To Save');
			return false;
		}
	} else {
		alert('Please Select Data To Save');
		return false;
	}
}


function close_fun(){
  window.location.reload();
}

$('input[name=\'location\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/orderqwerty/autocomplete1&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
			  		return {
						label: item.location,
						value: item.location_id
			  		}
				}));
		  	}
			});
	  	}, 
	  	select: function(event, ui) {
			$('input[name=\'location\']').val(ui.item.label);
			$('input[name=\'location_id\']').val(ui.item.value);
			$('.dropdown-menu').hide();    
			$('#input-t_number').focus();
			return false;
	  	},
  		focus: function(event, ui) {
		return false;
  	}
});
</script>
<script type="text/javascript">
function findtable(){
	var filter_table = $('input[name=\'table\']').val();
	var filter_table = filter_table.replace(/[^A-Z0-9]+/i, '');
	if(filter_table != '' && filter_table != undefined && filter_table != '0'){
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/orderqwerty/findtable&token=<?php echo $token; ?>&filter_tname=' +filter_table,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				if(data.status == 1){
					inn = 1;
					h_table_id = $('#input-t_number_id').val();
					if(h_table_id != ''){
						if(h_table_id != data.table_id){
							inn = 0;
						}
					}
					if(inn == 1){
						$('input[name=\'table\']').val(data.name);
						$('input[name=\'table\']').prop('readonly', true);
						$('input[name=\'table_id\']').val(data.name);
						$('input[name=\'table_id\']').prop('readonly', true);
						$('input[name=\'location\']').val(data.loc_name);
						$('input[name=\'location_id\']').val(data.loc_id);
						$('input[name=\'rate_id\']').val(data.rate_id);
						$('input[name=\'parcel_status\']').val(data.parcel_detail);
						$('input[name=\'service_charge\']').val(data.service_charge);
						tablein();
					} else{
						location.replace('index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
					}
				} else {
					if(data.status == 2){
						alert('Table Already Running');
						location.replace('index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
						return false;
					} else {
						alert('Table Does Not Exist');
						location.replace('index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
						return false;
					}
				}
			}
		});
	} else {
		alert('Table Does Not Exist');
		window.location.reload();
		return false;
	}
	return false;
}

$(document).ready(function(){
	$('#myModalmodifier').hide();
});

function savemodifier(id){
	var checkedclass;
	var rate = 0;
	$.each($("#modifierbody :input[type='checkbox']:checked"), function(){  
		value = $(this).val();
		checkedclass = $(this).attr('class');
		if(id == 'savemodifier'){
			rate = rate + parseInt($('#rate_'+value).val());
			$('#cancelmodifier_'+value).val(0);
		}
		if(id == 'closemodifier'){
			previouslychecked = parseInt($('#cancelmodifier_'+value).val());
			if(previouslychecked == 0){
				rate = rate + parseInt($('#rate_'+value).val());
			}
		}
	});

	$.each($("#modifierbody :input[type='checkbox']:not(:checked)"), function(){  
		value = $(this).val();
		if(id == 'savemodifier'){
			$('#cancelmodifier_'+value).val(1);
		}
	});
	
	checkedlength = $("#modifierbody :input[type='checkbox']:checked").length;
	id = checkedclass.split('_');
	modifierqty = $('#modifierid_'+id[1]).val();
	if(checkedlength > modifierqty){
		alert("Quantity cannot be greater than "+modifierqty);
		$('#savemodifier').removeAttr("data-dismiss","modal"); 
		$('#closemodifier').removeAttr("data-dismiss","modal"); 	
		return false;
	} else{
		$('#savemodifier').attr("data-dismiss","modal"); 
		$('#closemodifier').attr("data-dismiss","modal"); 
	}

	displayrate = parseFloat($('#rate_'+id[1]).val());
	displayqty = parseFloat($('#qty_'+id[1]).val());
	displayamt = parseFloat($('#amt_'+id[1]).val());
	displayis_liq = parseInt($('#is_liq_'+id[1]).val());
	displayftotal = parseFloat($('#input-ftotal').val());
	displayltotal = parseFloat($('#input-ltotal').val());
	displaygrandtotal = parseFloat($('#input-grand_total').val());
	modifierrate = displayrate + rate;
	modifieramt = displayqty * modifierrate;
	$('#rate_'+id[1]).val(modifierrate);
	$('#amt_'+id[1]).val(modifieramt);
	if(id[1] == '1' && displayis_liq == 0 ){
		$('#input-ftotal').val(modifieramt);
	} else{
		if(displayis_liq == 0){
			ftotal = (displayftotal + modifieramt) - displayamt;
			$('#input-ftotal').val(ftotal);
		}
	}

	if(id[1] == '1' && displayis_liq == 1){
		$('#input-ltotal').val(modifieramt);
	} else{
		if(displayis_liq == 1){
			ltotal = (displayltotal + modifieramt) - displayamt;
			$('#input-ltotal').val(ltotal);
		}
	}

	$('.qty').trigger("change");
	idtouch = parseInt(id[1]);
	addid = idtouch + 1;
	$('#qty_'+id[1]).focus();
	$('#code_'+addid).focus();
}

function findcode(id){
	var filter_code = $('#'+id).val();
	var filter_rate_id = $('#input-rate_id').val();
	
	s_id = id.split('_');
	id_new = s_id[1]; 

	var is_new = $('#is_new'+id_new).val();
	var amt_value = $('#amt_'+id_new).val()
	var filter_code = filter_code.replace(/[^A-Z0-9]+/i, '');
	if(filter_code != '' && filter_code != undefined && amt_value == ''){
		var me = $(this);
	    
	    if ( me.data('requestRunning') ) {
	        return;
	    }

	    me.data('requestRunning', true);

		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/orderqwerty/autocomplete5&token=<?php echo $token; ?>&filter_name=' +filter_code+'&filter_rate_id=' +filter_rate_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					//idss = $('.code').attr('id');
					$("#kot_print_status").val('1');

					s_id = id.split('_');
					$('#code_'+s_id[1]).val(data.item_code);
					$('#is_set'+s_id[1]).val(1);
					$('#name_'+s_id[1]).val(data.item_name);
					$('#subcategoryid_'+s_id[1]).val(data.subcategoryid);
					$('#qty_'+s_id[1]).val(1);
					$('#rate_'+s_id[1]).val(data.purchase_price);
					$('#pre_rate_'+s_id[1]).val(data.purchase_price);

					$('#is_liq_'+s_id[1]).val(data.is_liq);
					$('#amt_'+s_id[1]).val(data.purchase_price);
					$('#tax1_'+s_id[1]).val(data.taxvalue1);
					//$('#tax1_value_'+s_id[1]).val(data.taxamt1);
					$('#tax2_'+s_id[1]).val(data.taxvalue2);
					//$('#tax2_value_'+s_id[1]).val(data.taxamt2);
					$('#message_'+s_id[1]).val();
					$('#usermsg_'+s_id[1]).val($('#message_'+s_id[1]).val());
					$('.dropdown-menu').hide();
					$('#qty_'+s_id[1]).focus();
					$('#qty_'+s_id[1]).select();
					var html;
					$('#modifierbody').html('');
					$('#modifierheader').html('');
					if(data.modifier_group != '0'){
						$('#ismodifier_'+s_id[1]).val(1);
						$('#parent_'+s_id[1]).val(1);
						modifierheader = '<h4>'+data.item_name+'</h4>';
						$('#modifierheader').append(modifierheader);
						// alert(data.modifieritems[0].modifieritem);
						motr = 1;
						for(var i=0;i<data.totalmodifiers;i++){
							html = '<ul style="list-style-type:none">';
								html += '<li>';
								if(data.modifieritems[i].default_item == '1'){
									html +='<input type="checkbox" checked ="checked" value="'+id_new+'_'+motr+'" class="r_'+id_new+'" id="checkmodifier_'+id_new+'_'+motr+'">&nbsp;'+data.modifieritems[i].modifieritem+'</input>';
									html +='<h5 style="float:right;margin-right:100px;">'+data.modifieritems[i].modifierrate+'</span>';
								} else {
									html +='<input type="checkbox" value="'+id_new+'_'+motr+'" class="r_'+id_new+'" id="checkmodifier_'+id_new+'_'+motr+'">&nbsp;'+data.modifieritems[i].modifieritem+'</input>';
									html +='<h5 style="float:right;margin-right:100px;">'+data.modifieritems[i].modifierrate+'</span>';
								}
								html1 = '<tr id="re_'+id_new+'_'+motr+'" class="re_'+id_new+'" style="display:none" >';
									html1 += '<td id="r_'+id_new+'_'+motr+'" class="r_'+id_new+'" style="display:none; padding-top: 2px;padding-bottom: 2px;font-size: 16px;">'
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][kot_status]" class="re_'+id_new+'" id="kot_status'+id_new+'_'+motr+'"/>';
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][pre_qty]" class="re_'+id_new+'" id="pre_qty'+id_new+'_'+motr+'"/>';
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][kot_no]" class="re_'+id_new+'" id="kot_no'+id_new+'_'+motr+'"/>';
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][is_new]" class="re_'+id_new+'" id="is_new'+id_new+'_'+motr+'"/>';
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][is_set]" class="re_'+id_new+'" id="is_set'+id_new+'_'+motr+'"/>';
										html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][cancelstatus]" class="re_'+id_new+'" id="cancelstatus'+id_new+'_'+motr+'"/>';
									html1 + '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][code]" value="'+data.modifieritems[i].itemcode+'" class="re_'+id_new+'" id="code_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][subcategoryid]" value="'+data.modifieritems[i].subcategoryid+'" class="re_'+id_new+'" id="subcategoryid_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax1]" class="re_'+id_new+'" id="tax1_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax2]" class="re_'+id_new+'" id="tax2_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax1_value]" class="re_'+id_new+'" id="tax1_value_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax2_value]" class="re_'+id_new+'" id="tax2_value_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][discount_per]" class="re_'+id_new+'" id="discount_per_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][discount_value]" class="re_'+id_new+'" id="discount_value_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][billno]" class="re_'+id_new+'" id="billno_'+id_new+'_'+motr+'" />';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][parent_id]" class="re_'+id_new+'" id="parent_id_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][ismodifier]" value="0" class="re_'+id_new+'" id="ismodifier_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][nc_kot_status]" value="0" class="re_'+id_new+'" id="nckotstatus'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][nc_kot_reason]" value="" class="re_'+id_new+'" id="nckotreason'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][transfer_qty]" value="0" class="re_'+id_new+'" id="transferqty'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][referparent]" value="'+id_new+'" class="re_'+id_new+'" id="referparent_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][parent]" value="0" class="re_'+id_new+'" id="parent_'+id_new+'_'+motr+'"/>';
									html1 += '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][name]" value="'+data.modifieritems[i].modifieritem+'" class="re_'+id_new+'" id="name_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][id]" class="re_'+id_new+'" id="id_'+id_new+'_'+motr+'"/>';
									html1 += '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" class="re_'+id_new+'"style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][qty]" value="1" id="qty_'+id_new+'_'+motr+'" autocomplete="off"/>';
									html1 += '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][rate]" value="'+data.modifieritems[i].modifierrate+'" class="re_'+id_new+'" id="rate_'+id_new+'_'+motr+'"/>';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][pre_rate]" value="'+data.modifieritems[i].modifierrate+'" class="re_'+id_new+'" id="pre_rate_'+id_new+'_'+motr+'"/>';
										
									html1 += '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="hidden" value="'+data.modifieritems[i].modifierrate+'" class="re_'+id_new+'" name="po_datas['+id_new+'_'+motr+'][amt]" id="amt_'+id_new+'_'+motr+'"/>';
									html1 += '</td>';
									html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
										html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" class="re_'+id_new+'"  name="po_datas['+id_new+'_'+motr+'][message]" id="message_'+id_new+'_'+motr+'" /><input type="hidden" value="'+data.modifieritems[i].is_liq+'" name="po_datas['+id_new+'_'+motr+'][is_liq]"  class="re_'+id_new+'" id="is_liq_'+id_new+'_'+motr+'"/>';
									html1 += '</td>';
									if(data.modifieritems[i].default_item == '1'){
										html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
											html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][cancelmodifier]" class="re_'+id_new+'" id="cancelmodifier_'+id_new+'_'+motr+'"/>';
										html1 += '</td>';
									} else{
										html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
											html1 += '<input type="hidden" value="1" name="po_datas['+id_new+'_'+motr+'][cancelmodifier]" class="re_'+id_new+'" id="cancelmodifier_'+id_new+'_'+motr+'"/>';
										html1 += '</td>';
									}
							 	html1 += '</tr>';
								html +='</li>';
								motr ++;
								$('#ordertab').append(html1);
							html += '<ul>';
							$('#modifierbody').append(html);
						}
						$('#myModalModifier').modal({show:true});
						html2 = '<input type="hidden" class="re_'+id_new+'" id="modifierid_'+id_new+'" value="'+data.modifier_qty+'">';
						$('#ordertab').append(html2);
					}
					if(data.is_liq == '1' && data.is_liq != ''){

						liqvalue = parseInt($('#liqcount').val());
  						liqcount = liqvalue + 1 ;
  						$('#liqcount').val(liqcount);

						tax1 = parseFloat(data.taxvalue1);
						tax2 = parseFloat(data.taxvalue2);
						amt  = parseFloat(data.purchase_price);
						
						tax1_value = amt*(tax1/100);
		  				tax2_value = amt*(tax2/100);

		  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
		  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
					}
					if(data.is_liq == '0' && data.is_liq != ''){

						foodvalue = parseInt($('#foodcount').val());
  						foodcount = foodvalue + 1 ;
  						$('#foodcount').val(foodcount);

						tax1 = parseFloat(data.taxvalue1);
						tax2 = parseFloat(data.taxvalue2);
						amt  = parseFloat(data.purchase_price);
						
						tax1_value = amt*(tax1/100);
		  				tax2_value = amt*(tax2/100);

		  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
		  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

					}
					if(s_id[1] != '0'){
						$('#code_'+s_id[1]).attr('readonly', 'readonly');
						$('#name_'+s_id[1]).attr('readonly', 'readonly');
					}
					gettotal(s_id[1],data.is_liq);
					return false;
				} else {
					$('#'+id).val('');
					$('#'+id).focus();
					alert('Item Code Does Not Exist');
					return false;
				}
			},
			complete: function() {
            	me.data('requestRunning', false);
        	}
		});
	} else {
		$('#qty_'+id_new).focus();
		return false;
	}
}

function tablein(){
	var person = '<?php echo $PERSONS ?>';
	var waiter = '<?php echo $WAITER ?>';
	var captain ='<?php echo $CAPTAIN ?>';
	var edit = '<?php echo $editorder ?>';
	var editorderid = '<?php echo $editorderid ?>';

    var location_id = $('input[name=\'location_id\']').val();
    var table_id = $('input[name=\'table_id\']').val();

    tab_index = 1;
	if($('#input-t_number').length > 0){
		tab_index ++;
	}
	if($('#input-waiter').length > 0){
		tab_index ++;
	}
	if($('#input-waiterid').length > 0){
		tab_index ++;
	}
	if($('#input-captainid').length > 0){
		tab_index ++;
	}
	if($('#input-captain').length > 0){
		tab_index ++;
	}
	if($('#input-person').length > 0){
		tab_index ++;
	}

    $.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/tableinfo&token=<?php echo $token; ?>&editorderid='+editorderid+'&lid='+location_id+'&tid='+table_id+'&edit='+ edit +'&tab_index='+tab_index,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			var html =data.html;
			//alert(html)
			ur = '';
			if(html != '') {
		  		$('.ordertab').html('');
		  		$('.ordertab').append(data.html);
			  	if($('#orderno').val() != '0' && $('#orderno').val() != undefined){
					if(edit == '1'){
						$('#input-waiterid').prop('readonly', false);
						$('#input-waiter').prop('readonly', false);
						$('#input-captainid').prop('readonly', false);
						$('#input-captain').prop('readonly', false);
						$('#input-person').prop('readonly', false);
					} else{
						$('#input-waiterid').prop('readonly', true);
						$('#input-waiter').prop('readonly', true);
						$('#input-captainid').prop('readonly', true);
						$('#input-captain').prop('readonly', true);
						$('#input-person').prop('readonly', true);
					}
				} else {
					$('#input-waiterid').prop('readonly', false);
					$('#input-waiter').prop('readonly', false);
					$('#input-captainid').prop('readonly', false);
					$('#input-captain').prop('readonly', false);
					$('#input-person').prop('readonly', false);
				}
		  		//alert(data.waiter);
		  		$('#input-waiter').val(data.waiter);
		  		$('#input-waiterid').val(data.waiter_code);
		  		$('#input-ids').val(data.i);
		  		$('#input-captainid').val(data.captain_code);
		   		$('#input-captain').val(data.captain);
				$('#input-waiter_id').val(data.waiter_id);
		   		$('#input-captain_id').val(data.captain_id);
				$('#input-person').val(data.person);
				$('#input-ftotal').val(data.ftotal);
				$('#input-ftotal_discount').val(data.ftotal_discount);
				$('#input-gst').val(data.gst);
				$('#input-ltotal').val(data.ltotal);
				$('#input-ltotal_discount').val(data.ltotal_discount);
				$('#input-gst').val(data.gst);
				$('#input-vat').val(data.vat);
				$('#input-cess').val(data.cess);
				$('#staxfood').val(data.staxfood);
				$('#staxliq').val(data.staxliq);
				$('#input-stax').val(data.stax);
				$('#foodcount').val(data.foodcount);
				$('#liqcount').val(data.liqcount);

				$('#input-dtotalvalue').val(data.dtotalvalue);
				$('#input-dchargeper').val(data.dchargeper);
				$('#input-dcharge').val(data.dcharge);
				$("#dchargeval").html(data.dchargeval);

				$('#input-dtotalvalue').val(data.dtotalvalue);
				$('#input-dchargeper').val(data.dchargeper);
				$('#input-dcharge').val(data.dcharge);
				$("#dchargeval").html(data.dchargeval);

				$('#input-ftotalvalue').val(data.ftotalvalue);
				$('#input-fdiscountper').val(data.fdiscountper);
				$("#fdisval").html(data.ftotalvalue);
				$('#input-fdiscount').val(data.discount);
				$('#input-ldiscount').val(data.ldiscount);
				$('#input-ldiscountper').val(data.ldiscountper);
				$("#ldisval").html(data.ltotalvalue);
				$('#input-ltotalvalue').val(data.ltotalvalue);
				$('#input-grand_total').val(data.grand_total);
				$('.grand_total_span').html(data.grand_total);
				$('#advance_id').val(data.advance_billno);
				if(data.bill_status == 1){
					$('#advance_id').prop('readonly',true);
				}
				$('#advance_amount').val(data.advance_amount);
				$('#input-roundtotal').val(data.roundtotal);
				$('#input-total_items').val(data.total_items);
				$('.total_items_span').html(data.total_items);
				$('#input-item_quantity').val(data.item_quantity);
				$('.item_quantity_span').html(data.item_quantity);
				$('#input-date_added').val(data.date_added);
				$('#input-time_added').val(data.time_added);
				$('#input-kot_no').val(data.kot_no);
				$('#input-cust_name').val(data.cust_name);
				$('#input-cust_id').val(data.cust_id);
				$('#input-cust_contact').val(data.cust_contact);
				$('#input-cust_email').val(data.cust_email);
				$('#input-cust_address').val(data.cust_address);
				$('#code_'+data.i).focus();
				$('.custcheck').html('');
				if(data.cust_name != ''){
					html2 ='<h4 style = "margin-top:10%" >'+data.cust_name+'</h4>';
				} else {
			 		html2 = '<a  data-toggle="tooltip" onclick="newreg()" title="new" class="btn btn-primary newreg" style="margin-top: 10%;display:none;">Register</a>'
				}
				$('.custcheck').append(html2);
				$('#link').attr('href','index.php?route=catalog/orderqwerty/printsb&token=<?php echo $token; ?>&order_id='+data.order);
				ur = 'index.php?route=catalog/orderqwerty/printsb&token=<?php echo $token; ?>&order_id='+data.order;
				tab_index = data.tab_index;
			} else {
				tab_index = 1;
				if($('#input-t_number').length > 0){
					tab_index ++;
				}
				if($('#input-waiter').length > 0){
					tab_index ++;
				}
				if($('#input-waiterid').length > 0){
					tab_index ++;
				}
				if($('#input-captainid').length > 0){
					tab_index ++;
				}
				if($('#input-captain').length > 0){
					tab_index ++;
				}
				if($('#input-person').length > 0){
					tab_index ++;
				}
				html1 = '<tr>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 15%;font-weight: bold;font-size: 20px;">Code</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 30%;font-weight: bold;font-size: 20px;">Name</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 7%;font-weight: bold;font-size: 20px;">Qty</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 13%;font-weight: bold;font-size: 20px;">Rate</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 13%;font-weight: bold;font-size: 20px;">Amt</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;width: 10%;font-weight: bold;font-size: 20px;">KOT</td>';
					html1 += '<td style="padding-top:2px; padding-bottom: 2px;font-weight: bold;font-size: 20px;">R</td>';
				html1 += '</tr>';
			 	html1 += '<tr id="re_1">';
					html1 += '<td style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;" class="r_1" >1';
						html1 += '<input type="hidden" value="0" name="po_datas[1][kot_status]" id="kot_status1" />';
						html1 += '<input type="hidden" value="0" name="po_datas[1][pre_qty]" id="pre_qty1" />';
						html1 += '<input type="hidden" value="0" name="po_datas[1][kot_no]" id="kot_no1" />';
						html1 += '<input type="hidden" value="0" name="po_datas[1][is_new]" id="is_new1" />';
						html1 += '<input type="hidden" value="0" name="po_datas[1][is_set]" id="is_set1" />';
						html1 += '<input type="hidden" value="0" name="po_datas[1][cancelstatus]" id="cancelstatus1" />';
					html1 += '</td>';
					html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;width:15%;">';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas[1][code]" id="code_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas[1][subcategoryid]" id="subcategoryid_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas[1][tax1]" id="tax1_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas[1][tax2]" id="tax2_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas[1][tax1_value]" id="tax1_value_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas[1][tax2_value]" id="tax2_value_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas[1][discount_per]" id="discount_per_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas[1][discount_value]" id="discount_value_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas[1][billno]" id="billno_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" name="po_datas[1][ismodifier]" value="1" id="ismodifier_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" name="po_datas[1][nc_kot_status]" value="0" id="nc_kot_status_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" name="po_datas[1][nc_kot_reason]" value="" id="nc_kot_reason_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" name="po_datas[1][transfer_qty]" value="0" id="transfer_qty_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas[1][parent_id]" value="0" id="parent_id_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas[1][parent]" value="0" id="parent_1" />';
						tab_index ++;
					html += '</td>';
					html1 += '<td style="padding-top: 2px;padding-bottom: 2px;" class="r_1">';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[1][name]" id="name_1" />';
						html1 += '<input style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas[1][id]" id="id_1" />';
						tab_index ++;
					html1 += '</td>';
					html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width:8%;" class="r_1">';
						<?php if($display_type == '1') { ?>
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs qty form-control" name="po_datas[1][qty]" id="qty_1" autocomplete="off"/></td>';
						<?php }  else { ?>
							html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty(1)" class="inputs qty form-control screenquantity" name="po_datas[1][qty]" id="qty_1" /></td>';	
						<?php } ?>
						tab_index ++;
					html1 += '</td>';
					html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width:13%;" class="r_1">';
					<?php if($display_type == '1') { ?>
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[1][rate]" id="rate_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="hidden" class="inputs pre_rate form-control" name="po_datas[1][pre_rate]" id="pre_rate_1" />';
						
					<?php }  else { ?>
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate(1)"  class="inputs rate form-control screenrate" name="po_datas[1][rate]" id="rate_1" />';
						html1 += '<input tabindex = "'+tab_index+'" type="hidden" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate(1)"  class="inputs pre_rate form-control screenrate" name="po_datas[1][pre_rate]" id="pre_rate_1" />';
						
					<?php } ?>
						tab_index ++;
					html1 += '</td>';
					html1 += '<td style="padding-top: 2px;padding-bottom: 2px;width: 13%" class="r_1">';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[1][amt]" id="amt_1" />';
					html1 += '</td>';		
					html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;width: 25%">';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[1][message]" id="message_1" autocomplete="off"/><input style="display:none" type="text" name="po_datas[1][is_liq]" value = "" id="is_liq_1" />';
						html1 += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" value="0" name="po_datas[1][cancelmodifier]" id="cancelmodifier_1" />';
						tab_index ++;
					html1 += '</td>';
						html1 += '<input style="padding: 0px 1px;" type="hidden" class="inputs usermsg form-control" name="po_datas[1][usermsg]" id="usermsg_1" />';
						html1 += '<input type="hidden" name="liq_tax_per" value="0"  id="liq_tax_per" />';
						html1 += '<input type="hidden" name="liq_tax_per2" value="0"  id="liq_tax_per2" />';
						html1 += '<input type="hidden" name="food_tax_per" value="0"  id="food_tax_per" />';
						html1 += '<input type="hidden" name="food_tax_per2" value="0"  id="food_tax_per2" />';
						// html1 += '<input type="hidden" name="foodcount" value="0"  id="foodcount" />';
						// html1 += '<input type="hidden" name="liqcount" value="0"  id="liqcount" />';
					html1 += '<td class="r_1" style="padding-top: 2px;padding-bottom: 2px;text-align: left;">';
						html1 += '<a tabindex = "'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder(1)" class="button inputs remove " id="remove_1" ><i class="fa fa-trash-o"></i></a>';
						tab_index ++;
					html1 += '</td>';
		  		html1 += '</tr>';
		  		html1 += '<input type="hidden" name="bill_status" value="0"  id="bill_status" />';
		  		html1 += '<input type="hidden" name="order_no" value="0"  id="orderno" />';
		  		html1 += '<input type="hidden" name="orderid" value="0"  id="orderid" />';
		  		html1 += '<input type="hidden" name="apporderid" value="0"  id="apporderid" />';
		  		html1 += '<input type="hidden" name="staxfood" value="0"  id="staxfood" />';
				html1 += '<input type="hidden" name="staxliq" value="0"  id="staxliq" />';
		  		$('.ordertab').html('');
		  		$('.ordertab').append(html1);
 				$('#input-waiter').val(data.waiter);
 				$('#input-waiterid').val(data.waiter_code);
 				$('#input-captainid').val(data.captain_code);
		   		$('#input-captain').val(data.captain);
				$('#input-waiter_id').val('');
		   		$('#input-captain_id').val('');
				$('#input-person').val(0);
				$('#input-ftotal').val(0);
				$('#input-ftotal_discount').val(0);
				$('#input-gst').val(0);
				$('#input-ltotal').val(0);
				$('#input-ltotal_discount').val(0);
				$('#input-vat').val(0);	
				$('#input-cess').val('');
				$('#staxfood').val('');
				$('#staxliq').val('');
				$('#input-stax').val('');

				$('#input-dtotalvalue').val('');
				$('#input-dchargeper').val('');
				$('#input-dcharge').val('');
				$("#dchargeval").html('');

				$('#input-ftotalvalue').val('');
				$('#input-fdiscountper').val('');
				$("#fdisval").html('');
				$('#input-fdiscount').val('');
				$('#input-ldiscount').val('');
				$('#input-ldiscountper').val('');
				$("#ldisval").html('');
				$('#input-ltotalvalue').val('');
				$('#input-kot_no').val(0);
			 	$('#input-cust_name').val('');
				$('#input-cust_id').val('');
				$('#input-cust_contact').val('');
				$('#input-cust_email').val('');
				$('#input-cust_address').val('');
				if($('#parcel_status').val() == '1' && $('#input-kot_no').val() == '0'){
					htmlc = '<iframe id="parceldetail" style="border: 0px;" src="index.php?route=catalog/customer/add&token=<?php echo $token; ?>&iframe=1" width="100%" height="80%"></iframe>';
					dialog_parceldetail.dialog("open");
					$('#dialog-form_parceldetail').html(htmlc)
				}
				if(waiter != 0){
					$('#input-waiterid').focus();
				} else if(captain != 0){
					$('#input-captainid').focus();
				} else if(person !=0){
					$('#input-person').focus();
				} else{
					$('.code').focus();
				}
				$('.custcheck').html('');
			  	$('#link').attr('href','index.php?route=catalog/orderqwerty/add&token=<?php echo $token; ?>');
			}

			ftotal = parseFloat($('#input-ftotal').val());
			ftotal_discount = parseFloat($('#ftotal_discount').val());
			ftax_per = parseFloat($('#food_tax_per').val());
			ftax_per2 = parseFloat($('#food_tax_per2').val());
			ltotal = parseFloat($('#input-ltotal').val());
			ltotal_discount = parseFloat($('#ltotal_discount').val());
			ltax_per = parseFloat($('#liq_tax_per').val());
			ltax_per2 = parseFloat($('#liq_tax_per2').val());
			gst = parseFloat($('#input-gst').val());
			vat = parseFloat($('#input-vat').val());
			cess = parseFloat($('#input-cess').val());
			staxfood = parseFloat($('#staxfood').val());
			staxliq = parseFloat($('#staxliq').val());
			stax = parseFloat($('#input-stax').val());
			ftotalvalue = parseFloat($('#input-ftotalvalue').val());
			fdiscountper = parseFloat($('#input-fdiscountper').val());
			fdiscount = parseFloat($('#input-fdiscount').val());
			ldiscount = parseFloat($('#input-ldiscount').val());
			ldiscountper = parseFloat($('#input-ldiscountper').val());
			ltotalvalue = parseFloat($('#input-ltotalvalue').val());
			dcharge = parseFloat($('#input-dcharge').val());
			dchargeper = parseFloat($('#input-dchargeper').val());
			dtotalvalue = parseFloat($('#input-dtotalvalue').val());
			gtotal = parseFloat($('#input-grand_total').val());

			if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
				ftotal = 0;
			}

			if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
				ftotal_discount = 0;
			}

			if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
				ftax_per = 0;
			}

			if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
				ftax_per2 = 0;
			}

			if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
				ltotal = 0;
			}

			if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
				ltotal_discount = 0;
			}

			if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
				ltax_per = 0;
			}

			if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
				ltax_per2 = 0;
			}

			if(gst == '' || gst == '0' || isNaN(gst)){
				gst = 0;
			}

			if(vat == '' || vat == '0' || isNaN(vat)){
				vat = 0;
			}

			if(cess == '' || cess == '0' || isNaN(cess)){
				cess = 0;
			}

			if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
				staxfood = 0;
			}

			if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
				staxliq = 0;
			}

			if(stax == '' || stax == '0' || isNaN(stax)){
				stax = 0;
			}

			if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
				ftotalvalue = 0;
			}

			if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
				fdiscount = 0;
			}

			if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
				fdiscountper = 0;
			}

			if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
				ldiscount = 0;
			}

			if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
				ldiscountper = 0;
			}

			if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
				ltotalvalue = 0;
			}

			if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
				dcharge = 0;
			}

			if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
				dchargeper = 0;
			}

			if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
				dtotalvalue = 0;
			}

			if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
				gtotal = 0;
			}

			if(fdiscountper > 99 || fdiscountper < 0){
				alert("Food Discount cannot be given");
				$('#input-fdiscountper').val(0);
				$('#fdisval').html(0);
				$('#input-fdiscount').val(0);
				$('.qty').trigger("change");
				return false;
			}

			if(ldiscountper > 99 || ldiscountper < 0){
				alert("Liquor Discount cannot be given");
				$('#input-ldiscountper').val(0);
				$('#input-ldiscount').val(0);
				$('#ldisval').html(0);
				$('.qty').trigger("change");
				return false;
			}

			if(ftotal > 0 && fdiscount > 0){
				if(fdiscount >= ftotal || fdiscount < 0){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				} else {
					if(ftotal > 0 && fdiscount > 0){
						fdifference = ftotal - fdiscount;
						if(fdifference < 1){
							alert("Food Discount cannot be given");
							$('#input-fdiscountper').val(0);
							$('#fdisval').html(0);
							$('#input-fdiscount').val(0);
							$('.qty').trigger("change");
							return false;
						}
					}	
				}
			}

			if(ltotal > 0 && ldiscount > 0){
				if(ldiscount >= ltotal || ldiscount < 0){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				} else {
					if(ltotal > 0 && ldiscount > 0){
						ldifference = ltotal - ldiscount;
						if(ldifference < 1){
							alert("Liquor Discount cannot be given");
							$('#input-ldiscountper').val(0);
							$('#input-ldiscount').val(0);
							$('#ldisval').html(0);
							$('.qty').trigger("change");
							return false;
						}
					}
				}
			}

			ftotaldiscountper = 0;
			ltotaldiscountper = 0;
			ftotal_discount = 0;
			ltotal_discount = 0;
			if(ftotal != '0'){
				$('#ftotal_discount').val(ftotal);
				ftotal_discount = ftotal;
				if(fdiscount != '0' && fdiscount != ''){
					$('#input-fdiscountper').val(0);
					fdiscountper = 0;
					ftotal_discount = ftotal - fdiscount;
					$('#ftotal_discount').val(ftotal_discount);
				} else {
					$('#input-fdiscount').val(0);
					fdiscount = 0;
				}
				if(fdiscountper != '0' && fdiscountper != ''){
					ftotaldiscountper = ((fdiscountper/100)*ftotal);
					$('#input-ftotalvalue').val(ftotaldiscountper);
					$("#fdisval").html(ftotaldiscountper);
					ftotal_discount = ftotal - ftotaldiscountper;
					$('#ftotal_discount').val(ftotal_discount);
				} else {
					$('#input-ftotalvalue').val(0);
					$("#fdisval").html('');
				}
			} else {
				fdiscountper = 0;
				fdiscountper = 0;
			}

			if(ltotal != '0'){
				$('ltotal_discount').val(ltotal);
				ltotal_discount = ltotal;
				if(ldiscount != '0' && ldiscount != ''){
					$('#input-ldiscountper').val(0);
					ldiscountper = 0;
					ltotal_discount = ltotal - ldiscount;
					$('#ltotal_discount').val(ltotal_discount);
				} else {
					$('#input-ldiscount').val(0);
					ldiscount = 0;
				}
				if(ldiscountper != '0' && ldiscountper != ''){
					ltotaldiscountper = ((ldiscountper/100)*ltotal);
					$('#input-ltotalvalue').val(ltotaldiscountper);
					$("#ldisval").html(ltotaldiscountper);
					ltotal_discount = ltotal - ltotaldiscountper;
					$('#ltotal_discount').val(ltotal_discount);
				} else {
					$('#input-ltotalvalue').val(0);
					$("#ldisval").html('');
				}
			} else {
				ldiscountper = 0;
				ldiscount = 0;
			}

			var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
			var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
			var inclusive = '<?php echo $INCLUSIVE ?>';
			disamountfood = 0;
			disamountliq = 0;
			totaldiscountamount = 0;
			gsttaxamt = 0;
			vattaxamt = 0;
			finaldisfood = 0;
			finaldisliq = 0;
			countfood = parseInt($('#foodcount').val());
			countliq = parseInt($('#liqcount').val());
			staxfood = 0;
			staxliq = 0;
			stax = 0;
			$('.qty:visible').each(function( index ) {
				idss = $(this).attr('id');
				s_id = idss.split('_');
				is_liq1 = $('#is_liq_'+s_id[1] ).val();

				if(is_liq1 == 0 && is_liq1 != ''){
					tax1 = parseFloat($('#tax1_'+s_id[1]).val());
					tax2 = parseFloat($('#tax2_'+s_id[1]).val());
					rate = parseFloat($('#rate_'+s_id[1] ).val());
		  			qty = parseFloat($('#qty_'+s_id[1] ).val());
		  			amt = parseFloat($('#amt_'+s_id[1] ).val());
		  			//amt = rate * qty;

		  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
						tax1 = 0;
					}

					if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
						tax2 = 0;
					}

					if(rate == '' || rate == '0' || isNaN(rate)){
						rate = 0;
					}

					if(qty == '' || qty == '0' || isNaN(qty)){
						qty = 0;
					}

					if(amt == '' || amt == '0' || isNaN(amt)){
						amt = 0;
					}

					if(fdiscountper > 0 || fdiscountper != ''){
						//totalfood = fdiscountper;
						//discount_value = amt*(totalfood/100);
						discount_value = amt*(fdiscountper/100);
						$('#discount_per_'+s_id[1]).val(fdiscountper);
						$('#discount_value_'+s_id[1]).val(discount_value);

						afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
						afterdiscountamt = amt - afterdiscount;
						
						service_charge = $('#input-service_charge').val();
						if (service_charge == 1) {
							staxfoods = afterdiscountamt*(servicechargefood/100);
						}else{
							staxfoods = 0;
						}
						
						tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
			  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

			  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
			  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

			  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

						disamountfood = disamountfood + afterdiscountamt;
						staxfood = staxfood + staxfoods;
						stax = stax + staxfoods;
						gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
						finaldisfood = finaldisfood + afterdiscount;

					} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
						discount_per = (fdiscount/ftotal)*100;
						//discount_value = totalfood/countfood;
						$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

						discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
						afterdiscount = amt*(discountvalueper/100);
						$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
						afterdiscountamt = amt - afterdiscount;

						service_charge = $('#input-service_charge').val();
						if (service_charge == 1) {
							staxfoods = afterdiscountamt*(servicechargefood/100);
						} else {
							staxfoods = 0;
						}
						
						tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
			  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

						$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
			  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

			  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

						disamountfood = disamountfood + afterdiscountamt;
						stax = stax + staxfoods;
						staxfood = staxfood + staxfoods;
						gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
						finaldisfood = finaldisfood + afterdiscount;
					} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
						$('#discount_per_'+s_id[1]).val(0);
						$('#discount_value_'+s_id[1]).val(0);

						service_charge = $('#input-service_charge').val();
						if (service_charge == 1) {
							staxfoods = amt * (servicechargefood/100);
						} else {
							staxfoods = 0;
						}

						tax1_value = (amt + staxfoods) * (tax1/100);
			  			tax2_value = (amt + staxfoods) * (tax2/100);

						$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
		  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

		  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
						$('#discount_value_'+s_id[1]).val(0);

						stax = stax + staxfoods;
						staxfood = staxfood + staxfoods;
						gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
					}
				}

				if(is_liq1 == 1 && is_liq1 != ''){
					tax1 = parseFloat($('#tax1_'+s_id[1]).val());
					tax2 = parseFloat($('#tax2_'+s_id[1]).val());
					rate = parseFloat($('#rate_'+s_id[1] ).val());
		  			qty = parseFloat($('#qty_'+s_id[1] ).val());
		  			amt = parseFloat($('#amt_'+s_id[1] ).val());
		  			//amt = rate * qty;

		  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
						tax1 = 0;
					}

					if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
						tax2 = 0;
					}

					if(rate == '' || rate == '0' || isNaN(rate)){
						rate = 0;
					}

					if(qty == '' || qty == '0' || isNaN(qty)){
						qty = 0;
					}

					if(amt == '' || amt == '0' || isNaN(amt)){
						amt = 0;
					}

					if(ldiscountper > 0 || ldiscountper != ''){

						discount_value = amt*(ldiscountper/100);
						$('#discount_per_'+s_id[1]).val(ldiscountper);
						$('#discount_value_'+s_id[1]).val(discount_value);

						afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
						afterdiscountamt = amt - afterdiscount;

						service_charge = $('#input-service_charge').val();
						if (service_charge == 1 ) {
							staxliqs = afterdiscountamt * (servicechargeliq / 100);
						} else {
							staxliqs = 0;
						}

						tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
			  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

			  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
			  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

			  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
						
						disamountliq = disamountliq + afterdiscountamt;
						stax = stax + staxliqs;
						staxliq = staxliq + staxliqs;
						vattaxamt = vattaxamt + vattax1 + vattax2;
						finaldisliq = finaldisliq + afterdiscount;

					} else if(ldiscount > 0 || ldiscount != ''){
						discount_per = (ldiscount/ltotal)*100;
						//discount_value = totalliq/countliq;
						$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

						discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
						afterdiscount = amt*(discountvalueper/100);
						$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
						afterdiscountamt = amt - afterdiscount;
						service_charge = $('#input-service_charge').val();
						if (service_charge == 1 ) {
							staxliqs = afterdiscountamt * (servicechargeliq / 100);
						} else {
							staxliqs = 0;
						}

						tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
			  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

			  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
			  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

			  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

						disamountliq = disamountliq + afterdiscountamt;
						stax = stax + staxliqs;
						staxliq = staxliq + staxliqs;
						vattaxamt = vattaxamt + vattax1 + vattax2;
						finaldisliq = finaldisliq + afterdiscount;
					} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
						$('#discount_per_'+s_id[1]).val(0);
						$('#discount_value_'+s_id[1]).val(0);

						service_charge = $('#input-service_charge').val();
						if (service_charge == 1 ) {
							staxliqs = amt * (servicechargeliq / 100);
						} else {
							staxliqs = 0;
						}

						tax1_value = (amt + staxliqs) * (tax1 / 100);
		  				tax2_value = (amt + staxliqs) * (tax2 / 100);

		  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
		  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
		  				$('#discount_value_'+s_id[1]).val(0);

		  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
						vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

						stax = stax + staxliqs;
						staxliq = staxliq + staxliqs;
						vattaxamt = vattaxamt + vattax1 + vattax2;
					}
				}
			});

			$('#input-gst').val(gsttaxamt.toFixed(2));
			$('#input-vat').val(vattaxamt.toFixed(2));
			$('#fdisval').html(finaldisfood.toFixed(2));
			$('#ldisval').html(finaldisliq.toFixed(2));
			if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
				$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
			} else{
				$('#input-fdiscount').val(finaldisfood.toFixed(2));
			}

			if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
				$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
			} else{
				$('#input-ldiscount').val(finaldisliq.toFixed(2));
			}

			if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
				staxamount = stax;
				$('#staxfood').val(staxfood.toFixed(2));
				$('#staxliq').val(staxliq.toFixed(2));
				$('#input-stax').val(staxamount.toFixed(2));
				$('#ftotal_discount').val(disamountfood.toFixed(2));
				$('#ltotal_discount').val(disamountliq.toFixed(2));
			} else {
				staxamount = stax;
				$('#staxfood').val(staxfood.toFixed(2));
				$('#staxliq').val(staxliq.toFixed(2));
				$('#input-stax').val(staxamount.toFixed(2));
				$('#ftotal_discount').val(disamountfood.toFixed(2));
				$('#ltotal_discount').val(disamountliq.toFixed(2));
			}

			gst = parseFloat($('#input-gst').val());
			vat = parseFloat($('#input-vat').val());
			stax = parseFloat($('#input-stax').val());
			if(inclusive == '1'){
				grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
			} else{
				grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
			}
			
			if(!isNaN(grand_total) && grand_total != ''){
				grand_total = grand_total.toFixed(2);
			}

			if(grand_total > 0){
				var roundtotals = grand_total.split('.');
				if(roundtotals[1] != undefined){
					roundtotal = roundtotals[1];
					if(roundtotal != '00'){
						$('#input-roundtotal').val((100 - roundtotal)/100);
					}
				}
			}

			advanceAmount = parseFloat($('#advance_amount').val());
			$('#input-grand_total').val(grand_total - advanceAmount);
			$('.grand_total_span').html(grand_total - advanceAmount);
			$('#oldgrand').val(grand_total);

			if(dcharge != 0){
				$('#input-dtotalvalue').val(dcharge);
				$('#dchargeval').html(dcharge);
			} else{
				$('#input-dtotalvalue').val(gtotal*(dchargeper/100));
				$('#dchargeval').html(gtotal*(dchargeper/100));
			}

			total_quantity = gettotal_quantity();
		  	total_items = gettotal_items();
		  	$('#input-total_items').val(total_items);
			$('.total_items_span').html(total_items);
			$('#input-item_quantity').val(total_quantity);
			$('.item_quantity_span').html(total_quantity);
		}
  	});
}

function bill() {
	orderid = $('#orderid').val();
	bill_status = $('#bill_status').val();
	advance_id = $('#advance_id').val();
	advance_amount = $('#advance_amount').val();
	grand_total = $('#input-grand_total').val();
	
	if(orderid != '' && orderid != '0' && orderid != undefined && bill_status == 0){
		id_length = $('#code_1').length;
		if(id_length > 0){
			code_value = $('#code_1').val();
			if(code_value != ''){
				var count = 0;
				$('.qty').each(function( index ) {
					if($(this).val() != ''){
						count ++;
					}
				});
				count ++;
				
				$('#name_'+count).removeClass("names");
				// url = 'index.php?route=catalog/orderqwerty/printsb&token=<?php echo $token; ?>&order_id='+orderid+'&advance_id='+advance_id+'&advance_amount='+advance_amount+'&grand_total='+grand_total;
				// // var win = window.open(ur, '_blank');
				// // win.focus();
				// location = url;
				//setTimeout(close_fun_1, 50);
				$(document).ajaxSend(function() {
					$("#overlay").fadeIn(300);　
				});
				$.ajax({
					type: "POST",
				  	url: 'index.php?route=catalog/orderqwerty/printsb&token=<?php echo $token; ?>&order_id='+orderid+'&advance_id='+advance_id+'&advance_amount='+advance_amount+'&grand_total='+grand_total,
				  	dataType: 'json',
				  	success: function(json) {
				  		//alert(json.status);
		  				tendering_status = '<?php echo $TENDERING_ENABLE; ?>';
						if(tendering_status == 1){
							htmlc = '<iframe id="tendering" style="border: 0px;" src="index.php?route=catalog/tendering&token=<?php echo $token; ?>&order_id='+orderid+'&grand_total='+grand_total+'" width="100%" height="100%"></iframe>';
							dialog_tendering.dialog("open");
							$('#dialog-form_tendering').html(htmlc)
							//location = url;
						} else {
							window.location.reload();
						}
					
			  		}
				}).done(function() {
					setTimeout(function(){
						$("#overlay").fadeOut(300);
					},500);
				});
			} else {
				alert('Please Select Table Data');		
			}
		} else {
			alert('Please Select Table Data');
		}
	} else {
		alert('Please Select Table Data / Already Billed');
		return false;
	}
}

function close_fun_1(){
	window.location.reload();
}

//--></script>
<script type="text/javascript"><!--
$('input[name=\'waiter\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/orderqwerty/autocomplete3&token=<?php echo $token; ?>&filter_wname=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
				  	return {
						label: item.name,
						value: item.waiter_id,
						code: item.code

				  	}
				}));
		  	}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'waiter\']').val(ui.item.label);
		$('input[name=\'waiter_id\']').val(ui.item.value);
		$('input[name=\'waiterid\']').val(ui.item.code);
		$('.dropdown-menu').hide();
		$('#input-captainid').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});

$('input[name=\'waiterid\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/orderqwerty/autocompletewaiterid&token=<?php echo $token; ?>&filter_wcode=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
				  	return {
						label: item.code,
						value: item.waiter_id,
						name: item.name
				  	}
				}));
		  	}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'waiterid\']').val(ui.item.label);
		$('input[name=\'waiter_id\']').val(ui.item.value);
		$('input[name=\'waiter\']').val(ui.item.name);
		$('.dropdown-menu').hide();
		$('#input-captainid').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});


$('input[name=\'cust_contact\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/orderqwerty/autocompletecustomer&token=<?php echo $token; ?>&filter_contact=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {
		  		if(json.length){
					response($.map(json, function(item) {
				  		return {
							label: item.cust_contact,
							value: item.cust_name,
							id: item.cust_id,
							address:item.cust_address,
							email:item.cust_email
						}
					}));
				} else {
					$('.custcheck').html('');
					$('.custcheck').append('<a data-toggle="tooltip" onclick="newreg()" title="new" class="btn btn-primary newreg" style="margin-top: 10%">Register</a>');  
				}
		  	}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'cust_name\']').val(ui.item.value);
		$('input[name=\'cust_contact\']').val(ui.item.label);
	 	$('input[name=\'cust_id\']').val(ui.item.id);
	  	$('input[name=\'cust_address\']').val(ui.item.address);
	  	$('input[name=\'cust_email\']').val(ui.item.email);
		$('.dropdown-menu').hide();
		$('.check_class').hide();  
		$('.custcheck').html('');  
		htmlname = '<h4 style = "margin-top:15%" >'+ui.item.value+'</h4>';
		$('.custcheck').append(htmlname);  
		$('#code_1').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});

$('input[name=\'cust_id\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
	  		url: 'index.php?route=catalog/orderqwerty/autocompletecustomer1&token=<?php echo $token; ?>&filter_id=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
		  			return {
						label: item.cust_id,
						value: item.cust_name,
						contact: item.cust_contact,
						address:item.cust_address,
						email:item.cust_email
					}
				}));
	  		}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'cust_name\']').val(ui.item.value);
		$('input[name=\'cust_contact\']').val(ui.item.contact);
	 	$('input[name=\'cust_id\']').val(ui.item.label);
	  	$('input[name=\'cust_address\']').val(ui.item.address);
	  	$('input[name=\'cust_email\']').val(ui.item.email);
		$('.dropdown-menu').hide();  
		$('.custcheck').html('');  
		htmlname = '<h4 style = "margin-top:15%" >'+ui.item.value+'</h4>';
		$('.custcheck').append(htmlname);  
		$('#code_1').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
 	}
});

window.onload = function() {
  var orderidmodify = '<?php echo $orderidmodify ?>';
  if(orderidmodify == ''){
  	$('#input-t_number').focus();
  }
}

//--></script>
<script type="text/javascript"><!--
$('input[name=\'captain\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
	  		url: 'index.php?route=catalog/orderqwerty/autocomplete4&token=<?php echo $token; ?>&filter_cname=' +  encodeURIComponent(request.term),
	  		dataType: 'json',
	  		success: function(json) {   
				response($.map(json, function(item) {
		  			return {
						label: item.name,
						value: item.waiter_id,
						code: item.code
		  			}
				}));
	  		}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'captain\']').val(ui.item.label);
		$('input[name=\'captain_id\']').val(ui.item.value);
		$('input[name=\'captainid\']').val(ui.item.code);
		$('.dropdown-menu').hide();    
		$('#input-person').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});

$('input[name=\'captainid\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
	  		url: 'index.php?route=catalog/orderqwerty/autocompletecaptainid&token=<?php echo $token; ?>&filter_ccode=' +  encodeURIComponent(request.term),
	  		dataType: 'json',
	  		success: function(json) {   
				response($.map(json, function(item) {
		  			return {
						label: item.code,
						value: item.waiter_id,
						name: item.name
		  			}
				}));
	  		}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'captainid\']').val(ui.item.label);
		$('input[name=\'captain_id\']').val(ui.item.value);
		$('input[name=\'captain\']').val(ui.item.name)
		$('.dropdown-menu').hide();    
		$('#input-person').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});
</script>
<script type="text/javascript"><!--
dialog = $("#dialog-form").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Cancel: function() {
			dialog.dialog( "close" );
		}
	},
});

dialog1 = $("#dialog-form1").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog1.dialog("close");
			html = '<a data-toggle="tooltip" onclick="check()" title="check" class="btn btn-primary check" style="margin-top: 10%">Check</a>';
			$('.custcheck').html('');
			$('.custcheck').append(html);
			$('.check').focus();
		}
	},
});

dialog_merge = $("#dialog-form_merge").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog_merge.dialog("close");
		}
	},
});

dialog_changetable = $("#dialog-form_changetable").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog_changetable.dialog("close");
			setTimeout(close_fun, 500);
		}
	},
});

dialog_dayclose = $("#dialog-form_dayclose").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true
});

dialog_moreoption = $("#dialog-form_moreoption").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog_moreoption.dialog("close");
		}
	},
});

dialog_tendering = $("#dialog-form_tendering").dialog({
		closeOnEscape: true,
		autoOpen: false,
		height: 750,
		width: 1000,
		modal: true,
		open: function(event, ui) {
        	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
    	}
	});

dialog_parceldetail = $("#dialog-form_parceldetail").dialog({
	autoOpen: false,
	height: 650,
	width: 1000,
	modal: true
});

dialog_complimentary = $("#dialog-form_complimentary").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			//$(this).dialog("close");
			dialog_complimentary.dialog("close");
			setTimeout(close_fun, 500);
			//html = '<a  data-toggle="tooltip" onclick="check()" title="check" class="btn btn-primary check" style="margin-top: 10%">Check</a>';
			//$('.custcheck').append(html);
		}
	},
});

var orderidmodify = '<?php echo $orderidmodify ?>';
if(orderidmodify != ''){
	dialog_paymentmode = $("#dialog-form_paymentmode").dialog({
		closeOnEscape: false,
		autoOpen: false,
		height: 750,
		width: 1000,
		modal: true,
		open: function(event, ui) {
        	$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
    	}
	});
} else{
	dialog_paymentmode = $("#dialog-form_paymentmode").dialog({
		autoOpen: false,
		height: 750,
		width: 1000,
		modal: true
	});
}

dialog_pendinginfo = $("#dialog-form_pendinginfo").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog_pendinginfo.dialog("close");
		}
	},
});

dialog_currentsaleinfo = $("#dialog-form_currentsaleinfo").dialog({
	autoOpen: false,
	height: 800,
	width: 1300,
	modal: true,
	buttons: {
		Done: function() {
			dialog_currentsaleinfo.dialog("close");
		}
	},
});

dialog_help = $("#dialog-form_help").dialog({
	autoOpen: false,
	height: 750,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			dialog_help.dialog("close");
		}
	},
});

dialog_select_qty = $("#dialog-form_select_qty").dialog({
	autoOpen: false,
	height: 300,
	width: 200,
	modal: true,
	buttons: {
		Done: function() {
			var increment = $("#autoincrementqty").val();
			$('#qty_'+increment).val($("#selectquantity").val());
			$('.qty').trigger('change');
			dialog_select_qty.dialog("close");
		}
	},
});

dialog_select_rate = $("#dialog-form_select_rate").dialog({
	autoOpen: false,
	height: 350,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			var increment = $("#autoincrementrate").val();
			$('#rate_'+increment).val($("#selectrate").val());
			$('.rate').trigger('change');
			dialog_select_rate.dialog("close");
		}
	},
});


dialog_select_persons = $("#dialog-form_select_persons").dialog({
	autoOpen: false,
	height: 350,
	width: 750,
	modal: true,
	buttons: {
		Done: function() {
			$('#input-person').val($("#selectpersons").val());
			dialog_select_persons.dialog("close");
		}
	},
});

dialog_kottransfer = $("#dialog-form_kottransfer").dialog({
	autoOpen: false,
	height: 700,
	width: 1650,
	closeOnEscape: true,
	modal: true,
	buttons: {
		Done: function() {
			dialog_kottransfer.dialog("close");
		}
	},
});

dialog_nckot = $("#dialog-form_nckot").dialog({
	autoOpen: false,
	height: 800,
	width: 900,
	modal: true,
	buttons: {
		Done: function() {
			dialog_nckot.dialog("close");
			setTimeout(close_fun, 500);
		}
	},
});

function close_fun(){
  window.location.reload();
}

function addwaiter(wid,wname){
	dialog.dialog( "close" );
	$('#input-waiter').val(wname);
	$('#input-waiter_id').val(wid);
}

function addcaptain(cpid,cpname){
	dialog.dialog( "close" );
	$('#input-captain').val(cpname);
	$('#input-captain_id').val(cpid);
}

function waitersel(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/waitersel&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			dialog.dialog("open");
			$('#dialog-form').html(data.html)
		}
	});
}

function waiterselid(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/waiterselid&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			dialog.dialog("open");
			$('#dialog-form').html(data.html)
		}
	});
}


function newreg(){
	no = $('#input-cust_contact').val();
	htmlc = '<iframe style="border: 0px; " src="index.php?route=catalog/customer/add&token=<?php echo $token; ?>&contactno='+no+'"width="100%" height="100%"></iframe>'
	dialog1.dialog("open");
	$('#dialog-form1').html(htmlc)
}

function dayclose(){
	htmlc = '<iframe style="border: 0px; " src="index.php?route=catalog/dayclose&token=<?php echo $token; ?>&dayclose=1" width="100%" height="100%"></iframe>'
	dialog_dayclose.dialog("open");
	$('#dialog-form_dayclose').html(htmlc)
}

function moreoption(){
	html = '<table class="table table-bordered table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="kot_fun()" style="cursor: pointer;width: 126px;">KOT</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="bill()" style="cursor: pointer;width: 126px;">BILL</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="paymentmode()" style="cursor: pointer;width: 126px;">SETTLEMENT</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="check_kot()" style="cursor: pointer;width: 126px;">CHECK KOT</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" id="billview" style="width: 126px;">BILL VIEW</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="pendingtable_fun()" style="cursor: pointer;width: 126px;">PENDING TABLE</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="nckot()" style="cursor: pointer;width: 126px;">NC KOT</a>';
			html += '</td>';
			html += '<td>';
				html += '<a onClick="mergebill()" id="mergebill" data-toggle="tooltip" title="<?php echo "Add bill" ?>" class="btn btn-info" style="cursor: pointer;width: 126px;">Add Bill</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="kottransfer()"style="cursor: pointer;width: 126px;">KOT Transfer</a>';
			html += '</td>';
			html += '<td>';
				html += '<a onClick="complimentary()" data-toggle="tooltip" title="<?php echo "Complimentary" ?>" class="btn btn-info" style="cursor: pointer;width: 126px;">Complimentary</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" style="cursor: pointer;width: 126px;">Split Bill</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" style="cursor: pointer;width: 126px;">Delivery Boy Detail</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a onClick="changetable()" data-toggle="tooltip" title="<?php echo "Change Table" ?>" class="btn btn-info" style="cursor: pointer;width: 126px;">Change Table</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" style="cursor: pointer;width: 126px;">Bill Modify</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="current_sale_fun()" style="cursor: pointer;width: 126px;">Current Sale</a>';
			html += '</td>';
			html += '<td>';
				html += '<a onClick="dayclose()" data-toggle="tooltip" title="<?php echo "DAY CLOSE" ?>" class="btn btn-info" style="cursor: pointer;width: 126px;">Day Close</a>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
				html += '<a class="btn btn-info" href="index.php?route=catalog/advance&token=<?php echo $token; ?>" style="cursor: pointer;width: 126px;">Advance Order</a>';
			html += '</td>';
			html += '<td>';
				html += '<a class="btn btn-info" onclick="pendingPrints()" style="cursor: pointer;width: 126px;">Pending Prints</a>';
			html += '</td>';
		html += '</tr>';
	html += '</table>';
	dialog_moreoption.dialog("open");
	$('#dialog-form_moreoption').html(html)
}

$(document).ready(function(){
	orderidmodify = '<?php echo $orderidmodify ?>';
	if(orderidmodify != ''){
		paymentmode();
	}
});

function paymentmode(){	
	var issettlement = '<?php echo SETTLEMENT ?>';

	orderidmodify = '<?php echo $orderidmodify ?>';
	if(orderidmodify != ''){
		orderid = orderidmodify;
	} else{
		orderid = $('#orderid').val();
	}
	orderno = $('#orderno').val();

	if(issettlement == '1'){
		if((orderid != '' && orderid != '0' && orderid != undefined && orderno != '' && orderno != '0' && orderno != undefined) || (orderidmodify != '')){
			htmlc = '<iframe id="paymentmode" style="border: 0px;" src="index.php?route=catalog/settlement&token=<?php echo $token; ?>&order_id='+orderid+'&orderidmodify='+orderidmodify+'" width="100%" height="100%"></iframe>';
			dialog_paymentmode.dialog("open");
			$('#dialog-form_paymentmode').html(htmlc)
		}else{
			alert("Please Select Table Name/Remove bill first");
			return false;
		}
	}else{

	}
}

$(document).on('click', '#billview', function(){
	$('#billview').attr('href','index.php?route=catalog/billview&token=<?php echo $token; ?>&qwerty=1');
});

function help(){
	html = '<table class="table table-bordered table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<th style="text-align: center;" >Shortcuts</th>';
			html += '<th style="text-align: center;" >Functions</th>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>PgUp</td>';
			html += '<td>KOT</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>PgDn</td>';
			html += '<td>Bill</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>Esc</td>';
			html += '<td>Referesh</td>';
		html += '</tr>';

	html += '</table>';
	dialog_help.dialog("open");
	$('#dialog-form_help').html(html)
}

function select_qty(id){
	html = '<table class="table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<td><h1 onclick="increase_qty()">+</h1></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td>';
			html += '<input style="padding: 0px 1px;" value="0" id="selectquantity" class="inputs form-control"/>';
			html += '<input type="hidden" style="padding: 0px 1px;" value="'+id+'" id="autoincrementqty"/>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h1 onclick="decrease_qty()">-</h1></td>';
		html += '</tr>';
	html += '</table>';
	dialog_select_qty.dialog("open");
	$('#dialog-form_select_qty').html(html)
}

function select_rate(id){
	html = '<table class="table table-bordered table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<td colspan="3">';
			html += '<input style="padding: 0px 1px;" value="" id="selectrate" class="inputs form-control"/>';
			html += '<input type="hidden" style="padding: 0px 1px;" value="'+id+'" id="autoincrementrate"/>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="seven" onclick="changerate(this)">7</h4></td>';
			html += '<td><h4 id="eight" onclick="changerate(this)">8</h4></td>';
			html += '<td><h4 id="nine"  onclick="changerate(this)">9</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="four" onclick="changerate(this)">4</h4></td>';
			html += '<td><h4 id="five" onclick="changerate(this)">5</h4></td>';
			html += '<td><h4 id="six"  onclick="changerate(this)">6</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="one"   onclick="changerate(this)">1</h4></td>';
			html += '<td><h4 id="two"   onclick="changerate(this)">2</h4></td>';
			html += '<td><h4 id="three" onclick="changerate(this)">3</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td colspan="2"><h4 id="zero" onclick="changerate(this)">0</h4></td>';
			html += '<td><h4 id="del" onclick="changerate(this)">del</h4></td>';
		html += '</tr>';
	html += '</table>';
	dialog_select_rate.dialog("open");
	$('#dialog-form_select_rate').html(html)
}

function select_persons(){
	html = '<table class="table table-bordered table-hover" style="text-align: center;">';
		html += '<tr>';
			html += '<td colspan="3">';
			html += '<input style="padding: 0px 1px;" value="" id="selectpersons" class="inputs form-control"/>';
			html += '</td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="seven" onclick="changeperson(this)">7</h4></td>';
			html += '<td><h4 id="eight" onclick="changeperson(this)">8</h4></td>';
			html += '<td><h4 id="nine"  onclick="changeperson(this)">9</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="four" onclick="changeperson(this)">4</h4></td>';
			html += '<td><h4 id="five" onclick="changeperson(this)">5</h4></td>';
			html += '<td><h4 id="six"  onclick="changeperson(this)">6</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td><h4 id="one"   onclick="changeperson(this)">1</h4></td>';
			html += '<td><h4 id="two"   onclick="changeperson(this)">2</h4></td>';
			html += '<td><h4 id="three" onclick="changeperson(this)">3</h4></td>';
		html += '</tr>';
		html += '<tr>';
			html += '<td colspan="2"><h4 id="zero" onclick="changeperson(this)">0</h4></td>';
			html += '<td><h4 id="del" onclick="changeperson(this)">del</h4></td>';
		html += '</tr>';
	html += '</table>';
	dialog_select_persons.dialog("open");
	$('#dialog-form_select_persons').html(html)
}

function changeperson(value){
	var id = $(value).attr("id");
	var test;
	switch(id){
		case 'one':
			test = $("#selectpersons").val()+1; 
			break;
		case 'two':
			test = $("#selectpersons").val()+2; 
			break;
		case 'three':
			test = $("#selectpersons").val()+3; 
			break;
		case 'four':
			test = $("#selectpersons").val()+4;
			break;
		case 'five':
			test = $("#selectpersons").val()+5; 
			break;
		case 'six':
			test = $("#selectpersons").val()+6; 
			break;
		case 'seven':
			test = $("#selectpersons").val()+7; 
			break;
		case 'eight':
			test = $("#selectpersons").val()+8; 
			break;
		case 'nine':
			test = $("#selectpersons").val()+9; 
			break;
		case 'zero':
			test = $("#selectpersons").val()+0; 
			break;
		case 'del': 
			var mainvalue = $('#selectpersons').val();
			var lastvalue = mainvalue.toString().split('').pop();
			result = mainvalue-lastvalue;
			test = result.toString().replace('0','');
			break;
		default :
			test = $("#selectpersons").val(0); 
			break;
	}
	$("#selectpersons").val(test);
}

function changerate(value){
	var id = $(value).attr("id");
	var test;
	switch(id){
		case 'one':
			test = $("#selectrate").val()+1; 
			break;
		case 'two':
			test = $("#selectrate").val()+2; 
			break;
		case 'three':
			test = $("#selectrate").val()+3; 
			break;
		case 'four':
			test = $("#selectrate").val()+4;
			break;
		case 'five':
			test = $("#selectrate").val()+5; 
			break;
		case 'six':
			test = $("#selectrate").val()+6; 
			break;
		case 'seven':
			test = $("#selectrate").val()+7; 
			break;
		case 'eight':
			test = $("#selectrate").val()+8; 
			break;
		case 'nine':
			test = $("#selectrate").val()+9; 
			break;
		case 'zero':
			test = $("#selectrate").val()+0; 
			break;
		case 'del': 
			var mainvalue = $('#selectrate').val();
			var lastvalue = mainvalue.toString().split('').pop();
			result = mainvalue-lastvalue;
			test = result.toString().replace('0','');
			break;
		default :
			test = $("#selectrate").val(0); 
			break;
	}
	$("#selectrate").val(test);
}

function increase_qty(){
	var counter = $('#selectquantity').val();
	var autoincrementqty = $('#autoincrementqty').val();
	var existingqty = $('#pre_qty'+autoincrementqty).val();
	var is_new = $('#is_new'+autoincrementqty).val();
	counter++;
	if(counter > existingqty && is_new == '1'){
		alert("Quantity Cannot be Greater than Ordered Quantity");
		$('#selectquantity').val(existingqty);
	} else {
		$('#selectquantity').val(counter);
	}
}

function decrease_qty(){
	var counter = $('#selectquantity').val();
	counter--;
	if(counter < 0){
		alert("Quantity cananot be Negative");
	} else {
		$('#selectquantity').val(counter);
	}
}

function pendingtable_fun(){
	dialog_moreoption.dialog("close");
		$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/pendingtableinfo&token=<?php echo $token; ?>',
		dataType: 'json',
		success: function(data){
			dialog_pendinginfo.dialog("open");
			$('#dialog-form_pendinginfo').html(data.table_info)
		}
	});
}

function current_sale_fun(){
	dialog_moreoption.dialog("close");
		htmlc = '<iframe style="border: 0px;" src="index.php?route=catalog/currentsale&token=<?php echo $token; ?>" width="100%" height="100%"></iframe>';
		dialog_currentsaleinfo.dialog("open");
		$('#dialog-form_currentsaleinfo').html(htmlc)
}



function mergebill(){
	orderid = $('#orderid').val();
		htmlc = '<iframe style="border: 0px;" src="index.php?route=catalog/billmerge&token=<?php echo $token; ?>&order_no='+orderid+'" width="100%" height="100%"></iframe>';
		dialog_merge.dialog("open");
		$('#dialog-form_merge').html(htmlc)
}

function kottransfer(){
	dialog_moreoption.dialog("close");
		htmlc = '<iframe id="kottransfer" style="border: 0px;" src="index.php?route=catalog/kottransfer&token=<?php echo $token; ?>" width="100%" height="80%"></iframe>';
		dialog_kottransfer.dialog("open");
		$('#dialog-form_kottransfer').html(htmlc)
}

function changetable(){
	orderid = $('#orderid').val();
	table_name = $('#input-t_number').val();
	table_id = $('#input-t_number_id').val();
		htmlc = '<iframe style="border: 0px;" src="index.php?route=catalog/tablechange&token=<?php echo $token; ?>&order_no='+orderid+'&table_frm='+table_id+'&table_frm1='+table_name+'" width="100%" height="100%"></iframe>';
		dialog_changetable.dialog("open");
		$('#dialog-form_changetable').html(htmlc)
}

function nckot(){
	dialog_moreoption.dialog("close");
	htmlc = '<iframe style="border: 0px;" src="index.php?route=catalog/nckot&token=<?php echo $token; ?>" width="100%" height="100%"></iframe>';
	dialog_nckot.dialog("open");
	$('#dialog-form_nckot').html(htmlc)
}

function complimentary(){
	orderid = $('#orderid').val();
	table_name = $('#input-t_number').val();
	table_id = $('#input-t_number_id').val();
	//if(orderid != '' && orderid != '0' && orderid != undefined){
		htmlc = '<iframe style="border: 0px;" src="index.php?route=catalog/complimentary&token=<?php echo $token; ?>&order_no='+orderid+'&table_frm='+table_id+'&table_frm1='+table_name+'" width="100%" height="100%"></iframe>';
		dialog_complimentary.dialog("open");
		$('#dialog-form_complimentary').html(htmlc)
	//} else {
		//alert('Please Select Table Data');
	//}
}

function captainsel(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/captainsel&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			dialog.dialog("open");
			$('#dialog-form').html(data.html)
		}
	});
}

function captainselid(){
	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/captainselid&token=<?php echo $token; ?>',
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			dialog.dialog("open");
			$('#dialog-form').html(data.html)
		}
	});
}

//--></script>
<script type="text/javascript"><!--

function closeIFrame(){
      location.replace('index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
}

function closeparceldetail(){
	$("#dialog-form_parceldetail").dialog("close");
	$('#input-waiterid').focus();
}

function check(){
  	no=$('#input-cust_contact').val();
	//alert(no)
  	$.ajax({
		type: "POST",
		url: 'index.php?route=catalog/orderqwerty/checkcustomers&token=<?php echo $token; ?>&contactno='+no,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
		   	$('input[name=\'cust_name\']').val(data.name);
			$('input[name=\'cust_contact\']').val(data.contactno);
	 		$('input[name=\'cust_id\']').val(data.c_id);
	  		$('input[name=\'cust_address\']').val(data.address);
	  		$('input[name=\'cust_email\']').val(data.email);
			$('.custcheck').html('');
			$('.custcheck').append(data.html);
		}
  	});
}
//--></script>
<script type="text/javascript">
var i = $('table tr').length;
function remove_folder(c) {
	console.log('in remove folder');
  	var b = $('table tr').length;
  	is_liq1 = $('#is_liq_'+c).val();
 	if(is_liq1 == 0 && is_liq1 != ''){
		amt1 = parseFloat($('#amt_'+c).val());
		if(amt1 == '' || amt1 == '0' || isNaN(amt1)){
			amt1 = 0;
		}
		tax1_value = parseFloat($('#tax1_value_'+c).val());
		tax2_value = parseFloat($('#tax2_value_'+c).val());
		discount_value = parseFloat($('discount_value_'+c).val());
		total = parseFloat($('#input-ftotal').val());
		total1= total - amt1;
		$('#input-ftotal').val(total1);
		$('#ftotal_discount').val(total1);
		tax1_value = 0;
		tax2_value = 0;
		discount_value = 0;
	} else if(is_liq1 == 1 && is_liq1 != ''){
		amt1 = parseFloat($('#amt_'+c).val());
		total = parseFloat($('#input-ltotal').val());
		tax1_value = parseFloat($('#tax1_value_'+c).val());
		tax2_value = parseFloat($('#tax2_value_'+c).val());
		discount_value = parseFloat($('discount_value_'+c).val());
		total1 = total - amt1;
		$('#input-ltotal').val(total1);
		$('#ltotal_discount').val(total1);
		tax1_value = 0;
		tax2_value = 0;
		discount_value = 0;
  	}
  	$('.re_'+c).remove();
  	$('#re_'+c).remove();
 	var final_datas = [];
 	var j = 1;
 	var final_datastest = [];
 	var jtest = 1;
 	$('.ordertab').find('tr').each (function() {
		idss = $(this).attr('id');
		if(idss != undefined){
			final_datas[j] = [];
			s_id = idss.split('_');
			id = s_id[1];
			final_datas[j]['kot_status'] = $('#kot_status'+id).val();
			final_datas[j]['cancelstatus'] = $('#cancelstatus'+id).val();	
			final_datas[j]['pre_qty'] = $('#pre_qty'+id).val();	
			final_datas[j]['kot_no'] = $('#kot_no'+id).val();	
			final_datas[j]['is_new'] = $('#is_new'+id).val();	
			final_datas[j]['referparent'] = id;
			final_datas[j]['parent'] = $('#parent_'+id).val();
			final_datas[j]['is_set'] = $('#is_set'+id).val();	
			final_datas[j]['code'] = $('#code_'+id).val();	
			final_datas[j]['name'] = $('#name_'+id).val();	
			final_datas[j]['id'] = $('#id_'+id).val();	
			final_datas[j]['qty'] = $('#qty_'+id).val();	
			final_datas[j]['rate'] = $('#rate_'+id).val();	
			final_datas[j]['amt'] = $('#amt_'+id).val();	
			final_datas[j]['is_liq'] = $('#is_liq_'+id).val();	
			final_datas[j]['cancelmodifier'] = $('#cancelmodifier_'+id).val();
			final_datas[j]['message'] = $('#message_'+id).val();
			final_datas[j]['tax1'] = $('#tax1_'+id).val();
			final_datas[j]['tax2'] = $('#tax2_'+id).val();
			final_datas[j]['tax1_value'] = $('#tax1_value_'+id).val();
			final_datas[j]['tax2_value'] = $('#tax2_value_'+id).val();
			final_datas[j]['discount_per'] = $('#discount_per_'+id).val();
			final_datas[j]['discount_value'] = $('#discount_value_'+id).val();
			final_datas[j]['subcategoryid'] = $('#subcategoryid_'+id).val();
			final_datas[j]['billno'] = $('#billno_'+id).val();
			final_datas[j]['ismodifier'] = $('#ismodifier_'+id).val();
			final_datas[j]['nc_kot_status'] = $('#nc_kot_status_'+id).val();
			final_datas[j]['nc_kot_reason'] = $('#nc_kot_reason_'+id).val();
			final_datas[j]['transfer_qty'] = $('#transfer_qty_'+id).val();
			final_datas[j]['parent_id'] = $('#parent_id_'+id).val();
			$('#re_'+id).remove();
			$('.ordertab').find("tr:hidden[class='re_"+id+"']").each (function() {
				idss = $(this).attr('id');
				if(idss != undefined){
					final_datastest[jtest] = [];
					idmodifier = idss.slice(3);
					final_datastest[jtest]['kot_status'] = $('#kot_status'+idmodifier).val();
					final_datastest[jtest]['cancelstatus'] = $('#cancelstatus'+idmodifier).val();	
					final_datastest[jtest]['pre_qty'] = $('#pre_qty'+idmodifier).val();	
					final_datastest[jtest]['kot_no'] = $('#kot_no'+idmodifier).val();	
					final_datastest[jtest]['is_new'] = $('#is_new'+idmodifier).val();	
					final_datastest[jtest]['is_set'] = $('#is_set'+idmodifier).val();	
					final_datastest[jtest]['code'] = $('#code_'+idmodifier).val();
					final_datastest[jtest]['referparent'] = $('#referparent_'+idmodifier).val();
					final_datastest[jtest]['parent'] = $('#parent_'+idmodifier).val();	
					final_datastest[jtest]['name'] = $('#name_'+idmodifier).val();	
					final_datastest[jtest]['id'] = $('#id_'+idmodifier).val();	
					final_datastest[jtest]['qty'] = $('#qty_'+idmodifier).val();	
					final_datastest[jtest]['rate'] = $('#rate_'+idmodifier).val();	
					final_datastest[jtest]['amt'] = $('#amt_'+idmodifier).val();	
					final_datastest[jtest]['is_liq'] = $('#is_liq_'+idmodifier).val();	
					final_datastest[jtest]['cancelmodifier'] = $('#cancelmodifier_'+idmodifier).val();
					final_datastest[jtest]['message'] = $('#message_'+idmodifier).val();
					final_datastest[jtest]['tax1'] = $('#tax1_'+idmodifier).val();
					final_datastest[jtest]['tax2'] = $('#tax2_'+idmodifier).val();
					final_datastest[jtest]['tax1_value'] = $('#tax1_value_'+idmodifier).val();
					final_datastest[jtest]['tax2_value'] = $('#tax2_value_'+idmodifier).val();
					final_datastest[jtest]['discount_per'] = $('#discount_per_'+idmodifier).val();
					final_datastest[jtest]['discount_value'] = $('#discount_value_'+idmodifier).val();
					final_datastest[jtest]['subcategoryid'] = $('#subcategoryid_'+idmodifier).val();
					final_datastest[jtest]['billno'] = $('#billno_'+idmodifier).val();
					final_datastest[jtest]['ismodifier'] = $('#ismodifier_'+idmodifier).val();
					final_datastest[jtest]['nc_kot_status'] = $('#nckotstatus_'+idmodifier).val();
					final_datastest[jtest]['nc_kot_reason'] = $('#nckotreason_'+idmodifier).val();
					final_datastest[jtest]['transfer_qty'] = $('#transferqty_'+idmodifier).val();
					final_datastest[jtest]['parent_id'] = $('#parent_id_'+idmodifier).val();
					jtest ++;
				}
			});
			$('.re_'+id).remove();
			j ++;
		}
	});

  	tab_index = 1;
	if($('#input-t_number').length > 0){
		tab_index ++;
	}
	if($('#input-waiter').length > 0){
		tab_index ++;
	}
	if($('#input-waiterid').length > 0){
		tab_index ++;
	}
	if($('#input-captainid').length > 0){
		tab_index ++;
	}
	if($('#input-captain').length > 0){
		tab_index ++;
	}
	if($('#input-person').length > 0){
		tab_index ++;
	}
  	i = 1;
  	last_key = j - 1;
  	for (var arrayindex in final_datas) {
  		if(final_datas[arrayindex]['cancelstatus'] == 1){
  			display = 'none';
  		} else{
  			display = '';
  		}
		html = '<tr id="re_'+i+'" style="display:'+display+'">';
			html += '<td class="r_'+i+'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">' + i;
				html += '<input type="hidden" value="'+final_datas[arrayindex]['kot_status']+'" name="po_datas['+i+'][kot_status]" id="kot_status'+i+'" />';
				html += '<input type="hidden" value="'+final_datas[arrayindex]['pre_qty']+'" name="po_datas['+i+'][pre_qty]" id="pre_qty'+i+'" />';
				html += '<input type="hidden" value="'+final_datas[arrayindex]['kot_no']+'" name="po_datas['+i+'][kot_no]" id="kot_no'+i+'" />';
				html += '<input type="hidden" value="'+final_datas[arrayindex]['is_new']+'" name="po_datas['+i+'][is_new]" id="is_new'+i+'" />';
				html += '<input type="hidden" value="'+final_datas[arrayindex]['is_set']+'" name="po_datas['+i+'][is_set]" id="is_set'+i+'" />';
				html += '<input type="hidden" value="'+final_datas[arrayindex]['cancelstatus']+'" name="po_datas['+i+'][cancelstatus]" id="cancelstatus'+i+'" />';
			html += '</td>';
			html += '<td class="r_'+i+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
				if(final_datas[arrayindex]['code'] == ''){
					html += '<input value="'+final_datas[arrayindex]['code']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas[' + i + '][code]" id="code_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['subcategoryid']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas[' + i + '][subcategoryid]" id="subcategoryid_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax1']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas[' + i + '][tax1]" id="tax1_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax2']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas[' + i + '][tax2]" id="tax2_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax1_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas[' + i + '][tax1_value]" id="tax1_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax2_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas[' + i + '][tax2_value]" id="tax2_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['discount_per']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas[' + i + '][discount_per]" id="discount_per_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['discount_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas[' + i + '][discount_value]" id="discount_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['billno']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas[' + i + '][billno]" id="billno_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['ismodifier']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" name="po_datas[' + i + '][ismodifier]" id="ismodifier_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['nc_kot_status']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" name="po_datas[' + i + '][nc_kot_status]" id="nc_kot_status_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['nc_kot_reason']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" name="po_datas[' + i + '][nc_kot_reason]" id="nc_kot_reason_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['transfer_qty']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" name="po_datas[' + i + '][transfer_qty]" id="transfer_qty_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['parent_id']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas[' + i + '][parent_id]" id="parent_id_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['referparent']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs referparent form-control" name="po_datas[' + i + '][referparent]" id="referparent_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['parent']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas[' + i + '][parent]" id="parent_' + i + '" />';
				} else {
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['code']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas[' + i + '][code]" id="code_' + i + '" />';
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['subcategoryid']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas[' + i + '][subcategoryid]" id="subcategoryid_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax1']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas[' + i + '][tax1]" id="tax1_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax2']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas[' + i + '][tax2]" id="tax2_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax1_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas[' + i + '][tax1_value]" id="tax1_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['tax2_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas[' + i + '][tax2_value]" id="tax2_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['discount_per']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas[' + i + '][discount_per]" id="discount_per_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['discount_value']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas[' + i + '][discount_value]" id="discount_value_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['billno']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas[' + i + '][billno]" id="billno_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['ismodifier']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" name="po_datas[' + i + '][ismodifier]" id="ismodifier_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['nc_kot_status']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" name="po_datas[' + i + '][nc_kot_status]" id="nc_kot_status_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['nc_kot_reason']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" name="po_datas[' + i + '][nc_kot_reason]" id="nc_kot_reason_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['nc_kot_reason']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" name="po_datas[' + i + '][transfer_qty]" id="transfer_qty_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['parent_id']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas[' + i + '][parent_id]" id="parent_id_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['referparent']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs referparent form-control" name="po_datas[' + i + '][referparent]" id="referparent_' + i + '" />';
					html += '<input value="'+final_datas[arrayindex]['parent']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas[' + i + '][parent]" id="parent_' + i + '" />';
				}
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="padding-top: 2px;padding-bottom: 2px;">';
				if(final_datas[arrayindex]['name'] == ''){
					html += '<input value="'+final_datas[arrayindex]['name']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[' + i + '][name]" id="name_' + i + '" />';
				} else {
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['name']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[' + i + '][name]" id="name_' + i + '" />';
				}
				html += '<input value="'+final_datas[arrayindex]['id']+'" style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas[' + i + '][id]" id="id_' + i + '" />';
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
			<?php if($display_type == '1') { ?>
				html += '<input value="'+final_datas[arrayindex]['qty']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs qty form-control" name="po_datas[' + i + '][qty]" id="qty_' + i + '" autocomplete="off"/>';
			<?php } else { ?>
				html += '<input value="'+final_datas[arrayindex]['qty']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('+i+')" class="inputs qty form-control screenquantity" name="po_datas[' + i + '][qty]" id="qty_' + i + '" />';
			<?php } ?>
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
			<?php if($display_type == '1') { ?>
				if(final_datas[arrayindex]['is_new'] == 1){
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['rate']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
				} else {
					html += '<input value="'+final_datas[arrayindex]['rate']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
				}
			<?php } else { ?>
				if(final_datas[arrayindex]['is_new'] == 1){
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['rate']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('+i+')" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
				} else {
					html += '<input value="'+final_datas[arrayindex]['rate']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('+i+')" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
				}
			<?php } ?>	
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
				html += '<input readonly="readonly" value="'+final_datas[arrayindex]['amt']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[' + i + '][amt]" id="amt_' + i + '" />';
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
				if(final_datas[arrayindex]['is_new'] == 1){
					html += '<input readonly="readonly" value="'+final_datas[arrayindex]['message']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[' + i + '][message]" id="message_' + i + '" /><input style="display:none" type="text" name="po_datas['+i+'][is_liq]"  id="is_liq_'+i+'" value="'+final_datas[arrayindex]['is_liq']+'" />';
					html += '<input value="'+final_datas[arrayindex]['cancelmodifier']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas[' + i + '][cancelmodifier]" id="cancelmodifier_' + i + '" />';
				} else {
					html += '<input value="'+final_datas[arrayindex]['message']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[' + i + '][message]" id="message_' + i + '" /><input style="display:none" type="text" name="po_datas['+i+'][is_liq]"  id="is_liq_'+i+'" value="'+final_datas[arrayindex]['is_liq']+'" />';	
					html += '<input value="'+final_datas[arrayindex]['cancelmodifier']+'" tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas[' + i + '][cancelmodifier]" id="cancelmodifier_' + i + '" />';	
				}
				tab_index ++;
			html += '</td>';
			html += '<td class="r_'+i+'" style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
				if(final_datas[arrayindex]['is_new'] == 0){
					html += '<a tabindex = "'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'"><i class="fa fa-trash-o"></i></a>';
					tab_index ++;
				} else {
					html += '-';
				}
			html += '</td>';
	 	html += '</tr>';
	 	$('#ordertab').append(html);
	 	imodifier = 1;
	 	for (var arrayindexmodifier in final_datastest) {
	 		if(final_datas[arrayindex]['referparent'] == final_datastest[arrayindexmodifier]['referparent']){
		 		html1 = '<tr id="re_'+arrayindex+'_'+imodifier+'" class="re_'+arrayindex+'" style="display:none" >';
					html1 += '<td id="r_'+arrayindex+'_'+imodifier+'" class="r_'+arrayindex+'" style="display:none; padding-top: 2px;padding-bottom: 2px;font-size: 16px;">'
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['kot_status']+'" name="po_datas['+arrayindex+'_'+imodifier+'][kot_status]" class="re_'+arrayindex+'" id="kot_status'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['pre_qty']+'" name="po_datas['+arrayindex+'_'+imodifier+'][pre_qty]" class="re_'+arrayindex+'" id="pre_qty'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['kot_no']+'" name="po_datas['+arrayindex+'_'+imodifier+'][kot_no]" class="re_'+arrayindex+'" id="kot_no'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['is_new']+'" name="po_datas['+arrayindex+'_'+imodifier+'][is_new]" class="re_'+arrayindex+'" id="is_new'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['is_set']+'" name="po_datas['+arrayindex+'_'+imodifier+'][is_set]" class="re_'+arrayindex+'" id="is_set'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['cancelstatus']+'" name="po_datas['+arrayindex+'_'+imodifier+'][cancelstatus]" class="re_'+arrayindex+'" id="cancelstatus'+arrayindex+'_'+imodifier+'"/>';
					html1 + '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][code]" value="'+final_datastest[arrayindexmodifier]['code']+'" class="re_'+arrayindex+'" id="code_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][subcategoryid]" value="'+final_datastest[arrayindexmodifier]['subcategoryid']+'" class="re_'+arrayindex+'" id="subcategoryid_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][tax1]" value="'+final_datastest[arrayindexmodifier]['tax1']+'" class="re_'+arrayindex+'" id="tax1_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][tax2]" value="'+final_datastest[arrayindexmodifier]['tax2']+'" class="re_'+arrayindex+'" id="tax2_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][tax1_value]" value="'+final_datastest[arrayindexmodifier]['tax1_value']+'" class="re_'+arrayindex+'" id="tax1_value_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][tax2_value]" value="'+final_datastest[arrayindexmodifier]['tax2_value']+'" class="re_'+arrayindex+'" id="tax2_value_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][discount_per]" value="'+final_datastest[arrayindexmodifier]['discount_per']+'" class="re_'+arrayindex+'" id="discount_per_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][discount_value]" value="'+final_datastest[arrayindexmodifier]['discount_value']+'" class="re_'+arrayindex+'" id="discount_value_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][billno]" value="'+final_datastest[arrayindexmodifier]['billno']+'" class="re_'+arrayindex+'" id="billno_'+arrayindex+'_'+imodifier+'" />';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][parent_id]" value="'+final_datastest[arrayindexmodifier]['parent_id']+'" class="re_'+arrayindex+'" id="parent_id_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][ismodifier]" value="'+final_datastest[arrayindexmodifier]['ismodifier']+'" class="re_'+arrayindex+'" id="ismodifier_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][nckotstatus]" value="'+final_datastest[arrayindexmodifier]['nc_kot_status']+'" class="re_'+arrayindex+'" id="nckotstatus_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][nckotreason]" value="'+final_datastest[arrayindexmodifier]['nc_kot_reason']+'" class="re_'+arrayindex+'" id="nckotreason_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][transferqty]" value="'+final_datastest[arrayindexmodifier]['transfer_qty']+'" class="re_'+arrayindex+'" id="transferqty_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][referparent]" value="'+final_datastest[arrayindexmodifier]['referparent']+'" class="re_'+arrayindex+'" id="referparent_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][parent]" value="'+final_datastest[arrayindexmodifier]['parent']+'" class="re_'+arrayindex+'" id="parent_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][name]" value="'+final_datastest[arrayindexmodifier]['name']+'" class="re_'+arrayindex+'" id="name_'+arrayindex+'_'+imodifier+'"/>';
						html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][id]" value="'+final_datastest[arrayindexmodifier]['id']+'" class="re_'+arrayindex+'" id="id_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" class="re_'+arrayindex+'"style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][qty]" value="'+final_datastest[arrayindexmodifier]['qty']+'" id="qty_'+arrayindex+'_'+imodifier+'" autocomplete="off"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][rate]" value="'+final_datastest[arrayindexmodifier]['rate']+'" class="re_'+arrayindex+'" id="rate_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="hidden" value="'+final_datastest[arrayindexmodifier]['amt']+'" class="re_'+arrayindex+'" name="po_datas['+arrayindex+'_'+imodifier+'][amt]" id="amt_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" class="re_'+arrayindex+'"  name="po_datas['+arrayindex+'_'+imodifier+'][message]" value="'+final_datastest[arrayindexmodifier]['message']+'" id="message_'+arrayindex+'_'+imodifier+'" /><input type="hidden" name="po_datas['+arrayindex+'_'+imodifier+'][is_liq]"  value="'+final_datastest[arrayindexmodifier]['is_liq']+'" class="re_'+arrayindex+'" id="is_liq_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
					html1 += '<td class="r_'+arrayindex+'" id="r_'+arrayindex+'_'+imodifier+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
						html1 += '<input type="hidden" value="'+final_datastest[arrayindexmodifier]['cancelmodifier']+'" name="po_datas['+arrayindex+'_'+imodifier+'][cancelmodifier]" class="re_'+arrayindex+'" id="cancelmodifier_'+arrayindex+'_'+imodifier+'"/>';
					html1 += '</td>';
			 	html1 += '</tr>';
			 	$('#ordertab').append(html1);
			 	imodifier++;
		 	}
	 	}
  		i ++;	
	}
	prev_i = i - 1;
	$('#input-ids').val(prev_i);
	$('#code_'+prev_i).focus();
	
	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('#fdisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;
				
				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}
				

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
	} else{
		$('#input-dtotalvalue').val(gtotal*(dchargeper/100));
		$('#dchargeval').html(gtotal*(dchargeper/100));
	}

	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);

}

function gettotal_quantity(){
	total_quantity = 0;

	gsttaxamt = 0;
	vattaxamt = 0;
	fdiscount = parseInt($('#input-fdiscount').val());
	fdiscountper = parseInt($('#input-fdiscountper').val());
	ldiscount = parseInt($('#input-ldiscount').val());
	ldiscountper = parseInt($('#input-ldiscountper').val());

	$('.qty:visible').each(function( index ) {
  		if(!isNaN($(this).val()) && $(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
  			total_quantity = total_quantity + parseFloat($(this).val());
  		}

  		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && (fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == '') && is_liq1 != '' ){
			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
			gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
			gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
		}
		if(is_liq1 == 1 && (ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == '') && is_liq1 != '' ){
			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
			vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
			vattaxamt = vattaxamt + vattax1 + vattax2;
		}

  	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));

  	return total_quantity;
}
function gettotal_items(){
	total_items = 0;
	$('.qty:visible').each(function( index ) {
  		if(!isNaN($(this).val()) && $(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
  			total_items = total_items + 1;
  		}
  	});
  	return total_items;
}

function gettotal(id,is_liq) {
	if(is_liq == 0) {
		total = parseFloat($('#input-ftotal').val());
		amt1 = parseFloat($('#amt_'+id).val());
		total1 = total + amt1;
		//gst1 = parseFloat($('#food_tax_per').val()) / 100;
		//gstamt = total1 * gst1;   
		//$('#input-gst').val(gstamt.toFixed(2));
		$('#input-ftotal').val(total1);
		$('#ftotal_discount').val(total1);
	} else {
		total = parseFloat($('#input-ltotal').val());
		amt1 = parseFloat($('#amt_'+id).val());
		total1 = total + amt1;
		//vat1 = parseFloat($('#liq_tax_per').val()) / 100;
		//vatamt = total1 * vat1; 
		//$('#input-vat').val(vatamt.toFixed(2));
		$('#input-ltotal').val(total1);
		$('#ltotal_discount').val(total1);
	}

	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;
				
				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}

				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}
				
				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
	} else{
		$('#input-dtotalvalue').val(gtotal*(dchargeper/100));
		$('#dchargeval').html(gtotal*(dchargeper/100));
	}

	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
}

$(document).ready(function(){
	var editorder = '<?php echo $editorder ?>';
	if(editorder == 1){
		findtable();
	}
});

$(document).on('keydown', '.inputs', function(e) {
  	var code = (e.keyCode ? e.keyCode : e.which);
  	//alert(code);
	if (code == 39) {
		var edit = '<?php echo $editorder ?>';
		idss = $(this).attr('id');
		s_id = idss.split('_');
		var na = $(this).attr('class');
		flag = 1;
		if(na == 'inputs qty form-control' || na == 'inputs qty form-control screenquantity' || na == 'inputs rate form-control screenrate' ||  na == 'inputs lst form-control' || na == 'inputs lst form-control ui-autocomplete-input' || na == 'button inputs remove ') {
			val1=$('#code_'+s_id[1]).val();
			val2=$('#name_'+s_id[1]).val();
			if(val1 != '' && val2 != '') {
				flag = 1;
			} else {
				flag = 0;
			}
		}
		if(flag == 1){
			var index = $('.inputs').index(this);
			var na1 = $(this).attr('id');
			if(na1 == 'input-cust_id'){
			} else if(($('#orderno').val() == '0') || ($('#orderno').val() != '0' && edit == '1')) {
				if(na1 == 'input-t_number'){
					$('#input-waiterid').focus();
				}else if(na1 == 'input-waiterid'){
					$('#input-waiter').focus();	
				}else if(na1 == 'input-waiter'){
					$('#input-captainid').focus();	
				}else if(na1 == 'input-captainid'){
					$('#input-captain').focus();	
				} else if(na1 == 'input-captain'){
					$('#input-person').focus();
					$('#input-person').select();
				} else {
					bill_status = $('#bill_status').val();
					if(((s_id[0] == 'message' || s_id[0] == 'rate' || s_id[0] == 'remove') && bill_status == 0) || ((s_id[0] == 'message' || s_id[0] == 'rate' || s_id[0] == 'remove') && bill_status == 1 && edit == '1') ) {
						ids = parseInt($('#input-ids').val());
						idss = $(this).attr('id');
						s_id = idss.split('_');
						current_id = parseInt(s_id[1]);
						next_id = current_id + 1;
						var na = $(this).attr('class');
						flag = 1;
						val1=$('#code_'+next_id).val();
						val2=$('#name_'+next_id).val();
						val3=$('#qty_'+next_id).val();
						val4=$('#is_set'+next_id).val();
						//if(!$(this).closest('tr').next().length) {
						if($(this).closest("tr").is(":last-child")){
							last_tr = 1;
							flag = 1;
							i = ids + 1;
						} else {
							if(val1 == undefined && val2 == undefined) {
								flag = 1;
								last_tr = 1;
								i = ids + 1;
							} else {
								last_tr = 0;
								flag = 0;
							}
						}
						if(flag == 1) {
							html = '<tr id="re_'+i+'">';
								html += '<td class="r_'+i+'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">' + i;
									html += '<input type="hidden" value="0" name="po_datas['+i+'][kot_status]" id="kot_status'+i+'" />';
									html += '<input type="hidden" value="0" name="po_datas['+i+'][pre_qty]" id="pre_qty'+i+'" />';
									html += '<input type="hidden" value="0" name="po_datas['+i+'][kot_no]" id="kot_no'+i+'" />';
									html += '<input type="hidden" value="0" name="po_datas['+i+'][is_new]" id="is_new'+i+'" />';
									html += '<input type="hidden" value="0" name="po_datas['+i+'][is_set]" id="is_set'+i+'" />';
									html += '<input type="hidden" value="0" name="po_datas['+i+'][cancelstatus]" id="cancelstatus'+i+'" />';
								html += '</td>';
								html += '<td class="r_'+i+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas[' + i + '][code]" id="code_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas[' + i + '][subcategoryid]" id="subcategoryid_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas[' + i + '][tax1]" id="tax1_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas[' + i + '][tax2]" id="tax2_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas[' + i + '][tax1_value]" id="tax1_value_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas[' + i + '][tax2_value]" id="tax2_value_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas[' + i + '][discount_per]" id="discount_per_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas[' + i + '][discount_value]" id="discount_value_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas[' + i + '][billno]" id="billno_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" value="1" name="po_datas[' + i + '][ismodifier]" id="ismodifier_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" value="0" name="po_datas[' + i + '][nc_kot_status]" id="nc_kot_status_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" value="" name="po_datas[' + i + '][nc_kot_reason]" id="nc_kot_reason_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" value="0" name="po_datas[' + i + '][transfer_qty]" id="transfer_qty_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" value="0" name="po_datas[' + i + '][parent_id]" id="parent_id_' + i + '" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" value="0" name="po_datas[' + i + '][parent]" id="parent_' + i + '" />';
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[' + i + '][name]" id="name_' + i + '" />';
									html += '<input style="padding: 0px 1px;" type="hidden" class="inputs id form-control" name="po_datas[' + i + '][id]" id="id_' + i + '" />';
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
								<?php if($display_type == '1') { ?>
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs qty form-control" name="po_datas[' + i + '][qty]" id="qty_' + i + '" autocomplete="off"/>';
								<?php } else { ?>
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('+i+')" class="inputs qty form-control screenquantity" name="po_datas[' + i + '][qty]" id="qty_' + i + '" />';
								<?php } ?>
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
								<?php if($display_type == '1') { ?>
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
								<?php } else { ?>
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('+i+')" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
								<?php } ?>	
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[' + i + '][amt]" id="amt_' + i + '" />';
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[' + i + '][message]" id="message_' + i + '" /><input style="display:none" type="text" name="po_datas['+i+'][is_liq]"  id="is_liq_'+i+'" />';
									html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" value="0" name="po_datas[' + i + '][cancelmodifier]" id="cancelmodifier_' + i + '" />';
									tab_index ++;
								html += '</td>';
								html += '<td class="r_'+i+'" style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
									html += '<a tabindex = "'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'"><i class="fa fa-trash-o"></i></a>';
									tab_index ++;
								html += '</td>';
						 	html += '</tr>';
							$('#ordertab').append(html);
							$('#input-ids').val(i);
							//$('#code_'+i).focus();
							i++;
						} else {
							var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
					  		if (!$next.length) {
					  			$next = $('[tabIndex=1]');
					  		}
					  		$next.focus();
					  		$next.select();
						}
					} else {
						var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				  		if (!$next.length) {
				  			$next = $('[tabIndex=1]');
				  		}
				  		$next.focus();
				  		$next.select();
					}			
				}
			}
		} else {
			alert('Please Enter Code/Name');
			return false;
		}
  	} else if (code == 17) {
		$('#input-ftotal').focus();
  	} else if (code == 37) {
		var na1 = $(this).attr('id');
		if(na1 == 'input-waiter'){
			$('#input-t_number').focus();
		}else if(na1 == 'input-captain'){
			$('#input-waiter').focus();	
		} else if(na1 == 'input-person'){
			$('#input-captain').focus();
			$('#input-captain').select();
		}
		if (code == 38){
			(this).select();
		} else {
			var $previous = $('[tabIndex=' + (+this.tabIndex - 1) + ']');
	        if (!$previous.length) {
	           $previous = $('[tabIndex=1]');
	        }
	        previous_index = this.tabIndex - 1;
	        $previous.focus();
	       	$previous.select();
		}

  	} else if (code == 40) {
		
  	}

  	if(code == 27){
  		window.location.reload();
  	}
  	if(code == 45){
  		check_kot();
  	}

  	if(code == 35){ //end 
  		location_id = $('#input-location_id').val();
  		$.ajax({
			type: "GET",
			url: 'index.php?route=catalog/order/loc_dats&token=<?php echo $token; ?>&lid='+location_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				//console.log(json.loc_dat);
				if(json.loc_dat.direct_bill == 0){
					bill();
				} else{
					kot_fun();
				}
			}
  		});
  	}

  	if(code == 34){//page up
  		location_id = $('#input-location_id').val();
  		$.ajax({
			type: "GET",
			url: 'index.php?route=catalog/order/loc_dats&token=<?php echo $token; ?>&lid='+location_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				console.log(json.loc_dat);
				if(json.loc_dat.direct_bill == 0){
					kot_fun();
				} else{
					bill();
				}
			}
  		});
  	}

  	if(code == 36){
  		help();
  	}
  
  	if (code == 46) {
		$('.dropdown-menu').hide();
  	}

	
	$('.code11').autocomplete({
	  	delay: 500,
	  	source: function(request, response) {
	  		filter_rate_id = $('#input-rate_id').val();
			$.ajax({
			  	url: 'index.php?route=catalog/orderqwerty/autocomplete5&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_rate_id='+filter_rate_id,
			  	dataType: 'json',
			  	success: function(json) {   
					response($.map(json, function(item) {
				  		return {
							label: item.item_code,
							value: item.item_name,
							is_liq: item.is_liq,
							purchase_price: item.purchase_price,
							taxvalue1:item.taxvalue1,
							taxvalue2:item.taxvalue2
				  		}
					}));
			  	}
			});
	  	}, 
	  	select: function(event, ui) {
			idss = $(this).attr('id');
			s_id = idss.split('_');
			$("#kot_print_status").val('1');
			
			$('#code_'+s_id[1]).val(ui.item.label);
			$('#name_'+s_id[1]).val(ui.item.value);
			$('#qty_'+s_id[1]).val(1);
			$('#rate_'+s_id[1]).val(ui.item.purchase_price);
			$('#is_liq_'+s_id[1]).val(ui.item.is_liq);
			$('#amt_'+s_id[1]).val(ui.item.purchase_price);
			$('#message_'+s_id[1]).val();
			$('#usermsg_'+s_id[1]).val($('#message_'+s_id[1]).val());
			$('.dropdown-menu').hide();
			$('#name_'+s_id[1]).focus();
			if(ui.item.is_liq == '1'){
				$('#liq_tax_per').val(ui.item.taxvalue1);
				$('#liq_tax_per2').val(ui.item.taxvalue2);
			}
			if(ui.item.is_liq == '0'){
				$('#food_tax_per').val(ui.item.taxvalue1);
				$('#food_tax_per2').val(ui.item.taxvalue2);
			}
			gettotal(s_id[1],ui.item.is_liq);
			return false;
	  	},
	  	focus: function(event, ui) {
			return false;
	  	}
	});
	
	$('.names').autocomplete({
	  	delay: 500,
	  	source: function(request, response) {
	  		filter_rate_id = $('#input-rate_id').val();
			$.ajax({
		  		url: 'index.php?route=catalog/orderqwerty/autocompletename&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_rate_id='+filter_rate_id,
		  		dataType: 'json',
		  		success: function(json) {   
					response($.map(json, function(item) {
			  			return {
							label:item.item_name +" - "+ item.shortname +" - "+ item.purchase_price,
							name:item.item_name,
							value: item.item_code,
							is_liq: item.is_liq,
							shortname:item.shortname,
							purchase_price: item.purchase_price,
							subcategoryid: item.subcategoryid,
							taxvalue1:item.taxvalue1,
							taxvalue2:item.taxvalue2,
							modifier_group:item.modifier_group,
							modifieritems:item.modifieritems,
							totalmodifiers:item.totalmodifiers
			  			}
					}));
		  		}
			});
	  	}, 

	  	select: function(event, ui) {
			idss = $(this).attr('id');
			s_id = idss.split('_');

			id_new = s_id[1];
			var val1 = $('#code'+id_new).val();
			var val2 = $('#name'+id_new).val();
			$("#kot_print_status").val('1');

			if($('#ordertab').closest("tr").is(":last-child")){
				last_tr = 1;
				flag = 1;
				podata = parseInt(id_new) + 1;
			} else {
				if(val1 == undefined && val2 == undefined) {
					flag = 1;
					last_tr = 1;
					podata = parseInt(id_new) + 1;
				} else {
					last_tr = 0;
					flag = 0;
				}
			}

			$('#code_'+s_id[1]).val(ui.item.value);
			$('#name_'+s_id[1]).val(ui.item.name);
			$('#qty_'+s_id[1]).val(1);
			$('#rate_'+s_id[1]).val(ui.item.purchase_price);
			$('#pre_rate_'+s_id[1]).val(ui.item.purchase_price);

			$('#amt_'+s_id[1]).val(ui.item.purchase_price);
			$('#subcategoryid_'+s_id[1]).val(ui.item.subcategoryid);
			$('#is_liq_'+s_id[1]).val(ui.item.is_liq);
			$('#message_'+s_id[1]).val();
			$('#usermsg_'+s_id[1]).val($('#message_'+s_id[1]).val());
			$('#tax1_'+s_id[1]).val(ui.item.taxvalue1);
			$('#tax2_'+s_id[1]).val(ui.item.taxvalue2);
			$('.dropdown-menu').hide();
			$('#qty_'+s_id[1]).focus();
			$('#qty_'+s_id[1]).select();
			var html;
			$('#modifierbody').html('');
			$('#modifierheader').html('');
			if(ui.item.modifier_group != '0'){
				$('#ismodifier_'+s_id[1]).val(1);
				$('#parent_'+s_id[1]).val(1);
				modifierheader = '<h4>'+ui.item.item_name+'</h4>';
				$('#modifierheader').append(modifierheader);
				motr = 1;
				for(var i=0;i<ui.item.totalmodifiers;i++){
					html = '<ul style="list-style-type:none">';
						html += '<li>';
						if(ui.item.modifieritems[i].default_item == '1'){
							html +='<input type="checkbox" checked ="checked" value="'+id_new+'_'+motr+'" class="r_'+id_new+'" id="checkmodifier_'+id_new+'_'+motr+'">&nbsp;'+ui.item.modifieritems[i].modifieritem+'</input>';
							html +='<h5 style="float:right;margin-right:100px;">'+ui.item.modifieritems[i].modifierrate+'</span>';
						} else {
							html +='<input type="checkbox" value="'+id_new+'_'+motr+'" class="r_'+id_new+'" id="checkmodifier_'+id_new+'_'+motr+'">&nbsp;'+ui.item.modifieritems[i].modifieritem+'</input>';
							html +='<h5 style="float:right;margin-right:100px;">'+ui.item.modifieritems[i].modifierrate+'</span>';
						}
						html1 = '<tr id="re_'+id_new+'_'+motr+'" class="re_'+id_new+'" style="display:none" >';
							html1 += '<td id="r_'+id_new+'_'+motr+'" class="r_'+id_new+'" style="display:none; padding-top: 2px;padding-bottom: 2px;font-size: 16px;">'
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][kot_status]" class="re_'+id_new+'" id="kot_status'+id_new+'_'+motr+'"/>';
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][pre_qty]" class="re_'+id_new+'" id="pre_qty'+id_new+'_'+motr+'"/>';
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][kot_no]" class="re_'+id_new+'" id="kot_no'+id_new+'_'+motr+'"/>';
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][is_new]" class="re_'+id_new+'" id="is_new'+id_new+'_'+motr+'"/>';
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][is_set]" class="re_'+id_new+'" id="is_set'+id_new+'_'+motr+'"/>';
								html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][cancelstatus]" class="re_'+id_new+'" id="cancelstatus'+id_new+'_'+motr+'"/>';
							html1 + '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][code]" value="'+ui.item.modifieritems[i].itemcode+'" class="re_'+id_new+'" id="code_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][subcategoryid]" value="'+ui.item.modifieritems[i].subcategoryid+'" class="re_'+id_new+'" id="subcategoryid_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax1]" class="re_'+id_new+'" id="tax1_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax2]" class="re_'+id_new+'" id="tax2_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax1_value]" class="re_'+id_new+'" id="tax1_value_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][tax2_value]" class="re_'+id_new+'" id="tax2_value_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][discount_per]" class="re_'+id_new+'" id="discount_per_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][discount_value]" class="re_'+id_new+'" id="discount_value_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][billno]" class="re_'+id_new+'" id="billno_'+id_new+'_'+motr+'" />';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][parent_id]" class="re_'+id_new+'" id="parent_id_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][ismodifier]" value="0" class="re_'+id_new+'" id="ismodifier_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][nc_kot_status]" value="0" class="re_'+id_new+'" id="nckotstatus_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][nc_kot_reason]" value="" class="re_'+id_new+'" id="nckotreason_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][transfer_qty]" value="0" class="re_'+id_new+'" id="transferqty_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][referparent]" value="'+id_new+'" class="re_'+id_new+'" id="referparent_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][parent]" value="0" class="re_'+id_new+'" id="parent_'+id_new+'_'+motr+'"/>';
							html1 += '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][name]" value="'+ui.item.modifieritems[i].modifieritem+'" class="re_'+id_new+'" id="name_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][id]" class="re_'+id_new+'" id="id_'+id_new+'_'+motr+'"/>';
							html1 += '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" class="re_'+id_new+'"style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][qty]" value="1" id="qty_'+id_new+'_'+motr+'" autocomplete="off"/>';
							html1 += '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][rate]" value="'+ui.item.modifieritems[i].modifierrate+'" class="re_'+id_new+'" id="rate_'+id_new+'_'+motr+'"/>';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" name="po_datas['+id_new+'_'+motr+'][pre_rate]" value="'+ui.item.modifieritems[i].modifierrate+'" class="re_'+id_new+'" id="pre_rate_'+id_new+'_'+motr+'"/>';
								
							html1 += '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="hidden" value="'+ui.item.modifieritems[i].modifierrate+'" class="re_'+id_new+'" name="po_datas['+id_new+'_'+motr+'][amt]" id="amt_'+id_new+'_'+motr+'"/>';
							html1 += '</td>';
							html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
								html1 += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" class="re_'+id_new+'"  name="po_datas['+id_new+'_'+motr+'][message]" id="message_'+id_new+'_'+motr+'" /><input type="hidden" value="'+ui.item.modifieritems[i].is_liq+'" name="po_datas['+id_new+'_'+motr+'][is_liq]"  class="re_'+id_new+'" id="is_liq_'+id_new+'_'+motr+'"/>';
							html1 += '</td>';
							if(ui.item.modifieritems[i].default_item == '1'){
								html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
									html1 += '<input type="hidden" value="0" name="po_datas['+id_new+'_'+motr+'][cancelmodifier]" class="re_'+id_new+'" id="cancelmodifier_'+id_new+'_'+motr+'"/>';
								html1 += '</td>';
							} else{
								html1 += '<td class="r_'+id_new+'" id="r_'+id_new+'_'+motr+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
									html1 += '<input type="hidden" value="1" name="po_datas['+id_new+'_'+motr+'][cancelmodifier]" class="re_'+id_new+'" id="cancelmodifier_'+id_new+'_'+motr+'"/>';
								html1 += '</td>';
							}
					 	html1 += '</tr>';
						html +='</li>';
						motr ++;
						$('#ordertab').append(html1);
					html += '<ul>';
					$('#modifierbody').append(html);
				}
				$('#myModalModifier').modal({show:true});
				html2 = '<input type="hidden" class="re_'+id_new+'" id="modifierid_'+id_new+'" value="'+ui.item.modifier_qty+'">';
				$('#ordertab').append(html2);
			}

			if(ui.item.is_liq == '1' && ui.item.is_liq != ''){

				liqvalue = parseInt($('#liqcount').val());
  				liqcount = liqvalue + 1 ;
  				$('#liqcount').val(liqcount);

				tax1 = parseFloat(ui.item.taxvalue1);
				tax2 = parseFloat(ui.item.taxvalue2);
				amt  = parseFloat(ui.item.purchase_price);
				
				tax1_value = amt*(tax1/100);
				tax2_value = amt*(tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

			}
			if(ui.item.is_liq == '0'  && ui.item.is_liq != ''){

  				foodvalue = parseInt($('#foodcount').val());
  				foodcount = foodvalue + 1 ;
  				$('#foodcount').val(foodcount);

				tax1 = parseFloat(ui.item.taxvalue1);
				tax2 = parseFloat(ui.item.taxvalue2);
				amt  = parseFloat(ui.item.purchase_price);

				tax1_value = amt*(tax1/100);
  				tax2_value = amt*(tax2/100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
			}

			if(s_id[1] != '0'){
				$('#code_'+s_id[1]).attr('readonly', 'readonly');
				$('#name_'+s_id[1]).attr('readonly', 'readonly');
			}
			gettotal(s_id[1],ui.item.is_liq);
			return false;
	  	},
	  	focus: function(event, ui) {
			return false;
	  	}
	});

	$('.lst').autocomplete({
	  	source: function(request, response) {
			$.ajax({
		  		url: 'index.php?route=catalog/orderqwerty/autocompletekotmsg&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  		dataType: 'json',
		  		success: function(json) {   
					response($.map(json, function(item) {
			  			return {
							label: item.message,
							value: item.msg_code
			  			}
					}));
		  		}
			});
	  	}, 
	  	select: function(event, ui) {
	  		idss = $(this).attr('id');
			s_id = idss.split('_');
			$('#message_'+s_id[1]).val(ui.item.label);
			$('#usermsg_'+s_id[1]).val(ui.item.value);
	  		return false;
	  	},
	  	focus: function(event, ui) {
			return false;
	  	},
	  	minLength: 0
	}).bind('focus', function(){ $(this).autocomplete("search"); } );
		
 if (code == 13) {
 		var person = '<?php echo $PERSONS ?>';
		var waiter = '<?php echo $WAITER ?>';
		var captain ='<?php echo $CAPTAIN ?>';
		var edit = '<?php echo $editorder ?>';

 		var index = $('.inputs').index(this);
		var na1 = $(this).attr('id');
  		var name = $(this).attr('name'); 
  		var class_name = $(this).attr('class'); 
  		var id = $(this).attr('id'); 
  		event.preventDefault();

  		if(name == 'table'){
	  		findtable();
  		}
  		if(class_name == 'inputs code form-control'){
  			findcode(id);
  		}

  		if((na1 == 'input-t_number' && $('#orderno').val() == '0') || (na1 == 'input-t_number' && $('#orderno').val() != '0' && edit == '1')){
			$('#input-waiterid').focus();
		}else if(na1 == 'input-waiterid' && captain != 0){
			var filter_wcode =$('#input-waiterid').val();
			if(filter_wcode != '' && filter_wcode != undefined && filter_wcode != '0'){
				$.ajax({
					type: "POST",
					url: 'index.php?route=catalog/order/autocompletewname&token=<?php echo $token; ?>&filter_wcode=' +filter_wcode,
					dataType: 'json',
					data: {"data":"check"},
					success: function(data){
						//alert(data.waiter_id);
						//$('#code').val(data.code);
						$('#input-waiter_id').val(data.waiter_id);
						$('#waiterid').val(data.Code);
						$('#input-waiter').val(data.name);
					}
				});
			}
			$('#input-captainid').focus();
		}else if(na1 == 'input-waiter' && captain != 0){
			$('#input-captainid').focus();	
		}else if(na1 == 'input-captainid' && captain != 0){
			var filter_ccode =$('#input-captainid').val();
			//alert(filter_ccode);
			if(filter_ccode != '' && filter_ccode != undefined && filter_ccode != '0'){
				$.ajax({
					type: "POST",
					url: 'index.php?route=catalog/order/autocompletecname&token=<?php echo $token; ?>&filter_ccode=' +filter_ccode,
					dataType: 'json',
					data: {"data":"check"},
					success: function(data){
						// alert(data.name);
						//$('#code').val(data.code);
						$('#input-captain_id').val(data.waiter_id);
						$('#captainid').val(data.code);
						$('#input-captain').val(data.name);
					}
				});
			}
			if (person == 1){
				$('#input-person').focus();
				$('#input-person').select();
			} else {
				$('.code').focus();
			}
		}else if(na1 == 'input-captain' && person != 0){
			$('#input-person').focus();
			$('#input-person').select();
		} else if(na1 == 'input-person'){
			var personss = $('#input-person').val();
			//console.log(personss);
			var person_compulsary = '<?php echo $PERSONS_COMPULSARY; ?>';
			if((personss == '' || personss == undefined || personss <= '0' || personss <= 0) && person_compulsary == 1){
				alert("Minimum person should be one");
				$('#input-person').val(0);
				return false;
				
			}
			$('.code').focus();
		}else if(na1 == 'input-waiterid' && captain == 0 && person != 0){
			var filter_wcode =$('#input-waiterid').val();
			if(filter_wcode != '' && filter_wcode != undefined && filter_wcode != '0'){
				$.ajax({
					type: "POST",
					url: 'index.php?route=catalog/order/autocompletewname&token=<?php echo $token; ?>&filter_wcode=' +filter_wcode,
					dataType: 'json',
					data: {"data":"check"},
					success: function(data){
						//alert(data.waiter_id);
						//$('#code').val(data.code);
						$('#input-waiter_id').val(data.waiter_id);
						$('#waiterid').val(data.Code);
						$('#input-waiter').val(data.name);
					}
				});
			}
			$('.code').focus();
		}else if(na1 == 'input-waiter' && captain == 0 && person != 0){
			$('#input-person').focus();
			$('#input-person').select();
		}else if(na1 == 'input-captainid' && person == 0){
			$('.code').focus();	
		} else if(na1 == 'input-captain' && person == 0){
			$('.code').focus();
		}else if(na1 == 'input-waiterid' && captain == 0 && person == 0){
			var filter_wcode =$('#input-waiterid').val();
			if(filter_wcode != '' && filter_wcode != undefined && filter_wcode != '0'){
				$.ajax({
					type: "POST",
					url: 'index.php?route=catalog/order/autocompletewname&token=<?php echo $token; ?>&filter_wcode=' +filter_wcode,
					dataType: 'json',
					data: {"data":"check"},
					success: function(data){
						//alert(data.waiter_id);
						//$('#code').val(data.code);
						$('#input-waiter_id').val(data.waiter_id);
						$('#waiterid').val(data.Code);
						$('#input-waiter').val(data.name);
					}
				});
			}
			$('.code').focus();
		} else if(na1 == 'input-waiter' && captain == 0 && person == 0){
			$('.code').focus();
		}

		if(class_name == 'inputs code form-control'){
			idss = $(this).attr('id');
			s_id = idss.split('_');
			current_id = parseInt(s_id[1]);
			code_value = $('#code_'+current_id).val();
			if(code_value == ''){
				$('#name_'+s_id[1]).focus();
			}
		}

		if(class_name == 'inputs names form-control ui-autocomplete-input'){
			idss = $(this).attr('id');
			s_id = idss.split('_');
			current_id = parseInt(s_id[1]);
			name_value = $('#name_'+current_id).val();
			kot_print_status = $("#kot_print_status").val();

			if(kot_print_status == 1){
				if(name_value == ''){
					var con = confirm('Do You want to Print KOT');				
					if(con !== false){
						kot_fun();
					} else {
						
					}
				}
			}

			if(kot_print_status == 0){
				if(name_value == ''){
					var con = confirm('Do You want to Print Bill');				
					if(con != false){
						bill();
						return false;
					} else {
						
					}
				}
		}
		}



		if(class_name == 'inputs rate form-control' || class_name == 'inputs rate form-control screenquantity' || class_name == 'inputs lst form-control' || class_name == 'inputs lst form-control ui-autocomplete-input') {

			if($(this).closest("tr").is(":last-child")){
				last_tr = 1;
				flag = 1;
				i = ids + 1;
			} else {
				if(val1 == undefined && val2 == undefined) {
					flag = 1;
					last_tr = 1;
					i = ids + 1;
				} else {
					last_tr = 0;
					flag = 0;
				}
			}
			$('#code_'+i).focus();
		}

  		if(class_name == 'inputs qty form-control' || class_name == 'inputs qty form-control screenquantity' || class_name == 'inputs lst form-control' || class_name == 'inputs lst form-control ui-autocomplete-input'|| class_name == 'inputs rate form-control' || class_name == 'inputs rate form-control screenquantity') {
			ids = parseInt($('#input-ids').val());
			idss = $(this).attr('id');
			s_id = idss.split('_');
			current_id = parseInt(s_id[1]);
			next_id = current_id + 1;
			var na = $(this).attr('class');
			flag = 1;
			val1=$('#code_'+next_id).val();
			val2=$('#name_'+next_id).val();
			if($(this).closest("tr").is(":last-child")){
				last_tr = 1;
				flag = 1;
				i = ids + 1;
			} else {
				if(val1 == undefined && val2 == undefined) {
					flag = 1;
					last_tr = 1;
					i = ids + 1;
				} else {
					last_tr = 0;
					flag = 0;
				}
			}

			idss = $(this).attr('id');
			s_id = idss.split('_');
			current_id = parseInt(s_id[1]);
			rate = $('#rate_'+current_id).val();
			if (rate == '' ) {
				alert('Please ENTER Item Rate');
				return false;
			};

			if((flag == 1 && $('#orderno').val() == '0') || (flag == 1 && $('#orderno').val() != '0' && edit == '1')) {
				html = '<tr id="re_'+i+'">';
					html += '<td class="r_'+i+'" style="display:none;padding-top: 2px;padding-bottom: 2px;font-size: 16px;">' + i
						html += '<input type="hidden" value="0" name="po_datas['+i+'][kot_status]" id="kot_status'+i+'" />';
						html += '<input type="hidden" value="0" name="po_datas['+i+'][pre_qty]" id="pre_qty'+i+'" />';
						html += '<input type="hidden" value="0" name="po_datas['+i+'][kot_no]" id="kot_no'+i+'" />';
						html += '<input type="hidden" value="0" name="po_datas['+i+'][is_new]" id="is_new'+i+'" />';
						html += '<input type="hidden" value="0" name="po_datas['+i+'][is_set]" id="is_set'+i+'" />';
						html += '<input type="hidden" value="0" name="po_datas['+i+'][cancelstatus]" id="cancelstatus'+i+'" />';
					html + '</td>';
					html += '<td class="r_'+i+'" style="width:15%;padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="text" class="inputs code form-control" name="po_datas[' + i + '][code]" id="code_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs subcategoryid form-control" name="po_datas[' + i + '][subcategoryid]" id="subcategoryid_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1 form-control" name="po_datas[' + i + '][tax1]" id="tax1_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2 form-control" name="po_datas[' + i + '][tax2]" id="tax2_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax1_value form-control" name="po_datas[' + i + '][tax1_value]" id="tax1_value_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs tax2_value form-control" name="po_datas[' + i + '][tax2_value]" id="tax2_value_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_per form-control" name="po_datas[' + i + '][discount_per]" id="discount_per_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs discount_value form-control" name="po_datas[' + i + '][discount_value]" id="discount_value_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs billno form-control" name="po_datas[' + i + '][billno]" id="billno_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs ismodifier form-control" value="1" name="po_datas[' + i + '][ismodifier]" id="ismodifier_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_status form-control" value="0" name="po_datas[' + i + '][nc_kot_status]" id="nc_kot_status_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs nc_kot_reason form-control" value="" name="po_datas[' + i + '][nc_kot_reason]" id="nc_kot_reason_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs transfer_qty form-control" value="0" name="po_datas[' + i + '][transfer_qty]" id="transfer_qty_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent_id form-control" name="po_datas[' + i + '][parent_id]" value="0" id="parent_id_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs parent form-control" name="po_datas[' + i + '][parent]" value="0" id="parent_' + i + '" />';
						tab_index ++;
					html += '</td>';
					html += '<td class="r_'+i+'" style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs names form-control" name="po_datas[' + i + '][name]" id="name_' + i + '" />';
						html += '<input style="padding: 0px 1px;font-size: 16px;" type="hidden" class="inputs id form-control" name="po_datas[' + i + '][id]" id="id_' + i + '" />';
						tab_index ++;
					html += '</td>';
					html += '<td class="r_'+i+'" style="width:8%;padding-top: 2px;padding-bottom: 2px;">';
					<?php if($display_type == '1') { ?>
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs qty form-control" name="po_datas[' + i + '][qty]" id="qty_' + i + '" autocomplete="off"/>';
					<?php } else { ?>
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_qty('+i+')" class="inputs qty form-control screenquantity" name="po_datas[' + i + '][qty]" id="qty_' + i + '" />';
					<?php } ?>
						tab_index ++;
					html += '</td>';
					html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
					<?php if($display_type == '1') { ?>
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="number" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="hidden" class="inputs pre_rate form-control" name="po_datas[' + i + '][pre_rate]" id="pre_rate_' + i + '" />';

					<?php } else { ?>	
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('+i+')" class="inputs rate form-control" name="po_datas[' + i + '][rate]" id="rate_' + i + '" />';
						html += '<input tabindex = "'+tab_index+'" type="hidden" style="padding: 0px 1px;font-size: 16px;" onclick="select_rate('+i+')" class="inputs pre_rate form-control" name="po_datas[' + i + '][pre_rate]" id="pre_rate_' + i + '" />';
						
					<?php } ?>	
						tab_index ++;
					html += '</td>';
					html += '<td class="r_'+i+'" style="width:13%;padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;background-color: #f2f2f2;font-size: 16px;" type="text" class="inputs form-control" readonly="readonly" name="po_datas[' + i + '][amt]" id="amt_' + i + '" />';
					html += '</td>';
					html += '<td class="r_'+i+'" style="width:25%;padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;font-size: 16px;" type="text" class="inputs lst form-control" name="po_datas[' + i + '][message]" id="message_' + i + '" /><input style="display:none" type="text" name="po_datas['+i+'][is_liq]"  id="is_liq_'+i+'" />';
						html += '<input tabindex = "'+tab_index+'" style="padding: 0px 1px;width:100%;font-size: 16px;" type="hidden" class="inputs cancelmodifier form-control" name="po_datas[' + i + '][cancelmodifier]" value="0" id="cancelmodifier_' + i + '" />';
						tab_index ++;
					html += '</td>';
					html += '<td class="r_'+i+'" style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
						html += '<a tabindex = "'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
						tab_index ++;
					html += '</td>';
			 	html += '</tr>';
				$('#ordertab').append(html);
				$('#input-ids').val(i);
				$('#code_'+i).focus();
				i++;
			} else {
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		  		if (!$next.length) {
		  			$next = $('[tabIndex=1]');
		  		}
		  		$('.code').focus();
		  		//$next.focus();
			}
		}

		if(class_name == 'inputs message form-control' || class_name == 'inputs message form-control screenquantity' || class_name == 'inputs lst form-control' || class_name == 'inputs lst form-control ui-autocomplete-input') {
			//alert('in');
			if($(this).closest("tr").is(":last-child")){
				//alert('in if');
				last_tr = 1;
				flag = 1;
				i = ids + 1;
			} else {
				if(val1 == undefined && val2 == undefined) {
					alert('in else');
					flag = 1;
					last_tr = 1;
					i = ids + 1;
				} else {
					last_tr = 0;
					flag = 0;
				}
			}

			$('#code_'+i).focus();
		}
	}
});

$(document).keyup(function(e) {    
    if (e.keyCode == 27) { 
        window.location.reload();
    }
});

$(document).on('keyup', '#input-ldiscount, #input-ldiscountper, #input-fdiscountper, #input-fdiscount, #input-cess, #input-stax', function(e) {
	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;
				
				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);


				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
	} else{
		$('#input-dtotalvalue').val(gtotal*(dchargeper/100));
		$('#dchargeval').html(gtotal*(dchargeper/100));
	}

	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
});

$(document).on('keyup', '#input-dchargeper', function(e) {
	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(dchargeper > 99 || dchargeper < 0){
		alert("Delivery Charge cannot be given");
		$('#input-dchargeper').val(0);
		$('#input-dchargeper').val(0);
		$('#dchargeval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	if(gtotal > 0 && dcharge > 0){
		if(dcharge >= gtotal || dcharge < 0){
			alert("Delivery Charge cannot be given");
			$('#input-dchargeper').val(0);
			$('#input-dcharge').val(0);
			$('#dchargeval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(gtotal > 0 && dcharge > 0){
				ddifference = gtotal - dcharge;
				if(ddifference < 1){
					alert("Delivery Charge cannot be given");
					$('#input-dchargeper').val(0);
					$('#input-dcharge').val(0);
					$('#dchargeval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}
				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}
				
				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dchargeper != 0){
		console.log("dchargeper in");
		$('#input-dtotalvalue').val((grand_total*(dchargeper/100)).toFixed(2));
		$('#dchargeval').html((grand_total*(dchargeper/100)).toFixed(2));
		$('#input-dcharge').val(0);
	}
	
	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
});

$(document).on('keyup', '#input-dcharge', function(e) {
	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(dchargeper > 99 || dchargeper < 0){
		alert("Delivery Charge cannot be given");
		$('#input-dchargeper').val(0);
		$('#input-dchargeper').val(0);
		$('#dchargeval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	if(gtotal > 0 && dcharge > 0){
		if(dcharge >= gtotal || dcharge < 0){
			alert("Delivery Charge cannot be given");
			$('#input-dchargeper').val(0);
			$('#input-dcharge').val(0);
			$('#dchargeval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(gtotal > 0 && dcharge > 0){
				ddifference = gtotal - dcharge;
				if(ddifference < 1){
					alert("Delivery Charge cannot be given");
					$('#input-dchargeper').val(0);
					$('#input-dcharge').val(0);
					$('#dchargeval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;
				
				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;


				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		console.log("dcharge in");
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
		$('#input-dchargeper').val(0);
	}
	
	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
});

$(document).on('keyup', '.rate', function(e) {

	idss = $(this).attr('id');
	//alert(idss);
  	s_id = idss.split('_');
  	if($('#rate_'+s_id[1] ).val() <= 0){
  		alert("Rate Should not be Zero/Negative ");
  		$('#rate_'+s_id[1] ).val();
  	}

  	var existingrate = parseFloat($('#pre_rate_'+s_id[1]).val());
  	//alert(existingrate);
  	var currentrate = parseFloat($('#rate_'+s_id[1]).val());
  	//alert(currentrate);
  	var is_new = parseFloat($('#is_new'+s_id[1]).val());
  	//alert(currentrate);
	var RATE_CHANGE = '<?php echo $RATE_CHANGE ?>';
  	if(RATE_CHANGE == 1){
	  	if(currentrate < existingrate ){
	  		alert("Rate Cannot be Less Then Original Rate");
	  		parseFloat($('#rate_'+s_id[1]).val(existingrate));
	  	}
	}

  	is_liq1 = $('#is_liq_'+s_id[1] ).val();
  	kot_stat = $('#kot_status'+s_id[1] ).val();
  	if(kot_stat == 1) {
		kot_stat =0;
		$('#kot_status'+s_id[1] ).val(kot_stat);
  	}
	if(is_liq1 == 0 && is_liq1 != ''){
		amt1 = parseFloat($('#amt_'+s_id[1] ).val());
		if(amt1 == '' || amt1 == '0' || isNaN(amt1)){
			amt1 = 0;
		}
		tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
			tax1 = 0;
		}
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
			tax2 = 0;
		}
		total = parseFloat($('#input-ftotal').val());
		if(total == '' || total == '0' || isNaN(total)){
			total = 0;
		}
		total1 = total - amt1;
		rate = parseFloat($('#rate_'+s_id[1] ).val());
  		if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
  		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}
		amt = parseFloat($('#amt_'+s_id[1] ).val());
  		if(amt == '' || amt == '0' || isNaN(amt)){
			amt = 0;
		}
  		amt = rate * qty;
  		tax1_value = amt*(tax1/100);
  		tax2_value = amt*(tax2/100);
  		total2 = total1 + amt;
  		$('#input-ftotal').val(total2);
  		$('#ftotal_discount' ).val(total2);
  		
  		if(amt > 0){
  			$('#amt_'+s_id[1] ).val(amt);
  		} else {
  			$('#amt_'+s_id[1] ).val('');
  		}
  		
  		$('#tax1_value_'+s_id[1] ).val(tax1_value);
  		$('#tax2_value_'+s_id[1] ).val(tax2_value);
	} else {
		amt1 = parseFloat($('#amt_'+s_id[1] ).val());
		if(amt1 == '' || amt1 == '0' || isNaN(amt1)){
			amt1 = 0;
		}
		tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
			tax1 = 0;
		}
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
			tax2 = 0;
		}
		total = parseFloat($('#input-ltotal').val());
		if(total == '' || total == '0' || isNaN(total)){
			total = 0;
		}
		total1 = total -amt1;
	  	rate = parseFloat($('#rate_'+s_id[1] ).val());
  		if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
  		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}
  		amt = rate * qty ;
  		tax1_value = amt*(tax1/100);
  		tax2_value = amt*(tax2/100);
  		total2 = total1 + amt;
  		$('#input-ltotal' ).val(total2);
  		$('#ltotal_discount' ).val(total2);
  		if(amt > 0){
  			$('#amt_'+s_id[1] ).val(amt);
  		} else {
  			$('#amt_'+s_id[1] ).val('');
  		}
  		$('#tax1_value_'+s_id[1] ).val(tax1_value);
  		$('#tax2_value_'+s_id[1] ).val(tax2_value);
	}
	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;
				
				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = amt * (servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
	} else{
		$('#input-dtotalvalue').val(gtotal*(dchargeper/100));
		$('#dchargeval').html(gtotal*(dchargeper/100));
	}

	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
});

$(document).on('keyup', '.qty', function(e) {
  	idss = $(this).attr('id');
  	s_id = idss.split('_');
  	d_id = $('id_'+s_id[1]).val();
  	if(d_id == '' || d_id == 0){
	  	if($('#qty_'+s_id[1] ).val() <= 0){
	  		alert("Quantity Should not be Zero ");
	  		$('#qty_'+s_id[1] ).val(1);
	  	}
	}
  	is_liq1 =$('#is_liq_'+s_id[1] ).val();
  	kot_stat=$('#kot_status'+s_id[1] ).val();
  	nc_kot_status = $('#nc_kot_status_'+s_id[1]).val();
	$('.ordertab').find("tr:hidden[class='re_"+s_id[1]+"']").each (function() {
		idss = $(this).attr('id');
		idmodifier = idss.slice(3);	
		nckotstatus = $('#nckotstatus_'+s_id[1]).val();
		$('#qty_'+idmodifier).val($('#qty_'+s_id[1] ).val());
		modifieritemqty = parseFloat($('#qty_'+idmodifier).val());
		modifieritemrate = 	parseFloat($('#rate_'+idmodifier).val());
		modifieramt = 0;
		if(nckotstatus == 0){
			modifieramt = modifieritemqty * modifieritemrate;
		}
		$('#amt_'+idmodifier).val(modifieramt);
	});

  	var existingqty = $('#pre_qty'+s_id[1]).val();
  	var currentqty = parseFloat($('#qty_'+s_id[1]).val());
  	var is_new = parseFloat($('#is_new'+s_id[1]).val());

  	if(currentqty > existingqty && is_new == '1'){
  		alert("Quantity Cannot be Greater than Ordered Quantity");
  		parseFloat($('#qty_'+s_id[1]).val(existingqty));
  	}

  	if( currentqty > 10 ){
  		if(confirm("Do you really want to add quantity more than 10")){
  		} else{
  			parseFloat($('#qty_'+s_id[1]).val(1));
  		}
  	}
  	 $("#kot_print_status").val(1);
  	
  	itemid = $('#id_'+s_id[1]).val();
 	if(itemid == ''){
 		if(currentqty <= 0){
	  		alert("Quantity Cannot be Zero");
	  		parseFloat($('#qty_'+s_id[1]).val(1));
  		}
 	} else{
 		if(currentqty < 0){
	  		alert("Quantity Cannot be Negative");
	  		parseFloat($('#qty_'+s_id[1]).val(1));
  		}
 	}

  	if(kot_stat == 1) {
		kot_stat = 0;
		$('#kot_status'+s_id[1]).val(kot_stat);
  	}

  	if(is_liq1 == 0 && is_liq1 != ''){
		amt1 = parseFloat($('#amt_'+s_id[1]).val());
		if(amt1 == '' || amt1 == '0' || isNaN(amt1)){
			amt1 = 0;
		}
		tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
			tax1 = 0;
		}
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
			tax2 = 0;
		}
		total = parseFloat($('#input-ftotal').val());
		if(total == '' || total == '0' || isNaN(total)){
			total = 0;
		}
		total1 = total - amt1;
  		rate = parseFloat($('#rate_'+s_id[1]).val());
  		if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}
		amt = 0;
  		if(nc_kot_status == 0){
  			amt = rate * qty ;
  		}
  		tax1_value = amt*(tax1/100);
  		tax2_value = amt*(tax2/100);
  		total2 = total1 + amt;
   		//gst1 = parseFloat($('#food_tax_per').val()) / 100;
   		//gstamt = total2 * gst1; 
  		//$('#input-gst' ).val(gstamt.toFixed(2));
  		$('#input-ftotal').val(total2);
  		$('#ftotal_discount').val(total2);
  		if(amt > 0){
  			$('#amt_'+s_id[1]).val(amt);
  		} else {
  			$('#amt_'+s_id[1]).val(0);
  		}
  		$('#tax1_value_'+s_id[1]).val(tax1_value);
  		$('#tax2_value_'+s_id[1]).val(tax2_value);
	} else {
		amt1 = parseFloat($('#amt_'+s_id[1] ).val());
		if(amt1 == '' || amt1 == '0' || isNaN(amt1)){
			amt1 = 0;
		}
		tax1 = parseFloat($('#tax1_'+s_id[1]).val());
		if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
			tax1 = 0;
		}
		tax2 = parseFloat($('#tax2_'+s_id[1]).val());
		if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
			tax2 = 0;
		}
		total = parseFloat($('#input-ltotal').val());
		if(total == '' || total == '0' || isNaN(total)){
			total = 0;
		}
		total1 = total -amt1;
  		rate = parseFloat($('#rate_'+s_id[1] ).val());
  		if(rate == '' || rate == '0' || isNaN(rate)){
			rate = 0;
		}
  		qty = parseFloat($('#qty_'+s_id[1] ).val());
		if(qty == '' || qty == '0' || isNaN(qty)){
			qty = 0;
		}
		amt = 0;
  		if(nc_kot_status == 0){
  			amt = rate * qty ;
  		}
  		tax1_value = amt*(tax1/100);
  		tax2_value = amt*(tax2/100);
  		total2 = total1 + amt;
  		//vat1 = parseFloat($('#liq_tax_per').val()) / 100;
  		//vatamt = total2 * vat1; 
		//$('#input-vat' ).val(vatamt.toFixed(2));
  		$('#input-ltotal').val(total2);
  		$('#ltotal_discount').val(total2);
  		
  		if(amt > 0){
  			$('#amt_'+s_id[1]).val(amt);
  		} else {
  			$('#amt_'+s_id[1]).val(0);
  		}
  		$('#tax1_value_'+s_id[1]).val(tax1_value);
  		$('#tax2_value_'+s_id[1]).val(tax2_value);
	}

	ftotal = parseFloat($('#input-ftotal').val());
	ftotal_discount = parseFloat($('#ftotal_discount').val());
	ftax_per = parseFloat($('#food_tax_per').val());
	ftax_per2 = parseFloat($('#food_tax_per2').val());
	ltotal = parseFloat($('#input-ltotal').val());
	ltotal_discount = parseFloat($('#ltotal_discount').val());
	ltax_per = parseFloat($('#liq_tax_per').val());
	ltax_per2 = parseFloat($('#liq_tax_per2').val());
	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	cess = parseFloat($('#input-cess').val());
	staxfood = parseFloat($('#staxfood').val());
	staxliq = parseFloat($('#staxliq').val());
	stax = parseFloat($('#input-stax').val());
	ftotalvalue = parseFloat($('#input-ftotalvalue').val());
	fdiscountper = parseFloat($('#input-fdiscountper').val());
	fdiscount = parseFloat($('#input-fdiscount').val());
	ldiscount = parseFloat($('#input-ldiscount').val());
	ldiscountper = parseFloat($('#input-ldiscountper').val());
	ltotalvalue = parseFloat($('#input-ltotalvalue').val());
	dcharge = parseFloat($('#input-dcharge').val());
	dchargeper = parseFloat($('#input-dchargeper').val());
	dtotalvalue = parseFloat($('#input-dtotalvalue').val());
	gtotal = parseFloat($('#input-grand_total').val());
	
	if(ftotal == '' || ftotal == '0' || isNaN(ftotal)){
		ftotal = 0;
	}

	if(ftotal_discount == '' || ftotal_discount == '0' || isNaN(ftotal_discount)){
		ftotal_discount = 0;
	}

	if(ftax_per == '' || ftax_per == '0' || isNaN(ftax_per)){
		ftax_per = 0;
	}

	if(ftax_per2 == '' || ftax_per2 == '0' || isNaN(ftax_per2)){
		ftax_per2 = 0;
	}

	if(ltotal == '' || ltotal == '0' || isNaN(ltotal)){
		ltotal = 0;
	}

	if(ltotal_discount == '' || ltotal_discount == '0' || isNaN(ltotal_discount)){
		ltotal_discount = 0;
	}

	if(ltax_per == '' || ltax_per == '0' || isNaN(ltax_per)){
		ltax_per = 0;
	}

	if(ltax_per2 == '' || ltax_per2 == '0' || isNaN(ltax_per2)){
		ltax_per2 = 0;
	}

	if(gst == '' || gst == '0' || isNaN(gst)){
		gst = 0;
	}

	if(vat == '' || vat == '0' || isNaN(vat)){
		vat = 0;
	}

	if(cess == '' || cess == '0' || isNaN(cess)){
		cess = 0;
	}

	if(staxfood == '' || staxfood == '0' || isNaN(staxfood)){
		staxfood = 0;
	}

	if(staxliq == '' || staxliq == '0' || isNaN(staxliq)){
		staxliq = 0;
	}

	if(stax == '' || stax == '0' || isNaN(stax)){
		stax = 0;
	}

	if(ftotalvalue == '' || ftotalvalue == '0' || isNaN(ftotalvalue)){
		ftotalvalue = 0;
	}

	if(fdiscount == '' || fdiscount == '0' || isNaN(fdiscount)){
		fdiscount = 0;
	}

	if(fdiscountper == '' || fdiscountper == '0' || isNaN(fdiscountper)){
		fdiscountper = 0;
	}

	if(ldiscount == '' || ldiscount == '0' || isNaN(ldiscount)){
		ldiscount = 0;
	}

	if(ldiscountper == '' || ldiscountper == '0' || isNaN(ldiscountper)){
		ldiscountper = 0;
	}

	if(ltotalvalue == '' || ltotalvalue == '0' || isNaN(ltotalvalue)){
		ltotalvalue = 0;
	}

	if(dcharge == '' || dcharge == '0' || isNaN(dcharge)){
		dcharge = 0;
	}

	if(dchargeper == '' || dchargeper == '0' || isNaN(dchargeper)){
		dchargeper = 0;
	}

	if(dtotalvalue == '' || dtotalvalue == '0' || isNaN(dtotalvalue)){
		dtotalvalue = 0;
	}

	if(gtotal == '' || gtotal == '0' || isNaN(gtotal)){
		gtotal = 0;
	}

	if(fdiscountper > 99 || fdiscountper < 0){
		alert("Food Discount cannot be given");
		$('#input-fdiscountper').val(0);
		$('#fdisval').html(0);
		$('#input-fdiscount').val(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ldiscountper > 99 || ldiscountper < 0){
		alert("Liquor Discount cannot be given");
		$('#input-ldiscountper').val(0);
		$('#input-ldiscount').val(0);
		$('#ldisval').html(0);
		$('.qty').trigger("change");
		return false;
	}

	if(ftotal > 0 && fdiscount > 0){
		if(fdiscount >= ftotal || fdiscount < 0){
			alert("Food Discount cannot be given");
			$('#input-fdiscountper').val(0);
			$('#fdisval').html(0);
			$('#input-fdiscount').val(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ftotal > 0 && fdiscount > 0){
				fdifference = ftotal - fdiscount;
				if(fdifference < 1){
					alert("Food Discount cannot be given");
					$('#input-fdiscountper').val(0);
					$('#fdisval').html(0);
					$('#input-fdiscount').val(0);
					$('.qty').trigger("change");
					return false;
				}
			}	
		}
	}

	if(ltotal > 0 && ldiscount > 0){
		if(ldiscount >= ltotal || ldiscount < 0){
			alert("Liquor Discount cannot be given");
			$('#input-ldiscountper').val(0);
			$('#input-ldiscount').val(0);
			$('#ldisval').html(0);
			$('.qty').trigger("change");
			return false;
		} else {
			if(ltotal > 0 && ldiscount > 0){
				ldifference = ltotal - ldiscount;
				if(ldifference < 1){
					alert("Liquor Discount cannot be given");
					$('#input-ldiscountper').val(0);
					$('#input-ldiscount').val(0);
					$('#ldisval').html(0);
					$('.qty').trigger("change");
					return false;
				}
			}
		}
	}

	ftotaldiscountper = 0;
	ltotaldiscountper = 0;
	ftotal_discount = 0;
	ltotal_discount = 0;
	if(ftotal != '0'){
		$('#ftotal_discount').val(ftotal);
		ftotal_discount = ftotal;
		if(fdiscount != '0' && fdiscount != ''){
			$('#input-fdiscountper').val(0);
			fdiscountper = 0;
			ftotal_discount = ftotal - fdiscount;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-fdiscount').val(0);
			fdiscount = 0;
		}
		if(fdiscountper != '0' && fdiscountper != ''){
			ftotaldiscountper = ((fdiscountper/100)*ftotal);
			$('#input-ftotalvalue').val(ftotaldiscountper);
			$("#fdisval").html(ftotaldiscountper);
			ftotal_discount = ftotal - ftotaldiscountper;
			$('#ftotal_discount').val(ftotal_discount);
		} else {
			$('#input-ftotalvalue').val(0);
			$("#fdisval").html('');
		}
	} else {
		fdiscountper = 0;
		fdiscountper = 0;
	}

	if(ltotal != '0'){
		$('ltotal_discount').val(ltotal);
		ltotal_discount = ltotal;
		if(ldiscount != '0' && ldiscount != ''){
			$('#input-ldiscountper').val(0);
			ldiscountper = 0;
			ltotal_discount = ltotal - ldiscount;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ldiscount').val(0);
			ldiscount = 0;
		}
		if(ldiscountper != '0' && ldiscountper != ''){
			ltotaldiscountper = ((ldiscountper/100)*ltotal);
			$('#input-ltotalvalue').val(ltotaldiscountper);
			$("#ldisval").html(ltotaldiscountper);
			ltotal_discount = ltotal - ltotaldiscountper;
			$('#ltotal_discount').val(ltotal_discount);
		} else {
			$('#input-ltotalvalue').val(0);
			$("#ldisval").html('');
		}
	} else {
		ldiscountper = 0;
		ldiscount = 0;
	}

	var servicechargefood = '<?php echo $SERVICE_CHARGE_FOOD ?>';
	var servicechargeliq = '<?php echo $SERVICE_CHARGE_LIQ ?>';
	var inclusive = '<?php echo $INCLUSIVE ?>';
	disamountfood = 0;
	disamountliq = 0;
	totaldiscountamount = 0;
	gsttaxamt = 0;
	vattaxamt = 0;
	finaldisfood = 0;
	finaldisliq = 0;
	countfood = parseInt($('#foodcount').val());
	countliq = parseInt($('#liqcount').val());
	staxfood = 0;
	staxliq = 0;
	stax = 0;
	$('.qty:visible').each(function( index ) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		is_liq1 = $('#is_liq_'+s_id[1] ).val();

		if(is_liq1 == 0 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(fdiscountper > 0 || fdiscountper != ''){
				//totalfood = fdiscountper;
				//discount_value = amt*(totalfood/100);
				discount_value = amt*(fdiscountper/100);
				$('#discount_per_'+s_id[1]).val(fdiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				staxfood = staxfood + staxfoods;
				stax = stax + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;

			} else if((fdiscount > 0 || fdiscount != '') && (is_liq1 != '')){
				discount_per = (fdiscount/ftotal)*100;
				//discount_value = totalfood/countfood;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1) {
					staxfoods = afterdiscountamt*(servicechargefood/100);
				} else {
					staxfoods = 0;
				}
				
				tax1_value = (afterdiscountamt + staxfoods) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1]).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1]).val(tax2_value.toFixed(2));

	  			gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountfood = disamountfood + afterdiscountamt;
				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
				finaldisfood = finaldisfood + afterdiscount;
			} else if(fdiscount == 0 || fdiscount == '' || fdiscountper == 0 || fdiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				staxfoods = amt * (servicechargefood/100);
				
				tax1_value = (amt + staxfoods) * (tax1/100);
	  			tax2_value = (amt + staxfoods) * (tax2/100);

				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

  				gsttax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				gsttax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				$('#discount_value_'+s_id[1]).val(0);

				stax = stax + staxfoods;
				staxfood = staxfood + staxfoods;
				gsttaxamt = gsttaxamt + gsttax1 + gsttax2;
			}
		}

		if(is_liq1 == 1 && is_liq1 != ''){
			tax1 = parseFloat($('#tax1_'+s_id[1]).val());
			tax2 = parseFloat($('#tax2_'+s_id[1]).val());
			rate = parseFloat($('#rate_'+s_id[1] ).val());
  			qty = parseFloat($('#qty_'+s_id[1] ).val());
  			amt = parseFloat($('#amt_'+s_id[1] ).val());
  			//amt = rate * qty;

  			if(tax1 == '' || tax1 == '0' || isNaN(tax1)){
				tax1 = 0;
			}

			if(tax2 == '' || tax2 == '0' || isNaN(tax2)){
				tax2 = 0;
			}

			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}

			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}

			if(amt == '' || amt == '0' || isNaN(amt)){
				amt = 0;
			}

			if(ldiscountper > 0 || ldiscountper != ''){

				discount_value = amt*(ldiscountper/100);
				$('#discount_per_'+s_id[1]).val(ldiscountper);
				$('#discount_value_'+s_id[1]).val(discount_value);

				afterdiscount = parseFloat($('#discount_value_'+s_id[1]).val());
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1 / 100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2 / 100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());
				
				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;

			} else if(ldiscount > 0 || ldiscount != ''){
				discount_per = (ldiscount/ltotal)*100;
				//discount_value = totalliq/countliq;
				$('#discount_per_'+s_id[1]).val(discount_per.toFixed(2));

				discountvalueper = parseFloat($('#discount_per_'+s_id[1]).val());
				afterdiscount = amt*(discountvalueper/100);
				$('#discount_value_'+s_id[1]).val(afterdiscount.toFixed(2));
				afterdiscountamt = amt - afterdiscount;

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = afterdiscountamt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (afterdiscountamt + staxliqs) * (tax1/100);
	  			tax2_value = (afterdiscountamt + staxliqs) * (tax2/100);

	  			$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
	  			$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));

	  			vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				disamountliq = disamountliq + afterdiscountamt;
				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
				finaldisliq = finaldisliq + afterdiscount;
			} else if(ldiscount == 0 || ldiscount == '' || ldiscountper == 0 || ldiscountper == ''){
				$('#discount_per_'+s_id[1]).val(0);
				$('#discount_value_'+s_id[1]).val(0);

				service_charge = $('#input-service_charge').val();
				if (service_charge == 1 ) {
					staxliqs = amt * (servicechargeliq / 100);
				} else {
					staxliqs = 0;
				}

				tax1_value = (amt + staxliqs) * (tax1 / 100);
  				tax2_value = (amt + staxliqs) * (tax2 / 100);

  				$('#tax1_value_'+s_id[1] ).val(tax1_value.toFixed(2));
  				$('#tax2_value_'+s_id[1] ).val(tax2_value.toFixed(2));
  				$('#discount_value_'+s_id[1]).val(0);

  				vattax1 = parseFloat($('#tax1_value_'+s_id[1] ).val());
				vattax2 = parseFloat($('#tax2_value_'+s_id[1] ).val());

				stax = stax + staxliqs;
				staxliq = staxliq + staxliqs;
				vattaxamt = vattaxamt + vattax1 + vattax2;
			}
		}
	});

	$('#input-gst').val(gsttaxamt.toFixed(2));
	$('#input-vat').val(vattaxamt.toFixed(2));
	$('#fdisval').html(finaldisfood.toFixed(2));
	$('#ldisval').html(finaldisliq.toFixed(2));
	if($('#input-fdiscount').val() != '' || $('#input-fdiscount').val() != 0){
		$('#input-ftotalvalue').val(finaldisfood.toFixed(2));
	} else{
		$('#input-fdiscount').val(finaldisfood.toFixed(2));
	}

	if($('#input-ldiscount').val() != '' || $('#input-ldiscount').val() != 0){
		$('#input-ltotalvalue').val(finaldisliq.toFixed(2));
	} else{
		$('#input-ldiscount').val(finaldisliq.toFixed(2));
	}

	if(disamountfood != 0.00 || disamountfood != 0 || disamountfood != '' || disamountliq != 0.00 || disamountliq != 0 || disamountliq != ''){
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	} else {
		staxamount = stax;
		$('#staxfood').val(staxfood.toFixed(2));
		$('#staxliq').val(staxliq.toFixed(2));
		$('#input-stax').val(staxamount.toFixed(2));
		$('#ftotal_discount').val(disamountfood.toFixed(2));
		$('#ltotal_discount').val(disamountliq.toFixed(2));
	}

	gst = parseFloat($('#input-gst').val());
	vat = parseFloat($('#input-vat').val());
	stax = parseFloat($('#input-stax').val());
	if(inclusive == '1'){
		grand_total = (ftotal + ltotal + cess + stax) - (finaldisfood + finaldisliq);
	} else{
		grand_total = (ftotal + ltotal + vat + gst + cess + stax) - (finaldisfood + finaldisliq);
	}
	
	if(!isNaN(grand_total) && grand_total != ''){
		grand_total = grand_total.toFixed(2);
	}

	if(grand_total > 0){
		var roundtotals = grand_total.split('.');
		if(roundtotals[1] != undefined){
			roundtotal = roundtotals[1];
			if(roundtotal != '00'){
				$('#input-roundtotal').val((100 - roundtotal)/100);
			}
		}
	}

	advanceAmount = parseFloat($('#advance_amount').val());
	$('#input-grand_total').val(grand_total - advanceAmount);
	$('.grand_total_span').html(grand_total - advanceAmount);
	$('#oldgrand').val(grand_total);

	if(dcharge != 0){
		$('#input-dtotalvalue').val(dcharge);
		$('#dchargeval').html(dcharge);
	} else{
		$('#input-dtotalvalue').val(grand_total*(dchargeper/100));
		$('#dchargeval').html(grand_total*(dchargeper/100));
	}

	total_quantity = gettotal_quantity();
  	total_items = gettotal_items();
  	$('#input-total_items').val(total_items);
	$('.total_items_span').html(total_items);
	$('#input-item_quantity').val(total_quantity);
	$('.item_quantity_span').html(total_quantity);
});

$(document).on('select', '.qty', function(e) {
	cancel_permission = '<?php echo $cancel_kot_permission ?>';
  	idss = $(this).attr('id');
  	s_id = idss.split('_');
  	item_id = $('#id_'+s_id[1]).val();
  	if(cancel_permission == 0 && (item_id != '' || item_id != 0)){
  		alert("You do not have permission");
  		$('#qty_'+s_id[1]).prop('readonly', true);
  		return false;
  	}
});

$(document).on('change', '#input-person', function(e) {
	person = $('#input-person').val();
	var person_compulsary = '<?php echo $PERSONS_COMPULSARY; ?>';
	/*
	if(person <= 0 && person_compulsary == 1){
		alert("Minimum person should be one");
		$('#input-person').val(1);
	}
	*/
});

function back_fun(){
	page_no = $('#input-page_no').val();
	if(page_no == 1){

	} else if(page_no == 2) {
		table_id = $('#input-location_id').val();
		table_name = $('#input-location').val();
		$("#back-input").attr("onclick","table("+table_id+", "+table_name+")");
	}
}
</script>
<?php echo $footer; ?>