<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
    	
	    <div class="container-fluid">
		    <div class="pull-right">
		    	 <?php if($show_item_send == 0) { ?>
		    	<a onclick="$('#mynewhorseModal').modal('toggle');" data-toggle="tooltip" title="<?php echo 'Sync Menu Items'; ?>" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i>Sync Menu Item</a>
		    	<?php } else { ?>
		    		<a  data-toggle="tooltip" title="<?php echo 'Sync Menu Items'; ?>" class="btn btn-primary">Sync Menu Item Button Disbled Threee Attemt</a>
		    	<?php } ?>
		    	<a  href="<?php echo $item_onoff; ?>" data-toggle="tooltip" title="<?php echo ' Items ON/OFF'; ?>" class="btn btn-primary" style = "display: none;"> Item ON/OFF</a>
		    	<a  href="<?php echo $urban_piper_item; ?>" data-toggle="tooltip" title="<?php echo ' Online Item'; ?>" class="btn btn-primary"> Online Item</a>
		    	<a href="<?php echo $generate_short_name; ?>" data-toggle="tooltip" title="<?php echo 'Generate Short Name'; ?>" class="btn btn-primary">Generate Short Name</a>
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		       	<button type="button" id="button-export" class="btn btn-primary pull-right"><i class=""></i> <?php echo 'Export'; ?></button>
		    </div>
		    <div class="pull-right">
		    	<form action="<?php echo $import_data_online_func; ?>" method="post" enctype="multipart/form-data" id="import_data_online_func">
          			<input type="file" name="import_data" id="import_data" accept="csv" />
          			<a onclick="$('#import_data_online_func').submit();" class="button"><?php echo 'Upload and Import Item'; ?></a>
        		</form>
		    </div>

		    <div class="pull-right">
		    	<form action="<?php echo $restore; ?>" method="post" enctype="multipart/form-data" id="restore">
          			<input type="file" name="import_data" id="import_data" accept="csv" />
          			<a onclick="$('#restore').submit();" class="button"><?php echo 'Upload and Import Item'; ?></a>
        		</form>
		    </div>

		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
    <div id="mynewhorseModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="">
    	<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
		<div class="modal-dialog" style="width: 50%;height: 500px;">
		<!-- Modal content-->
			<div class="modal-content" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Do you want to delete All previous Records(Please tick if Yes)</h4>
				</div>
				<div class="modal-body">
					
						<input type="checkbox" class = "flush_items" name="" value="false"  /> &nbsp;Flush Items <br>
						<input type="checkbox" class = "flush_categories" name="" value="false"  /> &nbsp;Flush Categories <br>
						<input type="checkbox" class = "flush_taxes" name="" value="false"  /> &nbsp;Flush Taxes <br>
						<input type="checkbox" class = "flush_charges" name="" value="false "  /> &nbsp;Flush charges  <br>
					
				</div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="finalsync_menu()" >Send</button>
			  </div>
			</div>
		</div>
	</div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($errors) { ?>
		<div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $errors; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			       <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Item Name'; ?></label>
				          <input type="text" name="filter_item_name" value="<?php echo $filter_item_name; ?>" placeholder="<?php echo 'Item Name'; ?>" id="input-filter_item_name" class="form-control" />
				          <input type="hidden" name="filter_item_id" value="<?php echo $filter_item_id; ?>" id="input-filter_item_id" class="form-control" />
				         
				        </div>
			       </div>
			       <div class="col-sm-4">
					   <div class="form-group">
					       <label class="control-label" for="input-location_name"><?php echo 'Item Code'; ?></label>
					       <input type="text" name="filter_item_code" value="<?php echo $filter_item_code; ?>" placeholder="<?php echo 'Item Code'; ?>" id="input-filter_item_code" class="form-control" />
					    </div>
			    	</div>
			    	<div class="col-sm-4">
					   <div class="form-group">
					    	<label class="control-label" for="input-location_name"><?php echo 'Barcode'; ?></label>
					    	<input type="text" name="filter_barcode" value="<?php echo $filter_barcode; ?>" placeholder="<?php echo 'Barcode'; ?>" id="input-filter_barcode" class="form-control" />
					    	<!-- <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button> -->
					    </div>
			    	</div>
			    	
			    		
	    			 <div class="col-sm-4">
	    			 	<label class="control-label" for="input-location_name"><?php echo 'Api Item Status'; ?></label>
	                    <select name="filter_status" id="filter_status" class="form-control">
	                        <?php foreach ($status as $key => $tvalue) { ?>
	                            <?php if($key == $filter_status){ ?>
	                              <option value="<?php echo $key ?>" selected="selected"><?php echo $tvalue?></option>
	                            <?php } else { ?>
	                              <option value="<?php echo $key ?>"><?php echo $tvalue?></option>
	                            <?php } ?>
	                        <?php } ?>    
	                    </select>
          			</div>
	                    <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			    		
			    	
			    	
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  <td class="text-left"><?php if ($sort == 'item_code') { ?>
					<a href="<?php echo $sort_item_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo " Item Code"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_item_code; ?>"><?php echo " Item Code"; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'item_name') { ?>
					<a href="<?php echo $sort_item_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Item Name'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_item_name; ?>"><?php echo 'Item Name'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'department') { ?>
					<a href="<?php echo $sort_department; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Department'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_department; ?>"><?php echo 'Department'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'item_category') { ?>
					<a href="<?php echo $sort_item_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Category'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_item_category; ?>"><?php echo 'Category'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'barcode') { ?>
					<a href="<?php echo $sort_barcode; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Bar Code'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_barcode; ?>"><?php echo 'Barcode'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_1') { ?>
					<a href="<?php echo $sort_rate_1; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 1'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_1; ?>"><?php echo 'Rate 1'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_2') { ?>
					<a href="<?php echo $sort_rate_2; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 2'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_2; ?>"><?php echo 'Rate 2'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_3') { ?>
					<a href="<?php echo $sort_rate_3; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 3'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_3; ?>"><?php echo 'Rate 3'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_4') { ?>
					<a href="<?php echo $sort_rate_4; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 4'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_4; ?>"><?php echo 'Rate 4'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_5') { ?>
					<a href="<?php echo $sort_rate_5; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 5'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_5; ?>"><?php echo 'Rate 5'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php if ($sort == 'rate_6') { ?>
					<a href="<?php echo $sort_rate_6; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Rate 6'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_rate_6; ?>"><?php echo 'Rate 6'; ?></a>
					<?php } ?></td>
				  <td class="text-right"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
				<?php foreach ($items as $item) { ?>
				<tr>
				  <td class="text-center"><?php if (in_array($item['item_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" />
					<?php } ?></td>
				  <td class="text-left"><?php echo $item['item_code']; ?></td>
				  <td class="text-right"><?php echo $item['item_name']; ?></td>
				  <td class="text-right"><?php echo $item['department']; ?></td>
				  <td class="text-right"><?php echo $item['item_category']; ?></td>
				  <td class="text-right"><?php echo $item['barcode']; ?></td>
				  <td class="text-right"><?php echo $item['rate_1']; ?></td>
				  <td class="text-right"><?php echo $item['rate_2']; ?></td>
				  <td class="text-right"><?php echo $item['rate_3']; ?></td>
				  <td class="text-right"><?php echo $item['rate_4']; ?></td>
				  <td class="text-right"><?php echo $item['rate_4']; ?></td>
				  <td class="text-right"><?php echo $item['rate_6']; ?></td>
				  <td class="text-right"><a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
				  <?php if($item['item_onoff_status'] == '0' ){ ?>
				  	<a href="<?php echo $item['item_onoff_update']; ?>" data-toggle="tooltip" title="<?php echo "Item ON/OFF" ?>" class="btn btn-danger" style= "display: none;"><i class="fa fa-toggle-on"></i></a>
				<?php } else { ?>
				  	<a href="<?php echo $item['item_onoff_update']; ?>" data-toggle="tooltip" title="<?php echo "Item ON/OFF" ?>" class="btn btn-success" style= "display: none;"><i  class="fa fa-toggle-on"></i></a>
				<?php }?>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/item&token=<?php echo $token; ?>';

  	var filter_item_name = $('input[name=\'filter_item_name\']').val();
  	if (filter_item_name) {
		var filter_item_id = $('input[name=\'filter_item_id\']').val();
		if (filter_item_id) {
	  		url += '&filter_item_id=' + encodeURIComponent(filter_item_id);
	  		url += '&filter_item_name=' + encodeURIComponent(filter_item_name);
	  	}
  	}

  	var filter_item_code = $('input[name=\'filter_item_code\']').val();
  	if (filter_item_code) {
		url += '&filter_item_code=' + encodeURIComponent(filter_item_code);
	}

	var filter_barcode = $('input[name=\'filter_barcode\']').val();
	if (filter_barcode) {
		url += '&filter_barcode=' + encodeURIComponent(filter_barcode);
	}
	 var filter_status = $('select[name=\'filter_status\']').val();
	  if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
      }

  	
  	location = url;
});

$('input[name=\'filter_item_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocomplete&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_name,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_name\']').val(ui.item.label);
    $('input[name=\'filter_item_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_item_code\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocompletecode&token=<?php echo $token; ?>&filter_item_code=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_item_code\']').val(ui.item.label);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});


$('input[name=\'filter_barcode\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/item/autocompletebarcode&token=<?php echo $token; ?>&filter_barcode=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.item_code,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_barcode\']').val(ui.item.label);
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$(".flush_items").on('change', function() {
  if ($(this).is(':checked')) {
    $(this).attr('value', 'true');
  } else {
    $(this).attr('value', 'false');
  }
});
$(".flush_categories").on('change', function() {
  if ($(this).is(':checked')) {
    $(this).attr('value', 'true');
  } else {
    $(this).attr('value', 'false');
  }
});
$(".flush_taxes").on('change', function() {
  if ($(this).is(':checked')) {
    $(this).attr('value', 'true');
  } else {
    $(this).attr('value', 'false');
  }
});
$(".flush_charges").on('change', function() {
  if ($(this).is(':checked')) {
    $(this).attr('value', 'true');
  } else {
    $(this).attr('value', 'false');
  }
});


	function finalsync_menu(){
		//reason = $('#reason').val();
		//stores = $('#stores').val();
		// $(document).ajaxSend(function() {
		// 	$("#overlay").fadeIn(200);　
		// });
		var flush_items = $('.flush_items').val();
		var flush_categories = $('.flush_categories').val();
		var flush_taxes = $('.flush_taxes').val();
		var flush_charges = $('.flush_charges').val();

		//if(reason != ''){
			$.ajax({
				type: "POST",
			  	url: 'index.php?route=catalog/item/sync_menu&token=<?php echo $token; ?>',
			  	 data : {
            	flush_items : flush_items,
			 	flush_categories : flush_categories,
            	flush_taxes : flush_taxes,
            	flush_charges : flush_charges,

            },
			  	dataType: 'json',
			  	beforeSend: function() {
		  			$("#overlay").fadeIn(200);　
				},
			  	complete: function() {
		  			$("#overlay").fadeOut(50);
				},  
			  	success: function(json) {
			  		$('#mynewhorseModal').modal('hide');
			  		if(json.error){
			  			alert(json.error);
			  		}
			  		window.location.reload(); 
			  	},
			  	error: function(xhr, ajaxOptions, thrownError) {
			  		
		  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		  			window.location.reload(); 
				}
			});
		// }else {
		// 	alert("Please Enter Reason");
		// }
		
		
		
	}


</script></div>
<script type="text/javascript">
$('#button-export').on('click', function() {
  var url = 'index.php?route=catalog/item/export&token=<?php echo $token; ?>';
  location = url;
});
</script>
<style type="text/css">
	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}
</style>
<?php echo $footer; ?>