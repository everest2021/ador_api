<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1>Log Report</h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					<?php /*<div class="table-responsive col-sm-6 col-sm-offset-3">
					  <table class="table table-bordered table-hover" style="text-align: center;">
						  	<tr>
						  		<th style="text-align: center;">Bill Number</th>
						  		<th style="text-align: center;">Bill Amount</th>
						  	</tr>
						  	<?php $total = 0; ?>
						  	<?php foreach ($billdatas as $data) {?>
						  		<tr>
							  		<td><?php echo $data['order_no'] ?></td>
							  		<td><?php echo $data['grand_total'] ?></td>
							  		<?php $total = $total + $data['grand_total']  ?>
						  		</tr>
						  	<?php } ?>
						  	<?php if($total != '0') { ?>
						  		<tr>
							  		<td><b>Total</b></td>
							  		<td><b><?php echo $total ?></b></td>
						  		</tr>
						  	<?php }?>
					  	</table>
				 	</div> */?>
					<div class="col-sm-6 col-sm-offset-3">
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Log Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Id</th>
							<th style="text-align: center;">Date</th>
							<th style="text-align: center;">Time</th>
							<th style="text-align: center;">Username</th>
							<th style="text-align: center;">IPaddress</th>
							<th style="text-align: center;">Action</th>
						</tr>
						<?php $grandtotal=0; if(isset($_POST['submit'])){ ?>
							  	<?php foreach ($showdata as $key => $value) {?>
							  		<tr>
							  			<td colspan="6"><b><?php echo $key; ?><b></td>
							  		</tr>  
								  	<?php foreach ($value as $akey =>$data1) { ?>
								  		<tr>
									  		<td><?php echo $data1['id']; ?></td>
									  		<td><?php echo $data1['time'] ?></td>
									  		<td><?php echo $data1['date']; ?></td>
									  		<td><?php echo $data1['user_name']; ?></td>
									  		<td><?php echo $data1['ip_address']; ?></td>
									  		<td><?php echo $data1['action']; ?></td>
									  	</tr> 
									<?php } ?>
							  	<?php } ?>
					  	<?php }  ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		function close_fun_1(){
			window.location.reload();
		}

		/*$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/shiftclose_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/shiftclose_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});*/

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>