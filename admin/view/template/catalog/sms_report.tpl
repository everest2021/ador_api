<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" id="form" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2 col-sm-offset-3">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $filter_enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								   	<div style = "display:none" class="col-sm-2">
								   		<label>Pay Type</label>
								    	<select class="form-control" name="filter_type" id="filter_type">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($types as $key => $value) { ?>
								    			<?php if($key == $filter_type) { ?>
								    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
								    			<?php } else { ?>
								    				<option value="<?php echo $key ?>"><?php echo $value?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								    <div style="display: none" class="col-sm-2">
								   		<label>User</label>
								    	<select class="form-control" name="filter_user_id" id="filter_type">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($users as $key => $value) { ?>
								    			<?php if($key == $filter_user_id) { ?>
								    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
								    			<?php } else { ?>
								    				<option value="<?php echo $key ?>"><?php echo $value?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								   	<div  class="col-sm-2">
								       	<label>Contact</label>
									    <input type="text" id="filter_number" value = "<?php $filter_number;?>" name="filter_number" class="form-control" placeholder="Contact">
									</div>
									<div class="col-sm-12">
										<br>
										<center><a id ="filter" class="btn btn-primary" value="Show">Submit</a>
										<a style = "display:none;" id="export" type="button" class="btn btn-primary">Export</a></center>

									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-10 col-sm-offset-1">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>SMS REPORT</b></h3></center>
						<h3>From : <?php echo $filter_startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $filter_enddate; ?></h3><br>
						<?php if ($final_datas) {?>
						  	<table class="table table-bordered table-hover" style="text-align: center;">
								<thead>
									<tr>
										<td style="text-align: center;width: 50px !important ;"><?php echo 'Date'; ?></td>
										<td style="text-align: center;"><?php echo 'Customer'; ?></td>
										<td style="text-align: center;"><?php echo 'Contact'; ?></td>
									</tr>
								</thead>
								<tbody>
									<?php if($final_datas){ ?>
										<?php foreach($final_datas as $fkeys => $fvalues){
										$date = date('d-m-Y',strtotime($fvalues['date'])); ?>
												<tr>			          
													<td style="text-align: center;width: 144px !important ;" class="left">
														<?php echo $date; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $fvalues['fullname']; ?>
													</td>
													<td style="text-align: center;" class="left">
														<?php echo $fvalues['number']; ?>
													</td>
												</tr>
										<?php } ?>
									<?php } ?>
								</tbody>
							</table>
						<?php }  ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker({
			dateFormat: 'dd-mm-yy',			
	  	});

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/sms_report&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_name = $('input[name=\'filter_name\']').val();
		 	var filter_id = $('input[name=\'filter_id\']').val();
		 	var filter_type = $('select[name=\'filter_type\']').val();
		 	var filter_user_id = $('select[name=\'filter_user_id\']').val();
		 	var filter_number = $('input[name=\'filter_number\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		  	if (filter_number) {
				url += '&filter_number=' + encodeURIComponent(filter_number);
		  	}

		  	if (filter_user_id) {
				url += '&filter_user_id=' + encodeURIComponent(filter_user_id);
		  	}
		  
		  	if (filter_type) {
				url += '&filter_type=' + encodeURIComponent(filter_type);
		  	}
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  	var url = 'index.php?route=catalog/sms_report/export&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var filter_name = $('input[name=\'filter_name\']').val();
		 	var filter_id = $('input[name=\'filter_id\']').val();
		 	var filter_type = $('select[name=\'filter_type\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		   	if (filter_name) {
				if (filter_id) {
				  	url += '&filter_id=' + encodeURIComponent(filter_id);
					url += '&filter_name=' + encodeURIComponent(filter_name);
			  	}
		  	}
		  	if (filter_type) {
				url += '&filter_type=' + encodeURIComponent(filter_type);
		  	}
		  	location = url;
		});

		
		$('#type').on('change', function() {
			$('#form').submit();
		});

		$('#filter_name').autocomplete({
		  	delay: 500,
		  	source: function(request, response) {
		  		filter_type = $('#filter_type').val();
				$.ajax({
			  		url: 'index.php?route=catalog/captain_and_waiter/name&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_type=' +  encodeURIComponent(filter_type),
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.name,
								value: item.id,
				  			}
						}));
			  		}
				});
		  	}, 
		  	select: function(event, ui) {
		  		$('#filter_id').val(ui.item.value);
		  		$('#filter_name').val(ui.item.label);
		  		return false;
		  	}
		});
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>

<?php echo $footer; ?>