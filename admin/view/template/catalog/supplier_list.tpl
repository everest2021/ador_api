<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" 
	             class="btn btn-primary"><i class="fa fa-plus"></i></a>
		         <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
	        </div>
	         <h1><?php echo $heading_title; ?></h1>
	        <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
	        </ul>
	    </div>
  </div>
  <div class="container-fluid">
	    <?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	    <?php } ?>
	    <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	     <?php } ?>
	  <div class="panel panel-default">
	  <div class="panel-heading">
		   <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
	  </div>
	  <div class="panel-body">
		  <div class="well">
		     <div class="row">
			     <div class="col-sm-4">
			          <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Supplier Name'; ?></label>
				          <input type="text" name="filter_supplier" value="<?php echo $filter_supplier; ?>" placeholder="<?php echo ' Name'; ?>" id="input-filter_supplier" class="form-control" />
				          <input type="hidden" name="filter_supplier_id" value="<?php echo $filter_supplier_id; ?>" id="filter_supplier_id" class="form-control" />
				          <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			         </div>
			      </div>
		    </div>
		</div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		  <div class="table-responsive">
			<table class="table table-bordered table-hover">
			  <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  <td class="text-left">
					<?php if ($sort == 'fitler_code') { ?>
					<a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo " Item Code"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_code; ?>"><?php echo " Code"; ?></a>
					<?php } ?></td>
				  <td class="text-left">
					<?php if ($sort == 'filter_supplier') { ?>
					<a href="<?php echo $sort_supplier; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Item supplier'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_supplier; ?>"><?php echo 'Supplier'; ?></a>
					<?php } ?></td>
				  <td class="text-left">
					<?php if ($sort == 'filter_city') { ?>
					<a href="<?php echo $sort_city; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Item Type"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_city; ?>"><?php echo "City"; ?></a>
					<?php } ?></td>
				  
				  <td class="text-right"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($waiters) { ?>
				<?php foreach ($waiters as $waiter) { ?>
				<tr>
				  <td class="text-center">
					<?php if (in_array($waiter['filter_supplier_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $waiter['filter_supplier_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $waiter['filter_supplier_id']; ?>" />
					<?php } ?></td>
				  <td class="text-left"><?php echo $waiter['filter_code']; ?></td>
				  <td class="text-left"><?php echo $waiter['filter_supplier']; ?></td>
				  <td class="text-left"><?php echo $waiter['filter_city']; ?></td>
				  <td class="text-right"><a href="<?php echo $waiter['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			</table>
		  </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/supplier&token=<?php echo $token; ?>';

  var filter_supplier = $('input[name=\'filter_supplier\']').val();
  if (filter_supplier) {
	var filter_supplier_id = $('input[name=\'filter_supplier_id\']').val();
	if (filter_supplier_id) {
	  url += '&filter_supplier_id=' + encodeURIComponent(filter_supplier_id);
	  }
	  url += '&filter_supplier=' + encodeURIComponent(filter_supplier);
  }

 //  var filter_sport_type = $('select[name=\'filter_sport_type\']').val();
 //  if (filter_sport_type != '*') {
	// url += '&filter_sport_type=' + encodeURIComponent(filter_sport_type);
 //  }

  location = url;
});

$('input[name=\'filter_supplier\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/supplier/autocomplete&token=<?php echo $token; ?>&filter_supplier=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.supplier,
            value: item.supplier_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_supplier\']').val(ui.item.label);
    $('input[name=\'filter_supplier_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
</script></div>
<?php echo $footer; ?>