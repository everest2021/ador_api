<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-2" >
					       					<center><label>Start Date</label></center>

									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-2" style="display: none;">
									    	<center><label>End Date</label></center>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
									<div class="col-sm-2">
								    	<center><label>Select Store</label></center>
								     	<select name="store" id="store" class="form-control">
								     		<!-- <option value="">All</option> -->
								     		<?php foreach($stores as $storez) { ?>
								     			<?php if($storez['id'] == $store) { ?>
								     				<option value = "<?php echo $storez['id'] ?>" selected="selected"><?php echo $storez['store_name'] ?></option>
								     			<?php } else { ?>
								     				<option value = "<?php echo $storez['id'] ?>" ><?php echo $storez['store_name'] ?></option>
								     			<?php } ?>
								     		<?php } ?>
								     	</select>
								    </div>
								    <div class="col-sm-2">
								    	<center><label>Select Type</label></center>
								     	<select name="type" id="type" class="form-control">
								     		<!-- <option value="">All</option> -->
								     		<?php foreach($typess as $typez) {  //echo'<pre>';print_r($type);exit;?>
								     			<?php if($typez['type_id'] == $type) { ?>
								     				<option value = "<?php echo $typez['type_id'] ?>" selected="selected"><?php echo $typez['type'] ?></option>
								     			<?php } else { ?>
								     				<option value = "<?php echo $typez['type_id'] ?>" ><?php echo $typez['type'] ?></option>
								     			<?php } ?>
								     		<?php } ?>
								     	</select>
								    </div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" class="btn btn-primary" value="Show"></center>

								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button style="display: none;" id="print" type="button" class="btn btn-primary">Print</button>
						<button id="excel" type="button" class="btn btn-primary">Export Excel</button>
				 	</div>
					<div class="col-sm-6 col-sm-offset-3">
					<?php if($startdate != '' && $enddate != ''){ ?>
						<center><h3><b>Current Stock LBS Report</b></h3></center>
						<h3>From : <?php echo $startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
						  <table class="table table-bordered table-hover" style="text-align: center;">
								<?php $total = 0; ?>
								<tr>
									<th style="text-align: center;">Brand Name</th>
									<?php foreach($brandsizezz as $key => $value) {  ?>
										<th style="text-align: center;"><?php echo $value['brand_size'] ?></th>
									<?php } ?>
									<th style="text-align: center;">Loose</th>
								</tr>
								<?php foreach($finaldatas as $key => $value) { //echo'<pre>';print_r($value);exit; ?>
									<tr>
										<td><?php echo $key ?></td>
										<?php foreach ($value as $skey => $svalue) { ?>
											<td><?php echo $svalue['qty'] ?></td>
										<?php } ?>
										<?php foreach ($loosezz as $lkey => $lvalue) { ?>
											<?php if ($key == $lkey) { ?>
												<td><?php echo $lvalue ?></td>
											<?php } ?>

										<?php } ?>
									</tr>
								<?php } ?>
						  	</table>
					  <?php } ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  	var url = 'index.php?route=catalog/currentstockreport_lsb/prints&token=<?php echo $token; ?>';

		  	var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  	var store = $('select[name=\'store\']').val();
		  	var type = $('select[name=\'type\']').val();


		  	if (filter_startdate) {
			 	url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}

		  	if (filter_enddate) {
			  	url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}

		   	if (store) {
			  	url += '&store=' + encodeURIComponent(store);
		  	}

		   	if (type) {
			  	url += '&type=' + encodeURIComponent(type);
		  	}
		  	
		  	location = url;
		  	//setTimeout(close_fun_1, 50);
		});


		$('#excel').on('click', function() {
		  	var url = 'index.php?route=catalog/currentstockreport_lsb/excelprint&token=<?php echo $token; ?>';

		  	var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  	var store = $('select[name=\'store\']').val();
		  	var type = $('select[name=\'type\']').val();


		  	if (filter_startdate) {
			 	url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}

		  	if (filter_enddate) {
			  	url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}

		   	if (store) {
			  	url += '&store=' + encodeURIComponent(store);
		  	}

		   	if (type) {
			  	url += '&type=' + encodeURIComponent(type);
		  	}
		  	
		  	location = url;
		  	//setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>