<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  <div class="container-fluid">
	<div class="panel-body" style="padding-bottom: 0%;">
		<div style="display:inline-block; width:50%;float:left;">
	  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 3%;overflow-y: auto;height:auto">
		  		<div class="col-md-12">
			  		<div class="col-md-9 col-md-offset-1">
						<table style="text-align: right;" class="table table-bordered table-hover">
							<tr>
								<td>Total Bill Amount :</td>
								<td style="text-align: left;"><?php echo $getTotalAmount['total_amount'] ?></td>
							</tr>
							<?php foreach($billdatas as $billdata) { ?>
								<tr>
									<td>Cancel Bill Amount (-) :</td>
									<td style="text-align: left;"><?php echo $cancelamount['total_amt'] ?></td>
								</tr>
								<tr>
									<td>Discount Amount (-) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_discount'] ?></td>
								</tr>
								<tr>
									<td>Complimentry Amount (-) :</td>
									<td style="text-align: left;"></td>
								</tr>
								<tr>
									<td>Advance Amount (-) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_advance'] ?></td>
								</tr>
								<tr>
									<td>Sub Total :</td>
									<td style="text-align: left;"><?php echo round($billdata['bill_amount'] - $billdata['total_discount']) ?></td>
								</tr>
								<tr>
									<td>Service Charges (+) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_stax'] ?></td>
								</tr>
								<tr>
									<td>Vat (+) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_vat'] ?></td>
								</tr>
								<tr>
									<td>GST (+) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_gst'] ?></td>
								</tr>
							</table>
							<br>
							<table style="text-align: right;" class="table table-bordered table-hover">
						  		<tr>
									<td>Other Amount (+) :</td>
									<td style="text-align: left;"></td>
								</tr>
								<tr>
									<td>Rounding Amount (+) :</td>
									<td style="text-align: left;"><?php echo $billdata['total_roundamt'] ?></td>
								</tr>
								<tr>
									<td>Advance Amount (+):</td>
									<td style="text-align: left;"><?php echo $advancetotal ?></td>
								</tr>
								<tr>
									<td style="font-size: 15px;font-weight: bolder;">Net Amount :</td>
									<td style="text-align: left;font-size: 15px;font-weight: bolder;"><?php echo round($billdata['net_amount']) ?></td>
								</tr>
							<?php } ?>
								<tr>
									<td style="font-size: 15px;font-weight: bolder;">Unsettled Bill Amount (-) :</td>
									<td style="text-align: left;font-size: 15px;font-weight: bolder;"><?php echo round($unsettlebill) ?></td>
								</tr>
								<?php foreach($currentamounts as $currentamount) { ?>
									<tr>
										<td style="font-size: 15px">Running Table Amount :</td>
										<td style="text-align: left;font-size: 15px"><?php echo $currentamount['current_amount'] ?></td>
									</tr>
								<?php } ?>
							</table>

					</div>
				</div>
	  		</div>
		</div>
		<div style="display:inline-block; width:50%;float:left;padding-left: 10px;">
	  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 5%;overflow-y: auto;height:auto;">
		  		<div class="col-md-12">
		  			<div style="background-color: silver; height: 307px;">
						<table style="background-color: white;" class="table table-bordered table-hover">
					  		<tr>
								<td>Table Group Name</td>
								<td>Total Bill</td>
								<td>Amount</td>
					  		</tr>
					  		<?php foreach($tableinfos as $tableinfo) { ?>
						  		<tr>
						  			<td><?php echo $tableinfo['location'] ?></td>
						  			<td><?php echo $tableinfo['order_no'] ?></td>
						  			<td><?php echo $tableinfo['grand_total'] ?></td>
						  		</tr>
					  		<?php } ?>
						</table>
					</div>
					<br>
					
					<div class="col-md-12" style="padding: 0px;">
						<table style="text-align: right" class="table table-bordered table-hover">
					  		<tr style="text-align: center;">
								<td></td>
								<td>Quantity</td>
								<td>Amount</td>
					  		</tr>
					  		<tr>
						  		<td>Cancel KOT :</td> 
						  		<td><?php echo $fooddata['cancelqty_food'] ?></td>
						  		<td><?php echo $fooddata['cancelamt_food'] ?></td>
					  		</tr>
					  		<tr>
						  		<td>Cancel BOT :</td>
						  		<td><?php echo $liqdata['cancelqty_liq'] ?></td>
						  		<td><?php echo $liqdata['cancelamt_liq'] ?></td>
					  		</tr>
					  		<tr>
						  		<td>Cancel Bill :</td>
						  		<td><?php echo $cancelbillitem['total_qty'] ?></td>
						  		<td><?php echo $cancelbillitem['total_amt'] ?></td>
					  		</tr>
					  		<?php foreach($billdatas as $billdata) { ?>
						  		<tr>
							  		<td style="font-size: 15px;font-weight: bolder;">Food :</td>
							  		<td style="font-size: 15px;font-weight: bolder;"><?php echo round($billdata['total_ftotal'] - $billdata['total_discount']) ?></td>
							  		<td style="font-size: 15px;font-weight: bolder;">BAR :<?php echo $billdata['total_ltotal'] ?></td>
						  		</tr>
					  		<?php } ?>
					  		<tr>
						  		<td>Last Ref No :</td>
						  		<td colspan="2"  style="text-align: left;"><?php echo $lastbill ?></td>
					  		</tr>
						</table>
					</div>
				</div>
	  		</div>
		</div>
		<div class="col-md-12">
			<table style="" class="table table-bordered table-hover">
		  		<tr>
					<td>Cash</td>
					<td>Credit Card</td>
					<td>Meal Pass</td>
					<td>Coupon</td>
					<td>Prepaid Card</td>
					<td>Room Service</td>
					<td>On Account</td>
		  		</tr>
		  		<?php foreach($settleamts as $settleamt) { ?>
			  		<tr>
						<td><?php echo $settleamt['total_cashamt'] ?></td>
						<td><?php echo $settleamt['total_creditamt'] ?></td>
						<td><?php echo $settleamt['total_mealamt'] ?></td>
						<td><?php echo $settleamt['total_passamt'] ?></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
			  		</tr>
			  	<?php } ?>
			</table>
		</div>
	</div>
<!-- 	<script type="text/javascript">
		var data = $('#netamount').val();
		var afterdec = data.split('.')[1];
		alert(afterdec);
	</script> -->
</div>
<?php echo $footer; ?>