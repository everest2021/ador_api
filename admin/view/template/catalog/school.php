<?php
class ControllerCatalogSchool extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/school');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/school');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/school');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/school');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			

			$order_id = $this->model_catalog_school->addOrder($this->request->post);

			

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

				$url .= '&order_id=' . $order_id;
			

			$this->response->redirect($this->url->link('catalog/school/getlist', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function edit() {
		$this->load->language('catalog/school');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/school');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$school_id = $this->model_catalog_school->editSchool($this->request->get['school_id'], $this->request->post);
			//$school_id = 46;
			if($this->request->post['is_set'] == '0' && $this->request->post['status'] == '1'){
				
				$school_datas = $this->db->query("SELECT * FROM `oc_school` WHERE `school_id` = '".$school_id."' ");
				if($school_datas->num_rows > 0){	
					$school_data = $school_datas->row;
					// $customer_data['firstname'] = 'Digamber';
					// $customer_data['middlename'] = 'Narayan';
					// $customer_data['lastname'] = 'Patil';
					// $school_data['principal_mobile'] = '9987760244';
					// $school_data['school_mobile'] = '9987760244';
					// $school_data['pt_teacher_contact'] = '9987760244';

					$username = 'dindia';
					$password = 'DIGITALINDIA007';
					$senderid = 'PaYYou';
					
					$school_data['principal_name'] = str_replace(' ', '%20', $school_data['principal_name']);
					$message_principal = 'Hi,%20'.$school_data['principal_name'].',%20your%20school%20is%20registered%20Successfully.%20Username%20:%20'.$school_data['udias_number'].'%20and%20Password%20:%20'.$school_data['password'].'.%20for%20more%20information%20visit%20vvcs.in';
					$number = $school_data['principal_mobile'];
					$url = 'http://sms.thedigitalindia.com/api/sendsms?username='.$username.'&password='.$password.'&senderid='.$senderid.'&message='.$message_principal.'&numbers='.$number.'&dndrefund=1';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					if($output === false){
						$this->log->write('Principal Curl error: ' . curl_error($ch));
					} else {
						$this->log->write('Pricipal Message Sent');
						$this->log->write(print_r($output, true));
					}
					curl_close($ch);

					$school_data['name'] = str_replace(' ', '%20', $school_data['name']);
					$school_school = 'Hi,%20'.$school_data['name'].',%20your%20school%20is%20registered%20Successfully.%20Username%20:%20'.$school_data['udias_number'].'%20and%20Password%20:%20'.$school_data['password'].'.%20for%20more%20information%20visit%20vvcs.in';
					$number = $school_data['school_mobile'];
					$url = 'http://sms.thedigitalindia.com/api/sendsms?username='.$username.'&password='.$password.'&senderid='.$senderid.'&message='.$school_school.'&numbers='.$number.'&dndrefund=1';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					if($output === false){
						$this->log->write('School Curl error: ' . curl_error($ch));
					} else {
						$this->log->write('School Message Sent');
						$this->log->write(print_r($output, true));
					}
					curl_close($ch);

					$school_data['pt_teacher_name'] = str_replace(' ', '%20', $school_data['pt_teacher_name']);
					$school_pt_teacher = 'Hi,%20'.$school_data['pt_teacher_name'].',%20your%20school%20is%20registered%20Successfully.%20Username%20:%20'.$school_data['udias_number'].'%20and%20Password%20:%20'.$school_data['password'].'.%20for%20more%20information%20visit%20vvcs.in';
					$number = $school_data['pt_teacher_contact'];
					$url = 'http://sms.thedigitalindia.com/api/sendsms?username='.$username.'&password='.$password.'&senderid='.$senderid.'&message='.$school_pt_teacher.'&numbers='.$number.'&dndrefund=1';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					if($output === false){
						$this->log->write('PT Teacher Curl error: ' . curl_error($ch));
					} else {
						$this->log->write('PT Teacher Message Sent');
						$this->log->write(print_r($output, true));
					}
					curl_close($ch);
					
					$school_data['pt_teacher_name'] = str_replace(' ', '%20', $school_data['pt_teacher_name']);
					$school_pt_teacher = 'Hi,%20'.$school_data['pt_teacher_name'].',%20your%20school%20is%20registered%20Successfully.%20Username%20:%20'.$school_data['udias_number'].'%20and%20Password%20:%20'.$school_data['password'].'.%20for%20more%20information%20visit%20vvcs.in';
					$number = '9730941488';//$school_data['pt_teacher_contact'];
					$url = 'http://sms.thedigitalindia.com/api/sendsms?username='.$username.'&password='.$password.'&senderid='.$senderid.'&message='.$school_pt_teacher.'&numbers='.$number.'&dndrefund=1';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					if($output === false){
						$this->log->write('Test PT Teacher Curl error: ' . curl_error($ch));
					} else {
						$this->log->write('Test PT Teacher Message Sent');
						$this->log->write(print_r($output, true));
						$this->log->write(print_r($url, true));
					}
					curl_close($ch);

					require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
					require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
					require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

					$mail = new PHPMailer();
					//$mail->SMTPDebug = 3;
					//$mail->isMail(); // Set mailer to use SMTP
					//$mail->Host = 'localhost'; 
					$school_data['principal_name'] = str_replace('%20', ' ', $school_data['principal_name']);
					$message = 'Hi '.$school_data['principal_name']. ", \n\n";
					$message .= 'Your School is registered Successfully.'."\n";
					$message .= 'Username : ' . $school_data['udias_number']."\n";
					$message .= 'Password : ' . $school_data['password']."\n\n";
					$message .= 'for more information visit vvcs.in';
					$subject = 'NM Kala Krida Registration';
					$from_email = 'fargose.aaron@gmail.com';
					$to_emails = $school_data['principal_email'];
					if($to_emails != ''){
						$to_emails_array = explode(',', $to_emails);
					}
					// echo '<pre>';
					// print_r($to_emails_array);
					// exit;
					//$to_email = 'fargose.aaron@gmail.com';
					$mail->IsSMTP();
					$mail->SMTPAuth = true;
					$mail->Host = 'localhost';//$this->config->get('config_smtp_host');
					$mail->Port = '587';//$this->config->get('config_smtp_port');
					$mail->Username = 'sportreg2017@sportsbasket.in';//$this->config->get('config_smtp_username');
					$mail->Password = 'sportregist2017';//$this->config->get('config_smtp_password');
					$mail->SMTPSecure = false;
					$mail->smtpConnect([
						'ssl' => [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						]
					]);
					$mail->SetFrom('sportreg2017@sportsbasket.in', 'NM Kala Krida 2017');
					$mail->Subject = $subject;
					//$mail->isHTML(true);
					//$mail->MsgHTML($invoice_mail_text);
					$mail->Body = html_entity_decode($message);
					//$mail->addAttachment($file_path, $filename);
					foreach($to_emails_array as $ekey => $evalue){
						//$to_name = 'Aaron Fargose';
						$mail->AddAddress($evalue);
					}
					if($mail->Send()) {
						$this->log->write('Principal Mail Sent');
					} else {
						$this->log->write('Principal Mail Not Sent');
						$this->log->write(print_r($mail->ErrorInfo, true));
					}

					$mail = new PHPMailer();
					//$mail->SMTPDebug = 3;
					//$mail->isMail(); // Set mailer to use SMTP
					//$mail->Host = 'localhost'; 
					$school_data['name'] = str_replace('%20', ' ', $school_data['name']);
					$message = 'Hi '.$school_data['name']. ", \n\n";
					$message .= 'Your School is registered Successfully.'."\n";
					$message .= 'Username : ' . $school_data['udias_number']."\n";
					$message .= 'Password : ' . $school_data['password']."\n\n";
					$message .= 'for more information visit vvcs.in.';
					$subject = 'NM Kala Krida Registration';
					$from_email = 'fargose.aaron@gmail.com';
					$to_emails = $school_data['school_email'];
					if($to_emails != ''){
						$to_emails_array = explode(',', $to_emails);
					}
					// echo '<pre>';
					// print_r($to_emails_array);
					// exit;
					//$to_email = 'fargose.aaron@gmail.com';
					$mail->IsSMTP();
					$mail->SMTPAuth = true;
					$mail->Host = 'localhost';//$this->config->get('config_smtp_host');
					$mail->Port = '587';//$this->config->get('config_smtp_port');
					$mail->Username = 'sportreg2017@sportsbasket.in';//$this->config->get('config_smtp_username');
					$mail->Password = 'sportregist2017';//$this->config->get('config_smtp_password');
					$mail->SMTPSecure = false;
					$mail->smtpConnect([
						'ssl' => [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						]
					]);
					$mail->SetFrom('sportreg2017@sportsbasket.in', 'NM Kala Krida 2017');
					$mail->Subject = $subject;
					//$mail->isHTML(true);
					//$mail->MsgHTML($invoice_mail_text);
					$mail->Body = html_entity_decode($message);
					//$mail->addAttachment($file_path, $filename);
					foreach($to_emails_array as $ekey => $evalue){
						//$to_name = 'Aaron Fargose';
						$mail->AddAddress($evalue);
					}
					if($mail->Send()) {
						$this->log->write('School Mail Sent');
					} else {
						$this->log->write('School Mail Not Sent');
						$this->log->write(print_r($mail->ErrorInfo, true));
					}

					$mail = new PHPMailer();
					//$mail->SMTPDebug = 3;
					//$mail->isMail(); // Set mailer to use SMTP
					//$mail->Host = 'localhost'; 
					$school_data['pt_teacher_name'] = str_replace('%20', ' ', $school_data['pt_teacher_name']);
					$message = 'Hi '.$school_data['pt_teacher_name']. ", \n\n";
					$message .= 'Your School is registered Successfully.'."\n";
					$message .= 'Username : ' . $school_data['udias_number']."\n";
					$message .= 'Password : ' . $school_data['password']."\n\n";
					$message .= 'for more information visit vvcs.in';
					$subject = 'NM Kala Krida Registration';
					$from_email = 'fargose.aaron@gmail.com';
					$to_emails = $school_data['pt_teacher_email'];
					if($to_emails != ''){
						$to_emails_array = explode(',', $to_emails);
					}
					// echo '<pre>';
					// print_r($to_emails_array);
					// exit;
					//$to_email = 'fargose.aaron@gmail.com';
					$mail->IsSMTP();
					$mail->SMTPAuth = true;
					$mail->Host = 'localhost';//$this->config->get('config_smtp_host');
					$mail->Port = '587';//$this->config->get('config_smtp_port');
					$mail->Username = 'sportreg2017@sportsbasket.in';//$this->config->get('config_smtp_username');
					$mail->Password = 'sportregist2017';//$this->config->get('config_smtp_password');
					$mail->SMTPSecure = false;
					$mail->smtpConnect([
						'ssl' => [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						]
					]);
					$mail->SetFrom('sportreg2017@sportsbasket.in', 'NM Kala Krida 2017');
					$mail->Subject = $subject;
					//$mail->isHTML(true);
					//$mail->MsgHTML($invoice_mail_text);
					$mail->Body = html_entity_decode($message);
					//$mail->addAttachment($file_path, $filename);
					foreach($to_emails_array as $ekey => $evalue){
						//$to_name = 'Aaron Fargose';
						$mail->AddAddress($evalue);
					}
					if($mail->Send()) {
						$this->log->write('PT Teacher Mail Sent');
					} else {
						$this->log->write('PT Teacher Mail Not Sent');
						$this->log->write(print_r($mail->ErrorInfo, true));
					}	
				}
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/school');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/school');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $school_id) {
				$this->model_catalog_school->deleteSchool($school_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function getList() {
		$this->load->language('catalog/school');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/school');
		
			$data['loc'] = '';
			$data['locid'] = '';
			$data['table'] = '';
			$data['table_id'] = '';
			$data['waiter'] = '';
			$data['waiter_id'] = '';
			$data['captain'] = '';
			$data['captain_id'] = '';
			$data['person'] = '';
			$data['ftotal'] = 0;
			$data['gst'] = '';
			$data['ltotal'] = '';
			$data['vat'] = '';
			$data['cess'] = '';
			$data['stax'] = '';
			$data['discount'] = '';
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$query = $this->db->query("SELECT * FROM `oc_order_info` WHERE `order_id`='".$order_id."'")->rows;
			$query1 = $this->db->query("SELECT * FROM `oc_order_items` WHERE `order_id`='".$order_id."'")->rows;
			$data['orderitems'] = $query1;
			$data['loc'] = $query[0]['location'];
			$data['locid'] = $query[0]['location_id'];
			$data['table'] = $query[0]['t_name'];
			$data['table_id'] = $query[0]['table_id'];
			$data['waiter'] = $query[0]['waiter'];
			$data['waiter_id'] = $query[0]['waiter_id'];
			$data['captain'] = $query[0]['captain'];
			$data['captain_id'] = $query[0]['captain_id'];
			$data['person'] = $query[0]['person'];
			$data['ftotal'] = $query[0]['ftotal'];
			$data['gst'] = $query[0]['gst'];
			$data['ltotal'] = $query[0]['ltotal'];
			$data['vat'] = $query[0]['vat'];
			$data['cess'] = $query[0]['cess'];
			$data['stax'] = $query[0]['stax'];
			$data['discount'] = $query[0]['discount'];
			// echo '<pre>';
			// print_r($query[0]['location']);
			// print_r($query);
			// print_r($query1);
			// exit;
		} 
		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/school/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/school/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['schools'] = array();

		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_status' => $filter_status,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		$locations =  $this->db->query("SELECT  * FROM " . DB_PREFIX . "location")->rows;
		
		
		foreach ($locations as $lkey => $loc) {
			if($data['locid'] != ''){
				$data['sel'] = $data['locid'];
			}
				elseif($lkey == 0)
				{
					$data['sel'] = $loc['location_id'];
				}
				$data['locations'][] = array(
						'select' => $data['sel'],
						'name'	=> $loc['location'],
						'id'	=>	$loc['location_id']
					);
			
		}
		/*echo '<pre>';
		print_r($data['locations']);
		exit;*/

		$school_total = $this->model_catalog_school->getTotalSchools($filter_data);

		$results = $this->model_catalog_school->getSchools($filter_data);

		foreach ($results as $result) {
			if($result['status'] == '1'){
				$status = 'सक्रिय / Active';
			} else {
				$status = 'निष्क्रिय / Inactive';
			}
			$data['schools'][] = array(
				'school_id' => $result['school_id'],
				'name'        => $result['name'],
				'status'        => $status,
				'edit'        => $this->url->link('catalog/school/edit', 'token=' . $this->session->data['token'] . '&school_id=' . $result['school_id'] . $url, true)
			);
		}

		$data['token'] = $this->session->data['token'];

		$data['school_types'] = array(
			'1' => 'KALA',
			'2' => 'KRIDA',
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
// if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/school/add', 'token=' . $this->session->data['token'] . $url, true);
		// } 
		$data['sort_name'] = $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $school_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($school_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($school_total - $this->config->get('config_limit_admin'))) ? $school_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $school_total, ceil($school_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_name_id'] = $filter_name_id;
		$data['filter_status'] = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/order', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['school_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_filename'] = $this->language->get('entry_filename');
		$data['entry_mask'] = $this->language->get('entry_mask');

		$data['help_filename'] = $this->language->get('help_filename');
		$data['help_mask'] = $this->language->get('help_mask');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['text_select'] = '--Please Select--';

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['udias_number'])) {
			$data['error_udias_number'] = $this->error['udias_number'];
		} else {
			$data['error_udias_number'] = '';
		}

		if (isset($this->error['registration_source'])) {
			$data['error_registration_source'] = $this->error['registration_source'];
		} else {
			$data['error_registration_source'] = '';
		}

		if (isset($this->error['principal_name'])) {
			$data['error_principal_name'] = $this->error['principal_name'];
		} else {
			$data['error_principal_name'] = '';
		}

		if (isset($this->error['principal_email'])) {
			$data['error_principal_email'] = $this->error['principal_email'];
		} else {
			$data['error_principal_email'] = '';
		}

		if (isset($this->error['principal_mobile'])) {
			$data['error_principal_mobile'] = $this->error['principal_mobile'];
		} else {
			$data['error_principal_mobile'] = '';
		}

		if (isset($this->error['principal_address'])) {
			$data['error_principal_address'] = $this->error['principal_address'];
		} else {
			$data['error_principal_address'] = '';
		}

		if (isset($this->error['district'])) {
			$data['error_district'] = $this->error['district'];
		} else {
			$data['error_district'] = '';
		}

		if (isset($this->error['taluka'])) {
			$data['error_taluka'] = $this->error['taluka'];
		} else {
			$data['error_taluka'] = '';
		}

		if (isset($this->error['class_from'])) {
			$data['error_class_from'] = $this->error['class_from'];
		} else {
			$data['error_class_from'] = '';
		}

		if (isset($this->error['class_to'])) {
			$data['error_class_to'] = $this->error['class_to'];
		} else {
			$data['error_class_to'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['education_trust_name'])) {
			$data['error_education_trust_name'] = $this->error['education_trust_name'];
		} else {
			$data['error_education_trust_name'] = '';
		}

		if (isset($this->error['school_email'])) {
			$data['error_school_email'] = $this->error['school_email'];
		} else {
			$data['error_school_email'] = '';
		}

		if (isset($this->error['school_mobile'])) {
			$data['error_school_mobile'] = $this->error['school_mobile'];
		} else {
			$data['error_school_mobile'] = '';
		}

		if (isset($this->error['school_landline'])) {
			$data['error_school_landline'] = $this->error['school_landline'];
		} else {
			$data['error_school_landline'] = '';
		}

		if (isset($this->error['school_address'])) {
			$data['error_school_address'] = $this->error['school_address'];
		} else {
			$data['error_school_address'] = '';
		}

		if (isset($this->error['school_pincode'])) {
			$data['error_school_pincode'] = $this->error['school_pincode'];
		} else {
			$data['error_school_pincode'] = '';
		}

		if (isset($this->error['pt_teacher_name'])) {
			$data['error_pt_teacher_name'] = $this->error['pt_teacher_name'];
		} else {
			$data['error_pt_teacher_name'] = '';
		}

		if (isset($this->error['pt_teacher_email'])) {
			$data['error_pt_teacher_email'] = $this->error['pt_teacher_email'];
		} else {
			$data['error_pt_teacher_email'] = '';
		}

		if (isset($this->error['pt_teacher_contact'])) {
			$data['error_pt_teacher_contact'] = $this->error['pt_teacher_contact'];
		} else {
			$data['error_pt_teacher_contact'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['order_id'])) {
			$data['action'] = $this->url->link('catalog/school/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/school/edit', 'token=' . $this->session->data['token'] . '&school_id=' . $this->request->get['school_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/school', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$school_data = array();
		if (isset($this->request->get['school_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$school_data = $this->model_catalog_school->getSchool($this->request->get['school_id']);
		}

		// echo '<pre>';
		// print_r($school_data);
		// exit;

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['school_id'])) {
			$data['school_id'] = $this->request->get['school_id'];
		} elseif (!empty($school_data)) {
			$data['school_id'] = $school_data['school_id'];
		} else {
			$data['school_id'] = 0;
		}

		if (isset($this->request->post['udias_number'])) {
			$data['udias_number'] = $this->request->post['udias_number'];
		} elseif(!empty($school_data)) {
			$data['udias_number'] = $school_data['udias_number'];
		} else {
			$data['udias_number'] = '';
		}

		if (isset($this->request->post['registration_name'])) {
			$data['registration_name'] = $this->request->post['registration_name'];
		} elseif(!empty($school_data)) {
			$data['registration_name'] = $school_data['registration_name'];
		} else {
			$data['registration_name'] = '';
		}

		if (isset($this->request->post['registration_source'])) {
			$data['registration_source'] = $this->request->post['registration_source'];
		} elseif(!empty($school_data)) {
			$data['registration_source'] = $school_data['registration_source'];
		} else {
			$data['registration_source'] = '';
		}

		if (isset($this->request->post['principal_name'])) {
			$data['principal_name'] = $this->request->post['principal_name'];
		} elseif(!empty($school_data)) {
			$data['principal_name'] = $school_data['principal_name'];
		} else {
			$data['principal_name'] = '';
		}

		if (isset($this->request->post['principal_email'])) {
			$data['principal_email'] = $this->request->post['principal_email'];
		} elseif(!empty($school_data)) {
			$data['principal_email'] = $school_data['principal_email'];
		} else {
			$data['principal_email'] = '';
		}

		if (isset($this->request->post['principal_mobile'])) {
			$data['principal_mobile'] = $this->request->post['principal_mobile'];
		} elseif(!empty($school_data)) {
			$data['principal_mobile'] = $school_data['principal_mobile'];
		} else {
			$data['principal_mobile'] = '';
		}

		if (isset($this->request->post['principal_address'])) {
			$data['principal_address'] = $this->request->post['principal_address'];
		} elseif(!empty($school_data)) {
			$data['principal_address'] = $school_data['principal_address'];
		} else {
			$data['principal_address'] = '';
		}

		if (isset($this->request->post['district'])) {
			$data['district'] = $this->request->post['district'];
		} elseif(!empty($school_data)) {
			$data['district'] = $school_data['district'];
		} else {
			$data['district'] = 'Palghar';
		}

		$data['districts'] = array(
			'Palghar' => 'Palghar',
		);

		if (isset($this->request->post['taluka'])) {
			$data['taluka'] = $this->request->post['taluka'];
		} elseif(!empty($school_data)) {
			$data['taluka'] = $school_data['taluka'];
		} else {
			$data['taluka'] = 'Vasai';
		}

		$data['talukas'] = array(
			'Vasai' => 'Vasai',
		);

		if (isset($this->request->post['class_from'])) {
			$data['class_from'] = $this->request->post['class_from'];
		} elseif(!empty($school_data)) {
			$data['class_from'] = $school_data['class_from'];
		} else {
			$data['class_from'] = '';
		}

		$data['standards'] = array(
			'Sr. Kg' => 'Sr. Kg',
			'Jr. Kg' => 'Jr. Kg',
			'I' => 'I',
			'II' => 'II',
			'III' => 'III',
			'IV' => 'IV',
			'V' => 'V',
			'VI' => 'VI',
			'VII' => 'VII',
			'VIII' => 'VIII',
			'IX' => 'IX',
			'X' => 'X',
		);

		if (isset($this->request->post['class_to'])) {
			$data['class_to'] = $this->request->post['class_to'];
		} elseif(!empty($school_data)) {
			$data['class_to'] = $school_data['class_to'];
		} else {
			$data['class_to'] = '';
		}

		// $data['school_names'] = array(
		// 	'1' => 'St Francis De Sales',
		// 	'2' => 'Maharashtra India',
		// 	'3' => 'JP Luddahani',
		// );
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif(!empty($school_data)) {
			$data['name'] = $school_data['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['education_trust_name'])) {
			$data['education_trust_name'] = $this->request->post['education_trust_name'];
		} elseif(!empty($school_data)) {
			$data['education_trust_name'] = $school_data['education_trust_name'];
		} else {
			$data['education_trust_name'] = '';
		}

		if (isset($this->request->post['school_email'])) {
			$data['school_email'] = $this->request->post['school_email'];
		} elseif(!empty($school_data)) {
			$data['school_email'] = $school_data['school_email'];
		} else {
			$data['school_email'] = '';
		}

		if (isset($this->request->post['school_mobile'])) {
			$data['school_mobile'] = $this->request->post['school_mobile'];
		} elseif(!empty($school_data)) {
			$data['school_mobile'] = $school_data['school_mobile'];
		} else {
			$data['school_mobile'] = '';
		}

		if (isset($this->request->post['school_landline'])) {
			$data['school_landline'] = $this->request->post['school_landline'];
		} elseif(!empty($school_data)) {
			$data['school_landline'] = $school_data['school_landline'];
		} else {
			$data['school_landline'] = '';
		}

		if (isset($this->request->post['school_address'])) {
			$data['school_address'] = $this->request->post['school_address'];
		} elseif(!empty($school_data)) {
			$data['school_address'] = $school_data['school_address'];
		} else {
			$data['school_address'] = '';
		}

		if (isset($this->request->post['school_pincode'])) {
			$data['school_pincode'] = $this->request->post['school_pincode'];
		} elseif(!empty($school_data)) {
			$data['school_pincode'] = $school_data['school_pincode'];
		} else {
			$data['school_pincode'] = '';
		}

		if (isset($this->request->post['pt_teacher_name'])) {
			$data['pt_teacher_name'] = $this->request->post['pt_teacher_name'];
		} elseif(!empty($school_data)) {
			$data['pt_teacher_name'] = $school_data['pt_teacher_name'];
		} else {
			$data['pt_teacher_name'] = '';
		}

		if (isset($this->request->post['pt_teacher_email'])) {
			$data['pt_teacher_email'] = $this->request->post['pt_teacher_email'];
		} elseif(!empty($school_data)) {
			$data['pt_teacher_email'] = $school_data['pt_teacher_email'];
		} else {
			$data['pt_teacher_email'] = '';
		}

		if (isset($this->request->post['pt_teacher_contact'])) {
			$data['pt_teacher_contact'] = $this->request->post['pt_teacher_contact'];
		} elseif(!empty($school_data)) {
			$data['pt_teacher_contact'] = $school_data['pt_teacher_contact'];
		} else {
			$data['pt_teacher_contact'] = '';
		}

		if (isset($this->request->post['is_set'])) {
			$data['is_set'] = $this->request->post['is_set'];
		} elseif(!empty($school_data)) {
			$data['is_set'] = $school_data['is_set'];
		} else {
			$data['is_set'] = '0';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} elseif(!empty($school_data)) {
			$data['password'] = $school_data['password'];
		} else {
			$data['password'] = '0';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif(!empty($school_data)) {
			$data['status'] = $school_data['status'];
		} else {
			$data['status'] = '1';
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/school_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/school')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['udias_number']) < 11) || (utf8_strlen($this->request->post['udias_number']) > 11)) {
			$this->error['udias_number'] = 'Please Enter Valid UDIAS Number';
		}

		if ((utf8_strlen($this->request->post['registration_source']) < 1) || (utf8_strlen($this->request->post['registration_source']) > 255)) {
			$this->error['registration_source'] = 'Please Upload School Registration Certificate';
		}

		if ((utf8_strlen($this->request->post['principal_name']) < 1) || (utf8_strlen($this->request->post['principal_name']) > 255)) {
			$this->error['principal_name'] = 'Please Enter Principal Name';
		}

		if ((utf8_strlen($this->request->post['principal_email']) < 1) || (utf8_strlen($this->request->post['principal_email']) > 255)) {
			$this->error['principal_email'] = 'Please Enter Principal Email';
		} else {
			if(!filter_var($this->request->post['principal_email'], FILTER_VALIDATE_EMAIL)){
				$this->error['principal_email'] = 'Please Enter Valid Principal Email';
			}
		}

		if ((utf8_strlen($this->request->post['principal_mobile']) < 10) || (utf8_strlen($this->request->post['principal_mobile']) > 10)) {
			$this->error['principal_mobile'] = 'Please Enter Valid Principal Mobile';
		}

		if ((utf8_strlen($this->request->post['principal_address']) < 1) || (utf8_strlen($this->request->post['principal_address']) > 255)) {
			$this->error['principal_address'] = 'Please Enter Principal Address';
		}

		if ($this->request->post['district'] == '') {
			$this->error['district'] = 'Please Select District';
		}

		if ($this->request->post['taluka'] == '') {
			$this->error['taluka'] = 'Please Select Taluka';
		}

		if ($this->request->post['class_from'] == '') {
			$this->error['class_from'] = 'Please Select Class From';
		}

		if ($this->request->post['class_to'] == '') {
			$this->error['class_to'] = 'Please Select Class To';
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'] = 'Please Enter School Name';
		}

		// if ((utf8_strlen($this->request->post['education_trust_name']) < 1) || (utf8_strlen($this->request->post['education_trust_name']) > 255)) {
		// 	$this->error['education_trust_name'] = 'Please Enter Education Trust Name';
		// }

		if ((utf8_strlen($this->request->post['school_email']) < 1) || (utf8_strlen($this->request->post['school_email']) > 255)) {
			$this->error['school_email'] = 'Please Enter School Email';
		} else {
			if(!filter_var($this->request->post['school_email'], FILTER_VALIDATE_EMAIL)){
				$this->error['school_email'] = 'Please Enter Valid School Email';
			}
		}

		if ((utf8_strlen($this->request->post['school_mobile']) < 10) || (utf8_strlen($this->request->post['school_mobile']) > 10)) {
			$this->error['school_mobile'] = 'Please Enter School Mobile';
		}

		// if ((utf8_strlen($this->request->post['school_landline']) < 1) || (utf8_strlen($this->request->post['school_landline']) > 255)) {
		// 	$this->error['school_landline'] = 'Please Enter School Landline';
		// }

		if ((utf8_strlen($this->request->post['school_address']) < 1) || (utf8_strlen($this->request->post['school_address']) > 255)) {
			$this->error['school_address'] = 'Please Enter School Address';
		}

		if ((utf8_strlen($this->request->post['school_pincode']) < 1) || (utf8_strlen($this->request->post['school_pincode']) > 255)) {
			$this->error['school_pincode'] = 'Please Enter School Pincode';
		}

		if ((utf8_strlen($this->request->post['pt_teacher_name']) < 1) || (utf8_strlen($this->request->post['pt_teacher_name']) > 255)) {
			$this->error['pt_teacher_name'] = 'Please Enter PT Teacher Name';
		}

		if ((utf8_strlen($this->request->post['pt_teacher_email']) < 1) || (utf8_strlen($this->request->post['pt_teacher_email']) > 255)) {
			$this->error['pt_teacher_email'] = 'Please Enter PT Teacher Email';
		} else {
			if(!filter_var($this->request->post['pt_teacher_email'], FILTER_VALIDATE_EMAIL)){
				$this->error['pt_teacher_email'] = 'Please Enter Valid PT Teacher Email';
			}
		}

		if ((utf8_strlen($this->request->post['pt_teacher_contact']) < 10) || (utf8_strlen($this->request->post['pt_teacher_contact']) > 10)) {
			$this->error['pt_teacher_contact'] = 'Please Enter PT Teacher Contact';
		}

		if ((utf8_strlen($this->request->post['password']) < 1) || (utf8_strlen($this->request->post['password']) > 255)) {
			$this->error['password'] = 'Please Enter Password';
		}

		// if ($this->request->post['email'] != '') {
		// 	if(!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)){
		// 		$this->error['email'] = 'Please Enter Valid Email';
		// 	}
		// }

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/school')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		// foreach ($this->request->post['selected'] as $school_id) {
		// 	$product_total = $this->model_catalog_product->getTotalProductsBySchoolId($school_id);

		// 	if ($product_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
		// 	}
		// }

		return !$this->error;
	}

	public function upload() {
		$this->load->language('account/school');
		$json = array();
		
		$file_show_path = HTTP_SERVER.'system/storage/download/';
		$file_upload_path = DIR_DOWNLOAD.'/';
		$image_name = $this->request->get['image_name'].'_'.date('YmdHis');
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$raw_file_name = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
				$img_extension = strtolower(substr(strrchr($raw_file_name, '.'), 1));

				$filename = $image_name.'.'.$img_extension;//basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// $this->log->write(print_r($this->request->files, true));
				// $this->log->write($image_name);
				// $this->log->write($img_extension);
				// $this->log->write($filename);

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array();

				$extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

				$filetypes = explode("\n", $extension_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}
				$allowed[] = 'jpg';
				$allowed[] = 'jpeg';
				$allowed[] = 'png';
				$allowed[] = 'pdf';

				//$this->log->write(print_r($allowed, true));

				if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

				$filetypes = explode("\n", $mime_allowed);

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				//$this->log->write(print_r($this->request->files,true));

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!$json) {
			$file = $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $file_upload_path . $file);
			$destFile = $file_upload_path . $file;
			chmod($destFile, 0777);
			$json['filename'] = $file;
			$json['link_href'] = $file_show_path . $file;

			$json['success'] = $this->language->get('text_upload');
		}
		//sleep(5);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete5() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/item');

			$filter_data = array(
				'filter_item_code' => $this->request->get['filter_name'],
				
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_item->getItems($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'item_code' => $result['item_code'],
					'purchase_price' => $result['purchase_price'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function autocompletename() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/item');

			$filter_data = array(
				'filter_item_name' => $this->request->get['filter_name'],
				
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_item->getItems($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'item_code' => $result['item_code'],
					'purchase_price' => $result['purchase_price'],
					'item_name'        => strip_tags(html_entity_decode($result['item_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['item_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	public function tableinfo() {
			$tid =$this->request->get['tid'];
			$lid=$this->request->get['lid'];
			$tableinfo =  $this->db->query("SELECT  * FROM oc_order_info WHERE `location_id` ='".$lid."' AND `table_id` = '".$tid."' AND `date` = '" . $this->db->escape(DATE('Y-m-d')) . "' AND bill_status = '0' ORDER BY date DESC LIMIT 1 ");
			$data['html'] = '';
			if($tableinfo->num_rows>0)
			{
			$order =	$tableinfo->row['order_id'];
			$tableitems = $this->db->query("SELECT  * FROM oc_order_items WHERE order_id = '".$tableinfo->row['order_id']."' ")->rows;
		$data['i']=1;
		 foreach ($tableinfo->row as $tkey => $tinfo)
		 {

		 	$data[$tkey] = $tinfo;
		 }
		 $data['html'] .= '<tr>
					<td>No.</td>
					<td>Code</td>
					<td>Name</td>
					<td>QTY</td>
					<td>Rate</td>
					<td>AMT</td>
					<td>Test</td>
					<td>Delete</td>
			  </tr>';
		 foreach ($tableitems as $lkey => $items) {
				
		 	$data['html'] .= '<tr><td>'.$data['i'].' </td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" class="inputs code" name="po_datas['.$data['i'].'][code]" value="'.$items['code'].'" id="code_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td><input type="text" class="inputs names" name="po_datas['.$data['i'].'][name]" value="'.$items['name'].'" id="name_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][qty]" value="'.$items['qty'].'" class="inputs qty" id="qty_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][rate]" value="'.$items['rate'].'" class="inputs" id="rate_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:20%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][amt]" value="'.$items['amt'].'" class="inputs" id="amt_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:15%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][test]" value="'.$items['test'].'" class="inputs lst" id="test_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="text-align: left;">
					  -
					</td>
					 </tr>';

	 	$data['i']++;
		}

		 	$data['html'] .= '<tr><td>'.$data['i'].' </td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" class="inputs code" name="po_datas['.$data['i'].'][code]" value="" id="code_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td><input type="text" class="inputs names" name="po_datas['.$data['i'].'][name]" value="" id="name_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][qty]" value="" class="inputs qty" id="qty_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:10%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][rate]" value="" class="inputs" id="rate_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:20%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][amt]" value="" class="inputs" id="amt_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="width:15%"><input type="text" style="width:100%" name="po_datas['.$data['i'].'][test]" value="" class="inputs lst" id="test_'.$data['i'].'" /></td>';
		 	$data['html'] .= '<td style="text-align: left;">
					  -
					</td>
					 </tr>';
					 $data['html'] .= '<input type="hidden" style="width:100%" name="orderid" value="'.$order.'"  id="orderid" />';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	public function tables() {
			$lname =$this->request->get['lname'];
			$id=$this->request->get['id'];
			$locname= "'".$lname ."'";
			$locations =  $this->db->query("SELECT  * FROM oc_table WHERE location_id ='".$id."'")->rows;
			
		$i=1;
		$data['html'] = '';
		foreach ($locations as $lkey => $loc) {
				$tname= "'".$loc['name']."'";
			$data['html'] .= '<a  style="margin-bottom: 2%;margin-right: 5%;margin-left: 2%;background-color: green;border-color: green;font-size: 280%;width: 10%;" data-toggle="tooltip" onClick="cat('.$loc['table_id'].','.$id.','.$locname.','.$tname.')" title='.$loc['name'].' class="btn btn-primary" >'.$i.'</a>';
			$i++;
		}
		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	public function additem() {
			$itemid =$this->request->get['itemid'];
			$iteminfo =  $this->db->query("SELECT  * FROM oc_item WHERE item_id ='".$itemid."'")->row;
			
	
		$data['item_code'] = $iteminfo['item_code'];
		$data['item_name'] = $iteminfo['item_name'];
		$data['purchase_price'] = $iteminfo['purchase_price'];
		

		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	public function cat() {
			$lname =$this->request->get['lname'];
			$lname1 = "'".$lname."'";
			$lid=$this->request->get['lid'];
			$tname =$this->request->get['tname'];
			$tname1 = "'".$tname."'";
			$tid=$this->request->get['tid'];
			$categorys =  $this->db->query("SELECT  * FROM oc_category")->rows;
			
		$i=1;
		//$data['html'] = '<a  style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="table('.$lid.','.$lname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="table('.$lid.','.$lname1.')"> <img  src="http://localhost:8012/hotel/system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		
		foreach ($categorys as $ckey => $cat) {
				
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    <div class="product-thumb transition">
      <div class="image">
                  <a onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$cat['category_id'].')">
            <img  src="'.$cat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$cat['category'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          </a>
              </div>
      <div class="caption">
        <h5 style="text-align: center;">
          '.$cat['category'].'
        </h5>
      </div>
      
    </div>
  </div>';
			$i++;
		}
		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	public function subcat() {
			$lname =$this->request->get['lname'];
			$lname1 = "'".$lname."'";
			$lid=$this->request->get['lid'];
			$tname =$this->request->get['tname'];
			$tname1 = "'".$tname."'";
			$tid=$this->request->get['tid'];
			$catid=$this->request->get['catid'];
			$subcategorys =  $this->db->query("SELECT  * FROM oc_subcategory WHERE parent_id ='".$catid."'")->rows;
			
		$i=1;
		//$data['html'] = '<a  style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a><br>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.')"> <img  src="http://localhost:8012/hotel/system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		
		foreach ($subcategorys as $sckey => $scat) {
				
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    <div class="product-thumb transition">
      <div class="image">
                  <a onClick="items('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.','.$scat['category_id'].')">
            <img  src="'.$scat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$scat['name'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          </a>
              </div>
      <div class="caption">
        <h5 style="text-align: center;">
          '.$scat['name'].'
        </h5>
      </div>
      
    </div>
  </div>';
			$i++;
		}
		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
	public function items() {
			$lname =$this->request->get['lname'];
			$lname1 = "'".$lname."'";
			$lid=$this->request->get['lid'];
			$tname =$this->request->get['tname'];
			$tname1 = "'".$tname."'";
			$tid=$this->request->get['tid'];
			$catid=$this->request->get['catid'];
			$scatid=$this->request->get['scatid'];
			$items =  $this->db->query("SELECT  * FROM oc_item WHERE item_sub_category_id ='".$scatid."'")->rows;
			
		$i=1;
		//$data['html'] = '<a  style="margin-bottom: 2%;margin-right: 5%;font-size:150%;margin-left: 2%;background-color: blue;border-color: blue;width: 15%;" onClick="cat('.$tid.','.$lid.','.$lname1.','.$tname1.')" data-toggle="tooltip"  class="btn btn-primary" >BACK</a><br>';
		$data['html'] = '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;"><div class="image">';
		$data['html'] .= '<a style="margin-bottom: 2%;margin-right: 5%;" onClick="subcat('.$lid.','.$lname1.','.$tid.','.$tname1.','.$catid.')"> <img  src="http://localhost:8012/hotel/system/storage/download/back.jpg"  class="img-responsive" style="width:60%;margin-left: 20%;"></a></div></div></div>';
		
		foreach ($items as $sckey => $scat) {
				
			//$data['html'] .= '<div style="display:inline;"><a  style="margin-bottom: 2%;margin-right: 5%;font-size: 280%;margin-left: 2%;background-color: white;border-color: white;width: 10%;"  data-toggle="tooltip"  title='.$cat['name'].' class="btn btn-primary" ><img style="width: 150%;" src="'.$cat['image'].'"></a><div><lable  class="control-label" >'.$cat['name'].'</lable></div></div>';
			$data['html'] .= '<div class="product-layout col-lg-3 col-md-3 col-sm-2 col-xs-12" style="float: none !important;margin: 0 auto;display: inline-block;">
    <div class="product-thumb transition">
      <div class="image">
                  <a onClick="add('.$scat['item_id'].')">
            <img  src="'.$scat['photo_source'].'" alt="Navghar-Manikpur Kala Krida 2017" title="'.$scat['item_name'].'" class="img-responsive" style="width:60%;margin-left: 20%;">
          </a>
              </div>
      <div class="caption">
        <h5 style="text-align: center;">
          '.$scat['item_name'].'
        </h5>
      </div>
      
    </div>
  </div>';
			$i++;
		}
		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function autocomplete4() {
		$json = array();

		if (isset($this->request->get['filter_cname'])) {
			$this->load->model('catalog/school');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_cname'],
				
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_school->getcaptains($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'captain_id' => $result['captain_id'],
					'c_name'        => strip_tags(html_entity_decode($result['c_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['c_name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete3() {
		$json = array();

		if (isset($this->request->get['filter_wname'])) {
			$this->load->model('catalog/school');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_wname'],
				
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_school->getwaiters($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'waiter_id' => $result['waiter_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete2() {
		$json = array();

		if (isset($this->request->get['filter_tname'])) {
			$this->load->model('catalog/school');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_tname'],
				'filter_loc' => $this->request->get['filter_loc'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_school->gettables($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'table_id' => $result['table_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
public function autocomplete1() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/school');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_school->getlocations($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'location_id' => $result['location_id'],
					'location'        => strip_tags(html_entity_decode($result['location'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['location'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/school');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_school->getSchools($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'school_id' => $result['school_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}