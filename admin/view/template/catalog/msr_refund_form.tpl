<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button onclick="submit_fun()" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a style="display: none;" href="<?php echo $save; ?>" data-toggle="tooltip" title="<?php echo "Save"; ?>" class="btn btn-default"><i class="fa fa-save"></i></a>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form  method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		   			<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-2 control-label"><?php echo 'MSR No.'; ?></label>
						<div class="col-sm-4">
							<input type="text" name="msr_no" id="msr_no" placeholder="<?php echo 'MSR No.'; ?>" class="form-control"/>
						
						</div>
						
					   <label style="font-size: 16px;" class="col-sm-1 control-label"><?php echo 'Mobile No.'; ?></label>
						<div class="col-sm-4">
							<input type="text" name="mob_no" id="mob_no" placeholder="<?php echo 'Mobile No.'; ?>" class="form-control"/>
						
						</div>
					</div>
					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label"><?php echo 'Customer Info:'; ?></label>
					</div>

					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'MSR NO.'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="msr" id="msr" value="<?php echo $msr ?>" placeholder="<?php echo 'MSR'; ?>" class="form-control"/>
						</div>
						
					</div>

					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Contact'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="contact" id="contact" value="<?php echo $contact ?>" placeholder="<?php echo 'Contact'; ?>" class="form-control"/>
						</div>
					</div>
				    <div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Customer Name'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="customername" id="customername" value="<?php echo $customername ?>" placeholder="<?php echo 'Name'; ?>" class="form-control"/>
							<input type="hidden" name="c_id" id="c_id" value="<?php echo $c_id ?>" class="form-control"/>
							<?php /*<a target="_blank" href="<?php echo $add_customer; ?>" data-toggle="tooltip" title="<?php echo 'Add Customer'; ?>" class="btn btn-default"><i class="fa fa-plus"></i></a> */?>
						</div>
					</div>
					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Previous Balance'; ?></label>
						<div class="col-sm-5">
							<input type="text" readonly="readonly" id="pre_bal" placeholder="<?php echo 'Previous Balance'; ?>" class="form-control"/>
							<input type="hidden" name="pre_ball" id="pre_ball" value="<?php echo $pre_bal ?>" class="form-control"/>
							
						</div>
					</div>
					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Amount'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="amt" id="amt" placeholder="<?php echo 'Amount'; ?>" class="form-control amt_enter"/>
						</div>
					</div>

					<div  style = "display:none" class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Extra Amount'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="extra_amt" id="extra_amt" placeholder="<?php echo 'Extra Amount'; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Total Amount'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="total_amt" id="total_amt" placeholder="<?php echo 'Total Amount'; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Remark'; ?></label>
						<div class="col-sm-5">
							<input type="text" name="remark" id="remark" placeholder="<?php echo 'Remark'; ?>" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
					   <label style="font-size: 16px;" class="col-sm-4 control-label"><?php echo 'Pay By'; ?></label>
						<div class="col-sm-1">
							<input style="font-size: 18px;" type="radio" name="payby" id="cash" value="cash" checked="checked" />&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;">Cash</span>
						</div>
		 						<div class="col-sm-1">
							<input style="font-size: 18px;"type="radio" name="payby" id="card" value="card"/>&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;">Card</span>
						</div>
						<div class="col-sm-1">
								<input style="font-size: 18px;"type="radio" name="payby" id="online" value="online"/>&nbsp;&nbsp;&nbsp;<span style="font-size: 18px;">Online</span>
							</div>
						</div>
						
				</form>
				<div class="form-group" style="padding-left: 50%;">
       				<button style="padding: 2%;" onclick="submit_funs()"  data-toggle="tooltip" title="<?php echo "Save And Print" ?>" id="save_print" class="btn btn-primary"><i class="">Save And Print</i></button>
				</div>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">
  	$(document).on('keydown', '#msr_no, #mob_no,#amt', function(e) {
  		var code = (e.keyCode ? e.keyCode : e.which);
  		var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    var name = $(this).attr('name'); 
		    //alert(name);
		  	msr = $('#msr').val();
		  	msr_no = $('#msr_no').val();
		  	
		  	if(name == 'mob_no'){
		  		mob_search();
		  	}

		  	classss = $(this).attr('class');
			//alert(classss);

		  	if(classss == 'form-control amt_enter'){
		  		amt = $('#amt').val();
		  		//alert(amt);
		  		if(amt != ''){
			  		submit_fun();
			    	return false;
				}
			}
		    e.preventDefault();
		   return false;
		  }
  		//console.log(code);
  		//alert(code);
  	});

  	$(document).ready(function(){
  		$('#msr_no').focus();
  	});

  	function submit_fun(){
  		amount = parseFloat($('#amt').val());
  		if(amount == '' || amount == '0' || isNaN(amount)){
			amount = 0;
		}

  		pre_bal = parseFloat($('#pre_ball').val());
  		if(pre_bal == '' || pre_bal == '0' || isNaN(pre_bal)){
			pre_bal = 0;
		}

		if(pre_bal >= amount && amount != 0) {
	  		$('#amt').removeClass("amt_enter");
			$('#save').attr('onclick','').unbind('click');
			$('#save_print').attr('onclick','').unbind('click');

			action = '<?php echo $action; ?>';
			action = action.replace("&amp;", "&");
			action = action.replace("&amp;", "&");
			//alert(action);
			$('#form-sport').attr("action", action); 
			$('#form-sport').submit();
			//e.preventDefault();
		    return false;
		} else {
			alert("Please Check Amount Properly...");
			return false;
		}
  		//alert('inn');
		
	}

	function submit_funs(){

  		amount = parseFloat($('#amt').val());
  		if(amount == '' || amount == '0' || isNaN(amount)){
			amount = 0;
		}

		//alert(amount);

  		pre_bal = parseFloat($('#pre_ball').val());
  		if(pre_bal == '' || pre_bal == '0' || isNaN(pre_bal)){
			pre_bal = 0;
		}
		//alert(pre_bal);

		if(pre_bal >= amount && amount != 0) {
	  		$('#amt').removeClass("amt_enter");
			$('#save_print').attr('onclick','').unbind('click');

			action = '<?php echo $action; ?>';
			action = action.replace("&amp;", "&");
			action = action.replace("&amp;", "&");
			//alert(action);
			$('#form-sport').attr("action", action); 
			$('#form-sport').submit();
			//e.preventDefault();
		    return false;
		} else {
			alert("Please Check Amount Properly...");
			return false;
		}
		    return false;

  		//alert('inn');
		
	}
  	
  	// $("#amt").change(function(){
  	$(document).on('keyup', '#amt', function(e) {

  		amount = parseFloat($('#amt').val());
  		if(amount == '' || amount == '0' || isNaN(amount)){
			amount = 0;
		}
  		pre_bal = parseFloat($('#pre_ball').val());
  		if(pre_bal == '' || pre_bal == '0' || isNaN(pre_bal)){
			pre_bal = 0;
		}
  		ext_amt = parseFloat($('#extra_amt').val());
  		if(ext_amt == '' || ext_amt == '0' || isNaN(ext_amt)){
			ext_amt = 0;
		}
  		total_amt = amount - pre_bal - ext_amt;
  		total_amt = Math.abs(total_amt);
  		$('#total_amt').val(total_amt);

  		//ext_amt = $('#extra_amt').val();
  	});


  	$("#extra_amt").change(function(){
  		amount = parseFloat($('#amt').val());
  		if(amount == '' || amount == '0' || isNaN(amount)){
			amount = 0;
		}
  		pre_bal = parseFloat($('#pre_ball').val());
  		if(pre_bal == '' || pre_bal == '0' || isNaN(pre_bal)){
			pre_bal = 0;
		}
  		ext_amt = parseFloat($('#extra_amt').val());
  		if(ext_amt == '' || ext_amt == '0' || isNaN(ext_amt)){
			ext_amt = 0;
		}
  		total_amt = amount - pre_bal - ext_amt;
  		total_amt = Math.abs(total_amt);
  		if(total_amt == '' || total_amt == '0' || isNaN(total_amt)){
			total_amt = 0;
		}
  		$('#total_amt').val(total_amt);

  		//ext_amt = $('#extra_amt').val();


  	});

  	function mob_search(){
		mob_no = $('#mob_no').val();
		//alert(mob_no);
    	$.ajax({
      		url: 'index.php?route=catalog/msr_refund/mob_search&token=<?php echo $token; ?>&filter_mob_no=' +mob_no,
      		dataType: 'json',
      		success: function(json) {   
    			//console.log(json);
    			$('#customername').val(json.name);
			  	$('#c_id').val(json.c_id);
			  	$('#contact').val(json.contact);
			  	$('#mob_no').val(json.contact);
			  	$('#msr').val(json.msr_no);
			  	msr_noo = json.msr_no;
			  	c_id = $('#c_id').val();
			  	//alert(c_id);
			  	$.ajax({
				  	url:'index.php?route=catalog/msr_refund/getPreviousBal&token=<?php echo $token; ?>&cust_id='+c_id+'&msr_no='+ encodeURIComponent(msr_noo),
				  	type:'post',
				  	dataType: 'json',
				  	success:function(json){
				  		pre_balz = json.pre_bal
				  		if(pre_balz == '' || pre_balz == '0' || pre_balz == 'null' || pre_balz == null || isNaN(pre_balz)){
							pre_balz = 0;
						}
				  		$('#pre_bal').val(pre_balz);
				  		$('#pre_ball').val(pre_balz);
				  	}
			  	});
			  	$('#amt').focus();
      		}

    	});
	}
  	// $(document).ready(function(){
	  // 	$("#msr_no").change(function(){
	    	$('#msr_no').autocomplete({
			  	delay: 500,
			  	source: function(request, response) {
			    	$.ajax({
			      		url: 'index.php?route=catalog/msr_refund/autocompletecustmsrno&token=<?php echo $token; ?>&filter_msr_no=' +  encodeURIComponent(request.term),
			      		dataType: 'json',
			      		success: function(json) {   
			        		response($.map(json, function(item) {
			        			//console.log(item);
			        			$('#customername').val(item.name);
							  	$('#c_id').val(item.c_id);
							  	$('#contact').val(item.contact);
							  	$('#msr').val(item.msr_no);
							  	$('#mob_no').val(item.contact);


							  	// $('#pre_bal').val(item.pre_bal);
							  	// $('#pre_ball').val(item.pre_bal);
							  	msr_noo = item.msr_no;
							  	c_id = $('#c_id').val();
							  	//alert(c_id);
							  	$.ajax({
								  	url:'index.php?route=catalog/msr_refund/getPreviousBal&token=<?php echo $token; ?>&cust_id='+c_id+'&msr_no='+ encodeURIComponent(msr_noo),
								  	type:'post',
								  	dataType: 'json',
								  	success:function(json){
								  		pre_balz = json.pre_bal
								  		if(pre_balz == '' || pre_balz == '0' || pre_balz == 'null' || pre_balz == null || isNaN(pre_balz)){
											pre_balz = 0;
										}
								  		$('#pre_bal').val(pre_balz);
								  		$('#pre_ball').val(pre_balz);
								  	}
							  	});
			          			
			        		}));
			        		$('#amt').focus();
			      		}

			    	});
			  	}, 
				
		  		focus: function(event, ui) {

		    		return false;
		  		}
			});
	//   	});
	// });
  	</script>
</div>
<?php echo $footer; ?>