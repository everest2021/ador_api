<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       	<div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
									    	<select class="form-control" name="filter_category">
										    	<option selected="selected" value="99">All</option>
										    	<?php foreach($categorys as $key => $value) { ?>
										    	<?php if($key == $filter_category) { ?>
										    		<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
										    	<?php } else {?>
										    		<option value="<?php echo $key ?>"><?php echo $value?></option>
										    	<?php } ?>
										    	<?php } ?>
									    	</select>
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 		<button style="display: none;" id="new" type="button" class="btn btn-primary">new</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="pdfprint" type="button" class="btn btn-primary">Export</button>
				 	</div>
				 	<?php if(isset($_POST['submit']) ){ ?>
						<div class="col-sm-6 col-sm-offset-3">
						<center style = "display:none;">
							<?php echo HOTEL_NAME ?>
							<?php echo HOTEL_ADD ?>
						</center>
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Day Summary Report</b></h3></center>
						<h3>From : <?php echo $startdate; ?></h3>
						<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
						  <table class="table table-bordered table-hover" style="text-align: center;">
							<tr>
								<th style="text-align: center;">Login Summary</th>
								<th style="text-align: right;" colspan="4">Amount</th>
							</tr>
							<?php $usertotal = 0; ?>
							<?php if($orderdataarrays != array()){ ?>
								<?php foreach($orderdataarrays as $key => $value) { ?>
									<tr>
										<td><b>User :<?php echo $value['orderdata']['login_id']?></b></td>
										<td><b><?php echo $value['orderdata']['login_name']?></b></td>
										<td>T.Sale</td>
										<td><?php echo $value['orderdata']['netsale'] ?></td>
									</tr>
									<tr>
										<td>Usettle Bill Amount</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['pendingamount'] ?></td>
									</tr>
									<tr>
										<td>Cash</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['total_cash']?></td>
									</tr>
									<tr>
										<td>Credit Card</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['total_card']?></td>
									</tr>
									<tr>
										<td>Online</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['total_pay_online']?></td>
									</tr>
									<tr>
										<td>on/Ac</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['onac']?></td>
									</tr>
									<tr>
										<td>Meal Pass</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['total_mealpass']?></td>
									</tr>
									<tr>
										<td>Pass</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['total_pass']?></td>
									</tr>
									<tr>
									<td>MSR Card</td>
									<td></td>
									<td></td>
									<td><?php echo $value['orderdata']['msr']?></td>
								</tr>
									<!-- <tr>
										<td>Pending table</td>
										<td></td>
										<td><?php //echo $value['orderdata']['pendingbill'] ?></td>
										<td><?php //echo $value['orderdata']['pendingamount'] ?></td>
									</tr> -->
									<tr>
										<td>Complement Bill</td>
										<td></td>
										<td><?php echo $value['orderdata']['complimentarycount'] ?></td>
										<td><?php echo $value['orderdata']['complimentarybill'] ?></td>
									</tr>
									<tr>
										<td>Canceled Bill</td>
										<td></td>
										<td><?php echo $value['orderdata']['cancelledcount'] ?></td>
										<td><?php echo $value['orderdata']['cancelledbill'] ?></td>
									</tr>
									<tr>
										<td>Canceled Kot</td>
										<td></td>
										<td><?php echo $value['orderdata']['cancelkot'] ?></td>
										<td><?php echo $value['orderdata']['cancelamountkot'] ?></td>
									</tr>
									<tr>
										<td>Canceled Bot</td>
										<td></td>
										<td><?php echo $value['orderdata']['cancelbot'] ?></td>
										<td><?php echo $value['orderdata']['cancelamountbot'] ?></td>
									</tr>
									<tr>
										<td>NC KOT</td>
										<td></td>
										<td><?php echo $value['orderdata']['totalnc'] ?></td>
										<td><?php echo $value['orderdata']['totalamountnc'] ?></td>
									</tr>
									<tr>
										<td>DUPLICATE BILL</td>
										<td></td>
										<td><?php echo $value['orderdata']['duplicatecount'] ?></td>
										<td><?php echo $value['orderdata']['duplicate'] ?></td>
									</tr>
									<tr>
										<td>Disc Before Bill</td>
										<td></td>
										<td><?php echo $value['orderdata']['discountbillcount'] ?></td>
										<td><?php echo $value['orderdata']['discountamount'] ?></td>
									</tr>
									<tr>
										<td>DUPLICATE KOT</td>
										<td></td>
										<td></td>
										<td>0.00</td>
									</tr>
									<tr>
										<td>Disc After Bill</td>
										<td></td>
										<td></td>
										<td>0.00</td>
									</tr>
									<tr>
										<td>Advance Amount (-)</td>
										<td></td>
										<td></td>
										<td><?php echo $value['orderdata']['advanceamount'] ?></td>
									</tr>
									
									
									<?php
										// $usertotal = $usertotal + $value['orderdata']['total_cash'] + $value['orderdata']['total_card']+ $value['orderdata']['total_pay_online'] + $value['orderdata']['onac'] + $value['orderdata']['pendingamount'] + $value['orderdata']['advanceamount'] + $value['orderdata']['msr'] ;
										$usertotal = round($usertotal + $value['orderdata']['netsale'] );
									?>
								<?php } ?>
								<!-- <tr>
									<td>Total Roundoff</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalroundoff'] ?></td>
								</tr> -->
								<tr>
									<td><b>All User Totals</b></td>
									<td></td>
									<td></td>
									<td><b><?php echo $usertotal ?></b></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td><b>Table Group</b></td>
									<td></td>
									<td></td>
									<td><b>Amount</b></td>
								</tr>
								<?php foreach($orderlocdatas as $locdata){ ?>
									<tr>
										<td><?php echo $locdata['location'] ?></td>
										<td></td>
										<td></td>
										<td><?php echo $locdata['netsale'] ?></td>
									</tr>
								<?php } ?>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Sub Total</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalnetsale'] ?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>KKC Amt(+)</td>
									<td></td>
									<td></td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>SBC Amt(+)</td>
									<td></td>
									<td></td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>S-Chrg Amt(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalscharge'] ?></td>
								</tr>
								<tr>
									<td>Vat(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalvat'] ?></td>
								</tr>
								<tr>
									<td>Gst(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalgst'] ?></td>
								</tr>
								<tr>
									<td>Packaging(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalpackaging']; ?></td>
								</tr>

								<tr>
									<td>Packaging CGST(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalpackaging_cgst']; ?></td>
								</tr>

								<tr>
									<td>Packaging SGST(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalpackaging_sgst']; ?></td>
								</tr>

								<tr>
									<td>R-Off Amt(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totalroundoff']; ?></td>
								</tr>

								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Discount Amt(-)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['totaldiscount'] ?></td>
								</tr>
								<tr>
									<td>P-Discount Amt(-)</td>
									<td></td>
									<td></td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>Cancel Bill Amt(-)</td>
									<td></td>
									<td></td>
									<td>0.00</td>
								</tr>

								<tr>
									<td>Advance. Amt(-)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['advance'] ?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Gross Total</td>
									<td></td>
									<td></td>
									<td><?php echo $orderlocdatasamt['nettotalamt'] ?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td><b>Payment Summary</b></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Cash</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['totalcash']?></td>
								</tr>
								<tr>
									<td>Credit Card</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['totalcard']?></td>
								</tr>
								<tr>
									<td>Online</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['totalpayonline']?></td>
								</tr>
								<tr>
									<td>On A/c</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['onac']?></td>
								</tr>
								<tr>
									<td>Meal Pass</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['mealpass']?></td>
								</tr>
								<tr>
									<td>Pass</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['pass']?></td>
								</tr>
								<tr>
									<td>Room Service</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['room']?></td>
								</tr>
								<tr>
									<td>MSR Card</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['msr']?></td>
								</tr>
								<tr>
									<td>Advance Amount (-)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['advanceamt']?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Total</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['total']?></td>
								</tr>
								<tr>
									<td>Card tip</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['tip_amount']?></td>
								</tr>
								<tr>
									<td>Advance Amt(+)</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['advance']?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td><b>Grand Total</b></td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['totalpaysumm']?></td>
								</tr>

								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td><b>Online Payment Summary</b></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>

								<tr>
									<td>Swiggy</td>
									<td></td>
									<td></td>
									<td><?php echo $onlineorderamt['swiggy_total']?></td>
								</tr>

								<tr>
									<td>Zomato</td>
									<td></td>
									<td></td>
									<td><?php echo $onlineorderamt['zomato_total']?></td>
								</tr>

								<tr>
									<td>Online</td>
									<td></td>
									<td></td>
									<td><?php echo $onlineorderamt['online_total']?></td>
								</tr>

								<tr>
									<td>Uber</td>
									<td></td>
									<td></td>
									<td><?php echo $onlineorderamt['uber_total']?></td>
								</tr>

								<tr>
									<td><b>Grand Total</b></td>
									<td></td>
									<td></td>
									<td><b><?php echo $onlineorderamt['g_tot']?></b></td>
								</tr>

								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td><b>Tax Details</b></td>
									<td></td>
									<td><b>Net Amt</b></td>
									<td><b>Tax Amt</b></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<?php foreach($ordertaxamt['liqtax'] as $value) { ?>
									<tr>
										<td>Vat</td>
										<td><?php echo $value['tax1'] ?> % On</td>
										<td><?php echo $value['amt'] ?> is</td>
										<td><?php echo $value['tax1_value'] ?></td>
									</tr>
								<?php } ?>
									<tr>
										<td><b>Total Vat</b></td>
										<td></td>
										<td><b><?php echo $ordertaxamt['liqnetamt'] ?></b></td>
										<td><b><?php echo $ordertaxamt['taxvalliq'] ?></b></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<?php foreach($ordertaxamt['foodtax'] as $value) { ?>
									<tr>
										<td>GST</td>
										<td><?php echo $value['tax1'] ?> % On</td>
										<td><?php echo $value['amt'] ?> is</td>
										<td><?php echo $value['tax1_value'] ?></td>
									</tr>
								<?php } ?>
								<tr><td colspan="5" height="35"></td></tr>
									<tr>
										<td>CGST</td>
										<td></td>
										<td></td>
										<td><?php echo $ordertaxamt['taxvalfood']/2 ?></td>
									</tr>
									<tr>
										<td>SGST</td>
										<td></td>
										<td></td>
										<td><?php echo $ordertaxamt['taxvalfood']/2 ?></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
									<tr>
										<td><b>Total Gst</b></td>
										<td></td>
										<td><b><?php echo $ordertaxamt['foodnetamt'] ?></b></td>
										<td><b><?php echo $ordertaxamt['taxvalfood'] ?></b></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
									<tr>
										<td><b>Category</b></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
									<tr>
										<td>Food</td>
										<td></td>
										<td></td>
										<td><?php echo $catsubs['foodcat']?></td>
									</tr>
									<tr>
										<td>Liquor</td>
										<td></td>
										<td></td>
										<td><?php echo $catsubs['liqcat'] ?></td>
									</tr>
									<tr>
										<td>Total</td>
										<td></td>
										<td></td>
										<td><?php echo $catsubs['foodcat'] + $catsubs['liqcat'] ?></td>
									</tr>
									<tr>
										<td>Discount</td>
										<td></td>
										<td></td>
										<td><?php echo $catsubs['cat_fdis'] + $catsubs['cat_ldis'] ?></td>
									</tr>
									<tr>
										<td><b>Category Net Total</b></td>
										<td></td>
										<td></td>
										<td><b><?php echo ($catsubs['foodcat'] + $catsubs['liqcat']) - ($catsubs['cat_fdis'] + $catsubs['cat_ldis']) ?></b></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
									<tr>
										<td><b>Sub Category</b></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
									<?php foreach($catsubs['subcat'] as $key => $value) { ?>
										<tr>
											<td><?php echo $value['name'] ?></td>
											<td></td>
											<td></td>
											<td><?php echo $value['amount'] ?></td>
										</tr>
									<?php } ?>
									<tr>
										<td><b>Total<b></td>
										<td></td>
										<td></td>
										<td><?php echo $catsubs['totalamt']?></td>
									</tr>
									<tr>
										<td>Discount</td>
										<td></td>
										<td></td>
										<td><?php echo  $catsubs['discountamt'] ?></td>
									</tr>
									<tr>
										<td><b>Sub Category Net Total</b></td>
										<td></td>
										<td></td>
										<td><b><?php echo $catsubs['totalamt'] - $catsubs['discountamt']?></b></td>
									</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>Last Bill No</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['lastbill'] ?></td>
									<td></td>
								</tr>
								<tr>
									<td>Bill Printed Count</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['bill_printed'] ?></td>
									<td></td>
								</tr>
								<tr>
									<td>Last Kot No</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['lastkot'] ?></td>
									<td></td>
								</tr>
								<tr>
									<td>Last Bot No</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['lastbot'] ?></td>
									<td></td>
								</tr>
								<tr>
									<td>Unsettle Bill Amt</td>
									<td></td>
									<!-- <td><?php echo $orderdatalastarray['orderdatalast']['pendingbill'] ?></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['pendingamount'] ?></td> -->
									<td><?php echo $pendingtablecount ?></td>
									<td><?php echo $pendingtable_amt ?></td>
								</tr>
								<tr>
									<td>Duplicate Bill</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['duplicatecount'] ?></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['duplicate'] ?></td>
								</tr>
								<tr>
									<td>Pending Table</td>
									<td></td>
									<td><?php echo $pendingtablecount ?></td>
									<td><?php echo $pendingtable_amt ?></td>
								</tr>
								<tr>
									<td>Compliment Bill</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['complimentarycount'] ?></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['complimentarybill'] ?></td>
								</tr>
								<tr>
									<td>Cancelled Bill</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelledcount'] ?></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelledbill'] ?></td>
								</tr>
								<tr>
									<td>Cancelled Kot</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelkot'] ?></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountkot'] ?></td>
								</tr>
								<tr>
									<td>Cancelled Bot</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelbot'] ?></td>
									<td><?php echo $orderdatalastarray['orderitemlast']['cancelamountbot'] ?></td>
								</tr>
								<tr>
									<td>NC KOT</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['totalnc'] ?></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['totalamountnc'] ?></td>
								</tr>
								<tr>
									<td>CHANGE BILL</td>
									<td></td>
									<td><?php echo $NumberOfbillmodify ?></td>
									<td><?php echo $totalbillamount ?></td>
								</tr>
								<tr>
									<td>Disc Before Bill</td>
									<td></td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>Disc after Bill</td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['discountbillcount'] ?></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['discountamount'] ?></td>
								</tr>
								<tr><td colspan="5" height="35"></td></tr>
								<tr>
									<td>CASH DROVEREST</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>OPENING BALANCE</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>CASH SALE</td>
									<td></td>
									<td></td>
									<td><?php echo $orderpaymentamt['totalcash']?></td>
								</tr>
								<tr>
									<td>PAID IN</td>
									<td></td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['paid_in']; ?></td>
								</tr>
								<tr>
									<td>PAID OUT</td>
									<td></td>
									<td></td>
									<td><?php echo $orderdatalastarray['orderdatalast']['paid_out']; ?></td>
								</tr>
								<tr>
									<td>TOTAL BALANCE IN DROVER</td>
									<td></td>
									<td></td>
									<td>0</td>
								</tr>
								<tr>
									<td>TIP</td>
									<td></td>
									<td></td>
									<td><?php echo $tip ?></td>
								</tr>
								<tr>
									<td>No. Of Persons</td>
									<td></td>
									<td></td>
									<td><?php echo $person ?></td>
								</tr>
								<tr>
									<td>Average Grand Total</td>
									<td></td>
									<td></td>
									<td><?php echo $average_grand_total ?></td>
								</tr>
							  </table>
							  <br>
						  <?php } ?>
						  <center><h5><b>End of Report</b></h5></center>
					 	</div>
				 	<?php } ?>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		$('#print').on('click', function() {

			// var forprintarray = JSON.parse('<?php echo $forprintarray ?>');
			// console.log(forprintarray);

		  var url = 'index.php?route=catalog/dailydaysummaryreport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});
		$('#pdfprint').on('click', function() {

			// var forprintarray = JSON.parse('<?php echo $forprintarray ?>');
			// console.log(forprintarray);

		  var url = 'index.php?route=catalog/dailydaysummaryreport/pdfprints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});


		$('#new').on('click', function() {

			// var forprintarray = JSON.parse('<?php echo $forprintarray ?>');
			// console.log(forprintarray);

		  var url = 'index.php?route=catalog/dailydaysummaryreport/newbtn&token=<?php echo $token; ?>';

		 
		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});


		$('#sendmail').on('click', function() {

			// var forprintarray = JSON.parse('<?php echo $forprintarray ?>');
			// console.log(forprintarray);

		  var url = 'index.php?route=catalog/dailydaysummaryreport/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});


		
		// function close_fun_1(){
		// 	window.location.reload();
		// }
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>