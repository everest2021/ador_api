<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				    <div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Customer Name'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="customername" id="customername" value="<?php echo $customername ?>" placeholder="<?php echo 'Name'; ?>" class="form-control"/>
							<input type="hidden" name="c_id" id="c_id" value="<?php echo $c_id ?>" class="form-control"/>
							<?php /*<a target="_blank" href="<?php echo $add_customer; ?>" data-toggle="tooltip" title="<?php echo 'Add Customer'; ?>" class="btn btn-default"><i class="fa fa-plus"></i></a> */?>
						</div>
					</div>
					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Contact'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="contact" id="contact" value="<?php echo $contact ?>" placeholder="<?php echo 'Contact'; ?>" class="form-control"/>
						</div>
					</div>
					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Balance'; ?></label>
						<div class="col-sm-3">
							<input type="text" readonly="readonly" id="balance" placeholder="<?php echo 'Balance'; ?>" class="form-control"/>
						</div>
					</div>
					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'Credit'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="credit" id="Credit" placeholder="<?php echo 'Credit'; ?>" class="form-control"/>
						</div>
					</div>
					<div class="form-group">
					   <label class="col-sm-3 control-label"><?php echo 'MSR No.'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="msr_no" id="msr_no" placeholder="<?php echo 'MSR No.'; ?>" class="form-control"/>
						</div>
					</div>
				</form>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">
    $('#customername').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/customercredit/autocompletecustname&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.name + " - " + item.contact,
	            value: item.name,
	            contact:item.contact,
	            c_id:item.c_id,
	            balance: item.balance
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
	  	$('#customername').val((ui.item.label).substring(0, ui.item.label.indexOf('-')));
	  	$('#c_id').val(ui.item.c_id);
	  	$('#contact').val(ui.item.contact);
	  	$('#balance').val(ui.item.balance);
	    return false;
	  },
	  focus: function(event, ui) {

	    return false;
	  }
	});
  	</script>
</div>
<?php echo $footer; ?>