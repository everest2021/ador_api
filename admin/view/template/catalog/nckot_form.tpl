<?php echo $header; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
  <div class="page-header">
  </div>
  <div class="container-fluid">
	<?php $tab_index = 1; ?>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-order" class="form-horizontal">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 style= control-label">Table No :</label>
					<div class="col-sm-2">
				  		<input autocomplete="off" type="text" name="table" value="<?php echo $table ?>" placeholder="<?php echo 'Table'; ?>" id="input-t_number" class="form-control inputs" />
				  		<?php $tab_index ++; ?>
				  		<input type="hidden" name="table_id" value="" id="input-t_number_id" class="form-control inputs" />
			  		</div>
			  		<div class="col-sm-2">
		  				<a id="form-order" onclick="save()" title="Update" class="btn btn-primary" style="font-size:20px;">Update</a>
			  		</div>
		  		</div>
			</div>
			<div class="panel-body" style="padding-bottom: 0%;">
				<div style="display:inline-block; width:100%;float:left;">
			  		<div style="width:100%;">
						<div class="col-sm-3">
					  		<input type="text" readonly="readonly" name="location" value="" placeholder="<?php echo 'location'; ?>" id="input-location" class="form-control inputs list" style="display: none"/>
					  		<input type="hidden" name="location_id" value="" id="input-location_id" class="form-control" />
						</div>
			  		</div>
			  		<div style="width:100%; float:left;padding-top: 1%;padding-bottom: 5%;">
						<table style="" class="table table-bordered table-hover ordertab">
					  		<tr>
								<td style="padding-top: 2px;padding-bottom: 2px;">No.</td>
								<td style="width:0%;padding-top: 2px;padding-bottom: 2px;"></td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Code</td>
								<td style="width:0%;padding-top: 2px;padding-bottom: 2px;">Name</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Qty</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Rate</td>
								<td style="width:10%;padding-top: 2px;padding-bottom: 2px;">Amt</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">KOT</td>
								<td style="width:20%;padding-top: 2px;padding-bottom: 2px;">Reason</td>
					  		</tr>
						</table>

			  		</div>
				</div>
			</div>
	    </form>
	    <div class="col-sm-6">
			<a id="form-order" onclick="save()" title="Update" class="btn btn-primary" style="font-size:20px;">Update</a>
		</div>
	</div>
</div>

<script type="text/javascript">
var tab_index = '<?php echo $tab_index; ?>';
$('.readonly').attr('readonly',"readonly");
</script>
<script type="text/javascript">
function findtable(){
	var filter_table = $('input[name=\'table\']').val();
	if(filter_table != '' && filter_table != undefined){
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/nckot/findtable&token=<?php echo $token; ?>&filter_tname=' +filter_table,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					$('input[name=\'table\']').val(data.name);
					$('input[name=\'table_id\']').val(data.table_id);
					$('input[name=\'location\']').val(data.loc_name);
					$('input[name=\'location_id\']').val(data.loc_id);
					$('input[name=\'rate_id\']').val(data.rate_id);
					tablein();
					$('#input-waiter').focus();
				} else {
					alert('Table Does Not Exist');
					//window.location.reload();
					return false;
				}
			}
		});
	}
	return false;
}
function tablein(){
    var location_id = $('input[name=\'location_id\']').val();
    var table_id = $('input[name=\'table_id\']').val();
    $.ajax({
		type: "POST",
		url: 'index.php?route=catalog/nckot/tableinfo&token=<?php echo $token; ?>&lid='+location_id+'&tid='+table_id+'&tab_index='+tab_index,
		dataType: 'json',
		data: {"data":"check"},
		success: function(data){
			var html =data.html;
			ur = '';
			if(html != '') {
		  		$('.ordertab').html('');
		  		$('.ordertab').append(data.html);
		  		$('.readonly').attr('readonly',"readonly");
			} else {
				alert("Table does not exist");
			}
		}
  	});
}

$(document).on('keydown', '.inputs', function(e) {
  	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
  		var name = $(this).attr('name'); 
  		var class_name = $(this).attr('class'); 
  		var id = $(this).attr('id'); 
  		event.preventDefault();
  		if(name == 'table'){
	  		findtable();
  		}
	}
});

function close_fun_1(){
	window.location.reload();
}

$(document).ready(function() {
  $('#input-t_number').focus();
});

function close_fun(){
  window.location.reload();
}
function save(){
  	action = '<?php echo $action; ?>';
  	action = action.replace("&amp;", "&");
  	$.post(action, $('#form-order').serialize()).done(function(data) {
    	var parsed = JSON.parse(data);
     	if(parsed.info == 1){
     		$('.sucess').html('');
     		$('.sucess').append(parsed.done);
     	} else {
     		alert("Nothing is changed");
     	}
	});
}
</script>
<?php echo $footer; ?>