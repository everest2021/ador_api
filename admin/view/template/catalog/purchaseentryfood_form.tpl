<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="container-fluid">
		<?php /*  <h1><?php echo $heading_title; ?></h1>
	  	<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } echo "kevin";?>
	  	</ul> */ ?>
	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
	  		<div class="panel-heading" style="padding-bottom: 15px;">
				<h3><i class="fa fa-pencil"></i> <?php echo $text_form; ?>   </h3>
				<div class="pull-right">
					<?php if($edit == 1){ ?>
		  				<button type="button" onClick="submit_fun()" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
					<?php } else { ?>
						<button type="button" onClick="print()" data-toggle="tooltip" title="Print" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>
		  				<button type="button" onClick="submit_fun()" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
					<?php } ?>
						<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	  			</div>

	  			<div class="panel-body">
					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		  				<div class="form-group">
							<label class="col-sm-2 control-label" for="input-mname"><?php echo 'Supplier'; ?></label>
							<div class="col-sm-3 sup_div">
						  		<select tabindex="1" style="padding: 0px 1px;" name="supplier_id" id="input-supplier_id" class="form-control inputs">
				                  	<option value="">Please Select</option>
			                  		<?php foreach($suppliers as $ukey => $uvalue){ ?>
			                  			<?php if($ukey == $supplier_id){ ?>
			                  				<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
			                  			<?php } else { ?>
			                  				<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
			                  			<?php } ?>
			                  		<?php } ?>
				                </select>
						  		<input type="hidden" name="supplier_name" value="<?php echo $supplier_name ?>" placeholder="<?php echo 'Supplier Name'; ?>" style="" id="input-supplier_name" class="form-control" />
						  		<!-- <input type="hidden" name="supplier_id" value="<?php //echo $supplier_id ?>" id="input-supplier_id" class="form-control" style="" /> -->
						  		<input type="hidden" name="purchase_id" value="<?php echo $id; ?>" id="purchase_id"/>
						  	</div>
						  	<label class="col-sm-2 control-label" for="input-filter_invoice_no"><?php echo 'Invoice Number'; ?></label>
							<div class="col-sm-3">
								<input tabindex="2" type="text" name="filter_invoice_no" value="<?php echo $filter_invoice_no; ?>" placeholder="<?php echo 'Search Invoice Number'; ?>" id="input-filter_invoice_no" class="form-control inputs" />
							</div>
					  	</div>
					  	<div class="form-group" style="text-align: center;display: none;">
					  		<a onClick="search_fun();" class="btn btn-primary">Search</a>
					  	</div>
		  				<div class="form-group">
		  					<label class="col-sm-2 control-label" for="input-invoice_no"><?php echo 'Invoice Number'; ?></label>
							<div class="col-sm-3">
								<input tabindex="3" readonly="readonly" type="text" name="invoice_no" value="<?php echo $invoice_no; ?>" placeholder="<?php echo 'Invoice Number'; ?>" id="input-invoice_no" class="form-control inputs" />
							</div>
							<label class="col-sm-2 control-label" for="input-date"><?php echo 'Date'; ?></label>
							<div class="col-sm-3 date">
			  					<input tabindex="4" readonly="readonly" type="text" name="invoice_date" value="<?php echo $invoice_date; ?>" placeholder="<?php echo 'Invoice Date'; ?>" id="input-invoice_date" class="form-control inputs" />
			  					<span class="input-group-btn">
									<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
			  					</span>
							</div>
							
		  				</div>
		  				<div class="form-group">
							<label class="col-sm-2 control-label" for="input-store"><?php echo 'Store'; ?></label>
							<div class="col-sm-3">
			  					<select tabindex="5" style="padding: 0px 1px;" name="store_id" id="input-store_id" class="form-control inputs">
				                  	<option value="">Please Select</option>
			                  		<?php foreach($stores as $ukey => $uvalue){ ?>
			                  			<?php if($ukey == $store_id){ ?>
			                  				<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
			                  			<?php } else { ?>
			                  				<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
			                  			<?php } ?>
			                  		<?php } ?>
				                </select>
			  					<input type="hidden" name="store_name" value="<?php echo $store_name ?>" placeholder="<?php echo 'Store Name'; ?>" style="" id="input-store_name" class="form-control" />
						  		<!-- <input type="hidden" name="store_id" value="<?php //echo $store_id ?>" id="input-store_id" class="form-control" style="" /> -->
							</div>
							<label class="col-sm-2 control-label" for="input-update_master"><?php echo 'Update Stock'; ?></label>
							<div class="col-sm-3">
			  					<?php if($update_master == 1){ ?>
			  						<input tabindex="6" checked="checked" type="checkbox" name="update_master" value="1" placeholder="<?php echo 'Update Stock'; ?>" id="input-update_master" class="form-control inputs" />
								<?php } else { ?>
									<input tabindex="6" type="checkbox" name="update_master" value="1" placeholder="<?php echo 'Update Stock'; ?>" id="input-update_master" class="form-control inputs" />
								<?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-category"><?php echo 'Category'; ?></label>
							<div class="col-sm-3">
		  						<input readonly="readonly" tabindex="7" style="padding: 0px 1px;" name="category" value="Food" id="input-category" class="form-control">
							</div>

							<label class="col-sm-2 control-label batch_no" for="input-batch_no"><?php echo 'Batch No'; ?></label>
							<div class="col-sm-3 batch_no">
								<input tabindex="8" type="text" name="batch_no" value="<?php echo $batch_no ?>" placeholder="<?php echo 'Batch No'; ?>" id="input-batch_no" class="form-control inputs batch_no" />
							</div>

							<label style="display:none" class="col-sm-2 control-label tp_no" for="input-tp_no"><?php echo 'TP No'; ?></label>
							<div style="display:none" class="col-sm-3 tp_no">
								<input tabindex="8" type="text" name="tp_no" value="<?php echo $tp_no ?>" placeholder="<?php echo 'TP No'; ?>" id="input-tp_no" class="form-control inputs tp_no" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-narration"><?php echo 'Narration'; ?></label>
							<div class="col-sm-3">
		  						<input tabindex="9" type="text" name="narration" value="<?php echo $narration ?>" placeholder="<?php echo 'Narration'; ?>" id="input-narration" class="form-control inputs" />
							</div>
							<label style="display: none" class="col-sm-2 control-label item_type_class" for="input-item_type"><?php echo 'Item Type'; ?></label>
							<div style="display: none" class="col-sm-3 item_type_class">
		  						<select tabindex="10" style="padding: 0px 1px;" name="item_type" id="input-item_type" class="form-control">
				                  	<option value="">Please Select</option>
				                </select>
							</div>
						</div>
		  
		  				<div class="form-group"style="padding: 5px;">
							<table style="width:100%;margin-left:0%;margin-top:18px;" class="table table-bordered table-hover ordertab" id="ordertab">
								<tr>
								  <td style="text-align: center;">Description</td>
								  <td style="text-align: center;width: 7%;">Qty</td>
								  <td style="text-align: center;width: 5%;">Available Qty</td>
								  <td style="text-align: center;width: 5%;">Unit</td>
								  <td style="text-align: center;width: 7%;">Rate</td>
								  <td style="text-align: center;width: 5%;">Free Qty</td>
								  <td style="text-align: center;width: 7%;">Discount (%)</td>
								  <td style="text-align: center;width: 7%;">Discount (Rs)</td>
								  <td style="text-align: center;width: 7%;">Tax (%)</td>
								  <td style="text-align: center;width: 7%;">Tax (Rs)</td>
								  <td style="text-align: center;width: 7%;">Amount</td>
								  <td style="text-align: center;width: 7%;">Exp Date</td>
								  <td style="width:3%;text-align: center;">Action</td>
								</tr>
								<?php $extra_field_row = 1; ?>
				  				<?php $tab_index = 11; ?>
						  		<?php if($po_datas) { ?>
									<?php foreach ($po_datas as $pkey => $po_data) { ?>
										<tr id="re_<?php echo $extra_field_row ?>">
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas[<?php echo $extra_field_row ?>][description_code_search]" value="<?php echo $po_data['description_code_search']; ?>" class="inputs code form-control" id="description-code-search_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 55%;display: inline;"  type="text" class="inputs description form-control" name="po_datas[<?php echo $extra_field_row ?>][description]" value="<?php echo $po_data['description']; ?>"  id="description_<?php echo $extra_field_row ?>" />
												
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_id]" value="<?php echo $po_data['description_id']; ?>" class="inputs" id="description-id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_code]" value="<?php echo $po_data['description_code']; ?>" class="inputs" id="description-code_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_barcode]" value="<?php echo $po_data['description_barcode']; ?>" class="inputs" id="description-barcode_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][id]" value="<?php echo $po_data['id']; ?>" class="inputs" id="id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][p_id]" value="<?php echo $po_data['p_id']; ?>" class="inputs" id="p_id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][item_category]" value="<?php echo $po_data['item_category']; ?>" class="inputs" id="item-category_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][type_id]" value="<?php echo $po_data['type_id']; ?>" class="inputs" id="type_id_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas[<?php echo $extra_field_row ?>][qty]" value="<?php echo $po_data['qty']; ?>" id="qty_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_qty]" value="<?php echo $po_data['qty']; ?>" class="inputs" id="hidden-qty_<?php echo $extra_field_row ?>" />
												<?php if (isset($error_po_datas[$extra_field_row]['qty'])) { ?>
							                    	<div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['qty']; ?></div>
							                    <?php } ?>
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" class="inputs avq form-control" name="po_datas[<?php echo $extra_field_row ?>][avq]" value="<?php echo $po_data['avq']; ?>" id="avq_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<!-- <input tabindex="<?php echo $tab_index; ?>" type="text" class="inputs units form-control" name="po_datas[<?php echo $extra_field_row ?>][unit]" value="<?php //echo $po_data['unit']; ?>"  id="unit_<?php echo $extra_field_row ?>" /> -->
												<!-- <input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" value="<?php //echo $po_data['unit_id']; ?>" class="inputs" id="unit-id_<?php echo $extra_field_row ?>" />			 -->
												<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" id="unit-id_<?php echo $extra_field_row; ?>" class="inputs units form-control">
								                  	<option value="">Please Select</option>
							                  		<?php foreach($po_data['units'] as $ukey => $uvalue){ ?>
							                  			<?php if($ukey == $po_data['unit_id']){ ?>
							                  				<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
							                  			<?php } else { ?>
							                  				<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
							                  			<?php } ?>
							                  		<?php } ?>
								                </select>
												<input type="hidden" class="" name="po_datas[<?php echo $extra_field_row ?>][unit]" value="<?php echo $po_data['unit']; ?>"  id="unit_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][rate]" value="<?php echo $po_data['rate']; ?>" class="inputs rates form-control" id="rate_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_rate]" value="<?php echo $po_data['rate']; ?>" class="inputs" id="hidden-rate_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][free_qty]" value="<?php echo $po_data['free_qty']; ?>" class="inputs form-control" id="free-qty_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_free_qty]" value="<?php echo $po_data['free_qty']; ?>" class="inputs" id="hidden-free-qty_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][discount_percent]" value="<?php echo $po_data['discount_percent']; ?>" class="inputs discounts form-control" id="discount-percent_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][discount_amount]" value="<?php echo $po_data['discount_amount']; ?>" class="inputs discount_value form-control" id="discount-amount_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" readonly="readonly" type="text" class="inputs taxes form-control" name="po_datas[<?php echo $extra_field_row ?>][tax_name]" value="<?php echo $po_data['tax_name']; ?>"  id="tax-name_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][tax_percent]" value="<?php echo $po_data['tax_percent']; ?>" class="inputs" id="tax-percent_<?php echo $extra_field_row ?>" />
												<?php /* ?>
												<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][tax_percent]" id="tax-percent_<?php echo $extra_field_row ?>" class="form-control">
								                  	<option value="">Please Select</option>
							                  		<?php foreach($taxes as $ukey => $uvalue){ ?>
							                  			<?php if($ukey == $po_data['tax_percent']){ ?>
							                  				<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
							                  			<?php } else { ?>
							                  				<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
							                  			<?php } ?>
							                  		<?php } ?>
								                </select>
												<?php */ ?>
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][tax_value]" value="<?php echo $po_data['tax_value']; ?>" class="inputs form-control" id="tax-value_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][amount]" value="<?php echo $po_data['amount']; ?>" class="inputs form-control" id="amount_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][exp_date]" value="<?php echo $po_data['exp_date']; ?>" class="inputs form-control date" id="exp-date_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">
								  				<a tabindex="<?php echo $tab_index; ?>" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('<?php echo $extra_field_row ?>')" class="button inputs remove " id="remove_<?php echo $extra_field_row ?>" ><i class="fa fa-trash-o"></i></a>
												<?php $tab_index ++; ?>
											</td>
								 		</tr>
										<?php $extra_field_row++; ?>
									<?php } ?>
								<?php } else { ?>
									<tr id="re_<?php echo $extra_field_row ?>">
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas[<?php echo $extra_field_row ?>][description_code_search]" value="" class="inputs code form-control" id="description-code-search_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 55%;display: inline;"  type="text" class="inputs description form-control" name="po_datas[<?php echo $extra_field_row ?>][description]" value=""  id="description_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_id]" value="" class="inputs" id="description-id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_code]" value="" class="inputs" id="description-code_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_barcode]" value="" class="inputs" id="description-barcode_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][id]" value="0" class="inputs" id="id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][p_id]" value="0" class="inputs" id="p_id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][item_category]" value="" class="inputs" id="item-category_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][type_id]" value="" class="inputs" id="type_id_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas[<?php echo $extra_field_row ?>][qty]" value="" id="qty_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_qty]" value="0" class="inputs" id="hidden-qty_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" class="inputs avq form-control" name="po_datas[<?php echo $extra_field_row ?>][avq]" id="avq_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<!-- <input tabindex="<?php echo $tab_index; ?>" type="text" class="inputs form-control units" name="po_datas[<?php echo $extra_field_row ?>][unit]" value=""  id="unit_<?php echo $extra_field_row ?>" /> -->
											<!-- <input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" value="" class="inputs" id="unit-id_<?php echo $extra_field_row ?>" /> -->
											<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" id="unit-id_<?php echo $extra_field_row; ?>" class="inputs form-control units">
							                  	<option value="">Please Select</option>
						                  		<?php foreach($units as $ukey => $uvalue){ ?>
					                  				<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
						                  		<?php } ?>
							                </select>
											<input type="hidden" class="" name="po_datas[<?php echo $extra_field_row ?>][unit]" value=""  id="unit_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][rate]" value="" class="inputs rates form-control" id="rate_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_rate]" value="" class="inputs" id="hidden-rate_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][free_qty]" value="" class="inputs form-control" id="free-qty_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_free_qty]" value="" class="inputs" id="hidden-free-qty_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][discount_percent]" value="" class="inputs discounts form-control" id="discount-percent_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][discount_amount]" value="" class="inputs discount_value form-control" id="discount-amount_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" readonly="readonly" type="text" class="inputs taxes form-control" name="po_datas[<?php echo $extra_field_row ?>][tax_name]" value=""  id="tax-name_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][tax_percent]" value="" class="inputs" id="tax-percent_<?php echo $extra_field_row ?>" />
											<?php /* ?>	
											<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][tax_percent]" id="tax-percent_<?php echo $extra_field_row ?>" class="form-control">
							                  	<option value="">Please Select</option>
						                  		<?php foreach($taxes as $ukey => $uvalue){ ?>
						                  			<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
						                  		<?php } ?>
							                </select>
											<?php */ ?>
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][tax_value]" value="" class="inputs form-control" id="tax-value_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][amount]" value="" class="inputs form-control" id="amount_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][exp_date]" value="" class="inputs form-control date" id="exp-date_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">
											<a tabindex="<?php echo $tab_index; ?>" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('<?php echo $extra_field_row ?>')" class="button inputs remove " id="remove_<?php echo $extra_field_row ?>" ><i class="fa fa-trash-o"></i></a>
											<?php $tab_index ++; ?>
										</td>
								 	</tr>
								<?php } ?>
								<input type="hidden" name="ids" value="<?php echo $extra_field_row ?>" id="input-ids" />
				  			</table>
		  				</div>
		  				<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_qty"><?php echo 'Qty'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="50" readonly="readonly" type="text" name="total_qty" value="<?php echo $total_qty; ?>" placeholder="<?php echo 'Total Quantity'; ?>" id="input-total_qty" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_amt"><?php echo 'Amount'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="51" readonly="readonly" type="text" name="total_amt" value="<?php echo $total_amt; ?>" placeholder="<?php echo 'Total Amount'; ?>" id="input-total_amt" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_cash_discount"><?php echo 'Cash Discount'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="52" readonly="readonly" type="text" name="total_cash_discount" value="<?php echo $total_cash_discount; ?>" placeholder="<?php echo 'Total Cash Discount'; ?>" id="input-total_cash_discount" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_tax"><?php echo 'Tax'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="53" readonly="readonly" type="text" name="total_tax" value="<?php echo $total_tax; ?>" placeholder="<?php echo 'Total Tax'; ?>" id="input-total_tax" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_add_charges"><?php echo 'Additional Charges'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="54" type="text" name="total_add_charges" value="<?php echo $total_add_charges; ?>" placeholder="<?php echo 'Total Additional Charges'; ?>" id="input-total_add_charges" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
		  						&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_pay"><?php echo 'Total Pay'; ?></label>
							<div class="col-sm-2">
		  						<input tabindex="55" readonly="readonly" type="text" name="total_pay" value="<?php echo $total_pay; ?>" placeholder="<?php echo 'Total Pay'; ?>" id="total_pay" class="form-control inputs" />
							</div>
						</div>
		  			</form>
	  			</div>
			</div>
  		</div>
	</div>
<script type="text/javascript"><!--

$('#input-store_id').on('change', function() {
  store_name = $('#input-store_id option:selected').text();
  $('#input-store_name').val(store_name);
});

$('#input-supplier_id').on('change', function() {
  supplier_name = $('#input-supplier_id option:selected').text();
  $('#input-supplier_name').val(supplier_name);
});

$(document).on('change', '.units', function(e) {
  idss = $(this).attr('id');
  s_id = idss.split('_');
  id = s_id[1];
  unit_name = $('#unit-id_'+id+' option:selected').text();
  $('#unit_'+id).val(unit_name);
  unit_id = $('#unit-id_'+id+' option:selected').val();
  description_id = $('#description-id_'+id).val();
  description_code = $('#description-code_'+id).val();
  store_id = $('#input-store_id option:selected').val();
  $.ajax({
  	url:'index.php?route=catalog/purchaseentryfood/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code+'&store_id='+store_id,
  	type:'post',
  	dataType: 'json',
  	success:function(json){
  		$('#avq_'+id).val(json.avq);
  		return false;
  	}
  });
});

var tab_index = '<?php echo $tab_index; ?>';
function search_fun(){
	supplier_id = $('#input-supplier_id').val();
	supplier_name = $('#input-supplier_name').val();
	filter_invoice_no = $('#input-filter_invoice_no').val();
	if(supplier_id == '' || supplier_id == '0'){
		alert('Please Select Valid Supplier Name');
		return false;
	}

	if(supplier_name == ''){
		alert('Please Select Supplier Name');
		return false;
	}

	if(filter_invoice_no == ''){
		alert('Please Enter Invoice Number');
		return false;
	}
	redirect_url = '<?php echo $redirect_url ?>';
	redirect_url = redirect_url.replace('&amp;', '&');
  	redirect_url = redirect_url.replace('&amp;', '&');
	url = redirect_url+'&supplier_id='+supplier_id+'&supplier_name='+supplier_name+'&filter_invoice_no='+filter_invoice_no+'&category=Food';
	location = url;
}

$('#input-filter_invoice_no').keydown(function(e) {
	if (e.keyCode == 13) {
		search_fun();
	}
});

$('input[name=\'supplier_name\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/purchaseentry/autocomplete_supplier&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
				  	return {
						label: item.supplier,
						value: item.supplier_id
				  	}
				}));
		  	}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'supplier_name\']').val(ui.item.label);
		$('input[name=\'supplier_id\']').val(ui.item.value);
		$('#input-filter_invoice_no').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});

$('input[name=\'store_name\']').autocomplete({
  	delay: 500,
  	source: function(request, response) {
		$.ajax({
		  	url: 'index.php?route=catalog/purchaseentry/autocomplete_store&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  	dataType: 'json',
		  	success: function(json) {   
				response($.map(json, function(item) {
				  	return {
						label: item.store_name,
						value: item.id
				  	}
				}));
		  	}
		});
  	}, 
  	select: function(event, ui) {
		$('input[name=\'store_name\']').val(ui.item.label);
		$('input[name=\'store_id\']').val(ui.item.value);
		$('#input-update_master').focus();
		return false;
  	},
  	focus: function(event, ui) {
		return false;
  	}
});

$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
});

function submit_fun(){
  save = '<?php echo $action ?>';
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  $('#form-manufacturer').attr('action', save);
  $('#form-manufacturer').submit();
}

$(document).on('keydown', '.inputs', function(e) {
  	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		var na = $(this).attr('class');
		current_id = parseInt(s_id[1]);
		next_id = current_id + 1;
		if(na == 'inputs code form-control'){
			search_fun_code(s_id);
  		}
  		if(na == 'inputs qtys form-control'){
  			$('#description-code-search_'+next_id).focus();
  		}

		var index = $('.inputs').index(this);
		if(s_id[0] == 'remove') {
			ids = parseInt($('#input-ids').val());
			current_id = parseInt(s_id[1]);
			next_id = current_id + 1;
			var na = $(this).attr('class');
			flag = 1;
			val1=$('#description_'+next_id).val();
			if($(this).closest("tr").is(":last-child")){
				flag = 1;
				i = ids + 1;
			} else {
				if(val1 == undefined) {
					flag = 1;
					i = ids + 1;
				} else {
					flag = 0;
				}
			}
			if(flag == 1) {
				html = '<tr id="re_'+i+'">';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="" class="inputs code form-control" id="description-code-search_'+i+'" />';
						tab_index ++;
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 55%;display: inline;"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value=""  id="description_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="" class="inputs" id="description-id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="" class="inputs" id="description-code_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="" class="inputs" id="description-barcode_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][id]" value="0" class="inputs" id="id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="0" class="inputs" id="p-id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="" class="inputs" id="item-category_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="" class="inputs" id="type_id_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="0" id="hidden-qty_'+i+'" />';
						tab_index ++
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="" id="avq_'+i+'" />';
						tab_index ++
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="inputs units form-control">';
		                  	html += '<option value="">Please Select</option>';
	                  	html += '</select>';
						html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="" class="inputs" id="hidden-rate_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][free_qty]" value="" class="inputs form-control" id="free-qty_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_free_qty]" value="" class="inputs" id="hidden-free-qty_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_percent]" value="" class="inputs discounts form-control" id="discount-percent_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_amount]" value="" class="inputs discount_value form-control" id="discount-amount_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" readonly="readonly" type="text" class="inputs taxes form-control" name="po_datas['+i+'][tax_name]" value=""  id="tax-name_'+i+'" /><input type="hidden" name="po_datas['+i+'][tax_percent]" value="" class="inputs" id="tax-percent_'+i+'" />';	
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][tax_value]" value="" class="inputs form-control" id="tax-value_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="" class="inputs form-control" id="amount_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][exp_date]" value="" class="inputs form-control date" id="exp-date_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
						html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
						tab_index ++;
					html += '</td>';
			 	html += '</tr>';
				$('table').append(html);
				//alert('innnn');
				$('#input-ids').val(i);
				$('#description-code-search_'+i).focus();
				i++;
				return false;
			} else {
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		  		if (!$next.length) {
		  			$next = $('[tabIndex=1]');
		  		}
		  		//alert('innn1');
		  		$next.focus();
		  		$next.select();
			}
		} else {
			if(s_id[0] == 'input-category'){
				category = $('#input-category').val();
				if(category == 'Food'){
					$('#input-batch_no').focus();
					return false;
				} else if(category == 'Food'){
					$('#input-tp_no').focus();
					return false;
				} else {
					var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
			  		if (!$next.length) {
			  			$next = $('[tabIndex=1]');
			  		}
			  		//alert('innn2');
			  		$next.focus();
			  		$next.select();
			  		return false;	
				}
			} else {
				ids = parseInt($('#input-ids').val());
				current_id = parseInt(s_id[1]);
				next_id = current_id + 1;
				var na = $(this).attr('class');
				flag = 1;
				val2=$('#description-code-search_'+current_id).val();
				//console.log(na);
				if(na == 'inputs qtys form-control'){
					if(val2 != ''){
						val1=$('#description_'+next_id).val();
							if(val1 == undefined) {
								flag = 1;
								i = ids + 1;
							} else {
								flag = 0;
							}
						//console.log(val1);
						if(flag == 1) {
							html = '<tr id="re_'+i+'">';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="" class="inputs code form-control" id="description-code-search_'+i+'" />';
									tab_index ++;
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 55%;display: inline;"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value=""  id="description_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="" class="inputs" id="description-id_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="" class="inputs" id="description-code_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="" class="inputs" id="description-barcode_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][id]" value="0" class="inputs" id="id_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="0" class="inputs" id="p-id_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="" class="inputs" id="item-category_'+i+'" />';
									html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="" class="inputs" id="type_id_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="0" id="hidden-qty_'+i+'" />';
									tab_index ++
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="" id="avq_'+i+'" />';
									tab_index ++
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									//html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;"  type="text" class="inputs units form-control" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
									//html += '<input type="hidden" name="po_datas['+i+'][unit_id]" value="" class="inputs" id="unit-id_'+i+'" />';
									html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="inputs units form-control">';
					                  	html += '<option value="">Please Select</option>';
				                  	html += '</select>';
									html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="" class="inputs" id="hidden-rate_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][free_qty]" value="" class="inputs form-control" id="free-qty_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_free_qty]" value="" class="inputs" id="hidden-free-qty_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_percent]" value="" class="inputs discounts form-control" id="discount-percent_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_amount]" value="" class="inputs discount_value form-control" id="discount-amount_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" readonly="readonly" type="text" class="inputs taxes form-control" name="po_datas['+i+'][tax_name]" value=""  id="tax-name_'+i+'" /><input type="hidden" name="po_datas['+i+'][tax_percent]" value="" class="inputs" id="tax-percent_'+i+'" />';	
									<?php /* ?>
									html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][tax_percent]" id="tax-percent_'+i+'" class="form-control">';
					                  	html += '<option value="">Please Select</option>';
				                  		<?php foreach($taxes as $ukey => $uvalue){ ?>
				                  			html += '<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>';
				                  		<?php } ?>
					                html += '</select>';
					                <?php */ ?>
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][tax_value]" value="" class="inputs form-control" id="tax-value_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="" class="inputs form-control" id="amount_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
									html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][exp_date]" value="" class="inputs form-control date" id="exp-date_'+i+'" />';
									tab_index ++;
								html += '</td>';
								html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
									html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
									tab_index ++;
								html += '</td>';
						 	html += '</tr>';
							$('table').append(html);
							//alert('innnn');

							$('#input-ids').val(i);
							$('#description-code-search_'+i).focus();
							
							i++;
						}

						// if($('#qty_'+i ).val() < 0 ){
					 //  		alert("Qty Should not be Zero/Negative ");
					 //  		$('#qty_'+i ).val(1);
					 //  		return false;
					 //  	}
					}
				}
					
				/*var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
		  		if (!$next.length) {
		  			$next = $('[tabIndex=1]');
		  		}
		  		//alert(code);
		  		current_id = parseInt(s_id[1]);
				next_id = current_id + 1;

				$('#input-ids').val(next_id);
				$('#description-code-search_'+next_id).focus();

		  		$next.focus();
		  		$next.select();*/
			}
			return false;
		}			
	} else if (code == 37) {
		var $previous = $('[tabIndex=' + (+this.tabIndex - 1) + ']');
        if (!$previous.length) {
           $previous = $('[tabIndex=1]');
        }
        previous_index = this.tabIndex - 1;
        $previous.focus();
       	$previous.select();

	} else if (code == 39) {
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
  		if (!$next.length) {
  			$next = $('[tabIndex=1]');
  		}
  		$next.focus();
		$next.select();

        // previous_index = this.tabIndex - 1;
        // $previous.focus();
       	// $previous.select(); 
	}

  	if(code == 27){
  		window.location.reload();
  	}
  	/*idss = $(this).attr('id');
	s_id = idss.split('_');
  	des_code =  $('#description-code-search_'+s_id[1]).val();
  
  	if(des_code == " " || ){
  		$('#rate_'+s_id[1]).focus();
  	} else {*/
	  	
	  	
	
	//}
	$('.description').autocomplete({
			delay: 500,
			source: function(request, response) {
				filter_category = $('#input-category').val();
				//console.log("innn2222");
				$.ajax({
			  		url: 'index.php?route=catalog/purchaseentryfood/autocomplete_description&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_category='+filter_category,
			  		dataType: 'json',
			  		success: function(json) {   
						response($.map(json, function(item) {
				  			return {
								label: item.item_name,
								value: item.id,
								rate: item.rate,
								item_code: item.item_code,
								bar_code: item.bar_code,
								tax: item.tax,
								tax_value: item.tax_value,
								uom: item.uom,
								units: item.units,
								rate: item.rate,
								item_category: item.item_category,
								unit_name: item.unit_name,
								stock_type: item.stock_type
				  			}
				  			
						}));
			  		}
				});
				return false;
			}, 
			select: function(event, ui) {
				idss = $(this).attr('id');
				s_id = idss.split('_');
				$('#description_'+s_id[1]).val(ui.item.label);
				$('#description-code-search_'+s_id[1]).val(ui.item.item_code);
				$('#description-id_'+s_id[1]).val(ui.item.value);
				$('#description-code_'+s_id[1]).val(ui.item.item_code);
				$('#description-barcode_'+s_id[1]).val(ui.item.bar_code);
				$('#tax-name_'+s_id[1]).val(ui.item.tax);
				$('#tax-percent_'+s_id[1]).val(ui.item.tax_value);
				$('#item-category_'+s_id[1]).val(ui.item.item_category);
				
				$('#unit-id_'+s_id[1]).find('option').remove();
			    if(ui.item.units){
			        $.each(ui.item.units, function (i, item) {
			          $('#unit-id_'+s_id[1]).append($('<option>', { 
			              value: item.unit_id,
			              text : item.unit_name 
			          }));
			          return false;
			        });
			    }
				if(ui.item.item_category == 'Food'){
					$('#unit_'+s_id[1]).val(ui.item.unit_name);
					$('#unit-id_'+s_id[1]).val(ui.item.uom);
					$('#unit_'+s_id[1]).attr("readonly", "readonly");
				} else {
					$('#unit_'+s_id[1]).val('');
					$('#unit-id_'+s_id[1]).val('');
					$('#unit_'+s_id[1]).removeAttr("readonly");
				}
				if($('#unit-id_'+s_id[1]).val() != ''){
					unit_id = $('#unit-id_'+s_id[1]).val();
					description_id = $('#description-id_'+s_id[1]).val();
					description_code = $('#description-code_'+s_id[1]).val();
					store_id = $('#input-store_id option:selected').val();
					var s_id = s_id[1];
					$.ajax({
					  	url:'index.php?route=catalog/purchaseentryfood/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code+'&store_id='+store_id+'&stock_type='+ui.item.stock_type,
					  	type:'post',
					  	dataType: 'json',
					  	success:function(json){
					  		$('#avq_'+s_id).val(json.avq);
					  	}
				  	});
				}

				
				$('#rate_'+s_id).val(ui.item.rate);
				$('#hidden-rate_'+s_id).val(ui.item.rate);
				$('#qty_'+s_id).focus();
				gettotal(s_id);
				return false;
			},
			focus: function(event, ui) {
				return false;
			}

		});

	
	//return false;
});

$('.unitsss').autocomplete({
		delay: 500,
		source: function(request, response) {
			filter_item_type = $('#input-item_type').val();
			$.ajax({
		  		url: 'index.php?route=catalog/purchaseentry/autocomplete_units&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_item_type='+filter_item_type,
		  		dataType: 'json',
		  		success: function(json) {   
					response($.map(json, function(item) {
			  			return {
							label: item.unit,
							value: item.unit_id,
			  			}
			  			return false;
					}));
		  		}
			});
		}, 
		select: function(event, ui) {
			idss = $(this).attr('id');
			s_id = idss.split('_');
			$('#unit_'+s_id[1]).val(ui.item.label);
			$('#unit-id_'+s_id[1]).val(ui.item.value);
			$('#rate_'+s_id[1]).focus();
			gettotal(s_id[1]);
			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});

	$('.taxes').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
		  		url: 'index.php?route=catalog/purchaseentry/autocomplete_taxes&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
		  		dataType: 'json',
		  		success: function(json) {   
					response($.map(json, function(item) {
			  			return {
							label: item.tax_name,
							value: item.tax_value,
							id: item.id,
			  			}
			  			return false;
					}));
		  		}
			});
		}, 
		select: function(event, ui) {
			idss = $(this).attr('id');
			s_id = idss.split('_');
			$('#tax-name_'+s_id[1]).val(ui.item.label);
			$('#tax-percent_'+s_id[1]).val(ui.item.value);
			$('#exp-date_'+s_id[1]).focus();
			gettotal(s_id[1]);
			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});

	

	
	$('.date').datetimepicker({
	  pickTime: false,
	  format: 'DD-MM-YYYY'
	});
// $('.code').keydown(function(e) {
// 		if (e.keyCode == 13) {
// 			idss = $(this).attr('id');
// 			console.log(idss);
// 			s_id = idss.split('_');
// 			search_fun_code(s_id);
// 			return false;
// 		}
// 		//return false;
// 	});

$(document).on('keyup', '.qtys, .rates, .discounts, .taxes', function(e) {
	idss = $(this).attr('id');
  	s_id = idss.split('_');
  	if($('#rate_'+s_id[1] ).val() < 0  ){
  		alert("Rate Should not be Zero/Negative ");
  		$('#rate_'+s_id[1] ).val(1);
  	}
  	if($('#qty_'+s_id[1] ).val() < 0 ){
  		alert("Qty Should not be Zero/Negative ");
  		$('#qty_'+s_id[1] ).val(1);
  	}
	gettotal(s_id[1]); 
	return false;	
});

$(document).on('keyup', '.discount_value', function(e) {
	idss = $(this).attr('id');
  	s_id = idss.split('_');

  	/*
  	discount_value = $('#discount-value_'+s_id[1]).val();
  	if(discount_value == '' || discount_value == '0' || isNaN(discount_value)){
		discount_value = 0;
	}
  	if(discount_value > 0){
  		$('#discount-percent_'+s_id[1]).val('');
  	}
  	*/
	gettotal(s_id[1]); 
	return false;	
});

$( document ).ready(function() {
	$('#remove_1').attr('onclick','');
	$('#remove_1').text('-');
});

$(document).on('keyup', '#input-total_add_charges', function(e) {
	total_amt = parseFloat($('#input-total_amt').val());
	if(total_amt == '' || total_amt == '0' || isNaN(total_amt)){
		total_amt = 0;
	}
	total_cash_discount = parseFloat($('#input-total_cash_discount').val());
	if(total_cash_discount == '' || total_cash_discount == '0' || isNaN(total_cash_discount)){
		total_cash_discount = 0;
	}
	total_tax = parseFloat($('#input-total_tax').val());
	if(total_tax == '' || total_tax == '0' || isNaN(total_tax)){
		total_tax = 0;
	}
	total_add_charges = parseFloat($('#input-total_add_charges').val());
	if(total_add_charges == '' || total_add_charges == '0' || isNaN(total_add_charges)){
		total_add_charges = 0;
	}
	total_pay = (total_amt - total_cash_discount) + (total_tax + total_add_charges);
	$('#total_pay').val(total_pay);
	return false;
});

function gettotal(id) {
	//console.log('get total 33333333');
	/*
	total_quantity = parseFloat($('#input-qty').val());
	if(total_quantity == '' || total_quantity == '0' || isNaN(total_quantity)){
		total_quantity = 0;
	}
	total_amt = parseFloat($('#input-total_amt').val());
	if(total_amt == '' || total_amt == '0' || isNaN(total_amt)){
		total_amt = 0;
	}
	total_cash_discount = parseFloat($('#input-total_cash_discount').val());
	if(total_cash_discount == '' || total_cash_discount == '0' || isNaN(total_cash_discount)){
		total_cash_discount = 0;
	}
	total_tax = parseFloat($('#input-total_tax').val());
	if(total_tax == '' || total_tax == '0' || isNaN(total_tax)){
		total_tax = 0;
	}
	total_add_charges = parseFloat($('#input-total_add_charges').val());
	if(total_add_charges == '' || total_add_charges == '0' || isNaN(total_add_charges)){
		total_add_charges = 0;
	}
	total_pay = parseFloat($('#input-total_pay').val());
	if(total_pay == '' || total_pay == '0' || isNaN(total_pay)){
		total_pay = 0;
	}
	*/
	
	qty = parseFloat($('#qty_'+id).val());
	if(qty == '' || qty == '0' || isNaN(qty)){
		qty = 0;
	}
	rate = parseFloat($('#rate_'+id).val());
	if(rate == '' || rate == '0' || isNaN(rate)){
		rate = 0;
	}
	discount_percent = parseFloat($('#discount-percent_'+id).val());
	discount_value = 0;
	if(discount_percent == '' || discount_percent == '0' || isNaN(discount_percent)){
		discount_percent = 0;
	} else {
		if(discount_percent > 100){
			alert('Discount Percent Cannot be greater than 100');
			return false;
		}
	}
	tax_percent = parseFloat($('#tax-percent_'+id).val());
	tax_value = 0;
	if(tax_percent == '' || tax_percent == '0' || isNaN(tax_percent)){
		tax_percent = 0;
	}
	single_amt = qty * rate;
	if(single_amt != 0){
		if(discount_percent != 0 && discount_percent != ''){
			discount_value = ((discount_percent/100)*single_amt);
			$('#discount-amount_'+id).val(discount_value);
		}
		if(tax_percent != 0 && tax_percent != ''){
			tax_value = ((tax_percent/100)*(single_amt - discount_value));
		}
	}
	amount = (single_amt - discount_value) + tax_value;
	$('#tax-value_'+id).val(tax_value);
	$('#amount_'+id).val(amount);
	
 	total_qty = 0;
 	total_amt = 0;
 	total_cash_discount = 0;
 	total_tax = 0;
 	total_add_charges = parseFloat($('#input-total_add_charges').val());
	if(total_add_charges == '' || total_add_charges == '0' || isNaN(total_add_charges)){
		total_add_charges = 0;
	}
 	total_pay = 0;
 	$('.ordertab').find('tr').each (function() {
 	//console.log('innn');
		idss = $(this).attr('id');
		if(idss != undefined){
			idss = $(this).attr('id');
  			s_ids = idss.split('_');
			s_id = s_ids[1];
			qty = parseFloat($('#qty_'+s_id).val());
			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}
			total_qty = total_qty + qty;
			rate = parseFloat($('#rate_'+s_id).val());
			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}
			single_amt = qty * rate;
			total_amt = total_amt + single_amt;
			discount_amount = parseFloat($('#discount-amount_'+s_id).val());
			if(discount_amount == '' || discount_amount == '0' || isNaN(discount_amount)){
				discount_amount = 0;
			}
			total_cash_discount = total_cash_discount + discount_amount;
			tax_value = parseFloat($('#tax-value_'+s_id).val());
			if(tax_value == '' || tax_value == '0' || isNaN(tax_value)){
				tax_value = 0;
			}
			total_tax = total_tax + tax_value;

		}
		 //return false; 
	});
	total_pay = (total_amt - total_cash_discount) + (total_tax + total_add_charges);
	$('#input-total_qty').val(total_qty);	
	$('#input-total_amt').val(total_amt);	
	$('#input-total_cash_discount').val(total_cash_discount);	
	$('#input-total_tax').val(total_tax);	
	$('#input-total_add_charges').val(total_add_charges);	
	$('#total_pay').val(total_pay);
	return false;	
}

function remove_folder(c){
	$('#re_'+c).remove();
	var units = '<?php echo $units1 ?>';
	units = JSON.parse(units);
 	var final_datas = [];
 	var j = 1;
 	$('.ordertab').find('tr').each (function() {
		idss = $(this).attr('id');
		if(idss != undefined){
			final_datas[j] = [];
			s_id = idss.split('_');
			id = s_id[1];
			final_datas[j]['description'] = $('#description_'+id).val();	
			final_datas[j]['description_id'] = $('#description-id_'+id).val();	
			final_datas[j]['description_code'] = $('#description-code_'+id).val();	
			final_datas[j]['description_code_search'] = $('#description-code-search_'+id).val();	
			final_datas[j]['description_barcode'] = $('#description-barcode_'+id).val();	
			final_datas[j]['item_category'] = $('#item-category_'+id).val();	
			final_datas[j]['qty'] = $('#qty_'+id).val();	
			final_datas[j]['hidden_qty'] = $('#hidden-qty_'+id).val();	
			final_datas[j]['units'] = units;	
			final_datas[j]['unit'] = $('#unit_'+id).val();	
			final_datas[j]['unit_id'] = $('#unit-id_'+id).val();	
			final_datas[j]['rate'] = $('#rate_'+id).val();	
			final_datas[j]['hidden_rate'] = $('#hidden-rate_'+id).val();	
			final_datas[j]['free_qty'] = $('#free-qty_'+id).val();	
			final_datas[j]['hidden_free_qty'] = $('#hidden-free-qty_'+id).val();	
			final_datas[j]['discount_percent'] = $('#discount-percent_'+id).val();	
			final_datas[j]['discount_amount'] = $('#discount-amount_'+id).val();	
			final_datas[j]['tax_name'] = $('#tax-name_'+id).val();	
			final_datas[j]['tax_percent'] = $('#tax-percent_'+id).val();
			final_datas[j]['tax_value'] = $('#tax-value_'+id).val();	
			final_datas[j]['amount'] = $('#amount_'+id).val();	
			final_datas[j]['type_id'] = $('#type_id_'+id).val();
			final_datas[j]['exp_date'] = $('#exp-date_'+id).val();	
			final_datas[j]['avq'] = $('#avq_'+id).val();
			$('#re_'+id).remove();
			j ++;
		}
		 return false; 
	});
  	tab_index = 9;
  	i = 1;
  	last_key = j - 1;
  	for (var arrayindex in final_datas) {
		html = '<tr id="re_'+i+'">';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="'+final_datas[arrayindex]['description_code_search']+'" class="inputs code form-control" id="description-code-search_'+i+'" />';
				tab_index ++;
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width:55%;display:inline"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value="'+final_datas[arrayindex]['description']+'" id="description_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="'+final_datas[arrayindex]['description_id']+'" class="inputs" id="description-id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="'+final_datas[arrayindex]['description_code']+'" class="inputs" id="description-code_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="'+final_datas[arrayindex]['description_barcode']+'" class="inputs" id="description-barcode_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][id]" value="'+final_datas[arrayindex]['id']+'" class="inputs" id="id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="'+final_datas[arrayindex]['p_id']+'" class="inputs" id="p-id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="'+final_datas[arrayindex]['item_category']+'" class="inputs" id="item-category_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="'+final_datas[arrayindex]['type_id']+'" class="inputs" id="type_id_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="'+final_datas[arrayindex]['qty']+'" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="'+final_datas[arrayindex]['hidden_qty']+'" id="hidden-qty_'+i+'" />';
				tab_index ++
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="'+final_datas[arrayindex]['avq']+'" id="avq_'+i+'" />';
				tab_index ++
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				//html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;"  type="text" class="inputs units form-control" name="po_datas['+i+'][unit]" value="'+final_datas[arrayindex]['unit']+'"  id="unit_'+i+'" /><input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				//html += '<input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="form-control">';
                  	html += '<option value="">Please Select</option>';
              		for (var food in units) {
                        var unit_id = units[food].unit_id;
                        var unit_name = units[food].unit;
                        if(unit_id == final_datas[arrayindex]['unit_id']){
                       		html += '<option selected="selected" value="'+unit_id+'">'+unit_name+'</option>';
                       	} else{
                       		html += '<option value="'+unit_id+'">'+unit_name+'</option>';
                       	}
                    }
                html += '</select>';
				html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value="'+final_datas[arrayindex]['unit']+'"  id="unit_'+i+'" /><input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="'+final_datas[arrayindex]['rate']+'" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="'+final_datas[arrayindex]['hidden_rate']+'" class="inputs" id="hidden-rate_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][free_qty]" value="'+final_datas[arrayindex]['free_qty']+'" class="inputs form-control" id="free-qty_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_free_qty]" value="'+final_datas[arrayindex]['hidden_free_qty']+'" class="inputs" id="hidden-free-qty_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_percent]" value="'+final_datas[arrayindex]['discount_percent']+'" class="inputs discounts form-control" id="discount-percent_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][discount_amount]" value="'+final_datas[arrayindex]['discount_amount']+'" class="inputs discount_value form-control" id="discount-amount_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" readonly="readonly" type="text" class="inputs taxes form-control" name="po_datas['+i+'][tax_name]" value="'+final_datas[arrayindex]['tax_name']+'"  id="tax-name_'+i+'" /><input type="hidden" name="po_datas['+i+'][tax_percent]" value="'+final_datas[arrayindex]['tax_percent']+'" class="inputs" id="tax-percent_'+i+'" />';	
				<?php /* ?>
				html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][tax_percent]" id="tax-percent_'+i+'" class="form-control">';
                  	html += '<option value="">Please Select</option>';
              		<?php foreach($taxes as $ukey => $uvalue){ ?>
              			html += '<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>';
              		<?php } ?>
                html += '</select>';
                <?php */ ?>
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][tax_value]" value="'+final_datas[arrayindex]['tax_value']+'" class="inputs form-control" id="tax-value_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="'+final_datas[arrayindex]['amount']+'" class="inputs form-control" id="amount_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][exp_date]" value="'+final_datas[arrayindex]['exp_date']+'" class="inputs form-control date" id="exp-date_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
				html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
				tab_index ++;
			html += '</td>';
	 	html += '</tr>';
	 	$('table').append(html);
  		i ++;	
	}
	prev_i = i - 1;
	$('#input-ids').val(prev_i);
	$('#remove_1').attr('onclick','');
	$('#remove_1').text('-');
	gettotal(prev_i);
	$('#description-code-search_'+prev_i).focus();
}

function print(){
	firstinvoicenumber = $('#input-invoice_no').val();
	searchinvoicenumber = $('#input-filter_invoice_no').val();
	purchase_id = $('#purchase_id').val();

	if(firstinvoicenumber != '' || searchinvoicenumber != ''){
		location = 'index.php?route=catalog/purchaseentryfood/prints&token=<?php echo $token; ?>&purchase_id='+purchase_id;
	}
}

function search_fun_code(s_id){
		filter_name_1 = $('#description-code-search_'+s_id[1]).val();
		filter_category = $('#input-category').val();
		s_ids = s_id[1];
		var me = $(this);
		if ( me.data('requestRunning') ) {
	        return;
	    }

	    me.data('requestRunning', true);
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/purchaseentryfood/autocomplete_description&token=<?php echo $token; ?>&filter_name_1=' +filter_name_1+'&filter_category='+filter_category,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					$('#description_'+s_ids).val(data.item_name);
					$('#description-id_'+s_ids).val(data.id);
					$('#description-code_'+s_ids).val(data.item_code);
					$('#description-barcode_'+s_ids).val(data.bar_code);
					$('#description-code-search_'+s_ids).val(data.item_code);
					$('#tax-name_'+s_ids).val(data.tax);
					$('#tax-percent_'+s_ids).val(data.tax_value);
					$('#item-category_'+s_ids).val(data.item_category);
					$('#qty_'+s_ids ).val();
					
					$('#unit-id_'+s_ids).find('option').remove();
				    if(data.units){
				        $.each(data.units, function (i, item) {
				          $('#unit-id_'+s_ids).append($('<option>', { 
				              value: item.unit_id,
				              text : item.unit_name 
				          }));
				          return false;
				        });
				    }
					if(data.item_category == 'Food'){
						$('#unit_'+s_ids).val(data.uom);
						$('#unit-id_'+s_ids).val(data.uom);
						$('#unit_'+s_ids).attr("readonly", "readonly");
					} else {
						$('#unit_'+s_ids).val('');
						$('#unit-id_'+s_ids).val('');
						$('#unit_'+s_ids).removeAttr("readonly");
					}

					if($('#unit-id_'+s_ids).val() != '' && $('#description-id_'+s_ids).val() != '' && $('#description-code_'+s_ids).val() != ''){
						unit_id = $('#unit-id_'+s_ids).val();
						description_id = $('#description-id_'+s_ids).val();
						description_code = $('#description-code_'+s_ids).val();
						store_id = $('#input-store_id option:selected').val();
						var s_id = s_ids;
						$.ajax({
						  	url:'index.php?route=catalog/purchaseentryfood/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code+'&store_id='+store_id+'&stock_type='+data.stock_type,
						  	type:'post',
						  	dataType: 'json',
						  	success:function(json){
						  		$('#avq_'+s_id).val(json.avq);
						  	}
					  });
					}

					$('#rate_'+s_ids).val(data.rate);
					$('#hidden-rate_'+s_ids).val(data.rate);
					//console.log(data.rate);
					$('#qty_'+s_ids).focus();
					gettotal(s_ids);
					return false;					
				} else {
					//alert('Item Code/Barcode Does Not Exist');
					$('#description_'+s_ids).val('');
					$('#description-id_'+s_ids).val('');
					$('#description-code_'+s_ids).val('');
					$('#description-barcode_'+s_ids).val('');
					$('#description-code-search_'+s_ids).val('');
					$('#tax-name_'+s_ids).val('');
					$('#tax-percent_'+s_ids).val('');
					$('#item-category_'+s_ids).val('');
					$('#unit_'+s_ids).val('');
					$('#unit-id_'+s_ids).val('');
					$('#unit_'+s_ids).removeAttr("readonly");
					$('#rate_'+s_ids).val('');
					$('#hidden-rate_'+s_ids).val('');
					$('#description-code-search_'+s_ids).focus();
					gettotal(s_ids);
					return false;
				}
			},
			complete: function() {
            	me.data('requestRunning', false);
        	}
		});		
	}

//--></script>
<?php echo $footer; ?>