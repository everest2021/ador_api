<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		     </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
					<div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<div class="col-sm-offset-2">
						       		<div class="form-row">
						       			<div class="col-sm-2">
									    	<label> Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder=" Date">
									    </div>

									    <div class="col-sm-3">
									    	<center><label>Item Name</label></center>
									     	<input type="text" name='item_name' value="<?php echo $item_name?>" id="item_name" class="form-control">
									     	<input type="hidden" name='item_id' value="<?php echo $item_id?>" id="item_id" class="form-control">
									    </div>
									    
									    <div class="col-sm-2">
									    	<center><label>Unit</label></center>
									     	<select name="unit_id" id="unit_id" class="form-control">
									     		<option value="">Please Select</option>
									     	</select>
									     	<br>
									     	<input type="hidden" name="unit_val" id="unit_val" value="<?php echo $unit_id ?>">
									    </div>
									    <div class="col-sm-2">
									    	<center><label>Select Store</label></center>
									     	<select name="store" id="store" class="form-control">
									     		<option value="">All</option>
									     		<?php foreach($stores as $store) { ?>
									     			<?php if($store['id'] == $store_id) { ?>
									     				<option value = "<?php echo $store['id'] ?>" selected="selected"><?php echo $store['store_name'] ?></option>
									     			<?php } else { ?>
									     				<option value = "<?php echo $store['id'] ?>" ><?php echo $store['store_name'] ?></option>
									     			<?php } ?>
									     		<?php } ?>
									     	</select>
									    </div>
									</div>
								</div>
							</div>
							<br>
							<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
					    </div>
					</div>
					<div class="col-sm-6 col-sm-offset-3">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Stock Report</b></h3></center>
						<?php if($final_data) { ?>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Item Name</th>
							<th style="text-align: center;">Unit</th>
							<th style="text-align: center;">Opening Balance</th>
							<th style="text-align: center;">Purchase</th>
							<th style="text-align: center;">Store Trans. From</th>
							<th style="text-align: center;">Store Trans. To</th>
							<th style="text-align: center;">Loose Stock</th>
							<th style="text-align: center;">Sales</th>
							<th style="text-align: center;">Quantity</th>
						</tr>
						<?php foreach($final_data as $key =>$value) { /*echo "<pre>"; print_r($key)*/?>
						<tr>
							<td><?php echo $value['item_name'] ?></td>
							<td><?php echo $value['unit'] ?></td>
							<td><?php echo $value['opening_bal'] ?></td>
							<td><?php echo $value['total_purchase'] ?></td>
							<td><?php echo $value['s_t_f'] ?></td>
							<td><?php echo $value['s_t_t'] ?></td>
							<td><?php echo $value['loose'] ?></td>
							<td><?php echo $value['sale'] ?></td>
							<td><?php echo $value['avail_quantity'] ?></td>

						</tr>
						<?php } ?>	  	
					  	</table>
				 	<?php } ?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_status = $('select[name=\'filter_status\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_status) {
			  url += '&filter_status=' + encodeURIComponent(filter_status);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}

	$('input[name=\'item_name\']').autocomplete({
	  	delay: 500,
	  	source: function(request, response) {
			$.ajax({
			  	url: 'index.php?route=catalog/stockreportliq/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			  	dataType: 'json',
			  	success: function(json) {   
					response($.map(json, function(item) {
					  	return {
							label: item.name,
							value: item.id
					  	}
					}));
			  	}
			});
	  	}, 
	  	select: function(event, ui) {
			$('input[name=\'item_name\']').val(ui.item.label);
			$('input[name=\'item_id\']').val(ui.item.value);
			type_id = $('#item_id').val();
			$.ajax({
			  	url: 'index.php?route=catalog/stockreportliq/getUnit&token=<?php echo $token; ?>&filter_id='+type_id,
			  	type:"POST",
			  	dataType: 'json',
			  	success: function(json) {   
					$('#unit_id').find('option').remove();
					if(json['units']){
				        $.each(json['units'], function (i, item) {
				          $('#unit_id').append($('<option>', { 
				              value: item.id,
				              text : item.name
				          }));
				        });
				    }
			  	}
			});
			return false;
	  	},
	  	focus: function(event, ui) {
			return false;
	  	}
	});

	$(document).ready(function(){
		type_id = $('#item_id').val();
		unit_val = $('#unit_val').val();
		if(type_id != ''){
			$.ajax({
			  	url: 'index.php?route=catalog/stockreportliq/getUnit&token=<?php echo $token; ?>&filter_id='+type_id,
			  	type:"POST",
			  	dataType: 'json',
			  	success: function(json) {   
					$('#unit_id').find('option').remove();
					if(json['units']){
				        $.each(json['units'], function (i, item) {
				          if(item.id == unit_val){
				          	$("#unit_id").append("<option selected='selected' value="+item.id+">"+item.name+"</option>")
				          } else{
				          	$("#unit_id").append("<option value="+item.id+">"+item.name+"</option>")
				          }
				        });
				    }
			  	}
			});
		}
	});

	// $(document).on('change','#unit_id',function(){
	// 	unit_id = 
	// });

	$('input[name=\'item_name\']').keyup(function(){
		item_name = $('#item_name').val();
		if(item_name == ''){
			$('#item_id').val('');
		}
	});
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>