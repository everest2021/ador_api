<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a style="display:none;" href="<?php echo $action; ?>" data-toggle="tooltip" title="<?php echo "Save"; ?>" class="btn btn-primary"><i class="fa fa-save"></i></a>
				<button  type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo "Save"; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo "Cancel"; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
				<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="container-fluid">
			<div class="panel panel-default">
				<div class="panel-heading">
				</div>
				<div class="panel-body">
					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'WAITER'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="WAITER" value="<?php echo $WAITER; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'CAPTAIN'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="CAPTAIN" value="<?php echo $CAPTAIN; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'PERSONS'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="PERSONS" value="<?php echo $PERSONS; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'PERSONS COMPULSARY'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="PERSONS_COMPULSARY" value="<?php echo $PERSONS_COMPULSARY; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'INCLUSIVE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="INCLUSIVE" value="<?php echo $INCLUSIVE; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SKIP TABLE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SKIPTABLE" value="<?php echo $SKIPTABLE; ?>"  class="form-control" />
							</div>
						</div>
						
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SKIP SUB-CATEGORY'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SKIPSUBCATEGORY" value="<?php echo $SKIPSUBCATEGORY; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'NOCATEGORY'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="NOCATEGORY" value="<?php echo $NOCATEGORY; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'CATEGORY'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="CATEGORY" value="<?php echo $CATEGORY; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'KOT FORMAT'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="KOT_RATE_AMT" value="<?php echo $KOT_RATE_AMT; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'PRINTER NAME'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="PRINTER_NAME" value="<?php echo $PRINTER_NAME; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'TENDERING ENABLE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="TENDERING_ENABLE" value="<?php echo $TENDERING_ENABLE; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'PRINTER TYPE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="PRINTER_TYPE" value="<?php echo $PRINTER_TYPE; ?>"  class="form-control" />
							</div>
						</div>
						   <div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'RATE CHANGE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="RATE_CHANGE" value="<?php echo $RATE_CHANGE; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SETTLEMENT'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SETTLEMENT" value="<?php echo $SETTLEMENT; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'SETTLEMENT OFF'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SETTLEMENT_ON" value="<?php echo $SETTLEMENT_ON; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'CHECK KOT GRAND TOTAL'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="CHECK_KOT_GRAND_TOTAL" value="<?php echo $CHECK_KOT_GRAND_TOTAL; ?>"  class="form-control" />
							</div>
						</div>

						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'KOT/BILL CANCEL REASON'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="KOT_BILL_CANCEL_REASON" value="<?php echo $KOT_BILL_CANCEL_REASON; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SERVICE CHARGE FOOD'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SERVICE_CHARGE_FOOD" value="<?php echo $SERVICE_CHARGE_FOOD; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SERVICE CHARGE LIQ'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SERVICE_CHARGE_LIQ" value="<?php echo $SERVICE_CHARGE_LIQ; ?>"  class="form-control" />
							</div>
						</div>
						
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'BARCODE PRINTER'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="BARCODE_PRINTER" value="<?php echo $BARCODE_PRINTER; ?>"  class="form-control" />
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'LOCAL PRINT'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="LOCAL_PRINT" value="<?php echo $LOCAL_PRINT; ?>"  class="form-control" />
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'COMPLIMENTARY STATUS'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="COMPLIMENTARY_STATUS" value="<?php echo $COMPLIMENTARY_STATUS; ?>"  class="form-control" />
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'HOTEL NAME'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="HOTEL_NAME" value="<?php echo $HOTEL_NAME; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'HOTEL ADD'; ?></label>
							<div class="col-sm-3">
								<textarea name="HOTEL_ADD" class="form-control" rows="5"><?php echo $HOTEL_ADD; ?></textarea>
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'GST NO'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="GST_NO" value="<?php echo $GST_NO; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'TEXT1'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="TEXT1" value="<?php echo $TEXT1; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'TEXT2'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="TEXT2" value="<?php echo $TEXT2; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'TEXT3'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="TEXT3" value="<?php echo $TEXT3; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'BAR NAME'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="BAR_NAME" value="<?php echo $BAR_NAME; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'BAR ADD'; ?></label>
							<div class="col-sm-3">
								<textarea name="BAR_ADD" class="form-control" rows="5"><?php echo $BAR_ADD; ?></textarea>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'ADVANCE NOTE'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="ADVANCE_NOTE" value="<?php echo $ADVANCE_NOTE; ?>"  class="form-control" />
							</div>
						</div>
						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SMS LINK 1'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SMS_LINK_1" value="<?php echo $SMS_LINK_1; ?>"  class="form-control" />
							</div>
						</div>
						<div style = "display:none" class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SMS LINK 2'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SMS_LINK_2" value="<?php echo $SMS_LINK_2; ?>"  class="form-control" />
							</div>
						</div>
						<div style = "display:none" class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SMS LINK 3'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SMS_LINK_3" value="<?php echo $SMS_LINK_3; ?>"  class="form-control" />
							</div>
						</div>
						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'OWNER CONTACT NUMBER FOR SMS'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="CONTACT_NUMBER" value="<?php echo $CONTACT_NUMBER; ?>"  class="form-control" />
							</div>
						</div>

						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'TO EMAIL FOR REPORTS'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="EMAIL_TO_REPORT" value="<?php echo $EMAIL_TO_REPORT; ?>"  class="form-control" />
							</div>
						</div>

						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'EMAIL USERNAME'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="EMAIL_USERNAME" value="<?php echo $EMAIL_USERNAME; ?>"  class="form-control" />
							</div>
						</div>

						<div  class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'PASSWORD FOR EMAIL'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="EMAIL_TO_REPORT_PASSWORD" value="<?php echo $EMAIL_TO_REPORT_PASSWORD; ?>"  class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'STARTUP PAGE'; ?></label>
							<div class="col-sm-3">
								<select name="STARTUP_PAGE" class="form-control">
									<?php foreach($startup_data as $skey => $svalue) { ?>
										<?php if($skey == $STARTUP_PAGE) { ?>
											<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue?></option>
										<?php } else { ?>
											<option value="<?php echo $skey ?>"><?php echo $svalue?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<!-- <input type="text" name="STARTUP_PAGE" value="<?php echo $STARTUP_PAGE; ?>"  class="form-control" /> -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'DAYCLOSE POPUP'; ?></label>
							<div class="col-sm-3">
								<select name="DAYCLOSE_POPUP" class="form-control">
									<?php foreach ($popup as $key => $value) { ?>
										<?php if($key==$DAYCLOSE_POPUP){ ?>
											<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
										<?php } else{ ?>
											<option value="<?php echo $key ?>"><?php echo $value?></option>
										<?php }?>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo 'BILL PRINTING FORMAT'; ?></label>
							<div class="col-sm-3">
								<select name="PRINTER_MODEL" class="form-control">
									<?php foreach ($printer_model as $key => $value) { ?>
										<?php if($key==$PRINTER_MODEL){ ?>
											<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
										<?php } else{ ?>
											<option value="<?php echo $key ?>"><?php echo $value?></option>
										<?php }?>
									<?php }?>
								</select>
							</div>
						</div> 

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'VAT SHOW'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="VAT_SHOW" value="<?php echo $VAT_SHOW; ?>"  class="form-control" />
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'SCRG SHOW'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="SCRG_SHOW" value="<?php echo $SCRG_SHOW; ?>"  class="form-control" />
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-3 control-label"><?php echo 'IMAGE ON BILL'; ?></label>
							<div class="col-sm-3">
								<input type="text" name="IS_LOGO" value="<?php echo $IS_LOGO; ?>"  class="form-control" />
							</div>
						</div>
						
					</div>
				</div>
			</form>
		</div>
	</div>
	
</div>
<?php echo $footer; ?>