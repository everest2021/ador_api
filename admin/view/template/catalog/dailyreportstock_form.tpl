<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
								    <div class="col-sm-2">
								    	<label>Start Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div class="col-sm-2">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								    <div class="col-sm-1">
								    	<label>Department</label>
								    	<select class="form-control" name="filter_departmentvalue">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
									    	<?php foreach($departmentlist as $dept ) { ?>
									    		<?php if($dept['department'] == $departmentvalue ) { ?>
									    			<option value="<?php echo $dept['department'] ?>" selected="selected"><?php echo $dept['department']; ?></option>
									    		<?php } else { ?>
									    			<option value="<?php echo $dept['department'] ?>"><?php echo $dept['department']; ?></option>
									    		<?php } ?>	
									    	<?php } ?>	
								    	</select>
								    </div>
								    <div class="col-sm-2">
								    	<label>Category</label>
								    	<select class="form-control" name="filter_category">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($categorys as $key => $value) { ?>
									    		<?php if($value['category_id'] == $category ) { ?>
									    			<option value="<?php echo $value['category_id'] ?>" selected="selected"><?php echo $value['category']?></option>
									    		<?php } else { ?>
									    			<option value="<?php echo $value['category_id'] ?>"><?php echo $value['category']?></option>
									    		<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								    <div class="col-sm-2">
								    	<label>SubCategory</label>
								    	<select class="form-control" name="filter_subcategory">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($subcategorys as $key => $value) { ?>
									    		<?php if($value['category_id'] == $subcategory ) { ?>
									    			<option value="<?php echo $value['category_id'] ?>" selected="selected"><?php echo $value['name']?></option>
									    		<?php } else { ?>
									    			<option value="<?php echo $value['category_id'] ?>"><?php echo $value['name']?></option>
									    		<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
								    <div class="col-sm-1">
								   		<label>Type</label>
								    	<select class="form-control" name="filter_type">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($types as $key => $value) { ?>
								    			<?php if($key == $type) { ?>
								    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
								    			<?php } else { ?>
								    				<option value="<?php echo $key ?>"><?php echo $value?></option>
								    			<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
									<div class="col-sm-2">
								    	<label>Table Group</label>
								    	<select class="form-control" name="filter_tablegroup">
								    		<option value='' selected="selected"><?php echo "All" ?></option>
								    		<?php foreach($tablegroups as $key => $value) { ?>
									    		<?php if($value['location_id'] == $tablegroup ) { ?>
									    			<option value="<?php echo $value['location_id'] ?>" selected="selected"><?php echo $value['location']?></option>
									    		<?php } else { ?>
									    			<option value="<?php echo $value['location_id'] ?>"><?php echo $value['location']?></option>
									    		<?php } ?>
								    		<?php } ?>
								    	</select>
								    </div>
									<div class="col-sm-12">
										<br>
										<center><input type="submit" class="btn btn-primary" value="Show"></center>
									</div>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="pdfprint" type="button" class="btn btn-primary">Export</button>
				 		<!-- <button id="xlsprint" type="button" class="btn btn-primary">Export</button> -->


				 	</div>
				
					<div class="col-sm-6 col-sm-offset-3">
					<center style = "display:none;">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Item Wise Sales Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Product Name</th>
							<th style="text-align: center;">Quantity</th>
							<th style="text-align: center;">Amount</th>
						</tr>
						<?php 
							$qtytotal = 0; 
							$amttotal = 0;
							$qtytotalm = 0; 
							$amttotalm = 0; 
							$vat = 0; 
							$gst = 0; 
							$stax = 0; 
							$grandtotal = 0; 
							$discount = 0; 
							$cancelamt = 0; 
							$roundoff = 0;
							$advance = 0;
							$packaging = 0;
							$packaging_cgst = 0;
							$packaging_sgst = 0;
						?>
						<?php foreach($departments as $dept => $value ) {?>
							<?php $departmentqty = 0; $departmentamt = 0;?>
							<?php if($value != array()) {?>
								<tr>
								  	<td colspan="3" style="text-align: left;padding-left: 80px;"><?php echo $dept ?></td>
							  	</tr>
							<?php } ?>
							<?php foreach($value as $key) {?>
						  	<tr>
						  	<?php if($key['cancel_bill'] == 1){ ?>
						  		<td><?php echo $key['name'] = '' ?></td>
						  		<td><?php echo $key['quantity'] = 0 ?></td>
						  		<td><?php echo $key['amount'] = 0 ?></td>
						  	<?php } else {?>
						  		<td><?php echo $key['name'] ?></td>
						  		<td><?php echo $key['quantity'] ?></td>
						  		<td><?php echo $key['amount'] ?></td>
						  	<?php } //echo '<pre>';print_r($key['amount']);?>
						  		<?php $qtytotal = $qtytotal + $key['quantity']; $amttotal = $amttotal + $key['amount'];?>
						  		<?php $departmentqty = $departmentqty + $key['quantity']; $departmentamt = $departmentamt + $key['amount'];?>
						  	</tr>
						  	<?php }?>
						  	
						  	<?php if($value != array()) {?>
							  	<tr>
							  		<td>Total</td>
							  		<td><?php echo $departmentqty ?></td>
							  		<td><?php echo $departmentamt ?></td>
							  	</tr>
							<?php } ?>
					  	<?php } //exit;?>
					  		<tr>
						  		<td colspan="4"></td>
					  		</tr>
					  	<?php /*<tr>
					  		<td colspan="4"><b>Modifiers</b></td>
					  	</tr>
					  	<?php foreach($departmentsm as $dept => $value ) {?>
							<?php $departmentqty = 0; $departmentamt = 0;?>
							<?php if($value != array()) {?>
								<tr>
								  	<td colspan="3" style="text-align: left;padding-left: 80px;"><?php echo $dept ?></td>
							  	</tr>
							<?php } ?>
							<?php foreach($value as $key) {?>
						  	<tr>
						  	<?php if($key['cancel_bill'] == 1){ ?>
						  		<td><?php echo $key['name'] = '' ?></td>
						  		<td><?php echo $key['quantity'] = 0 ?></td>
						  		<td><?php echo $key['amount'] = 0 ?></td>
						  	<?php } else {?>
						  		<td><?php echo $key['name'] ?></td>
						  		<td><?php echo $key['quantity'] ?></td>
						  		<td><?php echo $key['amount'] ?></td>
						  	<?php } ?>
						  		<?php $qtytotalm = $qtytotalm + $key['quantity']; $amttotalm = $amttotalm + $key['amount'];?>
						  		<?php $departmentqty = $departmentqty + $key['quantity']; $departmentamt = $departmentamt + $key['amount'];?>
						  	</tr>
						  	<?php }?>
						  	<?php if($value != array()) {?>
							  	<tr>
							  		<td>Total</td>
							  		<td><?php echo $departmentqty ?></td>
							  		<td><?php echo $departmentamt ?></td>
							  	</tr>
							<?php } ?>
					  	<?php }?>
					  	<tr>
					  		<td colspan="4"><b>Modifiers</b></td>
					  	</tr> */?>
					  	<?php if($modifiers != []) { ?>
						  	<tr>
						  		<td colspan="4"><b>Modifiers Start</b></td>
						  	</tr>
						  	<?php $qtytotalm = 0; $amttotalm = 0;?>
						  	<?php foreach($modifiers as $modifier) { ?>
						  	<tr>
						  		<td><?php echo $modifier['name'] ?></td>
						  		<td><?php echo $modifier['quantity'] ?></td>
						  		<td><?php echo $modifier['amount'] ?></td>
						  		<?php 
						  			$qtytotalm = $qtytotalm + $modifier['quantity'];
						  			$amttotalm = $amttotalm + $modifier['amount'];
						  		 ?>
						  	</tr>
						  	<?php } ?>
						  	
						  	<tr>
						  		<td>Total</td>
						  		<td><?php echo $qtytotalm ?></td>
						  		<td><?php echo $amttotalm ?></td>
						  	</tr>
						  	<tr>
						  		<td colspan="4"><b>Modifiers End</b></td>
						  	</tr>
					  	<?php } ?>
					  	<?php if($qtytotal != '0' && $amttotal != '0' ) { ?>
					  		<tr>
						  		<td><b>Grand Total</b></td>
						  		<td><b><?php echo $qtytotal ?></b></td>
						  		<td><b><?php echo $amttotal ?></b></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2"><b>Gross Total : </b></td>
						  		<td><b><?php echo $amttotal ?></b></td>
					  		</tr>
					  		<?php if($_POST['filter_departmentvalue'] == '' && $_POST['filter_category'] == '' && $_POST['filter_subcategory'] == '' && $_POST['filter_type'] == '' && $_POST['filter_tablegroup'] == '') { ?> 
					  		<tr>
						  		<td colspan="2">P- Chrg Amt (+) : </td>
						  		<td>0</td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">O- Chrg Amt (+) : </td>
						  		<td>0</td>
					  		</tr>
					  		<?php foreach($totals as $total) {
					  			// echo "<pre>";
					  			// print_r($total);
					  			// exit;
					  			if($total['liq_cancel'] == 1){
						  			$vat = 0;
						  		}else{
						  			$vat = $vat + $total['vat'];
						  		}
						  		if($total['food_cancel'] == 1){
						  			$gst = 0;
						  		} else{
						  			$gst = $gst + $total['gst'];
						  		}

						  		$packaging = $packaging + $total['packaging'];
						  		$packaging_cgst = $packaging_cgst + $total['packaging_cgst'];
						  		$packaging_sgst = $packaging_sgst + $total['packaging_sgst'];

						  		$stax = $stax + $total['stax'];
						  		if($total['food_cancel'] == 1){
						  			$total['ftotalvalue'] = 0;
						  		}
						  		if($total['liq_cancel'] == 1){
						  			$total['ltotalvalue'] = 0;
						  		}
						  		$discount = $discount + $total['ftotalvalue'] + $total['ltotalvalue'];
						  		$grandtotal = $grandtotal + $total['grand_total'];
						  		if($total['liq_cancel'] == 1 || $total['food_cancel'] == 1){
						  			$total['roundtotal'] = 0;
						  		}
						  		$roundoff = $roundoff + $total['roundtotal'];
						  		$advance = $advance + $total['advance_amount'];
					  		} ?>
					  		<tr>
						  		<td colspan="2">S- Chrg Amt (+) : </td>
						  		<td><?php echo $stax ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">Vat Amt (+) :</td>
						  		<td><?php echo $vat ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">GST Amt (+) :</td>
						  		<td><?php echo $gst ?></td>
					  		</tr>

					  		<tr>
						  		<td colspan="2">Packaging AMT (+) :</td>
						  		<td><?php echo $packaging ?></td>
					  		</tr>

					  		<tr>
						  		<td colspan="2">Packaging CGST (+) :</td>
						  		<td><?php echo $packaging_cgst ?></td>
					  		</tr>

					  		<tr>
						  		<td colspan="2">Packaging SGST (+) :</td>
						  		<td><?php echo $packaging_sgst ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">R-Off Amt (+) :</td>
						  		<td><?php echo $roundoff / 2; ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">Discount Amt (-) :</td>
						  		<td><?php echo $discount ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">P- Discount Amt (-) :</td>
						  		<td>0</td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">Cancel Bill Amt (-) :</td>
						  		<td><?php echo $cancelamount ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">Comp. Amt (-) :</td>
						  		<td>0</td>
					  		</tr>
					  		<tr>
						  		<td colspan="2">Advance. Amt (-):</td>
						  		<td><?php echo $advance ?></td>
					  		</tr>

					  		<tr>
						  		<td colspan="2"><b>Net Amount :</b></td>
						  		<?php if($INCLUSIVE == 1){ ?>
						  			<td><b><?php echo ($amttotal + $stax + $roundoff) - $discount//$grandtotal ?></b></td>
						  		<?php } else { ?>
						  			<td><b><?php echo ($amttotal + $stax + $vat + $gst + $roundoff + $packaging + $packaging_cgst + $packaging_sgst) - $discount//$grandtotal ?></b></td>
						  		<?php } ?>
					  		</tr>
					  		<tr>
						  		<td colspan="2"><b>Advance Amt (+) :</b></td>
						  		<td><?php echo $advancetotal ?></td>
					  		</tr>
					  		<tr>
						  		<td colspan="2"><b>Grand Total (+) :</b></td>
						  		<?php if($INCLUSIVE == 1){ ?>
						  			<td><b><?php echo ($amttotal + $stax + $roundoff) - $discount//$grandtotal ?></b></td>
						  		<?php } else { ?>
						  			<td><b><?php echo ($amttotal + $stax + $vat + $gst + $roundoff + $packaging + $packaging_cgst + $packaging_sgst) - $discount//$grandtotal ?></b></td>
						  		<?php } ?>
					  		</tr>
					  		<?php } ?>
					  </table>
					  <br>
					  <center><h5><b>End of Report</b></h5></center>
					  <?php }?>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/dailyreportstock/itemwisereport&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_departmentvalue = $('select[name=\'filter_departmentvalue\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();
		  var filter_subcategory = $('select[name=\'filter_subcategory\']').val();
		  var filter_type = $('select[name=\'filter_type\']').val();
		  var filter_tablegroup = $('select[name=\'filter_tablegroup\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_departmentvalue) {
			  url += '&filter_departmentvalue=' + encodeURIComponent(filter_departmentvalue);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  if (filter_subcategory) {
			  url += '&filter_subcategory=' + encodeURIComponent(filter_subcategory);
		  }

		  if (filter_type) {
			  url += '&filter_type=' + encodeURIComponent(filter_type);
		  }

		  if (filter_tablegroup) {
			  url += '&filter_tablegroup=' + encodeURIComponent(filter_tablegroup);
		  }

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#pdfprint').on('click', function() {

			

		  var url = 'index.php?route=catalog/dailyreportstock/pdfprints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_departmentvalue = $('select[name=\'filter_departmentvalue\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();
		  var filter_subcategory = $('select[name=\'filter_subcategory\']').val();
		  var filter_type = $('select[name=\'filter_type\']').val();
		  var filter_tablegroup = $('select[name=\'filter_tablegroup\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_departmentvalue) {
			  url += '&filter_departmentvalue=' + encodeURIComponent(filter_departmentvalue);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  if (filter_subcategory) {
			  url += '&filter_subcategory=' + encodeURIComponent(filter_subcategory);
		  }

		  if (filter_type) {
			  url += '&filter_type=' + encodeURIComponent(filter_type);
		  }

		  if (filter_tablegroup) {
			  url += '&filter_tablegroup=' + encodeURIComponent(filter_tablegroup);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});

		$('#sendmail').on('click', function() {

			

		  var url = 'index.php?route=catalog/dailyreportstock/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_departmentvalue = $('select[name=\'filter_departmentvalue\']').val();
		  var filter_category = $('select[name=\'filter_category\']').val();
		  var filter_subcategory = $('select[name=\'filter_subcategory\']').val();
		  var filter_type = $('select[name=\'filter_type\']').val();
		  var filter_tablegroup = $('select[name=\'filter_tablegroup\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_departmentvalue) {
			  url += '&filter_departmentvalue=' + encodeURIComponent(filter_departmentvalue);
		  }

		  if (filter_category) {
			  url += '&filter_category=' + encodeURIComponent(filter_category);
		  }

		  if (filter_subcategory) {
			  url += '&filter_subcategory=' + encodeURIComponent(filter_subcategory);
		  }

		  if (filter_type) {
			  url += '&filter_type=' + encodeURIComponent(filter_type);
		  }

		  if (filter_tablegroup) {
			  url += '&filter_tablegroup=' + encodeURIComponent(filter_tablegroup);
		  }

		  // url += '&datatest=' + encodeURIComponent(btoa(JSON.stringify(forprintarray)));

		  location = url;
		  //setTimeout(close_fun_1, 50);
		});
	</script>

	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>