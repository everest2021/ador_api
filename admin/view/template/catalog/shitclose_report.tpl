<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<div class="col-sm-offset-4" >
					       		<center><h4><b>Select Date</b></h4></center><br>
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					<?php /*<div class="table-responsive col-sm-6 col-sm-offset-3">
					  <table class="table table-bordered table-hover" style="text-align: center;">
						  	<tr>
						  		<th style="text-align: center;">Bill Number</th>
						  		<th style="text-align: center;">Bill Amount</th>
						  	</tr>
						  	<?php $total = 0; ?>
						  	<?php foreach ($billdatas as $data) {?>
						  		<tr>
							  		<td><?php echo $data['order_no'] ?></td>
							  		<td><?php echo $data['grand_total'] ?></td>
							  		<?php $total = $total + $data['grand_total']  ?>
						  		</tr>
						  	<?php } ?>
						  	<?php if($total != '0') { ?>
						  		<tr>
							  		<td><b>Total</b></td>
							  		<td><b><?php echo $total ?></b></td>
						  		</tr>
						  	<?php }?>
					  	</table>
				 	</div> */?>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-8 col-sm-offset-2">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Shift Close Report</b></h3></center>
					<h3>From : <?php echo date('d/m/Y',strtotime($startdate)); ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo date('d/m/Y',strtotime($enddate)); ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Shift Id</th>
							<!-- <th style="text-align: center;">Date</th> -->
							<th style="text-align: center;">Closing Time</th>
							<th style="text-align: center;">Total Bill</th>
							<th style="text-align: center;">Total Sale</th>
							<th style="text-align: center;">Total Cash</th>
							<th style="text-align: center;">Total OnAcc</th>
							<th style="text-align: center;">Total Card</th>
						</tr>
						<?php $grandtotal=0;  ?>
							  	<?php foreach ($showdata as $key => $value) {?>
							  		<tr>
							  			<td colspan="7"><b><?php echo $key; ?><b></td>
							  		</tr> 
								  	<?php foreach ($value as $akey =>$data1) { ?>
								  		<tr>
									  		<td><?php echo $data1['shift_id']; ?></td>
									  		<!-- <td><?php echo date('d-m-Y',strtotime($data1['bill_date'])); ?></td> -->
									  		<td><?php echo $data1['shift_time'] ?></td>
									  		<td><?php echo $data1['total']; ?></td>
									  		<td><?php echo $data1['totalbill']; ?></td>
									  		<td><?php echo $data1['cash']; ?></td>
									  		<td><?php echo $data1['onacc']; ?></td>
									  		<td><?php echo $data1['card'] + $data1['online']; ?></td>
									  	</tr> 
										<?php $grandtotal += $data1['totalbill'];
									 }  ?> 
							  	<?php } ?>
							  	<tr>
							  		<td colspan="6"><b>Grand Total :</b></td>
							  		<td  style="text-align: left;"><b><?php echo $grandtotal ?></b></td>
							  	</tr>
					  	
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/shiftclose_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/shiftclose_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>