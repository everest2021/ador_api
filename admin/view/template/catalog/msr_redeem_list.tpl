<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div class="pull-right">
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button style="display: none;"> type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		    </div>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			       <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Customer Name'; ?></label>
				          <input type="text" name="filter_customername" value="<?php echo $filter_customername; ?>" placeholder="<?php echo 'Customer Name'; ?>" id="input-filter_customername" class="form-control" />
				          <input type="hidden" name="filter_cust_id" value="<?php echo $filter_cust_id; ?>" id="input-filter_cust_id" class="form-control" />
				        </div>
				        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			       </div>
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
					<td><?php if ($sort == 'customer_name') { ?>
					<a href="<?php echo $sort_customer_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Customer Name'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_customer_name; ?>"><?php echo 'Customer Name'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'contact') { ?>
					<a href="<?php echo $sort_contact; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Contact'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_contact; ?>"><?php echo 'Contact'; ?></a>
					<?php } ?></td>
					
					<td>
					<a href=""><?php echo 'Previous Balance'; ?></a>
					</td>
					<td>
					<a href=""><?php echo 'Amount'; ?></a>
					</td>
					<td>
					<a href=""><?php echo 'Extra Amount'; ?></a>
					</td>
					<td>
					<a href=""><?php echo 'Total Amt'; ?></a>
					</td>
					<td>
					<a href=""><?php echo 'Pay By'; ?></a>
					</td>

				  <td class="text-right" style="display: none"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
				<?php foreach ($items as $item) { ?>
				<tr>
				  <td><?php if (in_array($item['cust_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['cust_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['cust_id']; ?>" />
					<?php } ?></td>
				  <td><?php echo $item['customername']; ?></td>
				  <td><?php echo $item['contact']; ?></td>
				  <td><?php echo $item['pre_bal']; ?></td>
				  <td><?php echo $item['amount']; ?></td>
				  <td><?php echo $item['extra_amt']; ?></td>
				  <td><?php echo $item['total_amt']; ?></td>
				  <td><?php echo $item['payby']; ?></td>
				  <td class="text-right" style="display: none">
				  	<a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
				  </td>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>	
<script type="text/javascript">
$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/msr_recharge&token=<?php echo $token; ?>';

  	var filter_customername = $('input[name=\'filter_customername\']').val();
  	if (filter_customername) {
		var filter_cust_id = $('input[name=\'filter_cust_id\']').val();
		if (filter_cust_id) {
	  		url += '&filter_cust_id=' + encodeURIComponent(filter_cust_id);
	  		url += '&filter_customername=' + encodeURIComponent(filter_customername);
	  	}
  	}

  	location = url;
});

$('input[name=\'filter_customername\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/msr_recharge/autocomplete&token=<?php echo $token; ?>&filter_customername=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.customername,
            value: item.item_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_customername\']').val(ui.item.label);
    $('input[name=\'filter_cust_id\']').val(ui.item.value);	
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});
</script>
<?php echo $footer; ?>