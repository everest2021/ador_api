<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport_group').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo 'Sport Group Name'; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo 'SportGroup Name'; ?>" id="input-filter_name" class="form-control" />
                <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="input-filter_name_id" class="form-control" />
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-sport_type"><?php echo 'Sport Type'; ?></label>
                <select name="filter_sport_type" id="filter_sport_type" class="form-control">
                  <option value="*">All</option>
                  <?php foreach($sport_types as $skey => $svalue){ ?>
                    <?php if($skey == $filter_sport_type){ ?>
                      <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-participant_type"><?php echo 'Participant Type'; ?></label>
                <select name="filter_participant_type" id="filter_participant_type" class="form-control">
                  <option value="*">All</option>
                  <?php foreach($part_types as $skey => $svalue){ ?>
                    <?php if($skey == $filter_participant_type){ ?>
                      <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport_group">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'sport_group') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'sport_type') { ?>
                    <a href="<?php echo $sort_sport_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sport Type'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sport_type; ?>"><?php echo 'Sport Type'; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'participant_type') { ?>
                    <a href="<?php echo $sort_participant_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Participant Type'; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_participant_type; ?>"><?php echo 'Participant Type'; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($sport_groups) { ?>
                <?php foreach ($sport_groups as $sport_group) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($sport_group['sport_group_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $sport_group['sport_group_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $sport_group['sport_group_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $sport_group['sport_group']; ?></td>
                  <td class="text-left"><?php echo $sport_group['sport_type']; ?></td>
                  <td class="text-left"><?php echo $sport_group['participant_type']; ?></td>
                  <td class="text-right"><a href="<?php echo $sport_group['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/sport_group&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').val();
  if (filter_name) {
    var filter_name_id = $('input[name=\'filter_name_id\']').val();
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }
  }

  var filter_sport_type = $('select[name=\'filter_sport_type\']').val();
  if (filter_sport_type != '*') {
    url += '&filter_sport_type=' + encodeURIComponent(filter_sport_type);
  }

  var filter_participant_type = $('select[name=\'filter_participant_type\']').val();
  if (filter_participant_type != '*') {
    url += '&filter_participant_type=' + encodeURIComponent(filter_participant_type);
  }

  location = url;
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/sport_group/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['sport_group'],
            value: item['sport_group_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_name\']').val(item['label']);
    $('input[name=\'filter_name_id\']').val(item['value']);
  }
});
//--></script></div>
<?php echo $footer; ?>