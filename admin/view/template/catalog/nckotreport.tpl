<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show">
									<a id="export" type="button" class="btn btn-primary">Export</a></center>
								</div>
							</div>
					    </div>
					</div>
				 	<!-- <div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div> -->
					<div class="col-sm-6 col-sm-offset-3">
					<center style = "display:none">
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>NC Kot Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Ref No</th>
							<th style="text-align: center;">Table No.</th>
							<th style="text-align: center;">Bill Date</th>
							<th style="text-align: center;">Item Name</th>
							<th style="text-align: center;">Qty</th>
							<th style="text-align: center;">Rate</th>
							<th style="text-align: center;">Amount</th>
							<th style="text-align: center;">Nc Kot Reason</th>
						</tr>
						<?php if($nckotdata){ ?>
							<?php $totalamount = 0;$amount = 0;$totalqty = 0;?>
							<?php foreach($nckotdata as $key => $data) { ?>
									<tr>
										<td><?php echo $data['order_no'] ?></td>
										<td><?php echo $data['table_id'] ?></td>
										<td><?php echo $data['bill_date'] ?></td>
										<td><?php echo $data['name'] ?></td>
										<td><?php echo $data['qty'] ?></td>
										<td><?php echo $data['rate'] ?></td>
										<td><?php echo $data['qty']*$data['rate'] ?></td>
										<td><?php echo $data['nc_kot_reason'] ?></td>
									</tr>
								<?php $totalamount = $totalamount + ($data['qty']*$data['rate']);
								$totalqty = $totalqty + $data['qty'];?>
							<?php } ?>
							<tr>
								<td colspan="4">Total</td>
								<td><?php echo $totalqty;?></td>
								<td></td>
								<td><?php echo $totalamount;?></td>
								<td></td>
							</tr>
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/nckotreport/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

	 	$('#export').on('click', function() {
		  	var url = 'index.php?route=catalog/nckotreport/export&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		 	//var filter_id = $('input[name=\'filter_id\']').val();
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		  	//alert(url);
		  	location = url;
		});



		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
</div>
<?php echo $footer; ?>