<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-4">
						       		<div class="form-row">
									    <div class="col-sm-3">
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					<?php /*<div class="table-responsive col-sm-6 col-sm-offset-3">
					  <table class="table table-bordered table-hover" style="text-align: center;">
						  	<tr>
						  		<th style="text-align: center;">Bill Number</th>
						  		<th style="text-align: center;">Bill Amount</th>
						  	</tr>
						  	<?php $total = 0; ?>
						  	<?php foreach ($billdatas as $data) {?>
						  		<tr>
							  		<td><?php echo $data['order_no'] ?></td>
							  		<td><?php echo $data['grand_total'] ?></td>
							  		<?php $total = $total + $data['grand_total']  ?>
						  		</tr>
						  	<?php } ?>
						  	<?php if($total != '0') { ?>
						  		<tr>
							  		<td><b>Total</b></td>
							  		<td><b><?php echo $total ?></b></td>
						  		</tr>
						  	<?php }?>
					  	</table>
				 	</div> */?>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Billwise Sales Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Ref No</th>
							<th style="text-align: center;">Food</th>
							<th style="text-align: center;">GST</th>
							<th style="text-align: center;">F.Disc</th>
							<th style="text-align: center;">Bar</th>
							<th style="text-align: center;">VAT</th>
							<th style="text-align: center;">S-Chrg</th>
							<th style="text-align: center;">Packing-Chrg</th>
							<th style="text-align: center;">L.Disc</th>
							<th style="text-align: center;">Total</th>
							<th style="text-align: center;">Pay By</th>
							<th style="text-align: center;">Pay By Cash</th>
							<th style="text-align: center;">Pay By Card</th>
							<th style="text-align: center;">Pay By Online</th>
							<th style="text-align: center;">On Account</th>
							<th style="text-align: center;">T No</th>
							<th style="text-align: center;">Time</th>

						</tr>
						<?php if(isset($_POST['submit'])){ ?>
							<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0; $packaging = 0;$packaging_cgst = 0;$packaging_sgst = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="17"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sdiscount = 0; $sgrandtotal = 0; $svat = 0; $sgst = 0; $sfoodtotal = 0; $sfdistotal = 0; $sliqtotal = 0;$sldistotal = 0; $sdiscountvalue = 0; $safterdiscount = 0; $sstax = 0; $sroundoff = 0; $stotal = 0; $sadvance = 0; $spackaging = 0;$spackaging_cgst = 0;$spackaging_sgst = 0;?>
								  	<?php foreach ($value as $data) { //echo'<pre>';print_r($data);exit;?>
								  		<tr >
									  		<td><?php echo $data['order_no'] ?></td>
									  		<?php if($data['food_cancel'] == 1) { ?>
									  			<td><?php echo $data['ftotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ftotal']; $foodtotal += $data['ftotal'];?></td>
									  		<?php } ?>
									  		<td><?php echo $data['gst'] ?></td>
									  		<td><?php echo $data['ftotalvalue']; $fdistotal += $data['ftotalvalue'] ?></td>
									  		<?php if($data['liq_cancel'] == 1) { ?>
									  			<td><?php echo $data['ltotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ltotal']; $liqtotal +=$data['ltotal'];  ?></td>
									  		<?php } ?>
									  		<td><?php echo $data['vat'] ?></td>
									  		<td><?php echo $data['stax'] ?></td>
									  		<td><?php echo $data['packaging'] ?></td>
									  		<td><?php echo $data['ltotalvalue'];$ldistotal +=$data['ltotalvalue']; ?></td>
									  		<?php if($INCLUSIVE == 1) { ?>
									  			<td><?php echo round($data['ftotal'] + $data['ltotal'])?></td>
									  		<?php } else { ?>
									  			<td><?php echo round($data['grand_total'] ); ?></td>
									  		<?php } ?>
									  		<?php if($data['pay_cash'] != '0') { ?>
									  			<td>Cash</td>
									  		<?php } else if($data['urbanpiper_order_id'] != '0' || $data['urbanpiper_order_id'] != '') { ?>
									  			<td><?php echo $data['order_from'] ?></td>
									  		<?php } else if($data['pay_card'] != '0') { ?>
									  			<td>Card</td>
									  		<?php } else if($data['pay_online'] != '0') { ?>
									  			<td>Online(<?php echo $data['payment_type'] ?>)</td>
									  		<?php } else if($data['onac'] != '0.00') { ?>
									  			<td>OnAc</td>
									  		<?php } else if($data['mealpass'] != '0') { ?>
									  			<td>Meal Pass</td>
									  		<?php } else if($data['pass'] != '0') { ?>
									  			<td>Pass</td>
									  		<?php } else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { ?>
									  			<td>Cash+Card</td>
									  		<?php } else { ?>
									  			<td>Pending</td>
									  		<?php } ?>

									  		<td><?php echo $data['pay_cash'] ?></td>
									  		<td><?php echo $data['pay_card'] ?></td>
									  		<td><?php echo $data['pay_online'] ?></td>
									  		<td><?php echo $data['onac'] ?></td>

									  		<td><?php echo $data['t_name'] ?></td>
									  		<td><?php echo $data['out_time'] ?></td>
									  	</tr>
									  	<?php 
									  	 	  if($data['liq_cancel'] == 1){
									  		  	$vat = 0;
									  		  	$svat = 0;
									  		  } else{
									  		  	$vat = $vat + $data['vat'];
									  		  	$svat = $svat + $data['vat'];

									  		  }
									  		  if($data['food_cancel'] == 1){
									  		  	$gst = 0;
									  		  	$sgst = 0;
									  		  } else{
									  		  	$gst = $gst + $data['gst'];
									  		  	$sgst = $sgst + $data['gst'];
									  		  }
									  		  $packaging = $packaging + $data['packaging'];
										  		$packaging_cgst = $packaging_cgst + $data['packaging_cgst'];
										  		$packaging_sgst = $packaging_sgst + $data['packaging_sgst'];
									  		  $stax = $stax + $data['stax'];
									  		  if($data['food_cancel'] == 1){
									  		  	$data['ftotalvalue'] = 0; 
									  		  } 
									  		  if($data['liq_cancel'] == 1){
									  		  	$data['ltotalvalue'] = 0;
									  		  }
									  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
									  		  if($INCLUSIVE == 1){
									  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
									  		  	$sgrandtotal = round($sgrandtotal + $data['ftotal'] + $data['ltotal']); 
									  		  	$total = $grandtotal + $stax;
									  		  	$stotal = $sgrandtotal + $sstax;


									  		  } else{
								  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['packaging'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
								  		  	 	$sgrandtotal = round($sgrandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['packaging'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
								  		  	 	$total = $grandtotal + $gst + $vat + $stax + $packaging;
								  		  	 	$stotal = $sgrandtotal + $sgst + $svat + $sstax;


									  		  }
									  		  $discountvalue = $discountvalue + $discount;
									  		  $sdiscountvalue = $sdiscountvalue + $discount;
									  		  $afterdiscount = $total - $discountvalue;
									  		   $safterdiscount = $stotal - $sdiscountvalue;
									  		 // $roundoff = $roundoff + $data['roundtotal'] /2 ;
									  		   $roundoff = $roundoff + $data['roundtotal']  ;
									  		  $sroundoff = $sroundoff + $data['roundtotal'];
									  		  $advance = $advance + $data['advance_amount'];
									  	?>
									<?php } ?>
									<!-- <tr>
								  		<td colspan="10"><b>Bill Total :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php  echo $sgrandtotal ?>  </b></td>
								  	</tr>

								  	<tr>
								  		<td colspan="10">GST Amt (+) :</td>
								  		<td colspan="5" style="text-align: left;"><?php echo $sgst ?></td>
							  		</tr>

							  		<tr>
								  		<td colspan="10"><b>Total :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php echo ($stotal + $sroundoff) ?></b></td>
							  		</tr>

								  	<tr>
								  		<td colspan="10">Discount Amt (-) :</td>
								  		<td colspan="5" style="text-align: left;"><?php echo $sdiscountvalue ?></td>
							  		</tr>

							  		<tr>
								  		<td colspan="10"><b>Net Amount :</b></td>
								  		<td colspan="5" style="text-align: left;"><b><?php echo ($safterdiscount + $sroundoff) ?></b></td>
							  		</tr> -->
							  	<?php }  ?>
							  	<tr>
							  		<td colspan="17"><b style="font-size: 25px;">All Total </b></td>
							  		
							  	</tr>
							  	<tr>
							  		<td colspan="10"><b>Bill Total :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $grandtotal ?></b></td>
							  	</tr>
							  	<tr>
							  		<td colspan="10">O- Chrg Amt (+) : </td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Vat Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $vat ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">S- Chrg Amt (+) : </td>
							  		<td colspan="7" style="text-align: left;"><?php echo $stax ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">GST Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $gst ?></td>
						  		</tr>
						  			<tr>
							  		<td colspan="10">KKC Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  			<tr>
							  		<td colspan="10">SBC Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">R-Off Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $roundoff; ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Packaging (+)</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $packaging ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Total :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($total + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Discount Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $discountvalue ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Cancel BIll Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $cancelamount ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Comp. Amt (-) :</td>
							  		<td colspan="7" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Advance. Amt (-):</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $advance ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Net Amount :</b></td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10">Advance. Amt (+) :</td>
							  		<td colspan="7" style="text-align: left;"><?php echo $advancetotal ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Grand Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>NC KOT :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $nc_kot_amt ?></b></td>
						  		</tr>


						  		<tr>
							  		<td colspan="10"><b>Food Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $foodtotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Total Food Discount (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $fdistotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Liquar Total (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $liqtotal ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Total Liquar Discount (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $ldistotal ?></b></td>
						  		</tr>

						  		<!-- <tr>
							  		<td colspan="10"><b>Packaging (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $packaging ?></b></td>
						  		</tr>

						  		<tr>
							  		<td colspan="10"><b>Packaging CGST  (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $packaging_cgst ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="10"><b>Packaging SGST (+) :</td>
							  		<td colspan="7" style="text-align: left;"><b><?php echo $packaging_sgst ?></b></td>
						  		</tr> -->
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/dailybill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/dailybill/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#sendmail').on('click', function() {
		  var url = 'index.php?route=catalog/dailybill/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>