<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
	<div class="container-fluid">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="subbom">
			<div class="panel-body">
			    <div class="well">
				   <div class="row">
				       <div class="col-sm-12">
				       		<div class="col-sm-2 col-sm-offset-5">
				       			<h4><b>BOM Type</b></h4>
				       			<h5><?php echo $bomtype?></h5>
				       		</div>
				       		<div class="col-sm-1">
				       			<h4><b>Category</b></h4>
				       			<h5><?php echo $category?></h5>
				       		</div>
						</div>
						<div class="col-sm-12">
							<br>
							<div class="col-sm-1 col-sm-offset-2">
								<h4 style="margin-top:6px;"><b>Name :</b></h4>
							</div>
							<div class="col-sm-8">
								<table class="table table-bordered">
									<tr>
										<td style="text-align: center;"><?php echo $code?></td>
										<td style="text-align: center;"><?php echo $name?></td>
									</tr>
								</table>
							</div>
						</div>
				    </div>
			 	</div>
			 	<div class="pull-right">
			    	<button type="button"  id="addbutton" class="btn btn-primary" onclick="validate()"><i class="fa fa-plus"></i></button>
			       	<button type="button"  class="btn btn-danger" onclick="$('#delete').submit()"><i class="fa fa-trash-o"></i></button>
			       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    	</div>
			 	<div class="form-group col-md-12 col-sm-offset-2"">
		   			<label class="col-sm-2 style= control-label" style="margin-top:6px;width: 9%"><?php echo 'Category :'; ?></label>
			   		<div class="col-sm-3">
				  		<select class="form-control" id="filter_subbomcategory" name="filter_subbomcategory">
				    		<option value='' selected="selected"><?php echo "All" ?></option>
				    		<?php foreach($subbomcategorys as $value) { ?>
				    			<?php if($value['id'] == $subbomcategory) { ?>
				    				<option value="<?php echo $value['id'] ?>" selected="selected"><?php echo $value['name'] ?></option>
				    			<?php } else { ?>
				    			 	<option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>
				    			<?php } ?>
				    		<?php } ?>
						</select>
			   		</div>
			    </div>
			    <div class="form-group col-md-12 col-sm-offset-2" style="border-top: none;">
		   			<label class="col-sm-2 style= control-label" style="margin-top:6px;width: 9%"><?php echo 'Item Name :'; ?></label>
			   		<div class="col-sm-3">
				  		<select class="form-control" id = "filter_subbomitem" name="filter_subbomitem">
				    		<option value='' selected="selected"><?php echo "All" ?></option>
				    		<?php foreach($subbomitemdata as $key => $value) { ?>
				    			<?php if($value['item_code'] == $subbomitem) { ?>
				    				<option value="<?php echo $value['item_code'] ?>" selected="selected"><?php echo $value['item_name'] ?></option>
				    			<?php } else { ?>
				    			 	<option value="<?php echo $value['item_code'] ?>" ><?php echo $value['item_name'] ?></option>
				    			<?php } ?>
				    		<?php } ?>
				    		<input type="hidden" id="purchase_price">
						</select>
			   		</div>
			    </div>
			    <div class="form-group col-md-12 col-sm-offset-2" style="border-top: none;">
		   			<label class="col-sm-2 style= control-label" style="margin-top:6px;width: 9%"><?php echo 'Qty :'; ?></label>
			   		<div class="col-sm-1">
				  		<input type="text" class="form-control" id="qty" name="qty" />
			   		</div>
			   		<label class="col-sm-2 style= control-label" style="margin-top:6px;width:6%"><?php echo 'Unit :'; ?></label>
			   		<!-- <div class="col-sm-1">
				  		<input type="text" class="form-control" id="unit_name" value="" name="unit_name"/>
				  		<input type="hidden"  id="unit_id" value="" name="unit_id"/>
			   		</div> -->
			   		<div class="col-sm-1">
				  		<select class="form-control" name="filter_unit" id="filter_unit">
				    		<option value='' selected="selected"><?php echo "All" ?></option>
				    		<?php foreach($units as $key => $value) { ?>
				    			<?php if($value['unit_id'] == $unit) { ?>
				    				<option value="<?php echo $value['unit_id'] ?>" selected="selected"><?php echo $value['unit'] ?></option>
				    			<?php } else { ?>
				    			 	<option value="<?php echo $value['unit_id'] ?>" ><?php echo $value['unit'] ?></option>
				    			<?php } ?>
				    		<?php } ?>
				    		<input type="hidden" id="unit_name">
						</select>
			   		</div>
			   		<label class="col-sm-2 style= control-label" style="margin-top:6px;width:8%"><?php echo 'Wastage :'; ?></label>
			   		<div class="col-sm-1">
				  		<input type="text" class="form-control" id="wastage" value="0" name="wastage"/>
			   		</div>
			    </div>
			    <div class="form-group col-md-12 col-sm-offset-2" style="border-top: none;">
		   			<label class="col-sm-2 style= control-label" style="margin-top:6px;width: 9%"><?php echo 'Cost :'; ?></label>
			   		<div class="col-sm-1">
				  		<input type="text" class="form-control" id="cost" value="0" name="cost"/>
			   		</div>
			    </div>
			    <div class="form-group col-md-12 col-sm-offset-2" style="border-top: none;">
		   			<label class="col-sm-2 style= control-label" style="margin-top:6px;width: 9%"><?php echo 'Store :'; ?></label>
			   		<div class="col-sm-3">
				  		<select class="form-control" id="store" name="filter_store">
				    		<option value="" selected="selected"><?php echo "All" ?></option>
				    		<?php foreach($stores as $key => $value) { ?>
				    			<?php if($value['id'] == $store) { ?>
				    				<option value="<?php echo $value['id'] ?>" selected="selected"><?php echo $value['store_name'] ?></option>
				    			<?php } else { ?>
				    			 	<option value="<?php echo $value['id'] ?>" ><?php echo $value['store_name'] ?></option>
				    			<?php } ?>
				    		<?php } ?>
						</select>
			   		</div>
			    </div>
			</div>
<!-- 			<div class="col-sm-12">
				<br>
				<center><input type="submit" class="btn btn-primary" value="Add"></center>
				<br>
			</div> -->
		</form>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="delete">
			<div class="col-md-12">
			<table class="table table-bordered">
				<tr>
					<th style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>
					<th>Store Name</th>
					<th>Item Code</th>
					<th>Item Name</th>
					<th>Qty</th>
					<th>Unit</th>
					<th>Cost</th>
				</tr>
				<?php if ($lists) { $total_cost = 0 ;?>
					<?php foreach($lists as $list) { //echo'<pre>';print_r($list);exit;?>
					<tr>
						<td class="text-center"><?php if (in_array($list['item_code'], $selected)) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $list['id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $list['id']; ?>" />
						<?php } ?></td>
						<td><?php echo $list['store_name'] ?></td>
						<td><?php echo $list['item_code']?></td>
						<td><?php echo $list['item_name']?></td>
						<td><?php echo $list['qty']?></td>
						<td><?php echo $list['unit']?></td>
						<td><?php echo $list['cost']?></td>
						<?php $total_cost = $total_cost + number_format($list['cost'],3); ?>
					</tr>
					<?php } ?>
					<tr style="text-align: right;">
						<td colspan="6">Total</td>
						<td style="text-align: left"><?php echo number_format($total_cost,3) ?></td>
					<tr>
				<?php } ?>
			</table>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		$(document).ready(function () {
    		$("#filter_subbomcategory").change(function () {
    			$("#subbom").submit();
    		});
		});

		$("#qty,#wastage").keyup(function () {
			var qty = parseFloat($('#qty').val());
			var wastage = parseFloat($('#wastage').val());
			var purchase_price = parseFloat($('#purchase_price').val());
			if(qty != '' && wastage != ''){
				$('#cost').val(((qty*wastage)/purchase_price).toFixed(3));
			}
			if(qty != '' && wastage == ''){
				$('#cost').val(((qty)/purchase_price).toFixed(3));
			}
    	});

    	function validate(){
    		if($('#store').val() == ''){
    			alert("Please Enter Store Name");
    			return false;
    		} else{
    			$("#subbom").submit();
    		}
    	}

    	$('#filter_subbomitem').change(function(){
    		var id = $('#filter_subbomitem').val();
    		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/subbom/getprice&token=<?php echo $token; ?>&filter_id='+id,
				dataType: 'json',
				data: {"data":"check"},
				success: function(data){
					$('#purchase_price').val(data[0].purchase_price);
					$('#filter_unit').find('option').remove();
					if(data){
						$.each(data, function (i, item) {
							$('#filter_unit').append($('<option>', { 
								value: item.unit_id,
								text : item.unit_name ,
							}));
						});
						$('#unit_name').val();
					}
				}
    		})
    	});

    	$('#filter_unit').on('change', function() {
			unit = $('#filter_unit option:selected').text();
		  $('#unit_name').val(unit);

		});

		// $('#store').on('change', function() {
  // 			if(this.value == ''){
  // 				alert("hi");
  // 				return false;
  // 			}
		// });
	</script>
</div>
<?php echo $footer; ?>