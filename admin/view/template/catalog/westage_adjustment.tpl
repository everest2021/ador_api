<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading" style="padding-bottom: 15px;">
				<h3><i class="fa fa-pencil"></i> <?php echo $text_form; ?>   </h3>
				<div class="pull-right">
					<?php if($edit == 1){ ?>
						<button type="button" onClick="submit_fun()" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
					<?php } else { ?>
						<button style="display:none" type="button" onClick="print()" data-toggle="tooltip" title="Print" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button>
						<button type="button" onClick="submit_fun()" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
					<?php } ?>
						<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
				</div>
				<div class="panel-body">
					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-invoice_no"><?php echo 'Slip Number'; ?></label>
							<div class="col-sm-3">
								<input tabindex="3" type="text" readonly="readonly" name="invoice_no" value="<?php echo $invoice_no; ?>" placeholder="<?php echo 'Slip Number'; ?>" id="input-invoice_no" class="form-control inputs" />
							</div>
							<label class="col-sm-2 control-label" for="input-filter_invoice_no"><?php echo 'Search Slip Number'; ?></label>
							<div class="col-sm-3">
								<input tabindex="2" type="text" name="filter_invoice_no" value="<?php echo $filter_invoice_no; ?>" placeholder="<?php echo 'Search Slip Number'; ?>" id="input-filter_invoice_no" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-store"><?php echo 'From Store'; ?></label>
							<div class="col-sm-3">
								<select tabindex="5" style="padding: 0px 1px;" name="from_store_id" id="input-from_store_id" class="form-control inputs">
									<option value="">Please Select</option>
									<?php foreach($stores as $ukey => $uvalue){ ?>
										<?php if($ukey == $from_store_id){ ?>
											<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
										<?php } else { ?>
											<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="from_store_name" value="<?php echo $from_store_name ?>" placeholder="<?php echo 'Store Name'; ?>" style="" id="input-from_store_name" class="form-control" />
							</div>
							<input type="hidden" id="input-category">
							<label style="display: none;" class="col-sm-2 control-label" for="input-store"><?php echo 'To Store'; ?></label>
							<div style="display: none;" class="col-sm-3">
								<select tabindex="5" style="padding: 0px 1px;" name="to_store_id" id="input-to_store_id" class="form-control inputs">
									<option value="">Please Select</option>
									<?php foreach($stores as $ukey => $uvalue){ ?>
										<?php if($ukey == $to_store_id){ ?>
											<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
										<?php } else { ?>
											<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="to_store_name" value="<?php echo $to_store_name ?>" placeholder="<?php echo 'Store Name'; ?>" style="" id="input-to_store_name" class="form-control" />
								<!-- <input type="hidden" name="store_id" value="<?php //echo $store_id ?>" id="input-store_id" class="form-control" style="" /> -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-date"><?php echo 'Date'; ?></label>
							<div class="col-sm-3 date">
								<input tabindex="4" readonly="readonly" type="text" name="invoice_date" value="<?php echo $invoice_date; ?>" placeholder="<?php echo 'Invoice Date'; ?>" id="input-invoice_date" class="form-control inputs" />
							</div>
							<label class="col-sm-2 control-label" for="input-narration"><?php echo 'Narration'; ?></label>
							<div class="col-sm-3">
								<input tabindex="9" type="text" name="narration" value="<?php echo $narration ?>" placeholder="<?php echo 'Narration'; ?>" id="input-narration" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group" style="padding: 5px;">
							<table style="width:100%;margin-left:0%;margin-top:18px;" class="table table-bordered table-hover ordertab" id="ordertab">
								<tr>
								  <td style="text-align: center;width: 20%;">Description</td>
								  <td style="text-align: center;width: 7%;">Qty</td>
								  <td style="text-align: center;width: 5%;">Available Qty</td>
								  <td style="text-align: center;width: 5%;">Unit</td>
								  <td style="text-align: center;width: 7%;">Rate</td>
								  <td style="text-align: center;width: 7%;">Amount</td>
								  <td style="text-align: center;width: 7%;">Reason</td>
								  <td style="width:3%;text-align: center;">Action</td>
								</tr>
								<?php $extra_field_row = 1; ?>
								<?php $tab_index = 11; ?>
								<?php if($po_datas) { ?>
									<?php foreach ($po_datas as $pkey => $po_data) { ?>
										<tr id="re_<?php echo $extra_field_row ?>">
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas[<?php echo $extra_field_row ?>][description_code_search]" value="<?php echo $po_data['description_code_search']; ?>" class="inputs code form-control" id="description-code-search_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 60%;display: inline;"  type="text" class="inputs description form-control" name="po_datas[<?php echo $extra_field_row ?>][description]" value="<?php echo $po_data['description']; ?>"  id="description_<?php echo $extra_field_row ?>" />
												
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_id]" value="<?php echo $po_data['description_id']; ?>" class="inputs" id="description-id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_code]" value="<?php echo $po_data['description_code']; ?>" class="inputs" id="description-code_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_barcode]" value="<?php echo $po_data['description_barcode']; ?>" class="inputs" id="description-barcode_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][id]" value="<?php echo $po_data['id']; ?>" class="inputs" id="id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][p_id]" value="<?php echo $po_data['p_id']; ?>" class="inputs" id="p_id_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][item_category]" value="<?php echo $po_data['item_category']; ?>" class="inputs" id="item-category_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][type_id]" value="<?php echo $po_data['type_id']; ?>" class="inputs" id="type_id_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas[<?php echo $extra_field_row ?>][qty]" value="<?php echo $po_data['qty']; ?>" id="qty_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_qty]" value="<?php echo $po_data['qty']; ?>" class="inputs" id="hidden-qty_<?php echo $extra_field_row ?>" />
												<?php if (isset($error_po_datas[$extra_field_row]['qty'])) { ?>
													<div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['qty']; ?></div>
												<?php } ?>
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" class="inputs avq form-control" name="po_datas[<?php echo $extra_field_row ?>][avq]" value="<?php echo $po_data['avq']; ?>" id="avq_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<!-- <input tabindex="<?php echo $tab_index; ?>" type="text" class="inputs units form-control" name="po_datas[<?php echo $extra_field_row ?>][unit]" value="<?php //echo $po_data['unit']; ?>"  id="unit_<?php echo $extra_field_row ?>" /> -->
												<!-- <input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" value="<?php //echo $po_data['unit_id']; ?>" class="inputs" id="unit-id_<?php echo $extra_field_row ?>" />			 -->
												<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" id="unit-id_<?php echo $extra_field_row; ?>" class="inputs units form-control">
													<option value="">Please Select</option>
													<?php foreach($po_data['units'] as $ukey => $uvalue){ ?>
														<?php if($ukey == $po_data['unit_id']){ ?>
															<option selected="selected" value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
														<?php } else { ?>
															<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
														<?php } ?>
													<?php } ?>
												</select>
												<input type="hidden" class="" name="po_datas[<?php echo $extra_field_row ?>][unit]" value="<?php echo $po_data['unit']; ?>"  id="unit_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][rate]" value="<?php echo $po_data['rate']; ?>" class="inputs rates form-control" id="rate_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_rate]" value="<?php echo $po_data['rate']; ?>" class="inputs" id="hidden-rate_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][amount]" value="<?php echo $po_data['amount']; ?>" class="inputs form-control" id="amount_<?php echo $extra_field_row ?>" />
												<?php $tab_index ++; ?>
											</td>
											<td style="padding-top: 2px;padding-bottom: 2px;">
												<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs reasons form-control"  name="po_datas[<?php echo $extra_field_row ?>][reason]" value="<?php echo $po_data['reason']; ?>" id="reason_<?php echo $extra_field_row ?>" />
												<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_reason]" value="<?php echo $po_data['reason']; ?>" class="inputs" id="hidden-reason_<?php echo $extra_field_row ?>" />
												<?php if (isset($error_po_datas[$extra_field_row]['reason'])) { ?>
													<div class="text-danger"><?php echo $error_po_datas[$extra_field_row]['reason']; ?></div>
												<?php } ?>
												<?php $tab_index ++; ?>
											</td>
											<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">
												<a tabindex="<?php echo $tab_index; ?>" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('<?php echo $extra_field_row ?>')" class="button inputs remove " id="remove_<?php echo $extra_field_row ?>" ><i class="fa fa-trash-o"></i></a>
												<?php $tab_index ++; ?>
											</td>
										</tr>
										<?php $extra_field_row++; ?>
									<?php } ?>
								<?php } else { ?>
									<tr id="re_<?php echo $extra_field_row ?>">
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas[<?php echo $extra_field_row ?>][description_code_search]" value="" class="inputs code form-control" id="description-code-search_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;width: 60%;display: inline;"  type="text" class="inputs description form-control" name="po_datas[<?php echo $extra_field_row ?>][description]" value=""  id="description_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_id]" value="" class="inputs" id="description-id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_code]" value="" class="inputs" id="description-code_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][description_barcode]" value="" class="inputs" id="description-barcode_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][id]" value="0" class="inputs" id="id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][p_id]" value="0" class="inputs" id="p_id_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][item_category]" value="" class="inputs" id="item-category_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][type_id]" value="" class="inputs" id="type_id_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas[<?php echo $extra_field_row ?>][qty]" value="" id="qty_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_qty]" value="0" class="inputs" id="hidden-qty_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" class="inputs avq form-control" name="po_datas[<?php echo $extra_field_row ?>][avq]" id="avq_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<!-- <input tabindex="<?php echo $tab_index; ?>" type="text" class="inputs form-control units" name="po_datas[<?php echo $extra_field_row ?>][unit]" value=""  id="unit_<?php echo $extra_field_row ?>" /> -->
											<!-- <input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" value="" class="inputs" id="unit-id_<?php echo $extra_field_row ?>" /> -->
											<select tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" name="po_datas[<?php echo $extra_field_row ?>][unit_id]" id="unit-id_<?php echo $extra_field_row; ?>" class="inputs form-control units">
												<option value="">Please Select</option>
												<?php foreach($units as $ukey => $uvalue){ ?>
													<option value="<?php echo $ukey ?>"><?php echo $uvalue ?></option>
												<?php } ?>
											</select>
											<input type="hidden" class="" name="po_datas[<?php echo $extra_field_row ?>][unit]" value=""  id="unit_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][rate]" value="" class="inputs rates form-control" id="rate_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_rate]" value="" class="inputs" id="hidden-rate_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas[<?php echo $extra_field_row ?>][amount]" value="" class="inputs form-control" id="amount_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="padding-top: 2px;padding-bottom: 2px;">
											<input tabindex="<?php echo $tab_index; ?>" style="padding: 0px 1px;" type="text" class="inputs reasons form-control"  name="po_datas[<?php echo $extra_field_row ?>][reason]" value="" id="reason_<?php echo $extra_field_row ?>" />
											<input type="hidden" name="po_datas[<?php echo $extra_field_row ?>][hidden_reason]" value="0" class="inputs" id="hidden-reason_<?php echo $extra_field_row ?>" />
											<?php $tab_index ++; ?>
										</td>
										<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">
											<a tabindex="<?php echo $tab_index; ?>" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('<?php echo $extra_field_row ?>')" class="button inputs remove " id="remove_<?php echo $extra_field_row ?>" ><i class="fa fa-trash-o"></i></a>
											<?php $tab_index ++; ?>
										</td>
									</tr>
								<?php } ?>
								<input type="hidden" name="ids" value="<?php echo $extra_field_row ?>" id="input-ids" />
							</table>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
								&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_qty"><?php echo 'Qty'; ?></label>
							<div class="col-sm-2">
								<input tabindex="50" readonly="readonly" type="text" name="total_qty" value="<?php echo $total_qty; ?>" placeholder="<?php echo 'Total Quantity'; ?>" id="input-total_qty" class="form-control inputs" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-3">
								&nbsp;
							</div>
							<label class="col-sm-3 control-label">&nbsp;</label>
							<label class="col-sm-2 control-label" for="total_pay"><?php echo 'Total Pay'; ?></label>
							<div class="col-sm-2">
								<input tabindex="55" readonly="readonly" type="text" name="total_pay" value="<?php echo $total_pay; ?>" placeholder="<?php echo 'Total Pay'; ?>" id="total_pay" class="form-control inputs" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript"><!--

$('#input-from_store_id').on('change', function() {
  store_name = $('#input-from_store_id option:selected').text();
  $('#input-from_store_name').val(store_name);
  store_id = $('#input-from_store_id option:selected').val();
});

$('#input-to_store_id').on('change', function() {
  store_name = $('#input-to_store_id option:selected').text();
  $('#input-to_store_name').val(store_name);
});

$(document).on('change', '.units', function(e) {
  idss = $(this).attr('id');
  s_id = idss.split('_');
  id = s_id[1];
  unit_name = $('#unit-id_'+id+' option:selected').text();
  $('#unit_'+id).val(unit_name);
  unit_id = $('#unit-id_'+id+' option:selected').val();
  description_id = $('#description-id_'+id).val();
  description_code = $('#description-code_'+id).val();
  $.ajax({
	url:'index.php?route=catalog/westage_adjustment/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code,
	type:'post',
	dataType: 'json',
	success:function(json){
		$('#avq_'+id).val(json.avq);
	}
  });
});

var tab_index = '<?php echo $tab_index; ?>';
function search_fun(){
	filter_invoice_no = $('#input-filter_invoice_no').val();

	if(filter_invoice_no == ''){
		alert('Please Enter Invoice Number');
		return false;
	}
	redirect_url = '<?php echo $redirect_url ?>';
	redirect_url = redirect_url.replace('&amp;', '&');
	redirect_url = redirect_url.replace('&amp;', '&');
	url = redirect_url+'&filter_invoice_no='+filter_invoice_no;
	location = url;
}

$('#input-filter_invoice_no').keydown(function(e) {
	if (e.keyCode == 13) {
		search_fun();
	}
});

$('.date').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
});

function submit_fun(){
  save = '<?php echo $action ?>';
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  save = save.replace('&amp;', '&');
  $('#form-manufacturer').attr('action', save);
  $('#form-manufacturer').submit();
}

$(document).on('keydown', '.inputs', function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		idss = $(this).attr('id');
		s_id = idss.split('_');
		ids = parseInt($('#input-ids').val());
		current_id = parseInt(s_id[1]);
		next_id = current_id + 1;
		var na = $(this).attr('class');
		var index = $('.inputs').index(this);
		if(na == 'inputs code form-control'){
			search_fun_code(s_id);
  		}
  		if(na == 'inputs qtys form-control'){
  			$('#description-code-search_'+next_id).focus();
  		}
		if(s_id[0] == 'remove') {
			// ids = parseInt($('#input-ids').val());
			// current_id = parseInt(s_id[1]);
			// next_id = current_id + 1;
			var na = $(this).attr('class');
			flag = 1;
			val1=$('#description_'+next_id).val();
			if($(this).closest("tr").is(":last-child")){
				flag = 1;
				i = ids + 1;
			} else {
				if(val1 == undefined) {
					flag = 1;
					i = ids + 1;
				} else {
					flag = 0;
				}
			}
			if(flag == 1) {
				html = '<tr id="re_'+i+'">';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="" class="inputs code form-control" id="description-code-search_'+i+'" />';
						tab_index ++;
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 60%;display: inline;"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value=""  id="description_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="" class="inputs" id="description-id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="" class="inputs" id="description-code_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="" class="inputs" id="description-barcode_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][id]" value="0" class="inputs" id="id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="0" class="inputs" id="p-id_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="" class="inputs" id="item-category_'+i+'" />';
						html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="" class="inputs" id="type_id_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="0" id="hidden-qty_'+i+'" />';
						tab_index ++
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="" id="avq_'+i+'" />';
						tab_index ++
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						//html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;"  type="text" class="inputs units form-control" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
						//html += '<input type="hidden" name="po_datas['+i+'][unit_id]" value="" class="inputs" id="unit-id_'+i+'" />';
						html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="inputs units form-control">';
							html += '<option value="">Please Select</option>';
						html += '</select>';
						html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="" class="inputs" id="hidden-rate_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="" class="inputs form-control" id="amount_'+i+'" />';
						tab_index ++;
					html += '</td>';
					html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
						html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs reasons form-control"  name="po_datas['+i+'][reason]" value="" id="reason_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_reason]" value="0" id="hidden-reason_'+i+'" />';
						tab_index ++
					html += '</td>';
					html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
						html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
						tab_index ++;
					html += '</td>';
				html += '</tr>';
				$('table').append(html);
				$('#input-ids').val(i);
				$('#description-code-search_'+i).focus();
				i++;
			} else {
				var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				if (!$next.length) {
					$next = $('[tabIndex=1]');
				}
				$next.focus();
				$next.select();
			}
		} else {
			if(s_id[0] == 'input-category'){
				category = $('#input-category').val();
				if(category == 'Food'){
					$('#input-batch_no').focus();
				} else if(category == 'Food'){
					$('#input-tp_no').focus();
				} else {
					var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
					if (!$next.length) {
						$next = $('[tabIndex=1]');
					}
					$next.focus();
					$next.select();	
				}
			} else {
				ids = parseInt($('#input-ids').val());
				current_id = parseInt(s_id[1]);
				next_id = current_id + 1;
				var na = $(this).attr('class');
				flag = 1;
				val2=$('#description-code-search_'+current_id).val();
				//console.log(na);
				if(na == 'inputs qtys form-control'){
					if(val2 != ''){
						val1=$('#description_'+next_id).val();
							if(val1 == undefined) {
								flag = 1;
								i = ids + 1;
							} else {
								flag = 0;
							}
					if(flag == 1) {
						html = '<tr id="re_'+i+'">';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="" class="inputs code form-control" id="description-code-search_'+i+'" />';
								tab_index ++;
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 60%;display: inline;"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value=""  id="description_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="" class="inputs" id="description-id_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="" class="inputs" id="description-code_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="" class="inputs" id="description-barcode_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][id]" value="0" class="inputs" id="id_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="0" class="inputs" id="p-id_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="" class="inputs" id="item-category_'+i+'" />';
								html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="" class="inputs" id="type_id_'+i+'" />';
								tab_index ++;
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="0" id="hidden-qty_'+i+'" />';
								tab_index ++
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="" id="avq_'+i+'" />';
								tab_index ++
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								//html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;"  type="text" class="inputs units form-control" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
								//html += '<input type="hidden" name="po_datas['+i+'][unit_id]" value="" class="inputs" id="unit-id_'+i+'" />';
								html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="inputs units form-control">';
									html += '<option value="">Please Select</option>';
								html += '</select>';
								html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value=""  id="unit_'+i+'" />';
								tab_index ++;
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="" class="inputs" id="hidden-rate_'+i+'" />';
								tab_index ++;
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="" class="inputs form-control" id="amount_'+i+'" />';
								tab_index ++;
							html += '</td>';
							html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
								html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs reasons form-control"  name="po_datas['+i+'][reason]" value="" id="reason_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_reason]" value="0" id="hidden-reason_'+i+'" />';
								tab_index ++
							html += '</td>';
							html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
								html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
								tab_index ++;
							html += '</td>';
						html += '</tr>';
						$('table').append(html);
						$('#input-ids').val(i);
						$('#description-code-search_'+i).focus();
						i++;
					}

					// if($('#qty_'+i ).val() < 0 ){
				 //  		alert("Qty Should not be Zero/Negative ");
				 //  		$('#qty_'+i ).val(1);
				 //  		return false;
				 //  	}
				}
			}
					
				/*var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
				if (!$next.length) {
					$next = $('[tabIndex=1]');
				}
				$next.focus();
				$next.select();*/
			}
		}			
	} else if (code == 37) {
		var $previous = $('[tabIndex=' + (+this.tabIndex - 1) + ']');
		if (!$previous.length) {
		   $previous = $('[tabIndex=1]');
		}
		previous_index = this.tabIndex - 1;
		$previous.focus();
		$previous.select();

	} else if (code == 39) {
		var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
  		if (!$next.length) {
  			$next = $('[tabIndex=1]');
  		}
  		$next.focus();
		$next.select();
	}

	if(code == 27){
		window.location.reload();
	}

	$('.description').autocomplete({
		delay: 500,
		source: function(request, response) {
			filter_category = $('#input-category').val();
			$.ajax({
				url: 'index.php?route=catalog/westage_adjustment/autocomplete_description&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_category='+filter_category,
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						return {
							label: item.item_name,
							value: item.id,
							rate: item.rate,
							item_code: item.item_code,
							bar_code: item.bar_code,
							tax: item.tax,
							tax_value: item.tax_value,
							uom: item.uom,
							units: item.units,
							rate: item.rate,
							item_category: item.item_category,
							unit_name: item.unit_name,
							stock_type:item.stock_type
						}
					}));
				}
			});
		}, 
		select: function(event, ui) {
			idss = $(this).attr('id');
			s_id = idss.split('_');
			$('#description_'+s_id[1]).val(ui.item.label);
			$('#description-code-search_'+s_id[1]).val(ui.item.item_code);
			$('#description-id_'+s_id[1]).val(ui.item.value);
			$('#description-code_'+s_id[1]).val(ui.item.item_code);
			$('#description-barcode_'+s_id[1]).val(ui.item.bar_code);
			$('#tax-name_'+s_id[1]).val(ui.item.tax);
			$('#tax-percent_'+s_id[1]).val(ui.item.tax_value);
			$('#item-category_'+s_id[1]).val(ui.item.item_category);
			
			$('#unit-id_'+s_id[1]).find('option').remove();
			if(ui.item.units){
				$.each(ui.item.units, function (i, item) {
				  $('#unit-id_'+s_id[1]).append($('<option>', { 
					  value: item.unit_id,
					  text : item.unit_name 
				  }));
				});
			}
			if(ui.item.item_category == 'Food'){
				$('#unit_'+s_id[1]).val(ui.item.unit_name);
				$('#unit-id_'+s_id[1]).val(ui.item.uom);
				$('#unit_'+s_id[1]).attr("readonly", "readonly");
			} else {
				$('#unit_'+s_id[1]).val('');
				$('#unit-id_'+s_id[1]).val('');
				$('#unit_'+s_id[1]).removeAttr("readonly");
			}
			if($('#unit-id_'+s_id[1]).val() != ''){
				unit_id = $('#unit-id_'+s_id[1]).val();
				description_id = $('#description-id_'+s_id[1]).val();
				description_code = $('#description-code_'+s_id[1]).val();
				store_id = $('#input-from_store_id option:selected').val();
				var s_id = s_id[1];
				$.ajax({
					url:'index.php?route=catalog/westage_adjustment/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code+'&store_id='+store_id+'&stock_type='+ui.item.stock_type,
					type:'post',
					dataType: 'json',
					success:function(json){
						$('#avq_'+s_id).val(json.avq);
					}
			  });
			}

			
			$('#rate_'+s_id).val(ui.item.rate);
			$('#hidden-rate_'+s_id).val(ui.item.rate);
			$('#qty_'+s_id).focus();
			gettotal(s_id);
			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});


	

	$('.date').datetimepicker({
	  pickTime: false,
	  format: 'DD-MM-YYYY'
	});
});

$(document).on('keyup', '.qtys, .rates', function(e) {
	idss = $(this).attr('id');
	s_id = idss.split('_');
	if($('#rate_'+s_id[1] ).val() < 0){
  		alert("Rate Should not be Zero/Negative ");
  		$('#rate_'+s_id[1] ).val(1);
  	}
  	if($('#qty_'+s_id[1] ).val() < 0){
  		alert("Qty Should not be Zero/Negative ");
  		$('#qty_'+s_id[1] ).val(1);
  	}
	gettotal(s_id[1]); 	
	return false;
});

$( document ).ready(function() {
	$('#remove_1').attr('onclick','');
	$('#remove_1').text('-');
	return false;
});

function gettotal(id) {
	qty = parseFloat($('#qty_'+id).val());
	if(qty == '' || qty == '0' || isNaN(qty)){
		qty = 0;
	}
	rate = parseFloat($('#rate_'+id).val());
	if(rate == '' || rate == '0' || isNaN(rate)){
		rate = 0;
	}
	single_amt = qty * rate;
	amount = single_amt;
	$('#amount_'+id).val(amount);
	
	total_qty = 0;
	total_amt = 0;
	total_pay = 0;
	$('.ordertab').find('tr').each (function() {
		idss = $(this).attr('id');
		if(idss != undefined){
			idss = $(this).attr('id');
			s_ids = idss.split('_');
			s_id = s_ids[1];
			qty = parseFloat($('#qty_'+s_id).val());
			if(qty == '' || qty == '0' || isNaN(qty)){
				qty = 0;
			}
			total_qty = total_qty + qty;
			rate = parseFloat($('#rate_'+s_id).val());
			if(rate == '' || rate == '0' || isNaN(rate)){
				rate = 0;
			}
			single_amt = qty * rate;
			total_amt = total_amt + single_amt;
		}
	});
	total_pay = total_amt;
	$('#input-total_qty').val(total_qty);
	$('#total_pay').val(total_pay);
	return false;	
}

function remove_folder(c){
	$('#re_'+c).remove();
	var units = '<?php echo $units1 ?>';
	units = JSON.parse(units);
	var final_datas = [];
	var j = 1;
	$('.ordertab').find('tr').each (function() {
		idss = $(this).attr('id');
		if(idss != undefined){
			final_datas[j] = [];
			s_id = idss.split('_');
			id = s_id[1];
			final_datas[j]['description'] = $('#description_'+id).val();	
			final_datas[j]['description_id'] = $('#description-id_'+id).val();	
			final_datas[j]['description_code'] = $('#description-code_'+id).val();	
			final_datas[j]['description_code_search'] = $('#description-code-search_'+id).val();	
			final_datas[j]['description_barcode'] = $('#description-barcode_'+id).val();	
			final_datas[j]['item_category'] = $('#item-category_'+id).val();	
			final_datas[j]['qty'] = $('#qty_'+id).val();	
			final_datas[j]['hidden_qty'] = $('#hidden-qty_'+id).val();	
			final_datas[j]['reason'] = $('#reason_'+id).val();	
			final_datas[j]['hidden_reason'] = $('#hidden-reason_'+id).val();
			final_datas[j]['units'] = units;	
			final_datas[j]['unit'] = $('#unit_'+id).val();	
			final_datas[j]['unit_id'] = $('#unit-id_'+id).val();	
			final_datas[j]['rate'] = $('#rate_'+id).val();	
			final_datas[j]['hidden_rate'] = $('#hidden-rate_'+id).val();	
			final_datas[j]['amount'] = $('#amount_'+id).val();	
			final_datas[j]['type_id'] = $('#type_id_'+id).val();
			final_datas[j]['avq'] = $('#avq_'+id).val();
			$('#re_'+id).remove();
			j ++;
		}
	});
	tab_index = 9;
	i = 1;
	last_key = j - 1;
	for (var arrayindex in final_datas) {
		html = '<tr id="re_'+i+'">';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width: 35%;display: inline;" type="text" name="po_datas['+i+'][description_code_search]" value="'+final_datas[arrayindex]['description_code_search']+'" class="inputs code form-control" id="description-code-search_'+i+'" />';
				tab_index ++;
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;width:60%;display:inline"  type="text" class="inputs description form-control" name="po_datas['+i+'][description]" value="'+final_datas[arrayindex]['description']+'" id="description_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_id]" value="'+final_datas[arrayindex]['description_id']+'" class="inputs" id="description-id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_code]" value="'+final_datas[arrayindex]['description_code']+'" class="inputs" id="description-code_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][description_barcode]" value="'+final_datas[arrayindex]['description_barcode']+'" class="inputs" id="description-barcode_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][id]" value="'+final_datas[arrayindex]['id']+'" class="inputs" id="id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][p_id]" value="'+final_datas[arrayindex]['p_id']+'" class="inputs" id="p-id_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][item_category]" value="'+final_datas[arrayindex]['item_category']+'" class="inputs" id="item-category_'+i+'" />';
				html += '<input type="hidden" name="po_datas['+i+'][type_id]" value="'+final_datas[arrayindex]['type_id']+'" class="inputs" id="type_id_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs qtys form-control"  name="po_datas['+i+'][qty]" value="'+final_datas[arrayindex]['qty']+'" id="qty_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_qty]" value="'+final_datas[arrayindex]['hidden_qty']+'" id="hidden-qty_'+i+'" />';
				tab_index ++
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" readonly="readonly" class="inputs avq form-control"  name="po_datas['+i+'][avq]" value="'+final_datas[arrayindex]['avq']+'" id="avq_'+i+'" />';
				tab_index ++
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				//html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;"  type="text" class="inputs units form-control" name="po_datas['+i+'][unit]" value="'+final_datas[arrayindex]['unit']+'"  id="unit_'+i+'" /><input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				//html += '<input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				html += '<select tabindex="'+tab_index+'" style="padding: 0px 1px;" name="po_datas['+i+'][unit_id]" id="unit-id_'+i+'" class="form-control">';
					html += '<option value="">Please Select</option>';
					for (var food in units) {
						var unit_id = units[food].unit_id;
						var unit_name = units[food].unit;
						if(unit_id == final_datas[arrayindex]['unit_id']){
							html += '<option selected="selected" value="'+unit_id+'">'+unit_name+'</option>';
						} else{
							html += '<option value="'+unit_id+'">'+unit_name+'</option>';
						}
					}
				html += '</select>';
				html += '<input type="hidden" class="" name="po_datas['+i+'][unit]" value="'+final_datas[arrayindex]['unit']+'"  id="unit_'+i+'" /><input type="hidden" name="po_datas['+i+'][unit_id]" value="'+final_datas[arrayindex]['unit_id']+'" class="inputs" id="unit-id_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][rate]" value="'+final_datas[arrayindex]['rate']+'" class="inputs rates form-control" id="rate_'+i+'" /><input type="hidden" name="po_datas['+i+'][hidden_rate]" value="'+final_datas[arrayindex]['hidden_rate']+'" class="inputs" id="hidden-rate_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" readonly="readonly" style="padding: 0px 1px;" type="text" name="po_datas['+i+'][amount]" value="'+final_datas[arrayindex]['amount']+'" class="inputs form-control" id="amount_'+i+'" />';
				tab_index ++;
			html += '</td>';
			html += '<td style="padding-top: 2px;padding-bottom: 2px;">';
				html += '<input tabindex="'+tab_index+'" style="padding: 0px 1px;" type="text" class="inputs reasons form-control"  name="po_datas['+i+'][reason]" value="'+final_datas[arrayindex]['reason']+'" id="reason_'+i+'" /><input type="hidden" class="inputs"  name="po_datas['+i+'][hidden_reason]" value="'+final_datas[arrayindex]['hidden_reason']+'" id="hidden-reason_'+i+'" />';
				tab_index ++
			html += '</td>';
			html += '<td style="text-align: left;padding-top: 2px;padding-bottom: 2px;">';
				html += '<a tabindex="'+tab_index+'" style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove" id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a>';
				tab_index ++;
			html += '</td>';
		html += '</tr>';
		$('table').append(html);
		i ++;	
	}
	prev_i = i - 1;
	$('#input-ids').val(prev_i);
	$('#remove_1').attr('onclick','');
	$('#remove_1').text('-');
	gettotal(prev_i);
	$('#description-code-search_'+prev_i).focus();
}

	// $('.code').keydown(function(e) {
	// 	if (e.keyCode == 13) {
	// 		idss = $(this).attr('id');
	// 		s_id = idss.split('_');
	// 		search_fun_code(s_id);
	// 		return false;
	// 	}
	// });

	function search_fun_code(s_id){
		filter_name_1 = $('#description-code-search_'+s_id[1]).val();
		filter_category = $('#input-category').val();
		s_ids = s_id[1];
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/westage_adjustment/autocomplete_description&token=<?php echo $token; ?>&filter_name_1=' +filter_name_1+'&filter_category='+filter_category,
			dataType: 'json',
			data: {"data":"check"},
			success: function(data){
				//alert(data.name);
				if(data.status == 1){
					$('#description_'+s_ids).val(data.item_name);
					$('#description-id_'+s_ids).val(data.id);
					$('#description-code_'+s_ids).val(data.item_code);
					$('#description-barcode_'+s_ids).val(data.bar_code);
					$('#description-code-search_'+s_ids).val(data.item_code);
					$('#tax-name_'+s_ids).val(data.tax);
					$('#tax-percent_'+s_ids).val(data.tax_value);
					$('#item-category_'+s_ids).val(data.item_category);
					
					$('#unit-id_'+s_ids).find('option').remove();
					if(data.units){
						$.each(data.units, function (i, item) {
						  $('#unit-id_'+s_ids).append($('<option>', { 
							  value: item.unit_id,
							  text : item.unit_name 
						  }));
						});
					}
					if(data.item_category == 'Food'){
						$('#unit_'+s_ids).val(data.uom);
						$('#unit-id_'+s_ids).val(data.uom);
						$('#unit_'+s_ids).attr("readonly", "readonly");
					} else {
						$('#unit_'+s_ids).val('');
						$('#unit-id_'+s_ids).val('');
						$('#unit_'+s_ids).removeAttr("readonly");
					}

					if($('#unit-id_'+s_ids).val() != '' && $('#description-id_'+s_ids).val() != '' && $('#description-code_'+s_ids).val() != ''){
						unit_id = $('#unit-id_'+s_ids).val();
						description_id = $('#description-id_'+s_ids).val();
						description_code = $('#description-code_'+s_ids).val();
						store_id = $('#input-from_store_id option:selected').val();
						var s_id = s_ids;
						$.ajax({
							url:'index.php?route=catalog/westage_adjustment/getAvailableQuantity&token=<?php echo $token; ?>&unit_id='+unit_id+'&description_id='+description_id+'&description_code='+description_code+'&store_id='+store_id+'&stock_type='+data.stock_type,
							type:'post',
							dataType: 'json',
							success:function(json){
								$('#avq_'+s_id).val(json.avq);
							}
					  });
					}

					$('#rate_'+s_ids).val(data.rate);
					$('#hidden-rate_'+s_ids).val(data.rate);
					$('#qty_'+s_ids).focus();
					gettotal(s_ids);
					return false;					
				} else {
					//alert('Item Code/Barcode Does Not Exist');
					$('#description_'+s_ids).val('');
					$('#description-id_'+s_ids).val('');
					$('#description-code_'+s_ids).val('');
					$('#description-barcode_'+s_ids).val('');
					$('#description-code-search_'+s_ids).val('');
					$('#tax-name_'+s_ids).val('');
					$('#tax-percent_'+s_ids).val('');
					$('#item-category_'+s_ids).val('');
					$('#unit_'+s_ids).val('');
					$('#unit-id_'+s_ids).val('');
					$('#unit_'+s_ids).removeAttr("readonly");
					$('#rate_'+s_ids).val('');
					$('#hidden-rate_'+s_ids).val('');
					//$('#qty_'+s_ids).focus();
					gettotal(s_ids);
					return false;
				}
			}
		});		
	}
//--></script>
<?php echo $footer; ?>