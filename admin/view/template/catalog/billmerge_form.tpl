<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right sav">
				<button type="submit" form="form-bill_merge" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo 'Add Bill'; ?></h1>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="alert alert-danger" style="display: none;">
			<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-bill_merge" class="form-horizontal">
			  		<div class="form-group">
						<label class="col-sm-3 control-label" style="width:12%"><?php echo 'Master Ref No.'; ?></label>
						<div class="col-sm-3">
							<input type="text" name="order_no" id="order_no" value="<?php echo $order_no ?>" class="form-control" />
							<div class="" style="display: none;" id="order_no_error"></div>
						</div>
						<div class="col-sm-2">
							<span id="food_total_span" style="font-weight: bold;"><?php echo $food_total ?></span>
							<input type="hidden" name="food_total" id="food_total" value="<?php echo $food_total ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="liquor_total_span" style="font-weight: bold;"><?php echo $liquor_total ?></span>
							<input type="hidden" name="liquor_total" id="liquor_total" value="<?php echo $liquor_total ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="grand_total_span" style="font-weight: bold;"><?php echo $grand_total ?></span>	
							<input type="hidden" name="grand_total" id="grand_total" value="<?php echo $grand_total ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'Ref No 1'; ?></label>
						<div class="col-sm-3">
							<input tabindex="1" type="text" name="order_no_1" id="order_no_1" value="<?php echo $order_no_1 ?>" class="form-control" />
							<div class="" style="display: none;" id="order_no_1_error"></div>
						</div>
						<div class="col-sm-2" style="">
							<span id="food_total_1_span" style="font-weight: bold;"><?php echo $food_total_1 ?></span>	
							<input type="hidden" name="food_total_1" id="food_total_1" value="<?php echo $food_total_1 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="liquor_total_1_span" style="font-weight: bold;"><?php echo $liquor_total_1 ?></span>
							<input type="hidden" name="liquor_total_1" id="liquor_total_1" value="<?php echo $liquor_total_1 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="grand_total_1_span" style="font-weight: bold;"><?php echo $grand_total_1 ?></span>
							<input type="hidden" name="grand_total_1" id="grand_total_1" value="<?php echo $grand_total_1 ?>" class="form-control" />
						</div>
			  		</div>
			  		<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'Ref No 2'; ?></label>
						<div class="col-sm-3">
							<input tabindex="2" type="text" name="order_no_2" id="order_no_2" value="<?php echo $order_no_2 ?>" class="form-control" />
							<div class="" style="display: none;" id="order_no_2_error"></div>
						</div>
						<div class="col-sm-2" style="">
							<span id="food_total_2_span" style="font-weight: bold;"><?php echo $food_total_2 ?></span>	
							<input type="hidden" name="food_total_2" id="food_total_2" value="<?php echo $food_total_2 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="liquor_total_2_span" style="font-weight: bold;"><?php echo $liquor_total_2 ?></span>
							<input type="hidden" name="liquor_total_2" id="liquor_total_2" value="<?php echo $liquor_total_2 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="grand_total_2_span" style="font-weight: bold;"><?php echo $grand_total_2 ?></span>
							<input type="hidden" name="grand_total_2" id="grand_total_2" value="<?php echo $grand_total_2 ?>" class="form-control" />
						</div>
			  		</div>
			  		<div class="form-group">
						<label class="col-sm-3 control-label" style=""><?php echo 'Ref No 3'; ?></label>
						<div class="col-sm-3">
							<input tabindex="3" type="text" name="order_no_3" id="order_no_3" value="<?php echo $order_no_3 ?>" class="form-control" />
							<div class="" style="display: none;" id="order_no_3_error"></div>
						</div>
						<div class="col-sm-2" style="">
							<span id="food_total_3_span" style="font-weight: bold;"><?php echo $food_total_3 ?></span>	
							<input type="hidden" name="food_total_3" id="food_total_3" value="<?php echo $food_total_3 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="liquor_total_3_span" style="font-weight: bold;"><?php echo $liquor_total_3 ?></span>
							<input type="hidden" name="liquor_total_3" id="liquor_total_3" value="<?php echo $liquor_total_3 ?>" class="form-control" />
							&nbsp;&nbsp;
							<span id="grand_total_3_span" style="font-weight: bold;"><?php echo $grand_total_3 ?></span>
							<input type="hidden" name="grand_total_3" id="grand_total_3" value="<?php echo $grand_total_3 ?>" class="form-control" />
						</div>
			  		</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
$('#order_no').on('focusout', function() {
  	order_no = $('#order_no').val();
  	console.log(order_no);
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/billmerge/getorderinfo&token=<?php echo $token; ?>&order_no='+order_no,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_error').hide();
					$('#order_no_error').removeClass('text-danger');
					$('#food_total_span').html('Food Total : ' + data.ftotal);
					$('#liquor_total_span').html('Liquor Total : ' + data.ltotal);
					$('#grand_total_span').html('Grand Total : ' + data.grand_total);
					$('#food_total').val(data.ftotal);
					$('#liquor_total').val(data.ltotal);
					$('#grand_total').val(data.grand_total);
				} else {
					$('#order_no_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_error').html('Ref Number Does not Exist');
					}
					$('#order_no_error').show();
					$('#food_total_span').html('');
					$('#liquor_total_span').html('');
					$('#grand_total_span').html('');
					$('#food_total').val('');
					$('#liquor_total').val('');
					$('#grand_total').val('');
				}
			}
		});
	} else {
		$('#order_no_error').removeClass('text-danger');
		$('#order_no_error').html('');
		$('#order_no_error').hide();
		$('#food_total_span').html('');
		$('#liquor_total_span').html('');
		$('#grand_total_span').html('');
		$('#food_total').val('');
		$('#liquor_total').val('');
		$('#grand_total').val('');	
	}
});

$('#order_no_1').on('focusout', function() {
  	order_no = $('#order_no_1').val();
  	master_order_no = $('#order_no').val();
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/billmerge/getorderinfo&token=<?php echo $token; ?>&order_no='+order_no+'&master_order_no='+master_order_no,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_1_error').hide();
					$('#order_no_1_error').removeClass('text-danger');
					$('#food_total_1_span').html('Food Total : ' + data.ftotal);
					$('#liquor_total_1_span').html('Liquor Total : ' + data.ltotal);
					$('#grand_total_1_span').html('Grand Total : ' + data.grand_total);
					$('#food_total_1').val(data.ftotal);
					$('#liquor_total_1').val(data.ltotal);
					$('#grand_total_1').val(data.grand_total);
				} else {
					$('#order_no_1_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_1_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_1_error').html('Ref Number Does not Exist');
					}
					$('#order_no_1_error').show();
					$('#food_total_1_span').html('');
					$('#liquor_total_1_span').html('');
					$('#grand_total_1_span').html('');
					$('#food_total_1').val('');
					$('#liquor_total_1').val('');
					$('#grand_total_1').val('');
				}
			}
		});
	} else {
		$('#order_no_1_error').removeClass('text-danger');
		$('#order_no_1_error').html('');
		$('#order_no_1_error').hide();
		$('#food_total_1_span').html('');
		$('#liquor_total_1_span').html('');
		$('#grand_total_1_span').html('');
		$('#food_total_1').val('');
		$('#liquor_total_1').val('');
		$('#grand_total_1').val('');	
	}
});

$('#order_no_2').on('focusout', function() {
  	order_no = $('#order_no_2').val();
  	master_order_no = $('#order_no').val();
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/billmerge/getorderinfo&token=<?php echo $token; ?>&order_no='+order_no+'&master_order_no='+master_order_no,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_2_error').hide();
					$('#order_no_2_error').removeClass('text-danger');
					$('#food_total_2_span').html('Food Total : ' + data.ftotal);
					$('#liquor_total_2_span').html('Liquor Total : ' + data.ltotal);
					$('#grand_total_2_span').html('Grand Total : ' + data.grand_total);
					$('#food_total_2').val(data.ftotal);
					$('#liquor_total_2').val(data.ltotal);
					$('#grand_total_2').val(data.grand_total);
				} else {
					$('#order_no_2_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_2_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_2_error').html('Ref Number Does not Exist');
					}
					$('#order_no_2_error').show();
					$('#food_total_2_span').html('');
					$('#liquor_total_2_span').html('');
					$('#grand_total_2_span').html('');
					$('#food_total_2').val('');
					$('#liquor_total_2').val('');
					$('#grand_total_2').val('');
				}
			}
		});
	} else {
		$('#order_no_2_error').removeClass('text-danger');
		$('#order_no_2_error').html('');
		$('#order_no_2_error').hide();
		$('#food_total_2_span').html('');
		$('#liquor_total_2_span').html('');
		$('#grand_total_2_span').html('');
		$('#food_total_2').val('');
		$('#liquor_total_2').val('');
		$('#grand_total_2').val('');	
	}
});

$('#order_no_3').on('focusout', function() {
  	order_no = $('#order_no_3').val();
  	master_order_no = $('#order_no').val();
  	if(order_no != '' && order_no != '0'){
	  	$.ajax({
			url: 'index.php?route=catalog/billmerge/getorderinfo&token=<?php echo $token; ?>&order_no='+order_no+'&master_order_no='+master_order_no,
			dataType: 'json',
			success: function(data){
				console.log(data);
				if(data.status == 1){
					$('#order_no_3_error').hide();
					$('#order_no_3_error').removeClass('text-danger');
					$('#food_total_3_span').html('Food Total : ' + data.ftotal);
					$('#liquor_total_3_span').html('Liquor Total : ' + data.ltotal);
					$('#grand_total_3_span').html('Grand Total : ' + data.grand_total);
					$('#food_total_3').val(data.ftotal);
					$('#liquor_total_3').val(data.ltotal);
					$('#grand_total_3').val(data.grand_total);
				} else {
					$('#order_no_3_error').addClass('text-danger');
					if(data.status == 2){
						$('#order_no_3_error').html('Ref Number Already Merged with Another Bill');
					} else {
						$('#order_no_3_error').html('Ref Number Does not Exist');
					}
					$('#order_no_3_error').show();
					$('#food_total_3_span').html('');
					$('#liquor_total_3_span').html('');
					$('#grand_total_3_span').html('');
					$('#food_total_3').val('');
					$('#liquor_total_3').val('');
					$('#grand_total_3').val('');
				}
			}
		});
	} else {
		$('#order_no_3_error').removeClass('text-danger');
		$('#order_no_3_error').html('');
		$('#order_no_3_error').hide();
		$('#food_total_3_span').html('');
		$('#liquor_total_3_span').html('');
		$('#grand_total_3_span').html('');
		$('#food_total_3').val('');
		$('#liquor_total_3').val('');
		$('#grand_total_3').val('');	
	}
});

$(document).on('keypress','input,select', function (e) {
//$('input,select').on('keypress', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
        if (!$next.length) {
            $next = $('[tabIndex=1]');
        }
        $next.focus();
    }
});

$(document).on('keyup','#save_button', function (e) {
    if (e.keyCode == 33) {
        save();
    }
});

$(document).ready(function() {
  	//order_no = $('#order_no').val();
  	//if(order_no != '' && order_no != '0' && order_no != undefined){
  		$('.sav').html('');
		html = '<a tabindex="4" id="save_button" onclick="save()" title="Update" class="btn btn-primary">Update</a>';
		$('.sav').append(html);
  	//}
  	$('#order_no').focus();
});

function save(){
  	action = '<?php echo $action; ?>';
  	action = action.replace("&amp;", "&");
  	$.post(action, $('#form-bill_merge').serialize()).done(function(data) {
    	var parsed = JSON.parse(data);
     	if(parsed.info == 1){
     		$('.sucess').html('');
     		$('.sucess').append(parsed.done);
     	}
	});
}
</script>