<?php echo $header; ?>
<div id="content" class="sucess">
	<div class="page-header">
		<form action="<?php echo $action; ?>" id="payform" method="post">
			<div class="container-fluid">
				<h3><?php echo 'Bill Settlement' ; ?></h3>
				
				<a id="payform" onclick="save()" title="Update" class="btn btn-info" style="margin-left:130px; font-size:25px; width:200px; height: 50px; ">Update</a><br><br>
				<a id="cashbtn"  title="Update" class="btn btn-info" style="font-size:25px; width:160px; height: 50px; ">Cash</a>
				<a id="cardbtn"  title="Update" class="btn btn-info" style="font-size:25px; width:160px; height: 50px;">Card</a>
				<a id="onlinebtn" title="Update" class="btn btn-info" style="font-size:25px; width:160px; height: 50px; ;">Online</a>
				
				<div class="pull-right sav">
					<table class="table table-bordered">
						<tr>
							<th>Date</th>
							<th>Amount</th>
							<th>Bal</th>
							<th>Ref No</th>
							<th>Table No.</th>
						</tr>
						<tr>
							<td><?php echo $billdata['bill_date'] ?></td>
							<td><?php echo round($billdata['grand_total']) ?></td>
							<td><span id="bal">0</span></td>
							<td><input type="hidden" name="order_id" value="<?php echo $billdata['order_id'] ?>"><?php echo $billdata['order_no'] ?></td>
							<input type="hidden" name="grand_total" id = "grandtotal" value="<?php echo round($billdata['grand_total']) ?>">
							<td><?php echo $billdata['t_name'] ?></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="container-fluid">
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px; font-size: 13px; width:15%">Grand Total:<?php echo round($billdata['grand_total']) ?> </label>
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width:15%">Cash</label>
				<?php if($msr_nos == '') {?>
					<input autocomplete="off" type="text" class="col-sm-2 form-control amt" id="cash" value="<?php echo round($billdata['grand_total']) ?>" style="display: inline;width: 10%" >
					<input type="hidden" name="cash" value="<?php echo round($billdata['grand_total']) ?>" id="cashval">
					<input type="hidden" id="default" value="<?php echo round($billdata['grand_total']) ?>">
				<?php } else { ?>
					<input autocomplete="off" type="hidden" class="col-sm-2 form-control amt" id="cash" value="0" style="display: inline;width: 10%">
					<input type="hidden" name="cash" value="0" id="cashval">
					<input type="hidden" id="default" value="0">
				<?php } ?>
				<?php if($orderidmodify != '') { ?>
					<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 10%">Remark</label>
					<input autocomplete="off" type="text" name='remark' class="col-sm-1 form-control" id="remark" style="display: inline;width: 20%">
				<?php } ?>
				<input type="hidden" name="orderid" id="orderid">
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Credit Card</label>
				<input autocomplete="off" type="text" class="col-sm-2 form-control amt" value="0" id="credit" style="display: inline;width: 10%">
				<input type="hidden" name="credit" id="creditval">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 10%">Card No.</label>
				<input autocomplete="off" type="text" class="col-sm-1 form-control amt" name="cardno" id="cardno" style="display: inline;width: 20%">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Customer Name</label>
				<input autocomplete="off" type="text" class="col-sm-1 form-control amt" id="custname" name="creditcustname" style="display: inline;width: 15%">
			  </div>
			  <div class="form-group row">
				<!-- <label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Online</label>
				<input autocomplete="off" type="text" class="col-sm-2 form-control amt" value="0" id="online"  name="online" style="display: inline;width: 10%">
				<input type="hidden" name="online" id="creditval"> -->
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width:15%">Online</label>
				<input autocomplete="off" type="text" value="0" name="online" class="col-sm-2 form-control amt" id="online" style="display: inline;width: 10%">

				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width:15%">Payment Type</label>

				<select  name="payment_type" style="width:15%" class="form-control">

					<option value="<?php echo '0' ?>" select="select"><?php echo 'Please select'?></option>

					<?php foreach ($online_type as $key => $value) {?>

						<option value="<?php echo $value['payment_type'] ?>"><?php echo $value['payment_type']?></option>

					<?}?>

				</select>


			</div>

			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width:15%">Meal Pass</label>
				<input autocomplete="off" type="text" value="0" name="mealpass" class="col-sm-2 form-control amt" id="mealpass" style="display: inline;width: 10%">
				<label class="col-sm-1 col-form-label" style="margin-top: 5px;width:5%">Tip</label>
				<input autocomplete="off" type="text" value="0" name="tip" class="col-sm-2 form-control" style="display: inline;width: 10%">
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width:15%">Pass</label>
				<input autocomplete="off" type="text" value="0" name="pass" class="col-sm-2 form-control amt" id="pass" style="display: inline;width: 10%">
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Room Service</label>
				<input autocomplete="off" type="text" value="0" readonly="readonly" class="col-sm-2 form-control" id="roomservice" name="roomservice" style="display: inline;width: 10%">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 10%">Room No.</label>
				<input autocomplete="off" type="text" name="roomno" id="roomno" class="col-sm-1 form-control" style="display: inline;width: 20%">
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">MSR Card</label>
				<?php if($msr_nos != '') {?>
					<input autocomplete="off" type="text" id="msrcard" value="<?php echo round($billdata['grand_total']) ?>"  class="col-sm-2 form-control" name="msrcard" style="display: inline;width: 10%">
				<?php }else { ?>
					<input autocomplete="off" type="text" id="msrcard" value="0" class="col-sm-2 form-control" name="msrcard" style="display: inline;width: 10%">
				<?php } ?>
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 10%">Card No.</label>
				<?php // echo'<pre>';print_r($msr_data);exit;?>
				<?php if($msr_data) {?>

					<input autocomplete="off" type="text" name="msrcardno" id = "msrcardno" class="col-sm-1 form-control" value="<?php echo $msr_data['msr_no']; ?>" style="display: inline;width: 20%">
					<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Customer Name</label>
					<input autocomplete="off" type="text" name="msrcustname" id= "msrcustname" class="col-sm-1 form-control" value="<?php echo $msr_data['name']; ?>" style="display: inline;width: 15%">
					<input autocomplete="off" type="text" name="msr_cust_id" id= "msr_cust_id"  value="<?php echo $msr_data['c_id'];?>" class="col-sm-1 form-control" style="display:none;width: 15%">
					<input autocomplete="off" type="text" id="mrs_amt" readonly="readonly" name="mrs_amt" value="<?php echo $msr_bals?>" class="col-sm-2 form-control amt" style="display: inline;width: 10%;margin-right: 10px;">
				<?php }else { ?>
					<input autocomplete="off" type="text" name="msrcardno" id = "msrcardno" class="col-sm-1 form-control" value="" style="display: inline;width: 20%">
					<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Customer Name</label>
					<input autocomplete="off" type="text" name="msrcustname" id= "msrcustname" class="col-sm-1 form-control" value="" style="display: inline;width: 15%">
					<input autocomplete="off" type="text" name="msr_cust_id" id= "msr_cust_id"  value="" class="col-sm-1 form-control" style="display:none;width: 15%">
					<input autocomplete="off" type="text" id="mrs_amt" readonly="readonly" name="mrs_amt" value="" class="col-sm-2 form-control amt" style="display: inline;width: 10%;margin-right: 10px;">

				<?php } ?>
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">On Account</label>
				<input autocomplete="off" type="text" class="col-sm-1 form-control" id="onaccust" value="<?php echo ($onaccount != 0 ? $billdata['cust_name'] : '') ?>" style="display: inline;width: 20%">
				<input autocomplete="off" type="text" id="onac" readonly="readonly" name="onac" value="<?php echo $onaccount?>" class="col-sm-2 form-control amt" style="display: inline;width: 10%;margin-right: 10px;">
				<input type="hidden" name="c_id" value="<?php echo $billdata['cust_id'] ?>" id="c_id">
				<input type="hidden" name="onaccontact" value="<?php echo $billdata['cust_contact'] ?>" id="onaccontact">
				<input type="hidden" name="onacname" value="<?php echo $billdata['cust_name'] ?>" id="onacname">
			  </div>
			  <div class="form-group row">
				<label class="col-sm-2 col-form-label" style="margin-top: 5px;width: 15%">Cr. Balance</label>
				<input autocomplete="off" type="text" readonly="readonly" name="deductamount" id="deductamount" class="col-sm-2 form-control" style="display: inline;width: 10%">
				<!-- <input autocomplete="off" type="text" class="col-sm-1 form-control" style="display: inline;width: 20%"> -->
			  </div>
			  	<?php if($complimentary_status == 1) { ?>
				  	<div class="form-group row">
						<label class="col-sm-3 col-form-label" style="margin-top: 5px;width: 20%">Complimentary Reason</label>
						<input autocomplete="off" type="text" name="complimentary_reason" id="complimentary_reason" class="col-sm-2 form-control" style="display: inline;width: 20%">
						<input autocomplete="off" type="hidden" name="compl_status" id="compl_status" class="col-sm-2 form-control" value="1">
				  		
				  	</div>
				  	<div class="form-group row">
						<a id="compli_btn" title="compli_btn" class="btn btn-info" style="font-size:20px; width:200px; height: 50px; ;">Complimentary</a>
					</div>
				<?php } ?>
			</div>
		</form>
	</div>
	<script type="text/javascript">
	$(document).on('keydown', '#cash', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
			save();
		  }
		//console.log(code);
		//alert(code);
	});

	$(document).ready(function() {
		$('#cash').focus();
	});

	$('#msrcardno').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/msr_recharge/autocompletecustmsrno&token=<?php echo $token; ?>&filter_msr_no=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						$('#msrcustname').val(item.name);
						$('#msr_cust_id').val(item.c_id);
						msr_noo = item.msr_no;
						c_id = $('#msr_cust_id').val();
						//alert(c_id);
						$.ajax({
							url:'index.php?route=catalog/msr_recharge/getPreviousBal&token=<?php echo $token; ?>&cust_id='+c_id+'&msr_no='+ encodeURIComponent(msr_noo),
							type:'post',
							dataType: 'json',
							success:function(json){
								pre_balz = json.pre_bal
								if(pre_balz == '' || pre_balz == '0' || pre_balz == 'null' || pre_balz == null || isNaN(pre_balz)){
									pre_balz = 0;
								}
								$('#mrs_amt').val(pre_balz);
								//$('#pre_ball').val(pre_balz);
							}
						});
						
					}));
				}
			});
		}, 
		focus: function(event, ui) {
			return false;
		}
	});


	function save(){
		var orderidmodify = '<?php echo $orderidmodify ?>';
		var orderid = '<?php echo $order_id ?>';
		$('#orderid').val(orderid);
		action = '<?php echo $action; ?>';
		action = action.replace("&amp;", "&");
		action = action.replace("&amp;", "&");
		$.post(action, $('#payform').serialize()).done(function(data) {
			var parsed = JSON.parse(data);
			//alert(parsed.info);

			if(parsed.info == 1){
				$('.sucess').html('');
				$('.sucess').append(parsed.done);

			}else if(parsed.info == 2){
				//top.close();
				//$('.sucess').hide();
				//dailog.dailog('hide');
				//$('.sucess').modal('hide');
				$('.sucess').html('');
				$('.sucess').append(parsed.done);
				//window.close(); 
				 //close();
			}else if(parsed.info == 3){
				$('.sucess').html('');
				$('.sucess').append(parsed.done);
			}else if(parsed.info == 4){
				alert("Complimentary Remark is Compulsory ...!!!");
					return false;
			}else{
				if(orderidmodify != ''){
					alert("Remark is Compulsory");
					return false;
				}else{
					alert("Nothing is Settle/Payment is greater/less");
					return false;
				}
			}
		});
	}


	$("#compli_btn").on('click',function(){
		$("#cash").val(0);
		$("#credit").val(0);
		$("#cashval").val(0);
		$("#creditval").val(0);
		$("#online").val(0);
		save();
	});
				
	$("#cashbtn").on('click',function(){
		$("#cash").val($("#default").val());
		$("#credit").val(0);
		$("#cashval").val($("#cash").val());
		$("#creditval").val(0);
		$("#online").val(0);
		save();
	});

	$("#cardbtn").on('click',function(){
		$("#credit").val($("#default").val());
		$("#cash").val(0);
		$("#cashval").val(0);
		$("#creditval").val($("#credit").val());
		$("#online").val(0);
		save();
	});

	$("#onlinebtn").on('click',function(){
		$("#online").val($("#default").val());
		$("#payment_type").val();
		$("#cash").val(0);
		$("#cashval").val(0);
		$("#creditval").val(0);
		$("#credit").val(0);
		save();
	});


	$(document).on('keyup','#cash','#credit', function(e){
		cashval = parseInt($('#cash').val());
		creditval = parseInt($('#credit').val());
		totalamt = parseInt($('#grandtotal').val());
		remamountcash = totalamt - cashval;
		$('#credit').val(remamountcash);
		if( $('#cash').val() == ''){
			$('#credit').val(totalamt);
		} 
		// if ($('#cash').val() == '' && $('#credit').val() == '') {
		// 		$('#msrcard').val(totalamt);
		// 	}
	});

	// $(document).on('keyup','#credit', function(e){
	// 	cashval = parseInt($('#cash').val());
	// 	creditval = parseInt($('#credit').val());
	// 	totalamt = parseInt($('#grandtotal').val());

	// 	remamountcredit = totalamt - creditval;
	// 	$('#cash').val(remamountcredit);
	// 	if( $('#credit').val() == ''){
	// 		$('#cash').val(totalamt);
	// 	}

	// });

	$(document).on('keyup', '#cash,#credit,#online,#cardno,#custname,#mealpass,#pass,#roomservice,#msrcard,#onac', function(e) {
		if($("#cash").val() < 0 || $("#credit").val() < 0 || $("#online").val() < 0 || $("#mealpass").val() < 0 || $("#pass").val() < 0 || $("#roomservice").val() < 0 || $("#msrcard").val() < 0 || $("#deductamount").val() < 0 /*$("#onac").val() < 0*/){
			alert("Amount cannot be negative/amount is more");
			$("#cash").val($("#default").val());
			$("#credit").val(0);
			$("#online").val(0);
			$("#mealpass").val(0);
			$("#pass").val(0);
			$("#roomservice").val(0);
			$("#msrcard").val(0);
			//$("#onac").val();
			return false;
		}
		$("#cashval").val($("#cash").val());
		$("#creditval").val($("#credit").val());
	});

	

	$('#onaccust').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/settlement/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {   
					response($.map(json, function(item) {
						return {
							label: item.name + "-" + item.contact,
							value: item.name,
							c_id: item.c_id,
							contact: item.contact
						}
					}));
				}
			});
		}, 
		select: function(event, ui) {
			$('#onaccust').val(ui.item.label);
			$('#onacname').val(ui.item.value);
			$('#c_id').val(ui.item.c_id);
			$('#onaccontact').val(ui.item.contact);
			var orderid = '<?php echo $order_id ?>';
			$.ajax({
				url: 'index.php?route=catalog/settlement/credit&token=<?php echo $token; ?>&c_id='+ui.item.c_id+'&order_id='+orderid,
				type:"POST",
				dataType: 'json',
				success: function(json) {   
					$('#onac').val(json.credit);
				}
			});
			if($('#onaccust').val() != ''){
				$('#deductamount').prop('readonly',false);
			} else{
				$('#deductamount').prop('readonly',true);
				$('#deductamount').val(0);
				$('#onac').val(0);
			}
		},
	});

	$(document).on('keyup','#onaccust', function(e){
		if($('#onaccust').val() != ''){
			$('#deductamount').prop('readonly',false);
		} else{
			$('#deductamount').prop('readonly',true);
			$('#deductamount').val(0);
			$('#c_id').val(0);
			$('#onaccontact').val(0);
			$('#onacname').val(0);
			$('#onac').val(0);
		}
	});

	$(document).on('keyup','#roomno', function(e){
		if($('#roomno').val() != ''){
			$('#roomservice').prop('readonly',false);
		} else{
			$('#roomservice').prop('readonly',true);
			$('#roomservice').val(0);
		}
	});

	$(document).ready(function(){
		customer = $('#onaccust').val();
		if(customer != ''){
			$('#deductamount').prop('readonly', false);
		}
	});

	</script>
</div>
	