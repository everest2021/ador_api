<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Store name</b></h4></center><br>
					       		<div class="form-row">
						       		<div class="col-sm-offset-5">
							       		<div class="col-sm-4">
									    	<select class="form-control" name="filter_storename">
										    	<option value='' selected="selected"><?php echo "All" ?></option>
									    		<?php foreach($storenames as $key => $value) { ?>
										    		<?php if($value['store_code'] == $storename ) { ?>
										    			<option value="<?php echo $value['store_code'] ?>" selected="selected"><?php echo $value['store_name']?></option>
										    		<?php } else { ?>
										    			<option value="<?php echo $value['store_code'] ?>"><?php echo $value['store_name']?></option>
										    		<?php } ?>
									    		<?php } ?>
									    	</select>
									    </div>
									</div>
							    </div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
				 	<div class="col-sm-offset-10">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
				 	</div>
					<!-- <div class="col-sm-6 col-sm-offset-3">
					<?php if($startdate != '' && $enddate != ''){ ?>
						<center><h5><b>Daily Sales Summary Report</b></h5></center>
						<h5>From : <?php echo $startdate; ?></h5>
						<h5 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h5><br>
						  <table class="table table-bordered table-hover" style="text-align: center;">
								<?php $total = 0; ?>
								<?php foreach($finaldatas as $finaldata) { ?>
									<tr>
										<th style="text-align: center;">DATE</th>
										<th style="text-align: center;">CASH</th>
										<th style="text-align: center;">CARD</th>
										<th style="text-align: center;">NET SALE</th>
										<th style="text-align: center;">VAT</th>
										<th style="text-align: center;">GST</th>
										<th style="text-align: center;">TOTAL BILL</th>
									</tr>
									<?php foreach($finaldata['date_array'] as $data) { ?>
										<tr>
											<td><?php echo $data['date'] ?></td>
											<td><?php echo $data['grandtotalcash'] ?></td>
											<td><?php echo $data['grandtotalcredit'] ?></td>
											<td><?php echo $data['total'] ?></td>
											<td><?php echo $data['totalvat'] ?></td>
											<td><?php echo $data['totalgst'] ?></td>
											<td><?php echo $data['totalcount'] ?></td>
										</tr>
									<?php } ?>
								<?php } ?>
						  	</table>
					  <?php } ?>
				 	</div> -->
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportdaywise/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		  location = url;
		  setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
</div>
<?php echo $footer; ?>