<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
	    	<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
		      <h1><?php echo "Online Food Order Report" ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo "Online Food Order Report"; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>Agg Order ID</label>
									    	<input type="agg_order_id" name='agg_order_id' value="<?php echo $agg_order_id?>" class="form-control " placeholder="Agg Order ID">
									    </div>
									    <div class="col-sm-3">
									   		<label>Order From</label>
									    	<select class="form-control" name="filter_type">
									    		<option value='' selected="selected"><?php echo "All" ?></option>
									    		<?php foreach($types as $key => $value) { ?>
									    			<?php if($key == $type) { ?>
									    				<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
									    			<?php } else { ?>
									    				<option value="<?php echo $key ?>"><?php echo $value?></option>
									    			<?php } ?>
									    		<?php } ?>
									    	</select>
								    	</div>
								    	<div class="col-sm-3">
									   		<label>Order Status</label>
									    	<select class="form-control" name="filter_order_status">
									    		<option value='' selected="selected"><?php echo "All" ?></option>
									    		<?php foreach($urbanpiper_order_status as $ukey => $uvalue) { ?>
									    			<?php if($ukey == $urbapiper_status) { ?>
									    				<option value="<?php echo $ukey ?>" selected="selected"><?php echo $uvalue?></option>
									    			<?php } else { ?>
									    				<option value="<?php echo $ukey ?>"><?php echo $uvalue?></option>
									    			<?php } ?>
									    		<?php } ?>
									    	</select>
								    	</div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10" style="display: none;">
				 		<button id="print" type="button" class="btn btn-primary">Print</button>
						<a id="sendmail" data-toggle="tooltip" title="<?php echo "Send Mail"; ?>" style="width: 70px;height: 34px;" class="btn btn-primary "><i class="fa fa-envelope "></i></a>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 	</div>
					<div class="col-sm-10 col-sm-offset-1">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Online Food Order Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
							<tr>
								<th style="text-align: center;">Sr No</th>
								<th style="text-align: center;">Date</th>
								<th style="text-align: center;">Telephone</th>
								<th style="text-align: center;">Customer</th>
								<th style="text-align: center;">Order No</th>
								<th style="text-align: center;">Agg Order No</th>

								<th style="text-align: center;">Grand Total </th>
								<th style="text-align: center;">Order From</th>
								<th style="text-align: center;">Payment Type</th>
								<th style="text-align: center;">Status</th>
								<th style="text-align: center;">Reject Reason</th>
								<th style="text-align: center;">Action</th>

							</tr>
							<?php if(isset($_POST['submit'])){ ?>
								<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $foodtotal = 0; $fdistotal = 0; $liqtotal = 0;$ldistotal = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0;?>
							  	<?php foreach ($billdatas as $key => $value) {?>
							  		<tr>
							  			<td colspan="12"><b><?php echo $key ?><b></td>
							  		</tr>
							  		<?php $sr_no = 1; ?>
								  	<?php foreach ($value as $data) {?>
								  		<tr id="order_table_<?php echo  $data['online_order_id'] ?>" onclick="filter_rank(<?php echo  $data['online_order_id'] ?>)">
								  			<td><?php echo $sr_no ?></td>
								  			<td>     <?php  $order_time = '';
												if($data['online_created_time'] != ''){
													$order_time =  date('d-m-Y H:i:s', $data['online_created_time']/1000); //date("d-m-Y h:i:sa", date($ovalue['online_deliverydatetime']));
												}echo $order_time ?></td>
											<td><?php echo $data['telephone'] ?></td>
								  			<td><?php echo ($data['firstname']) ?></td>
								  			<td><?php echo $data['online_order_id'] ?></td>
								  			<td><?php echo $data['zomato_order_id'] ?></td>
								  			<td style="color: #3391ff; "><?php echo (number_format($data['grand_tot'],2)) ?></td>
								  			<td><?php echo $data['online_channel'] ?></td>

								  			<td><?php echo $data['online_payment_method'].'('. $data['online_instruction'].')'?></td>
								  			<td><?php echo $data['new_state'] ;?></td>
								  			<td><?php echo $data['message'] ;?></td>

								  			<?php if($data['new_state'] != 'Cancelled') { ?>
												<td >
									      			<a onclick="bill_print(<?php echo $data['online_order_id'] ?>)"  class="btn btn" style="background-color: #3c8d49;color:#fff;font-size: 13px;"> Print Bill</a>
									      		</td>
											<?php } else { ?>
												<td >
									      		</td>
											<?php }  ?>

								  			
								  				
								  		</tr>
								  		<tr class="hide_tr order_item_table_<?php echo$data['online_order_id'] ?>">
								  			<td colspan="12">
								  				<table class="table table-bordered table-hover " id="order_item_table_<?php echo  $data['online_order_id'] ?>" >
								  					<thead  style="background-color: #e72b0b; color: #fff; font-family: monospace; font-size: 130%;">
												    	<tr>
												      		<th scope="col">Order No</th>
												      		<th scope="col">Item Name</th>
												      		<th scope="col">Qty</th>
												      		<th scope="col"  colspan="4">Rate</th>
												      		<th scope="col">Amt</th>
												      		<th scope="col">GST</th>
												      		<th scope="col">GST Value</th>
												      		<th scope="col">Total</th>
												    	</tr>
												  	</thead>
												  	<tbody >
												  		
												  	</tbody>
								  				</table>
								  			</td>
								  		</tr>
								  		
								  	<?php $sr_no++; } ?>
								<?php } ?>
							<?php } ?>
					  	</table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_type = $('select[name=\'filter_type\']').val();
		  var filter_order_status = $('select[name=\'filter_order_status\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_order_status) {
			  url += '&urbapiper_status=' + encodeURIComponent(filter_order_status);
		  }

		   if (filter_type) {
			  url += '&type=' + encodeURIComponent(filter_type);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#sendmail').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/sendmail&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}
		.hide_tr{
			display: none;
		}	

		h3,h4 {
			color: black;
		}
	</style>
	<script type="text/javascript">
		function filter_rank(online_order_id){
			if($('#order_item_table_'+online_order_id+' > tbody > tr').length ){
				$('#order_item_table_'+online_order_id+" > tbody").html('');
				$('.hide_tr').hide();
			} else {
				if(online_order_id != '' && online_order_id > 0){
					$.ajax({
						url: 'index.php?route=catalog/werafood_order_report/function_ranking&token=<?php echo $token; ?>&order_id='+online_order_id,
						type: 'post',
						dataType: 'json',
						success: function(json) {
							if(json['html'] != ''){
								$('.hide_tr').hide();
								$('.order_item_table_'+online_order_id).show();
								$('#order_item_table_'+online_order_id+" > tbody").html('');
								$('#order_item_table_'+online_order_id+" > tbody").html(json['html']);
							}
						}
					});	
				}
			}
		}

		function bill_print(order_id){
		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/werafood_order_report/bill_print&token=<?php echo $token; ?>&order_id='+order_id,
				dataType: 'json',
				data: {"data":"check"},
				success: function(json){
					//location.reload();
				}
			});
		}
	</script>

</div>
<?php echo $footer; ?>