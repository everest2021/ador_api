<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
				<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
						<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
			   </ul>
			</div>
		</div>

  		<div class="container-fluid">
			<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>

	   		<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
				</div>

	   			<div class="panel-body">
		   			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
			     		<div class="form-group">
				            <label class="col-sm-2 control-label" for="input-filename0"><?php echo 'Upload Images'; ?></label>
				            <div class="col-sm-4">
				              	<div class="input-group div_1">
				                	<input style="height: 35px;display: none;" type="text" name="filename0" value="<?php //echo $filename0; ?>" placeholder="<?php echo 'File Name'; ?>" id="input-filename0" class="form-control" />
				                	<span class="input-group-btn">
				                  		<button type="button" id="button-upload0" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload'; ?>&nbsp;&nbsp;&nbsp;</button>
				                	</span> 
				              	</div>
				            </div>
				        </div>
				        <?php $extra_field = 1; ?>
				        <?php if($otherimages_datas){ ?>
					        <div class="form-group" id="productraw_row<?php echo $extra_field; ?>">
					            <div class="col-sm-2">
					            	<b>Name</b>
					            </div>
					            <div class="col-sm-2" style="width: 8%;">
					            	<b>Action</b>
					            </div>
					        </div>
				            <?php foreach($otherimages_datas as $ckey => $cvalue){ ?>
				              	<div class="form-group" id="productraw_row<?php echo $extra_field; ?>">
				                	<div class="col-sm-2" style="">
				                  		<a style="cursor: pointer;" target="_blank" href="<?php echo $cvalue['value_proc'] ?>"><?php echo $cvalue['image']; ?></a>
				                	</div>
				                	<div class="col-sm-2" style="margin-top: 0px;width: 8%;">
				                  		<a style="cursor: pointer;color: #fff;background-color: red;" onclick="remove_folder('<?php echo $cvalue['image_id']; ?>')" class="button form-control" id="remove<?php echo $extra_field; ?>" >
				                    		<?php echo 'Remove'; ?>
				                  		</a>
				                	</div>
				              	</div>
				              	<?php $extra_field ++; ?>
				            <?php } ?>
				        <?php } ?>
		  			</form>
				</div>
			</div>
		</div>
	</div>
	<script>
	$('#button-upload0').on('click', function() {
	  $('#form-upload0').remove();
	  $('body').prepend('<form enctype="multipart/form-data" id="form-upload0" style="display: none;"><input id="input-file" type="file" name="file[]" multiple /></form>');
	  $('#form-upload0 #input-file').trigger('click');
	  if (typeof timer != 'undefined') {
	      clearInterval(timer);
	  }
	  timer = setInterval(function() {
	    if ($('#form-upload0 #input-file').val() != '') {
	      clearInterval(timer);   
	      $.ajax({
	        url: 'index.php?route=catalog/baner/uploadmulti&token=<?php echo $token; ?>',
	        type: 'post',   
	        dataType: 'json',
	        data: new FormData($('#form-upload0')[0]),
	        cache: false,
	        contentType: false,
	        processData: false,   
	        beforeSend: function() {
	          $('#button-upload0').button('loading');
	        },
	        complete: function() {
	          $('#button-upload0').button('reset');
	        },  
	        success: function(json) {
	          if (json['error']) {
	            alert(json['error']);
	          }
	          if (json['success']) {
	            alert(json['success']);
	            console.log(json);
	            
	            reload_href = json['reload_href'];
	            reload_href = reload_href.replace("&amp;", "&");
	            reload_href = reload_href.replace("&amp;", "&");
	            reload_href = reload_href.replace("&amp;", "&");
	            reload_href = reload_href.replace("&amp;", "&");
	            reload_href = reload_href.replace("&amp;", "&");
	            window.location.href = reload_href;
	            
	            //$('input[name=\'filename0\']').attr('value', json['filename']);
	            //$('input[name=\'mask\']').attr('value', json['mask']);
	            //var previewHtml = '<br/><img src="'+json['img_src']+'" width="150" height="100" />';
	            //$('.div_1').nextAll().remove();
	            //$('.div_1').after(previewHtml);
	          }
	        },      
	        error: function(xhr, ajaxOptions, thrownError) {
	          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
	      });
	    }
	  }, 500);
	});  

	function remove_folder(image_id){
		
	  	$.ajax({
		    url: 'index.php?route=catalog/baner/remove&token=<?php echo $token; ?>&image_id='+image_id,
		    type: 'post',   
		    dataType: 'json',
		    success: function(json) {
		      if (json['success']) {
		        reload_href = json['reload_href'];
		        reload_href = reload_href.replace("&amp;", "&");
		        reload_href = reload_href.replace("&amp;", "&");
		        reload_href = reload_href.replace("&amp;", "&");
		        reload_href = reload_href.replace("&amp;", "&");
		        reload_href = reload_href.replace("&amp;", "&");
		        reload_href = reload_href.replace("&amp;", "&");
		        window.location.href = reload_href;  
		      }
		    }
	  	});
	}
	</script>
</div> 
<?php echo $footer; ?>