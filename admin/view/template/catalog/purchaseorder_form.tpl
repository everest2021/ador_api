<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       	<button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="save" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       	<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		    </div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		    </ul>
	    </div>
	</div>
    <div class="container-fluid">
    	<?php if ($error_warning) { ?>
		    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    </div>
	    <?php } ?>
	    <div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		    </div>
			<div class="panel-body">
		   		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
				  	<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo 'Date'; ?></label>
					    <div class="col-sm-3">
					     	<input autocomplete="off" readonly="readonly" type="text" name='date' value="<?php echo date('d-m-Y') ?>" class="form-control">
					    </div>
					    <label class="col-sm-1 control-label"><?php echo 'Store'; ?></label>
					    <div class="col-sm-3">
					     	<input type="text" readonly="readonly" name="storename" value="<?php echo $storename ?>" class="form-control">
					     	<input type="hidden" name="store" value="<?php echo $store ?>">
					    </div>
				    </div>
				    <div class="form-group">
				    	<label class="col-sm-1 control-label"><?php echo 'Item Code'; ?></label>
						<div class="col-sm-2">
							<input type="hidden" id="itemid" class="form-control"/>
							<input type="text" autocomplete="off" id="itemcode" placeholder="<?php echo 'Item Code'; ?>" class="form-control"/>
						</div>
					    <label class="col-sm-1 control-label"><?php echo 'Item Name'; ?></label>
						<div class="col-sm-2">
							<input type="text" id="itemname" placeholder="<?php echo 'Item Name'; ?>" class="form-control"/>
						</div>
						<label class="col-sm-1 control-label"><?php echo 'Quantity'; ?></label>
						<div class="col-sm-2">
							<input type="text" id="quantity" placeholder="<?php echo 'Quantity'; ?>" class="form-control"/>
							<input type="hidden" id="rate" class="form-control"/>
						</div>
						<label class="col-sm-1 control-label"><?php echo 'Notes'; ?></label>
						<div class="col-sm-2">
							<input type="text" id="notes" placeholder="<?php echo 'Notes'; ?>" class="form-control"/>
						</div>
					</div>
					<table id="data" class="table table-bordered">
						<tr>
							<th>Sr.No</th>
							<th>Item Name</th>
							<th>Quantity</th>
							<th>Rate</th>
							<th>Notes</th>
							<th>Remove</th>
						</tr>
					</table>
				</form>
	  		</div>
		</div>
  	</div>
  	<script type="text/javascript">
  	var i = 1;
  	$('#itemname').autocomplete({
	  delay: 500,
	  source: function(request, response) {
	    $.ajax({
	      url: 'index.php?route=catalog/purchaseorder/autocompleteitemname&token=<?php echo $token; ?>&filter_item_name=' +  encodeURIComponent(request.term),
	      dataType: 'json',
	      success: function(json) {   
	        response($.map(json, function(item) {
	          return {
	            label: item.item_name,
	            value: item.item_id,
	            code : item.item_code,
	            rate : item.rate
	          }
	        }));
	      }
	    });
	  }, 
	  select: function(event, ui) {
	  	$('#itemcode').val(ui.item.code);
	  	$('#itemname').val(ui.item.label);
	  	$('#itemid').val(ui.item.value);
	  	$('#rate').val(ui.item.rate);
		$('#quantity').focus();
	    return false;
	  }
	});

	$(document).keypress(function(event){
     if (event.which == '13') {
        event.preventDefault();
      }
	});

	$('#itemcode').keydown(function(e) {
		if(e.keyCode == 13){
			itemcode = $('#itemcode').val();
		    $.ajax({
		      url: 'index.php?route=catalog/purchaseorder/autocompleteitemname&token=<?php echo $token; ?>&filter_item_code='+itemcode,
		      dataType: 'json',
		      success: function(json) {
		      	if(json != ''){  
		      		$('#itemcode').val(json[0].item_code);
		      		$('#itemname').val(json[0].item_name);
		      		$('#itemid').val(json[0].item_id);
		      		$('#rate').val(json[0].rate);
		      		$('#quantity').focus();
	      		} else{
	      			alert("Item Doesn't Exists");
	      			$('#itemcode').select();
	      			return false;
	      		}
		      }
		    });
		}
	});

	$('#quantity').keydown(function(e) {
		if(e.keyCode == 13){
			$('#notes').focus();
		}
	});

	$('#notes').keydown(function(e) {
		itemname = $('#itemname').val();
		if(e.keyCode == 13){
			if(itemname != ''){
				itemid = $('#itemid').val();
				itemcode = $('#itemcode').val();
				qty = $('#quantity').val();
				rate = $('#rate').val();
				notes = $('#notes').val();
				html = '<tr id="re_'+i+'">'; 
				  html += '<td>'+i+'</td>';
			      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][item]" value="'+itemname+'"  class="form-control"/></td>';
			      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][qty]" id="qty_'+i+'" value = "'+qty+'" style="display:inline-block" class="qty form-control"/>';
			      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][rate]" id="rate_'+i+'" value = "'+rate+'" style="display:inline-block" class="rate form-control"/>';
			      html += '<input type="hidden" name="po_datas[' + i + '][itemcode]" value="'+itemcode+'"  class="form-control"/>';
			      html += '<input type="hidden" name="po_datas[' + i + '][itemid]" value="'+itemid+'"  class="form-control"/>';
			      html += '<td><input type="text" autocomplete = "off" name="po_datas[' + i + '][msg]" id="msg_'+i+'" value = "'+notes+'" class="msg form-control"/></td>';
			      html += '<td><a style="cursor: pointer;font-size: 16px;" onclick="remove_folder('+i+')" class="button inputs remove " id="remove_'+i+'" ><i class="fa fa-trash-o"></i></a></td>';
		      	html += '</tr>';
		      	$('#data').append(html);
				i++;
				$('#itemcode').focus();
				$('#itemcode').select();
				$('#quantity').val('');
				$('#notes').val('')
			}
		}
	});

	function remove_folder(c) {
		$('#re_'+c).remove();
	}

    $('.form_datetime').datetimepicker({
	  pickTime: false,
	  format: 'DD-MM-YYYY'
	});

  	</script>
</div>
<?php echo $footer; ?>