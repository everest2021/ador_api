<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
		    <div class="pull-right">
		    	<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		       	<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
		    </div>
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
	    </div>
    </div>
  <div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		   <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		    </div>
		<div class="panel-body">
		    <div class="well">
			   <div class="row">
			       <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Date'; ?></label>
				          <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="<?php echo 'Date'; ?>" id="input-filter_date" class="form-control form_datetime" />
				        </div>
			       </div>
			        <div class="col-sm-4">
				       <div class="form-group">
				          <label class="control-label" for="input-name"><?php echo 'Status'; ?></label>
				          <select class="form-control" name="filter_status" id="input-filter_status">
				          	<option selected="selected" value="All">All</option>
					          <?php foreach($status as $key => $value){?>
					          	<?php if($filter_status == $key && $filter_status != ''){ ?>
					          		<option value="<?php echo $key ?>" selected="selected"><?php echo $value?></option>
					          	<?php } else { ?>
					          		<option value="<?php echo $key ?>"><?php echo $value?></option>
					          	<?php } ?>
					          <?php } ?>
				          </select>
				        </div>
				       	<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			       </div>
			    </div>
		   </div>
	    </div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		    <div class="table-responsive">
			  <table class="table table-bordered table-hover">
			   <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>

				  	<td><?php if ($sort == 'po_number') { ?>
					<a href="<?php echo $sort_po_number; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Po Number'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_po_number; ?>"><?php echo 'Po Number'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'po_date') { ?>
					<a href="<?php echo $sort_po_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Date'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_po_date; ?>"><?php echo 'Date'; ?></a>
					<?php } ?></td>

					<td><?php if ($sort == 'status') { ?>
					<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Status'; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_status; ?>"><?php echo 'Status'; ?></a>
					<?php } ?></td>

				</tr>
			  </thead>
			  <tbody>
				<?php if ($items) { ?>
				<?php foreach ($items as $item) { ?>
				<tr>
				  <td><?php if (in_array($item['item_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $item['item_id']; ?>" />
					<?php } ?></td>
				  <td><?php echo $item['po_number']; ?></td>
				  <td><?php echo date('d-m-Y', strtotime($item['date'])); ?></td>
				  <?php if($item['status'] == '0') { ?>
				  	<td>Pending</td>
				  <?php } else { ?>
				  	<td>Done</td>
				  <?php } ?>
				</tr>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			  </table>
		    </div>
		</form>
		<div class="row">
		    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>

<script type="text/javascript">
$('.form_datetime').datetimepicker({
  pickTime: false,
  format: 'DD-MM-YYYY'
});

$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/purchaseorder&token=<?php echo $token; ?>';

  	var filter_date = $('input[name=\'filter_date\']').val();
  	if (filter_date) {
	  	url += '&filter_date=' + encodeURIComponent(filter_date);
  	}

  	var filter_status = $('select[name=\'filter_status\']').val();
  	if (filter_status) {
  		url += '&filter_status=' + encodeURIComponent(filter_status);
  	}

  	location = url;
});
</script>
<?php echo $footer; ?>