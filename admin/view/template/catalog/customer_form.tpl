<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;">
  	<div class="page-header">
		<div class="container-fluid">		
	  		<div class="pull-right sav">
				<button type="submit" form="form-sport" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1 class="testtitle"><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb bread">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid" >
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="well">
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label class="control-label" for="filter_name"><?php echo 'Customer Name'; ?></label>
							<input type="text" name="filter_name" value="" placeholder="<?php echo ' Name'; ?>" id="filter_name" class="form-control" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="control-label" for="filter_msr_no"><?php echo 'MSR NO'; ?></label>
							<input type="text" name="filter_msr_no" value="" placeholder="<?php echo ' MSR NO'; ?>" id="filter_msr_no" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
			  		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Customer Name'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="name"  id="name" value="<?php echo $name; ?>" placeholder="<?php echo 'Name'; ?>" class="form-control" />
				  			<input type="hidden" name="c_id" id="c_id" class="form-control" />
						     <?php if ($error_name) { ?>
                             <div class="text-danger"><?php echo $error_name; ?></div>
                             <?php } ?>
						</div>
						<label class="col-sm-3 control-label"><?php echo 'Customer Code'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="code" id="code" value="<?php echo $code; ?>" placeholder="<?php echo 'code'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Contact'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="contact" id="contact" value="<?php echo $contact; ?>" placeholder="<?php echo 'Contact'; ?>" class="form-control" />
				  			<!-- <?php if($flag == 1){ ?> -->
			   		        <input type="hidden" name="contactin" value="<?php echo $flag; ?>" id ="input-contactin" class="form-control" />
			   		<!-- <?php } ?> -->
							<?php if ($error_contact) { ?>
                            	<div class="text-danger"><?php echo $error_contact; ?></div>
                            <?php } ?>
                            <span id="error"></span>
						</div>
						<label class="col-sm-3 control-label" style="display: none"><?php echo 'Mobile No'; ?></label>
						<div class="col-sm-3" style="display: none">
				  			<input type="text" name="mobile_no" value="<?php echo $mobile_no; ?>" placeholder="<?php echo 'Mobile No'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'Address'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="address" id="address" value="<?php echo $address; ?>" placeholder="<?php echo 'Address'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group ">
						
						<label class="col-sm-3 control-label"><?php echo 'Alternate Number'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="alternate_no" id="alternate_no" value="<?php echo $alternate_no; ?>" placeholder="<?php echo 'Alternate No'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'Email 1'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="email" id="email" value="<?php echo $email; ?>" placeholder="<?php echo 'Email'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group">
						
						<label class="col-sm-3 control-label"><?php echo 'Email 2'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="email2" id="email2" value="<?php echo $email2; ?>" placeholder="<?php echo 'Email 2'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'GST NO'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="gst_no" id="gst_no" value="<?php echo $gst_no; ?>" placeholder="<?php echo 'Gst No'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group ">
						
						<label class="col-sm-3 control-label"><?php echo 'MSR NO'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="msr_no" id="msr_no" value="<?php echo $msr_no; ?>" placeholder="<?php echo 'Msr No'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'Dis %'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="discount" id="discount" value="<?php echo $discount; ?>" placeholder="<?php echo 'Discount'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group ">
						
						<label class="col-sm-3 control-label"><?php echo 'Landmark'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="landmark" id="landmark" value="<?php echo $landmark; ?>" placeholder="<?php echo 'Landmark'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'Area 1'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="area_1" id="area_1" value="<?php echo $area_1; ?>" placeholder="<?php echo 'Area 1'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div class="form-group ">
						
						<label class="col-sm-3 control-label"><?php echo 'Area 2'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="area_2" id="area_2" value="<?php echo $area_2; ?>" placeholder="<?php echo 'Area 2'; ?>" class="form-control" />
						</div>
						<label class="col-sm-3 control-label"><?php echo 'DOB'; ?></label>
						    <div class="col-sm-3">
						     	<input type="text" id="date_dob"  name='dob' value="<?php echo $dob?>" class="form-control form_datetime" placeholder="DOB">
						    </div>
			   		</div>
			   		<div class="form-group ">
						
					    <label class="col-sm-3 control-label"><?php echo 'Anniversary'; ?></label>
						    <div class="col-sm-3">
						    	<input type="text" id="date_anniversary" name='anniversary' value="<?php echo $anniversary?>" class="form-control form_datetime" placeholder="Envessory">
						    </div>
					
						<label class="col-sm-3 control-label"><?php echo 'Passport No'; ?></label>
					    <div class="col-sm-3">
					     	<input type="text" name='passport' id="passport" value="<?php echo $passport?>" class="form-control" placeholder="Passport No">
					    </div>
					</div>
			   		</div>
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
$('.well').hide();
$('#input-location_id').on('change', function() {
  loc_name = $('#input-location_id option:selected').text();
  $('#location').val(loc_name);
});

$(document).ready(function() {
  	falg = $('#input-contactin').val();
  	//alert(falg);
  	if(falg==1){
  		$('.sav').html('');
  		$('.bread').html('');
  		$('.testtitle').hide();
  		$('.panel-heading').hide();
  		$('.well').show();
		html = '<a data-toggle="tooltip" onclick="save(this.id)" id="save" title="save" class="btn btn-primary" >Save</a>';
		html += '<a title="Add" onclick="save(this.id)" id="add" class="btn btn-primary" ><i class="fa fa-plus"></i></button>'
  		$('.sav').append(html);
	}
});

function save(id){
  	action = '<?php echo $action1; ?>';
  	//alert(action);
  	action = action.replace("&amp;", "&");
  	$.post(action, $('#form-department').serialize()).done(function(data) {
    	var parsed = JSON.parse(data);
     	if(parsed.done != '' && id == 'save'){
     		$('.sucess').append(parsed.done);
     	}
     	if(parsed.done != '' && id == 'add'){
     		location = 'index.php?route=catalog/customer/add&token=<?php echo $token; ?>&iframe=1';
     	}

     	if(parsed.info != '1'){
     		$('#error').html(parsed.info);
     		$('#error').css('color','red');
     	}
	});
  	// alert('in');
  	// dialog1.dialog("close");
  	// alert(action);
}

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/customer/autocompletename&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name + " - " + item.contact,
            value: item.c_id, 
            contact : item.contact,
			address : item.address,
			email : item.email,
			email2 : item.email2,
			code : item.code,
			mobile_no : item.mobile_no,
			alternate_no : item.alternate_no,
			gst_no : item.gst_no,
			msr_no : item.msr_no,
			discount : item.discount,
			landmark : item.landmark,
			area_1 : item.area_1,
			area_2 : item.area_2,
			dob : item.dob,
			anniversary: item.anniversary,
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val((ui.item.label).substring(0, ui.item.label.indexOf('-')));
    $('input[name=\'c_id\']').val(ui.item.value);
    $('input[name=\'name\']').val((ui.item.label).substring(0, ui.item.label.indexOf('-')));
    $('input[name=\'code\']').val(ui.item.code);
	$('input[name=\'contact\']').val(ui.item.contact);
	$('input[name=\'mobile_no\']').val(ui.item.mobile_no);
	$('input[name=\'address\']').val(ui.item.address);	
	$('input[name=\'alternate_no\']').val(ui.item.alternate_no);
	$('input[name=\'email\']').val(ui.item.email);
	$('input[name=\'email2\']').val(ui.item.email2);
	$('input[name=\'gst_no\']').val(ui.item.gst_no);
	$('input[name=\'msr_no\']').val(ui.item.msr_no);
	$('input[name=\'discount\']').val(ui.item.discount);
	$('input[name=\'landmark\']').val(ui.item.landmark);
	$('input[name=\'area_1\']').val(ui.item.area_1);
	$('input[name=\'area_2\']').val(ui.item.area_2);
	$('input[name=\'dob\']').val(ui.item.dob);
	$('input[name=\'anniversary\']').val(ui.item.anniversary);
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_msr_no\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/customer/autocompletemsrno&token=<?php echo $token; ?>&filter_msr_no=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
        	//console.log(item);
        	$('#name').val(item.name);
		  	$('#c_id').val(item.c_id);
		  	$('#contact').val(item.contact);
		  	$('#code').val(item.code);
		  	$('#mobile_no').val(item.mobile_no);
		  	$('#address').val(item.address);
		  	$('#alternate_no').val(item.alternate_no);
		  	$('#email').val(item.email);
		  	$('#email2').val(item.email2);
		  	$('#gst_no').val(item.gst_no);
		  	$('#msr_no').val(item.msr_no);
		  	$('#contact').val(item.contact);
		  	$('#discount').val(item.discount);
		  	$('#landmark').val(item.landmark);
		  	$('#area_1').val(item.area_1);

		  	$('#area_2').val(item.area_2);
		  	$('#dob').val(item.dob);
		  	$('#anniversary').val(item.anniversary);
          
        }));
      }
    });
  }, 
 
  focus: function(event, ui) {

    return false;
  }
});

</script>
<script type="text/javascript">
	 	$("#date_dob").datepicker();
</script>
<script type="text/javascript">
	 	$("#date_anniversary").datepicker();
</script>
<?php echo $footer; ?>