<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
     <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>
    <div class="container-fluid">
      <div class="pull-right">
            <a style="background-color: #a37c70;border-color:#a37c70; " onclick="link_add_store_form()"  class="btn btn-success"><i class="fa fa-home" aria-hidden="true"></i>  Adding/Updating stores</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if($store_add == 0) { ?>
            <a style="background-color: #1a6662;border-color:#1a6662;" onclick="addstore()" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i>Send Store</a>&nbsp;&nbsp;
        <?php } else { ?>
             <a  style="background-color: #1a6662;border-color:#1a6662; " data-toggle="tooltip" class="btn btn-primary">Send Store Button Disbled Threee Attemt</a>
        <?php } ?>
        <a style="background-color: #1a6662;border-color:#1a6662;" onclick="check_live_store()" class="btn btn-success"></i>Live Store</a>&nbsp;&nbsp;
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
       
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                    <td class="text-left">Payload URL</td>
                    <td class="text-left">Event </td>
                    <td class="text-left">Action </td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td class="text-left"><b style="color:#2196f3"><?php echo $product['url']; ?></b></td>
                            <td class="text-left"><?php echo $product['event_type']; ?></td>
                            <td class="text-right">
                                <a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});

function link_add_store_form() {
      //  load_unseen_notification("closed_re");
        link_add_store_form1 = '<?php echo $link_add_store_form; ?>';
        link_add_store_form1 = link_add_store_form1.replace("&amp;", "&");
        window.location = link_add_store_form1; 
    }

function addstore(){
  //  load_unseen_notification("closed_re");
    $(document).ajaxSend(function() {
        $("#overlay").fadeIn(200);　
    });
    var ajax_my = 1;
    $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/urbanpiper_order/addstore&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {"data":"check"},
            success: function(json){
                if(json.success){
                    alert(json.success);
                    $('.sucess').html('');
                    $('.sucess').append(json.done);
                } else if(json.errors) {
                    alert(json.errors);
                } else {
                    alert("somthing is wrong");
                }
            location.reload();
            }
        }).done(function() {
            setTimeout(function(){
                $("#overlay").fadeOut(50);
            },50);
        });

}

function check_live_store() {
   // load_unseen_notification("closed_re");

    check_store_live1 = '<?php echo $check_store_live; ?>';
    check_store_live1 = check_store_live1.replace("&amp;", "&");

    window.location = check_store_live1; 
}




//--></script></div>
<style type="text/css">
    

    #overlay{   
        position: fixed;
        top: 0;
        z-index: 100;
        width: 100%;
        height:100%;
        display: none;
        background: rgba(0,0,0,0.6);
    }

    .cv-spinner {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;  
}
.spinner {
    width: 40px;
    height: 40px;
    border: 4px #09287d solid;
    border-top: 4px #2e93e6 solid;
    border-radius: 50%;
    animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
    0% { 
        transform: rotate(0deg); 
    }
    100% { 
        transform: rotate(359deg); 
    }
}
.is-hide{
    display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}






.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 40px;
}

input:checked + .slider {
  background-color: #00891d;
}

input:focus + .slider {
  box-shadow: 0 0 1px #00891d;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<?php echo $footer; ?>