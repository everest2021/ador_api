<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
		     </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
					<div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       	<center><h4><b>Select Data</b></h4></center><br>
					       		<div class="form-row">
					       			<div class="col-sm-2 col-sm-offset-4">
								    	<label> Date</label>
								     	<input type="text" name='filter_startdate' value="<?php echo $filter_startdate?>" class="form-control form_datetime" placeholder="Start Date">
								    </div>
								    <div style="display: none;" class="col-sm-2 ">
								    	<label>End Date</label>
								    	<input type="text" name='filter_enddate' value="<?php echo $filter_enddate?>" class="form-control form_datetime" placeholder="End Date">
								    </div>
								    <div class="col-sm-2">
									    	<center><label>Select Store</label></center>
									     	<select name="store" id="store" class="form-control">
									     		<option value="0">All</option>
									     		<?php foreach($stores as $store) { ?>
									     			<?php if($store['id'] == $store_id) { ?>
									     				<option value = "<?php echo $store['id'] ?>" selected="selected"><?php echo $store['store_name'] ?></option>
									     			<?php } else { ?>
									     				<option value = "<?php echo $store['id'] ?>" ><?php echo $store['store_name'] ?></option>
									     			<?php } ?>
									     		<?php } ?>
									     	</select>
									    </div>
								</div>
							</div>
							<div class="col-sm-12">
							<br>
							<center><a id ="filter" class="btn btn-primary" value="Show">Show</a>
							<a id="export" type="button" class="btn btn-primary">Export</a></center>
					    	</div>
					    </div>
					</div>
					<div class="col-sm-12 col-sm-offset-0">
						<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
						<?php date_default_timezone_set("Asia/Kolkata");?>
						<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
						<center><h3><b>Stock Report</b></h3></center>
					  	<table class="table table-bordered table-hover" style="text-align: center;">
					  		<?php foreach($final_data as $fkey =>$fvalue) { ?>
								<tr>
									<th style="text-align: center;">Item Name</th>
									<th style="text-align: center;">Opening Qty</th>
									<th style="text-align: center;">Purchase Qty</th>
									<th style="text-align: center;">Return</th>
									<th style="text-align: center;">Manufacture</th>
									<th style="text-align: center;">Trans. From Qty</th>
									<th style="text-align: center;">Trans. In Qty</th>
									<th style="text-align: center;">Wastage /Adjustment Qty</th>
									<th style="text-align: center;">Sales</th>
									<th style="text-align: center;">Closing Qty</th>
									<th style="text-align: center;">Purchase Price</th>
									<th style="text-align: center;">Value</th>
								</tr>
								<tr>
									<td colspan="12"><b><?php echo $fvalue['store_name'] ?></b></td>
								</tr>
								<?php foreach($fvalue['all_data'] as $key =>$value) { //echo "<pre>"; print_r($fvalue['all_data'])?>
									<tr>
										<td><?php echo $value['item_name'] ?></td>
										<td><?php echo $value['open_bal'] ?></td>
										<td><?php echo $value['purchase_order'] ?></td>
										<td><?php echo "0" ?></td>
										<td><?php echo $value['manufac_amt'] ?></td>
										<td><?php echo $value['stock_transfer_from'] ?></td>
										<td><?php echo $value['stock_transfer_to'] ?></td>
										<td><?php echo $value['wastage_amt'] ?></td>
										<td><?php echo $value['sale_qty'] ?></td>
										<td><?php echo $value['avail_quantity'] ?></td>
										<td><?php echo $value['stock_rate'] ?></td>
										<td><?php echo $value['final_amt'] ?></td>
									</tr>
								<?php } ?>
								<tr>
									<td><b><?php echo "Total" ?></b></td>
									<td><?php echo $fvalue['sub_total_open_bal'] ?></td>
									<td><?php echo $fvalue['sub_total_purchase_order'] ?></td>
									<td><?php echo "0" ?></td>
									<td><?php echo $fvalue['sub_total_manufacture_order'] ?></td>
									<td><?php echo $fvalue['sub_total_stock_transfer_from'] ?></td>
									<td><?php echo $fvalue['sub_total_stock_transfer_to'] ?></td>
									<td><?php echo $fvalue['sub_total_wastage_amt'] ?></td>
									<td><?php echo $fvalue['sub_total_sale_qty'] ?></td>
									<td><?php echo $fvalue['sub_total_avail_quantity'] ?></td>
									<td><?php echo $fvalue['sub_total_stock_rate'] ?></td>
									<td><?php echo $fvalue['sub_total_final_amt'] ?></td>
								</tr>
					 			<tr>
					 				<td colspan="12"></td>
					 			</tr>
							<?php } ?>
							<tr>
								<td colspan="1"><b><?php echo "Grand Total" ?></b></td>
								<td><?php echo $grand_total_open_bal ?></td>
								<td><?php echo $grand_total_purchase_order ?></td>
								<td><?php echo "0" ?></td>
								<td><?php echo $grand_total_manufacture_order ?></td>
								<td><?php echo $grand_total_stock_transfer_from ?></td>
								<td><?php echo $grand_total_stock_transfer_to ?></td>
								<td><?php echo $grand_total_wastage_amt ?></td>
								<td><?php echo $grand_total_sale_qty ?></td>
								<td><?php echo $grand_total_avail_quantity ?></td>
								<td><?php echo $grand_total_stock_rate ?></td>
								<td><?php echo $grand_total_final_amt ?></td>
							</tr>  	
					  	</table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/reportbill/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  var filter_status = $('select[name=\'filter_status\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }

		  if (filter_status) {
			  url += '&filter_status=' + encodeURIComponent(filter_status);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#filter').on('click', function() {
		  	var url = 'index.php?route=catalog/stock_report&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var store_id = $('select[name=\'store\']').val();
		  	
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		  	 if (store_id) {
			  url += '&store_id=' + encodeURIComponent(store_id);
		  }
		  	//alert(url);
		   	
		  	location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}

		$('#export').on('click', function() {
		  	var url = 'index.php?route=catalog/stock_report/export&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  	var filter_enddate = $('input[name=\'filter_enddate\']').val();
		  	var store_id = $('select[name=\'store\']').val();
		  	
		  	
			if (filter_startdate) {
				url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  	}
			if (filter_enddate) {
				url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  	}
		  	if (store_id) {
			  url += '&store_id=' + encodeURIComponent(store_id);
		  }
		   	
		  	location = url;
		});

	</script>
		<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>
	</script>
</div>
<?php echo $footer; ?>