<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
	  <div class="container-fluid">
			<div class="pull-right">
				<?php if($show_category_time == 0) { ?>
					<a  href="<?php echo $send_timing; ?>" class="btn btn-primary" style = "display: none;"><i class="fa fa-paper-plane" aria-hidden="true"></i>Send Category Timing</a>
				<?php } else { ?>
					<a  data-toggle="tooltip" title="<?php echo 'Sync Menu Items'; ?>" style = "display: none;" class="btn btn-primary">Send Category Timing Button Disbled Threee Attemt</a>
		    	<?php } ?>
				<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
			  <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			    <ul class="breadcrumb">
			       <?php foreach ($breadcrumbs as $breadcrumb) { ?>
			       <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			       <?php } ?>
			    </ul>
	   </div>
   </div>
   <div class="container-fluid">
		  <?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			   <button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		   <?php } ?>
		   <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		  <?php } ?>
	  <div class="panel panel-default">
		  <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
	      </div>
	  <div class="panel-body">
		  <div class="well">
		        <div class="row">
			       <div class="col-sm-4">
			          <div class="form-group">
				         <label class="control-label" for="input-name"><?php echo 'Category Name'; ?></label>
				         <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo ' Name'; ?>" id="filter_name" class="form-control" />
				         <input type="hidden" name="filter_category_id" value="<?php echo $filter_category_id; ?>" id="filter_category_id" class="form-control" />
				       </div>
			       </div>
			    <div class="col-sm-4">
				   <div class="form-group">
				       <label class="control-label" for="input-location_name"><?php echo 'Parent Category'; ?></label>
				       <input type="text" name="filter_parent" value="<?php echo $filter_parent; ?>" placeholder="<?php echo 'Parent Category'; ?>" id="input-filter_parent" class="form-control" />
				       <input type="hidden" name="filter_parent_id" value="<?php echo $filter_parent_id; ?>" id="input-filter_parent_id" class="form-control" />
				       <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
				    </div>
			    </div>
		   </div>
		</div>
		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
		  <div class="table-responsive">
			<table class="table table-bordered table-hover">
			  <thead>
				<tr>
				  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  <td class="text-right">
					<?php if ($sort == 'category_id') { ?>
					<a href="<?php echo $category_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Category_id"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $category_id; ?>"><?php echo "Category Id"; ?></a>
					<?php } ?></td>
				  <td class="text-right">
					<?php if ($sort == 'name') { ?>
					<a href="<?php echo $name; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Category Name"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $name; ?>"><?php echo "Category Name"; ?></a>
					<?php } ?></td>
				  <td class="text-right">
					<?php if ($sort == 'parent_category') { ?>
					<a href="<?php echo $sort_parent_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Parent Category"; ?></a>
					<?php } else { ?>
					<a href="<?php echo $sort_parent_category; ?>"><?php echo "Parent Category"; ?></a>
					<?php } ?></td>
				  <td class="text-right">Sort Order</td>
				  <td class="text-right"><?php echo $column_action; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($tables) { ?>
				<?php $i = '1';?>
				<?php foreach ($tables as $table) { ?>
				<tr>
				  <td class="text-center">
					<?php if (in_array($table['category_id'], $selected)) { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $table['category_id']; ?>" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="selected[]" value="<?php echo $table['category_id']; ?>" />
					<?php } ?></td>
				  <td class="text-left"><?php echo $table['category_id']; ?></td>
				  <td class="text-right"><?php echo $table['name']; ?></td>
				  <td class="text-right"><?php echo $table['parent_category']; ?></td>
				  <td class="text-right"><?php echo $table['sort_order_api']; ?></td>
				  <td class="text-right"><a href="<?php echo $table['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
				</tr>
				<?php $i++?>
				<?php } ?>
				<?php } else { ?>
				<tr>
				  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			</table>
		  </div>
		</form>
		<div class="row">
		  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/subcategory&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').val();
  if (filter_name) {
	var filter_category_id = $('input[name=\'filter_category_id\']').val();
	if (filter_category_id) {
	  url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	   }
	  url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_parent = $('input[name=\'filter_parent\']').val();
  if (filter_parent) {
	var filter_category_id = $('input[name=\'filter_category_id\']').val();
	if (filter_category_id) {
	  url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	   }
	  url += '&filter_parent=' + encodeURIComponent(filter_parent);
  }

  location = url;

});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/subcategory/autocompletecname&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.category_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'category_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_parent\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/subcategory/autocompletepname&token=<?php echo $token; ?>&filter_parent=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.parent_category,
            value: item.category_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_parent\']').val(ui.item.label);
    $('input[name=\'category_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

</script></div>
<?php echo $footer; ?>