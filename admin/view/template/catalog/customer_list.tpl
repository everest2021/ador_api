<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sport').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="filter_name"><?php echo 'Customer Name'; ?></label>
								<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo ' Name'; ?>" id="filter_name" class="form-control" />
								<input type="hidden" name="filter_c_id" value="<?php echo $filter_c_id; ?>" id="filter_c_id" class="form-control" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="input-location_name"><?php echo 'Contact'; ?></label>
								<input type="text" name="filter_contact" value="<?php echo $filter_contact; ?>" placeholder="<?php echo 'Contact'; ?>" id="input-filter_contact" class="form-control" />
								<input type="hidden" name="filter_c_id" value="<?php echo $filter_c_id; ?>" id="input-filter_c_id" class="form-control" />
								<button type="button" id="button-filter"  class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
							</div>
						</div>
					</div>
				</div>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sport">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<td class="text-left">
										<?php echo "Sr.No"; ?>
									</td>
									<td class="text-right">
										<?php if ($sort == 'name') { ?>
										<a href="<?php echo $name; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Customer Name"; ?></a>
										<?php } else { ?>
										<a href="<?php echo $name; ?>"><?php echo "Customer Name"; ?></a>
										<?php } ?></td>
									<td class="text-right">
										<?php if ($sort == 'contact') { ?>
										<a href="<?php echo $contact; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Contact"; ?></a>
										<?php } else { ?>
										<a href="<?php echo $contact; ?>"><?php echo "Contact"; ?></a>
										<?php } ?></td>
									<td class="text-right">
										<?php if ($sort == 'email') { ?>
										<a href="<?php echo $email; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Email"; ?></a>
										<?php } else { ?>
										<a href="<?php echo $email; ?>"><?php echo "Email"; ?></a>
										<?php } ?></td>
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($customers) { ?>
								<?php $i = '1';?>
								<?php foreach ($customers as $customer) { ?>
								<tr>
									<td class="text-center">
										<?php if (in_array($customer['c_id'], $selected)) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $customer['c_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $customer['c_id']; ?>" />
										<?php } ?></td>
									<td class="text-left"><?php echo $i; ?></td>
									<td class="text-right"><?php echo $customer['name']; ?></td>
									<td class="text-right"><?php echo $customer['contact']; ?></td>
									<td class="text-right"><?php echo $customer['email']; ?></td>
									<td class="text-right"><a href="<?php echo $customer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
								</tr>
								<?php $i++?>
								<?php } ?>
								<?php } else { ?>
								<tr>
									<td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/customer&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();
	if (filter_name) {
		var filter_c_id = $('input[name=\'filter_c_id\']').val();
		if (filter_c_id) {
			url += '&filter_c_id=' + encodeURIComponent(filter_c_id);
		}
			url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_contact = $('input[name=\'filter_contact\']').val();
	if (filter_contact) {
		var filter_c_id = $('input[name=\'filter_c_id\']').val();
		if (filter_c_id) {
			url += '&filter_c_id=' + encodeURIComponent(filter_c_id);
		}
			url += '&filter_contact=' + encodeURIComponent(filter_contact);
	}

	// var filter_location = $('input[name=\'filter_contact\']').val();
	// if (filter_location) {
	// 	var filter_c_id = $('input[name=\'filter_c_id\']').val();
	// 	if (filter_c_id) {
	// 		url += '&filter_c_id=' + encodeURIComponent(filter_c_id);
	// 		}
	// 		url += '&filter_name=' + encodeURIComponent(filter_contact);
	// }

	location = url;
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/customer/autocompletename&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.c_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'c_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

$('input[name=\'filter_contact\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/customer/autocompletecontact&token=<?php echo $token; ?>&filter_contact=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.contact,
            value: item.c_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_contact\']').val(ui.item.label);
    $('input[name=\'c_id\']').val(ui.item.value);
		
    return false;
  },
  focus: function(event, ui) {

    return false;
  }
});

// $('input[name=\'filter_location\']').autocomplete({
// 	'source': function(request, response) {
// 		$.ajax({
// 			url: 'index.php?route=catalog/tabel/autocomplete&token=<?php echo $token; ?>&filter_location=' +  encodeURIComponent(request),
// 			dataType: 'json',
// 			success: function(json) {
// 				response($.map(json, function(item) {
// 					return {
// 						label: item['location'],
// 						value: item['location_id']
// 					}
// 				}));
// 			}
// 		});
// 	},
// 	'select': function(item) {
// 		$('input[name=\'filter_location\']').val(item['label']);
// 		$('input[name=\'filter_location_id\']').val(item['value']);
// 	}
// });


</script></div>
<?php echo $footer; ?>