<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				
		  		<button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a style="display: none;" href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb" style='display:none;'>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
	  		<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  		</div>
	  		<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-table_no"><?php echo 'Table No' ?></label>
						<div class="col-sm-3">
			  				<input type="number" name="table_no" value="<?php echo $table_no; ?>"  id="input-table_no" class="form-control" />
							
						</div>
		  			</div>
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-contact"><?php echo 'Contact Number' ?></label>
						<div class="col-sm-3">
			  				<input type="number" name="contact" value="<?php echo $contact; ?>"  id="input-contact" class="form-control" />
							<?php if ($error_contact_no) { ?>
			                	<div class="text-danger"><?php echo $error_contact_no; ?></div>
			                <?php } ?>
						</div>
		  			</div>
					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-name"><?php echo 'Customer Name' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="name" value="<?php echo $name; ?>"  id="input-name" class="form-control"  />
							<?php if ($error_name) { ?>
			                	<div class="text-danger"><?php echo $error_name; ?></div>
			                <?php } ?>
						</div>
		  			</div>
		  			
					<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-email"><?php echo 'E-mail Id' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="email" value="<?php echo $email; ?>"  id="input-email" class="form-control"  />
							<?php if ($error_email) { ?>
			                	<div class="text-danger"><?php echo $error_email; ?></div>
			                <?php } ?>
						</div>
		  			</div>
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-email"><?php echo 'Address' ?></label>
						<div class="col-sm-3">
			  				<input type="text" name="address" value="<?php echo $address; ?>"  id="input-address" class="form-control"  />
						</div>
		  			</div>
		  			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-dob"><?php echo 'Date Of Birth' ?></label>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_dob"><?php echo 'Date' ?></label>
							<select name="date_dob" id="date_dob" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_dob){ ?>
		                  				<option selected="selected" value="<?php echo $date_dob ?>"><?php echo $date ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_dob"><?php echo 'Month' ?></label>
							<select name="month_dob" id="month_dob" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_dob){ ?>
		                  				<option selected="selected" value="<?php echo $month_dob ?>"><?php echo $month ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-2">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_dob"><?php echo 'Year' ?></label>
							<select name="year_dob" id="year_dob" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_dob){ ?>
		                  				<option selected="selected" value="<?php echo $year_dob ?>"><?php echo $year ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
		   			</div>

		   			<div class="form-group">
						<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date Of Anniversary' ?></label>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-date_doa"><?php echo 'Date' ?></label>
							<select name="date_doa" id="date_doa" class="form-control">
		                  		<?php for ($date=1; $date<=31; $date++) { ?>
		                  			<?php if($date == $date_doa){ ?>
		                  				<option selected="selected" value="<?php echo $date_doa ?>"><?php echo $date ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $date ?>"><?php echo $date ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-1">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-month_doa"><?php echo 'Month' ?></label>
							<select name="month_doa" id="month_doa" class="form-control">
		                  		<?php for ($month=1; $month<=12; $month++) { ?>
		                  			<?php if($month == $month_doa){ ?>
		                  				<option selected="selected" value="<?php echo $month_doa ?>"><?php echo $month ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $month ?>"><?php echo $month ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>
						</div>
						<div class="col-sm-2">
							<label style="font-size: 16px;" class="col-sm-2 control-label" for="input-year_doa"><?php echo 'Year' ?></label>
							<select name="year_doa" id="year_doa" class="form-control">
		                  		<?php for ($year=1950; $year<=2020; $year++) { ?>
		                  			<?php if($year == $year_doa){ ?>
		                  				<option selected="selected" value="<?php echo $year_doa ?>"><?php echo $year ?></option>
		                  			<?php } else { ?>
		                  				<option value="<?php echo $year ?>"><?php echo $year ?></option>
		                  			<?php } ?>
		                  		<?php } ?>
			                </select>

						</div>
		   			</div>
				</form>
		  	</div>
	  	</div>
		<div class="form-group">
	  		<div class="col-sm-5">
				<button style="margin-top: 0px;margin-bottom: 2px;margin-left: 170px;font-size: 20px; " type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary col-sm-5"><i >Save</i></button>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
.even{
 background-color:#cccccc;
 
}
.odd{
 background-color:white;
}
</style>
</div>
<script type="text/javascript">
	$(".form_datetime").datepicker({
		dateFormat: 'dd-mm-yy',			
  	});

// Contact search

$('input[name=\'contact\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/reception/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            value: item.contact_no,
            contact_no: item.contact_no,
            name: item.name,
            address: item.address,
            email: item.email,
            date_dob: item.date_dob,
            date_doa: item.date_doa,
            month_dob: item.month_dob,
            month_doa: item.month_doa,
            year_dob: item.year_dob,
            year_doa: item.year_doa,


          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
  	console.log(ui.item);
    $('input[name=\'name\']').val(ui.item.name);
    $('input[name=\'contact\']').val(ui.item.contact_no);
    $('input[name=\'address\']').val(ui.item.address);
    $('input[select=\'location\']').val(ui.item.location);
    $('input[name=\'dob\']').val(ui.item.dob);
    $('input[name=\'doa\']').val(ui.item.doa);
    $('input[name=\'email\']').val(ui.item.email);
    loc = ui.item.location;
    date_dob = ui.item.date_dob;
    date_doa = ui.item.date_doa;
    month_dob = ui.item.month_dob;
    month_doa = ui.item.month_doa;
    year_dob = ui.item.year_dob;
    year_doa = ui.item.year_doa;
   // alert(year_doa);

    //$("#location option[selected]").removeAttr("selected"); 
    $("#location option[value="+loc+"]").attr("selected","selected");

    $("#date_doa option[value="+date_doa+"]").attr("selected","selected"); 
    $("#date_dob option[value="+date_dob+"]").attr("selected","selected"); 
    $("#month_dob option[value="+month_dob+"]").attr("selected","selected"); 
    $("#month_doa option[value="+month_doa+"]").attr("selected","selected"); 
    $("#year_dob option[value="+year_dob+"]").attr("selected","selected");     
    $("#year_doa option[value="+year_doa+"]").attr("selected","selected");     


    // $('select[name="location"] option:selected').attr("selected",null);
    // $('select[name="location"] option[value="ui.item.location"]').attr("selected","selected");

    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});


</script>
<?php echo $footer; ?>