<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right" >
				<a style="display: none;"  href="<?php echo $export; ?>" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa"></i> <?php echo 'Export'; ?></a>
				<a style="display: none;" href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button style="display: none;" type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-feedback').submit() : false;"><i class="fa fa-trash-o"></i></button>
	  		</div>
	  		<h1><?php echo $heading_title; ?></h1>
	  		<ul class="breadcrumb" style="display: none;">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<!-- <?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?> -->
	<div class="panel panel-default">
	  	<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
	  	</div>
	  	<div class="panel-body">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-feedback">
		  		<div class="table-responsive">
					<table class="table table-bordered table-hover">
			  			<thead>
							<tr>
				  				<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				  				<td class="text-left">
									<a><?php echo 'Sr.No'; ?></a>
				  				</td>
				  				<td class="text-left">
									<a><?php echo 'Customer Name'; ?></a>
								</td>
				  				<td class="text-left">
									<a><?php echo 'Contact No'; ?></a>
								</td>
				  				<td class="text-left">
									<a><?php echo 'Email'; ?></a>
								</td>
								<td class="text-left">
									<a><?php echo 'Date Of Entry'; ?></a>
								</td>
				  				
				  				<td class="text-right"><?php echo $column_action; ?></td>
							</tr>
							<tr>
			  					<td></td>
			  					<td></td>
			  					<td>
									<div class="input-group">
						  				<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo "Customer Name"; ?>" id="input-filter_name" class="form-control" />
					  				</div>
			  					</td>
			  					<td>
			  						<div class="input-group">
						  				<input type="text" name="filter_contact_no" value="<?php echo $filter_contact_no; ?>" placeholder="<?php echo "Contact No"; ?>" id="input-filter_contact_no" class="form-control" />
					  				</div>
			  					</td>
			  					<td>
			  						<div class="input-group">
						  				<input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo "Email Id"; ?>" id="input-filter_email" class="form-control" />
					  				</div>
			  					</td>
			  					<td>
			  						<div class="input-group">
						  				<input type="text" name="filter_date" value="<?php echo $filter_date; ?>" placeholder="<?php echo "Date"; ?>" id="input-date" class="form-control" />
					  				</div>
			  					</td>
			  					<td>
			  						<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo 'Filter'; ?></button>
			  					</td>
							</tr>
			  			</thead>
			  		<tbody>
					<?php if ($feedbacks){ ?>
				  		<?php $i = 1 ?>
				  			<?php foreach ($feedbacks as $feedback) { ?>
							<tr>
					  			<td class="text-center"><?php if (in_array($feedback, $selected)) { ?>
									<input type="checkbox" name="selected[]" value="<?php echo $feedback['feedback_id']; ?>" checked="checked" />
								<?php } else { ?>
									<input type="checkbox" name="selected[]" value="<?php echo $feedback['feedback_id']; ?>" />
								<?php } ?></td>
								<td class="text-left"><?php echo $i; ?></td>
				  				<td class="text-left"><?php echo $feedback['cust_name']; ?></td>
				  				<td class="text-left"><?php echo $feedback['contact_no']; ?></td>
				  				<td class="text-left"><?php echo $feedback['email']; ?></td>
				  				<td class="text-left"><?php echo $feedback['date']; ?></td>
				  				<td class="text-right"><a href="<?php echo $feedback['edit']; ?>" data-toggle="tooltip" title="<?php echo "View"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
				  				<a style="display: none;" href="<?php echo $feedback['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" ><i class="fa fa-trash-o"></i></a></td>
							</tr>
						<?php $i ++; ?>  
				  	<?php } ?>
					<?php } else { ?>
				  	<tr>
						<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
				  	</tr>
				<?php } ?>
			  </tbody>
			</table>
		  </div>
		</form>
		<div class="row">
		  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {
  	var url = 'index.php?route=catalog/feedback_report&token=<?php echo $token; ?>';

  	var filter_name = $('input[name=\'filter_name\']').val();
  	if (filter_name) {
	  	url += '&filter_name=' + encodeURIComponent(filter_name);
	}
  	

  	var filter_contact_no = $('input[name=\'filter_contact_no\']').val();
  	if (filter_contact_no) {
		url += '&filter_contact_no=' + encodeURIComponent(filter_contact_no);
	}

	var filter_email = $('input[name=\'filter_email\']').val();
	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

	var filter_date = $('input[name=\'filter_date\']').val();
	if (filter_date) {
		url += '&filter_date=' + encodeURIComponent(filter_date);
	}
  	
  	location = url;
});

$('input[name=\'filter_contact_no\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/feedback_report/autocomplete&token=<?php echo $token; ?>&filter_contact_no=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['contact_no'],
			value: item['contact_no']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_contact_no\']').val(item['label']);
	
  }
});


$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/feedback_report/autocompletename&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['name'],
			value: item['name']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_name\']').val(item['label']);
	
  }
});

$('input[name=\'filter_email\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/feedback_report/autocompleteemail&token=<?php echo $token; ?>&filter_email=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['email'],
			value: item['email']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_email\']').val(item['label']);
	
  }
});

$('input[name=\'filter_date\']').autocomplete({
  'source': function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/feedback_report/autocompletedate&token=<?php echo $token; ?>&filter_date=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {
		response($.map(json, function(item) {
		  return {
			label: item['date'],
			value: item['date']
		  }
		}));
	  }
	});
  },
  'select': function(item) {
	$('input[name=\'filter_date\']').val(item['label']);
	
  }
});


$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<?php echo $footer; ?>