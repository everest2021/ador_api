<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
	    <div class="container-fluid">
	        <div class="pull-right">
		       <button id="save" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	           <h1><?php echo $heading_title; ?></h1>
	           <ul class="breadcrumb">
		           <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		            <?php } ?>
	           </ul>
	    </div>
  </div>
  <div class="container-fluid">
	   <?php if ($error_warning) { ?>
	   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	       <button type="button" class="close" data-dismiss="alert">&times;</button>
	   </div>
	    <?php } ?>
	   <div class="panel panel-default">
	       <div class="panel-heading">
		       <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	        </div>
	   <div class="panel-body">
		   <form  method="post" enctype="multipart/form-data" id="form-sport" class="form-horizontal">
		   	<div class="form-group ">
			   <label class="col-sm-3 control-label"><?php echo 'Item Code'; ?></label>
			    <div class="col-sm-2">
			        <input type="text" name="item_code" value="<?php echo $item_code; ?>" readonly="readonly" placeholder="<?php echo 'Code'; ?>" class="form-control" />
		            <?php if ($error_code) { ?>
		            	<div class="text-danger"><?php echo $error_code; ?></div>
		            <?php } ?>
			    </div>
			    <label class="col-sm-1 control-label"><?php echo 'Manage Stock'; ?></label>
		   		<div class="col-sm-1" >
		        	<select name="manage_stock" id="input-manage_stock" class="form-control">
			          	<?php foreach($manage_stocks as $mkey => $mvalue){ ?>
			          		<?php if($mkey == $manage_stock){ ?>
				      			<option value="<?php echo $mkey ?>" selected="selected"><?php echo $mvalue; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $mkey ?>"><?php echo $mvalue ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		   		</div>
			    <?php if($show_in_purchase == '1'){ ?>
      			<label class="col-sm-2 control-label"><?php echo 'Show In Purchase'; ?></label>
      			<div class="col-sm-1">
					<input type="checkbox" name="show_in_purchase" value="1" checked ="checked"  style="margin-right:8%"> <br>
				</div>
				<?php } else { ?>
					<label class="col-sm-2 control-label"><?php echo 'Show In Purchase'; ?></label>
				<div class="col-sm-1">
					<input type="checkbox" id="show_in_purchase" checked ="checked" name="show_in_purchase" value="1" style="margin-right:8%;">  <br />
				</div>
				<?php } ?>


				<label class="col-sm-2 control-label"></label>
					<div class="col-sm-2">
			        <a href="<?php echo $vendor ?>" class="btn btn-primary">Vendor</a>
			        <a href="<?php echo $bom ?>" class="btn btn-primary">BOM</a>
			        </div>
		    </div> 
        	<div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'Item Name'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="item_name" value="<?php echo $item_name; ?>" placeholder="<?php echo 'Item Name'; ?>" class="form-control" />
		            <?php if ($error_name) { ?>
		            	<div class="text-danger"><?php echo $error_name; ?></div>
		             <?php } ?>
		        </div>
        		<label class="col-sm-3 control-label"><?php echo 'UMO'; ?></label>
		   		<div class="col-sm-3" >
		        	<select name="uom" id="input-uom" class="form-control">
		            	<!-- <option ><?php echo "Please Select" ?></option> -->
			          	<?php foreach($item_umos as $skey){ ?>
			          		<?php if($skey['unit_id'] == $uom){ ?>
				      			<option value="<?php echo $skey['unit_id'] ?>" selected="selected"><?php echo $skey['unit']; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $skey['unit_id'] ?>"><?php echo $skey['unit'] ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		        	<input type="hidden" name="unit_name" value='<?php echo $unit_name ?>'  id="unit_name"/>
		   		</div>
	        </div>
	        <div class="form-group ">
		        <label class="col-sm-3 control-label"><?php echo 'Barcode'; ?></label>
		        <div class="col-sm-1">
		            <input type="text" name="bar_code" value="<?php echo $bar_code; ?>" placeholder="<?php echo 'Item Name'; ?>" class="form-control" />
		        </div>
		        <div class="col-sm-2">
		            <input type="text" name="bar_code_1" value="<?php echo $bar_code_1; ?>" placeholder="<?php echo 'Item Name'; ?>" class="form-control" />
		        </div>
		        <label class="col-sm-3 control-label"><?php echo 'Purchase Rate'; ?></label>
		        <div class="col-sm-3">
		            <input type="text" name="purchase_rate" value="<?php echo $purchase_rate; ?>" placeholder="<?php echo 'Purchase Rate'; ?>" class="form-control" />
		        </div>
	        </div>  
	    	<div class="form-group ">
    			<label class="col-sm-3 control-label"><?php echo 'Item Type'; ?></label>
		   		<div class="col-sm-3" >
		        	<select name="item_type" id="input-item_type" class="form-control">
		            	<!-- <option ><?php echo "Please Select" ?></option> -->
			          	<?php foreach($item_types as $skey => $svalue){ ?>
			          		<?php if($skey == $item_type){ ?>
				      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		   		</div>
		 		<label class="col-sm-3 control-label"><?php echo 'Reorder level'; ?></label>
        		<div class="col-sm-3">
            		<input type="text" name="reorder_level" value="<?php echo $reorder_level; ?>" placeholder="<?php echo 'Reorder Level'; ?>" class="form-control" />
        		</div>
			</div>
			<div class="form-group ">
    			<label class="col-sm-3 control-label"><?php echo 'Item Category'; ?></label>
		   		<div class="col-sm-3" >
		        	<select name="item_category" id="input-item_category" class="form-control">
		            	<!-- <option ><?php echo "Please Select" ?></option> -->
			          	<?php foreach($item_categorys as $skey => $svalue){ ?>
			          		<?php if($skey == $item_category){ ?>
				      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		   		</div>
				<label class="col-sm-3 control-label"><?php echo 'Store Name'; ?></label>
		   		<div class="col-sm-3" >
		        	<select name="store_id" id="store_id" class="form-control">
		            	<!-- <option ><?php echo "Please Select" ?></option> -->
			          	<?php foreach($store_names as $skey => $svalue){ ?>
			          		<?php if($skey == $store_id){ ?>
				      			<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		        	<input type="hidden" name="store_name" id="store_name" value="<?php echo $store_name; ?>" size="50" />
		   		</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 control-label"><?php echo 'Category'; ?></label>
		   		<div class="col-sm-3" >
		        	<select name="category_id" id="category_id" class="form-control">
			          	<?php foreach($stockcategory_data as $gkey => $gvalue) { ?>
                            <?php if ($gkey == $category_id) { ?>
				      			<option value="<?php echo $gkey ?>" selected="selected"><?php echo $gvalue; ?></option>
			          		<?php } else { ?>
				      			<option value="<?php echo $gkey ?>"><?php echo $gvalue ?></option>
			          		<?php } ?>
			          	<?php } ?>
		        	</select>
		        	<input type="hidden" name="category" id="category" value="<?php echo $category; ?>" size="50" />
		   		</div>
				<label class="col-sm-3 control-label" for="input-n_title" ><?php echo 'Tax'; ?></label>
				<div class="col-sm-2">
					<select name="tax_value" id="tax_value" class="form-control">
	                  <?php foreach($tax_data as $gkey => $gvalue) { ?>
	                    <?php if ($gkey == $tax_value) { ?>
	                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
	                    <?php } else { ?>
	                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
	                    <?php } ?>
	                  <?php } ?>
	                </select>
	                <input type="hidden" name="tax" id="tax" value="<?php echo $tax; ?>" size="50" /></br>
              	</div>
			</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div> 
<?php echo $footer; ?>
<script type="text/javascript">

$('#category_id').on('change', function() {
  department_name = $('#category_id option:selected').text();
  $('#category').val(department_name);
});

department_name = $('#category_id option:selected').text();
  $('#category').val(department_name);

$('#store_id').on('change', function() {
  store_name = $('#store_id option:selected').text();
  $('#store_name').val(store_name);
});

store_name = $('#store_id option:selected').text();
$('#store_name').val(store_name);

$('#input-uom').on('change', function() {
  unit_name = $('#input-uom option:selected').text();
  $('#unit_name').val(unit_name);
});

unit_name = $('#input-uom option:selected').text();
  $('#unit_name').val(unit_name);

$('#tax_value').on('change', function() {
  tax_value = $('#tax_value option:selected').text();
  $('#tax').val(tax_value);
 });


  $("#save").click(function(){
	$('#save').attr('onclick','').unbind('click');
    action1 = '<?php echo $action; ?>';
	action1 = action1.replace("&amp;", "&");
	action1 = action1.replace("&amp;", "&");
	//alert(action1);
	$('#form-sport').attr("action", action1); 
	$('#form-sport').submit();
	//alert('inn');
	return false;
  });

function submit_fun(){

	action1 = '<?php echo $action; ?>';
	action1 = action1.replace("&amp;", "&");
	action1 = action1.replace("&amp;", "&");
	//alert(action1);
	$('#form-sport').attr("action", action1); 
	$('#form-sport').submit();
	//alert('inn');
	return false;
}
 
  </script>