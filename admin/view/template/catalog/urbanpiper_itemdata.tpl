<?php echo $header; ?>
<div id="content" class="sucess">
	<div class="page-header">
		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
		<form action="<?php echo $action; ?>" id="payform" method="post">
			<div class="container-fluid">
					<div class="col-sm-4">
				  		<label style="font-size: 20px;color: black; font-weight: unset;">Customer Name: <?php echo $final_datas['customer_data']['name']?></label>
				  	</div>
				  	<div class="col-sm-4">
				  		<label style="font-size: 20px; color: black;font-weight: unset;">Mobile No: <?php echo $final_datas['customer_data']['phone_number']?></label>
				  	</div>
				  	<div class="col-sm-4">
				  		<label style="font-size: 20px;color: black;font-weight: unset;">Order Type : <?php echo ucwords($enable_delivery) ?></label>
				  	</div>

				  	<div class="col-sm-4">
				  		<label style="font-size: 20px;color: black;font-weight: unset;"><?php echo ucwords($online_channel) ?> Order ID : <?php echo $zomato_order_id; ?></label>
				  	</div>
				
				  	<div class="col-sm-4" >
				  		<label style="font-size: 20px; color: black;font-weight: unset;"><?php echo ucwords($online_channel) ?> Order Type: <?php echo strtoupper($zomato_order_type) ?> </label>
				  	</div>
				  	<div class="col-sm-4">
				  		<label style="font-size: 20px;color: black;font-weight: unset;"><?php echo ucwords($online_channel) ?> Delivery Type : <?php echo ucwords($online_deliery_type) ?></label>
				  	</div>


				<table class="table" style="background-color: #fff;width: 96%;margin-left: 2%;margin-top: 3%;font-size: 100%;cursor: default;">
				  	<input type="hidden" id="order_id" name="order_id" value="<?php echo $order_id ?>">
				  	<thead  style="background-color: #e72b0b; color: #fff; font-family: monospace; font-size: 130%;">
				    	<tr>
				      		<th scope="col">Order No</th>
				      		<th scope="col">Item Name</th>
				      		<th scope="col">Qty</th>
				      		<th scope="col" colspan="4">Rate</th>
				      		<th scope="col">Amt</th>
				      		<th scope="col">GST</th>
				      		<th scope="col">GST Value</th>
				      		<th scope="col">Total</th>
				      		
				      		
				    	</tr>
				  	</thead>
		  			<tbody>
		  				<?php foreach ($final_datas['item_data'] as $pkey => $pvalue) { ?>
						    <tr  style="background-color: #fff;color: #000;font-family: monospace;font-size: 120%;cursor: pointer;">
						      	<td><?php echo $pvalue['order_id'] ?></td>
						      	<td><?php echo $pvalue['item_name'] ?></td>
						      	<td><?php echo $pvalue['item_quantity'] ?></td>
						      	<td colspan="4"><?php echo $pvalue['item_unit_price'] ?></td>
						      	<td><?php echo $pvalue['subtotal'] ?></td>
						      	<td><?php echo $pvalue['gst'] ?></td>
						      	<td><?php echo $pvalue['gst_val'] ?></td>
						      	<td><?php echo $pvalue['grand_tot'] ?></td>
						      	
						    </tr>

		  				<?php } ?>
		  				<tr style="font-size: 20px;">
		  					<td style="color: black;">T.Items</td>
		  					<td style="color: black;"> <?php echo $total_item; ?> </td>
		  					<td style="color: black;">T.Qty</td>
		  					<td style="color: black;" ><?php echo $total_qty; ?></td>
		  					<?php if(!empty($ftotalvalue != '')) { ?>
		  						<td style="color: black;white-space:pre" colspan="3">Discount: <?php echo $ftotalvalue ?></td>
		  					<?php }else { ?>
		  						<td style="color: black;white-space:pre" colspan="3"></td>
		  					<?php } ?>
		  					<td style="color: black;">Charge</td>
		  					<td style="color: black;"  ><?php echo $total_charge; ?></td>
		  					<td  style="color: black;text-align: right;">G.Total</td>
		  					<!-- <td style="color: black;text-align: left;" ><?php echo round($grand_total) ?></td> -->
		  					<td style="color: #208f13;text-align: left;" ><?php echo $grand_total ?></td>
						</tr>
						
		  			</tbody>
				</table>
				<?php if ($coupon != '') { ?>
					<div class="col-sm-12">
			  			<label style="font-size: 20px; color: black;">Coupon: <?php echo $coupon ?></label>
			  		</div>
				<?php } ?> 
				<?php if ($delivery_charge > 0.00) { ?>
					<div class="col-sm-12">
			  			<label style="font-size: 20px; color: black;">Delivery Charge: <?php echo $delivery_charge ?></label>
			  		</div>
				<?php } ?> 
				 <?php if ($total_charge > 0.00) { ?>
					<div class="col-sm-12">
			  			<label style="font-size: 20px; color: black;">Packaging Charge: <?php echo $total_charge ?></label>
			  		</div>
				<?php } ?> 
				<?php if ($order_instructions != '') { ?>
					<div class="col-sm-12">
				  		<label style="font-size: 20px; color: green;">Order Instruction: <?php echo $order_instructions ?></label>
				  	</div>
				  	
				<?php } ?> 

				 <?php if ($zomato_rider_otp != '') { ?>
					<div class="col-sm-12">
			  			<label style="font-size: 20px; color: black;">Zomato Otp: <?php echo $zomato_rider_otp ?></label>
			  		</div>
				<?php } ?>  

				<?php if ($Merchant_Sponsored_Discount > 0) { ?>
					<!-- <div class="col-sm-6">
				  		<label style="font-size: 20px; color: black;">(Aggregator Discount: <?php echo $zomato_discount ?>)</label>
				  	</div> -->
				  	<div class="col-sm-12">
				  		<label style="font-size: 20px; color: black;">(Merchant Sponsored Discount: <?php echo $Merchant_Sponsored_Discount ?>)</label>
				  	</div>
				<?php } ?>  

				<?php if($status == 'Placed') {?>
					<div class="col-sm-offset-0">
						<div class="col-sm-6">
							<?php if($show_accept == 0) { ?>
					    		<a  onclick="accept_btn()" id="accept-input"  class="btn btn-block" style="background: green;color: #fff;font-size: 25px;">Accept</a>
					    	<?php } else { ?>
					    		<a  class="btn btn-block" style="background: green;color: #fff;font-size: 25px;">Disabled Acpt</a>
					    	<?php } ?>
						</div>
						<div class="col-sm-6">
							<?php if($show_cancelled == 0) { ?>
					    		<a  onclick="reject_btn()" id="reject-input"  class="btn btn-block" style="background: #e72b0b;color: #fff;font-size: 25px;">Reject</a>
					    	<?php } else { ?>
								<a   id="reject-input"  class="btn btn-block" style="background: #e72b0b;color: #fff;font-size: 25px;">Disabled Reject</a>
							<?php } ?>
						</div>
					</div>
				<?php } elseif($status == 'Acknowledged' || ($status == 'Dispatched')) { ?>
					<div class="col-sm-2" style="float: right;">
						<a  onclick="bill_print(<?php echo $order_id ?>)" id="bill_print"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Bill Print</a>
					</div>

					<div class="col-sm-3" style="float: right;">
						<?php if($show_cancelled == 0) { ?>
							<a  onclick="order_ready(<?php echo $order_id ?>)" id="order_ready"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Order Ready</a>
						<?php } else { ?>
							<a  id="order_ready"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Disabled Order Ready</a>
						<?php } ?>
					</div>
				<?php } elseif(($status == 'Food Ready') || ($status == 'Dispatched') || ($status == 'Completed')) { ?>
					<div class="col-sm-2" style="float: right;">
						<a  onclick="bill_print(<?php echo $order_id ?>)" id="bill_print"  class="btn btn-block" style="background: red;color: #fff;font-size: 18px;">Bill Print</a>
					</div>
				<?php } elseif($status == 'Cancelled') { ?>
					<div style="margin-left: 74%; display: grid;" class="col-sm-12">
					    <img src="cancel.JPG" width="100px" />
					    <?php if($swiggy_cancel_msg != '') { ?>
					    	<div style="vertical-align: middle;width: calc(100% - 100px); font-size: 17px;font-weight: 600;color: red ;"><?php echo $swiggy_cancel_msg ?></div>
					    <?php } else { ?>
					    	<div style="vertical-align: middle;width: calc(100% - 100px); font-size: 17px;font-weight: 600;color: red ;">Order is Cancelled</div>
					    <?php } ?>
					    	
					</div>
				<?php } ?>
				<br>
			  	<div class="col-sm-12 rjreason">
			     	<div class="col-sm-12" id="reject_reason" style="margin-top: 3%; ">
				 		<label for="reject_re" style="color: #000;font-size: 25px;">Reject Reason:</label>
			      	</div>
			     </div>
			     <div class="col-sm-12 rjreason" >
						<div class="col-sm-6" >
				      		<select class="form-control" id="reject_res" name="reject_reason" style="font-size: 22px;height: 50px;">
					    		<option value='' selected="selected"><?php echo "Please Select" ?></option>
					    		
					    		<?php foreach($reject_reason as $rkey => $rvalue) { ?>
						    		<option value="<?php echo $rkey ?>" style="font-size: 20px;"><?php echo $rvalue?></option>
					    		<?php } ?>
				      		</select>
				      	</div>
				     

			      	<div class="col-sm-6"  id="ok_btns" >
						<a  onclick="ok_btn()" id="ok-input"  class="btn btn-small" style="background: green;color: #fff;font-size: 25px;">OK</a>
					</div>
			  	</div>
			  	<br />
			</div>
		</form>
		<div class="col-sm-12">
			<div class="col-sm-6 form-group" style="font-family: monospace;">
				<div class="table table-sm">
					<table id="discount" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
						<thead>
							<tr>
								<td class="text-left" colspan="3">Order History</td>
							</tr>
							<tr>
								<td class="text-left">Date</td>
								<td class="text-left">Status</td>
								<td class="text-left">Message</td>
							</tr>
						</thead>
						<?php  if(!empty($final_datas['urbanpiper_order_status'])){  ?>
							<tbody>
								<?php foreach ($final_datas['urbanpiper_order_status'] as $rkey => $rivalue) { 
								?>
									<tr class = "<?php echo $rivalue['order_color']; ?>">
										<td class="text-left"> <?php echo $rivalue['timestamp']; ?> </td>
										<td class="text-left"> <?php echo $rivalue['new_state']; ?></td>
										<td class="text-left"> <?php echo $rivalue['message'];?> </td>
									</tr>
								<?php   }  ?>
							</tbody>
						<?php  }  else { ?>
							<td class="text-center" colspan="3"> No Result</td>
						<?php }  ?>
					</table>
				</div>
			</div>
			<div class="col-sm-6 form-group" style="font-family: monospace;">
				<div class="table table-sm">
					<table id="discount" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
						<thead>
							<tr>
								<td class="text-left" colspan="3">Rider History</td>
							</tr>
							<tr>
								<td class="text-left">Rider Name</td>
								<td class="text-left">Rider Contact No</td>
								<td class="text-left">Status</td>
							</tr>
						</thead>
						<?php  if(!empty($final_datas['rider_data'])){  ?>
							<tbody>
								<?php foreach ($final_datas['rider_data'] as $rkey => $ridervalue) { 
									
								?>
									<tr class="<?php echo $ridervalue['order_color'];?>">
										<td class="text-left"> <?php echo $ridervalue['rider_name']; ?> </td>
										<td class="text-left"> <?php echo $ridervalue['rider_numbers']; ?></td>
										<td class="text-left"> <?php echo $ridervalue['rider_status'];?> </td>
									</tr>
								<?php }  ?>
							</tbody>
						<?php  }  else { ?>
							<td class="text-center" colspan="3"> No Result</td>
						<?php }  ?>
					</table>
				</div>
			</div>
		</div>
	</div>
<style type="text/css">
	.notification-message-unread {
	 	color:white;
	    background-color: red;
	  animation: blinker 7s linear infinite;
	}

	@keyframes blinker {
	 0%, 49% {
	    color:white;
	    background-color:green;
	  }
	  50%, 100% {
		background-color: red;
		 color:white;
			}
	}
	.myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;
    }

    .ui-button.cancelButton {
	    border: 1px solid #aaaaaa
	    ;
	    color: #FF0000
	    ;
	}

	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>	

	<script type="text/javascript">
	$(document).on('keydown', '#cash', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
			save();
		  }
		//console.log(code);
		//alert(code);
	});

	$(document).ready(function() {
		$('#prep_time').hide();
		//$('#reject_reason').hide();
		$('.rjreason').hide();
		$('#ok_btns').hide();


	});

	function bill_print(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/urbanpiper_itemdata/bill_print&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				//if(json.info == 1){
				if(json){
					$('.sucess').html('');
					$('.sucess').append(json.done);
					location.reload();
				}
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}

	function order_ready(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/urbanpiper_itemdata/order_ready&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				$('.sucess').html('');
				$('.sucess').append(json.done);
				/*if(json.success){
					alert(json.success);
					$('.sucess').html('');
					$('.sucess').append(json.done);
				} else if(json.errors) {
					alert(json.errors);
					$('.sucess').html('');
					$('.sucess').append(json.done);
				} else {
					alert("somthing is wrong");
				}*/
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}

	/*function accept_btn(){
		$('#prep_time').show();
		$('#ok_btns').show();
		$('#reject_reason').hide();
		$('#reject_re').html('');


		//return false;
	}*/

	function reject_btn(){
		$('.rjreason').show();
		$('#ok_btns').show();
		$('#prep_time').hide();
		
		//return false;
	}

	function ok_btn() {
		order_id = $('#order_id').val();
		reject_re = $('#reject_res').val();
		//alert(reject_re);
		if(reject_reason != ''){
			$(document).ajaxSend(function() {
				$("#overlay").fadeIn(200);　
			});
			urll = 'index.php?route=catalog/urbanpiper_itemdata/reject_order&token=<?php echo $token; ?>&order_id=' +order_id+'&reject_reason='+encodeURIComponent(reject_re);
			$.ajax({
				type: "POST",
			  	url: urll,
			  	dataType: 'json',
			  	success: function(json) {
			  		$('.sucess').html('');
					$('.sucess').append(json.done);
			  		//alert(json);
			  // 		if(json.info == 1){
			  // 			alert(json.success);
					// 	$('.sucess').html('');
					// 	$('.sucess').append(json.done);
					// } else if(json.errors) {
					// 	alert(json.errors);
					// 	$('.sucess').html('');
					// 	$('.sucess').append(json.done);
					// } else {
					// 	alert('somthing is wrong !!!');
					// }
			  	}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
		} else {
			alert("Please Select Reason For Cancel Order")
		}
	}

	function accept_btn(){
		order_id = $('#order_id').val();
		/*order_from = $('#order_from').val();
		pre_timess = $('#pre_timess').val();
		reject_re = $('#reject_res').val();*/
		reject_re = $('#reject_res').val();
		
		if(reject_re == ''){
			$(document).ajaxSend(function() {
				$("#overlay").fadeIn(200);　
			});
			$.ajax({
				type: "POST",
			  	url: 'index.php?route=catalog/urbanpiper_itemdata/update_order&token=<?php echo $token; ?>&order_id='+order_id,
			  	dataType: 'json',
			  	success: function(json) {
			  		//console.log(json);
			  		$('.sucess').html('');
					$('.sucess').append(json.done);
			  		//console.log(json);
			  // 		if(json.info == 1){
			  // 			alert(json.success);
					// 	$('.sucess').html('');
					// 	$('.sucess').append(json.done);
					// } else if( json.info == 2 ) {
					// 	alert(json.reason);
					// } else {
					// 	alert("somthing is wrong");
					// }
			  	}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
		} 
	}
	</script>
</div>
	