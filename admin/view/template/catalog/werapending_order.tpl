<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" style="padding-bottom: 0px;">
  <div class="container-fluid" style="padding: 0px;">
  	<div class="table-wrap">
  		<br>
  		<div class="col-sm-12">
  			<div class="col-sm-2">
  				<a style="float: right;background-color: #1a6662" onclick="addstore()" class="btn btn-success"><i class="fa fa-home" aria-hidden="true"></i>  Adding/Updating stores</a>
  			</div>
  			<div class="col-sm-8">
	  			<a style="float: right;background-color: #e72b0b" onclick="store_onoff()" data-toggle="tooltip" title="<?php echo ' Store ON/OFF'; ?>" class="btn btn-primary"> Store ON/OFF</a>
	  			
				<label style="padding-left: 20px;display: none;"> Store ON/OFF</label>
				<label class="switch" style="padding-left: 20px;display: none;">
					<?php if($store_status == '1'){ ?>
				  		<input type="checkbox" value="1"  checked="checked" name="store_status" id="store_status">
				 	<?php } else { ?>
				  		<input type="checkbox" value="0"  name="store_status" id="store_status">
				 	<?php } ?>
				  <span class="slider" style="border-radius: 40px;"></span>
				</label>
				&nbsp;&nbsp;&nbsp;
			</div>
			<div class="col-sm-2">
  				<a style="float: right;" onclick="pullorder()" class="btn btn-success">Pull Pending Orders</a>
  			</div>
  		</div>
  		<br>
  		<div id="overlay">
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
		</div>
  		<table class="table" style="background-color: #fff;width: 96%;margin-left: 2%;margin-top: 3%;font-size: 149%;cursor: default;">
		  	<thead  style="background-color: #e72b0b; color: #fff; font-family: monospace; font-size: 130%;">
		    	<tr>
		      		<th scope="col">Sr.No</th>
		      		<th scope="col">Order No</th>
		      		<th scope="col">Order Date</th>
		      		<th scope="col">Order Delivery Time</th>
		      		<th scope="col">Payment Mode</th>
		      		<th scope="col">Order Type</th>
		      		<th scope="col" colspan="2">Grand Total</th>
		    	</tr>
		  	</thead>
  			<tbody>
  				<?php foreach ($pending_orders as $pkey => $pvalue) { ?>
  					<?php if($pvalue['status'] == 'Accept') { ?>
  						<?php 	$colors = 'green'; 
  							$font_color = '#fff';
  						?>
  					<?php } else { ?>
  						<?php 	$colors = 'white'; 
  							$font_color = '#000';

  						?>

  					<?php } ?>
					    <tr class="dialog"  style="background-color: <?php echo $colors; ?>;color:<?php echo $font_color; ?>;font-family: monospace;font-size: 120%;cursor: pointer;">
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['sr_no'] ?></td>
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_id'] ?></td>
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_time'] ?></td>
					      	<!-- <td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_from'] ?></td> -->
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['delivery_time'] ?></td>
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['payment_mode'] ?></td>
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['order_type'] ?></td>
					      	<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"><?php echo $pvalue['gross_amount'] ?></td>
					      	<?php if($pvalue['status'] == 'Food Ready') { ?>
					      		<td >
					      			<a onclick="order_ready(<?php echo $pvalue['order_id'] ?>)" class="btn btn" style="background-color: red;color: #fff;font-size: 13px;"> Order Ready</a>
					      			<a onclick="bill_print(<?php echo $pvalue['order_id'] ?>)"  class="btn btn" style="background-color: red;color: #fff;font-size: 13px;"> Print Bill</a>
					      		</td>
					      	<?php } else { ?>
					      		<td onclick="order_dat(<?php echo $pvalue['order_id'] ?> )"></td>
					      	<?php } ?>

					    </tr>


  				<?php } ?>
  			</tbody>
		</table>
		<div id="dialog" title="Accept Screen">
			
		</div>
		<div id="dialog_storeonoff" title="Store ON / OFF">
			
		</div>
	</div>
	</div>
</div>
<style type="text/css">
	.myTitleClass .ui-dialog-titlebar {
          background: #e72b0b;
          color: #fff;
    }

    .ui-button.cancelButton {
	    border: 1px solid #aaaaaa
	    ;
	    color: #FF0000
	    ;
	}

	#overlay{	
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
	}

	.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #09287d solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	0% { 
		transform: rotate(0deg); 
	}
	100% { 
		transform: rotate(359deg); 
	}
}
.is-hide{
	display:none;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}






.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 40px;
}

input:checked + .slider {
  background-color: #00891d;
}

input:focus + .slider {
  box-shadow: 0 0 1px #00891d;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


</style>
<script type="text/javascript">
	//$('#dialog').find('.ui-button').addClass('cancelButton');

	function order_ready(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
			type: "POST",
			url: 'index.php?route=catalog/werapending_order/order_ready&token=<?php echo $token; ?>&order_id='+order_id,
			dataType: 'json',
			data: {"data":"check"},
			success: function(json){
				if(json.info == 1){
					// $('.sucess').html('');
					// $('.sucess').append(json.done);
					location.reload();
				} else if(json.info == 2) {
					alert(json.reason);
				} else {
					alert('Something is wrong');
				}
			}
		}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
	}

	function bill_print(order_id){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/werapending_order/bill_print&token=<?php echo $token; ?>&order_id='+order_id,
				dataType: 'json',
				data: {"data":"check"},
				success: function(data){
				console.log(data); 
					location.reload();
				}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});
		}

	function order_dat(order_id){
		//alert(order_id);

		htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/urbanpiper_itemdata&token=<?php echo $token; ?>&order_id='+order_id+'" width="100%" height="100%"></iframe>';
			dialog.dialog("open");
			$('#dialog').html(htmlc)
   
	}

	function pullorder(){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/werapending_order/getList&token=<?php echo $token; ?>',
				dataType: 'json',
				data: {"data":"check"},
				success: function(data){
				console.log(data); 
					location.reload();
				}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});

	}

	function addstore(){
		$(document).ajaxSend(function() {
			$("#overlay").fadeIn(200);　
		});
		$.ajax({
    			type: "POST",
				url: 'index.php?route=catalog/werapending_order/addstore&token=<?php echo $token; ?>',
				dataType: 'json',
				data: {"data":"check"},
				success: function(json){
					if (json['error']) {
						for (i in json['error']) {
							alert(json['error'][i]);
						}
					}
					if (json['success']) {
						alert(json['success']);
					}
				
				}
			}).done(function() {
				setTimeout(function(){
					$("#overlay").fadeOut(50);
				},50);
			});

	}


	function store_onoff(){
		htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/store_onoff&token=<?php echo $token; ?>&status='+status+'" width="100%" height="100%"></iframe>';
		dialog_storeonoff.dialog("open");
		$('#dialog_storeonoff').html(htmlc)
	}
	
  	// $(function() {
   //  	$( ".dialog" ).click(function(){
   //  		var eee = $('.dialog').val();
   //  		//alert(eee); 
   //  		$.ajax({
   //  			type: "POST",
			// 	url: 'index.php?route=catalog/werapending_order/order_info_datas&token=<?php echo $token; ?>',
			// 	dataType: 'json',
			// 	data: {"data":"check"},
			// 	success: function(data){ 
			// 	//alert('inn');      
			// 		dialog.dialog("open");
			// 	}
			// });
   //  	});
  	// });


  	/*$('#store_status').on('click', function() {
        if($(this).prop("checked") == true){
        	status = 1;
        	//alert(status);
        	htmlc = '<iframe id="itemdata" style="border: 0px;" src="index.php?route=catalog/store_onoff&token=<?php echo $token; ?>&status='+status+'" width="100%" height="100%"></iframe>';
			dialog_storeonoff.dialog("open");
			$('#dialog_storeonoff').html(htmlc)
            //alert("Checkbox is checked.");
        }
        else if($(this).prop("checked") == false){
        	status = 0;
            alert("Checkbox is unchecked.");
        }
    
    });*/

		/*stat = $('#store_status').val();
		alert(stat);*/

  	

  dialog = $("#dialog").dialog({
	autoOpen: false,
	height: 600,
	width: 900,
	modal: true,
	dialogClass: 'myTitleClass',
	buttons: {

	},
});

  dialog_storeonoff = $("#dialog_storeonoff").dialog({
	autoOpen: false,
	height: 600,
	width: 900,
	modal: true,
	dialogClass: 'myTitleClass',
	buttons: {

	},
});

  function closeIFrame(){
      location.replace('index.php?route=catalog/werapending_order&token=<?php echo $token; ?>');
	}

</script>
<?php echo $footer; ?>