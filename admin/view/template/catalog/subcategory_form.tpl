<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-sport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'Category Name'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo 'Category Name'; ?>" class="form-control" />
                            <?php if ($error_name) { ?>
                            <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label"><?php echo 'Parent Category'; ?></label>
                        <div class="col-sm-3">
                            <select name="parent_id" id="input-parent_id" class="form-control">
                                <option value="0" ><?php echo "Root category" ?></option>
                                <?php foreach($categorys as $skey => $svalue){ ?>
                                    <?php if($skey == $parent_id){ ?>
                                        <option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="parent_category" id="parent_category" value="<?php echo $parent; ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Colour'; ?></label>
                        <div class="col-sm-3">
                            <?php if($colour != ''){ ?>
                                <input type="text" name="colour" value="<?php echo $colour; ?>" placeholder="<?php echo $colour; ?>" id="colorSelector" class="form-control" style="background-color: <?php echo $colour; ?>" />
                            <?php } else { ?>
                                <input type="text" name="colour" value="<?php echo $colour; ?>" placeholder="<?php echo $colour; ?>" id="colorSelector" class="form-control" />
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label"><?php echo 'Image'; ?></label>
                        <div class="col-sm-3">
                              <input readonly="readonly" type="text" name="photo" value="<?php echo $photo; ?>" placeholder="<?php echo 'Image'; ?>" id="input-photo" class="form-control" />
                            <input type="hidden" name="photo_source" value="<?php echo $photo_source; ?>" id="input-photo_source" class="form-control" />
                                <span class="input-group-btn">
                                <button type="button" id="button-photo" data-loading-text="<?php echo 'Please Wait'; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo 'Upload Image'; ?></button>
                                </span>
                                <?php if($photo_source != ''){ ?>
                            <br/ ><a target="_blank" style="cursor: pointer;" id="photo_source" class="thumbnail" href="<?php echo $photo_source; ?>">View Image</a>
                            <?php } ?>
                        </div>
                    </div>
                     <div class="form-group required">
                        <label class="col-sm-3 control-label"><?php echo 'Sort Order For Integration'; ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="sort_order_api" value="<?php echo $sort_order_api; ?>" placeholder="<?php echo 'Sort Order For Integration'; ?>" class="form-control" />
                        </div>
                    </div>
                    <?php if($valid == 1) { ?>
                        <div class="form-group">

                            
                               
                             <input type="hidden" name="category_id" id="category_id" value="<?php echo $category_id; ?>"  size="6" class="form-control"/>
                             <div class="col-sm-12">
                                 <label style="margin-left: 40%;margin-right: 10%;" ><?php  echo ucwords('(If you use this feature please insert all day and time)') ?> </label>
                              </div>
                              <div class="col-sm-12" style="margin-bottom: 3%;">
                                <label class="col-sm-2 control-label" style="text-align:left"><?php echo 'Title For Time'; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="title_for_time" value="<?php echo $title_for_time; ?>" placeholder="<?php echo 'Title For Time'; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label" >Day</label>
                                <div class="col-sm-2">
                                    <select name="day_open_store" id="day_open_store" class="form-control"  tabindex="15">
                                        <?php foreach ($days_name as $daykey =>  $dayvalue) { ?>
                                            <option value="<?php echo $daykey ?>" selected="selected"><?php echo $dayvalue; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label" >Start Time</label>
                                <div class="col-sm-2">
                                    <input type="text" name="store_intime" id="store_intime"  size="6" class="time form-control" autocomplete="off" />
                                </div>
                                <label class="col-sm-2 control-label" >End Time</label>
                                <div class="col-sm-2">
                                    <input type="text" name="store_outtime" id="store_outtime"  size="6" class="time form-control" autocomplete="off" />
                                </div>
                                <a onclick="addtimes()" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                            </div>

                            <div class="col-sm-12" style="margin-top: 5%;">
                                <table id="tbcharge" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <td class="text-center"><?php echo "Days"; ?></td>
                                          <td class="text-center"><?php echo "Start Time"; ?></td>
                                          <td class="text-center"><?php echo "End Time"; ?></td>
                                          <td class="text-center">Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($store_all) { ?>

                                            <?php foreach($store_all as $store_timingkey => $store_timing) {  ?>
                                                 <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][id]"  value="<?php echo $store_timing['id']; ?>">
                                                <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][day]"  value="<?php echo $store_timing['day']; ?>">
                                                <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][start_time]"  value="<?php echo $store_timing['end_time']; ?>">
                                                <input type= "hidden"  name="store_all[<?php echo $store_timingkey ?>][end_time]"  value="<?php echo $store_timing['end_time']; ?>">

                                                <tr>
                                                    <td class="text-center"><?php echo $store_timing['day']; ?></td>
                                                    <td class="text-center"><?php echo $store_timing['start_time']; ?></td>
                                                    <td class="text-center"><?php echo $store_timing['end_time']; ?></td>
                                                    <td>
                                                        <a type="button" class="btn btn-danger" onclick='delete_store_day(<?php echo $store_timing['id'] ?>);'><i class="fa fa-minus-circle"></i></a>
                                                    </td>
                                                   
                                                   
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="4">No Result Found</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div> 
  <script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">

    function addtimes(){
        var day_open_store = $('#day_open_store').val() || '';
        var store_intime = $('#store_intime').val() || '';
        var store_outtime = $('#store_outtime').val() || ''; 
        var category_id = $('#category_id').val() ; 


         if(day_open_store == '' || store_intime == '' || store_outtime == '' || category_id == 0){
            alert('Please fill all data');
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/subcategory/gettiming&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                day_open_store: day_open_store,
                store_intime: store_intime,
                store_outtime: store_outtime,
                category_id:category_id
            },
            success: function(json) {
                if (json.html != '' ) {
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                    $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                } 
                
            }
        });
    }

    function delete_store_day(id){
        if (confirm("Sure you want to delete this record? This cannot be undone later.")) {
            $.ajax({
                url:'index.php?route=catalog/subcategory/deleterecord&token=<?php echo $token; ?>'+'&id='+id,
                method: "POST",
                dataType: 'json',
                success: function(json)
                {   
                     $('#tbcharge tbody').html('');
                    $('#tbcharge tbody').append(json.html);
                     $('#day_open_store').val('');
                    $('#store_intime').val('');
                     $('#store_outtime').val('');
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
             return false;
        }
    }

 $('.time').timepicker({timeFormat: 'hh:mm:ss'});

 $.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('#input-parent_id').on('change', function() {
  loc_name = $('#input-parent_id option:selected').text();
  $('#parent_category').val(loc_name);
 
});

$('#colorSelector').ColorPicker({
    color: '#0000ff',
    onShow: function (colpkr) {
        $(colpkr).fadeIn(500);
        return false;
    },
    onHide: function (colpkr) {
        $(colpkr).fadeOut(500);
        return false;
    },
    onChange: function (hsb, hex, rgb) {
        $('#colorSelector').css('backgroundColor', '#' + hex);
        $('#colorSelector').val('#' + hex);
    },
    onSubmit: function (hsb, hex, rgb) {
        $('#colorSelector').css('backgroundColor', '#' + hex);
        $('#colorSelector').val('#' + hex);
    }
});

$('#button-photo').on('click', function() {
  $('#form-photo').remove();
  $('body').prepend('<form enctype="multipart/form-data" id="form-photo" style="display: none;"><input type="file" name="file" /></form>');
  $('#form-photo input[name=\'file\']').trigger('click');
  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }
  timer = setInterval(function() {
    if ($('#form-photo input[name=\'file\']').val() != '') {
      clearInterval(timer); 
      image_name = 'subcategory';  
      $.ajax({
        url: 'index.php?route=catalog/category/upload&token=<?php echo $token; ?>'+'&image_name='+image_name,
        type: 'post',   
        dataType: 'json',
        data: new FormData($('#form-photo')[0]),
        cache: false,
        contentType: false,
        processData: false,   
        beforeSend: function() {
          $('#button-upload').button('loading');
        },
        complete: function() {
          $('#button-upload').button('reset');
        },  
        success: function(json) {
          if (json['error']) {
            alert(json['error']);
          }
          if (json['success']) {
            alert(json['success']);
            console.log(json);
            $('input[name=\'photo\']').attr('value', json['filename']);
            $('input[name=\'photo_source\']').attr('value', json['link_href']);
            d = new Date();
            var previewHtml = '<br/><a target="_blank" style="cursor: pointer;" id="photo_source" href="'+json['link_href']+'">View Image</a>';
            $('#photo_source').remove();
            $('#button-photo').after(previewHtml);
          }
        },      
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});

</script>
<?php echo $footer; ?>