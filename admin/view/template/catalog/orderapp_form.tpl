<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row" style="margin-left: 50px;margin-top: 30px" >
					<form action="<?php echo $except; ?>" method="post" enctype="multipart/form-data" id="form-option" class="form-horizontal">
						<div class="col-md-6" style="border: 1px #000 solid; ">
							<div class="col-md-3" style="width:40%;margin-top: 30px">
								<div>
									<label style=" width: 40%; font-size: 15px;"><?php echo 'Order No'; ?></label><span style="font-size: 15px;" class="orderappmargin"><?php echo $cut_detail['order_no']?></span>
								</div>
								<div>
									<label style="width: 40%; font-size: 15px;" ><?php echo 'Name'; ?></label><span style="font-size: 15px;" class="orderappmargin"><?php echo $cut_detail['cus_name'] ?></span>
								</div>
								<div>
									<label  style="width: 40%;font-size: 15px; "><?php echo 'Phone no'; ?></label><span style="font-size: 15px;" class="orderappmargin"><?php echo $cut_detail['contact'] ?></span>
								</div>
								<div>
									<label style="width: 40%;font-size: 15px; "><?php echo 'Add'; ?></label><span style="font-size: 15px;" class="orderappmargin"><?php echo $cut_detail['delivery_address']?></span>
								</div><br>
								<div>
								<label style="text-align:left;font-size: 15px;"><?php echo 'Delivery Time'; ?></label>
									<span>
										<select name="exp_delivery_time" method = "post"  id="input-department" style="padding: 5px" class="">
											<option ><?php echo "Please Select" ?></option>
											<?php foreach($timee as $skey => $svalue){ ?>
												<?php if($skey == $cut_detail['exp_delivery_time']){ ?>
													<option value="<?php echo $skey ?>" selected = "selected"><?php echo $svalue; ?></option>
												<?php } else { ?>
													<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</span>
								</div><br>
									<div>
										<?php if($cut_detail['status'] != '1'){ ?>
											<button type="submit" class="btn btn-primary">Accept</button>&nbsp;&nbsp;&nbsp;
										<?php } ?>
										<?php if($cut_detail['status'] != '2'){ ?>
											<a href="<?php echo $cancel; ?>" class="btn btn-primary">Cancel</a>&nbsp;&nbsp;&nbsp;
										<?php } ?>
										<a href="<?php echo $back; ?>" class="btn btn-primary">Back</a>
									</div>
								<!-- <div>
									<button type="submit" form="form-sport" data-toggle="tooltip" title="Save" class="btn btn-primary"></button>
								</div><br> -->
							</div>
							<div class="col-md-3" style="width:60%;margin-top: 30px">
								<div>
									<?php $qty = 0;
									 $item = 0;
									 $tot= 0;
									 foreach($item_detail as $key) {  ?>
										<label style="width: 30%;display: inline-block;font-size: 15px; "><?php echo $key['item_name']; ?></label>
										<label style="width: 30%;display: inline-block;font-size: 15px;  "><?php echo $key['quantity']?></label>
										<br>
									<?php $qty = $qty + $key['quantity'];$item++;$tot = $tot + $key['total']; } ?>
								</div>
								<div>
									<label style="width: 30%;font-size: 15px; "><?php echo 'Grand Total'; ?></label><span class="orderappmargin"><?php echo $tot?></span>
								</div>
								<div>
									<label style="width: 30%;font-size: 15px; "><?php echo 'Total Item'; ?></label><span class="orderappmargin"><?php echo $item?></span>
								</div>
								<div>
									<label style="width: 30%;font-size: 15px; "><?php echo 'Total Qutntity'; ?></label><span class="orderappmargin"><?php echo $qty ?></span>
								</div>
							</div>
						</div>
					</form> 
				</div>
			</div>
		</div>
	</div>
</div>    
<?php echo $footer; ?>