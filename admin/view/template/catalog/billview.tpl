<?php echo $header; ?>
<div id="exTab2" class="container-fluid">	
	<div class="row">
		<div class="col-md-8">
			<ul class="nav nav-tabs">
				<li class="active">
		    		<a href="#1" data-toggle="tab">Bill View</a>
				</li>
				<li style="display: none;">
					<a href="#2" data-toggle="tab">Liquor</a>
				</li>
				<a style="margin-top: 2px;" class="btn btn-primary" id="back">Back</a>
			</ul>
			<div class="tab-content">
			  	<div style="overflow:auto; ;height:700px;" class="tab-pane active" id="1">
		  			<table class="table table-bordered" id="tablefood" style="cursor: pointer;">
			  			<tr>
			  				<th>REF.NO.</th>
			  				<th>F.B.No.</th>
			  				<th>F.TOT</th>
			  				<th>GST</th>
			  				<th>DIS</th>
			  				<th>Net Total</th>
			  				<th>L.B.No.</th>
			  				<th>L.TOT</th>
			  				<th>VAT</th>
			  				<th>DIS</th>
			  				<th>Net Total</th>
			  				<th>F&B Total</th>
			  				<th>Pay By</th>
			  				<th>Waiter</th>
			  				<th>Captain</th>
			  				<th>Tbl No.</th>
			  				<th>In</th>
			  				<th>Out</th>
			  				<th>Action</th>
			  			</tr>
			  			<?php foreach($order_datas as $order_data) { ?>
			  				<tr>
			  					<td><?php echo $order_data['order_no'] ?></td>
			  					<td><?php echo $order_data['food_bill_no'] ?></td>
			  					<td><?php echo $order_data['ftotal'] ?></td>
			  					<td><?php echo $order_data['gst'] ?></td>
			  					<td><?php echo $order_data['ftotalvalue']?></td>
			  					<?php if($INCLUSIVE == 1){ ?>
			  						<td><?php echo $order_data['ftotal']?></td>
			  					<?php } else { ?>
			  						<td><?php echo $order_data['ftotal'] + $order_data['gst'] ?></td>
			  					<?php } ?>
			  					<td><?php echo $order_data['liq_bill_no'] ?></td>
			  					<td><?php echo $order_data['ltotal'] ?></td>
			  					<td><?php echo $order_data['vat'] ?></td>
			  					<td><?php echo $order_data['ltotalvalue']?></td>
			  					<?php if($INCLUSIVE == 1){ ?>
			  						<td><?php echo $order_data['ltotal']?></td>
			  					<?php } else { ?>
			  						<td><?php echo $order_data['ltotal'] + $order_data['vat'] ?></td>
			  					<?php } ?>
			  					<td><?php echo $order_data['grand_total']?></td>
			  					<?php if($order_data['pay_card'] != 0) { ?>
			  						<td>Card</td>
			  					<?php } else if($order_data['pay_cash'] != 0) { ?>
			  						<td>Cash</td>
			  					<?php } else if($order_data['pay_online'] != 0) { ?>
			  						<td>Online</td>
			  					<?php } else if($order_data['pay_cash'] != 0 && $order_data['pay_card'] != 0) { ?>
			  						<td>Cash+Card</td>
			  					<?php } else { ?>
			  						<td>Pending</td>
			  					<?php } ?>
			  					<td><?php echo $order_data['waiter'] ?></td>
			  					<td><?php echo $order_data['captain'] ?></td>
			  					<td><?php echo $order_data['t_name'] ?></td>
			  					<td><?php echo $order_data['time_added'] ?></td>
			  					<td><?php echo $order_data['out_time'] ?></td>
			  					<td> 

			  						<?php if($order_data['cancel_status'] == 0){ ?>
			  							<?php if($cancel_bill == 1){ ?>
			  								<?php if($KOT_BILL_CANCEL_REASON == 1){ ?>
				  								<a  class="btn btn-primary"onclick="cancel_fun(<?php echo $order_data['order_id']?>,<?php echo $order_data['is_liq']?>);">Cancel</a> &nbsp;
				  							<?php } else { ?>
			  									<a href="index.php?route=catalog/billview/delete&token=<?php echo $token;?>&orderid=<?php echo $order_data['order_id']?>&is_liq=<?php echo $order_data['is_liq']?>" class="btn btn-primary" onclick="return confirm('Are you sure?')">Cancel</a> &nbsp;
			  								<?php } ?>
				  						<?php } ?>
				  						<?php if($duplicate_bill == 1){ ?>
				  							<a href="index.php?route=catalog/order/printsb&token=<?php echo $token;?>&order_id=<?php echo $order_data['order_id']?>&duplicate=1" class="btn btn-primary" onclick="return confirm('Are you sure?')">Duplicate</a>
				  						<?php } ?>
				  						<?php if($edit_bill == 1){ ?>
				  							<a href="index.php?route=catalog/order&token=<?php echo $token;?>&order_id=<?php echo $order_data['order_id']?>&lid=<?php echo $order_data['location_id']?>&tid=<?php echo $order_data['table_id']?>&edit=1"  class="btn btn-primary" onclick="return confirm('Are you sure?')">Edit</a>
				  						<?php } ?>
				  					<?php } else { ?>
				  						Cancelled
				  					<?php } ?>
			  					</td>
			  					<td style="display: none"><?php echo $order_data['order_id'] ?></td>
			  				</tr>
			  			<?php } ?>
		  			</table>
				</div>
				<div style="display:none;overflow:auto; ;height:300px;" class="tab-pane" id="2">
		  			<table class="table table-bordered" id="tableliq" style="cursor: pointer;">
			  			<tr>
			  				<th>Ref No.</th>
			  				<th>Bill No.</th>
			  				<th>Amt</th>
			  				<th>GST</th>
			  				<th>Disc</th>
			  				<th>Net Total</th>
			  				<th>F&B Total</th>
			  				<th>Pay By</th>
			  				<th>Waiter</th>
			  				<th>Captain</th>
			  				<th>Tbl No.</th>
			  				<th>In</th>
			  				<th>Out</th>
			  				<th>Action</th>
			  			</tr>
			  			<?php foreach($testingliq as $test) { ?>
			  				<tr>
			  					<td><?php echo $test['order_no'] ?></td>
			  					<td><?php echo $test['billno'] ?></td>
			  					<td><?php echo $test['ltotal'] ?></td>
			  					<td><?php echo $test['vat'] ?></td>
			  					<td><?php echo $test['ltotalvalue'] ?></td>
			  					<?php if($INCLUSIVE == 1){ ?>
			  						<td><?php echo $test['ltotal']?></td>
			  					<?php } else { ?>
			  						<td><?php echo $test['ltotal'] + $test['vat'] ?></td>
			  					<?php } ?>
			  					<?php if($test['food_cancel'] == 1){ ?>
			  						<td><?php echo $test['grand_total'] - $test['ftotal'] - $test['staxfood']?></td>
			  					<?php } else { ?> 
			  						<td><?php echo $test['grand_total']?></td>
			  					<?php } ?>
			  					<?php if($test['pay_card'] != 0) { ?>
			  						<td>Card</td>
			  					<?php } else if($test['pay_cash'] != 0) { ?>
			  						<td>Cash</td>
			  					<?php } else if($test['pay_online'] != 0) { ?>
			  						<td>Online</td>
			  					<?php } else if($test['pay_cash'] != 0 && $test['pay_card'] != 0) { ?>
			  						<td>Cash+Card</td>
			  					<?php } else { ?>
			  						<td>Pending</td>
			  					<?php } ?>
			  					<td><?php echo $test['waiter'] ?></td>
			  					<td><?php echo $test['captain'] ?></td>
			  					<td><?php echo $test['t_name'] ?></td>
			  					<td><?php echo $test['time_added'] ?></td>
			  					<td><?php echo $test['out_time'] ?></td>
			  					<td>
			  						<?php if($cancel_bill == 1){ ?>
			  							<?php if($KOT_BILL_CANCEL_REASON == 1){ ?>
				  							<a  class="btn btn-primary"onclick="cancel_fun(<?php echo $order_data['order_id']?>,<?php echo $order_data['is_liq']?>);">Cancel</a> &nbsp;
			  							<?php } else { ?>
		  									<a href="index.php?route=catalog/billview/delete&token=<?php echo $token;?>&orderid=<?php echo $order_data['order_id']?>&is_liq=<?php echo $order_data['is_liq']?>" class="btn btn-primary" onclick="return confirm('Are you sure?')">Cancel</a> &nbsp;
		  								<?php } ?>
			  						<?php } ?>
				  					<?php if($duplicate_bill == 1){ ?>
				  						<a href="index.php?route=catalog/order/printsb&token=<?php echo $token;?>&order_id=<?php echo $test['order_id']?>&duplicate=1" class="btn btn-primary" onclick="return confirm('Are you sure?')">Duplicate</a>
				  					<?php } ?>
				  					<?php if($edit_bill == 1){ ?>
				  						<a href="index.php?route=catalog/order&token=<?php echo $token;?>&order_id=<?php echo $test['order_id']?>&lid=<?php echo $test['location_id']?>&tid=<?php echo $test['table_id']?>&edit=1"  class="btn btn-primary" onclick="return confirm('Are you sure?')">Edit</a>
			  						<?php } ?>
			  					</td>
			  					<td style="display: none"><?php echo $test['order_id'] ?></td>
			  				</tr>
			  			<?php } ?>
		  			</table>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<table id="data" class="table table-bordered">
				<tr>
					<th>Description</th>
					<th>Qty</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#tablefood tr").click(function(){
	   var orderid=$(this).find('td:last').html();
	   $('#data').html('');
	   $.ajax({
	   		type: "POST",
	   		url: 'index.php?route=catalog/billview/getdatafood&token=<?php echo $token;?>&orderidfood=' + encodeURIComponent(orderid),
	   		dataType: 'json',
	   		success: function(data){
   				var $trm = $('<tr>').append(
   								$('<th>').text("Description"),
   								$('<th>').text("Qty"),
   								$('<th>').text("Rate"),
   								$('<th>').text("Amount")
   							);
   				$('#data').append($trm);
	   			$.each(data,function(i,fooditem){
	   				var $tr = $('<tr>').append(
	   								$('<td>').text(fooditem.name),
	   								$('<td>').text(fooditem.qty),
	   								$('<td>').text(fooditem.rate),
	   								$('<td>').text(fooditem.amt)
	   							);
	   					$('#data').append($tr);
	   			});
	   		}
	   })
	});

	$("#tableliq tr").click(function(){
	   var orderid=$(this).find('td:last').html();
	   $('#data').html('');
	   $.ajax({
	   		type: "POST",
	   		url: 'index.php?route=catalog/billview/getdataliq&token=<?php echo $token;?>&orderidliq=' + encodeURIComponent(orderid),
	   		dataType: 'json',
	   		success: function(data){
   				var $trm = $('<tr>').append(
								$('<th>').text("Description"),
								$('<th>').text("Qty"),
								$('<th>').text("Rate"),
								$('<th>').text("Amount")
							);
   				$('#data').append($trm);
	   			$.each(data,function(i,liqitem){
	   				var $tr = $('<tr>').append(
	   								$('<td>').text(liqitem.name),
	   								$('<td>').text(liqitem.qty),
	   								$('<td>').text(liqitem.rate),
	   								$('<td>').text(liqitem.amt)
	   							);
	   					$('#data').append($tr);
	   			});
	   		}
	   })
	});

	$('#back').click(function(){
		tablet = '<?php echo $tablet ?>';
		qwerty = '<?php echo $qwerty ?>';
		if(tablet == '1'){
			$('#back').attr('href','index.php?route=catalog/ordertab&token=<?php echo $token; ?>');
		} else if(qwerty == '1'){
			$('#back').attr('href','index.php?route=catalog/orderqwerty&token=<?php echo $token; ?>');
		} else {
			$('#back').attr('href','index.php?route=catalog/order&token=<?php echo $token; ?>');
		}
	});

	function cancel_fun(order_id,is_liq){
		var reason = prompt("Please enter reason");
		if (reason != null && reason != '' ) {
			$.ajax({
		   		type: "GET",
		   		url: 'index.php?route=catalog/billview/delete&token=<?php echo $token;?>&orderid=' + encodeURIComponent(order_id)+'&is_liq=' + is_liq +'&reason='+ reason  ,
		   		dataType: 'json',
		   		success: function(data){
		   			$.ajax({
				   		type: "GET",
				   		url: 'index.php?route=catalog/order/printcancel&token=<?php echo $token;?>&orderid=' + encodeURIComponent(order_id)+'&cancel=1',
				   		dataType: 'json',
				   		success: function(data){
			   				window.location.reload();
				   		}
				  	})
		   		}
		  	})
		  	
		} else {
			alert("Please Enter Reason To Cancel Bill");
			return false;

		}

	}

</script>
<?php echo $footer; ?>