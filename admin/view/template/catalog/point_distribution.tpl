<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				<button type="submit" form="form-department" data-toggle="tooltip" title="<?php echo 'Save' ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo 'Cancel' ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
	  		<h1><?php echo 'Point Distribution'; ?></h1>
	  		<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
	  		</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo 'Point Distribution'; ?></h3>
		  	</div>
		  	<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-department" class="form-horizontal">
			  		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Date'; ?></label>
						<div class="col-sm-3">
				  			<input type="date" name="date" id="date" value="" placeholder="<?php echo 'Date'; ?>" class="form-control" />
						</div>
			   		</div>

			   		<div class="form-group required">
						<label class="col-sm-3 control-label"><?php echo 'Bill No:'; ?></label>
						<div class="col-sm-3">
				  			<input type="text" name="bill_no" id="bill_no" value="" placeholder="<?php echo 'Bill No'; ?>" class="form-control" />
						</div>
			   		</div>
			   		<div id="amount_div" class="form-group ">
						<label class="col-sm-3 control-label"><?php echo 'Amount'; ?></label>
						      
			   		</div>
			   		<div id="item" class="form-group">
				        <label class="col-sm-3 control-label">Item</label>
				        <table>
							  <tr>
							    <td id="item_div"> </td>
							    <td id="item_div1"> </td>
							    <td id="item_div2"> </td>
							  </tr>
						</table>	  
				        
				    </div>
				    <div id="text">
				    <!--<input type="text" name="birthday" id="birthday" value="" placeholder="<?php echo 'Birthday Point'; ?>" class="form-control" />	-->
					</div>
					
				</form>
		  	</div>
		</div>
  	</div>
</div> 
<script type="text/javascript">
	$('#input-subcategory_id').on('change', function() {
	  loc_name = $('#input-subcategory_id option:selected').text();
	  $('#subcategory').val(loc_name);
	 
	});

$('#type').on('change', function() {
	  type_name = $('#type option:selected').text();
	  $('#type_name').val(type_name);
});
</script>
<script type="text/javascript">
$(document).ready(
    function() {
        $('#bill_no').keyup(
            function() {
            	var date = document.getElementById('date').value;
                var value = document.getElementById('bill_no').value;
               // alert(value);
               // alert(date);
               $.ajax({
	   			url:'index.php?route=catalog/point_distribution/ajax&token=<?php echo $token;?>&date='+date+'&bill_no='+value,
	   			type:"GET",
	  			dataType:'json',
	  			success:function(json){
			    console.log(json);
			    //alert(json.amount)
			    //alert(json.item_name)
			    //$('#item').find('div').remove();
			    $('#item_div').find('input').remove();
			    $('#item_div1').find('input').remove();
			    $('#item_div2').find('input').remove();
			    $('#amount_div').find('input').remove();
			   // $('#amount').find('input').remove();
			    if(json){
			    	$('#amount_div').append($('<input type="text" style="width:23%" readonly class="form-control col-xs-3" name="amount" value="'+json[0].amount+'" />'));
				    $.each(json, function (i, item) {
				   	$('#item_div').append($('<input type="text" readonly style="width:80%;margin-bottom: 13px;" class="form-control col-sm-3" name="'+item.item_name+'" onkeyup="function(name)" value="'+item.item_name+'"  />'));
				   	$('#item_div1').append($('<input type="text" id="qtychk'+item.id_checkbox+'" readonly  onkeyup="function()" style="width:80%;margin-bottom: 13px;" class="form-control col-xs-3" name="item_quantity" value="'+item.item_quantity+'" /> '));
				   	$('#item_div2').append($('<input type="checkbox"  style="margin-bottom:13px;padding:10px;" class="form-control col-xs-3 check" id="chk'+item.id_checkbox+'"  onclick="myFunction(id)" /> <br />'));
				   	//id="qty'+item.item_quantity+'"
				    });
			    }
			    }
			    });
   
            });
});
</script>
<script>
	function myFunction2(id) {
		var checkBox = document.getElementById(id).value;
		alert(checkBox);
		if (checkBox == 'chk1'){
	  		var count =0;// $("#holder input").size();
	  		var $ctrl1 = $('<input/>').attr({ type: 'text', name:'Bithday Point', value:'Bithday Point', class:'form-control',style:'width:50%;margin-left: 24%;margin-bottom: 11px;' });        
		    $("#text").append($ctrl1);
		}
	}
</script>	
<script>
function myFunction(id,name) {
  var checkBox = document.getElementById(id);
  //alert(name);
  if (checkBox.checked == true){
  		var count =0;// $("#holder input").size();
	    var requested =document.getElementById('qty'+id).value;//parseInt($("#ppl").val(),10);
	    if (requested > count) {
	        for(i=count; i<requested; i++) {
	        var $ctrl = $('<input/>').attr({ type: 'text', name:'text', value:'text', class:'form-control',style:'width:50%;margin-left: 24%;margin-bottom: 11px;' });        
	            $("#text").append($ctrl);
	        }
	    }
    }else{
    	$('#text').find('input').remove();
    }

}
</script>
<?php echo $footer; ?>