<?php if($hide == 0){ ?>
	<?php if($storename == 0){ ?>
	<ul id="menu">
	  	<li style="display:none;" id="dashboard"><a href="<?php echo $home; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li>
	  	<li id="catalog" style="font-weight: bold;font-size: 20px;"><a class="parent"><i  class="fa fa-tags fa-fw"></i> <span style="font-weight: bold;font-size: 20px;"><?php echo 'Master'; ?></span></a>
	<ul style="width: 400px;">
		<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'APP'; ?></span></a>
			<ul style="width: 400px;">
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"href="<?php echo $venue; ?>"><?php echo 'Venue Master'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"href="<?php echo $meal; ?>"><?php echo 'Meal Master'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"href="<?php echo $events; ?>"><?php echo 'Events Master'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $activity; ?>"><?php echo 'Activity Master'; ?></a></li>
			   	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $banner; ?>"><?php echo 'Banner'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $testimonal; ?>"><?php echo 'Testimonal'; ?></a></li>
			</ul>
		<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Menu Entry'; ?></span></a>
			<ul style="width: 400px;" >
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"href="<?php echo $item; ?>"><?php echo 'Item'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $modifier; ?>"><?php echo 'Modifier'; ?></a></li>
			  	
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $department; ?>"><?php echo 'Department'; ?></a></li>
			  	
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $category; ?>"><?php echo 'Category'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $subcategory; ?>"><?php echo 'SubCategory'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $onlinepayment; ?>"><?php echo 'OnlinePayment'; ?></a></li>
			  	
			</ul>

		<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Other Entry'; ?></span></a>
			<ul style="width: 400px;" >
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $templet; ?>"><?php echo 'Template'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $itembulk; ?>"><?php echo 'Bulk Item Update'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $location; ?>"><?php echo 'Table Master'; ?></a></li>
			  	<li style="display: none;"><a href="<?php echo $table; ?>"><?php echo 'Table'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $point_distribution; ?>"><?php echo 'Point Distribution'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $customer; ?>"><?php echo 'Customer'; ?></a></li>
			  	
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $accountexpense; ?>"><?php echo 'Account Expense'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $waiter; ?>"><?php echo 'Employee'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $tax; ?>"><?php echo 'Tax'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $supplier; ?>"><?php echo 'Supplier'; ?></a></li>
			 	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $hotellocation; ?>"><?php echo 'Location'; ?></a></li>
			  	<li style="display: none;"><a href="<?php echo $captain; ?>"><?php echo 'Captain'; ?></a></li>


			</ul>

		<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'KOT'; ?></span></a>
			<ul style="width: 400px;">
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $kotmsg; ?>"><?php echo 'Kot Message'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $kotmsgitem; ?>"><?php echo 'Kot Message Item'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $kotmsgcat; ?>"><?php echo 'Kot Message Category'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $kotgroup; ?>"><?php echo 'Kotgroup'; ?></a></li>
			</ul>
		<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Stock'; ?></span></a>
			<ul style="width: 400px;">
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockcategory; ?>"><?php echo 'Stock Category'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockitem; ?>"><?php echo 'Stock Item'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $store_name; ?>"><?php echo 'Store'; ?></a></li>
			  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $brand; ?>"><?php echo 'Brand'; ?></a></li>
			</ul>
		 </li>
	</ul>
	</li>
	  	<li id="transaction" ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Transaction'; ?></span></a>
			<ul style="width: 400px;">
				<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Billing'; ?></span></a>
					<ul style="width: 400px;">
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $school; ?>"><?php echo 'Order'; ?></a></li>
						<li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderqwerty; ?>"><?php echo 'Order Keyboard'; ?></a></li>
						  <li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $ordertab; ?>"><?php echo 'Order Tablet'; ?></a></li>
						  <li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $ordertouch; ?>"><?php echo 'Order Touch'; ?></a></li>
						  <li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderapp; ?>"><?php echo 'API Order'; ?></a></li>
						  <li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $swiggyorder; ?>"><?php echo 'Swiggy Order'; ?></a></li>
						  <li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $bookingapp; ?>"><?php echo 'Online Booking'; ?></a></li>
					</ul>
				<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Food Stock Management'; ?></span></a>
			  		<ul style="width: 400px;">
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchaseentryfood; ?>"><?php echo 'Purchase Entry Food'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stocktransfer; ?>"><?php echo 'Stock Transfer'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockmanufacturer; ?>"><?php echo 'Manufacturer'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $bom; ?>"><?php echo 'BOM'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"href="<?php echo $purchaseorder; ?>"><?php echo 'Purchase Order'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $westage_adjustment; ?>"><?php echo 'Wastage / Adjustment'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchaseoutward; ?>"><?php echo 'Purchase Outward'; ?></a></li>
					</ul>
					<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Liqour Stock Management'; ?></span></a>
			  		<ul style="width: 400px;">
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchaseentry; ?>"><?php echo 'Purchase Entry Liquor '; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchaseentryloose; ?>"><?php echo ' Create Loose Stock'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stocktransferliq; ?>"><?php echo 'Stock Transfer Liquor'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stocktransferloose; ?>"><?php echo 'Stock Transfer Loose'; ?></a></li>
					</ul>
				<li style="width: 400px;" id="transaction"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Other'; ?></span></a>
					<ul style="width: 400px;">
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $sms; ?>"><?php echo 'SMS'; ?></a></li>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $customercredit; ?>"><?php echo 'Customer Credit'; ?></a></li>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $expensetrans; ?>"><?php echo 'Expense Transaction'; ?></a></li>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_recharge; ?>"><?php echo 'MSR Recharge'; ?></a></li>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_redeem; ?>"><?php echo 'MSR Redeem'; ?></a></li>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_refund; ?>"><?php echo 'MSR Refund'; ?></a></li>
						<li id="report"  ><a class="parent" href="<?php echo $feedback; ?>"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" ><?php echo 'FeedBack Form'; ?></span></a>
						<li id="report"  ><a class="parent" href="<?php echo $reception; ?>"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" ><?php echo 'Reception Form'; ?></span></a>
						<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $databasebackup; ?>"><?php echo 'Sync Server'; ?></a></li>
						
					</ul>
				</ul>
	  			</li>
			   	<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Reports'; ?></span></a>
					<ul style="width: 400px;">
						<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Daily Register'; ?></span></a>
							<ul style="width: 400px;">
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailybill; ?>"><?php echo 'Billwise Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailybillwiseitem_report; ?>"><?php echo 'Billwise Item Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none; " href="<?php echo $captainwaiter; ?>"><?php echo 'Cap And Wtr Comm Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $captainwaitersale; ?>"><?php echo 'Cap And Wtr Sale Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailyreportstock; ?>"><?php echo 'Itemwise Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $cancelkot; ?>"><?php echo 'Cancelled KOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $nckotreport; ?>"><?php echo 'NC KOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $cancelbot; ?>"><?php echo 'Cancelled BOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $cancelbill; ?>"><?php echo 'Cancelled Bill Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $duplicatebill; ?>"><?php echo 'Duplicate Bill Report'; ?></a></li>
					  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailydaysummaryreport; ?>"><?php echo 'Day Summary Report'; ?></a></li>
					  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailyminidaysummaryreport; ?>"><?php echo 'Mini Day Summary Report'; ?></a></li>
					  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $dailyshiftclose_report; ?>"><?php echo 'Shift Close Report'; ?></a></li>
					  			

							  	<li style="display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $currentstockreport; ?>"><?php echo 'Current Stock Report'; ?></a></li>
							</ul>

							<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Online Food Report'; ?></span></a>
								<ul style="width: 400px;">
							  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $werafood_order_report; ?>"><?php echo 'Online Food Order Report'; ?></a></li>
							  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $werafood_reject_report; ?>"><?php echo 'Online Food Reject Report'; ?></a></li>
							  		</ul>

						<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Monthly Register'; ?></span></a>
							<ul style="width: 400px;">
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $monthlyminireport; ?>"><?php echo 'Mini Day Summary Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $reportbill; ?>"><?php echo 'Billwise Report'; ?></a></li>

							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $gstbill; ?>"><?php echo 'GST Report'; ?></a></li>

							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $billwiseitem_report; ?>"><?php echo 'Billwise Item Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $captainwaiter; ?>"><?php echo 'Cap And Wtr Comm Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $captainwaitersale; ?>"><?php echo 'Cap And Wtr Sale Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $reportstock; ?>"><?php echo 'Itemwise Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $cancelkot; ?>"><?php echo 'Cancelled KOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $nckotreport; ?>"><?php echo 'NC KOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $cancelbot; ?>"><?php echo 'Cancelled BOT Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $cancelbill; ?>"><?php echo 'Cancelled Bill Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $duplicatebill; ?>"><?php echo 'Duplicate Bill Report'; ?></a></li>
					  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $daysummaryreport; ?>"><?php echo 'Day Summary Report'; ?></a></li>
					  			<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $shiftclose_report; ?>"><?php echo 'Shift Close Report'; ?></a></li>
					  			

							  	<li style="display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $currentstockreport; ?>"><?php echo 'Current Stock Report'; ?></a></li>
							</ul>
						<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Other Register'; ?></span></a>
							<ul style="width: 400px;">
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $selling_report; ?>"><?php echo 'Selling Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $foodbarreport; ?>"><?php echo 'Food And Bar Day Wise Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $reportdaywise; ?>"><?php echo 'Daywise Sale Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderprocesss; ?>"><?php echo 'Kitchen display status Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $advancereport; ?>"><?php echo 'Advance Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $creditreport; ?>"><?php echo 'Credit Report'; ?></a></li>
							  	<li style="display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $currentstockreport; ?>"><?php echo 'Current Stock Report'; ?></a></li>
							  	<li  style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $complimentaryreport; ?>"><?php echo 'Complimentary Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $expensereport; ?>"><?php echo 'Expense Report'; ?></a></li>
							   	<li  style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $billmodifyreport; ?>"><?php echo 'Bill Modify Report'; ?></a></li>
							   	<li  style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_recharge_report; ?>"><?php echo 'MSR Recharge Report'; ?></a></li>
							   	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_redeem_report; ?>"><?php echo 'MSR Redeem Report'; ?></a></li>
							   	<li  style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_refund_report; ?>"><?php echo 'MSR Refund Report'; ?></a></li>
							   	<li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_ledger; ?>"><?php echo 'MSR Ledger Report'; ?></a></li>
							   	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $msr_ledger_report; ?>"><?php echo 'MSR Ledger Report'; ?></a></li>
							   	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $sms_report; ?>"><?php echo 'SMS History Report'; ?></a></li>
					  			<li id="report"  ><a class="parent" href="<?php echo $feedback_report; ?>"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" ><?php echo 'FeedBack Report'; ?></span></a>

					  				<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $prebill_report; ?>"><?php echo 'Privious Bill Report'; ?></a></li>

					  				<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $customerwise_sale_report; ?>"><?php echo 'Customer Wise Sale Report'; ?></a></li>

					  				<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $customerwise_item_report; ?>"><?php echo 'Customer Wise Item Report'; ?></a></li>
							
							</ul>

						  <li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Food Register'; ?></span></a>
							<ul style="width: 400px;">
						  
								<li style="display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $currentstockreport; ?>"><?php echo 'Current Stock Report'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchaseorderreport; ?>"><?php echo 'Purchase Entry Report'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockreportfood; ?>"><?php echo 'Stock Report Food'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stock_report; ?>"><?php echo 'Stock Report'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $storewisereport; ?>"><?php echo 'PO Store Wise Report'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $itemwisereport; ?>"><?php echo 'PO Store Wise Item Report'; ?></a></li>
								<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $bomlazer; ?>"><?php echo 'BOM Wise Ledger Report'; ?></a></li>
							</ul>

							<li  id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Liquor Register'; ?></span></a>
							<ul style="width: 400px;">

							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $purchasereportloose; ?>"><?php echo 'Purchase Loose Report'; ?></a></li>
							  	<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockreportliq; ?>"><?php echo 'Stock Report Liquor'; ?></a></li>
							  	<li style="width: 400px;"> <a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockreportloose; ?>"><?php echo 'Stock Report Loose'; ?></a></li>
							  	<li><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $currentstockreport_lsb; ?>"><?php echo 'Current Stock Report LBS'; ?></a></li>
							</ul>
						  </li>
						</ul>
					  	</li>

					  <li id="report"  ><a class="parent"><i class="fa fa-tags fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Order Screen'; ?></span></a>
						<ul style="width: 400px;">
							<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;display: none;" href="<?php echo $werapending_order; ?>"><?php echo 'Online Pending Orders'; ?></a></li>
							<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $kitchen_display; ?>"><?php echo 'Kitchen Display'; ?></a></li>
							<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderprocess; ?>"><?php echo 'Order Entry Screen'; ?></a></li>
							<li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderprocess1; ?>"><?php echo 'Order Display Screen  '; ?></a></li>
							<li style="width: 400px;display: none;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderprocesss; ?>"><?php echo 'Order Process Report'; ?></a></li>

						</ul>
						
					  </li>

						
					  </li>

					  <li  id="system"  ><a class="parent"><i class="fa fa-cog fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Utility'; ?></span></a>
						<ul style="width: 400px;">
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $sync_client; ?>"><?php echo 'Sync Client'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $trancatedata; ?>"><?php echo 'Clear Data'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $stockclear; ?>"><?php echo 'Stock Clear'; ?></a></li>
						   <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $webhook; ?>"><?php echo 'Webhook'; ?></a></li>
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $barcode; ?>"><?php echo 'Barcode Printing'; ?></a></li>
						   <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $delete_report; ?>"><?php echo 'Log Reprt'; ?></a></li>
						 <!-- <li style="width: 400px;" style = "display:none"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $orderprocess_display; ?>"><?php //echo 'Order Display Screen'; ?></a></li>-->
						  <li style="width: 400px;"><a style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;" href="<?php echo $setting; ?>"><?php echo 'Setting'; ?></a></li>
						</ul>
					  </li>
					   <li  id="system"  >
					   	<a class="parent" href="<?php echo $urbanpiper_order ?>"><i class="fa fa-bell" aria-hidden="true" style="display: flex;">
					   	<span class="label label-pill label-danger count" style="border-radius:10px;padding: 6px 6px;font-size: 10px;"></span></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo 'Online Orders'; ?></span></a>
					  </li>
					  <li id="extension" style="display: none;"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span style="padding-top: 2px;padding-bottom: 2px;font-weight: bold;font-size: 20px;"><?php echo $text_extension; ?></span></a>
						<ul style="width: 400px;">
						  <li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li>
						  <li><a href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li>
						  <li><a href="<?php echo $theme; ?>"><?php echo $text_theme; ?></a></li>
						  <li><a href="<?php echo $analytics; ?>"><?php echo $text_analytics; ?></a></li>
						  <li><a href="<?php echo $captcha; ?>"><?php echo $text_captcha; ?></a></li>
						  <li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>
						  <li><a href="<?php echo $fraud; ?>"><?php echo $text_fraud; ?></a></li>
						  <li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li>
						  <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
						  <li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li>
						  <li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li>
						  <?php if ($openbay_show_menu == 1) { ?>
						  <li><a class="parent"><?php echo $text_openbay_extension; ?></a>
							<ul>
							  <li><a href="<?php echo $openbay_link_extension; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
							  <li><a href="<?php echo $openbay_link_orders; ?>"><?php echo $text_openbay_orders; ?></a></li>
							  <li><a href="<?php echo $openbay_link_items; ?>"><?php echo $text_openbay_items; ?></a></li>
							  <?php if ($openbay_markets['ebay'] == 1) { ?>
							  <li><a class="parent"><?php echo $text_openbay_ebay; ?></a>
								<ul>
								  <li><a href="<?php echo $openbay_link_ebay; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
								  <li><a href="<?php echo $openbay_link_ebay_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
								  <li><a href="<?php echo $openbay_link_ebay_links; ?>"><?php echo $text_openbay_links; ?></a></li>
								  <li><a href="<?php echo $openbay_link_ebay_orderimport; ?>"><?php echo $text_openbay_order_import; ?></a></li>
								</ul>
							  </li>
							  <?php } ?>
							  <?php if ($openbay_markets['amazon'] == 1) { ?>
							  <li><a class="parent"><?php echo $text_openbay_amazon; ?></a>
								<ul>
								  <li><a href="<?php echo $openbay_link_amazon; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
								  <li><a href="<?php echo $openbay_link_amazon_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
								  <li><a href="<?php echo $openbay_link_amazon_links; ?>"><?php echo $text_openbay_links; ?></a></li>
								</ul>
							  </li>
							  <?php } ?>
							  <?php if ($openbay_markets['amazonus'] == 1) { ?>
							  <li><a class="parent"><?php echo $text_openbay_amazonus; ?></a>
								<ul>
								  <li><a href="<?php echo $openbay_link_amazonus; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
								  <li><a href="<?php echo $openbay_link_amazonus_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
								  <li><a href="<?php echo $openbay_link_amazonus_links; ?>"><?php echo $text_openbay_links; ?></a></li>
								</ul>
							  </li>
							  <?php } ?>
							  <?php if ($openbay_markets['etsy'] == 1) { ?>
							  <li><a class="parent"><?php echo $text_openbay_etsy; ?></a>
								<ul>
								  <li><a href="<?php echo $openbay_link_etsy; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
								  <li><a href="<?php echo $openbay_link_etsy_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
								  <li><a href="<?php echo $openbay_link_etsy_links; ?>"><?php echo $text_openbay_links; ?></a></li>
								</ul>
							  </li>
							  <?php } ?>
							</ul>
						  </li>
						  <?php } ?>
						</ul>
					  </li>
					  <li id="design" style="display: none;"><a class="parent"><i class="fa fa-television fa-fw"></i> <span><?php echo $text_design; ?></span></a>
						<ul>
						  <li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li>
						  <li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li>
						</ul>
					  </li>
					  <li id="sale" style="display: none;"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
						<ul>
						  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
						  <li><a href="<?php echo $order_recurring; ?>"><?php echo $text_order_recurring; ?></a></li>
						  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
						  <li><a class="parent"><?php echo $text_voucher; ?></a>
							<ul>
							  <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
							  <li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_paypal ?></a>
							<ul>
							  <li><a href="<?php echo $paypal_search ?>"><?php echo $text_paypal_search ?></a></li>
							</ul>
						  </li>
						</ul>
					  </li>
					  <li style="display:none;" id="customer"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo 'Registration List'; ?></span></a>
						<ul>
						  <li><a href="<?php echo $customer; ?>"><?php echo 'Registration List'; ?></a></li>
						  <li style="display: none;"><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li>
						  <li style="display: none;"><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li>
						</ul>
					  </li>
					  <li style="display: none;"><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
						<ul>
						  <li><a href="<?php echo $marketing; ?>"><?php echo $text_marketing; ?></a></li>
						  <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
						  <li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li>
						  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
						</ul>
					  </li>
					  <li id="system" style="display: none;"><a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php echo $text_system; ?></span></a>
						<ul>
						  <li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li>
						  <li><a class="parent"><?php echo $text_users; ?></a>
							<ul>
							  <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
							  <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
							  <li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_localisation; ?></a>
							<ul>
							  <li><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li>
							  <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
							  <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
							  <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
							  <li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li>
							  <li><a class="parent"><?php echo $text_return; ?></a>
								<ul>
								  <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li>
								  <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li>
								  <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li>
								</ul>
							  </li>
							  <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
							  <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
							  <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
							  <li><a class="parent"><?php echo $text_tax; ?></a>
								<ul>
								  <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
								  <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li>
								</ul>
							  </li>
							  <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
							  <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_tools; ?></a>
							<ul>
							  <li><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li>
							  <li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
							  <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
							</ul>
						  </li>
						</ul>
					  </li>
					  <li id="reports" style="display: none;"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
						<ul>
						  <li><a class="parent"><?php echo $text_sale; ?></a>
							<ul>
							  <li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
							  <li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
							  <li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
							  <li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
							  <li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_product; ?></a>
							<ul>
							  <li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
							  <li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_customer; ?></a>
							<ul>
							  <li><a href="<?php echo $report_customer_online; ?>"><?php echo $text_report_customer_online; ?></a></li>
							  <li><a href="<?php echo $report_customer_activity; ?>"><?php echo $text_report_customer_activity; ?></a></li>
							  <li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
							  <li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
							  <li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
							</ul>
						  </li>
						  <li><a class="parent"><?php echo $text_marketing; ?></a>
							<ul>
							  <li><a href="<?php echo $report_marketing; ?>"><?php echo $text_marketing; ?></a></li>
							  <li><a href="<?php echo $report_affiliate; ?>"><?php echo $text_report_affiliate; ?></a></li>
							  <li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li>
							</ul>
						  </li>
						</ul>
					  </li>
					</ul>
	<?php } else { ?>
	<ul id="menu">
		<li><a href="<?php echo $purchaseorder; ?>"><?php echo 'Purchase Order'; ?></a></li>
		<li><a href="<?php echo $storewisereport; ?>"><?php echo 'Purchase Report'; ?></a></li>
		<li><a href="<?php echo $inwardreport; ?>"><?php echo 'Inward Report'; ?></a></li>
	</ul>
	<?php } ?>
<?php } ?>
<script type="text/javascript">
	/*var current_urls = '<?php echo $current_url ?>';
	var check_getqueue_ignore = false;
	if(current_urls == 0){
		setInterval(function(){ 
			load_unseen_notification();
		 }, 15000);
	}
	function load_unseen_notification(view = '') {
		if (check_getqueue_ignore) {
	    	return false;
	    }
		 	
	 	var check_ajax = localStorage.getItem("closed_ajax") || '';
	 	if(check_ajax == 'closed_ajax'){
	 		return false;
	 	}
	 	check_getqueue_ignore = true;
		var header_req =   $.ajax({
		   url:'index.php?route=catalog/urbanpiper_order/check_transection&token=<?php echo $token; ?>',
		   method:"POST",
		   data:{view:view},
		   dataType:"json",
		   success:function(json)
		   {
		   	console.log('in success');
		   	check_getqueue_ignore = false;
		    if(json.unseen_notification > 0)
		    {
		    	console.log(json.unseen_notification);
		     	$('.count').html(json.unseen_notification);
		     	//document.getElementById('audio').play();

		     	 $('#audio').remove();
    			$('body').append('<audio id="audio" src="bell_ringing_final.mp3" autoplay="false" ></audio>');
		     	
		    }
		   }
		});
	}*/
</script>