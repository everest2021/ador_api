
<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<link href="view/javascript/jquery/jquery-ui-1.12.1/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="view/javascript/jquery/jquery-ui-1.12.1/external/jquery/jquery.js"></script> 
<!--script src="https://code.jquery.com/jquery-1.12.4.min.js"></script-->
<script src="view/javascript/jquery/digitalKeyboard.js"></script>

<script src="view/javascript/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="view/javascript/jquery/jquery-ui-1.12.1/jquery-ui.css">
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/bootstrap-multiselect.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/jquery/bootstrap-multiselect.js"></script>

<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<script src="view/javascript/jquery/colorpicker/js/colorpicker.js" type="text/javascript"></script>
<link href="view/javascript/jquery/colorpicker/css/colorpicker.css" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script> -->


<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<?php if($hide == 0){ ?>
	<div id="container" style="">
	<body>
<?php } else { ?>
	<body style="overflow: hidden;">
	<?php if($hide == 1){ ?>				
		<div id="container" style="background-color: #000;">
	<?php } else { ?>
		<div id="container">
	<?php } ?>	
<?php } ?>
<?php if($hide == 0){ ?>
	<?php if($in == 0){ ?>
	<header id="header" class="navbar navbar-static-top">
	  <div class="navbar-header">
		<?php if ($logged) { ?>
		<a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
		<?php } ?>
		<a href="<?php echo $home; ?>" class="navbar-brand">
		  HOTEL
		  <!-- VVMC Sports -->
		</a>
	  </div>
	  <div id="dialog-form_orderpop" title = "Order Pop" >
	</div>

	  <?php if ($logged) { ?>
	  <ul class="nav pull-right">
		<li class="dropdown" style="display: none;"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i></a>
		  <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
			<li class="dropdown-header"><?php echo $text_order; ?></li>
			<li><a href="<?php echo $processing_status; ?>" style="display: block; overflow: auto;"><span class="label label-warning pull-right"><?php echo $processing_status_total; ?></span><?php echo $text_processing_status; ?></a></li>
			<li><a href="<?php echo $complete_status; ?>"><span class="label label-success pull-right"><?php echo $complete_status_total; ?></span><?php echo $text_complete_status; ?></a></li>
			<li><a href="<?php echo $return; ?>"><span class="label label-danger pull-right"><?php echo $return_total; ?></span><?php echo $text_return; ?></a></li>
			<li class="divider"></li>
			<li class="dropdown-header"><?php echo $text_customer; ?></li>
			<li><a href="<?php echo $online; ?>"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
			<li><a href="<?php echo $customer_approval; ?>"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
			<li class="divider"></li>
			<li class="dropdown-header"><?php echo $text_product; ?></li>
			<li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
			<li><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
			<li class="divider"></li>
			<li class="dropdown-header"><?php echo $text_affiliate; ?></li>
			<li><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li>
		  </ul>
		</li>
		<?php if($superadmin == '1') { ?>
		  <li class="active lists"><a href="<?php echo HTTP_HOME ?>" style="cursor: pointer;margin-left:180px;font-size: 130%;">Back</a></li>
		<?php } ?>
		<li class="dropdown" style="display: none;"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-home fa-lg"></i></a>
		  <ul class="dropdown-menu dropdown-menu-right">
			<li style="display: none;" class="dropdown-header"><?php echo $text_store; ?></li>
			<?php foreach ($stores as $store) { ?>
			<li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo 'Front End Registration'; ?></a></li>
			<?php } ?>
		  </ul>
		</li>
		<li class="dropdown" style="display: none;"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
		  <ul class="dropdown-menu dropdown-menu-right">
			<li class="dropdown-header"><?php echo $text_help; ?></li>
			<li><a href="http://www.opencart.com" target="_blank"><?php echo $text_homepage; ?></a></li>
			<li><a href="http://docs.opencart.com" target="_blank"><?php echo $text_documentation; ?></a></li>
			<li><a href="http://forum.opencart.com" target="_blank"><?php echo $text_support; ?></a></li>
		  </ul>
		</li>
		<li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
	  </ul>
	  <?php } ?>
	</header>
	<?php } ?>
<?php } ?>

<script type="text/javascript">

	dialog_orderpop = $("#dialog-form_orderpop").dialog({
		autoOpen: false,
		height: 400,
		width: 400,
		modal: true,
		/*buttons: {
			Done: function() {
				//$(this).dialog("close");
				dialog_orderpop.dialog("close");
				//html = '<a  data-toggle="tooltip" onclick="check()" title="check" class="btn btn-primary check" style="margin-top: 10%">Check</a>';
				//$('.custcheck').append(html);
			}
		},*/
	});

	dialog_ordercancel = $("#dialog-form_ordercancel").dialog({
		autoOpen: false,
		height: 400,
		width: 400,
		modal: true,
		/*buttons: {
			Done: function() {
				//$(this).dialog("close");
				dialog_orderpop.dialog("close");
				//html = '<a  data-toggle="tooltip" onclick="check()" title="check" class="btn btn-primary check" style="margin-top: 10%">Check</a>';
				//$('.custcheck').append(html);
			}
		},*/
	});
	

function accept_order_screen(){
	//alert('inn');
	window.location = 'index.php?route=catalog/werapending_order&token=<?php echo $token; ?>';
}




</script>