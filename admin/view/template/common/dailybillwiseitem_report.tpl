<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="sucess" style="overflow-y: hidden;overflow-x: hidden;">
    <div class="page-header">
	    <div class="container-fluid">
		      <h1><?php echo $heading_title; ?></h1>
		      <ul class="breadcrumb">
		         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		         <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		          <?php } ?>
		      </ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
		    <div class="panel-heading">
			    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
		    </div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div class="panel-body">
				    <div class="well">
					   <div class="row">
					       <div class="col-sm-12">
					       		<center><h4><b>Select Date</b></h4></center><br>
					       		<div class="col-sm-offset-3">
						       		<div class="form-row">
									    <div class="col-sm-3">
									    	<label>Start Date</label>
									     	<input type="text" name='filter_startdate' value="<?php echo $startdate?>" class="form-control form_datetime" placeholder="Start Date">
									    </div>
									    <div class="col-sm-3">
									    	<label>End Date</label>
									    	<input type="text" name='filter_enddate' value="<?php echo $enddate?>" class="form-control form_datetime" placeholder="End Date">
									    </div>
									    <div  class="col-sm-3">
								       		<label>Bill No</label>
									    	<input type="text" id="filter_billno" name="filter_billno" value="<?php echo $billno ?>" class="form-control" placeholder=" Bill NO">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<br>
									<center><input type="submit" name="submit" class="btn btn-primary" value="Show"></center>
								</div>
							</div>
					    </div>
					</div>
					
				 	<div class="col-sm-offset-10">
				 		<button style="display:none;" id="print" type="button" class="btn btn-primary">Print</button>

				 		<button id="export" type="button" class="btn btn-primary">Export</button>
				 		<button id="excel" type="button" class="btn btn-primary">Export Excel</button>
				 	</div>
					<div class="col-sm-7 col-sm-offset-3">
					<h4>
					<center>
						<?php echo HOTEL_NAME ?>
						<?php echo HOTEL_ADD ?>
					</center><h4>
					<h3 style="border-top: 1px solid;"><br><?php echo date('d/m/Y'); ?></h3>
					<?php date_default_timezone_set("Asia/Kolkata");?>
					<h3 style="text-align: right;margin-top: -20px;"><?php echo date('h:i:sa'); ?></h3>
					<center><h3><b>Billwise Item Report</b></h3></center>
					<h3>From : <?php echo $startdate; ?></h3>
					<h3 style="text-align: right;margin-top: -20px;">To : <?php echo $enddate; ?></h3><br>
					  <table class="table table-bordered table-hover" style="text-align: center;">
						
						
						<?php if(isset($_POST['submit'])){ ?>
							<?php $discount = 0; $grandtotal = 0; $vat = 0; $gst = 0; $discountvalue = 0; $afterdiscount = 0; $stax = 0; $roundoff = 0; $total = 0; $advance = 0;?>
							  	<?php foreach ($billdatas as $key => $data) { //echo "<pre>"; print_r($data);exit(); ?>
							  		
							  		<tr>
							  			<td colspan="10"><b><?php echo $data['b_count'] ?><b></td>
							  		</tr>
								  		<tr>
											<th style="text-align: center;">Ref No</th>
											<th style="text-align: center;">Food</th>
											<th style="text-align: center;">GST</th>
											<th style="text-align: center;">F.Disc</th>
											<th style="text-align: center;">Bar</th>
											<th style="text-align: center;">VAT</th>
											<th style="text-align: center;">L.Disc</th>
											<th style="text-align: center;">Total</th>
											<th style="text-align: center;">Pay By</th>
											<th style="text-align: center;">T No</th>
										</tr>
								  		<tr style = >
									  		<td><?php echo $data['order_no'] ?></td>
									  		<?php if($data['food_cancel'] == 1) { ?>
									  			<td><?php echo $data['ftotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ftotal'] ?></td>
									  		<?php } ?>
									  		<td><?php echo $data['gst'] ?></td>
									  		<td><?php echo $data['ftotalvalue'] ?></td>
									  		<?php if($data['liq_cancel'] == 1) { ?>
									  			<td><?php echo $data['ltotal'] = 0 ?></td>
									  		<?php } else { ?>
									  			<td><?php echo $data['ltotal'] ?></td>
									  		<?php } ?>
									  		<td><?php echo $data['vat'] ?></td>
									  		<td><?php echo $data['ltotalvalue'] ?></td>
									  		<?php if($INCLUSIVE == 1) { ?>
									  			<td><?php echo round($data['ftotal'] + $data['ltotal'])?></td>
									  		<?php } else { ?>
									  			<td><?php echo round($data['grand_total'] - ( $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue'])); ?></td>
									  		<?php } ?>
									  		<?php if($data['pay_cash'] != '0') { ?>
									  			<td>Cash</td>
									  		<?php } else if($data['pay_card'] != '0') { ?>
									  			<td>Card</td>
									  		<?php } else if($data['onac'] != '0.00') { ?>
									  			<td>OnAc</td>
									  		<?php } else if($data['mealpass'] != '0') { ?>
									  			<td>Meal Pass</td>
									  		<?php } else if($data['pass'] != '0') { ?>
									  			<td>Pass</td>
									  		<?php } else if($data['pay_card'] != '0' && $data['pay_cash'] != '0' && $data['onac'] != '0.00') { ?>
									  			<td>Cash+Card</td>
									  		<?php } else { ?>
									  			<td>Pending</td>
									  		<?php } ?>
									  		<td><?php echo $data['t_name'] ?></td>
									  	</tr>
									  	<?php 
									  	 	  if($data['liq_cancel'] == 1){
									  		  	$vat = 0;
									  		  } else{
									  		  	$vat = $vat + $data['vat'];
									  		  }
									  		  if($data['food_cancel'] == 1){
									  		  	$gst = 0;
									  		  } else{
									  		  	$gst = $gst + $data['gst'];
									  		  }
									  		  $stax = $stax + $data['stax'];
									  		  if($data['food_cancel'] == 1){
									  		  	$data['ftotalvalue'] = 0; 
									  		  } 
									  		  if($data['liq_cancel'] == 1){
									  		  	$data['ltotalvalue'] = 0;
									  		  }
									  		  $discount = $data['ftotalvalue'] + $data['ltotalvalue'];
									  		  if($INCLUSIVE == 1){
									  		  	$grandtotal = round($grandtotal + $data['ftotal'] + $data['ltotal']); 
									  		  	$total = $grandtotal + $stax;
									  		  } else{
								  		  	 	$grandtotal = round($grandtotal + $data['grand_total'] - ($data['vat'] + $data['gst'] + $data['stax']) + ($data['ftotalvalue'] + $data['ltotalvalue']));
								  		  	 	$total = $grandtotal + $gst + $vat + $stax; 
									  		  }
									  		  $discountvalue = $discountvalue + $discount;
									  		  $afterdiscount = $total - $discountvalue;
									  		  $roundoff = $roundoff + $data['roundtotal'];
									  		  $advance = $advance + $data['advance_amount'];
									  	?>

									  	<tr>
											<th style="text-align: center;">Sr.No</th>
											<th style="text-align: center;">Kot.No</th>
											<th colspan="5" style="text-align: center;">Item Name</th>
											<th style="text-align: center;">Rate</th>
											<th style="text-align: center;">Qty</th>
											<th style="text-align: center;">Total</th>
										</tr>
									  	
									  	<?php foreach($data['sub_data'] as $skey => $idata) { //echo "<pre>"; print_r($idata);exit();?>
											<tr>
												<td><?php echo $idata['sr_no'] ?></td>
												<td><?php echo $idata['kot_no'] ?></td>
												<td colspan="5" ><?php echo $idata['name'] ?></td>
												<td><?php echo $idata['rate'] ?></td>
												<td><?php echo $idata['qty'] ?></td>
												<td><?php echo $idata['amt'] ?></td>
											</tr>
										<?php } ?>
							  		<?php } ?>
							  	<tr>
							  		<td colspan="6"><b>Bill Total :</b></td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo $grandtotal ?></b></td>
							  	</tr>
							  	<tr>
							  		<td colspan="6">O- Chrg Amt (+) : </td>
							  		<td colspan="5" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Vat Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $vat ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">S- Chrg Amt (+) : </td>
							  		<td colspan="5" style="text-align: left;"><?php echo $stax ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">GST Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $gst ?></td>
						  		</tr>
						  			<tr>
							  		<td colspan="6">KKC Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;">0</td>
						  		</tr>
						  			<tr>
							  		<td colspan="6">SBC Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">R-Off Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $roundoff ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6"><b>Total :</b></td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo ($total + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Discount Amt (-) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $discountvalue ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Cancel BIll Amt (-) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $cancelamount ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Comp. Amt (-) :</td>
							  		<td colspan="5" style="text-align: left;">0</td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Advance. Amt (-):</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $advance ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6"><b>Net Amount :</b></td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6">Advance. Amt (+) :</td>
							  		<td colspan="5" style="text-align: left;"><?php echo $advancetotal ?></td>
						  		</tr>
						  		<tr>
							  		<td colspan="6"><b>Grand Total (+) :</td>
							  		<td colspan="5" style="text-align: left;"><b><?php echo ($afterdiscount + $roundoff) ?></b></td>
						  		</tr>
					  	<?php } ?>
					  </table>
				 	</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	 	$(".form_datetime").datepicker();

	 	$('#print').on('click', function() {
		  var url = 'index.php?route=catalog/billwiseitem_report/prints&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		function close_fun_1(){
			window.location.reload();
		}


		$('#export').on('click', function() {
		  var url = 'index.php?route=catalog/billwiseitem_report/export&token=<?php echo $token; ?>';

		  var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		    //setTimeout(close_fun_1, 50);
		});

		$('#excel').on('click', function() {
			var url = 'index.php?route=catalog/billwiseitem_report/excel&token=<?php echo $token; ?>';
			var filter_startdate = $('input[name=\'filter_startdate\']').val();
		  var filter_enddate = $('input[name=\'filter_enddate\']').val();

		  if (filter_startdate) {
			  url += '&filter_startdate=' + encodeURIComponent(filter_startdate);
		  }

		  if (filter_enddate) {
			  url += '&filter_enddate=' + encodeURIComponent(filter_enddate);
		  }
		    location = url;
		});

		function close_fun_1(){
			window.location.reload();
		}
	</script>
	<style>
		 td,th {
			  font-size: 20px;
			  color: black;
			}

		h3,h4 {
			color: black;
		}
	</style>

</div>
<?php echo $footer; ?>