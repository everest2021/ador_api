<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  	<div class="page-header">
		<div class="container-fluid">
	  		<div class="pull-right">
				<button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
  			<h1><?php echo $heading_title; ?></h1>
  			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
  			</ul>
		</div>
  	</div>
  	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
	  		<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
	  		</div>
	  		<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
		  			<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-username"><?php echo $entry_username; ?></label>
						<div class="col-sm-10">
			  				<input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
			  				<?php if ($error_username) { ?>
			  					<div class="text-danger"><?php echo $error_username; ?></div>
			  				<?php } ?>
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-user-group"><?php echo $entry_user_group; ?></label>
						<div class="col-sm-10">
			  				<select name="user_group_id" id="input-user-group" class="form-control">
								<?php foreach ($user_groups as $user_group) { ?>
									<?php if ($user_group['user_group_id'] == $user_group_id) { ?>
										<option value="<?php echo $user_group['user_group_id']; ?>" selected="selected"><?php echo $user_group['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $user_group['user_group_id']; ?>"><?php echo $user_group['name']; ?></option>
									<?php } ?>
								<?php } ?>
			  				</select>
						</div>
		  			</div> 
		  			 <div class="form-group">
						<label class="col-sm-2 control-label" for="input-employee-group"><?php echo "Employee"; ?></label>
						<div class="col-sm-10">
			  				<select name="employee_id" id="input_employee_group" class="form-control">
								<?php foreach ($employee_datas as $employee) { ?>
									<?php if ($employee['waiter_id'] == $employee_id) { ?>
										<option value="<?php echo $employee['waiter_id']; ?>" selected="selected"><?php echo $employee['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $employee['waiter_id']; ?>"><?php echo $employee['name']; ?></option>
									<?php } ?>
								<?php } ?>
			  				</select>
						</div>  
		  			</div>
		  			<div class="form-group required" style="display: none;">
						<label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
						<div class="col-sm-10">
			  				<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
			  				<?php if ($error_firstname) { ?>
			  					<div class="text-danger"><?php echo $error_firstname; ?></div>
			  				<?php } ?>
						</div>
		  			</div>
		  			<div class="form-group required" style="display: none;">
						<label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
						<div class="col-sm-10">
			  				<input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
			  				<?php if ($error_lastname) { ?>
			  					<div class="text-danger"><?php echo $error_lastname; ?></div>
			  				<?php } ?>
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
						<div class="col-sm-10">
			  				<input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
						</div>
		  			</div>
		  			<div class="form-group" style="display: none;">
						<label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
						<div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
			 	 			<input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
						</div>
		  			</div>
		  			<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
						<div class="col-sm-10">
			 	 			<input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" autocomplete="off" />
			  				<?php if ($error_password) { ?>
			  					<div class="text-danger"><?php echo $error_password; ?></div>
			  				<?php  } ?>
						</div>
		  			</div>
		  			<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
						<div class="col-sm-10">
			  				<input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
			  				<?php if ($error_confirm) { ?>
			  					<div class="text-danger"><?php echo $error_confirm; ?></div>
			  				<?php  } ?>
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
			  				<select name="status" id="input-status" class="form-control">
								<?php if ($status) { ?>
									<option value="0"><?php echo $text_disabled; ?></option>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<?php } else { ?>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<option value="1"><?php echo $text_enabled; ?></option>
								<?php } ?>
			  				</select>
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_cancel_kot; ?></label>
						<div class="col-sm-10">
							<select name="cancelkot" id="input-cancelkot" class="form-control">
						        <?php foreach($cancelkots as $kkey => $kvalue){ ?>
						        	<?php if($kkey == $cancelkot){ ?>
							    		<option value="<?php echo $kkey ?>" selected="selected"><?php echo $kvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $kkey ?>"><?php echo $kvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="cancelkot" value="<?php echo $cancelkot; ?>" placeholder="<?php echo $entry_cancel_kot; ?>" class="form-control" /> -->
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo 'Check Kot'; ?></label>
						<div class="col-sm-10">
							<select name="checkkot" id="input-checkkot" class="form-control">
						        <?php foreach($checkkots as $kkey => $kvalue){ ?>
						        	<?php if($kkey == $checkkot){ ?>
							    		<option value="<?php echo $kkey ?>" selected="selected"><?php echo $kvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $kkey ?>"><?php echo $kvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="cancelkot" value="<?php echo $cancelkot; ?>" placeholder="<?php echo $entry_cancel_kot; ?>" class="form-control" /> -->
						</div>
		  			</div>
		  			  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo 'Bill Print'; ?></label>
						<div class="col-sm-10">
							<select name="billprint" id="input-billprint" class="form-control">
						        <?php foreach($billprints as $kkey => $kvalue){ ?>
						        	<?php if($kkey == $billprint){ ?>
							    		<option value="<?php echo $kkey ?>" selected="selected"><?php echo $kvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $kkey ?>"><?php echo $kvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="cancelkot" value="<?php echo $cancelkot; ?>" placeholder="<?php echo $entry_cancel_kot; ?>" class="form-control" /> -->
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_edit_bill; ?></label>
						<div class="col-sm-10">
							<select name="editbill" id="input-editbill" class="form-control">
						        <?php foreach($editbills as $skey => $svalue){ ?>
						        	<?php if($skey == $editbill){ ?>
							    		<option value="<?php echo $skey ?>" selected="selected"><?php echo $svalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $skey ?>"><?php echo $svalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="editbill" value="<?php echo $editbill; ?>" placeholder="<?php echo $entry_edit_bill; ?>" class="form-control" /> -->
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_cancel_bill; ?></label>
						<div class="col-sm-10">
							<select name="cancelbill" id="input-cancelbill" class="form-control">
						        <?php foreach($cancelbills as $ckey => $cvalue){ ?>
						        	<?php if($ckey == $cancelbill){ ?>
							    		<option value="<?php echo $ckey ?>" selected="selected"><?php echo $cvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $ckey ?>"><?php echo $cvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="cancelbill" value="<?php echo $cancelbill; ?>" placeholder="<?php echo $entry_cancel_bill; ?>" class="form-control" /> -->
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo "Duplicate Bill"; ?></label>
						<div class="col-sm-10">
							<select name="duplicatebill" id="input-duplicatebill" class="form-control">
						        <?php foreach($duplicatebills as $dkey => $dvalue){ ?>
						        	<?php if($dkey == $duplicatebill){ ?>
							    		<option value="<?php echo $dkey ?>" selected="selected"><?php echo $dvalue ?></option>
						           <?php } else { ?>
							    		<option value="<?php echo $dkey ?>"><?php echo $dvalue ?></option>
						      		<?php } ?>
						    	<?php } ?>
				      		</select>
			  				<!-- <input type="text" name="cancelbill" value="<?php echo $cancelbill; ?>" placeholder="<?php echo $entry_cancel_bill; ?>" class="form-control" /> -->
						</div>
		  			</div>

		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-printer_type"><?php echo "Printer Type"; ?></label>
						<div class="col-sm-10">
			  				<select name="printer_type" id="input-printer_type" class="form-control">
					  			<option value="">Please Select</option>

								<?php foreach ($printer_types as $key => $value) { ?>
					  				<?php  if ($value == $printer_type) { ?>
					  					<option value="<?php echo $key?>" selected="selected"><?php echo $value; ?></option>
					  				<?php } else { ?>
					  					<option value="<?php echo $key?>"><?php echo $value; ?></option>
									<?php } ?>
								<?php }?>
			  				</select>
						</div>
		  			</div>
		  			<div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo "Printer Name"; ?></label>
						<div class="col-sm-10">
			  				<input type="text" name="printer_name" value="<?php echo $printer_name; ?>" placeholder="<?php echo "Printer Name"; ?>" class="form-control" />
						</div>
		  			</div>
		  			     
				</form>
	  		</div>
		</div>
  	</div>
</div>
<?php echo $footer; ?> 