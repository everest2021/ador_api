<?php
class ModelCatalogOrder extends Model {
	public function addOrder($data) {

		
		$this->log->write('---------------------------Order Model Start----------------------------------');
		//$this->log->write('User Name : ' .$this->user->getUserName());

		$order_info_report_id = $this->db->query("SELECT order_id from oc_order_info_report order by order_id DESC LIMIT 1");
		//if($order_info_report_id->num_rows > 0){
			$order_info_id = $this->db->query("SELECT order_id from oc_order_info  order by order_id DESC LIMIT 1");

			if($order_info_id->num_rows == 0){
				if($order_info_report_id->num_rows > 0){
					$increment_order_info_report = $order_info_report_id->row['order_id'];
				} else {
					$increment_order_info_report = 0;
				}
				$neworder_id = $increment_order_info_report + 1;
				$this->db->query("ALTER TABLE oc_order_info AUTO_INCREMENT = ".$neworder_id." ");
			}
		//}

		$order_items_report_id = $this->db->query("SELECT id from oc_order_items_report order by id DESC LIMIT 1");
		//if($order_items_report_id->num_rows > 0){
			$order_items_id = $this->db->query("SELECT id from oc_order_items  order by id DESC LIMIT 1");

			if($order_items_id->num_rows == 0){
				if($order_items_report_id->num_rows > 0){
					$increment_order_item_report = $order_items_report_id->row['id'];
				} else {
					$increment_order_item_report = 0;
				}
				$neworder_itemid = $increment_order_item_report + 1;
				$this->db->query("ALTER TABLE oc_order_items AUTO_INCREMENT = ".$neworder_itemid." ");
			}
		//}
		
		$ftotal = 0;
		$ftotal_discount = 0;
		$ftotalvalue = 0;
		$gst = 0;
		$staxfood = 0;
		$ltotal = 0;
		$ltotal_discount = 0;
		$ltotalvalue = 0;
		$vat = 0;
		$staxliq = 0;
		$stax = 0;
		foreach($data['po_datas'] as $pkeys => $pvalues){
			
			/*if($pvalues['nc_kot_status'] != 1){
				$data['po_datas'][$pkeys]['amt'] = $pvalues['rate'] * $pvalues['qty'];
				$pvalues['amt'] = $pvalues['rate'] * $pvalues['qty'];
			}*/

			if(!isset($pvalues['is_liq'])){
				$pvalues['is_liq'] = 0;
			}

			if($pvalues['cancelstatus'] == 0 && $pvalues['nc_kot_status'] == 0){
				$data['po_datas'][$pkeys]['amt'] = $pvalues['rate'] * $pvalues['qty'];
				$pvalues['amt'] = $pvalues['rate'] * $pvalues['qty'];
				if($pvalues['is_liq'] == '0' || $pvalues['is_liq'] == 0){
					$ftotal = ($ftotal) + $pvalues['amt'];
				} else if($pvalues['is_liq'] == '1' || $pvalues['is_liq'] == 1) {
					$ltotal = $ltotal + $pvalues['amt'];
				}
			}
		}

		$servicechargefood = $this->model_catalog_order->get_settings('SERVICE_CHARGE_FOOD');
		$servicechargeliq = $this->model_catalog_order->get_settings('SERVICE_CHARGE_LIQ');
		foreach($data['po_datas'] as $pkeys => $pvalues){
			
			if($pvalues['cancelstatus'] == 0 && $pvalues['nc_kot_status'] == 0){
				$data['po_datas'][$pkeys]['amt'] = $pvalues['rate'] * $pvalues['qty'];
				$pvalues['amt'] = $pvalues['rate'] * $pvalues['qty'];
				if($pvalues['is_liq'] == 0){
					if($data['fdiscountper'] > 0){
						$discount_value = $pvalues['amt'] * ($data['fdiscountper']/100);
						$afterdiscountamt = ($pvalues['amt'] - $discount_value);
					} elseif($data['discount'] > 0){
						$discount_per = (($data['discount'])/$ftotal)*100;
						$discount_value = $pvalues['amt'] * ($discount_per/100);
						$afterdiscountamt = ($pvalues['amt'] - $discount_value);
					} else {
						$afterdiscountamt = ($pvalues['amt']);
						$discount_value = 0;
					}
					$staxfood_1 = 0;
					if($servicechargefood > 0){
						if($data['service_charge'] == 1){
							$staxfood_1 = ($afterdiscountamt*($servicechargefood/100));		
						}else{
							$staxfood_1 = 0;
						}
					}
					$staxfood = $staxfood + $staxfood_1;
					$tax1_value = (($afterdiscountamt) + ($staxfood_1)) * (($pvalues['tax1']) / 100);
			  		$tax2_value = (($afterdiscountamt) + ($staxfood_1)) * (($pvalues['tax2']) / 100);
			  		
			  		$data['po_datas'][$pkeys]['tax1_value'] = $tax1_value;
			  		$data['po_datas'][$pkeys]['tax2_value'] = $tax2_value;
			  		$data['po_datas'][$pkeys]['stax'] = $staxfood_1;

			  		$gst = $gst + $tax1_value + $tax2_value;
			  		$ftotalvalue = $ftotalvalue + $discount_value;
			  		$ftotal_discount = $ftotal_discount + $afterdiscountamt; 
				} else {
					if($data['ldiscountper'] > 0){
						$discount_value = $pvalues['amt'] * ($data['ldiscountper']/100);
						$afterdiscountamt = $pvalues['amt'] - $discount_value;
					} elseif($data['ldiscount'] > 0){
						$discount_per = (($data['ldiscount'])/$ltotal)*100;
						$discount_value = $pvalues['amt'] * ($discount_per/100);
						$afterdiscountamt = $pvalues['amt'] - $discount_value;
					} else {
						$afterdiscountamt = $pvalues['amt'];
						$discount_value = 0;
					}
					$staxliq_1 = 0;
					if($servicechargeliq > 0){
						if ($data['service_charge'] == 1) {
							$staxliq_1 = $afterdiscountamt*($servicechargeliq/100);		
						}else{
							$staxliq_1 = 0;
						}
					}
					$staxliq = $staxliq + $staxliq_1;
					$tax1_value = ($afterdiscountamt + $staxliq_1) * ($pvalues['tax1'] / 100);
			  		$tax2_value = ($afterdiscountamt + $staxliq_1) * ($pvalues['tax2'] / 100);
			  		
			  		$data['po_datas'][$pkeys]['tax1_value'] = $tax1_value;
			  		$data['po_datas'][$pkeys]['tax2_value'] = $tax2_value;
			  		$data['po_datas'][$pkeys]['stax'] = $staxliq_1;

			  		$vat = $vat + $tax1_value + $tax2_value;
			  		$ltotalvalue = $ltotalvalue + $discount_value;
			  		$ltotal_discount = $ltotal_discount + $afterdiscountamt; 
				}
			} else {
				$data['po_datas'][$pkeys]['stax'] = 0;
			}
		}
		$stax = $staxfood + $staxliq;
		if($this->model_catalog_order->get_settings('INCLUSIVE') == 1){
			$grand_total = ($ftotal + $ltotal + $staxfood + $staxliq) - ($ftotalvalue + $ltotalvalue);
		} else{
			$grand_total = ($ftotal + $ltotal + $gst + $vat + $staxfood + $staxliq) - ($ftotalvalue + $ltotalvalue);
		}
		$grand_totals = number_format($grand_total,2,'.','');
		$grand_total_explode = explode('.', $grand_totals);
		$roundtotal = 0;
		if(isset($grand_total_explode[1])){
			if($grand_total_explode[1] >= 50){
				$roundtotals = 100 - $grand_total_explode[1];
				$roundtotal = ($roundtotals / 100); 
			} else {
				$roundtotal = -($grand_total_explode[1] / 100);
			}
		}
		$dtotalvalue = 0;
		if($data['dchargeper'] > 0){
			$dtotalvalue = $grand_total * ($data['dchargeper'] / 100);
		} elseif($data['dcharge'] > 0){
			$dtotalvalue = $data['dcharge'];
		}


		$data['ftotal'] = $ftotal;
		$data['ftotal_discount'] = $ftotal_discount;
		$data['ftotalvalue'] = $ftotalvalue;
		$data['staxfood'] = $staxfood;
		$data['ltotal'] = $ltotal;
		$data['ltotal_discount'] = $ltotal_discount;
		$data['ltotalvalue'] = $ltotalvalue;
		$data['staxliq'] = $staxliq;
		$data['stax'] = $stax;
		$data['grand_total'] = $grand_total;
		$data['roundtotal'] = $roundtotal;
		$data['dtotalvalue'] = $dtotalvalue;


		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$merge_status = 0;
		if($data['orderid'] == '0'){
			$order_info_daatsss = $this->db->query("SELECT * from oc_order_info WHERE table_id = '".$data['table_id']."' AND bill_status = '0' AND order_no = '0' AND bill_date = '".$last_open_date."' AND cancel_status = '0' ");
			
			if($order_info_daatsss->num_rows > 0){
				$order_datazz = $order_info_daatsss->row;

				$order_id = $order_datazz['order_id'];

				$data['gst'] = $data['gst'] + $order_datazz['gst'];
				$data['vat'] = $data['vat'] + $order_datazz['vat'];
				$data['cess'] = $data['cess'] + $order_datazz['cess'];

				$data['stax'] = $data['stax'] + $order_datazz['stax'];
				$data['ltotal'] = $data['ltotal'] + $order_datazz['ltotal'];
				$data['ftotal'] = $data['ftotal'] + $order_datazz['ftotal'];
				$data['staxliq'] = $data['staxliq'] + $order_datazz['staxliq'];
				$data['staxfood'] = $data['staxfood'] + $order_datazz['staxfood'];
				$data['roundtotal'] = $data['roundtotal']+  $order_datazz['roundtotal'];
				$data['total_items'] = $data['total_items'] + $order_datazz['total_items'];
				$data['dtotalvalue'] = $data['dtotalvalue'] + $order_datazz['dtotalvalue'];
				$data['grand_total'] = $data['grand_total'] + $order_datazz['grand_total'];
				$data['ftotalvalue'] = $data['ftotalvalue'] + $order_datazz['ftotalvalue'];
				$data['ltotalvalue'] = $data['ltotalvalue']  + $order_datazz['ltotalvalue'];
				$data['item_quantity'] = $data['item_quantity'] + $order_datazz['item_quantity'];
				$data['ftotal_discount'] = $data['ftotal_discount'] + $order_datazz['ftotal_discount'];
				$data['ltotal_discount'] = $data['ltotal_discount'] + $order_datazz['ltotal_discount'];

				$merge_status = 1;

				$this->db->query("UPDATE " . DB_PREFIX . "order_info SET  
					`ftotal` = '" . $this->db->escape($data['ftotal']) . "',
					`ftotal_discount` = '" . $this->db->escape($data['ftotal_discount']) . "',
					`gst` = '" . $this->db->escape($data['gst']) . "',
					`ltotal` = '" . $this->db->escape($data['ltotal']) . "',
					`ltotal_discount` = '" . $this->db->escape($data['ltotal_discount']) . "',
					`vat` = '" . $this->db->escape($data['vat']) . "',
					`cess` = '" . $this->db->escape($data['cess']) . "',
					`staxfood` = '" . $this->db->escape($data['staxfood']) . "',
					`staxliq` = '" . $this->db->escape($data['staxliq']) . "',
					`stax` = '" . $this->db->escape($data['stax']) . "',
					`ftotalvalue` = '" . $this->db->escape($data['ftotalvalue']) . "',
					`fdiscountper` = '" . $this->db->escape($data['fdiscountper']) . "',
					`discount` = '" . $this->db->escape($data['discount']) . "',
					`ldiscount` = '" . $this->db->escape($data['ldiscount']) . "',
					`ldiscountper` = '" . $this->db->escape($data['ldiscountper']) . "',
					`ltotalvalue` = '" . $this->db->escape($data['ltotalvalue']) . "',
					`dtotalvalue` = '" . $this->db->escape($data['dtotalvalue']) . "',
					`grand_total` = '" . $this->db->escape($data['grand_total']) . "',
					`roundtotal` = '" . $this->db->escape($data['roundtotal']) . "',
					`total_items` = '" . $this->db->escape($data['total_items']) . "',
					`item_quantity` = '" . $this->db->escape($data['item_quantity']) . "'
					WHERE order_id = '".$order_id."'
					");
			}
		}
		
		if($data['nc_kot_status'] == 1){
			$data['nc_kot_status'] = 1;
		} else {
			$data['nc_kot_status'] = 0;
		}

		//****************************************** Start : To Make KOT number different for liquior day wise *********************************** //

		$this->log->write('****** Kot No Start ******** ');

		$kotno2 = $this->db->query("SELECT `kot_no` FROM `oc_order_items` WHERE `bill_date` = '".$last_open_date."' AND is_liq = '0' order by `kot_no` DESC LIMIT 1");
		//$this->log->write('queryy ' .print_r("SELECT `kot_no` FROM `oc_order_items` WHERE `bill_date` = '".$last_open_date."' AND is_liq = 0 order by `kot_no` DESC LIMIT 1",true));
		if($kotno2->num_rows > 0){
			$kot_no2 = $kotno2->row['kot_no'];
			$kotno = $kot_no2 + 1;
		} else{
			$kotno = 1;
		}
			
		$kotno1 = $this->db->query("SELECT `kot_no` FROM `oc_order_items`  WHERE `bill_date` = '".$last_open_date."' AND is_liq = '1' order by `kot_no` DESC LIMIT 1");
		if($kotno1->num_rows > 0){
			$kot_no1 = $kotno1->row['kot_no'];
			$botno = $kot_no1 + 1;
		} else{
			$botno = 1;
		}

		//******************************************                   End                         *********************************** //
		if(!empty($data['orderid'])){
			$preinfo_datas = $this->db->query("SELECT * FROM `oc_order_info` WHERE order_id = '".$data['orderid']."' ")->row;
			if($preinfo_datas['payment_status'] == '1'){
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_info_modify SET 
								`batch_id` = '" . $this->db->escape($preinfo_datas['order_id']) . "',
								`app_order_id` = '" . $this->db->escape($preinfo_datas['app_order_id']) . "',
								`location` = '" . $this->db->escape($preinfo_datas['location']) . "', 
								`location_id` = '" . $this->db->escape($preinfo_datas['location_id']) ."',
								`parcel_status` = '" . $this->db->escape($preinfo_datas['parcel_status']) ."', 
								`cust_name` = '" . $this->db->escape($preinfo_datas['cust_name']) ."', 
								`cust_id` = '" . $this->db->escape($preinfo_datas['cust_id']) ."',
								`cust_contact` = '" . $this->db->escape($preinfo_datas['cust_contact']) ."',
								`cust_address` = '" . $this->db->escape($preinfo_datas['cust_address']) ."',
								`cust_email` = '" . $this->db->escape($preinfo_datas['cust_email']) ."',
								`gst_no` = '" . $this->db->escape($preinfo_datas['gst_no']) ."',
								`t_name` = '" . $this->db->escape($preinfo_datas['t_name']) . "',
								`table_id` = '" . $this->db->escape($preinfo_datas['table_id']) . "',  
								`waiter_code` = '" . $this->db->escape($preinfo_datas['waiter_code']) . "', 
								`waiter` = '" . $this->db->escape($preinfo_datas['waiter']) . "',  
								`waiter_id` = '" . $this->db->escape($preinfo_datas['waiter_id']) . "', 
								`captain_code` = '" . $this->db->escape($preinfo_datas['captain_code']) . "',   
								`captain` = '" . $this->db->escape($preinfo_datas['captain']) . "',  
								`captain_id` = '" . $this->db->escape($preinfo_datas['captain_id']) . "',
								`person` = '" . $this->db->escape($preinfo_datas['person']) . "',
								`ftotal` = '" . $this->db->escape($preinfo_datas['ftotal']) . "',
								`ftotal_discount` = '" . $this->db->escape($preinfo_datas['ftotal_discount']) . "',
								`gst` = '" . $this->db->escape($preinfo_datas['gst']) . "',
								`ltotal` = '" . $this->db->escape($preinfo_datas['ltotal']) . "',
								`ltotal_discount` = '" . $this->db->escape($preinfo_datas['ltotal_discount']) . "',
								`vat` = '" . $this->db->escape($preinfo_datas['vat']) . "',
								`cess` = '" . $this->db->escape($preinfo_datas['cess']) . "',
								`staxfood` = '" . $this->db->escape($preinfo_datas['staxfood']) . "',
								`staxliq` = '" . $this->db->escape($preinfo_datas['staxliq']) . "',
								`stax` = '" . $this->db->escape($preinfo_datas['stax']) . "',
								`ftotalvalue` = '" . $this->db->escape($preinfo_datas['ftotalvalue']) . "',
								`fdiscountper` = '" . $this->db->escape($preinfo_datas['fdiscountper']) . "',
								`discount` = '" . $this->db->escape($preinfo_datas['discount']) . "',
								`ldiscount` = '" . $this->db->escape($preinfo_datas['ldiscount']) . "',
								`ldiscountper` = '" . $this->db->escape($preinfo_datas['ldiscountper']) . "',
								`ltotalvalue` = '" . $this->db->escape($preinfo_datas['ltotalvalue']) . "',
								`dtotalvalue` = '" . $this->db->escape($preinfo_datas['dtotalvalue']) . "',
								`dchargeper` = '" . $this->db->escape($preinfo_datas['dchargeper']) . "',
								`dcharge` = '" . $this->db->escape($preinfo_datas['dcharge']) . "',
								`grand_total` = '" . $this->db->escape($preinfo_datas['grand_total']) . "',
								`advance_billno` = '" . $this->db->escape($preinfo_datas['advance_id']) . "',
								`advance_amount` = '" . $this->db->escape($preinfo_datas['advance_amount']) . "',
								`roundtotal` = '" . $this->db->escape($preinfo_datas['roundtotal']) . "',
								`total_items` = '" . $this->db->escape($preinfo_datas['total_items']) . "',
								`item_quantity` = '" . $this->db->escape($preinfo_datas['item_quantity']) . "',
								`nc_kot_status` = '" . $this->db->escape($preinfo_datas['nc_kot_status']) . "',
								`rate_id` = '" . $this->db->escape($preinfo_datas['rate_id']) . "',
								`bill_date` = '" . $this->db->escape($preinfo_datas['bill_date']) . "',
								`date_added` = '" . $this->db->escape($preinfo_datas['date_added']) . "',
								`time_added` = '" . $this->db->escape($preinfo_datas['time_added']) . "',
								`kot_no` = '" . $this->db->escape($preinfo_datas['kot_no']) . "',
								`date` = '" . $this->db->escape($preinfo_datas['date']) . "',
								`pay_card` = '" . $this->db->escape($preinfo_datas['pay_card']) . "',
								`payment_status` = '" . $this->db->escape($preinfo_datas['payment_status']) . "',
								`total_payment` = '" . $this->db->escape($preinfo_datas['total_payment']) . "',
								`pay_method` = '" . $this->db->escape($preinfo_datas['pay_method']) . "',
								`pay_online` = '" . $this->db->escape($preinfo_datas['pay_online']) . "',
								`pay_cash` = '" . $this->db->escape($preinfo_datas['pay_cash']) . "',
								`time` = '" . $this->db->escape($preinfo_datas['time']) . "',
								`bill_status` = '1',
								`bill_modify` = '1',
								`login_id` = '" . $this->db->escape($preinfo_datas['login_id']) . "',
								`login_name` = '" . $this->db->escape($preinfo_datas['login_name']) . "'");	
						//$preorder_id = $preinfo_datas['order_id'];
				
				$preitem_datas = $this->db->query("SELECT *,SUM(qty) as qty FROM `oc_order_items` WHERE order_id = '".$data['orderid']."' GROUP BY code")->rows;
				foreach ($preitem_datas as $pkey => $pvalue) {
					if(!isset($pvalue['nc_kot_status'])){
						$pvalue['nc_kot_status'] = 0;
					}
					if(!isset($pvalue['nc_kot_reason'])){
						$pvalue['nc_kot_reason'] = '';
					}
					if(!isset($pvalue['transfer_qty'])){
						$pvalue['transfer_qty'] = 0;
					}
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items_modify SET
								`order_id` = '" . $this->db->escape($pvalue['order_id']) . "',
								`batch_id` = '" . $this->db->escape($pvalue['order_id']) . "',
								`bill_no` = '" . $this->db->escape($pvalue['billno']) . "',
								`code` = '" . $this->db->escape($pvalue['code']) ."',
								`name` = '" . htmlspecialchars_decode($pvalue['name']) ."', 
								`qty` = '" . $this->db->escape($pvalue['qty']) . "', 
								`rate` = '" . $this->db->escape($pvalue['rate']) . "',  
								`amt` = '" . $this->db->escape($pvalue['amt']) . "',
								`ismodifier` = '" . $this->db->escape($pvalue['ismodifier']) . "',  
								`nc_kot_status` = '" . $this->db->escape($pvalue['nc_kot_status']) . "',
								`nc_kot_reason` = '" . $this->db->escape($pvalue['nc_kot_reason']) . "',
								`transfer_qty` = '" . $this->db->escape($pvalue['transfer_qty']) . "',
								`parent_id` = '" . $this->db->escape($pvalue['parent_id']) . "', 
								`parent` = '" . $this->db->escape($pvalue['parent']) . "',  
								`subcategoryid` = '" . $this->db->escape($pvalue['subcategoryid']) . "',
								`kot_status` = '" . $this->db->escape($pvalue['kot_status']) . "',
								`kot_no` = '" . $this->db->escape($pvalue['kot_no']) . "',
								`prefix` = '" . $this->db->escape($pvalue['prefix']) . "',
								`pre_qty` = '" . $this->db->escape($pvalue['pre_qty']) . "',
								`is_liq` = '" . $this->db->escape($pvalue['is_liq']) . "',
								`is_new` = '" . $this->db->escape($pvalue['is_new']) . "',
								`cancelstatus` = '" . $this->db->escape($pvalue['cancelstatus']) . "',
								`discount_per` = '" . $this->db->escape($pvalue['discount_per']) . "',
								`discount_value` = '" . $this->db->escape($pvalue['discount_value']) . "',
								`tax1` = '" . $this->db->escape($pvalue['tax1']) . "',
								`tax2` = '" . $this->db->escape($pvalue['tax2']) . "',
								`tax1_value` = '" . $this->db->escape($pvalue['tax1_value']) . "',
								`tax2_value` = '" . $this->db->escape($pvalue['tax2_value']) . "',
								`stax` = '" . $this->db->escape($pvalue['stax']) . "',
								`captain_id` = '" . $this->db->escape($pvalue['captain_id']) . "',
								`captain_commission` = '" . $this->db->escape($pvalue['captain_commission']) . "',
								`waiter_id` = '" . $this->db->escape($pvalue['waiter_id']) . "',
								`waiter_commission` = '" . $this->db->escape($pvalue['waiter_commission']) . "',
								`message` = '" . $this->db->escape($pvalue['message']) . "',
								`date` = '" . $this->db->escape($pvalue['date']) . "',
								`time` = '" . $this->db->escape($pvalue['time']) . "',
								`bill_date` = '" . $this->db->escape($preinfo_datas['bill_date']) . "',
								`login_id` = '" . $this->db->escape($pvalue['login_id']) . "',
								`cancel_bill` = '" . $this->db->escape($pvalue['cancel_bill']) . "',
								`printstatus` = '" . $this->db->escape($pvalue['printstatus']) . "',
								`login_name` = '" . $this->db->escape($pvalue['login_name']) . "'");
				}
			}
			$this->db->query("DELETE  FROM oc_order_info WHERE order_id = '".$data['orderid']."'");
			$this->db->query("DELETE  FROM oc_order_items WHERE order_id = '".$data['orderid']."'");
			$orderno = $data['order_no'];
			$o_kot_no = $data['kot_no'];
			$date_added = $data['date_added'];
			$time_added = $data['time_added'];
		} else {
			$orderno = 0;
			// $orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."'  ORDER BY `order_no` DESC LIMIT 1")->row;
			// $orderno = 1;
			// if(isset($orderno_q['order_no'])){
			// 		$orderno = $orderno_q['order_no'] + 1;
			// }
			$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
			$o_kot_no = 1;
			if(isset($kot_no_datas['kot_no'])){
				$o_kot_no = $kot_no_datas['kot_no'] + 1;
			}
			$date_added = date('Y-m-d');
			$time_added = date('H:i:s');
		}

		// echo'<pre>';
		// print_r($o_kot_no);
		// exit;

		if(isset($this->session->data['c_id'])){
			$testdata = $this->db->query("SELECT * FROM oc_customerinfo WHERE c_id = '".$this->session->data['c_id']."'")->row;
		} else{
			$testdata['name'] = '';
			$testdata['c_id'] = '';
			$testdata['contact'] = '';
			$testdata['address'] = '';
			$testdata['email'] = '';
			$testdata['gst_no'] = '';
		}

		if(!isset($data['waiter'])){
			$data['waiter'] = '';
		}

		if(!isset($data['cancel_reason'])){
			$data['cancel_reason'] = '';
		}

		if(!isset($data['waiter_id'])){
			$data['waiter_id'] = '';
		}

		if(!isset($data['waiterid'])){
			$data['waiterid'] = '';
		}

		if(!isset($data['captain'])){
			$data['captain'] = '';
		}

		if(!isset($data['captainid'])){
			$data['captainid'] = '';
		}

		if(!isset($data['captain_id'])){
			$data['captain_id'] = '';
		}

		if(!isset($data['person'])){
			$data['person'] = '';
		}

		if($data['ftotal_discount'] > 0 || $data['ltotal_discount'] > 0){
			$resaon_discount = $data['cancel_reason'];
		} else {
			$resaon_discount = '';
		}

		if($merge_status == 0){
			if(($data['orderid'] == '' || $data['orderid'] == '0' || $data['orderid'] == NULL)){
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
								`app_order_id` = '" . $this->db->escape($data['apporderid']) . "', 
								`location` = '" . $this->db->escape($data['location']) . "', 
								`location_id` = '" . $this->db->escape($data['location_id']) ."',
								`parcel_status` = '" . $this->db->escape($data['parcel_status']) ."', 
								`cust_name` = '" . $this->db->escape($testdata['name']) ."', 
								`cust_id` = '" . $this->db->escape($testdata['c_id']) ."',
								`cust_contact` = '" . $this->db->escape($testdata['contact']) ."',
								`cust_address` = '" . $this->db->escape($testdata['address']) ."',
								`cust_email` = '" . $this->db->escape($testdata['email']) ."',
								`gst_no` = '" . $this->db->escape($testdata['gst_no']) ."',
								`t_name` = '" . $this->db->escape($data['table']) . "',
								`table_id` = '" . $this->db->escape($data['table_id']) . "',  
								`waiter_code` = '" . $this->db->escape($data['waiterid']) . "', 
								`waiter` = '" . $this->db->escape($data['waiter']) . "',  
								`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "', 
								`captain_code` = '" . $this->db->escape($data['captainid']) . "',   
								`captain` = '" . $this->db->escape($data['captain']) . "',  
								`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
								`person` = '" . $this->db->escape($data['person']) . "',
								`ftotal` = '" . $this->db->escape($data['ftotal']) . "',
								`ftotal_discount` = '" . $this->db->escape($data['ftotal_discount']) . "',
								`gst` = '" . $this->db->escape($data['gst']) . "',
								`ltotal` = '" . $this->db->escape($data['ltotal']) . "',
								`ltotal_discount` = '" . $this->db->escape($data['ltotal_discount']) . "',
								`vat` = '" . $this->db->escape($data['vat']) . "',
								`cess` = '" . $this->db->escape($data['cess']) . "',
								`staxfood` = '" . $this->db->escape($data['staxfood']) . "',
								`staxliq` = '" . $this->db->escape($data['staxliq']) . "',
								`stax` = '" . $this->db->escape($data['stax']) . "',
								`ftotalvalue` = '" . $this->db->escape($data['ftotalvalue']) . "',
								`fdiscountper` = '" . $this->db->escape($data['fdiscountper']) . "',
								`discount` = '" . $this->db->escape($data['discount']) . "',
								`ldiscount` = '" . $this->db->escape($data['ldiscount']) . "',
								`ldiscountper` = '" . $this->db->escape($data['ldiscountper']) . "',
								`ltotalvalue` = '" . $this->db->escape($data['ltotalvalue']) . "',
								`dtotalvalue` = '" . $this->db->escape($data['dtotalvalue']) . "',
								`dchargeper` = '" . $this->db->escape($data['dchargeper']) . "',
								`dcharge` = '" . $this->db->escape($data['dcharge']) . "',
								`advance_billno` = '" . $this->db->escape($data['advance_id']) . "',
								`advance_amount` = '" . $this->db->escape($data['advance_amount']) . "',
								`grand_total` = '" . $this->db->escape($data['grand_total']) . "',
								`roundtotal` = '" . $this->db->escape($data['roundtotal']) . "',
								`total_items` = '" . $this->db->escape($data['total_items']) . "',
								`item_quantity` = '" . $this->db->escape($data['item_quantity']) . "',
								`nc_kot_status` = '" . $this->db->escape($data['nc_kot_status']) . "',
								`rate_id` = '" . $this->db->escape($data['rate_id']) . "',
								`bill_date` = '".$last_open_date."',
								`date_added` = '".$date_added."',
								`time_added` = '".$time_added."',
								`kot_no` = '".$o_kot_no."',
								`discount_reason` = '".$this->db->escape($resaon_discount)."',
								`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
								`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
								`login_id` = '" . $this->db->escape($this->user->getId()) . "',
								`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
			} else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET 
								`order_id` = '" . $this->db->escape($data['orderid']) . "',
								`app_order_id` = '" . $this->db->escape($data['apporderid']) . "',
								`location` = '" . $this->db->escape($data['location']) . "', 
								`location_id` = '" . $this->db->escape($data['location_id']) ."',
								`parcel_status` = '" . $this->db->escape($data['parcel_status']) ."', 
								`cust_name` = '" . $this->db->escape($testdata['name']) ."', 
								`cust_id` = '" . $this->db->escape($testdata['c_id']) ."',
								`cust_contact` = '" . $this->db->escape($testdata['contact']) ."',
								`cust_address` = '" . $this->db->escape($testdata['address']) ."',
								`cust_email` = '" . $this->db->escape($testdata['email']) ."',
								`gst_no` = '" . $this->db->escape($testdata['gst_no']) ."',
								`t_name` = '" . $this->db->escape($data['table']) . "',
								`table_id` = '" . $this->db->escape($data['table_id']) . "',  
								`waiter_code` = '" . $this->db->escape($data['waiterid']) . "', 
								`waiter` = '" . $this->db->escape($data['waiter']) . "',  
								`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "', 
								`captain_code` = '" . $this->db->escape($data['captainid']) . "',   
								`captain` = '" . $this->db->escape($data['captain']) . "',  
								`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
								`person` = '" . $this->db->escape($data['person']) . "',
								`ftotal` = '" . $this->db->escape($data['ftotal']) . "',
								`ftotal_discount` = '" . $this->db->escape($data['ftotal_discount']) . "',
								`gst` = '" . $this->db->escape($data['gst']) . "',
								`ltotal` = '" . $this->db->escape($data['ltotal']) . "',
								`ltotal_discount` = '" . $this->db->escape($data['ltotal_discount']) . "',
								`vat` = '" . $this->db->escape($data['vat']) . "',
								`cess` = '" . $this->db->escape($data['cess']) . "',
								`staxfood` = '" . $this->db->escape($data['staxfood']) . "',
								`staxliq` = '" . $this->db->escape($data['staxliq']) . "',
								`stax` = '" . $this->db->escape($data['stax']) . "',
								`ftotalvalue` = '" . $this->db->escape($data['ftotalvalue']) . "',
								`fdiscountper` = '" . $this->db->escape($data['fdiscountper']) . "',
								`discount` = '" . $this->db->escape($data['discount']) . "',
								`ldiscount` = '" . $this->db->escape($data['ldiscount']) . "',
								`ldiscountper` = '" . $this->db->escape($data['ldiscountper']) . "',
								`ltotalvalue` = '" . $this->db->escape($data['ltotalvalue']) . "',
								`dtotalvalue` = '" . $this->db->escape($data['dtotalvalue']) . "',
								`dchargeper` = '" . $this->db->escape($data['dchargeper']) . "',
								`dcharge` = '" . $this->db->escape($data['dcharge']) . "',
								`grand_total` = '" . $this->db->escape($data['grand_total']) . "',
								`advance_billno` = '" . $this->db->escape($data['advance_id']) . "',
								`advance_amount` = '" . $this->db->escape($data['advance_amount']) . "',
								`roundtotal` = '" . $this->db->escape($data['roundtotal']) . "',
								`total_items` = '" . $this->db->escape($data['total_items']) . "',
								`item_quantity` = '" . $this->db->escape($data['item_quantity']) . "',
								`nc_kot_status` = '" . $this->db->escape($data['nc_kot_status']) . "',
								`rate_id` = '" . $this->db->escape($data['rate_id']) . "',
								`discount_reason` = '".$this->db->escape($resaon_discount)."',
								`bill_date` = '".$last_open_date."',
								`date_added` = '".$date_added."',
								`time_added` = '".$time_added."',
								`kot_no` = '".$o_kot_no."',
								`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
								`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
								`login_id` = '" . $this->db->escape($this->user->getId()) . "',
								`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
			}
			$order_id = $this->db->getLastId();
		}
		
		
		$inf  = array();
		if(!empty($this->request->get['edit'])){
			$this->db->query("UPDATE oc_order_info SET order_no = '".$data['order_no']."', bill_status = '".$data['bill_status']."', bill_modify = '1', out_time = '".date('h:i:s')."', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE order_id = '".$order_id."'");
		}

		$itemdata = '';
		// echo'<pre>';
		// 	print_r($data);
		// 	exit;
		// $this->log->write('******** Podatas Start ********* ');
		// $this->log->write('Post Data ' .print_r($data['po_datas'],true));

		

		foreach($data['po_datas'] as $pkey => $pvalues){
			
			if(!isset($pvalues['nc_kot_status'])){
				$pvalues['nc_kot_status'] = 0;
			}
			if(!isset($pvalues['is_liq'])){
				$pvalues['is_liq'] = 0;
			}
			if(!isset($pvalues['kitchen_display'])){
				$pvalues['kitchen_display'] = 0;
			}
			if(!isset($pvalues['nc_kot_reason'])){
				$pvalues['nc_kot_reason'] = '';
			}
			if(!isset($pvalues['transfer_qty'])){
				$pvalues['transfer_qty'] = 0;
			}

			if($pvalues['ismodifier'] == '0'){
				$pvalues['parent_id'] = $autoincrementparentid;
			}
			
			if(isset($pvalues['code']) && $pvalues['code'] != '') {
				if(isset($pvalues['kot_status'])) {
	             	$kot_status = $pvalues['kot_status'];
	            } else {
	             	$kot_status = 0;
	            }
	            if(isset($pvalues['pre_qty'])){
	             	if($pvalues['qty'] > $pvalues['pre_qty']){
						$prefix = '+';
						$pre_qty =$pvalues['pre_qty'];
					} else {
						$prefix = '-';
						$pre_qty =$pvalues['pre_qty'];
					}
				} else {
					$prefix = '';
					$pre_qty =0;
				}
				if($pvalues['is_liq'] == '1'){
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $botno;
					}
				} else {
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $kotno;
					}
				}
				//$this->log->write('C_no ' .print_r($c_no,true));

				
				if($pvalues['qty'] < $pvalues['pre_qty']){
					//$this->log->write("Start");
					$cancelqty = $pvalues['pre_qty'] - $pvalues['qty'];
					$cancelamt = $cancelqty * $pvalues['rate'];

					$itemname = str_replace(' ', '%20', $pvalues['name']);
					$itemname = str_replace('&', 'and', $itemname);
					$itemname = str_replace('amp;', '', $itemname);

					if($pvalues['is_liq'] == '0'){
						$kotbot = $kotno;
						$text = "Cancelled%20Kot%20-%20Orderid%20:".$order_id.",%20Item%20Name%20:".$itemname.",%20Qty%20:".$cancelqty.",%20Amt%20:".$cancelamt.",%20Kot%20no%20:".$kotno;
						
						$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
						$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
						$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
						if ($setting_value_link_1 != '') {
							$link = $link_1."&phone=".$setting_value_number."&text=".$text;
							$ret = file($link);
							//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
							//$ret = file($link); 
						}
					} else{
						$kotbot = $botno; 
						$text = "Cancelled%20Bot%20-%20Orderid%20:".$order_id.",%20Item%20Name%20:".$itemname.",%20Qty%20:".$cancelqty.",%20Amt%20:".$cancelamt.",%20Bot%20no%20:".$botno;
						$setting_value_link_1 = $this->model_catalog_order->get_settings('SMS_LINK_1');
						$link_1 = html_entity_decode($setting_value_link_1, ENT_QUOTES, 'UTF-8');
						$setting_value_number = $this->model_catalog_order->get_settings('CONTACT_NUMBER');
						if ($setting_value_link_1 != '') {
							$link = $link_1."&phone=".$setting_value_number."&text=".$text;
							$ret = file($link);
							//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
							//$ret = file($link); 
						}
					}

					if($itemdata != ''){
						if($pvalues['ismodifier'] == '0'){
							$pvalues['parent_id'] = $itemdata;
						}
					}

					if($pvalues['is_liq'] == '1' && !empty($this->request->get['edit'])){
						$this->db->query("UPDATE oc_order_items SET billno = '".$pvalues['billno']."' WHERE order_id = '".$order_id."' AND billno = '0'");
					}

					if($pvalues['is_liq'] == '0' && !empty($this->request->get['edit'])){
						$this->db->query("UPDATE oc_order_items SET billno = '".$pvalues['billno']."' WHERE order_id = '".$order_id."' AND billno = '0'");
					}

					if ($data['captain_id'] != '' ) {
						$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['code']."'")->row;
						if ($itemdata['captain_rate_value'] == 1 ) {
							$caption_commition = $pvalues['qty'] * $itemdata['captain_rate_per'];
						} else {
							$caption_commition = $pvalues['qty'] *$pvalues['rate']/100 * $itemdata['captain_rate_per'];
						}
					} else {
						$caption_commition = '0';
					}

					if ($data['waiter_id'] != '') {
						if(isset($itemdata['stweward_rate_value'])){
							if ($itemdata['stweward_rate_value'] == 1 ) {
								if($itemdata['stweward_rate_per'] != ''){
									$st_r_per = $itemdata['stweward_rate_per'];
								} else {
									$st_r_per = 0;
								}
								$stweward_commition = $pvalues['qty'] * $st_r_per;
							} else {
								if($itemdata['stweward_rate_per'] != ''){
									$st_r_per = $itemdata['stweward_rate_per'];
								} else {
									$st_r_per = 0;
								}
								$stweward_commition = $pvalues['qty'] * $pvalues['rate']/100 * $st_r_per;
							}
						} else {
							$stweward_commition = 0;
						}
					} else {
						$stweward_commition = '0';
					}
				 	
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
					`order_id` = '" . $this->db->escape($order_id) . "',   
					`code` = '" . $this->db->escape($pvalues['code']) ."',
					`name` = '" . htmlspecialchars_decode($pvalues['name']) ."', 
					`qty` = '" . $this->db->escape($cancelqty) . "', 
					`rate` = '" . $this->db->escape($pvalues['rate']) . "',  
					`amt` = '" . $this->db->escape($cancelamt) . "',
					`ismodifier` = '" . $this->db->escape($pvalues['ismodifier']) . "',
					`nc_kot_status` = '" . $this->db->escape($pvalues['nc_kot_status']) . "',
					`nc_kot_reason` = '" . $this->db->escape($pvalues['nc_kot_reason']) . "',
					`transfer_qty` = '" . $this->db->escape($pvalues['transfer_qty']) . "',
					`parent_id` = '" . $this->db->escape($pvalues['parent_id']) . "',
					`parent` = '" . $this->db->escape($pvalues['parent']) . "',   
					`subcategoryid` = '" . $this->db->escape($pvalues['subcategoryid']) . "',
					`kot_status` = '" .$this->db->escape($kot_status). "',
					`kot_no` = '" .$this->db->escape($kotbot). "',
					`pre_qty` = '" .$this->db->escape($pre_qty). "',
					`is_liq` = '" . $this->db->escape($pvalues['is_liq']) . "',
					`message` = '" . $this->db->escape($pvalues['message']) . "',
					`discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
					`discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
					`tax1` = '" . $this->db->escape($pvalues['tax1']) . "',
					`tax2` = '" . $this->db->escape($pvalues['tax2']) . "',
					`tax1_value` = '" . $this->db->escape($pvalues['tax1_value']) . "',
					`tax2_value` = '" . $this->db->escape($pvalues['tax2_value']) . "',
					`stax` = '" . $this->db->escape($pvalues['stax']) . "',
					`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
					`captain_commission` = '" . $this->db->escape($caption_commition) . "',
					`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "',
					`waiter_commission` = '" . $this->db->escape($stweward_commition) . "',
					`cancelstatus` = '" . $this->db->escape('1') . "',
					`cancel_kot_reason` = '".$this->db->escape($data['cancel_reason'])."',
					`new_qty` = '" . $this->db->escape($cancelqty) . "',
					`new_rate` = '" . $this->db->escape($pvalues['rate']) . "', 
					`new_amt` = '" . $this->db->escape($cancelamt) . "',
					`new_discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
					`new_discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
					`kitchen_display` = '" . $this->db->escape($pvalues['kitchen_display']) . "',
					`bill_date` = '".$last_open_date."',
					`date` = '".date('Y-m-d')."',
					`time` = '".date('H:i:s')."',
					`login_id` = '" . $this->db->escape($this->user->getId()) . "',
					`login_name` = '" . $this->db->escape($this->user->getUserName()) . "' ");
					if($pvalues['ismodifier'] == '1' && $pvalues['parent'] == '1'){
						$itemdata = $this->db->getLastId();
					}
				}

				if($itemdata != ''){
					if($pvalues['ismodifier'] == '0'){
						$pvalues['parent_id'] = $autoincrementparentid;
					}
				}


				if ($data['captain_id'] != '') {
					$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['code']."'")->row;
					if ($itemdata['captain_rate_value'] == 1 ) {
						$caption_commition = $pvalues['qty'] * $itemdata['captain_rate_per'];
					} else {
						$caption_commition = $pvalues['qty'] *$pvalues['rate']/100 * $itemdata['captain_rate_per'];
					}
				} else {
					$caption_commition = '0';
				}

				if ($data['waiter_id'] != '') {
					$itemdata = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['code']."'")->row;
					if ($itemdata['stweward_rate_value'] == 1) {
						$stweward_commition = $pvalues['qty'] * $itemdata['stweward_rate_per'];
					}else{
						$stweward_commition = $pvalues['qty'] * $pvalues['rate']/100 * $itemdata['stweward_rate_per'];
					}
				} else {
					$stweward_commition = '0';
				}
					
				if($pvalues['is_new'] == '0' && (!empty($this->request->get['edit']) && ($pvalues['cancelmodifier'] == 0 && $pvalues['ismodifier'] == '0'))) {
					if($pvalues['qty'] == 0 || $pvalues['qty'] == 0.00){
						$pvalues['cancelstatus'] = 1;
					}


					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
							`order_id` = '" . $this->db->escape($order_id) . "',   
							`code` = '" . $this->db->escape($pvalues['code']) ."',
							`name` = '" . htmlspecialchars_decode($pvalues['name']) ."', 
							`qty` = '" . $this->db->escape($pvalues['qty']) . "', 
							`rate` = '" . $this->db->escape($pvalues['rate']) . "',  
							`amt` = '" . $this->db->escape($pvalues['amt']) . "',
							`ismodifier` = '" . $this->db->escape($pvalues['ismodifier']) . "',  
							`nc_kot_status` = '" . $this->db->escape($pvalues['nc_kot_status']) . "',
							`nc_kot_reason` = '" . $this->db->escape($pvalues['nc_kot_reason']) . "',
							`transfer_qty` = '" . $this->db->escape($pvalues['transfer_qty']) . "',
							`parent_id` = '" . $this->db->escape($pvalues['parent_id']) . "', 
							`parent` = '" . $this->db->escape($pvalues['parent']) . "',  
							`subcategoryid` = '" . $this->db->escape($pvalues['subcategoryid']) . "',
							`kot_status` = '" .$this->db->escape($kot_status). "',
							`kot_no` = '" .$this->db->escape($c_no). "',
							`prefix` = '" .$this->db->escape($prefix). "',
							`pre_qty` = '" .$this->db->escape($pre_qty). "',
							`is_liq` = '" . $this->db->escape($pvalues['is_liq']) . "',
							`is_new` = '" . $this->db->escape($pvalues['is_new']) . "',
							`cancelstatus` = '" . $this->db->escape($pvalues['cancelstatus']) . "',
							`discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
							`discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
							`tax1` = '" . $this->db->escape($pvalues['tax1']) . "',
							`tax2` = '" . $this->db->escape($pvalues['tax2']) . "',
							`tax1_value` = '" . $this->db->escape($pvalues['tax1_value']) . "',
							`tax2_value` = '" . $this->db->escape($pvalues['tax2_value']) . "',
							`stax` = '" . $this->db->escape($pvalues['stax']) . "',
							`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
							`captain_commission` = '" . $this->db->escape($caption_commition) . "',
							`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "',
							`waiter_commission` = '" . $this->db->escape($stweward_commition) . "',
							`message` = '" . $this->db->escape($pvalues['message']) . "',
							`kitchen_display` = '" . $this->db->escape($pvalues['kitchen_display']) . "',
							`bill_date` = '".$last_open_date."',
							`date` = '".date('Y-m-d')."',
							`time` = '".date('H:i:s')."',
							`new_qty` = '" . $this->db->escape($pvalues['qty']) . "',
							`new_rate` = '" . $this->db->escape($pvalues['rate']) . "', 
							`new_amt` = '" . $this->db->escape($pvalues['amt']) . "',
							`new_discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
							`new_discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");

					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items_modify SET
							`order_id` = '" . $this->db->escape($order_id) . "', 
							`batch_id` = '" . $this->db->escape($order_id) . "',  
							`code` = '" . $this->db->escape($pvalues['code']) ."',
							`name` = '" . htmlspecialchars_decode($pvalues['name']) ."', 
							`qty` = '" . $this->db->escape($pvalues['qty']) . "', 
							`rate` = '" . $this->db->escape($pvalues['rate']) . "',  
							`amt` = '" . $this->db->escape($pvalues['amt']) . "',
							`ismodifier` = '" . $this->db->escape($pvalues['ismodifier']) . "', 
							`nc_kot_status` = '" . $this->db->escape($pvalues['nc_kot_status']) . "',
							`nc_kot_reason` = '" . $this->db->escape($pvalues['nc_kot_reason']) . "', 
							`transfer_qty` = '" . $this->db->escape($pvalues['transfer_qty']) . "',
							`parent_id` = '" . $this->db->escape($pvalues['parent_id']) . "', 
							`parent` = '" . $this->db->escape($pvalues['parent']) . "',  
							`subcategoryid` = '" . $this->db->escape($pvalues['subcategoryid']) . "',
							`kot_status` = '" .$this->db->escape($kot_status). "',
							`kot_no` = '" .$this->db->escape($c_no). "',
							`prefix` = '" .$this->db->escape($prefix). "',
							`pre_qty` = '" .$this->db->escape($pre_qty). "',
							`is_liq` = '" . $this->db->escape($pvalues['is_liq']) . "',
							`is_new` = '" . $this->db->escape($pvalues['is_new']) . "',
							`cancelstatus` = '" . $this->db->escape($pvalues['cancelstatus']) . "',
							`discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
							`discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
							`tax1` = '" . $this->db->escape($pvalues['tax1']) . "',
							`tax2` = '" . $this->db->escape($pvalues['tax2']) . "',
							`tax1_value` = '" . $this->db->escape($pvalues['tax1_value']) . "',
							`tax2_value` = '" . $this->db->escape($pvalues['tax2_value']) . "',
							`stax` = '" . $this->db->escape($pvalues['stax']) . "',
							`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
							`captain_commission` = '" . $this->db->escape($caption_commition) . "',
							`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "',
							`waiter_commission` = '" . $this->db->escape($stweward_commition) . "',
							`message` = '" . $this->db->escape($pvalues['message']) . "',
							`bill_date` = '".$last_open_date."',
							`date` = '".date('Y-m-d')."',
							`time` = '".date('H:i:s')."',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
				} else {
					if(($pvalues['cancelmodifier'] == 0 && $pvalues['ismodifier'] == '0') || $pvalues['ismodifier'] == '1'){
						if($pvalues['qty'] == 0 || $pvalues['qty'] == 0.00){
							$pvalues['cancelstatus'] = 1;
						}

						$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
								`order_id` = '" . $this->db->escape($order_id) . "',   
								`code` = '" . $this->db->escape($pvalues['code']) ."',
								`name` = '" . htmlspecialchars_decode($pvalues['name']) ."', 
								`qty` = '" . $this->db->escape($pvalues['qty']) . "', 
								`rate` = '" . $this->db->escape($pvalues['rate']) . "',  
								`amt` = '" . $this->db->escape($pvalues['amt']) . "',
								`ismodifier` = '" . $this->db->escape($pvalues['ismodifier']) . "',  
								`nc_kot_status` = '" . $this->db->escape($pvalues['nc_kot_status']) . "',
								`nc_kot_reason` = '" . $this->db->escape($pvalues['nc_kot_reason']) . "',
								`transfer_qty` = '" . $this->db->escape($pvalues['transfer_qty']) . "',
								`parent_id` = '" . $this->db->escape($pvalues['parent_id']) . "', 
								`parent` = '" . $this->db->escape($pvalues['parent']) . "',  
								`subcategoryid` = '" . $this->db->escape($pvalues['subcategoryid']) . "',
								`kot_status` = '" .$this->db->escape($kot_status). "',
								`kot_no` = '" .$this->db->escape($c_no). "',
								`prefix` = '" .$this->db->escape($prefix). "',
								`pre_qty` = '" .$this->db->escape($pre_qty). "',
								`kitchen_display` = '" . $this->db->escape($pvalues['kitchen_display']) . "',
								`is_liq` = '" . $this->db->escape($pvalues['is_liq']) . "',
								`is_new` = '" . $this->db->escape($pvalues['is_new']) . "',
								`cancelstatus` = '" . $this->db->escape($pvalues['cancelstatus']) . "',
								`discount_per` = '" . $this->db->escape($pvalues['discount_per']) . "',
								`discount_value` = '" . $this->db->escape($pvalues['discount_value']) . "',
								`tax1` = '" . $this->db->escape($pvalues['tax1']) . "',
								`tax2` = '" . $this->db->escape($pvalues['tax2']) . "',
								`tax1_value` = '" . $this->db->escape($pvalues['tax1_value']) . "',
								`tax2_value` = '" . $this->db->escape($pvalues['tax2_value']) . "',
								`stax` = '" . $this->db->escape($pvalues['stax']) . "',
								`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
								`captain_commission` = '" . $this->db->escape($caption_commition) . "',
								`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "',
								`waiter_commission` = '" . $this->db->escape($stweward_commition) . "',
								`message` = '" . $this->db->escape($pvalues['message']) . "',
								`bill_date` = '".$last_open_date."',
								`date` = '".date('Y-m-d')."',
								`time` = '".date('H:i:s')."',
								`login_id` = '" . $this->db->escape($this->user->getId()) . "',
								`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
					}
				}
				$orderitemid = $this->db->getLastId();
				if($pvalues['ismodifier'] == '1'){
					$autoincrementparentid = $orderitemid;
				}
			}



			if($pvalues['message'] != ''){
				$id = $this->db->query("SELECT * FROM oc_kotmsg order by msg_code DESC LIMIT 0, 1")->row;
				$msg_code = $id['msg_code'] + 1 ;
				$is_exist = $this->db->query("SELECT * FROM oc_kotmsg WHERE `message` = '".$pvalues['message']."' ");
				if($is_exist->num_rows == 0){
					$this->db->query("INSERT INTO oc_kotmsg SET message = '".$pvalues['message']."', msg_code = '".$msg_code."' ");
				}	
			}

			if($pvalues['is_liq'] == '1' && !empty($this->request->get['edit'])){
				$this->db->query("UPDATE oc_order_items SET billno = '".$pvalues['billno']."' WHERE order_id = '".$order_id."' AND billno = '0'");
			}
			if($pvalues['is_liq'] == '0' && !empty($this->request->get['edit'])){
				$this->db->query("UPDATE oc_order_items SET billno = '".$pvalues['billno']."' WHERE order_id = '".$order_id."' AND billno = '0'");
			}
		}
		//$this->log->write('******** Podatas end ********* ');

		if(!empty($this->request->get['edit'])){
			$this->db->query("UPDATE oc_order_items SET bill_modify = '1', login_id = '".$this->user->getId()."', login_name = '".$this->user->getUserName()."' WHERE order_id = '".$order_id."'");
			$billnofood = $this->db->query("SELECT billno FROM oc_order_items WHERE order_id = '".$order_id."' AND billno <> '0' AND is_liq = '0'")->row;
			$billnoliq = $this->db->query("SELECT billno FROM oc_order_items WHERE order_id = '".$order_id."' AND billno <> '0' AND is_liq = '1'")->row;
			if($billnofood != array()){
				$this->db->query("UPDATE oc_order_items SET billno = '".$billnofood['billno']."' WHERE order_id = '".$order_id."' AND is_liq = '0' AND billno = '0'");
			}
			if($billnoliq != array()){
				$this->db->query("UPDATE oc_order_items SET billno = '".$billnoliq['billno']."' WHERE order_id = '".$order_id."' AND is_liq = '1' AND billno = '0'");
			}

			$billModifyData = $this->db->query("SELECT `order_no`, grand_total, item_quantity, `login_name`, `time` FROM `oc_order_info` WHERE `bill_modify` = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND day_close_status = '0' AND order_id = '".$order_id."'");
			$text = "Modified Bill \nRef No :".$billModifyData->row['order_no'].", Qty :".$billModifyData->row['item_quantity'].", Amt :".$billModifyData->row['grand_total'].", login name :".$billModifyData->row['login_name'].", Time :".$billModifyData->row['time'];
		}
		$this->db->query("UPDATE oc_order_items SET cancelstatus = '1' WHERE qty = '0.00' OR qty = '0'");
		$this->db->query("UPDATE oc_order_info SET cancel_status = '1' WHERE item_quantity = '0.00' OR item_quantity = '0' AND `order_id` = '".$order_id."' ");
		unset($this->session->data['c_id']);
		// $this->log->write('order_id ' .$order_id);
		// $this->log->write('---------------------------Order Model End-------------------------------');

		return $order_id;
	}
	
	public function getlocations($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_name']) && $data['filter_name'] != ' ') {
			$sql .= " AND location LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND order_id = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		$sort_data = array(
			'location',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY location";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function gettables($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "table WHERE 1=1 ";

		 if (!empty($data['filter_name'])) {
			$sql .= " AND name ='" . $this->db->escape($data['filter_name']) . "'";
		 }

		// if (!empty($data['filter_loc'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_loc']) . "' ";
		// }

		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getwaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' ";

		if (!empty($data['filter_wname'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_wname']) . "%'";
		}


		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getwaitersid($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' ";

		if (!empty($data['filter_wcode'])) {
			$sql .= " AND code LIKE '%" . $this->db->escape($data['filter_wcode']) . "%'";
		}

		// $this->log->write("SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' AND code LIKE '%" . $this->db->escape($data['filter_wcode']) . "%'" );

		$sort_data = array(
			'code',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY code";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getcaptains($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Captain' ";

		if (!empty($data['filter_cname']) && $data['filter_cname'] != ' ' ) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_cname']) . "%'";
		}


		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getcaptainsid($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Captain' ";

		if (!empty($data['filter_cname']) && $data['filter_cname'] != ' ' ) {
			$sql .= " AND code LIKE '%" . $this->db->escape($data['filter_ccode']) . "%'";
		}


		$sort_data = array(
			'code',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY code";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		// echo'<pre>';
		// print_r($query);exit;

		return $query->rows;
	}
	public function getcaptaindata($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Captain' ";

		if (!empty($data['filter_ccode']) && $data['filter_ccode'] != ' ' ) {
			$sql .= " AND code LIKE '%" . $this->db->escape($data['filter_ccode']) . "%'";
		}


		$sort_data = array(
			'code',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY code";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		// echo'<pre>';
		// print_r($query);exit;

		return $query->rows;
	}

	public function gettables_data($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
		 	$filter_name = $data['filter_name'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$filter_name);
		 	//$this->log->write(print_r($arr, true));
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				//$sql .= " AND (table_from >= '" . $this->db->escape($table_id) . "' AND table_to <= '" . $this->db->escape($table_id) . "') AND `a_to_z` = '1' ";
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		//$sql .= " AND (table_from >= '" . $this->db->escape($table_id) . "' AND table_to <= '" . $this->db->escape($table_id) . "') ";
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		}

		// if (!empty($data['filter_loc'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_loc']) . "' ";
		// }

		$sort_data = array(
			'location',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY location";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		// echo '<pre>';
		// print_r($query);
		// exit;
		return $query->row;
	}

	public function get_settings($key) {
		$setting_status = $this->db->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'");
		//$this->log->write($setting_status);
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}
	
}