<?php
class ModelCatalogTax extends Model {
	public function addWaiter($data) {
		
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "tax SET tax_name = '" .$this->db->escape($data['tax_name']). "',
					tax_value = '" . $this->db->escape($data['tax_value'])."',
					tax_type = '" . $this->db->escape($data['tax_type'])."',
					tax_code = '" . $this->db->escape($data['tax_code'])."',
					change_status = '" . $this->db->escape($data['change_status'])."',
					include_in_rates = '" . $this->db->escape($data['include_in_rates'])."' ");
		
		$id = $this->db->getLastId();

		return $id;
	}

	public function editWaiter($id, $data) {
		
		$this->db->query("UPDATE " . DB_PREFIX . "tax SET tax_name = '" .$this->db->escape($data['tax_name']). "',
													tax_value = '" . $this->db->escape($data['tax_value'])."',
													tax_type = '" . $this->db->escape($data['tax_type'])."',
													tax_code = '" . $this->db->escape($data['tax_code'])."',
													change_status = '" . $this->db->escape($data['change_status'])."',
													include_in_rates = '" . $this->db->escape($data['include_in_rates'])."'
													WHERE id = '" . (int)$id . "'");
		return $id;
	}

	public function deleteWaiter($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax WHERE id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getWaiter($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "tax WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getWaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "tax WHERE 1=1 ";

		if (!empty($data['filter_tax_name'])) {
			$sql .= " AND tax_name LIKE '%" . $this->db->escape($data['filter_tax_name']) . "%'";
		}

		// if (!empty($data['filter_waiter_id'])) {
		// 	$sql .= " AND waiter_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		// }

		

		$sort_data = array(
			'tax_name',
			'tax_type',
			'tax_code',
			'sport_type'
		);

		// print_r( $sort_data );
		// exit;
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY tax_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// echo $sql;
		// exit();

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalWaiter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "tax WHERE 1=1 ";

		if (!empty($data['filter_tax_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_tax_name']) . "%'";
		}

		if (!empty($data['filter_waiter_id'])) {
			$sql .= " AND sport_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}