<?php
class ModelCatalogSportGroup extends Model {
	public function addSportGroup($data) {
		$data['birthdate_date_from']=date('Y-m-d', strtotime($data['birthdate_date_from']));
		$data['birthdate_date_to']=date('Y-m-d', strtotime($data['birthdate_date_to']));

		$sport_types = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);
		$partypes = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);
		$sport_type = $sport_types[$data['sport_type_id']];
		$parti_type = $partypes[$data['participation_id']];
		$this->db->query("INSERT INTO " . DB_PREFIX . "sport_group SET
									sport_group = '" . $this->db->escape($data['sport_group']) . "', 
									sport_type = '" . $this->db->escape($sport_type) . "', 
									sport_type_id = '" . $this->db->escape($data['sport_type_id']) . "', 
									participation = '" . $this->db->escape($parti_type) . "', 
									participation_id = '" . $this->db->escape($data['participation_id']) . "',
									birthdate_date_from = '" . $this->db->escape($data['birthdate_date_from']) . "', 
									birthdate_date_to = '" . $this->db->escape($data['birthdate_date_to']) . "' ");
		$sport_group_id = $this->db->getLastId();

		$this->db->query("DELETE FROM " . DB_PREFIX . "sport_group_sports WHERE sport_group_id = '" . (int)$sport_group_id . "'");
		if(isset($data['sport_datas'])){
			foreach ($data['sport_datas'] as $skey => $svalue) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "sport_group_sports SET sport_group_id = '" . (int)$sport_group_id . "', sport_id = '" . (int)$skey . "', sport = '" . $this->db->escape($svalue) . "' ");
			}
		}
		return $sport_group_id;
	}

	public function editSportGroup($sport_group_id, $data) {
		$data['birthdate_date_from']=date('Y-m-d', strtotime($data['birthdate_date_from']));
		$data['birthdate_date_to']=date('Y-m-d', strtotime($data['birthdate_date_to']));
		$sport_types = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);
		$partypes = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);
		$sport_type = $sport_types[$data['sport_type_id']];
		$parti_type = $partypes[$data['participation_id']];
		$this->db->query("UPDATE " . DB_PREFIX . "sport_group SET 
									sport_group = '" . $this->db->escape($data['sport_group']) . "', 
									sport_type = '" . $this->db->escape($sport_type) . "', 
									sport_type_id = '" . $this->db->escape($data['sport_type_id']) . "',
									participation = '" . $this->db->escape($parti_type) . "', 
									participation_id = '" . $this->db->escape($data['participation_id']) . "', 
									birthdate_date_from = '" . $this->db->escape($data['birthdate_date_from']) . "', 
									birthdate_date_to = '" . $this->db->escape($data['birthdate_date_to']) . "'
									WHERE `sport_group_id` = '".$sport_group_id."'
						");

		$this->db->query("DELETE FROM " . DB_PREFIX . "sport_group_sports WHERE sport_group_id = '" . (int)$sport_group_id . "'");
		if(isset($data['sport_datas'])){
			foreach ($data['sport_datas'] as $skey => $svalue) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "sport_group_sports SET sport_group_id = '" . (int)$sport_group_id . "', sport_id = '" . (int)$skey . "', sport = '" . $this->db->escape($svalue) . "' ");
			}
		}
		return $sport_group_id;
	}

	public function deleteSportGroup($sport_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "sport_group WHERE sport_group_id = '" . (int)$sport_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "sport_group_sports WHERE sport_group_id = '" . (int)$sport_group_id . "'");
	}

	public function getSportGroup($sport_group_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport_group WHERE sport_group_id = '" . (int)$sport_group_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sport_group WHERE sport_group_id = '" . (int)$sport_group_id . "' ");
		return $query->row;
	}

	public function getSportGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "sport_group WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND sport_group LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND sport_group_id = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		if (!empty($data['filter_sport_type'])) {
			$sql .= " AND sport_type_id = '" . $this->db->escape($data['filter_sport_type']) . "' ";
		}

		if (!empty($data['filter_participant_type'])) {
			$sql .= " AND participation_id = '" . $this->db->escape($data['filter_participant_type']) . "' ";
		}

		$sort_data = array(
			'sport_group',
			'sport_type',
			'participation'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sport_group";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSportGroups($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sport_group WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND sport_group LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND sport_group_id = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		if (!empty($data['filter_sport_type'])) {
			$sql .= " AND sport_type_id = '" . $this->db->escape($data['filter_sport_type']) . "' ";
		}

		if (!empty($data['filter_participant_type'])) {
			$sql .= " AND participation_id = '" . $this->db->escape($data['filter_participant_type']) . "' ";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}