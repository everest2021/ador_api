<?php
class ModelCatalogReportStock extends Model {

	public function department_itemdata($data = array()){
	// $sql = "SELECT `department` FROM `oc_order_info` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) LEFT JOIN `oc_item` oitem ON(oitem.`item_code` = oit.`code`) WHERE `bill_date`>= '".$data['start_date']."' AND `bill_date`<= '".$data['end_date']."'";

	$sql = "SELECT department FROM oc_item WHERE 1=1";

	if (!empty($data['departmentvalue'])) {
		$sql .= " AND department = '" . $this->db->escape($data['departmentvalue']) . "' ";
	} 

	if (!empty($data['subcategory'])) {
		$sql .= " AND item_sub_category_id = '" . $this->db->escape($data['subcategory']) . "'";
	} 

	if (!empty($data['category'])) {
		$sql .= " AND item_category_id = '" . $this->db->escape($data['category']) . "'";
	}

	if (!empty($data['type'])) { 
		if($data['type'] == 'liq'){
			$sql .= " AND is_liq = '1'";
		} else if($data['type'] == 'food'){
			$sql .= " AND is_liq = '0'";
		}
	} 

	$sql .= " GROUP BY department ";
	// echo $sql;
	// exit();
	$this->log->write($sql);
	$department_itemdata = $this->db->query($sql);

	// echo "<pre>";
	// print_r($department_itemdata);
	// exit();
	return $department_itemdata->rows;
	}

	public function itemdata($start_date,$end_date,$xyz,$data = array()){

		$sql = "SELECT oit.`ismodifier`, oit.`name`, SUM(oit.`qty`) as quantity, SUM(oit.`amt`) as amount,oi.`food_cancel`, oi.`liq_cancel`, oit.`cancel_bill` FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) LEFT JOIN `oc_item` oitem ON(oitem.`item_code` = oit.`code`) WHERE oi.`bill_date`>= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oit.`complimentary_status` = '0' AND oi.`bill_status` = '1' AND oit.`cancelstatus` = '0' AND ((oi.`food_cancel` = '0' AND oi.`liq_cancel` = '0') OR (oi.`food_cancel` = '1' AND oi.`liq_cancel` = '0') OR (oi.`food_cancel` = '0' AND oi.`liq_cancel` = '1')) AND cancel_bill <> '1' AND oit.`ismodifier` = '1' AND oitem.`department` = '".$xyz['department']."'";


		if (!empty($data['departmentvalue'])) {
			$sql .= " AND oitem.`department` = '" . $this->db->escape($data['departmentvalue']) . "' ";
		} 

		if (!empty($data['subcategory'])) {
			$sql .= " AND oitem.`item_sub_category_id` = '" . $this->db->escape($data['subcategory']) . "'";
		} 

		if (!empty($data['category'])) {
			$sql .= " AND oitem.`item_category_id` = '" . $this->db->escape($data['category']) . "'";
		} 

		if (!empty($data['type'])) { 
			if($data['type'] == 'liq'){
				$sql .= "AND oitem.`is_liq` = '1'";
			} else if($data['type'] == 'food'){
				$sql .= "AND oitem.`is_liq` = '0'";
			}
		} 

		if (!empty($data['tablegroup'])) {
			$sql .= " AND oi.`location_id` = '" . $this->db->escape($data['tablegroup']) . "'";
		} 

		$sql .= " GROUP BY oit.`code` ";
		// $sql .= ";";
		// echo $sql;
		// echo '<pre>';
		// $itemdata = $this->db->query("SELECT `name`, SUM(`qty`) as quantity, SUM(`amt`) as amount FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) LEFT JOIN `oc_item` oitem ON(oitem.`item_code` = oit.`code`) WHERE `bill_date`>= '".$start_date."' AND `bill_date`<= '".$end_date."' AND oitem.`department` = '".$xyz['department']."' GROUP BY oit.`code` ")->rows;  
		$itemdata = $this->db->query($sql);
		// echo "<pre>";
		// print_r($itemdata);
		//  exit;
		return $itemdata->rows;
	}

	public function modifier($start_date,$end_date){
		return $this->db->query("SELECT oit.`name`, SUM(oit.`qty`) as quantity, SUM(oit.`amt`) as amount FROM `oc_order_info_report` oi LEFT JOIN `oc_order_items_report` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date`>= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oi.`bill_status` = '1' AND oit.`complimentary_status` = '0' AND oit.`cancelstatus` = '0' AND ((oi.`food_cancel` = '0' AND oi.`liq_cancel` = '0') OR (oi.`food_cancel` = '1' AND oi.`liq_cancel` = '0') OR (oi.`food_cancel` = '0' AND oi.`liq_cancel` = '1')) AND cancel_bill <> '1' AND oit.`ismodifier` = '0' GROUP BY oit.`name`")->rows;

	}

	public function totalamount($start_date,$end_date){
		return $this->db->query("SELECT * FROM oc_order_info_report WHERE bill_status = '1' AND cancel_status = '0' AND complimentary_status = '0' AND `bill_date`>= '".$start_date."' AND `bill_date`<= '".$end_date."'")->rows;
	}

	public function cancelamount($start_date,$end_date){
		$cancelbillitem = $this->db->query("SELECT COALESCE(SUM(amt - discount_value),0) as total_amt FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`bill_date`>= '".$start_date."' AND oi.`bill_date`<= '".$end_date."' AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1' AND oit.`ismodifier` = '1' AND oit.`complimentary_status` = '0' AND oi.`complimentary_status` = '0'")->row;
		return $cancelbillitem['total_amt'];
	}
}
?>