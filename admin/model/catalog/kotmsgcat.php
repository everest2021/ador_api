<?php
class ModelCatalogkotmsgcat extends Model {
	public function addMsgCat($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "kotmsg_cat SET name = '" . $this->db->escape($data['name'])."' ");
		$id = $this->db->getLastId();

		return $id;
	}

	public function addMsgCatItem($id,$datas) {
		foreach($datas as $data){
			$test = $this->db->query("SELECT id,name FROM oc_kotmsg_item WHERE id = '".$data."'")->row;
			$this->db->query("INSERT INTO " . DB_PREFIX ."kotmsg_cat_item SET 
								cat_item = '" . $this->db->escape($test['name'])."',
								cat_id = '" . $this->db->escape($test['id'])."',
								item_id = '".$id."'
								");
		}
	}

	public function editMsgCatItem($id,$datas) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "kotmsg_cat_item WHERE item_id = '" . (int)$id . "'");
		$this->addMsgCatItem($id,$datas);
	}

	public function editMsgCat($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "kotmsg_cat SET name = '" .$this->db->escape($data['name']). "' 
															 WHERE id = '" . (int)$id . "'");
		return $id;
	}

	public function deleteMsgCat($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "kotmsg_cat WHERE id = '" . (int)$id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "kotmsg_cat_item WHERE item_id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getMsgCat($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "kotmsg_cat WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getMsgCatItem($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "kotmsg_cat_item WHERE item_id = '" . (int)$id . "' ");
		return $query->rows;
	}

	public function getMsgCats($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "kotmsg_cat WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		// if (!empty($data['filter_waiter_id'])) {
		// 	$sql .= " AND waiter_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		// }

		

		$sort_data = array(
			'name',
			'id',
			'sport_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalMsgCat($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "kotmsg_cat WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . $this->db->escape($data['filter_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}