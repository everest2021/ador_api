<?php
class ModelCatalogkotmsg extends Model {
	public function addWaiter($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "kotmsg SET msg_code = '" .$this->db->escape($data['msg_code']). "',
					message = '" . $this->db->escape($data['message'])."' ");
		$id = $this->db->getLastId();

		return $id;
	}

	public function editWaiter($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "kotmsg SET msg_code = '" . $this->db->escape($data['msg_code']) . "',
															 message = '" .$this->db->escape($data['message']). "' 
															 WHERE id = '" . (int)$id . "'");
		return $id;
	}

	public function deleteWaiter($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "kotmsg WHERE id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getWaiter($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "kotmsg WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getWaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "kotmsg WHERE 1=1 ";

		if (!empty($data['filter_message'])) {
			$sql .= " AND message LIKE '" . $this->db->escape($data['filter_message']) . "%'";
		}

		// if (!empty($data['filter_waiter_id'])) {
		// 	$sql .= " AND waiter_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		// }

		

		$sort_data = array(
			'message',
			'msg_code',
			'sport_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY message";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalWaiter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "kotmsg WHERE 1=1 ";

		if (!empty($data['filter_message'])) {
			$sql .= " AND message LIKE '" . $this->db->escape($data['filter_message']) . "%'";
		}

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . $this->db->escape($data['filter_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}