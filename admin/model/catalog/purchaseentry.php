<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ModelCatalogPurchaseentry extends Model {
	public function addpurchaseentry($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['update_master'])){
			$data['update_master'] = 0;
		}
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "INSERT INTO `oc_purchase_transaction` SET 
				`supplier_id` = '".$this->db->escape($data['supplier_id'])."',
				`supplier_name` = '".$this->db->escape($data['supplier_name'])."',
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`update_master` = '".$this->db->escape($data['update_master'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`item_type` = '".$this->db->escape($data['item_type'])."',
				`category` = '".$this->db->escape($data['category'])."',
				`tp_no` = '".$this->db->escape($data['tp_no'])."',
				`batch_no` = '".$this->db->escape($data['batch_no'])."',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_amt` = '".$this->db->escape($data['total_amt'])."',
				`total_cash_discount` = '".$this->db->escape($data['total_cash_discount'])."',
				`total_tax` = '".$this->db->escape($data['total_tax'])."',
				`total_add_charges` = '".$this->db->escape($data['total_add_charges'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				";
		$this->db->query($sql);
		$this->log->write($sql);
		$id = $this->db->getLastId();

		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['description_code'] != '' || $pvalue['description'] != ''){
				if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
					$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
				}
				$sql = "INSERT INTO `oc_purchase_items_transaction` SET 
					`p_id` = '".$this->db->escape($id)."',
					`description` = '".$this->db->escape($pvalue['description'])."',
					`description_id` = '".$this->db->escape($pvalue['description_id'])."',
					`description_code` = '".$this->db->escape($pvalue['description_code'])."',
					`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
					`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
					`item_category` = '".$this->db->escape($pvalue['item_category'])."',
					`qty` = '".$this->db->escape($pvalue['qty'])."',
					`avqty` = '".$this->db->escape($pvalue['avq'])."',
					`unit` = '".$this->db->escape($pvalue['unit'])."',
					`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
					`rate` = '".$this->db->escape($pvalue['rate'])."',
					`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
					`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
					`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
					`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
					`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
					`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
					`amount` = '".$this->db->escape($pvalue['amount'])."',
					`type` = '".$this->db->escape($pvalue['type_id'])."',
					`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
					";
				$this->db->query($sql);
				$this->log->write($sql);
				if($data['update_master'] == 1){
					$sql = "UPDATE `oc_stock_item` SET `purchase_rate` = '" .$pvalue['rate']. "' WHERE `id` = '".$pvalue['description_id']."' ";
					$this->db->query($sql);
					$this->log->write($sql);

					$quantity_master = $pvalue['qty'] + $pvalue['free_qty'];
					$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
					//$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . "), `purchase_price` = '".$pvalue['rate']."'  WHERE `item_code` = '".$pvalue['description_code']."' ";
					$this->db->query($sql);
					$this->log->write($sql);
				}
			}
		}
		return $id;
	}

	public function editpurchaseentry($data, $id) {
		if(!isset($data['update_master'])){
			$data['update_master'] = 0;
		}
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "UPDATE `oc_purchase_transaction` SET 
				`supplier_id` = '".$this->db->escape($data['supplier_id'])."',
				`supplier_name` = '".$this->db->escape($data['supplier_name'])."',
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`update_master` = '".$this->db->escape($data['update_master'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`item_type` = '".$this->db->escape($data['item_type'])."',
				`tp_no` = '".$this->db->escape($data['tp_no'])."',
				`batch_no` = '".$this->db->escape($data['batch_no'])."',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_amt` = '".$this->db->escape($data['total_amt'])."',
				`total_cash_discount` = '".$this->db->escape($data['total_cash_discount'])."',
				`total_tax` = '".$this->db->escape($data['total_tax'])."',
				`total_add_charges` = '".$this->db->escape($data['total_add_charges'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				WHERE `id` = '".$id."'
				";
		$this->db->query($sql);
		$this->log->write($sql);

		$this->db->query("DELETE FROM `oc_purchase_items_transaction` WHERE `p_id` = '".$id."' ");
		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['description_code'] != '' || $pvalue['description'] != ''){
				if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
					$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
				}
				$sql = "INSERT INTO `oc_purchase_items_transaction` SET 
					`p_id` = '".$this->db->escape($id)."',
					`description` = '".$this->db->escape($pvalue['description'])."',
					`description_id` = '".$this->db->escape($pvalue['description_id'])."',
					`description_code` = '".$this->db->escape($pvalue['description_code'])."',
					`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
					`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
					`item_category` = '".$this->db->escape($pvalue['item_category'])."',
					`qty` = '".$this->db->escape($pvalue['qty'])."',
					`avqty` = '".$this->db->escape($pvalue['avq'])."',
					`unit` = '".$this->db->escape($pvalue['unit'])."',
					`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
					`rate` = '".$this->db->escape($pvalue['rate'])."',
					`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
					`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
					`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
					`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
					`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
					`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
					`amount` = '".$this->db->escape($pvalue['amount'])."',
					`type` = '".$this->db->escape($pvalue['type_id'])."',
					`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
					";
				$this->db->query($sql);
				$this->log->write($sql);
				if($data['update_master'] == 1){
					// $sql = "UPDATE `oc_stock_item` SET `rate` = (rate - " . $pvalue['hidden_rate'] . ") WHERE `id` = '".$pvalue['description_id']."' ";
					// $this->db->query($sql);
					// $this->log->write($sql);

					$hidden_quantity_master = $pvalue['hidden_qty'] + $pvalue['hidden_free_qty'];
					$sql = "UPDATE `oc_item` SET `quantity` = (quantity - " . $hidden_quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
					$this->db->query($sql);
					$this->log->write($sql);

					$sql = "UPDATE `oc_stock_item` SET `purchase_rate` = '".$pvalue['rate']."' WHERE `id` = '".$pvalue['description_id']."' ";
					$this->db->query($sql);
					$this->log->write($sql);

					$quantity_master = $pvalue['qty'] + $pvalue['free_qty'];
					$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
					//$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . "), `purchase_price` = '".$pvalue['rate']."'  WHERE `item_code` = '".$pvalue['description_code']."' ";
					$this->db->query($sql);
					$this->log->write($sql);
				}
			}
		}
		return $id;
	}

	public function deletepurchaseentry($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "purchase_transaction SET delete_status = '1' WHERE id = '" . (int)$id . "' ");
	}

	public function getPurchaseentry($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "purchase_transaction` WHERE `id` = '" . (int)$id . "' ");
		return $query->rows;
	}

	public function getPurchaseentryby_supp_invoice($supplier_id, $invoice_no, $category) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "purchase_transaction` WHERE `supplier_id` = '" . (int)$supplier_id . "' AND `invoice_no` = '".$invoice_no."' AND category = '".$category."'");
		return $query->rows;
	}

	public function availableBals($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0){
		// $date = date('Y-m-d');
		// $pre_date = date('Y-m-d', strtotime($date . "-1 day"));
		$sale_qty = 0;
		$total_purchase = 0;
		if($liquor == 1 && $description_code == '0'){
			$last_stock = $this->db->query("SELECT sum(closing_balance) as c_bal FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_id."' AND store_id = '".$store_id."' GROUP BY item_code ");
		} 

		
		if($last_stock->num_rows > 0){
			$last_stock = $last_stock->row['c_bal'];
		}
		 else{
			$last_stock = 0;
		}

		return $last_stock;

	}

	public function availableQuantity_report($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$startdate=0){
		$sale_qty = 0;
		$total_purchase = 0;
		$pre_date = date('Y-m-d', strtotime($startdate . "-1 day"));
		//echo $pre_date;
		if($liquor == 1 && $description_code == '0'){
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_id."' AND store_id = '".$store_id."' AND invoice_date = '".$pre_date."' ORDER BY id DESC LIMIT 1");
		} elseif($liquor == 0 && $description_code != '0') {
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_code."' AND store_id = '".$store_id."' ORDER BY id DESC LIMIT 1");
		}
		//echo  $last_stock->row['closing_balance'];
		if($last_stock->num_rows > 0){
			$last_stock = $last_stock->row['closing_balance'];
		}
		 else{
			$last_stock = 0;
		}

		//echo  $last_stock ; echo "<br>";

		return $last_stock;
	}


	
	public function total_purchase($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0, $startdate= 0){
		// echo $startdate;
		// exit;
		//$pre_date = date('Y-m-d', strtotime($startdate . "-1 day"));

		$total_purchase = 0;
		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}

		if($semifinished == 'Semi Finish'){
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		} else{
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		

		if($purchase_order->num_rows > 0){
			$total_purchase = $purchase_order->row['qty'];
		}
		 else {
			$total_purchase = 0;
		}
		return $total_purchase;

	}

	


	public function stock_transfer_f($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$startdate=0){

		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}
		// $purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='0' GROUP BY unit_id, description_id");

		// $purchase_order_loose_aval = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='1' GROUP BY unit_id, description_id");

		// if($purchase_order_loose->num_rows > 0){
		// 	$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
		// }

		// if($purchase_order_loose_aval->num_rows > 0){
		// 	$total_purchase = $total_purchase + $purchase_order_loose_aval->row['qty'];
		// } 

		if($liquor == 0 && $description_code != '0') {
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}elseif($liquor == 1 && $description_code == '0'){
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		if($stock_transfer_from->num_rows > 0){
			$stock_transfer_from = $stock_transfer_from->row['qty'];
		// 	$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
		// } elseif($stock_transfer_to->num_rows > 0) {
		// 	$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
		}
		 else{
			$stock_transfer_from = 0;
		}

		return $stock_transfer_from;

	}
	public function stock_transfer_t($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$startdate = 0){
		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}

		// $purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='0' GROUP BY unit_id, description_id");

		// $purchase_order_loose_aval = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='1' GROUP BY unit_id, description_id");

		// if($purchase_order_loose->num_rows > 0){
		// 	$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
		// }

		// if($purchase_order_loose_aval->num_rows > 0){
		// 	$total_purchase = $total_purchase + $purchase_order_loose_aval->row['qty'];
		// } 

		if($liquor == 0 && $description_code != '0') {
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}elseif($liquor == 1 && $description_code == '0'){
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		if($stock_transfer_to->num_rows > 0){
			$stock_transfer_to = $stock_transfer_to->row['qty'];
		// 	$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
		// } elseif($stock_transfer_to->num_rows > 0) {
		// 	$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
		}  else{
			$stock_transfer_to = 0;
		}

		return $stock_transfer_to;

	}

	public function stock_sale($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$startdate = 0){
		$sale_qty = 0;
		if($liquor == 1 && $description_code == '0'){
			$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$description_id."' AND quantity = '".$unit_id."' AND is_liq = '1'");
			if($item_data->num_rows > 0){
				$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_id."'");
				if($for_stores->num_rows > 0){
					foreach($for_stores->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
						//echo "SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code"; echo '<br>';
						if($order_item_data->num_rows > 0){
							if($order_item_data->row['code'] != ''){
								$sale_qty = $order_item_data->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						}
					}
				} else{
					$sale_qty = 0;
				}
			} else{
				$sale_qty = 0;
			}
			
			//exit;
		} elseif($liquor == 0 && $description_code != '0') {
			$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_code."' AND store_id = '".$store_id."'");
				if($item_data->num_rows > 0){
					$sale_qty = 0;
					foreach($item_data->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['parent_item_code']."' GROUP BY code");
						if($order_item_data->num_rows > 0){
							$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
						}
					}
				} else{
					$sale_qty = 0;
				}
		}
		
		// if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
		// 	$closing_balance = $total_purchase - $sale_qty;
		// } else{
		// 	$closing_balance = $last_stock + $total_purchase - $sale_qty;
		// }
		return $sale_qty;

	}

	public function loose_stock($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0, $startdate =0){
		$sale_qty = 0;
		$total_purchase = 0;
		$loose_data = 0;
		if($liquor == 1 && $description_code == '0'){
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_id."' AND store_id = '".$store_id."' AND invoice_date = '".$startdate."' ORDER BY id DESC LIMIT 1");
		} elseif($liquor == 0 && $description_code != '0') {
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_code."' AND store_id = '".$store_id."' ORDER BY id DESC LIMIT 1");
		}

		if($last_stock->num_rows > 0){
			$last_stock = $last_stock->row['closing_balance'];
		} else{
			$last_stock = 0;
		}

		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}

		if($semifinished == 'Semi Finish'){
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		} else{
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		//$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='0' GROUP BY unit_id, description_id");

		//$purchase_order_loose_aval = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='1' GROUP BY unit_id, description_id");


		$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' AND invoice_date = '".$startdate."'  GROUP BY unit_id, description_id");


		if($purchase_order->num_rows > 0){
			$total_purchase = $purchase_order->row['qty'];
		} else {
			$total_purchase = 0;
		}

		if($purchase_order_loose->num_rows > 0){
			$loose_data = $purchase_order_loose->row['qty'];
			$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
		}

		// if($purchase_order_loose_aval->num_rows > 0){
		// 	$total_purchase = $total_purchase + $purchase_order_loose_aval->row['qty'];
		// } 

		
		if($liquor == 0 && $description_code != '0') {
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}elseif($liquor == 1 && $description_code == '0'){
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		if($stock_transfer_from->num_rows > 0){
			$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
		} elseif($stock_transfer_to->num_rows > 0) {
			$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
		}

		if($liquor == 1 && $description_code == '0'){
			$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$description_id."' AND quantity = '".$unit_id."' AND is_liq = '1'");
			if($item_data->num_rows > 0){
				$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_id."'");
				if($for_stores->num_rows > 0){
					foreach($for_stores->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
						if($order_item_data->num_rows > 0){
							if($order_item_data->row['code'] != ''){
								$sale_qty = $order_item_data->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						}
					}
				} else{
					$sale_qty = 0;
				}
			} else{
				$sale_qty = 0;
			}
		} elseif($liquor == 0 && $description_code != '0') {
			$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_code."' AND store_id = '".$store_id."'");
				if($item_data->num_rows > 0){
					$sale_qty = 0;
					foreach($item_data->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['parent_item_code']."' GROUP BY code");
						if($order_item_data->num_rows > 0){
							$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
						}
					}
				} else{
					$sale_qty = 0;
				}
		}
		
		if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
			$closing_balance = $total_purchase - $sale_qty;
		} else{
			$closing_balance = $last_stock + $total_purchase - $sale_qty;
		}
		// echo'<pre>';
		// print_r($closing_balance);
		// exit;
		return $loose_data;

	}





	public function loose_report_sale($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0){
		$sale_qty = 0;
		// quantity is brand_type_id mistake 

		// exit;
		if($liquor == 1 && $description_code == '0'){
			$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$description_id."'  AND is_liq = '1'");
			
			// if($item_data->num_rows > 0){
			// 	$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_id."'");
				
			// 	if($for_stores->num_rows > 0){
					foreach($item_data->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['item_code']."'  GROUP BY code");

						
						if($order_item_data->num_rows > 0){
							
								
							if($order_item_data->row['code'] != ''){

								$sale_qty =  $sale_qty + $order_item_data->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						}
					}

				// 	echo'<pre>';
				// print_r($sale_qty);
			// 	} else{
			// 		$sale_qty = 0;
			// 	}
			// } else{
			// 	$sale_qty = 0;
			// }
		}
		 elseif($liquor == 0 && $description_code != '0') {
			$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_code."' AND store_id = '".$store_id."'");
				if($item_data->num_rows > 0){
					$sale_qty = 0;
					foreach($item_data->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['parent_item_code']."' GROUP BY code");
						if($order_item_data->num_rows > 0){
							$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
						}
					}
				} else{
					$sale_qty = 0;
				}
		}

		
		
		// if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
		// 	$closing_balance = $total_purchase - $sale_qty;
		// } else{
		// 	$closing_balance = $last_stock + $total_purchase - $sale_qty;
		// }
		return $sale_qty;

	}


	// public function loose_report_sale($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0){
	// 	$sale_qty = 0;
	// 	// quantity is brand_type_id mistake 

	// 	// exit;
	// 	if($liquor == 1 && $description_code == '0'){
	// 		$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$description_id."' AND quantity = '".$unit_id."' AND is_liq = '1'");
	// 	// 	echo'<pre>';
	// 	// print_r($item_data->rows);
	// 		if($item_data->num_rows > 0){
	// 			$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_id."'");
				
	// 			if($for_stores->num_rows > 0){
	// 				foreach($for_stores->rows as $key){
	// 					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");

						
	// 					if($order_item_data->num_rows > 0){
	// 						if($order_item_data->row['code'] != ''){
	// 							$sale_qty = $order_item_data->row['sale_qty'];
	// 						} else{
	// 							$sale_qty = 0;
	// 						}
	// 					}
	// 				}

	// 			// 	echo'<pre>';
	// 			// print_r($sale_qty);
	// 			} else{
	// 				$sale_qty = 0;
	// 			}
	// 		} else{
	// 			$sale_qty = 0;
	// 		}
	// 	}
	// 	 elseif($liquor == 0 && $description_code != '0') {
	// 		$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_code."' AND store_id = '".$store_id."'");
	// 			if($item_data->num_rows > 0){
	// 				$sale_qty = 0;
	// 				foreach($item_data->rows as $key){
	// 					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['parent_item_code']."' GROUP BY code");
	// 					if($order_item_data->num_rows > 0){
	// 						$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
	// 					}
	// 				}
	// 			} else{
	// 				$sale_qty = 0;
	// 			}
	// 	}

		
		
	// 	// if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
	// 	// 	$closing_balance = $total_purchase - $sale_qty;
	// 	// } else{
	// 	// 	$closing_balance = $last_stock + $total_purchase - $sale_qty;
	// 	// }
	// 	return $sale_qty;

	// }





	public function availableQuantity($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$startdate=0){
		
		$sale_qty = 0;
		$total_purchase = 0;
		$westage_amt = 0;
		if($startdate == 0){
			$startdate = date('Y-m-d');
			$pre_date = date('Y-m-d', strtotime($startdate . "-1 day"));
		} else {
			$pre_date = date('Y-m-d', strtotime($startdate . "-1 day"));
		}

		if($liquor == 1 && $description_code == '0'){
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_id."' AND store_id = '".$store_id."' AND invoice_date = '".$pre_date."' ORDER BY id DESC LIMIT 1");
			
		} elseif($liquor == 0 && $description_code != '0') {
			$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE purchase_size_id = '".$unit_id."' AND item_code = '".$description_code."' AND store_id = '".$store_id."' ORDER BY id DESC LIMIT 1");
		}

		if($last_stock->num_rows > 0){
			$last_stock = $last_stock->row['closing_balance'];
		} else{
			$last_stock = 0;
		}

		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}

		if($semifinished == 'Semi Finish'){
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		} else{
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		//$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='0' GROUP BY unit_id, description_id");
		//if($liquor == 1 && $description_code == '0'){
			$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'   GROUP BY unit_id, description_id");
		//}
		//$purchase_order_loose_aval = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='1' GROUP BY unit_id, description_id");


		if($purchase_order->num_rows > 0){
			$total_purchase = $purchase_order->row['qty'];
		} else {
			$total_purchase = 0;
		}

		if($purchase_order_loose->num_rows > 0){
			$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
		}

		// if($purchase_order_loose_aval->num_rows > 0){
		// 	$total_purchase = $total_purchase + $purchase_order_loose_aval->row['qty'];
		// } 

		// echo'<pre>';
		// print_r($total_purchase);
		// exit;

		if($liquor == 0 && $description_code != '0') {
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}elseif($liquor == 1 && $description_code == '0'){
			$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

			$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransferliq_items opit LEFT JOIN oc_stocktransferliq opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".$startdate."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		if($stock_transfer_from->num_rows > 0){
			$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
		} elseif($stock_transfer_to->num_rows > 0) {
			$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
		}

		// echo'<pre>';
		// print_r($total_purchase);
		// exit;

		if($liquor == 1 && $description_code == '0'){
			$item_data = $this->db->query("SELECT item_code, quantity, brand_id, item_name FROM oc_item WHERE brand_id = '".$description_id."' AND quantity = '".$unit_id."' AND is_liq = '1'");
			if($item_data->num_rows > 0){
				$for_stores = $this->db->query("SELECT * FROM oc_location WHERE store_name = '".$store_id."'");
				if($for_stores->num_rows > 0){
					foreach($for_stores->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");
						/*if($order_item_data->num_rows > 0){
							if($order_item_data->row['code'] != ''){
								$sale_qty = $order_item_data->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						}*/
						$order_item_data_report = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '1' AND oi.`bill_date` = '".$startdate."' AND code = '".$item_data->row['item_code']."' AND location_id = '".$key['location_id']."' GROUP BY code");

						if($order_item_data->num_rows > 0 && $order_item_data_report->num_rows > 0){
							$sale_qty = 0;
						} else if($order_item_data->num_rows > 0){
							if($order_item_data->row['code'] != ''){
								$sale_qty = $order_item_data->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						} else if($order_item_data_report->num_rows > 0){
							if($order_item_data_report->row['code'] != ''){
								$sale_qty = $order_item_data_report->row['sale_qty'];
							} else{
								$sale_qty = 0;
							}
						}
					}
				} else{
					$sale_qty = 0;
				}
			} else{
				$sale_qty = 0;
			}
		} elseif($liquor == 0 && $description_code != '0') {
			$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_code."' AND store_id = '".$store_id."'");
				if($item_data->num_rows > 0){
					$sale_qty = 0;
					foreach($item_data->rows as $key){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".date('Y-m-d')."' AND code = '".$key['parent_item_code']."' GROUP BY code");
						if($order_item_data->num_rows > 0){
							$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
						}
					}
				} else{
					$sale_qty = 0;
				}
		}

		//$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit_id = '".$unit_id."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		//if($westage_datas->num_rows > 0){
			//$westage_amt = abs($westage_datas->row['qty']);
		//}

		$unit_datas = $this->db->query("SELECT unit FROM oc_unit WHERE unit_id = '".$unit_id."'");
		if($unit_datas->num_rows > 0){
			$units = $unit_datas->row['unit'];
			$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."' AND unit = '".$this->db->escape($units)."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		} else {
			$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_id = '".$description_id."' AND invoice_date = '".date('Y-m-d')."'  AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}
		if($westage_datas->num_rows > 0){
			$westage_amt = abs($westage_datas->row['qty']);
		}
		

		 //echo'<pre>';
		 //print_r($westage_amt);
		// exit;
		
		if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
			//$closing_balance = $total_purchase + $westage_amt - $sale_qty ;
			$closing_balance = $total_purchase - ($sale_qty + $westage_amt);
		} else{
			//echo'innn';exit;
			//$closing_balance = $last_stock + $total_purchase + $westage_amt - $sale_qty ;
			$closing_balance = ($last_stock + $total_purchase) - ($sale_qty + $westage_amt);
		}
		// echo'<pre>';
		// print_r($closing_balance);
		// exit;
		return $closing_balance;

	}


	
	public function printBarcode($purchase_id, $type){
		$purchase_items = $this->db->query("SELECT SUM(qty) as qty, description, unit, description_id, SUM(amount) as amt FROM oc_purchase_items_transaction WHERE p_id = '".$this->request->get['purchase_id']."' AND item_category = '".$type."' GROUP BY description_id, unit_id")->rows;
			$barcode_number = $this->db->query("SELECT barcode_number FROM oc_purchase_transaction WHERE barcode_number <> 0 ORDER BY id DESC LIMIT 1");
			if($barcode_number->num_rows > 0){
				$barcode = $barcode_number->row['barcode_number'] + 1;
			} else{
				$barcode = 1;
			}
			$printername = BARCODE_PRINTER;
			try {
		 		$connector = new WindowsPrintConnector($printername);
			    $printer = new Printer($connector);
			    $printer->selectPrintMode(32);
			   	$printer->setEmphasis(true);
			   	$printer->setTextSize(2, 1);
				foreach($purchase_items as $purchase_item){
					if($type == 'Liquor'){
						$a = "{A".$barcode."-".$purchase_item['description_id']."-".$purchase_item['amt'];
					} else{
						$a = "{A".$barcode."-".$purchase_item['description_code']."-".$purchase_item['amt'];
					}
				   	for($i = 0; $i < $purchase_item['qty']; $i++){
					   	$printer->setJustification(Printer::JUSTIFY_CENTER);
					    $printer->text(HOTEL_NAME);
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_CENTER);
						$printer->setBarcodeHeight(48);
					    $printer->barcode($a, Printer::BARCODE_CODE128);
					    $printer->feed(1);
					    $printer->setJustification(Printer::JUSTIFY_CENTER);
					    $printer->setTextSize(1, 1);
					    $printer->text("Item Name :".$purchase_item['description']);
					    $printer->feed(1);
					    $printer->text("MRP :".$purchase_item['amt']);
					    $printer->feed(2);
					    $printer->cut();
					}
				}
			    $printer->close();
			} catch (Exception $e) {
			    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";exit;
			}
		$this->db->query("UPDATE oc_purchase_transaction SET barcode_number = '".$barcode."' WHERE id = '".$purchase_id."'");
	}
}