<?php
class ModelCatalogStockManufacturer extends Model {
	public function addpurchaseentry($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "INSERT INTO `oc_stockmanufacturer` SET 
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`category` = 'Food',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				";
		$this->db->query($sql);
		$this->log->write($sql);
		$id = $this->db->getLastId();

		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['description_code'] != '' || $pvalue['description'] != ''){
				$qty = 0;
				if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
					$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
				}
				$sql = "INSERT INTO `oc_stockmanufacturer_items` SET 
					`p_id` = '".$this->db->escape($id)."',
					`description` = '".$this->db->escape($pvalue['description'])."',
					`description_id` = '".$this->db->escape($pvalue['description_id'])."',
					`description_code` = '".$this->db->escape($pvalue['description_code'])."',
					`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
					`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
					`item_category` = '".$this->db->escape($pvalue['item_category'])."',
					`qty` = '".$this->db->escape($pvalue['qty'])."',
					`avqty` = '".$this->db->escape($pvalue['avq'])."',
					`unit` = '".$this->db->escape($pvalue['unit'])."',
					`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
					`rate` = '".$this->db->escape($pvalue['rate'])."',
					`amount` = '".$this->db->escape($pvalue['amount'])."',
					`remark` = '".$this->db->escape($pvalue['remark'])."',
					`exp_date` = '".$this->db->escape($pvalue['exp_date'])."',
					`type` = '".$this->db->escape($pvalue['type_id'])."'
					";
				$this->db->query($sql);
				$this->log->write($sql);
				$bom_items = $this->db->query("SELECT * FROM oc_bom_items WHERE parent_item_code = '".$pvalue['description_code']."'  AND store_id = '".$data['store_id']."'")->rows;
				foreach($bom_items as $bom_item){
					$stock_data = $this->db->query("SELECT * FROM oc_stock_item WHERE item_code = '".$bom_item['item_code']."'")->row;
					$qty = $bom_item['qty'] * $pvalue['qty'];
					$this->db->query("INSERT INTO oc_stockmanufacturer_deduction SET
										p_id = '".$this->db->escape($id)."',
										p_name = '".$this->db->escape($pvalue['description'])."',
										p_code = '".$this->db->escape($pvalue['description_code'])."',
										store_id = '".$this->db->escape($data['store_id'])."',
										store_name = '".$this->db->escape($data['store_name'])."',
										invoice_date = '".$this->db->escape($data['invoice_date'])."',
										description = '".$this->db->escape($stock_data['item_name'])."',
										description_id = '".$this->db->escape($stock_data['id'])."',
										description_code = '".$this->db->escape($stock_data['item_code'])."',
										qty = '".$this->db->escape($qty)."',
										unit = '".$this->db->escape($stock_data['unit_name'])."',
										unit_id = '".$this->db->escape($stock_data['uom'])."'
									");
				}
			}
		}
		return $id;
	}

	public function editpurchaseentry($data, $id) {
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "UPDATE `oc_stockmanufacturer` SET 
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`category` = 'Food',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				WHERE `id` = '".$id."'
				";
		$this->db->query($sql);
		$this->log->write($sql);

		$this->db->query("DELETE FROM `oc_stockmanufacturer_items` WHERE `p_id` = '".$id."' ");
		$this->db->query("DELETE FROM `oc_stockmanufacturer_deduction` WHERE `p_id` = '".$id."' ");
		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['description_code'] != '' || $pvalue['description'] != ''){
				if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
					$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
				}
				$sql = "INSERT INTO `oc_stockmanufacturer_items` SET 
					`p_id` = '".$this->db->escape($id)."',
					`description` = '".$this->db->escape($pvalue['description'])."',
					`description_id` = '".$this->db->escape($pvalue['description_id'])."',
					`description_code` = '".$this->db->escape($pvalue['description_code'])."',
					`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
					`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
					`item_category` = '".$this->db->escape($pvalue['item_category'])."',
					`qty` = '".$this->db->escape($pvalue['qty'])."',
					`avqty` = '".$this->db->escape($pvalue['avq'])."',
					`unit` = '".$this->db->escape($pvalue['unit'])."',
					`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
					`rate` = '".$this->db->escape($pvalue['rate'])."',
					`amount` = '".$this->db->escape($pvalue['amount'])."',
					`remark` = '".$this->db->escape($pvalue['remark'])."',
					`exp_date` = '".$this->db->escape($pvalue['exp_date'])."',
					`type` = '".$this->db->escape($pvalue['type_id'])."'
					";
				$this->db->query($sql);
				$this->log->write($sql);
				$bom_items = $this->db->query("SELECT * FROM oc_bom_items WHERE parent_item_code = '".$pvalue['description_code']."' AND store_id = '".$data['store_id']."'")->rows;
				foreach($bom_items as $bom_item){
					$stock_data = $this->db->query("SELECT * FROM oc_stock_item WHERE item_code = '".$bom_item['item_code']."'")->row;
					$qty = $bom_item['qty'] * $pvalue['qty'];
					$this->db->query("INSERT INTO oc_stockmanufacturer_deduction SET
										p_id = '".$this->db->escape($id)."',
										p_name = '".$this->db->escape($pvalue['description'])."',
										p_code = '".$this->db->escape($pvalue['description_code'])."',
										store_id = '".$this->db->escape($data['store_id'])."',
										store_name = '".$this->db->escape($data['store_name'])."',
										invoice_date = '".$this->db->escape($data['invoice_date'])."',
										description = '".$this->db->escape($stock_data['item_name'])."',
										description_id = '".$this->db->escape($stock_data['id'])."',
										description_code = '".$this->db->escape($stock_data['item_code'])."',
										qty = '".$this->db->escape($qty)."',
										unit = '".$this->db->escape($stock_data['unit_name'])."',
										unit_id = '".$this->db->escape($stock_data['uom'])."'
									");
				}
			}
		}
		return $id;
	}

	public function deletepurchaseentry($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "stockmanufacturer SET delete_status = '1' WHERE id = '" . (int)$id . "' ");
	}

	public function getPurchaseentry($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "stockmanufacturer` WHERE `id` = '" . (int)$id . "' ");
		return $query->rows;
	}

	public function getPurchaseentryby_supp_invoice($invoice_no) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "stockmanufacturer` WHERE `invoice_no` = '".$invoice_no."'");
		return $query->rows;
	}
}