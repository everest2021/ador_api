<?php
class ModelCatalogStockItem extends Model {
	public function addWaiter($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;

		if (!isset($data['show_in_purchase'])) {
			$data['show_in_purchase'] = 0;
		}
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "stock_item SET item_code = '" .$this->db->escape($data['item_code']). "',
				   bar_code = '" .$this->db->escape($data['bar_code']). "',
				   bar_code_1 = '" .$this->db->escape($data['bar_code_1']). "',
				   item_name = '" .$this->db->escape($data['item_name']). "',
				   category = '" .$this->db->escape($data['category']). "',
				   item_type = '" .$this->db->escape($data['item_type']). "',
				   manage_stock = '" .$this->db->escape($data['manage_stock']). "',
				   uom = '" .$this->db->escape($data['uom']). "',
				   unit_name = '" .$this->db->escape($data['unit_name']). "',
				   item_category = '" .$this->db->escape($data['item_category']). "',
				   category_id = '" .$this->db->escape($data['category_id']). "',
				   purchase_rate = '" .$this->db->escape($data['purchase_rate']). "',
				   reorder_level = '" .$this->db->escape($data['reorder_level']). "',
				   store_name = '" .$this->db->escape($data['store_name']). "',
				   store_id = '" .$this->db->escape($data['store_id']). "',
				   tax = '" .$this->db->escape($data['tax']). "',
				   tax_value = '" .$this->db->escape($data['tax_value']). "',
				   show_in_purchase = '" . $this->db->escape($data['show_in_purchase'])."' ");
		$id = $this->db->getLastId();

		$last_sel_datas = $this->db->query("SELECT bill_date FROM `oc_order_info` WHERE `day_close_status` = '0' LIMIT 1 ");
		if($last_sel_datas->num_rows > 0){
			$bill_date = $last_sel_datas->row['bill_date'];
		} else{
			$bill_date = date('Y-m-d');
		}
		$pre_date = date('Y-m-d', strtotime('-1 day', strtotime($bill_date)));

		$this->db->query("INSERT INTO purchase_test SET 
									item_code = '" .$this->db->escape($data['item_code']). "',
									store_id = '" .$this->db->escape($data['store_id']). "',
									item_name = '" .$this->db->escape($data['item_name']). "',
									purchase_size_id = '" .$this->db->escape($data['uom']). "',
									purchase_size = '" .$this->db->escape($data['unit_name']). "',
									closing_balance  = '0',
									invoice_date = '".$pre_date."'
									  ");
		return $id;
	}

	public function editWaiter($id, $data) {

		if (!isset($data['show_in_purchase'])) {
			$data['show_in_purchase'] = 0;
		}
		$this->db->query("UPDATE " . DB_PREFIX . "stock_item SET  item_code = '" .$this->db->escape($data['item_code']). "',
												   bar_code = '" .$this->db->escape($data['bar_code']). "',
												   bar_code_1 = '" .$this->db->escape($data['bar_code_1']). "',
												   item_name = '" .$this->db->escape($data['item_name']). "',
												   category = '" .$this->db->escape($data['category']). "',
												   item_type = '" .$this->db->escape($data['item_type']). "',
												   manage_stock = '" .$this->db->escape($data['manage_stock']). "',
												   uom = '" .$this->db->escape($data['uom']). "',
												   unit_name = '" .$this->db->escape($data['unit_name']). "',
												   purchase_rate = '" .$this->db->escape($data['purchase_rate']). "',
												   reorder_level = '" .$this->db->escape($data['reorder_level']). "',
												   store_name = '" .$this->db->escape($data['store_name']). "',
												   store_id = '" .$this->db->escape($data['store_id']). "',
												   item_category = '" .$this->db->escape($data['item_category']). "',
												   category_id = '" .$this->db->escape($data['category_id']). "',
												   tax = '" .$this->db->escape($data['tax']). "',
				   								   tax_value = '" .$this->db->escape($data['tax_value']). "',
												   show_in_purchase = '" .$this->db->escape($data['show_in_purchase']). "'
												   WHERE id = '" . (int)$id . "'");
		return $id;

		$last_sel_datas = $this->db->query("SELECT bill_date FROM `oc_order_info` WHERE `day_close_status` = '0' LIMIT 1 ");
		if($last_sel_datas->num_rows > 0){
			$bill_date = $last_sel_datas->row['bill_date'];
		} else{
			$bill_date = date('Y-m-d');
		}
		$pre_date = date('Y-m-d', strtotime('-1 day', strtotime($bill_date)));

		$this->db->query("UPDATE purchase_test SET 
									item_code = '" .$this->db->escape($data['item_code']). "',
									store_id = '" .$this->db->escape($data['store_id']). "',
									item_name = '" .$this->db->escape($data['item_name']). "',
									purchase_size_id = '" .$this->db->escape($data['uom']). "',
									purchase_size = '" .$this->db->escape($data['unit_name']). "',
									closing_balance  = '0',
									invoice_date = '".$pre_date."'
									 ");
	}

	public function deleteWaiter($id) {
		$last_sel_datas = $this->db->query("SELECT id,item_code FROM `oc_stock_item` WHERE id = '" . (int)$id . "' ");
		if($last_sel_datas->num_rows > 0){
			$this->db->query("DELETE FROM " . DB_PREFIX . "bom_items WHERE item_code = '" . (int)$last_sel_datas->row['item_code'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "stock_item WHERE id = '" . (int)$last_sel_datas->row['id'] . "'");
			$this->db->query("DELETE FROM purchase_test WHERE item_code = '" . (int)$last_sel_datas->row['item_code'] . "'");
		} 
		//$this->db->query("DELETE FROM " . DB_PREFIX . "stock_item WHERE id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getWaiter($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stock_item WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getWaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "stock_item WHERE 1=1 ";

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND item_name LIKE '" . $this->db->escape($data['filter_item_name']) . "%'";
		}

		// if (!empty($data['filter_waiter_id'])) {
		// 	$sql .= " AND waiter_id = '" . $this->db->escape($data['filter_waiter_id']) . "' ";
		// }

		

		$sort_data = array(
			'item_name',
			'item_code',
			'item_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY item_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalWaiter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "stock_item  WHERE 1=1 ";

		if (!empty($data['filter_store_name'])) {
			$sql .= " AND store_name LIKE '" . $this->db->escape($data['filter_store_name']) . "%'";
		}

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . $this->db->escape($data['filter_id']) . "' ";
		}

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}