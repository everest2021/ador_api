<?php
class ModelCatalogExpensetrans extends Model {
	public function addAccexpense($data) {

		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$exp_no_sql = "SELECT * FROM oc_expense_trans  ORDER BY `exp_no` DESC LIMIT 1 ";
		$exp_no_dates = $this->db->query($exp_no_sql);
		//echo "<pre>";print_r($exp_no_dates);exit;
		if($exp_no_dates->num_rows > 0){
			$exp_no = $exp_no_dates->row['exp_no'];
			$exp_no = $exp_no + 1;
		} else {
			$exp_no = 1;
		}

		$this->db->query("INSERT INTO " . DB_PREFIX ."expense_trans SET 
				    payment_type = '" .$this->db->escape($data['payment_type']). "',
				    type = '" .$this->db->escape($data['type']). "',
				    account_name = '" .$this->db->escape($data['account_name']). "',
					amount = '" .$this->db->escape($data['amount']). "',
					date = '" .$last_open_date. "',
					exp_no = '" .$exp_no. "',
				    remarks = '" .$this->db->escape($data['remarks']). "' ");
		$id = $this->db->getLastId();

		return $id;
	}

	public function editAccexpense($id, $data) {
		//echo "<pre>";print_r($data);exit;
		$this->db->query("UPDATE " . DB_PREFIX . "expense_trans SET 
						payment_type = '" . $this->db->escape($data['payment_type']) . "',
						type = '" .$this->db->escape($data['type']). "',
						account_name = '" .$this->db->escape($data['account_name']). "',
						amount = '" .$this->db->escape($data['amount']). "',
						remarks = '" . $this->db->escape($data['remarks']) . "'
						WHERE id = '" . (int)$id . "'");
		return $id;
	}

	public function deleteAccexpense($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "expense_trans WHERE id = '" . (int)$id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getAccexpense($id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "expense_trans WHERE id = '" . (int)$id . "' ");
		return $query->row;
	}

	public function getCategory(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."category");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getAccexpenses($data = array()) {
		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$sql = "SELECT * FROM `" . DB_PREFIX . "expense_trans` WHERE 1=1 AND `date` = '".$last_open_date."' ";

		if (!empty($data['filter_id'])) {
			$sql .= " AND `id` = '" . $this->db->escape($data['filter_id']) . "' ";
		}

		if (!empty($data['filter_account_name'])) {
			$sql .= " AND account_name LIKE '%" . $this->db->escape($data['filter_account_name']) . "%' ";
		}

		$sort_data = array(
			'account_name',
			'id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY account_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;
		// exit();
		//$this->log->write($sql);

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalAccexpense($data = array()) {
		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "expense_trans WHERE 1=1 AND `date` = '".$last_open_date."' ";

		if (!empty($data['filter_account_name'])) {
			$sql .= " AND `account_name` LIKE '%" . $this->db->escape($data['filter_account_name']) . "%'";
		}

		if (!empty($data['filter_id'])) {
			$sql .= " AND `id` = '" . $this->db->escape($data['filter_id']) . "' ";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}