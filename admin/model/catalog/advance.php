<?php
class ModelCatalogAdvance extends Model {
	public function addItem($data) {

		// if(!empty($data['booking_date'])){
		// 	$data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
		// }
		$rounddata = '';
		if(!empty($data['delivery_date'])){
			$data['delivery_date'] = date('Y-m-d', strtotime($data['delivery_date']));
		}
		if(!empty($data['delivery_time'])){	
			$data['delivery_time'] = date('H:i', strtotime($this->request->post['delivery_time']));
		}

		if($data['balance'] > 0){
			$roundtotals = explode('.',$data['balance']);
			if($roundtotals[1] != ''){
				$roundtotal = $roundtotals[1];
				if($roundtotal != '00'){
					$rounddata = (100 - $roundtotal)/100;
				}
			}
		}

		$this->db->query("INSERT INTO oc_advance SET 
							c_id = '".$data['c_id']."',
							customer_name = '".$data['customername']."',
							contact = '".$data['contact']."',
							address = '".$data['address']."',
							email = '".$data['email']."',
							gst_no = '".$data['gst_no']."',
							booking_date = '".date('Y-m-d')."',
							delivery_date = '".$data['delivery_date']."',
							delivery_time = '".$data['delivery_time']."',
							total = '".$data['total']."',
							gst = '".$data['gst']."',
							stax = '".$data['sc']."',
							grand_total = '".$data['grandtotal']."',
							roundtotal = '".$rounddata."',
							advance_amt = '".$data['advance']."',
							balance = '".$data['balance']."',
							login_id = '" . $this->db->escape($this->user->getId()) . "',
							login_name = '" . $this->db->escape($this->user->getUserName()) . "'
						");

		$advance_id = $this->db->getLastId();

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_advance_item SET 
								advance_id = '".$advance_id."',
								item_id = '".$value['itemid']."',
								item_code = '".$value['itemcode']."',
								item_name = '".$value['item']."',
								qty = '".$value['qty']."',
								rate = '".$value['rate']."',
								amt = '".$value['amt']."',
								subcategoryid = '".$value['subcategoryid']."',
								tax1 = '".$value['tax1']."',
								taxvalue1 = '".$value['taxvalue1']."',
								tax2 = '".$value['tax2']."',
								taxvalue2 = '".$value['taxvalue2']."',
								msg = '".$value['msg']."',
								login_id = '" . $this->db->escape($this->user->getId()) . "',
								login_name = '" . $this->db->escape($this->user->getUserName()) . "'
								");

			if($value['msg'] != ''){
				$id = $this->db->query("SELECT * FROM oc_kotmsg order by msg_code DESC LIMIT 0, 1")->row;
				$msg_code = $id['msg_code'] + 1 ;
				$is_exist = $this->db->query("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				
				$this->log->write("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				$this->log->write(print_r($is_exist, true));

				if($is_exist->num_rows == 0){
					$this->db->query("INSERT INTO oc_kotmsg SET message = '".$value['msg']."', msg_code = '".$msg_code."' ");
				}	
			}
		}
		$advancedata = $this->db->query("SELECT * FROM oc_advance WHERE 1=1");
		if($advancedata->num_rows > 0){
			$billno = $this->db->query("SELECT advance_billno FROM oc_advance ORDER BY advance_billno DESC LIMIT 1")->row;
			$billno = $billno['advance_billno'] + 1;
		} else{
			$billno = 1;
		}
		$this->db->query("UPDATE oc_advance SET advance_billno = '".$billno."' WHERE id = '".$advance_id."'");
		return $advance_id;
	}

	public function editItem($item_id, $data) {

		// if(!empty($data['booking_date'])){
		// 	$data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
		// }
		$rounddata = '';
		if(!empty($data['delivery_date'])){
			$data['delivery_date'] = date('Y-m-d', strtotime($data['delivery_date']));
		}
		if(!empty($data['delivery_time'])){	
			$data['delivery_time'] = date('H:i', strtotime($this->request->post['delivery_time']));
		}

		if($data['grandtotal'] > 0){
			$roundtotals = explode('.',$data['grandtotal']);
			if($roundtotals[1] != ''){
				$roundtotal = $roundtotals[1];
				if($roundtotal != '00'){
					$rounddata = (100 - $roundtotal)/100;
				}
			}
		}

		$this->db->query("DELETE FROM oc_advance_item WHERE advance_id = '".$item_id."'");
		$this->db->query("UPDATE oc_advance SET 
							c_id = '".$data['c_id']."',
							customer_name = '".$data['customername']."',
							contact = '".$data['contact']."',
							address = '".$data['address']."',
							email = '".$data['email']."',
							gst_no = '".$data['gst_no']."',
							delivery_date = '".$data['delivery_date']."',
							delivery_time = '".$data['delivery_time']."',
							total = '".$data['total']."',
							gst = '".$data['gst']."',
							stax = '".$data['sc']."',
							grand_total = '".$data['grandtotal']."',
							roundtotal = '".$rounddata."',
							advance_amt = '".$data['advance']."',
							balance = '".$data['balance']."'
							WHERE id='".$item_id."'
						");

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_advance_item SET 
								advance_id = '".$item_id."',
								item_id = '".$value['itemid']."',
								item_code = '".$value['itemcode']."',
								item_name = '".$value['item']."',
								qty = '".$value['qty']."',
								rate = '".$value['rate']."',
								amt = '".$value['amt']."',
								subcategoryid = '".$value['subcategoryid']."',
								tax1 = '".$value['tax1']."',
								taxvalue1 = '".$value['taxvalue1']."',
								tax2 = '".$value['tax2']."',
								taxvalue2 = '".$value['taxvalue2']."',
								msg = '".$value['msg']."'
								");

			if($value['msg'] != ''){
				$id = $this->db->query("SELECT * FROM oc_kotmsg order by msg_code DESC LIMIT 0, 1")->row;
				$msg_code = $id['msg_code'] + 1 ;
				$is_exist = $this->db->query("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				
				$this->log->write("SELECT * FROM oc_kotmsg WHERE `message` = '".$value['msg']."' ");
				$this->log->write(print_r($is_exist, true));

				if($is_exist->num_rows == 0){
					$this->db->query("INSERT INTO oc_kotmsg SET message = '".$value['msg']."', msg_code = '".$msg_code."' ");
				}	
			}
		}
	}

	public function deleteItem($item_id) {
		$this->db->query("DELETE FROM oc_advance WHERE id = '" . (int)$item_id . "'");
		$this->db->query("DELETE FROM oc_advance_item WHERE advance_id = '" . (int)$item_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
	}

	public function getItem($item_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM oc_advance WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getItems($data = array()) {
		$sql = "SELECT * FROM oc_advance WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND customer_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
			$sql .= " OR contact LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}
		
		if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}

		$sort_data = array(
			'booking_date',
			'customer_name',
			'contact',
			'advance_billno',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY booking_date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalItems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_advance WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND customer_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function makeBill($id){
		$test = '';
		$advancedata = $this->db->query("SELECT * FROM oc_advance WHERE id = '".$id."'")->row;
		$advanceitems = $this->db->query("SELECT * FROM oc_advance_item WHERE advance_id = '".$advancedata['id']."'")->rows;
		$countitem = $this->db->query("SELECT COUNT(*) as count, SUM(qty) as qty FROM oc_advance_item WHERE advance_id = '".$advancedata['id']."'")->row;
		$location = $this->db->query("SELECT * FROM oc_location WHERE is_advance = '1'")->row;
		// echo'<pre>';
		// print_r($location);
		// exit;
		$orderlocation = $this->db->query("SELECT table_id FROM oc_order_info WHERE location_id = ".$location['location_id']." AND table_id >= ".$location['table_from']." AND table_id <= ".$location['table_to']." ORDER BY table_id DESC LIMIT 1")->row;
		if($orderlocation != array()){
			for($i = $location['table_from']; $i <= $location['table_to']; $i++){
				if($i == $orderlocation['table_id']){
					if($i != $location['table_to']){
						$tableno = $i+1;
					} else{
						$tableno = $location['table_from'];
					}
					continue;
				}
			}
		} else{
			$tableno = $location['table_from'];
		}

		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
		$o_kot_no = 1;
		if(isset($kot_no_datas['kot_no'])){
			$o_kot_no = $kot_no_datas['kot_no'] + 1;
			//echo $ow_number;
		}

		$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
		if($kotno2->num_rows > 0){
			$kot_no2 = $kotno2->row['kot_no'];
			$kotno = $kot_no2 + 1;
		} else{
			$kotno = 1;
		}
		$advancedata['grand_total'] = $advancedata['grand_total'] - $advancedata['advance_amt'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET  
							`location` = '" . $this->db->escape($location['location']) . "', 
							`location_id` = '" . $this->db->escape($location['location_id']) ."',
							`cust_name` = '" . $this->db->escape($advancedata['customer_name']) ."', 
							`cust_id` = '" . $this->db->escape($advancedata['c_id']) ."',
							`cust_contact` = '" . $this->db->escape($advancedata['contact']) ."',
							`cust_address` = '" . $this->db->escape($advancedata['address']) ."',
							`cust_email` = '" . $this->db->escape($advancedata['email']) ."',
							`gst_no` = '" . $this->db->escape($advancedata['gst_no']) ."',
							`t_name` = '" . $this->db->escape($tableno) . "',
							`table_id` = '" . $this->db->escape($tableno) . "',  
							`ftotal` = '" . $this->db->escape($advancedata['total']) . "',
							`gst` = '" . $this->db->escape($advancedata['gst']) . "',
							`staxfood` = '" . $this->db->escape($advancedata['stax']) . "',
							`stax` = '" . $this->db->escape($advancedata['stax']) . "',
							`advance_billno` = '" . $this->db->escape($advancedata['advance_billno']) . "',
							`advance_amount` = '" . $this->db->escape($advancedata['advance_amt']) . "',
							`grand_total` = '" . $this->db->escape($advancedata['grand_total']) . "',
							`roundtotal` = '" . $this->db->escape($advancedata['roundtotal']) . "',
							`total_items` = '" . $this->db->escape($countitem['count']) . "',
							`item_quantity` = '" . $this->db->escape($countitem['qty']) . "',
							`rate_id` = '" . $this->db->escape($location['rate_id']) . "',
							`bill_date` = '".$last_open_date."',
							`date_added` = '" . $this->db->escape(DATE('Y-m-d')) . "',
							`time_added` = '" . $this->db->escape(DATE('H:i:s')) . "',
							`kot_no` = '".$o_kot_no."',
							`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
							`time` = '" . $this->db->escape(DATE('H:i:s')) . "',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
		$order_id = $this->db->getLastId();
		foreach($advanceitems as $advanceitem){
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET
							`order_id` = '" . $this->db->escape($order_id) . "',   
							`code` = '" . $this->db->escape($advanceitem['item_code']) ."',
							`name` = '" . $this->db->escape($advanceitem['item_name']) ."', 
							`qty` = '" . $this->db->escape($advanceitem['qty']) . "', 
							`rate` = '" . $this->db->escape($advanceitem['rate']) . "',  
							`amt` = '" . $this->db->escape($advanceitem['amt']) . "',
							`ismodifier` = 1,  
							`subcategoryid` = '" . $this->db->escape($advanceitem['subcategoryid']) . "',
							`kot_status` = 1,
							`kot_no` = '" .$this->db->escape($kotno). "',
							`pre_qty` = 1,
							`is_new` = 1,
							`tax1` = '" . $this->db->escape($advanceitem['tax1']) . "',
							`tax2` = '" . $this->db->escape($advanceitem['tax2']) . "',
							`tax1_value` = '" . $this->db->escape($advanceitem['taxvalue1']) . "',
							`tax2_value` = '" . $this->db->escape($advanceitem['taxvalue2']) . "',
							`message` = '" . $this->db->escape($advanceitem['msg']) . "',
							`date` = '".date('Y-m-d')."',
							`time` = '".date('h:i:s')."',
							`login_id` = '" . $this->db->escape($this->user->getId()) . "',
							`login_name` = '" . $this->db->escape($this->user->getUserName()) . "'");
		}
		$last_open_date_sql_order = "SELECT `bill_date` FROM `oc_order_info` WHERE `year_close_status` = '0' ORDER BY `bill_date` DESC LIMIT 1";
		$last_open_dates_order = $this->db->query($last_open_date_sql_order);
		if($last_open_dates_order->num_rows > 0){
			$last_open_date_order = $last_open_dates_order->row['bill_date'];
		} else {
			$last_open_date_order = date('Y-m-d');
		}

		$ordernogenrated = $this->db->query("SELECT order_no FROM oc_order_info WHERE order_id = '".$order_id."'")->row;
		if($ordernogenrated['order_no'] == '0'){
			$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date_order."'  ORDER BY `order_no` DESC LIMIT 1")->row;
			$orderno = 1;
			if(isset($orderno_q['order_no'])){
				$orderno = $orderno_q['order_no'] + 1;
			}

			$kotno2 = $this->db->query("SELECT `billno` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by `billno` DESC LIMIT 1");

			if($kotno2->num_rows > 0){
				$kot_no2 = $kotno2->row['billno'];
				$billno = $kot_no2 + 1;
			} else{
				$billno = 1;
			}

			$this->db->query("UPDATE oc_order_items SET billno = '".$billno."' WHERE is_liq = 0 AND order_id = '".$order_id."' AND cancelstatus = '0'");

			$update_sql = "UPDATE `oc_order_info` SET `bill_status` = '1', `order_no` = '".$orderno."', out_time = '".date('h:i:s')."' WHERE `order_id` = '".$order_id."' ";
			$this->db->query($update_sql);
		}
		return $order_id;
	}
}