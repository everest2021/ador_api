<?php
class ModelCatalogkotgroup extends Model {
	public function addDepartment($data) {
		$subcategory = '';
		if(!empty($data['subcategory']) && isset($data['subcategory'])){
			$subcategory = implode(',', $data['subcategory']);
		}
		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "kotgroup SET subcategory = '" .$this->db->escape($subcategory). "', 
					code = '" . $this->db->escape($data['code']) . "', 
					description = '" . $this->db->escape($data['description']) . "', 
					master_kotprint = '" . $this->db->escape($data['master_kotprint']) . "', 
					printer_type = '" . $this->db->escape($data['printertype']) . "', 
					printer = '" . $this->db->escape($data['printer'])."' ");
		$dep_id = $this->db->getLastId();

		return $dep_id;
	}

	public function editDepartment($code, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		$subcategory = '';
		if(!empty($data['subcategory']) && isset($data['subcategory'])){
			$subcategory = implode(',', $data['subcategory']);
		}
		$this->db->query("UPDATE " . DB_PREFIX . "kotgroup SET subcategory = '" . $this->db->escape($subcategory) . "',
																code = '" . $this->db->escape($data['code']) . "',
																description = '" . $this->db->escape($data['description'])."',
																master_kotprint = '" . $this->db->escape($data['master_kotprint']) . "', 
																printer_type = '" . $this->db->escape($data['printertype']) . "', 
																printer = '" . $this->db->escape($data['printer'])."'
																WHERE code = '" . (int)$code . "'");
		return $code;
	}

	public function deleteDepartment($code) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "kotgroup WHERE code = '" . (int)$code . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getDepartment($code) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "kotgroup WHERE code = '" . (int)$code . "' ");
		return $query->row;
	}

	public function getDepartments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "kotgroup WHERE 1=1 ";

		// if (!empty($data['filter_name'])) {
		// 	$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		// }

		// if (!empty($data['filter_name_id'])) {
		// 	$sql .= " AND code = '" . $this->db->escape($data['filter_name_id']) . "' ";
		// }

		

		$sort_data = array(
			'printer'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY printer";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDepartment($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "kotgroup WHERE 1=1 ";

		// if (!empty($data['filter_name'])) {
		// 	$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		// }

		// if (!empty($data['filter_name_id'])) {
		// 	$sql .= " AND sport_id = '" . $this->db->escape($data['filter_name_id']) . "' ";
		// }

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}