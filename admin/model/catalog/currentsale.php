<?php
class ModelCatalogcurrentsale extends Model {
	public function getInfo($bill_date) {
		$billinfos = $this->db->query("SELECT 
					SUM(ftotal + ltotal) as bill_amount,
					SUM(ftotalvalue + ltotalvalue) as total_discount,
					SUM(ftotal_discount + ltotal_discount) as sub_total,
					SUM(vat) as total_vat,
					SUM(gst) as total_gst,
					SUM(stax) as total_stax,
					SUM(grand_total) as net_amount,
					SUM(ftotal) as total_ftotal,
					SUM(ltotal) as total_ltotal,
					SUM(roundtotal) as total_roundamt,
					SUM(advance_amount) as total_advance
					from oc_order_info WHERE bill_status = '1' AND bill_date = '".$bill_date."' AND food_cancel <> '1' AND liq_cancel <> '1' AND cancel_status = '0'")->rows;
		return $billinfos;
	}

	public function getTotalAmount($bill_date) {
		$getTotalAmount = $this->db->query("SELECT 
					SUM(ftotal + ltotal) - SUM(ftotalvalue + ltotalvalue) as total_amount
					from oc_order_info WHERE bill_status = '1' AND bill_date = '".$bill_date."'")->row;
		return $getTotalAmount;
	}

					//(SUM(ftotal_discount) + SUM(ltotal_discount) - SUM(total_discount) as sub_total

	public function getTableInfo($bill_date){
		$tableinfos = $this->db->query("SELECT location, count(*) as order_no, sum(grand_total) as grand_total from oc_order_info WHERE bill_status = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND bill_date = '".$bill_date."' GROUP BY location")->rows;
		return $tableinfos;
	}

	public function currentAmount($bill_date){
		$currentinfos = $this->db->query("SELECT SUM(ftotal + ltotal) as current_amount FROM oc_order_info WHERE bill_status = '0' AND `date` = '".$bill_date."'")->rows;
		return $currentinfos;
	}

	public function lastBill($bill_date){
		$lastbill = '';
		$data = $this->db->query("SELECT order_no FROM oc_order_info WHERE bill_status = '1' AND order_no <> '0' AND food_cancel <> '1' AND liq_cancel <> '1' AND bill_date = '".$bill_date."' ORDER BY order_no DESC LIMIT 1");
		if($data->num_rows > 0){
			$lastbill = $data->row['order_no'];
		}
		return $lastbill;
	}

	public function unsettleAmount($bill_date){
		 $unsettleddata = $this->db->query("SELECT SUM(grand_total) as unsettletotal FROM oc_order_info WHERE bill_status = '1' AND pay_method = '0' AND bill_date = '".$bill_date."'");
		 if($unsettleddata->num_rows > 0){
		 	$unsettleamount = $unsettleddata->row['unsettletotal'];
		 } else{
		 	$unsettleamount = 0;
		 }
		 return $unsettleamount;
	}

	public function settleAmount($bill_date){
		 $settleddata = $this->db->query("SELECT 
		 	SUM(pay_cash) as total_cashamt,
		 	SUM(pay_card) as total_creditamt,
		 	SUM(mealpass) as total_mealamt,
		 	SUM(pass) as total_passamt
		 	FROM oc_order_info WHERE bill_status = '1' AND pay_method = '1' AND food_cancel <> '1' AND liq_cancel <> '1' AND bill_date = '".$bill_date."'")->rows;
		 return $settleddata;
	}

	public function cancelamount($bill_date){
		$ftotal = $this->db->query("SELECT SUM(ftotal - ftotalvalue) as total_ftotal FROM oc_order_info WHERE food_cancel = '1' AND bill_status = '1' AND bill_date = '".$bill_date."'")->row;
		$ltotal = $this->db->query("SELECT SUM(ltotal - ltotalvalue) as total_ltotal FROM oc_order_info WHERE liq_cancel = '1' AND bill_status = '1' AND bill_date = '".$bill_date."'")->row;
		$fltotal = $this->db->query("SELECT SUM((ftotal + ltotal) - (ftotalvalue + ltotalvalue)) as total_fltotal FROM oc_order_info WHERE food_cancel = '1' AND liq_cancel = '1' AND bill_status = '1' AND bill_date = '".$bill_date."'")->row;
		// return $this->db->query("SELECT grand_total FROM oc_order_info WHERE cancel_status = '1' AND bill_status = '1' AND `bill_date`>= '".$start_date."' AND `bill_date`<= '".$end_date."'")->rows;
		if($fltotal['total_fltotal'] != ''){
			return $fltotal['total_fltotal'];
		} else{
			return $ftotal['total_ftotal'] + $ltotal['total_ltotal'];
		}
	}

	public function cancelkotbot($bill_date){
		$cancelkotbot = $this->db->query("SELECT qty, amt, cancelstatus FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`date` = '".$bill_date."'  AND oit.`ismodifier` = '1' AND oit.`cancelstatus` = '1'")->rows;
		return $cancelkotbot;
	}

	public function cancelbillitem($bill_date){
		$cancelbillitem = $this->db->query("SELECT SUM(amt - discount_value) as total_amt, SUM(qty) as total_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oi.`order_id` = oit.`order_id`) WHERE oi.`date` = '".$bill_date."'  AND oit.`ismodifier` = '1' AND oit.`cancel_bill` = '1' AND billno <> '0' ")->row;
		return $cancelbillitem;
	}
}