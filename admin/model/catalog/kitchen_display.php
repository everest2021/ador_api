<?php
class ModelCatalogKitchenDisplay extends Model {
	public function addOrderprocess($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX .
				   "orderprocess SET c_name = '" .$this->db->escape($data['c_name']). "',
					c_code = '" . $this->db->escape($data['c_code'])."' ");
		$orderprocess_id = $this->db->getLastId();

		return $orderprocess_id;
	}

	public function editOrderprocess($orderprocess_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "orderprocess SET c_name = '" . $this->db->escape($data['c_name']) . "',
														      c_code = '" .$this->db->escape($data['c_code']). "' 
															 WHERE orderprocess_id = '" . (int)$orderprocess_id . "'");
		return $orderprocess_id;
	}

	public function deleteOrderprocess($orderprocess_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "orderprocess WHERE orderprocess_id = '" . (int)$orderprocess_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getOrderprocess($orderprocess_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "orderprocess WHERE orderprocess_id = '" . (int)$orderprocess_id . "' ");
		return $query->row;
	}

	public function getOrderprocesss($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "orderprocess WHERE 1=1 ";

		if (!empty($data['filter_c_name'])) {
			$sql .= " AND c_name LIKE '" . $this->db->escape($data['filter_c_name']) . "%'";
		}

		if (!empty($data['filter_orderprocess_id'])) {
			$sql .= " AND orderprocess_id = '" . $this->db->escape($data['filter_orderprocess_id']) . "' ";
		}

		

		$sort_data = array(
			'c_name',
			'sport_type'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY c_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalOrderprocess($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "orderprocess WHERE 1=1 ";

		if (!empty($data['filter_c_name'])) {
			$sql .= " AND c_name LIKE '" . $this->db->escape($data['filter_c_name']) . "%'";
		}

		// if (!empty($data['filter_orderprocess_id'])) {
		// 	$sql .= " AND _id = '" . $this->db->escape($data['filter_orderprocess_id']) . "' ";
		// }

		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}