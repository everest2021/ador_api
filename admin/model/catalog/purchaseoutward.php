<?php
class ModelCatalogPurchaseoutward extends Model {
	public function addItem($data) {

		// echo "<pre>";
		// print_r($data);
		// exit;

		if(!empty($data['date'])){
			$data['date'] = date('Y-m-d', strtotime($data['date']));
		}

		$this->db->query("INSERT INTO oc_outward_order SET 
							ow_prefix = 'OW-',
							ow_date = '".$data['date']."',
							outlet_id = '".$data['store']."'
						");

		$ow_id = $this->db->getLastId();

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_outward_order_items SET 
								ow_id = '".$ow_id."',
								item_id = '".$value['itemid']."',
								item_name = '".$value['item']."',
								quantity = '".$value['qty']."',
								notes = '".$value['msg']."',
								po_number = '".$value['po_number']."',
								is_new = '".$value['is_new']."'
							");
		}
		$owdata = $this->db->query("SELECT * FROM oc_outward_order WHERE 1=1");
		if($owdata->num_rows > 0){
			$ow_number = $this->db->query("SELECT ow_number FROM oc_outward_order ORDER BY ow_number DESC LIMIT 1")->row;
			$ow_number = $ow_number['ow_number'] + 1;
		} else{
			$ow_number = 1;
		}
		$this->db->query("UPDATE oc_outward_order SET ow_number = '".$ow_number."' WHERE id = '".$ow_id."'");
		return $ow_id;
	}

	public function editItem($item_id, $data) {

		if(!empty($data['date'])){
			$data['date'] = date('Y-m-d', strtotime($data['date']));
		}

		$this->db->query("DELETE FROM oc_outward_order_items WHERE ow_id = '".$item_id."'");
		$this->db->query("UPDATE oc_outward_order SET 
							ow_prefix = 'ow',
							ow_date = '".$data['delivery_date']."',
							outlet_id = '".$data['store']."'
							WHERE id='".$item_id."'
						");

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_outward_order_items SET 
								ow_id = '".$item_id."',
								item_id = '".$value['itemid']."',
								item_name = '".$value['item']."',
								quantity = '".$value['qty']."',
								notes = '".$value['msg']."',
								po_number = '".$value['po_number']."',
								is_new = '".$value['is_new']."'
							");
		}
	}

	public function deleteItem($item_id) {
		$this->db->query("DELETE FROM oc_outward_order WHERE id = '" . (int)$item_id . "'");
		$this->db->query("DELETE FROM oc_outward_order_items WHERE ow_id = '" . (int)$item_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
	}

	public function getItem($item_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM oc_outward_order WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getItems($data = array()) {
		$sql = "SELECT * FROM oc_outward_order WHERE 1=1 ";

		if (isset($data['filter_date']) && !empty($data['filter_date'])) {
			$sql .= " AND `ow_date` = '" . date('Y-m-d', strtotime($data['filter_date'])) . "' ";
		}

		// if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
		// 	$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		// }

		$sort_data = array(
			'ow_date',
			'ow_number'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY ow_date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalItems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_outward_order WHERE 1=1 ";

		if (isset($data['filter_date']) && !empty($data['filter_date'])) {
			$sql .= " AND `ow_date` = '" . date('Y-m-d', strtotime($data['filter_date'])) . "' ";
		}

		// if ($data['filter_status'] >= 0 && $data['filter_status'] != '') {
		// 	$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
		// }

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}