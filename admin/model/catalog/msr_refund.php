<?php
class ModelCatalogMsrRefund extends Model {
	public function addrechageamt($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$user = $this->user->getId();
		$user_name = $this->user->getUserName();
		$this->db->query("INSERT INTO oc_msr_recharge_transaction SET 
							cust_id = '".$data['c_id']."',
							cust_name = '".$data['customername']."',
							cust_mob_no = '".$data['contact']."',
							cust_pre_amt = '".$data['pre_ball']."',
							msr_no = '".$data['msr']."',
							amount = '".$data['amt']."',
							extra_amt = '".$data['extra_amt']."',
							total_amt = '".$data['total_amt']."',
							remark = '".$data['remark']."',
							pay_by = '".$data['payby']."',
							user_id = '".$user."',
							user = '".$user_name."',
							type = '-',
							trans_status = '1',

							date = '".date('Y-m-d')."'
						");
		return $this->db->getLastId();
	}

	public function editCredit($item_id,$data) {
		$this->db->query("UPDATE oc_customercredit SET 
							cust_id = '".$data['c_id']."',
							cust_name = '".$data['customername']."',
							cust_mob_no = '".$data['contact']."',
							cust_pre_amt = '".$data['pre_ball']."',
							msr_no = '".$data['msr']."',
							amount = '".$data['amt']."',
							extra_amt = '".$data['extra_amt']."',
							pay_by = '".$data['payby']."',
							type = '-',
							trans_status = '1',

							date = '".date('Y-m-d')."'
							WHERE id='".$item_id."'
						");
	}

	public function deleteCredit($item_id) {
		$this->db->query("DELETE FROM oc_customercredit WHERE id = '" . (int)$item_id . "'");
	}

	public function getCredit($item_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_customercredit WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getmsr_trans($data = array()) {
		$sql = "SELECT * FROM oc_msr_recharge_transaction WHERE 1=1 ";

		if (isset($data['filter_cust_id']) && !empty($data['filter_cust_id'])) {
			$sql .= " AND cust_id = '" . $data['filter_cust_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND cust_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		$sql .= " AND trans_status = '1' ";

		$sort_data = array(
			'cust_name',
			'cust_mob_no',
			
		);

		$sql .= " AND type = '-'";
		// $sql .= " GROUP BY cust_mob_no";  

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cust_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}


	public function getmsr_search($data = array()) {
		$sql = "SELECT * FROM oc_msr_recharge_transaction WHERE 1=1 ";

		if (isset($data['filter_cust_id']) && !empty($data['filter_cust_id'])) {
			$sql .= " AND cust_id = '" . $data['filter_cust_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND cust_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		$sort_data = array(
			'cust_name',
			'cust_mob_no',
			
		);

		 $sql .= " GROUP BY cust_mob_no";  

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cust_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalrecharge_trans($data = array()) {
		$sql = "SELECT * FROM oc_msr_recharge_transaction WHERE 1=1 ";

		if (isset($data['filter_cust_id']) && !empty($data['filter_cust_id'])) {
			$sql .= " AND cust_id = '" . $data['filter_cust_id'] . "' ";
		}

		if (!empty($data['filter_customername'])) {
			$sql .= " AND cust_name LIKE '%" . $this->db->escape($data['filter_customername']) . "%'";
		}

		//$sql .= " GROUP BY cust_mob_no";  

		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function pre_bal($msr_no, $cust_id){
		$pree_bal = 0;
		
		$prev_bals = $this->db->query("SELECT sum(amount) AS add_amt, sum(extra_amt) AS add_ext_amt  FROM oc_msr_recharge_transaction WHERE msr_no = '".$msr_no."' AND type = '+'");
		$refund = $this->db->query("SELECT sum(amount) AS refund_amt FROM oc_msr_recharge_transaction WHERE msr_no = '".$msr_no."' AND type = '-' ");
		$order_data = $this->db->query("SELECT sum(msrcard) AS msr_amt FROM oc_order_info WHERE msrcardno = '".$msr_no."' AND msr_cust_id = '".$cust_id."' AND cancel_status = '0' AND complimentary_status = '0' ");
		if($prev_bals->num_rows > 0){
			$pree_bal = $prev_bals->row['add_amt'] + $prev_bals->row['add_ext_amt'] - ($refund->row['refund_amt'] + $order_data->row['msr_amt']);
		} else{
			$pree_bal = 0;
		}
		// echo'<pre>';
		// print_r($pree_bal);
		// exit;
		
		return $pree_bal;

	}
}