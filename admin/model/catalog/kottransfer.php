<?php
class ModelCatalogkottransfer extends Model {
	public function addOrder($data) {
		date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->db->query($last_open_date_sql);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

		if($data['nc_kot_status'] == 1){
			$data['nc_kot_status'] = 1;
		} else {
			$data['nc_kot_status'] = 0;
		}

		if(isset($data['orderid'])){
			$this->db->query("DELETE  FROM oc_order_info WHERE order_id = '".$data['orderid']."'");
			$this->db->query("DELETE  FROM oc_order_items WHERE order_id = '".$data['orderid']."'");
			$orderno = $data['order_no'];
			$o_kot_no = $data['kot_no'];
			$date_added = $data['date_added'];
			$time_added = $data['time_added'];
		} else {
			$orderno_q = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."'  ORDER BY `order_no` DESC LIMIT 1")->row;
			$orderno = 1;
			if(isset($orderno_q['order_no'])){
				$orderno = $orderno_q['order_no'] + 1;
				//echo $ow_number;
			}
			$kot_no_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0' ORDER BY `kot_no` DESC LIMIT 1")->row;
			$o_kot_no = 1;
			if(isset($kot_no_datas['kot_no'])){
				$o_kot_no = $kot_no_datas['kot_no'] + 1;
				//echo $ow_number;
			}
			$date_added = date('Y-m-d');
			$time_added = date('H:i:s');
		}

		//****************************************** Start : To Make KOT number different for liquior day wise *********************************** //

		$kotno2 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 0 order by oit.`kot_no` DESC LIMIT 1");
			// print_r($kotno);
			// exit();
			if($kotno2->num_rows > 0){
				$kot_no2 = $kotno2->row['kot_no'];
				$kotno = $kot_no2 + 1;
			} else{
				$kotno = 1;
			}

			
		$kotno1 = $this->db->query("SELECT oit.`kot_no` FROM `oc_order_info` oi LEFT JOIN `oc_order_items` oit ON (oit.`order_id` = oi.`order_id`) WHERE oi.`bill_date` = '".$last_open_date."' AND is_liq = 1 order by oit.`kot_no` DESC LIMIT 1");
			// print_r($kotno);
			// exit();
			if($kotno1->num_rows > 0){
				$kot_no1 = $kotno1->row['kot_no'];
				$botno = $kot_no1 + 1;
			} else{
				$botno = 1;
			}

		//******************************************                   End                         *********************************** //



		$order_info_datas = $this->db->query("SELECT `order_no` FROM `oc_order_info` WHERE `bill_date` = '".$last_open_date."' AND `bill_status` = '0'  ORDER BY `order_no` DESC LIMIT 1");
		if($order_info_datas->num_rows == 0){
			$kot_no = 1;
			$bot_no = 1;
		} else {
			$order_item_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_items` WHERE 1=1 ORDER BY `kot_no` DESC LIMIT 1");
			if($order_item_datas->num_rows > 0){
				$order_item_data = $order_item_datas->row;
				$kot_no = $order_item_data['kot_no'] + 1;
			} else {
				$kot_no = 1;
			}

			// $order_item_datas = $this->db->query("SELECT `kot_no` FROM `oc_order_items` WHERE `is_liq` = '1' ORDER BY `kot_no` DESC LIMIT 1");
			// if($order_item_datas->num_rows > 0){
			// 	$order_item_data = $order_item_datas->row;
			// 	$bot_no = $order_item_data['kot_no'] + 1;
			// } else {
			// 	$bot_no = 1;
			// }
		}
		// `cust_name` = '" . $this->db->escape($data['cust_name']) . "',
		// `cust_contact` = '" . $this->db->escape($data['cust_contact']) . "', 
		// `cust_id` = '" . $this->db->escape($data['cust_id']) . "',
		// `cust_address` = '" . $this->db->escape($data['cust_address']) . "',
		// `cust_email` = '" . $this->db->escape($data['cust_email']) . "',
		$testdata = $this->db->query("SELECT * FROM oc_customerinfo WHERE c_id = '".$this->session->data['c_id']."'")->row;

		$this->db->query("INSERT INTO " . DB_PREFIX . "order_info SET 
						`location` = '" . $this->db->escape($data['location']) . "', 
						`location_id` = '" . $this->db->escape($data['location_id']) ."',
						`parcel_status` = '" . $this->db->escape($data['parcel_status']) ."', 
						`cust_name` = '" . $this->db->escape($testdata['name']) ."', 
						`cust_id` = '" . $this->db->escape($testdata['c_id']) ."',
						`cust_contact` = '" . $this->db->escape($testdata['contact']) ."',
						`cust_address` = '" . $this->db->escape($testdata['address']) ."',
						`cust_email` = '" . $this->db->escape($testdata['email']) ."',
						`gst_no` = '" . $this->db->escape($testdata['gst_no']) ."',
						`t_name` = '" . $this->db->escape($data['table']) . "',
						`table_id` = '" . $this->db->escape($data['table_id']) . "',  
						`waiter_code` = '" . $this->db->escape($data['waiterid']) . "', 
						`waiter` = '" . $this->db->escape($data['waiter']) . "',  
						`waiter_id` = '" . $this->db->escape($data['waiter_id']) . "', 
						`captain_code` = '" . $this->db->escape($data['captainid']) . "',   
						`captain` = '" . $this->db->escape($data['captain']) . "',  
						`captain_id` = '" . $this->db->escape($data['captain_id']) . "',
						`person` = '" . $this->db->escape($data['person']) . "',
						`ftotal` = '" . $this->db->escape($data['ftotal']) . "',
						`ftotal_discount` = '" . $this->db->escape($data['ftotal_discount']) . "',
						`gst` = '" . $this->db->escape($data['gst']) . "',
						`ltotal` = '" . $this->db->escape($data['ltotal']) . "',
						`ltotal_discount` = '" . $this->db->escape($data['ltotal_discount']) . "',
						`vat` = '" . $this->db->escape($data['vat']) . "',
						`cess` = '" . $this->db->escape($data['cess']) . "',
						`stax` = '" . $this->db->escape($data['stax']) . "',
						`ftotalvalue` = '" . $this->db->escape($data['ftotalvalue']) . "',
						`fdiscountper` = '" . $this->db->escape($data['fdiscountper']) . "',
						`discount` = '" . $this->db->escape($data['discount']) . "',
						`ldiscount` = '" . $this->db->escape($data['ldiscount']) . "',
						`ldiscountper` = '" . $this->db->escape($data['ldiscountper']) . "',
						`ltotalvalue` = '" . $this->db->escape($data['ltotalvalue']) . "',
						`grand_total` = '" . $this->db->escape($data['grand_total']) . "',
						`total_items` = '" . $this->db->escape($data['total_items']) . "',
						`item_quantity` = '" . $this->db->escape($data['item_quantity']) . "',
						`nc_kot_status` = '" . $this->db->escape($data['nc_kot_status']) . "',
						`rate_id` = '" . $this->db->escape($data['rate_id']) . "',
						`bill_date` = '".$last_open_date."',
						`date_added` = '".$date_added."',
						`time_added` = '".$time_added."',
						`kot_no` = '".$o_kot_no."',
						`date` = '" . $this->db->escape(DATE('Y-m-d')) . "',
						`time` = '" . $this->db->escape(DATE('H:i:s')) . "'");
		//`order_no` = '" . $this->db->escape($orderno) . "',
		$order_id = $this->db->getLastId();
		$inf  = array();
		
		foreach($data['po_datas'] as $pkey => $pvalues){
			foreach($data['po_datas'] as $pkeys => $pvaluess){
				if($pkey == $pkeys) {

				} elseif($pkey > $pkeys && $pvalues['code'] == $pvaluess['code'] && $pvalues['rate'] == $pvaluess['rate'] && $pvalues['message'] == $pvaluess['message']){
					$pvalues['code'] = '';
				} elseif ($pvalues['code'] == $pvaluess['code'] && $pvalues['rate'] == $pvaluess['rate'] && $pvalues['message'] == $pvaluess['message']) {
					$pvalues['qty'] = $pvalues['qty'] + $pvaluess['qty'];
					$pvalues['amt'] = $pvalues['qty'] * $pvalues['rate'];
				}
			}
			if(isset($pvalues['code']) && $pvalues['code'] != '') {
				if(isset($pvalues['kot_status'])) {
	             	$kot_status = $pvalues['kot_status'];
	            } else {
	             	$kot_status = 0;
	            }
	            if(isset($pvalues['pre_qty'])){
	             	if($pvalues['qty'] > $pvalues['pre_qty']){
						$prefix = '+';
						$pre_qty =$pvalues['pre_qty'];
					} else {
						$prefix = '-';
						$pre_qty =$pvalues['pre_qty'];
					}
				} else {
					$prefix = ' ';
					$pre_qty =0;
				}
				if($pvalues['is_liq'] == '1'){
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $botno;
					}
				} else {
					if($pvalues['kot_no'] != '0'){
						$c_no = $pvalues['kot_no'];
					} else {
						$c_no = $kotno;
					}
				}

				if($pvalues['qty'] > 0){
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_items SET 
							`order_id` = '" . $this->db->escape($order_id) . "', 
							`code` = '" . $this->db->escape($pvalues['code']) ."',
							`name` = '" . $this->db->escape($pvalues['name']) ."', 
							`qty` = '" . $this->db->escape($pvalues['qty']) . "', 
							`rate` = '" . $this->db->escape($pvalues['rate']) . "',  
							`amt` = '" . $this->db->escape($pvalues['amt']) . "',
							`kot_status` = '" .$this->db->escape($kot_status). "',
							`kot_no` = '" .$this->db->escape($c_no). "',
							`prefix` = '" .$this->db->escape($prefix). "',
							`pre_qty` = '" .$this->db->escape($pre_qty). "',
							`is_liq` = '" . $this->db->escape($pvalues['is_liq']) . "',
							`is_new` = '" . $this->db->escape($pvalues['is_new']) . "',
							`message` = '" . $this->db->escape($pvalues['message']) . "'");
					
					if($pvalues['is_liq'] == '1'){
						if($pvalues['kot_no'] == '0'){
							//$kot_no ++;
						}
					}
					if($pvalues['is_liq'] == '0'){
						if($pvalues['kot_no'] == '0'){
							//$kot_no ++;
						}
					}
				}

												// ***********************Start For Tax in oc_order_item ********************* // 

				$itemdatafoods = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['code']."' AND is_liq = '0'")->rows;
				$itemdataliqs = $this->db->query("SELECT * FROM oc_item WHERE item_code = '".$pvalues['code']."' AND is_liq = '1'")->rows;

				foreach($itemdatafoods as $itemdatafood){
					$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$itemdatafood['vat']."'");
					if($tax1->num_rows > 0){
						$taxvalue1 = $tax1->row['tax_value'];
					} else{
						$taxvalue1 = '0';
					}

					$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$itemdatafood['tax2']."'");
					if($tax2->num_rows > 0){
						$taxvalue2 = $tax2->row['tax_value'];
					} else{
						$taxvalue2 = '0';
					}

					$this->db->query("UPDATE oc_order_items SET tax1 = '".$taxvalue1."', tax2 = '".$taxvalue2."' WHERE code = '".$pvalues['code']."'");
				}


				foreach($itemdataliqs as $itemdataliqs){
					$tax1 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$itemdataliqs['vat']."'");
					if($tax1->num_rows > 0){
						$taxvalue1 = $tax1->row['tax_value'];
					} else{
						$taxvalue1 = '0';
					}

					$tax2 = $this->db->query("SELECT * FROM oc_tax WHERE id = '".$itemdataliqs['tax2']."'");
					if($tax2->num_rows > 0){
						$taxvalue2 = $tax2->row['tax_value'];
					} else{
						$taxvalue2 = '0';
					}

					$this->db->query("UPDATE oc_order_items SET tax1 = '".$taxvalue1."', tax2 = '".$taxvalue2."' WHERE code = '".$pvalues['code']."'");
				}

											// ***********************End For Tax in oc_order_item ********************* // 
			}

			if($pvalues['message'] != ''){
				$id = $this->db->query("SELECT * FROM oc_kotmsg order by msg_code DESC LIMIT 0, 1")->row['msg_code'];
				$msg_code = $id + 1 ;
				$is_exist = $this->db->query("SELECT * FROM oc_kotmsg WHERE `message` = '".$pvalues['message']."' ");
				
				$this->log->write("SELECT * FROM oc_kotmsg WHERE `message` = '".$pvalues['message']."' ");
				$this->log->write(print_r($is_exist, true));

				if($is_exist->num_rows == 0){
					$this->db->query("INSERT INTO oc_kotmsg SET message = '".$pvalues['message']."', msg_code = '".$msg_code."' ");
				}	
			}
		}
		unset($this->session->data['c_id']);
	}
	
	public function getlocations($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_name']) && $data['filter_name'] != ' ') {
			$sql .= " AND location LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND order_id = '" . $this->db->escape($data['filter_name_id']) . "' ";
		}

		$sort_data = array(
			'location',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY location";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function gettables($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "table WHERE 1=1 ";

		 if (!empty($data['filter_name'])) {
			$sql .= " AND name ='" . $this->db->escape($data['filter_name']) . "'";
		 }

		// if (!empty($data['filter_loc'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_loc']) . "' ";
		// }

		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getwaiters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' ";

		if (!empty($data['filter_wname'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_wname']) . "%'";
		}


		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getwaitersid($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' ";

		if (!empty($data['filter_wcode'])) {
			$sql .= " AND code LIKE '%" . $this->db->escape($data['filter_wcode']) . "%'";
		}

		// $this->log->write("SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Waiter' AND code LIKE '%" . $this->db->escape($data['filter_wcode']) . "%'" );

		$sort_data = array(
			'code',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY code";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getcaptains($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Captain' ";

		if (!empty($data['filter_cname']) && $data['filter_cname'] != ' ' ) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_cname']) . "%'";
		}


		$sort_data = array(
			'name',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getcaptainsid($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "waiter WHERE roll = 'Captain' ";

		if (!empty($data['filter_cname']) && $data['filter_cname'] != ' ' ) {
			$sql .= " AND code LIKE '%" . $this->db->escape($data['filter_ccode']) . "%'";
		}


		$sort_data = array(
			'code',
			'status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY code";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function gettables_data($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "location WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
		 	$filter_name = $data['filter_name'];
		 	$arr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$filter_name);
		 	//$this->log->write(print_r($arr, true));
		 	if(isset($arr[1])){
				$table_id = $arr[0];
				//$sql .= " AND (table_from >= '" . $this->db->escape($table_id) . "' AND table_to <= '" . $this->db->escape($table_id) . "') AND `a_to_z` = '1' ";
				$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`) AND `a_to_z` = '1' ";
		 	} else {
		 		$table_id = $arr[0];
		 		//$sql .= " AND (table_from >= '" . $this->db->escape($table_id) . "' AND table_to <= '" . $this->db->escape($table_id) . "') ";
		 		$sql .= " AND (" . $table_id . " BETWEEN `table_from` AND `table_to`)";
		 	}
		}

		// if (!empty($data['filter_loc'])) {
		// 	$sql .= " AND location_id = '" . $this->db->escape($data['filter_loc']) . "' ";
		// }

		$sort_data = array(
			'location',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY location";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		// echo '<pre>';
		// print_r($query);
		// exit;
		return $query->row;
	}
	
}