<?php
require DIR_SYSTEM . 'library/escpos-php-development/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
class ModelCatalogStockReport extends Model {
	
	public function availableQuantity($unit_id, $description_id, $liquor, $store_id, $semifinished, $description_code = 0,$start_date){
		// echo $unit_id;
		// echo'<br/>';
		// echo $description_id;
		// echo'<br/>';
		// echo $liquor;
		// echo'<br/>';
		// echo $store_id;
		// echo'<br/>';
		// echo $semifinished;
		// echo'<br/>';
		// echo $description_code;
		// echo'<br/>';
		//exit;
		$sale_qty = 0;
		$total_purchase = 0;
		$westage_amt = 0;
		$invoice_dates = $this->db->query("SELECT invoice_date FROM purchase_test WHERE invoice_date < '".$start_date."'  ORDER BY id DESC LIMIT 1");
		if($invoice_dates->num_rows > 0){
			$invoice_date = $invoice_dates->row['invoice_date'];
		} else {
			$invoice_date = date('Y-m-d');
		}
		// echo'<pre>';
		// print_r($invoice_date);
		// exit;


		$last_stock = $this->db->query("SELECT closing_balance FROM purchase_test WHERE invoice_date = '".$invoice_date."' AND item_code = '".$description_id."' AND store_id = '".$store_id."'");

		if($last_stock->num_rows > 0){
			$last_stock = $last_stock->row['closing_balance'];
		} else{
			$last_stock = 0;
		}

		if($liquor == 1){
			$category = 'Liquor';
		} else{
			$category = 'Food';
		}

		if($semifinished == 'Semi Finish'){
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_stockmanufacturer_items opit LEFT JOIN oc_stockmanufacturer opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		} else{
			//echo'inn';exit;
			$purchase_order = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		}

		$purchase_order_loose = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."'  AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='0' GROUP BY unit_id, description_id");

		$purchase_order_loose_aval = $this->db->query("SELECT SUM(qty) as qty FROM oc_purchase_loose_items_transaction opit LEFT JOIN oc_purchase_loose_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND store_id = '".$store_id."'  AND loose='1' GROUP BY unit_id, description_id");


		if($purchase_order->num_rows > 0){
			$total_purchase = $purchase_order->row['qty'];
		} else {
			$total_purchase = 0;
		}
		// echo'<pre>';
		// print_r("SELECT SUM(qty) as qty FROM oc_purchase_items_transaction opit LEFT JOIN oc_purchase_transaction opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND store_id = '".$store_id."' GROUP BY unit_id, description_id");
		// exit;
		if($purchase_order_loose->num_rows > 0){
			$total_purchase = $total_purchase - $purchase_order_loose->row['qty'];
		}

		if($purchase_order_loose_aval->num_rows > 0){
			$total_purchase = $total_purchase + $purchase_order_loose_aval->row['qty'];
		} 

		$stock_transfer_from = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");

		$stock_transfer_to = $this->db->query("SELECT SUM(qty) as qty FROM oc_stocktransfer_items opit LEFT JOIN oc_stocktransfer opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND to_store_id = '".$store_id."' GROUP BY unit_id, description_id");

		if($stock_transfer_from->num_rows > 0){
			$total_purchase = ($last_stock + $total_purchase) - $stock_transfer_from->row['qty'];
		} elseif($stock_transfer_to->num_rows > 0) {
			$total_purchase = $last_stock + $total_purchase + $stock_transfer_to->row['qty'];
		}

		$item_data = $this->db->query("SELECT * FROM oc_bom_items WHERE item_code = '".$description_id."' AND store_id = '".$store_id."'");
			if($item_data->num_rows > 0){
				$sale_qty = 0;
				foreach($item_data->rows as $key){
					//$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."' AND code = '".$key['parent_item_code']."' GROUP BY code");
					/*if($start_date < date('Y-m-d')){
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");
					} else {
						$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."' AND code = '".$key['parent_item_code']."' GROUP BY code");
					}
					if($order_item_data->num_rows > 0){
						$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
					}*/
					$order_item_data = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items oit LEFT JOIN oc_order_info oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."' AND code = '".$key['parent_item_code']."' GROUP BY code");
					$order_item_data_report = $this->db->query("SELECT code, SUM(qty) as sale_qty FROM oc_order_items_report oit LEFT JOIN oc_order_info_report oi ON (oit.`order_id` = oi.`order_id`) WHERE cancelstatus = 0 AND cancel_bill = 0 AND is_liq = '0' AND oi.`bill_date` = '".$start_date."'  AND code = '".$key['parent_item_code']."' GROUP BY code");
					if($order_item_data->num_rows > 0 && $order_item_data_report->num_rows > 0){
						$sale_qty = 0;
					} else if($order_item_data->num_rows > 0 ){
						$sale_qty = $sale_qty + $key['qty'] * $order_item_data->row['sale_qty'];
					} else if($order_item_data_report->num_rows > 0){
						$sale_qty = $sale_qty + $key['qty'] * $order_item_data_report->row['sale_qty'];
					}
				}
			} else{
				$sale_qty = 0;
			}

		$westage_datas = $this->db->query("SELECT SUM(qty) as qty FROM oc_westage_transfer_items opit LEFT JOIN oc_westage_transfer opt ON(opt.`id` = opit.`p_id`) WHERE description_code = '".$description_id."' AND invoice_date = '".$start_date."' AND category = '".$category."' AND from_store_id = '".$store_id."' GROUP BY unit_id, description_id");
			if($westage_datas->num_rows > 0){
				$westage_amt = abs($westage_datas->row['qty']);
			}

		// echo'<pre>';
		// print_r($description_code);
		// exit;
		
		if($stock_transfer_from->num_rows > 0 || $stock_transfer_to->num_rows > 0){
			$closing_balance = $total_purchase - ($sale_qty + $westage_amt);
		} else{
			//echo'innn';exit;
			$closing_balance = ($last_stock + $total_purchase) - ($sale_qty + $westage_amt);
		}
		/*echo "<pre>";print_r($last_stock);
		echo "<br>";print_r($total_purchase);
		echo "<br>";print_r($sale_qty);
		echo "<br>";print_r($westage_amt);
		echo'<pre>';
		print_r($closing_balance);
		exit;*/
		return $closing_balance;

	}


	
	
}