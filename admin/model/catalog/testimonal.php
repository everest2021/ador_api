<?php

class ModelCatalogTestimonal extends Model {

	public function addactivity($data) {



		$this->db->query("INSERT INTO " . DB_PREFIX .

				   "testimonal SET customer_name = '" .$this->db->escape($data['customer_name']). "',
				  	photo = '" .$this->db->escape($data['photo']). "',
				   	photo_source = '" .$this->db->escape($data['photo_source']). "',    
					discription = '" . $this->db->escape($data['discription']) . "' 
					");

		$id = $this->db->getLastId();



		return $id;

	}



	public function editactivity($id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "testimonal SET customer_name = '" .$this->db->escape($data['customer_name']). "',
							  	photo = '" .$this->db->escape($data['photo']). "',
							   	photo_source = '" .$this->db->escape($data['photo_source']). "',    
								discription = '" . $this->db->escape($data['discription']) . "'													WHERE id = '" . (int)$id . "'");

		return $id;

	}



	public function deleteactivity($id) {

		$this->db->query("DELETE FROM " . DB_PREFIX . "testimonal WHERE id = '" . (int)$id . "'");

		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");

	}



	public function getactivity($id) {

		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";

		//exit;

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "testimonal WHERE id = '" . (int)$id . "' ");

		return $query->row;

	}



	public function getactivitys($data = array()) {

		$sql = "SELECT * FROM " . DB_PREFIX . "testimonal WHERE 1=1 ";



		if (!empty($data['filter_name'])) {

			$sql .= " AND customer_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";

		}



		// if (!empty($data['filter_name_id'])) {

		// 	$sql .= " AND department_id = '" . $this->db->escape($data['filter_name_id']) . "' ";

		// }



		



		$sort_data = array(

			'customer_name',

			'sport_type'

		);



		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

			$sql .= " ORDER BY " . $data['sort'];

		} else {

			$sql .= " ORDER BY customer_name";

		}



		if (isset($data['order']) && ($data['order'] == 'DESC')) {

			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}



		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}



		$query = $this->db->query($sql);



		return $query->rows;

	}



	public function getTotalactivity($data = array()) {

		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "testimonal WHERE 1=1 ";



		if (!empty($data['filter_name'])) {

			$sql .= " AND customer_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";

		}



		// if (!empty($data['filter_name_id'])) {

		// 	$sql .= " AND sport_id = '" . $this->db->escape($data['filter_name_id']) . "' ";

		// }



		

		$query = $this->db->query($sql);

		return $query->row['total'];

	}

}