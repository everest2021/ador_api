<?php
class ModelCatalogCustomer extends Model {
	public function addCustomer($data) {

		//$this->db->query("DELETE FROM " . DB_PREFIX . "customerinfo WHERE c_id = '" . (int)$data['c_id'] . "'");
		
		$anniversary = '0000-00-00';
		$dob =  '0000-00-00';

		if ($data['anniversary'] != '0000-00-00' && $data['anniversary'] != '' )  {
			$anniversary = date('Y-m-d',strtotime($data['anniversary']));
		}

		if ($data['dob'] != '0000-00-00' && $data['dob'] !='' )  {
			$dob = date('Y-m-d',strtotime($data['dob']));
		}

		$this->db->query("INSERT INTO " . DB_PREFIX ."customerinfo SET 
					name = '" .$this->db->escape($data['name']). "',
				    contact = '" .$this->db->escape($data['contact']). "',
				    email = '" .$this->db->escape($data['email']). "',
				    email2 = '" .$this->db->escape($data['email2']). "',
				    code = '" .$this->db->escape($data['code']). "',
				    mobile_no = '" .$this->db->escape($data['mobile_no']). "',
				    alternate_no = '" .$this->db->escape($data['alternate_no']). "',
				    gst_no = '" .$this->db->escape($data['gst_no']). "',
				    msr_no = '" .$this->db->escape($data['msr_no']). "',
				    discount = '" .$this->db->escape($data['discount']). "',
				    landmark = '" .$this->db->escape($data['landmark']). "',
				    area_1 = '" .$this->db->escape($data['area_1']). "',
				    area_2 = '" .$this->db->escape($data['area_2']). "',
				    dob = '" .$this->db->escape($dob). "',
				    anniversary = '" .$this->db->escape($anniversary). "',
				    passport = '" .$this->db->escape($data['passport']). "',
				    address = '" .$this->db->escape($data['address']). "' ");
		$c_id = $this->db->getLastId();
		//$this->db->query("UPDATE oc_customercredit SET c_id = '".$c_id."' WHERE contact = '".$data['contact']."'");
		//$this->db->query("UPDATE oc_order_info SET onaccust = '".$c_id."' WHERE onaccontact = '".$data['contact']."'");
		return $c_id;
	}

	public function editCustomer($c_id, $data) {

		$anniversary = '0000-00-00';
		$dob =  '0000-00-00';

		if ($data['anniversary'] != '0000-00-00' && $data['anniversary'] !='' )  {
			$anniversary = date('Y-m-d',strtotime($data['anniversary']));
		}

		if ($data['dob'] != '0000-00-00' && $data['dob'] !='' )  {
			$dob = date('Y-m-d',strtotime($data['dob']));
		}

		$this->db->query("UPDATE " . DB_PREFIX . "customerinfo SET 
						contact = '" . $this->db->escape($data['contact']) . "',
						email = '" . $this->db->escape($data['email']) . "',
						address = '" . $this->db->escape($data['address']) . "',

						email2 = '" .$this->db->escape($data['email2']). "',
					    code = '" .$this->db->escape($data['code']). "',
					    mobile_no = '" .$this->db->escape($data['mobile_no']). "',
					    alternate_no = '" .$this->db->escape($data['alternate_no']). "',
					    gst_no = '" .$this->db->escape($data['gst_no']). "',
					    msr_no = '" .$this->db->escape($data['msr_no']). "',
					    discount = '" .$this->db->escape($data['discount']). "',
					    landmark = '" .$this->db->escape($data['landmark']). "',
					    area_1 = '" .$this->db->escape($data['area_1']). "',
					    area_2 = '" .$this->db->escape($data['area_2']). "',
					    dob = '" .$this->db->escape($dob). "',
					    anniversary = '" .$this->db->escape($anniversary). "',
					    passport = '" .$this->db->escape($data['passport']). "',
						name = '" .$this->db->escape($data['name']). "'
						WHERE c_id = '" . (int)$c_id . "'");
		return $c_id;
	}

	public function deleteCustomer($c_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customerinfo WHERE c_id = '" . (int)$c_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getCustomer($c_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customerinfo WHERE c_id = '" . (int)$c_id . "' ");
		return $query->row;
	}

	// public function getLocation(){
	// 	$query = $this->db->query("SELECT * FROM ".DB_PREFIX."location");
	// 	//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
	// 	return $query->rows;
	// }

	public function getCustomers($data = array()) {
		// echo "<pre>";
		// print_r($data);
		// exit();
		$sql = "SELECT * FROM " . DB_PREFIX . "customerinfo WHERE 1=1 ";

		if (isset($data['filter_c_id']) && !empty($data['filter_c_id'])) {
			$sql .= " AND c_id = '" . $data['filter_c_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR contact LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR mobile_no LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR alternate_no LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (isset($data['filter_contact'])) {
			$sql .= " AND contact LIKE '%" . $this->db->escape($data['filter_contact']) . "%'";
		}

		if (isset($data['filter_msr_no'])) {
			$sql .= " AND msr_no = '" . $this->db->escape($data['filter_msr_no']) . "'";
		}
		
		$sort_data = array(
			'name',
			'contact'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY c_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;
		
		$this->log->write($sql);

		$query = $this->db->query($sql);
		// echo "<pre>";
		// print_r($query);
		// exit();


		return $query->rows;
	}

	public function getTotalCustomer($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customerinfo WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_c_id'])) {
			$sql .= " AND c_id = '" . $this->db->escape($data['filter_c_id']) . "' ";
		}

		if (!empty($data['filter_contact'])) {
			$sql .= " AND contact LIKE '%" . $this->db->escape($data['filter_contact']) . "%' ";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND email = '" . $this->db->escape($data['filter_email']) . "' ";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}