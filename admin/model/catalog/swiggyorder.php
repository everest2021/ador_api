<?php
class ModelCatalogSwiggyOrder extends Model {
	public function addBrand($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX ."brand SET 
					brand = '" .$this->db->escape($data['brand']). "',
				    subcategory = '" .$this->db->escape($data['subcategory']). "',
				    subcategory_id = '" .$this->db->escape($data['subcategory_id']). "' ");
		$brand_id = $this->db->getLastId();

		return $brand_id;
	}

	public function editBrand($brand_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "brand SET 
						brand = '" . $this->db->escape($data['brand']) . "',
						subcategory = '" . $this->db->escape($data['subcategory']) . "',
						subcategory_id = '" .$this->db->escape($data['subcategory_id']). "'
						WHERE brand_id = '" . (int)$brand_id . "'");
        return $brand_id;
	}

	public function deleteBrand($brand_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand WHERE brand_id = '" . (int)$brand_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getBrand($brand_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "brand WHERE brand_id = '" . (int)$brand_id . "' ");
		return $query->row;
	}

	public function getCategory(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."subcategory");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getBrands($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "brand` WHERE 1=1 ";

		if (!empty($data['filter_brand'])) {
			$sql .= " AND `brand` LIKE '%" . $this->db->escape($data['filter_brand']) . "%'";
		}

		if (!empty($data['filter_brand_id'])) {
			$sql .= " AND `brand_id` = '" . $this->db->escape($data['filter_brand_id']) . "' ";
		}

		if (!empty($data['filter_brand_ids'])) {
			$sql .= " AND `brand_id` = '" . $this->db->escape($data['filter_brand_ids']) . "' ";
		}

		if (!empty($data['filter_subcategory'])) {
			$sql .= " AND subcategory LIKE '%" . $this->db->escape($data['filter_subcategory']) . "%' ";
		}

		if (!empty($data['filter_subcategory_id'])) {
			$sql .= " AND subcategory_id = '" . $this->db->escape($data['filter_subcategory_id']) . "' ";
		}

		$sort_data = array(
			'brand',
			'subcategory'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY brand";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getStatus($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "order_app` WHERE 1=1 ";

		// if (!empty($data['order_status'])) {
		// 	$sql .= " AND `brand` LIKE '%" . $this->db->escape($data['order_status']) . "%'";
		// }

		if (!empty($data['order_status'])) {
			$sql .= " AND `status` = '" . $this->db->escape($data['order_status']) . "' ";
		}

		// if (!empty($data['filter_brand_ids'])) {
		// 	$sql .= " AND `brand_id` = '" . $this->db->escape($data['filter_brand_ids']) . "' ";
		// }

		// if (!empty($data['filter_subcategory'])) {
		// 	$sql .= " AND subcategory LIKE '%" . $this->db->escape($data['filter_subcategory']) . "%' ";
		// }

		// if (!empty($data['filter_subcategory_id'])) {
		// 	$sql .= " AND subcategory_id = '" . $this->db->escape($data['filter_subcategory_id']) . "' ";
		// }

		// $sort_data = array(
		// 	'brand',
		// 	'subcategory'
		// );

		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// 	$sql .= " ORDER BY " . $data['sort'];
		// } else {
		// 	$sql .= " ORDER BY brand";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// 	$sql .= " DESC";
		// } else {
		// 	$sql .= " ASC";
		// }

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }

		$query = $this->db->query($sql);

		return $query->rows;
	}

	

	public function getTotalBrand($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "brand WHERE 1=1 ";

		if (!empty($data['filter_brand'])) {
			$sql .= " AND `brand` LIKE '%" . $this->db->escape($data['filter_brand']) . "%'";
		}

		if (!empty($data['filter_brand_id'])) {
			$sql .= " AND `brand_id` = '" . $this->db->escape($data['filter_brand_id']) . "' ";
		}

		if (!empty($data['filter_subcategory'])) {
			$sql .= " AND subcategory LIKE '%" . $this->db->escape($data['filter_subcategory']) . "%' ";
		}

		if (!empty($data['filter_subcategory_id'])) {
			$sql .= " AND subcategory_id = '" . $this->db->escape($data['filter_subcategory_id']) . "' ";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}