<?php
class ModelCatalogSubCategory extends Model {
	public function addSubCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX ."subcategory SET 
					name = '" .$this->db->escape($data['name']). "',
				    parent_category = '" .$this->db->escape($data['parent_category']). "',
				    colour = '" .$this->db->escape($data['colour']). "',
				    photo = '" .$this->db->escape($data['photo']). "',
					photo_source = '" .$this->db->escape($data['photo_source']). "',
					change_status = '" . $this->db->escape($data['change_status'])."',
					title_for_time = '" . $this->db->escape($data['title_for_time']) . "',
					sort_order_api = '" . $this->db->escape($data['sort_order_api']) . "',
				    parent_id = '" .$this->db->escape($data['parent_id']). "' ");
		$category_id = $this->db->getLastId();

		return $category_id;
	}

	public function editSubCategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "subcategory SET 
						parent_category = '" . $this->db->escape($data['parent_category']) . "',
						parent_id = '" . $this->db->escape($data['parent_id']) . "',
						name = '" .$this->db->escape($data['name']). "',
						colour = '" .$this->db->escape($data['colour']). "',
						photo = '" .$this->db->escape($data['photo']). "',
						change_status = '" . $this->db->escape($data['change_status'])."',
						title_for_time = '" . $this->db->escape($data['title_for_time']) . "',
						sort_order_api = '" . $this->db->escape($data['sort_order_api']) . "',
						photo_source = '" .$this->db->escape($data['photo_source']). "'
						WHERE category_id = '" . (int)$category_id . "'");
		return $category_id;
	}

	public function deleteSubCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "subcategory WHERE category_id = '" . (int)$category_id . "'");
		///$this->db->query("DELETE FROM " . DB_PREFIX . "sport_standard WHERE sport_id = '" . (int)$sport_id . "'");
	}

	public function getSubCategory($category_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "subcategory WHERE category_id = '" . (int)$category_id . "' ");
		return $query->row;
	}

	public function getCategory(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."category");
		//echo "SELECT * FROM ".DB_PREFIX."location WHERE location_id='".(int)$location_id."'";
		return $query->rows;
	}

	public function getSubCategorys($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "subcategory` WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND `category_id` = '" . $this->db->escape($data['filter_category_id']) . "' ";
		}

		if (!empty($data['filter_category_idf'])) {
			$sql .= " AND `parent_id` = '" . $this->db->escape($data['filter_category_idf']) . "' ";
		}

		if (!empty($data['filter_parent'])) {
			$sql .= " AND parent_category LIKE '%" . $this->db->escape($data['filter_parent']) . "%' ";
		}

		if (!empty($data['filter_parent_id'])) {
			$sql .= " AND parent_id = '" . $this->db->escape($data['filter_parent_id']) . "' ";
		}

		$sort_data = array(
			'name',
			'parent_category',
			'parent_id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// echo $sql;
		// exit();
		//$this->log->write($sql);

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalSubCategory($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "subcategory WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND `category_id` = '" . $this->db->escape($data['filter_category_id']) . "' ";
		}

		if (!empty($data['filter_parent_category'])) {
			$sql .= " AND parent_category LIKE '%" . $this->db->escape($data['filter_parent_category']) . "%' ";
		}

		if (!empty($data['filter_parent_id'])) {
			$sql .= " AND parent_id = '" . $this->db->escape($data['filter_parent_id']) . "' ";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}