<?php
class ModelCatalogModifier extends Model {
	public function addItem($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->db->query("INSERT INTO oc_modifier SET modifier_name = '".$data['modifiername']."' , max_quantity = '".$data['max_quantity']."' ");

		$modifier_id = $this->db->getLastId();

		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_modifier_items SET 
								modifier_id = '".$modifier_id."',
								modifier_item_id = '".$value['itemid']."',
								modifier_item_code = '".$value['itemcode']."',
								modifier_item = '".$value['item']."',
								default_item = '".$value['default_item']."'");
		}
	}

	public function editItem($item_id, $data) {
		$this->db->query("DELETE FROM oc_modifier_items WHERE modifier_id = '".$item_id."'");
		$this->db->query("UPDATE oc_modifier SET modifier_name = '".$data['modifiername']."', max_quantity = '".$data['max_quantity']."'  WHERE id='".$item_id."'");
		foreach ($data['po_datas'] as $key => $value) {
			$this->db->query("INSERT INTO oc_modifier_items SET 
								modifier_id = '".$item_id."',
								modifier_item_id = '".$value['itemid']."',
								modifier_item_code = '".$value['itemcode']."',
								modifier_item = '".$value['item']."',
								default_item = '".$value['default_item']."'");
		}
	}

	public function deleteItem($item_id) {
		$this->db->query("DELETE FROM oc_modifier WHERE id = '" . (int)$item_id . "'");
		$this->db->query("DELETE FROM oc_modifier_items WHERE modifier_id = '" . (int)$item_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
	}

	public function getItem($item_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM oc_modifier WHERE id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getItems($data = array()) {
		$sql = "SELECT * FROM oc_modifier WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND modifier_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}

		$sort_data = array(
			'modifier_name',
			'max_quantity',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY modifier_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalItems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_modifier WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND modifier_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}


		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}