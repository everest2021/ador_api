<?php
class ModelCatalogFeedbackReport extends Model {

	public function addFeedback($data) {
		$this->db->query("INSERT INTO `oc_fb_cus_detail` SET `name` = '".$this->db->escape($data['name'])."', contact_no = '".$this->db->escape($data['contact_no'])."', email = '".$this->db->escape($data['email'])."', location = '".$this->db->escape($data['location'])."', `dob` = '".date('Y-m-d', strtotime($data['dob']))."', `doa` = '".date('Y-m-d', strtotime($data['doa']))."' ");
		$cust_id = $this->db->getLastId();

		foreach ($data['ans_datas'] as $key => $value) {
			$this->db->query("INSERT INTO `oc_feedback_transaction` SET `cust_id` = '".$cust_id."', `question_id` = '".$this->db->escape($key)."', `values` = '".$this->db->escape($value)."' ");
		}
	}


	public function editFeedback($feedback_id,$data) {

		$this->db->query("UPDATE `oc_fb_cus_detail` SET
					name = '".$this->db->escape($data['name'])."',
					contact_no = '".$this->db->escape($data['contact_no'])."',
					email = '".$this->db->escape($data['email'])."', 
					location = '".$this->db->escape($data['location'])."',
					dob = '".date('Y-m-d', strtotime($data['dob']))."',
					doa = '".date('Y-m-d', strtotime($data['doa']))."' 
					WHERE id = '".$feedback_id."'");
		//$cust_id = $this->db->getLastId();

	}
		

	public function deleteFeedback($feedback_id) {
		$this->db->query("DELETE FROM oc_feedback1 WHERE feedback_id = '" . (int)$feedback_id . "'");
	}

	public function getFeedback($feedback_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_fb_cus_detail fcd LEFT JOIN oc_feedback_transaction fbt ON (fcd.`id` = fbt.`cust_id`) WHERE fcd.`id` = '" . (int)$feedback_id . "'");
		return $query->row;
	}

	public function getFeedbacks($data = array()) {
		$sql = "SELECT * FROM oc_fb_cus_detail";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `date` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_contact_no'])) {
			$sql .= " AND `contact_no` LIKE '%" . $this->db->escape($data['filter_contact_no']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND `email` LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}


		$sort_data = array(
			'name',
			
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `date_added`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalFeedbacks($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_fb_cus_detail WHERE 1=1";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getcontacts($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "fb_cus_detail WHERE 1=1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND contact_no = '" . $this->db->escape($data['filter_name']) . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}

}