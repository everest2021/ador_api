<?php
class ModelCatalogPurchaseentryLoose extends Model {
	public function addpurchaseentry($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['update_master'])){
			$data['update_master'] = 0;
		}
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "INSERT INTO `oc_purchase_loose_transaction` SET 
				`supplier_id` = '".$this->db->escape($data['supplier_id'])."',
				`supplier_name` = '".$this->db->escape($data['supplier_name'])."',
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`update_master` = '".$this->db->escape($data['update_master'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`item_type` = '".$this->db->escape($data['item_type'])."',
				`category` = '".$this->db->escape($data['category'])."',
				`tp_no` = '".$this->db->escape($data['tp_no'])."',
				`batch_no` = '".$this->db->escape($data['batch_no'])."',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_amt` = '".$this->db->escape($data['total_amt'])."',
				`total_cash_discount` = '".$this->db->escape($data['total_cash_discount'])."',
				`total_tax` = '".$this->db->escape($data['total_tax'])."',
				`total_add_charges` = '".$this->db->escape($data['total_add_charges'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				";
		$this->db->query($sql);
		//$this->log->write($sql);
		$id = $this->db->getLastId();

		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
				$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
			}
			$sql = "INSERT INTO `oc_purchase_loose_items_transaction` SET 
				`p_id` = '".$this->db->escape($id)."',
				`sub`  = '1',
				`loose` = '1',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`item_category` = '".$this->db->escape($pvalue['item_category'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($pvalue['unit'])."',
				`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
				`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
				`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
				`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
				`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
				`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
				";
			$this->db->query($sql);
			//$this->log->write($sql);
			$brand_data = $this->db->query("SELECT * FROM oc_brand WHERE brand_id = '".$pvalue['description_id']."' AND type_id = '".$pvalue['type_id']."'")->row; 
			$brand_type = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 1")->row;
			$sql1 = "INSERT INTO `oc_purchase_loose_items_transaction` SET 
				`p_id` = '".$this->db->escape($id)."',
				`add`  = '1',
				`loose` = '1',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`item_category` = '".$this->db->escape($pvalue['item_category'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($brand_type['size'])."',
				`unit_id` = '".$this->db->escape($brand_type['id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
				`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
				`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
				`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
				`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
				`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
				";
			//$this->db->query($sql1);
			//$this->log->write($sql1);
			if($data['update_master'] == 1){
				$sql = "UPDATE `oc_stock_item` SET `purchase_rate` = '" .$pvalue['rate']. "' WHERE `id` = '".$pvalue['description_id']."' ";
				$this->db->query($sql);
				$this->log->write($sql);

				$quantity_master = $pvalue['qty'] + $pvalue['free_qty'];
				$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
				//$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . "), `purchase_price` = '".$pvalue['rate']."'  WHERE `item_code` = '".$pvalue['description_code']."' ";
				$this->db->query($sql);
				$this->log->write($sql);
			}
		}
		return $id;
	}

	public function editpurchaseentry($data, $id) {
		if(!isset($data['update_master'])){
			$data['update_master'] = 0;
		}
		if($data['invoice_date'] != '' && $data['invoice_date'] != '00-00-0000'){
			$data['invoice_date']  = date('Y-m-d', strtotime($data['invoice_date']));
		}
		$sql = "UPDATE `oc_purchase_loose_transaction` SET 
				`supplier_id` = '".$this->db->escape($data['supplier_id'])."',
				`supplier_name` = '".$this->db->escape($data['supplier_name'])."',
				`invoice_no` = '".$this->db->escape($data['invoice_no'])."',
				`invoice_date` = '".$this->db->escape($data['invoice_date'])."',
				`store_id` = '".$this->db->escape($data['store_id'])."',
				`store_name` = '".$this->db->escape($data['store_name'])."',
				`update_master` = '".$this->db->escape($data['update_master'])."',
				`narration` = '".$this->db->escape($data['narration'])."',
				`item_type` = '".$this->db->escape($data['item_type'])."',
				`tp_no` = '".$this->db->escape($data['tp_no'])."',
				`batch_no` = '".$this->db->escape($data['batch_no'])."',
				`total_qty` = '".$this->db->escape($data['total_qty'])."',
				`total_amt` = '".$this->db->escape($data['total_amt'])."',
				`total_cash_discount` = '".$this->db->escape($data['total_cash_discount'])."',
				`total_tax` = '".$this->db->escape($data['total_tax'])."',
				`total_add_charges` = '".$this->db->escape($data['total_add_charges'])."',
				`total_pay` = '".$this->db->escape($data['total_pay'])."'
				WHERE `id` = '".$id."'
				";
		$this->db->query($sql);
		//$this->log->write($sql);

		$this->db->query("DELETE FROM `oc_purchase_loose_items_transaction` WHERE `p_id` = '".$id."' ");
		foreach($data['po_datas'] as $pkey => $pvalue){
			if($pvalue['exp_date'] != '' && $pvalue['exp_date'] != '00-00-0000'){
				$pvalue['exp_date']  = date('Y-m-d', strtotime($pvalue['exp_date']));
			}
			$sql = "INSERT INTO `oc_purchase_loose_items_transaction` SET 
				`p_id` = '".$this->db->escape($id)."',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`item_category` = '".$this->db->escape($pvalue['item_category'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($pvalue['unit'])."',
				`unit_id` = '".$this->db->escape($pvalue['unit_id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
				`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
				`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
				`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
				`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
				`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`loose` = '1',
				`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
				";
			$this->db->query($sql);
			//$this->log->write($sql);
			$brand_data = $this->db->query("SELECT * FROM oc_brand WHERE brand_id = '".$pvalue['description_id']."' AND type_id = '".$pvalue['type_id']."'")->row; 
			$brand_type = $this->db->query("SELECT * FROM oc_brand_type WHERE type_id = '".$brand_data['type_id']."' AND loose = 1")->row;
			$sql1 = "INSERT INTO `oc_purchase_loose_items_transaction` SET 
				`p_id` = '".$this->db->escape($id)."',
				`add`  = '1',
				`loose` = '1',
				`description` = '".$this->db->escape($pvalue['description'])."',
				`description_id` = '".$this->db->escape($pvalue['description_id'])."',
				`description_code` = '".$this->db->escape($pvalue['description_code'])."',
				`description_barcode` = '".$this->db->escape($pvalue['description_barcode'])."',
				`description_code_search` = '".$this->db->escape($pvalue['description_code_search'])."',
				`item_category` = '".$this->db->escape($pvalue['item_category'])."',
				`qty` = '".$this->db->escape($pvalue['qty'])."',
				`avqty` = '".$this->db->escape($pvalue['avq'])."',
				`unit` = '".$this->db->escape($brand_type['size'])."',
				`unit_id` = '".$this->db->escape($brand_type['id'])."',
				`rate` = '".$this->db->escape($pvalue['rate'])."',
				`free_qty` = '".$this->db->escape($pvalue['free_qty'])."',
				`discount_percent` = '".$this->db->escape($pvalue['discount_percent'])."',
				`discount_amount` = '".$this->db->escape($pvalue['discount_amount'])."',
				`tax_name` = '".$this->db->escape($pvalue['tax_name'])."',
				`tax_percent` = '".$this->db->escape($pvalue['tax_percent'])."',
				`tax_value` = '".$this->db->escape($pvalue['tax_value'])."',
				`amount` = '".$this->db->escape($pvalue['amount'])."',
				`type` = '".$this->db->escape($pvalue['type_id'])."',
				`exp_date` = '".$this->db->escape($pvalue['exp_date'])."'
				";
			//$this->db->query($sql1);
			//$this->log->write($sql1);
			if($data['update_master'] == 1){
				// $sql = "UPDATE `oc_stock_item` SET `rate` = (rate - " . $pvalue['hidden_rate'] . ") WHERE `id` = '".$pvalue['description_id']."' ";
				// $this->db->query($sql);
				// $this->log->write($sql);

				$hidden_quantity_master = $pvalue['hidden_qty'] + $pvalue['hidden_free_qty'];
				$sql = "UPDATE `oc_item` SET `quantity` = (quantity - " . $hidden_quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
				$this->db->query($sql);
				$this->log->write($sql);

				$sql = "UPDATE `oc_stock_item` SET `purchase_rate` = '".$pvalue['rate']."' WHERE `id` = '".$pvalue['description_id']."' ";
				$this->db->query($sql);
				$this->log->write($sql);

				$quantity_master = $pvalue['qty'] + $pvalue['free_qty'];
				$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . ") WHERE `item_code` = '".$pvalue['description_code']."' ";
				//$sql = "UPDATE `oc_item` SET `quantity` = (quantity + " . $quantity_master . "), `purchase_price` = '".$pvalue['rate']."'  WHERE `item_code` = '".$pvalue['description_code']."' ";
				$this->db->query($sql);
				$this->log->write($sql);
			}
		}
		return $id;
	}

	public function deletepurchaseentry($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "purchase_loose_transaction SET delete_status = '1' WHERE id = '" . (int)$id . "' ");
	}

	public function getPurchaseentry($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "purchase_loose_transaction` WHERE `id` = '" . (int)$id . "'");
		return $query->rows;
	}

	public function getPurchaseentryby_supp_invoice($supplier_id, $invoice_no) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "purchase_loose_transaction` WHERE `supplier_id` = '" . (int)$supplier_id . "' AND `invoice_no` = '".$invoice_no."' ");
		return $query->rows;
	}
}