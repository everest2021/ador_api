<?php
class ModelCatalogItem extends Model {
	public function addItem($data) {
		// echo'<pre>';
		// print_r($data);
		// exit;

		if(!isset($data['activate_item'])){
			$data['activate_item'] = 0;
		}
		if(!isset($data['stock_register'])){
			$data['stock_register'] = 0;
		}
		if(!isset($data['create_stock_item'])){
			$data['create_stock_item'] = 0;
		}
		if(!isset($data['decimal_mesurement'])){
			$data['decimal_mesurement'] = 0;
		}
		if(!isset($data['no_discount'])){
			$data['no_discount'] = 0;
		}
		if(!isset($data['hot_key'])){
			$data['hot_key'] = 0;
		}
		if(!isset($data['with_qty'])){
			$data['with_qty'] = 0;
		}
		if(!isset($data['gst_incu'])){
			$data['gst_incu'] = 0;
		}
		if(!isset($data['inapp'])){
			$data['inapp'] = 0;
		}
		if(!isset($data['ismodifier'])){
			$data['ismodifier'] = 0;
		}

		if($data['create_stock_item'] == 1 && $data['is_create_stock'] == 0){
			$data['is_create_stock'] = 1;
			$stockitem_code = $this->db->query("SELECT `item_code` FROM `oc_stock_item` ORDER BY id DESC LIMIT 1 ");
			if($stockitem_code->num_rows > 0){
				$stockitem_code = $stockitem_code->row['item_code'] + 1;
			} else {
				$stockitem_code = 5000;
			}

			$store_name = $this->db->query("SELECT `id`,`store_name` FROM `oc_store_name` ORDER BY id ASC LIMIT 1 ");
			
			if($store_name->num_rows > 0){
				$store_id = $store_name->row['id'];
				$store_name = $store_name->row['store_name'];
				
			} else {
				$store_id = "";
				$store_name = "";
				
			}
			// echo'<pre>';
			// print_r($store_id);
			// exit;
			$category_code = $this->db->query("SELECT `code` FROM `oc_stockcategory` ORDER BY id DESC LIMIT 1 ");
			if($category_code->num_rows > 0){
				$category_code = $category_code->row['code'] + 1;
			} else {
				$category_code = 1;
			}
			$this->db->query("INSERT INTO oc_stockcategory SET
								code = '".$category_code."',
								name = '".$data['item_name']."'
						");

			$this->db->query("INSERT INTO " . DB_PREFIX . "stock_item SET 
							item_code = '" . $stockitem_code . "',
							bar_code = '" . $this->db->escape($data['barcode']) . "',
							item_name= '" . $this->db->escape($data['item_name']) . "',
							purchase_rate = '" . $this->db->escape($data['purchase_price']) . "',
							item_type= 'Raw',
							uom = '" . $this->db->escape($data['uom']) . "',
							store_name = '" . $store_name . "',
							store_id = '" . $store_id . "',
							show_in_purchase = '1',
							item_category = 'Food',
							manage_stock = '1' 

							");
			


			$this->db->query("INSERT INTO oc_bom SET
								stock_item_code = '" . $this->db->escape($data['item_code']) . "',
								stock_item_name = '".$data['item_name']."'
						");

			$this->db->query("INSERT INTO oc_bom_items SET
									parent_item_code = '" . $this->db->escape($data['item_code']) . "',
									item_code = '".$stockitem_code."',
									item_name = '".$data['item_name']."',
									qty = '1',
									unit_id = '".$data['uom']."',
									cost = '".$data['purchase_price']."',
									store_id = '".$store_id."',
									store_name = '".$store_name."'
									");
		}


		$this->db->query("INSERT INTO " . DB_PREFIX . "item SET 
							item_code = '" . $this->db->escape($data['item_code']) . "',
							activate_item = '" . $this->db->escape($data['activate_item']) . "',
							stock_register= '" . $this->db->escape($data['stock_register']) . "',
							create_stock_item= '" . $this->db->escape($data['create_stock_item']) . "',
							decimal_mesurement = '" . $this->db->escape($data['decimal_mesurement']) . "',
							inapp = '" . $this->db->escape($data['inapp']) . "',
							ismodifier = '" . $this->db->escape($data['ismodifier']) . "',
							department = '" . $this->db->escape($data['department']) . "',
							kitchen_dispaly = '" . $this->db->escape($data['kitchen_dispaly']) . "',
							item_name = '" . htmlspecialchars_decode($data['item_name']) . "',
							purchase_price = '" . $this->db->escape($data['purchase_price']) . "',
							description = '" . $this->db->escape($data['description']) . "',
							ingredient = '" . $this->db->escape($data['ingredient']) . "',
							no_discount = '" . $this->db->escape($data['no_discount']) . "',
							rate_1 = '" . $this->db->escape($data['rate_1']) . "',
							rate_2 = '" . $this->db->escape($data['rate_2']) . "',
							rate_3 = '" . $this->db->escape($data['rate_3']) . "',
							rate_4 = '" . $this->db->escape($data['rate_4']) . "',
							rate_5 = '" . $this->db->escape($data['rate_5']) . "',
							rate_6 = '" . $this->db->escape($data['rate_6']) . "',
							api_rate = '" . $this->db->escape($data['api_rate']) . "',
							inc_rate_1 = '" . $this->db->escape($data['inc_rate_1']) . "',
							inc_rate_2 = '" . $this->db->escape($data['inc_rate_2']) . "',
							inc_rate_3 = '" . $this->db->escape($data['inc_rate_3']) . "',
							inc_rate_4 = '" . $this->db->escape($data['inc_rate_4']) . "',
							inc_rate_5 = '" . $this->db->escape($data['inc_rate_5']) . "',
							inc_rate_6 = '" . $this->db->escape($data['inc_rate_6']) . "',
							inc_api_rate = '" . $this->db->escape($data['inc_api_rate']) . "',
							gst_incu = '" . $this->db->escape($data['gst_incu']) . "',
							short_name = '" . htmlspecialchars_decode($data['short_name']) . "',
							uom = '" . $this->db->escape($data['uom']) . "',
							company = '" . $this->db->escape($data['company']) . "',
							item_category = '" . $this->db->escape($data['item_category']) . "',
							item_category_id = '" . $this->db->escape($data['item_category_id']) . "',
							item_sub_category = '" . $this->db->escape($data['item_sub_category']) . "',
							item_sub_category_id = '" . $this->db->escape($data['item_sub_category_id']) . "',
							mrp = '" . $this->db->escape($data['mrp']) . "',
							modifier_group = '" . $this->db->escape($data['modifier_group']) . "',
							modifier_qty = '" . $this->db->escape($data['modifier_qty']) . "',
							max_item = '" . $this->db->escape($data['max_item']) . "',
							vat = '" . $this->db->escape($data['vat']) . "',
							tax2 = '" . $this->db->escape($data['tax2']) . "',
							kotmsg_cat_id = '" . $this->db->escape($data['kotmsg_cat']) . "',
							s_tax = '" . $this->db->escape($data['s_tax']) . "',
							brand = '" . $this->db->escape($data['brand']) . "',
							brand_id = '" . $this->db->escape($data['brand_id']) . "',
							height = '" . $this->db->escape($data['height']) . "',
							width = '" . $this->db->escape($data['width']) . "',
							quantity = '" . $this->db->escape($data['quantity']) . "',
							colour = '" . $this->db->escape($data['colour']) . "',
							hot_key = '" . $this->db->escape($data['hot_key']) . "',
							captain_rate_per = '" . $this->db->escape($data['captain_rate_per']) . "',
							captain_rate_value = '" . $this->db->escape($data['captain_rate_value']) . "',
							barcode = '" . $this->db->escape($data['barcode']) . "',
							with_qty = '" . $this->db->escape($data['with_qty']) . "',
							stweward_rate_per = '" . $this->db->escape($data['stweward_rate_per']) . "',
							packaging_amt = '" . $this->db->escape($data['packaging_charge']) . "',
							serves = '" . $this->db->escape($data['serves']) . "',
							stweward_rate_value = '" . $this->db->escape($data['stweward_rate_value']) . "',
			                is_liq = '" . $this->db->escape($data['is_liq']) . "',
							photo = '" .$this->db->escape($data['photo']). "',
							change_status = '0',
                            photo_source = '" .$this->db->escape($data['photo_source']). "',
                            birthday_point = '" . $this->db->escape($data['birthday_point']) . "',
                            item_stock_api = '" . $this->db->escape($data['item_stock_api']) . "',
                            food_type_api = '" . $this->db->escape($data['food_type_api']) . "',
                            weight_for_api = '" . $this->db->escape($data['weight_for_api']) . "',
                            fullfillmode = '" . (isset($data['fullfillmode']) ? $this->db->escape(json_encode($data['fullfillmode'])) : '') . "', 
                            platfome_included = '" . (isset($data['platfome_included']) ? $this->db->escape(json_encode($data['platfome_included'])) : '') . "',
                            is_create_stock = '" . $this->db->escape($data['is_create_stock']) . "',
                            normal_point = '" . $this->db->escape($data['normal_point']) . "' ");
		//echo "query";
		//exit();
		$item_id = $this->db->getLastId();
		return $item_id;
	}

	public function editItem($item_id, $data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		if(!isset($data['activate_item'])){
			$data['activate_item'] = 0;
		}
		if(!isset($data['stock_register'])){
			$data['stock_register'] = 0;
		}
		if(!isset($data['create_stock_item'])){
			$data['create_stock_item'] = 0;
		}
		if(!isset($data['decimal_mesurement'])){
			$data['decimal_mesurement'] = 0;
		}
		if(!isset($data['no_discount'])){
			$data['no_discount'] = 0;
		}
		if(!isset($data['hot_key'])){
			$data['hot_key'] = 0;
		}
		if(!isset($data['with_qty'])){
			$data['with_qty'] = 0;
		}
		if(!isset($data['gst_incu'])){
			$data['gst_incu'] = 0;
		}
		if(!isset($data['inapp'])){
			$data['inapp'] = 0;
		}
		if(!isset($data['ismodifier'])){
			$data['ismodifier'] = 0;
		}

		if($data['create_stock_item'] == 1 && $data['is_create_stock'] == 0){
			$data['is_create_stock'] = 1;
			$stockitem_code = $this->db->query("SELECT `item_code` FROM `oc_stock_item` ORDER BY id DESC LIMIT 1 ");
			if($stockitem_code->num_rows > 0){
				$stockitem_code = $stockitem_code->row['item_code'] + 1;
			} else {
				$stockitem_code = 5000;
			}

			$store_name = $this->db->query("SELECT `id`,`store_name` FROM `oc_store_name` ORDER BY id ASC LIMIT 1 ");
			
			if($store_name->num_rows > 0){
				$store_id = $store_name->row['id'];
				$store_name = $store_name->row['store_name'];
				
			} else {
				$store_id = "";
				$store_name = "";
				
			}
			// echo'<pre>';
			// print_r($store_id);
			// exit;
			$category_code = $this->db->query("SELECT `code` FROM `oc_stockcategory` ORDER BY id DESC LIMIT 1 ");
			if($category_code->num_rows > 0){
				$category_code = $category_code->row['code'] + 1;
			} else {
				$category_code = 1;
			}
			$this->db->query("INSERT INTO oc_stockcategory SET
								code = '".$category_code."',
								name = '".$data['item_name']."'
						");

			$this->db->query("INSERT INTO " . DB_PREFIX . "stock_item SET 
							item_code = '" . $stockitem_code . "',
							bar_code = '" . $this->db->escape($data['barcode']) . "',
							item_name= '" . $this->db->escape($data['item_name']) . "',
							purchase_rate = '" . $this->db->escape($data['purchase_price']) . "',
							item_type= 'Raw',
							uom = '" . $this->db->escape($data['uom']) . "',
							
							store_name = '" . $store_name . "',
							store_id = '" . $store_id . "',
							show_in_purchase = '1',
							item_category = 'Food',
							manage_stock = '1' 

							");
			$bom_code = $this->db->query("SELECT `stock_item_code` FROM `oc_bom` ORDER BY id DESC LIMIT 1 ");
			if($bom_code->num_rows > 0){
				$bom_code = $bom_code->row['stock_item_code'] + 1;
			} else {
				$bom_code = 1300;
			}


			$this->db->query("INSERT INTO oc_bom SET
								stock_item_code = '" . $this->db->escape($data['item_code']) . "',
								stock_item_name = '".$data['item_name']."'
						");

			$this->db->query("INSERT INTO oc_bom_items SET
									parent_item_code = '" . $this->db->escape($data['item_code']) . "',
									item_code = '".$stockitem_code."',
									item_name = '".$data['item_name']."',
									qty = '1',
									unit_id = '".$data['uom']."',
									cost = '".$data['purchase_price']."',
									store_id = '".$store_id."',
									store_name = '".$store_name."'
									");
		


		}
		$this->db->query("UPDATE " . DB_PREFIX ."item SET 
							item_code = '" . $this->db->escape($data['item_code']) . "',
							activate_item = '" . $this->db->escape($data['activate_item']) . "',
							stock_register= '" . $this->db->escape($data['stock_register']) . "',
							create_stock_item= '" . $this->db->escape($data['create_stock_item']) . "',
							decimal_mesurement = '" . $this->db->escape($data['decimal_mesurement']) . "',
							inapp = '" . $this->db->escape($data['inapp']) . "',
							ismodifier = '" . $this->db->escape($data['ismodifier']) . "',
							department = '" . $this->db->escape($data['department']) . "',
							kitchen_dispaly = '" . $this->db->escape($data['kitchen_dispaly']) . "',
							item_name = '" . htmlspecialchars_decode($data['item_name']) . "',
							purchase_price = '" . $this->db->escape($data['purchase_price']) . "',
							description = '" . $this->db->escape($data['description']) . "',
							ingredient = '" . $this->db->escape($data['ingredient']) . "',
							no_discount = '" . $this->db->escape($data['no_discount']) . "',
							rate_1 = '" . $this->db->escape($data['rate_1']) . "',
							rate_2 = '" . $this->db->escape($data['rate_2']) . "',
							rate_3 = '" . $this->db->escape($data['rate_3']) . "',
							rate_4 = '" . $this->db->escape($data['rate_4']) . "',
							rate_5 = '" . $this->db->escape($data['rate_5']) . "',
							rate_6 = '" . $this->db->escape($data['rate_6']) . "',
							api_rate = '" . $this->db->escape($data['api_rate']) . "',
							inc_rate_1 = '" . $this->db->escape($data['inc_rate_1']) . "',
							inc_rate_2 = '" . $this->db->escape($data['inc_rate_2']) . "',
							inc_rate_3 = '" . $this->db->escape($data['inc_rate_3']) . "',
							inc_rate_4 = '" . $this->db->escape($data['inc_rate_4']) . "',
							inc_rate_5 = '" . $this->db->escape($data['inc_rate_5']) . "',
							inc_rate_6 = '" . $this->db->escape($data['inc_rate_6']) . "',
							inc_api_rate = '" . $this->db->escape($data['inc_api_rate']) . "',
							gst_incu = '" . $this->db->escape($data['gst_incu']) . "',
							short_name = '" . htmlspecialchars_decode($data['short_name']) . "',
							uom = '" . $this->db->escape($data['uom']) . "',
							company = '" . $this->db->escape($data['company']) . "',
							item_category = '" . $this->db->escape($data['item_category']) . "',
							item_category_id = '" . $this->db->escape($data['item_category_id']) . "',
							item_sub_category = '" . $this->db->escape($data['item_sub_category']) . "',
							item_sub_category_id = '" . $this->db->escape($data['item_sub_category_id']) . "',
							mrp = '" . $this->db->escape($data['mrp']) . "',
							modifier_group = '" . $this->db->escape($data['modifier_group']) . "',
							modifier_qty = '" . $this->db->escape($data['modifier_qty']) . "',
							max_item = '" . $this->db->escape($data['max_item']) . "',
							vat = '" . $this->db->escape($data['vat']) . "',
							tax2 = '" . $this->db->escape($data['tax2']) . "',
							kotmsg_cat_id = '" . $this->db->escape($data['kotmsg_cat']) . "',
							s_tax = '" . $this->db->escape($data['s_tax']) . "',
							brand = '" . $this->db->escape($data['brand']) . "',
							brand_id = '" . $this->db->escape($data['brand_id']) . "',
							height = '" . $this->db->escape($data['height']) . "',
							width = '" . $this->db->escape($data['width']) . "',
							quantity = '" . $this->db->escape($data['quantity']) . "',
							colour = '" . $this->db->escape($data['colour']) . "',
							hot_key = '" . $this->db->escape($data['hot_key']) . "',
							captain_rate_per = '" . $this->db->escape($data['captain_rate_per']) . "',
							captain_rate_value = '" . $this->db->escape($data['captain_rate_value']) . "',
							barcode = '" . $this->db->escape($data['barcode']) . "',
							with_qty = '" . $this->db->escape($data['with_qty']) . "',
							stweward_rate_per = '" . $this->db->escape($data['stweward_rate_per']) . "',
							packaging_amt = '" . $this->db->escape($data['packaging_charge']) . "',
							serves = '" . $this->db->escape($data['serves']) . "',
							item_stock_api = '" . $this->db->escape($data['item_stock_api']) . "',
							food_type_api = '" . $this->db->escape($data['food_type_api']) . "',
							stweward_rate_value = '" . $this->db->escape($data['stweward_rate_value']) . "',
			                is_liq = '" . $this->db->escape($data['is_liq']) . "',
							photo = '" .$this->db->escape($data['photo']). "',
							change_status = '1',
                            photo_source = '" .$this->db->escape($data['photo_source']). "',
                             birthday_point = '" . $this->db->escape($data['birthday_point']) . "',
                            weight_for_api = '" . $this->db->escape($data['weight_for_api']) . "',
                            fullfillmode = '" . (isset($data['fullfillmode']) ? $this->db->escape(json_encode($data['fullfillmode'])) : '') . "', 
                            platfome_included = '" . (isset($data['platfome_included']) ? $this->db->escape(json_encode($data['platfome_included'])) : '') . "',
                            is_create_stock = '" . $this->db->escape($data['is_create_stock']) . "',
                            normal_point = '" . $this->db->escape($data['normal_point']) . "'
                            WHERE item_id = '" . (int)$item_id . "'");
		return $item_id;
	}

	public function deleteItem($item_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "'");
	}

	public function getItem($item_id) {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "item WHERE item_id = '" . (int)$item_id . "' ");
		return $query->row;
	}

	public function getItems($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "item WHERE 1=1";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND item_id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND LCASE(item_name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_item_name'])) . "%'";
			$sql .= " OR short_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}
		
		if (!empty($data['filter_item_code'])) {
			$sql .= " AND item_code = '" . $this->db->escape($data['filter_item_code']) . "'";
		}

		if (!empty($data['filter_barcode'])) {
			$sql .= " AND barcode LIKE '%" . $this->db->escape($data['filter_barcode']) . "%'";
		}

		if (isset($data['filter_category']) && !empty($data['filter_category'])) {
			$sql .= " AND item_category_id = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (isset($data['filter_sub_category']) && !empty($data['filter_sub_category'])) {
			$sql .= " AND item_sub_category_id = '" . $this->db->escape($data['filter_sub_category']) . "'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND inapp = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		$sort_data = array(
			'item_name',
			'item_code',
			'department',
			'item_category',
			'barcode',
			'rate_1',
			'rate_2',
			'rate_3',
			'rate_4',
			'rate_5',
			'rate_6',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY item_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getItems1($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "item WHERE ismodifier = 0 AND activate_item = 1";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND item_id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND item_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
			$sql .= " OR short_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}
		
		if (!empty($data['filter_item_code'])) {
			$sql .= " AND item_code = '" . $this->db->escape($data['filter_item_code']) . "'";
		}

		if (!empty($data['filter_barcode'])) {
			$sql .= " AND barcode LIKE '%" . $this->db->escape($data['filter_barcode']) . "%'";
		}

		if (isset($data['filter_category']) && !empty($data['filter_category'])) {
			$sql .= " AND item_category_id = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (isset($data['filter_sub_category']) && !empty($data['filter_sub_category'])) {
			$sql .= " AND item_sub_category_id = '" . $this->db->escape($data['filter_sub_category']) . "'";
		}

		$sort_data = array(
			'item_name',
			'item_code',
			'department',
			'item_category',
			'barcode',
			'rate_1',
			'rate_2',
			'rate_3',
			'rate_4',
			'rate_5',
			'rate_6',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY item_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getItemscode($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "item WHERE ismodifier = 0 AND activate_item = 1";

		if (isset($data['filter_id']) && !empty($data['filter_id'])) {
			$sql .= " AND item_code = '" . $data['filter_id'] . "' ";
		}

		
		$sort_data = array(
			'item_name',
			'item_code',
			'department',
			'item_category',
			'barcode',
			'rate_1',
			'rate_2',
			'rate_3',
			'rate_4',
			'rate_5',
			'rate_6',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY item_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}

	public function getTotalItems($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "item WHERE 1=1 ";

		if (isset($data['filter_item_id']) && !empty($data['filter_item_id'])) {
			$sql .= " AND item_id = '" . $data['filter_item_id'] . "' ";
		}

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND item_name LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}
		
		if (!empty($data['filter_item_code'])) {
			$sql .= " AND item_code LIKE '%" . $this->db->escape($data['filter_item_code']) . "%'";
		}

		if (!empty($data['filter_barcode'])) {
			$sql .= " AND barcode LIKE '%" . $this->db->escape($data['filter_barcode']) . "%'";
		}

		if (isset($data['filter_category']) && !empty($data['filter_category'])) {
			$sql .= " AND item_category_id = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND inapp = '" . $this->db->escape($data['filter_status']) . "' ";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getDepartment() {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department");
		return $query->rows;
	}

	public function getCategory() {
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "sport WHERE sport_id = '" . (int)$sport_id . "' ";
		//exit;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category");
		return $query->rows;
	}

	public function getSubCategory() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "subcategory");
		return $query->rows;
	}
}