<?php
class ModelCatalogNotification extends Model {
	public function addTeacher($data) {
		$this->db->query("INSERT INTO oc_notification SET 
							title = '" . $this->db->escape($data['title']) . "',
							description = '" . $this->db->escape($data['description']) . "',
							date = '" . $this->db->escape($data['date']) . "',
							time = '" . $this->db->escape($data['time']) . "'
						");
		$id = $this->db->getLastId();
		return $id;
	}

	public function editTeacher($id, $data) {
		$this->db->query("UPDATE oc_notification SET 
							title = '" . $this->db->escape($data['title']) . "',
							description = '" . $this->db->escape($data['description']) . "',
							date = '" . $this->db->escape($data['date']) . "',
							time = '" . $this->db->escape($data['time']) . "'
							WHERE id = '" . (int)$id . "'");
	}

	public function deleteTeacher($id) {
		$this->db->query("DELETE FROM oc_notification WHERE id = '" . (int)$id . "'");
	}

	public function getTeacher($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM oc_notification WHERE id = '" . (int)$id . "'");

		return $query->row;
	}

	public function getTeachers($data = array()) {

		//echo "<pre>";print_r($data);exit;
		$sql = "SELECT * FROM oc_notification";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_title'])) {
			$sql .= " AND title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
		}

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . $this->db->escape($data['filter_id']) . "'";
		}

		$sort_data = array(
			'title',
			'date',
			'time'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY title";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTeachers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM oc_notification";

		$sql .= " WHERE 1=1";

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . $this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_title'])) {
			$sql .= " AND title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}