<?php
class ModelCatalogurbanpiperitemshow extends Model {
	public function getItems($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "item_urbanpiper WHERE 1=1";

		if (!empty($data['filter_item_name'])) {
			$sql .= " AND title LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}
		
		if (!empty($data['filter_item_code'])) {
			$sql .= " AND item_id = '" . $this->db->escape($data['filter_item_code']) . "'";
		}

		$sort_data = array(
			'title',
			'item_id',
		);


		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY title";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		//$this->log->write($sql);

		$query = $this->db->query($sql);

		// echo "<pre>";
		// print_r($query);
		// exit();

		return $query->rows;
	}



	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "item_urbanpiper WHERE 1=1 ";
		
		if (!empty($data['filter_item_name'])) {
			$sql .= " AND title LIKE '%" . $this->db->escape($data['filter_item_name']) . "%'";
		}
		
		if (!empty($data['filter_item_code'])) {
			$sql .= " AND item_id = '" . $this->db->escape($data['filter_item_code']) . "'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
