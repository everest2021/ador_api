<?php
class ModelUserUserGroup extends Model {
	public function addUserGroup($data) {

		$permission_datas = array();
		if(isset($data['permission']['access'])){
			$permission_datas['access'] = $data['permission']['access'];
		} else {
			$permission_datas['access'] = array();
		}
		if(isset($data['permission']['modify'])){
			$permission_datas['modify'] = $data['permission']['modify'];
		} else {
			$permission_datas['modify'] = array();
		}	
		$permission_datas['access'][] = 'catalog/table';
		if($user_group_id == 1){	
			//$permission_datas['access'][] = 'user/user';
			//$permission_datas['access'][] = 'user/user_permission';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'common/home';

			//$permission_datas['modify'][] = 'user/user';
			//$permission_datas['modify'][] = 'user/user_permission';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
		}
		$permission_datas['modify'][] = 'catalog/repair';

		$permission_datas['modify'][] = 'catalog/werapending_order';
		$permission_datas['modify'][] = 'catalog/werafood_itemdata';
		$permission_datas['modify'][] = 'catalog/werafood_order_report';
		$permission_datas['modify'][] = 'catalog/werafood_reject_report';
		$permission_datas['modify'][] = 'catalog/store_onoff';


		$permission_datas['modify'][] = 'catalog/dailybill';
		$permission_datas['modify'][] = 'catalog/dailybillwiseitem_report';
		$permission_datas['modify'][] = 'catalog/dailyreportstock';
		$permission_datas['modify'][] = 'catalog/dailydaysummaryreport';
		$permission_datas['modify'][] = 'catalog/dailyminidaysummaryreport';

		$permission_datas['modify'][] = 'catalog/dailyshiftclose_report';

		$permission_datas['modify'][] = 'catalog/msr_ledger_report';
		$permission_datas['modify'][] = 'catalog/msr_redeem_report';
		$permission_datas['modify'][] = 'catalog/customerwise_item_report';
		$permission_datas['modify'][] = 'catalog/stockclear';
		$permission_datas['modify'][] = 'catalog/onlinepayment';

		$permission_datas['modify'][] = 'catalog/customerwise_sale_report';
		$permission_datas['modify'][] = 'catalog/msr_redeem';
		$permission_datas['modify'][] = 'catalog/stock_invoice';
		$permission_datas['modify'][] = 'catalog/sms_report';
		$permission_datas['modify'][] = 'catalog/feedback';
		$permission_datas['modify'][] = 'catalog/feedback_report';
		$permission_datas['modify'][] = 'catalog/feedbackreport';
		$permission_datas['modify'][] = 'catalog/msr_recharge';
		$permission_datas['modify'][] = 'catalog/msr_refund';
		$permission_datas['modify'][] = 'catalog/msr_refund_report';
		$permission_datas['modify'][] = 'catalog/msr_recharge_report';
		$permission_datas['modify'][] = 'catalog/shiftclose_report';
		$permission_datas['modify'][] = 'catalog/billwiseitem_report';
		$permission_datas['modify'][] = 'catalog/prebill_report';
		$permission_datas['modify'][] = 'catalog/salepromotion';
		$permission_datas['modify'][] = 'catalog/sync_to';
		$permission_datas['modify'][] = 'catalog/msr_ledger';
		$permission_datas['modify'][] = 'catalog/reception';
		$permission_datas['modify'][] = 'catalog/accountexpense';
		$permission_datas['modify'][] = 'catalog/expensetrans';
		$permission_datas['modify'][] = 'catalog/nckotreport';
		$permission_datas['modify'][] = 'catalog/events';
		$permission_datas['modify'][] = 'catalog/databasebackup';
		$permission_datas['modify'][] = 'catalog/barcode';
		$permission_datas['modify'][] = 'catalog/selling_report';
		$permission_datas['modify'][] = 'catalog/shiftclose_report';
		$permission_datas['modify'][] = 'catalog/delete_report';
		$permission_datas['modify'][] = 'catalog/orderprocesss';
		$permission_datas['modify'][] = 'catalog/kitchen_display';
		$permission_datas['modify'][] = 'catalog/venue';
		$permission_datas['modify'][] = 'catalog/meal';
		$permission_datas['modify'][] = 'catalog/activity';
		$permission_datas['modify'][] = 'catalog/item';
		$permission_datas['modify'][] = 'catalog/modifier';
		$permission_datas['modify'][] = 'catalog/kotgroup';
		$permission_datas['modify'][] = 'catalog/itembulk';
		$permission_datas['modify'][] = 'catalog/department';
		$permission_datas['modify'][] = 'catalog/location';
		$permission_datas['modify'][] = 'catalog/table';
		$permission_datas['modify'][] = 'catalog/category';
		$permission_datas['modify'][] = 'catalog/subcategory';
		$permission_datas['modify'][] = 'catalog/brand';
		$permission_datas['modify'][] = 'catalog/customer';
		$permission_datas['modify'][] = 'catalog/customercredit';
		$permission_datas['modify'][] = 'catalog/waiter';
		$permission_datas['modify'][] = 'catalog/kotmsg';
		$permission_datas['modify'][] = 'catalog/kotmsgitem';
		$permission_datas['modify'][] = 'catalog/kotmsgcat';
		$permission_datas['modify'][] = 'catalog/taxmaster';
		$permission_datas['modify'][] = 'catalog/hotellocation';
		$permission_datas['modify'][] = 'catalog/order';
		$permission_datas['modify'][] = 'catalog/orderqwerty';
		$permission_datas['modify'][] = 'catalog/ordertab';
		$permission_datas['modify'][] = 'catalog/ordertouch';
		$permission_datas['modify'][] = 'catalog/billview';
		$permission_datas['modify'][] = 'catalog/dayclose';
		$permission_datas['modify'][] = 'catalog/shiftclose';
		$permission_datas['modify'][] = 'catalog/advance';
		$permission_datas['modify'][] = 'catalog/billmerge';
		$permission_datas['modify'][] = 'catalog/currentsale';
		$permission_datas['modify'][] = 'catalog/sale_stat';
		$permission_datas['modify'][] = 'catalog/kottransfer';
		$permission_datas['modify'][] = 'catalog/order_undo';
		$permission_datas['modify'][] = 'catalog/settlement';
		$permission_datas['modify'][] = 'catalog/tablechange';
		$permission_datas['modify'][] = 'catalog/onlinepayment';
		$permission_datas['modify'][] = 'catalog/complimentary';
		$permission_datas['modify'][] = 'catalog/tendering';
		$permission_datas['modify'][] = 'catalog/complimentaryreport';
		$permission_datas['modify'][] = 'catalog/billmodifyreport';
		$permission_datas['modify'][] = 'catalog/stock_report';
		$permission_datas['modify'][] = 'catalog/monthlyminireport';
		$permission_datas['modify'][] = 'catalog/reportbill';
		$permission_datas['modify'][] = 'catalog/gstbill';
		$permission_datas['modify'][] = 'catalog/reportstock';
		$permission_datas['modify'][] = 'catalog/reportdaywise';
		$permission_datas['modify'][] = 'catalog/storewisereport';
		$permission_datas['modify'][] = 'catalog/advancereport';
		$permission_datas['modify'][] = 'catalog/cancelkot';
		$permission_datas['modify'][] = 'catalog/cancelbot';
		$permission_datas['modify'][] = 'catalog/cancelbill';
		$permission_datas['modify'][] = 'catalog/duplicatebill';
		$permission_datas['modify'][] = 'catalog/currentstockreport';
		$permission_datas['modify'][] = 'catalog/currentstockreport_lsb';
		$permission_datas['modify'][] = 'catalog/purchaseorderreport';
		$permission_datas['modify'][] = 'catalog/purchasereportloose';
		$permission_datas['modify'][] = 'catalog/purchaseorder';
		$permission_datas['modify'][] = 'catalog/purchaseoutward';
		$permission_datas['modify'][] = 'catalog/stocktransfer';
		$permission_datas['modify'][] = 'catalog/stocktransferliq';
		$permission_datas['modify'][] = 'catalog/stocktransferloose';
		$permission_datas['modify'][] = 'catalog/stockmanufacturer';
		$permission_datas['modify'][] = 'catalog/stockreportliq';
		$permission_datas['modify'][] = 'catalog/stockreportloose';
		$permission_datas['modify'][] = 'catalog/stockreportfood';
		$permission_datas['modify'][] = 'catalog/bom';
		$permission_datas['modify'][] = 'catalog/setting';
		$permission_datas['modify'][] = 'catalog/trancatedata';
		$permission_datas['modify'][] = 'catalog/westage_adjustment';
		$permission_datas['modify'][] = 'catalog/captain_and_waiter_sale';
		$permission_datas['modify'][] = 'catalog/captain_and_waiter';
		$permission_datas['modify'][] = 'catalog/food_bar_report';
		$permission_datas['modify'][] = 'catalog/bomlazer';
		$permission_datas['modify'][] = 'catalog/itemwisereport';
		$permission_datas['modify'][] = 'catalog/inwardreport';
		$permission_datas['modify'][] = 'catalog/subbom';
		$permission_datas['modify'][] = 'catalog/stockcategory';
		$permission_datas['modify'][] = 'catalog/store_name';
		$permission_datas['modify'][] = 'catalog/store_item';
		$permission_datas['modify'][] = 'catalog/supplier';
		$permission_datas['modify'][] = 'catalog/purchaseentry';
		$permission_datas['modify'][] = 'catalog/purchaseentryfood';
		$permission_datas['modify'][] = 'catalog/orderapp';
		$permission_datas['modify'][] = 'catalog/swiggyorder';
		$permission_datas['modify'][] = 'catalog/bookingapp';
		$permission_datas['modify'][] = 'catalog/daysummaryreport';
		$permission_datas['modify'][] = 'catalog/creditreport';
		$permission_datas['modify'][] = 'catalog/nckot';

		$permission_datas['modify'][] = 'catalog/urbanpiper_order';
		$permission_datas['modify'][] = 'catalog/urbanpiper_itemdata';

		$permission_datas['modify'][] = 'catalog/urbanpier_store';
		$permission_datas['modify'][] = 'catalog/test_print';

		$permission_datas['modify'][] = 'catalog/webhook';
		$permission_datas['modify'][] = 'catalog/urbanpiperitem_show';
		$permission_datas['modify'][] = 'catalog/urbanpiper_store_list';
		$permission_datas['modify'][] = 'catalog/urbanpiper_platform_list';
		$permission_datas['modify'][] = 'catalog/urbanpiper_platform_item';
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? $this->db->escape(json_encode($permission_datas)) : '') . "'");
	
		return $this->db->getLastId();
	}

	public function editUserGroup($user_group_id, $data) {
		$permission_datas = array();
		if(isset($data['permission']['access'])){
			$permission_datas['access'] = $data['permission']['access'];
		} else {
			$permission_datas['access'] = array();
		}
		if(isset($data['permission']['modify'])){
			$permission_datas['modify'] = $data['permission']['modify'];
		} else {
			$permission_datas['modify'] = array();
		}
		$permission_datas['access'][] = 'catalog/table';
		if($user_group_id == 1){	
			//$permission_datas['access'][] = 'user/user';
			//$permission_datas['access'][] = 'user/user_permission';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'common/home';

			//$permission_datas['modify'][] = 'user/user';
			//$permission_datas['modify'][] = 'user/user_permission';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
		}
		$permission_datas['modify'][] = 'catalog/repair';
		
		$permission_datas['modify'][] = 'catalog/werapending_order';
		$permission_datas['modify'][] = 'catalog/werafood_itemdata';
		$permission_datas['modify'][] = 'catalog/werafood_order_report';
		$permission_datas['modify'][] = 'catalog/werafood_reject_report';
		$permission_datas['modify'][] = 'catalog/msr_ledger_report';
		$permission_datas['modify'][] = 'catalog/store_onoff';
		

		$permission_datas['modify'][] = 'catalog/feedback';
		$permission_datas['modify'][] = 'catalog/feedback_report';
		$permission_datas['modify'][] = 'catalog/feedback_report';
		$permission_datas['modify'][] = 'catalog/msr_recharge';
		$permission_datas['modify'][] = 'catalog/msr_refund';
		$permission_datas['modify'][] = 'catalog/customerwise_sale_report';

		$permission_datas['modify'][] = 'catalog/dailybill';
		$permission_datas['modify'][] = 'catalog/dailybillwiseitem_report';
		$permission_datas['modify'][] = 'catalog/dailyreportstock';
		$permission_datas['modify'][] = 'catalog/dailydaysummaryreport';
		$permission_datas['modify'][] = 'catalog/dailyshiftclose_report';

		$permission_datas['modify'][] = 'catalog/msr_redeem';
		$permission_datas['modify'][] = 'catalog/sync_to';

		$permission_datas['modify'][] = 'catalog/msr_ledger';
		$permission_datas['modify'][] = 'catalog/reception';
		$permission_datas['modify'][] = 'catalog/sms_report';
		$permission_datas['modify'][] = 'catalog/shiftclose_report';
		$permission_datas['modify'][] = 'catalog/billwiseitem_report';
		$permission_datas['modify'][] = 'catalog/prebill_report';
		$permission_datas['modify'][] = 'catalog/salepromotion';
		$permission_datas['modify'][] = 'catalog/stockclear';
		$permission_datas['modify'][] = 'catalog/onlinepayment';
		$permission_datas['modify'][] = 'catalog/nckotreport';
		$permission_datas['modify'][] = 'catalog/accountexpense';
		$permission_datas['modify'][] = 'catalog/expensetrans';
		$permission_datas['modify'][] = 'catalog/venue';
		$permission_datas['modify'][] = 'catalog/meal';
		$permission_datas['modify'][] = 'catalog/activity';
		$permission_datas['modify'][] = 'catalog/item';
		$permission_datas['modify'][] = 'catalog/modifier';
		$permission_datas['modify'][] = 'catalog/kotgroup';
		$permission_datas['modify'][] = 'catalog/itembulk';
		$permission_datas['modify'][] = 'catalog/department';
		$permission_datas['modify'][] = 'catalog/location';
		$permission_datas['modify'][] = 'catalog/table';
		$permission_datas['modify'][] = 'catalog/category';
		$permission_datas['modify'][] = 'catalog/subcategory';
		$permission_datas['modify'][] = 'catalog/brand';
		$permission_datas['modify'][] = 'catalog/customer';
		$permission_datas['modify'][] = 'catalog/customercredit';
		$permission_datas['modify'][] = 'catalog/waiter';
		$permission_datas['modify'][] = 'catalog/kotmsg';
		$permission_datas['modify'][] = 'catalog/kotmsgitem';
		$permission_datas['modify'][] = 'catalog/kotmsgcat';
		$permission_datas['modify'][] = 'catalog/taxmaster';
		$permission_datas['modify'][] = 'catalog/hotellocation';
		$permission_datas['modify'][] = 'catalog/order';
		$permission_datas['modify'][] = 'catalog/orderqwerty';
		$permission_datas['modify'][] = 'catalog/ordertab';
		$permission_datas['modify'][] = 'catalog/ordertouch';
		$permission_datas['modify'][] = 'catalog/billview';
		$permission_datas['modify'][] = 'catalog/dayclose';
		$permission_datas['modify'][] = 'catalog/shiftclose';
		$permission_datas['modify'][] = 'catalog/advance';
		$permission_datas['modify'][] = 'catalog/billmerge';
		$permission_datas['modify'][] = 'catalog/currentsale';
		$permission_datas['modify'][] = 'catalog/salestat';

		$permission_datas['modify'][] = 'catalog/kottransfer';
		$permission_datas['modify'][] = 'catalog/order_undo';
		$permission_datas['modify'][] = 'catalog/shiftclose_report';
		$permission_datas['modify'][] = 'catalog/delete_report';
		$permission_datas['modify'][] = 'catalog/settlement';
		$permission_datas['modify'][] = 'catalog/onlinepayment';
		$permission_datas['modify'][] = 'catalog/westage_adjustment';
		$permission_datas['modify'][] = 'catalog/billmodifyreport';
		$permission_datas['modify'][] = 'catalog/complimentary';
		$permission_datas['modify'][] = 'catalog/tendering';
		$permission_datas['modify'][] = 'catalog/complimentaryreport';
		$permission_datas['modify'][] = 'catalog/tablechange';
		$permission_datas['modify'][] = 'catalog/monthlyminireport';
		$permission_datas['modify'][] = 'catalog/reportbill';
		$permission_datas['modify'][] = 'catalog/gstbill';
		$permission_datas['modify'][] = 'catalog/reportstock';
		$permission_datas['modify'][] = 'catalog/reportdaywise';
		$permission_datas['modify'][] = 'catalog/advancereport';
		$permission_datas['modify'][] = 'catalog/cancelkot';
		$permission_datas['modify'][] = 'catalog/cancelbot';
		$permission_datas['modify'][] = 'catalog/cancelbill';
		$permission_datas['modify'][] = 'catalog/duplicatebill';
		$permission_datas['modify'][] = 'catalog/currentstockreport';
		$permission_datas['modify'][] = 'catalog/currentstockreport_lsb';
		
		$permission_datas['modify'][] = 'catalog/purchaseorderreport';
		$permission_datas['modify'][] = 'catalog/purchasereportloose';
		$permission_datas['modify'][] = 'catalog/storewisereport';
		$permission_datas['modify'][] = 'catalog/purchaseorder';
		$permission_datas['modify'][] = 'catalog/purchaseoutward';
		$permission_datas['modify'][] = 'catalog/stocktransfer';
		$permission_datas['modify'][] = 'catalog/stocktransferliq';
		$permission_datas['modify'][] = 'catalog/stocktransferloose';
		$permission_datas['modify'][] = 'catalog/stockmanufacturer';
		$permission_datas['modify'][] = 'catalog/stockreportliq';
		$permission_datas['modify'][] = 'catalog/stockreportloose';
		$permission_datas['modify'][] = 'catalog/stockreportfood';
		$permission_datas['modify'][] = 'catalog/bom';
		$permission_datas['modify'][] = 'catalog/bomlazer';
		$permission_datas['modify'][] = 'catalog/itemwisereport';
		$permission_datas['modify'][] = 'catalog/inwardreport';
		$permission_datas['modify'][] = 'catalog/setting';
		$permission_datas['modify'][] = 'catalog/stock_report';
		$permission_datas['modify'][] = 'catalog/orderprocesss';
		$permission_datas['modify'][] = 'catalog/kitchen_display';
		$permission_datas['modify'][] = 'catalog/captain_and_waiter';
		$permission_datas['modify'][] = 'catalog/food_bar_report';
		$permission_datas['modify'][] = 'catalog/subbom';
		$permission_datas['modify'][] = 'catalog/stockcategory';
		$permission_datas['modify'][] = 'catalog/store_name';
		$permission_datas['modify'][] = 'catalog/store_item';
		$permission_datas['modify'][] = 'catalog/supplier';
		$permission_datas['modify'][] = 'catalog/purchaseentry';
		$permission_datas['modify'][] = 'catalog/orderapp';
		$permission_datas['modify'][] = 'catalog/swiggyorder';
		$permission_datas['modify'][] = 'catalog/bookingapp';
		$permission_datas['modify'][] = 'catalog/daysummaryreport';
		$permission_datas['modify'][] = 'catalog/creditreport';
		$permission_datas['modify'][] = 'catalog/nckot';
		$permission_datas['modify'][] = 'catalog/urbanpiper_order';
		$permission_datas['modify'][] = 'catalog/urbanpier_store';
		$permission_datas['modify'][] = 'catalog/test_print';
		$permission_datas['modify'][] = 'catalog/urbanpiper_itemdata';
		$permission_datas['modify'][] = 'catalog/webhook';
		$permission_datas['modify'][] = 'catalog/urbanpiperitem_show';
		$permission_datas['modify'][] = 'catalog/urbanpiper_store_list';
		$permission_datas['modify'][] = 'catalog/urbanpiper_platform_list';
		$permission_datas['modify'][] = 'catalog/urbanpiper_platform_item';

		$this->db->query("UPDATE " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? $this->db->escape(json_encode($permission_datas)) : '') . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function deleteUserGroup($user_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function getUserGroup($user_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		$user_group = array(
			'name'       => $query->row['name'],
			'permission' => json_decode($query->row['permission'], true)
		);

		return $user_group;
	}

	public function getUserGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group WHERE `user_group_id` <> '1'";

		$sql .= " ORDER BY name";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group WHERE `user_group_id` <> '1'");

		return $query->row['total'];
	}

	public function addPermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type][] = $route;

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}

	public function removePermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type] = array_diff($data[$type], array($route));

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}
}