<?php
class ModelCustomerCustomer extends Model {
	public function addCustomer($data) {
		$sql = "SELECT * FROM `oc_school` WHERE 1=1 ";
		$schoolss = $this->db->query($sql)->rows;
		$school_names['7777'] = 'Open';
		foreach($schoolss as $skey => $svalue){
			$school_names[$svalue['school_id']] = $svalue['name'];
		}
		$standards = array(
			'1' => 'I',
			'2' => 'II',
			'3' => 'III',
		-	'4' => 'IV',
			'5' => 'V',
			'6' => 'VI',
			'7' => 'VII',
			'8' => 'VIII',
			'9' => 'IX',
			'10' => 'X',
		);
		$sport_types = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);
		$participant_types = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);
		$genders = array(
			'1' => 'पुरुष',
			'2' => 'स्त्री',
		);
		$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
		$sportss = $this->db->query($sql)->rows;
		foreach($sportss as $skey => $svalue){
			$sports[$svalue['sport_id']] = $svalue['name'];
		}
		$firstname = '';
		$middlename = '';
		$lastname = '';
		$gender = '';
		$gender_id = '';
		$telephone = '';
		$email = '';
		$aadhar_card_number = '';
		$weight = '';
		$dob = '';
		$age = '';
		$sport_id = '';
		$sport = '';
		$partner_required = '';
		$partner_dob = '';
		$partner_age = '';
		$partner_firstname = '';
		$partner_middlename = '';
		$partner_lastname = '';
		if($data['participant_type'] == '2'){
			$firstname = $data['po_datas'][0]['firstname'];
			$middlename = $data['po_datas'][0]['middlename'];
			$lastname = $data['po_datas'][0]['lastname'];
			if(isset($genders[$data['gender']])){
				$gender = $genders[$data['gender']];
				$gender_id = $data['gender'];
			} else {
				$gender = '';
				$gender_id = 0;
			}
			$telephone = $data['telephone'];
			$email = $data['email'];
			$aadhar_card_number = '';//$data['aadhar_card_number'];
			$weight = $data['weight'];
			if(isset($data['po_datas'][0]['year']) && isset($data['po_datas'][0]['month']) && isset($data['po_datas'][0]['day'])){
				$dob = $data['po_datas'][0]['year'].'-'.$data['po_datas'][0]['month'].'-'.$data['po_datas'][0]['day'];
			} else {
				$dob = '0000-00-00';
			}
			$age = $data['po_datas'][0]['age'];
			$sport_id = $data['sport'];
			$sport = $sports[$data['sport']];
			$partner_required = '';
			$partner_dob = '';
			$partner_age = '';
			$partner_firstname = '';
			$partner_middlename = '';
			$partner_lastname = '';
		} else {
			$firstname = $data['firstname'];
			$middlename = $data['middlename'];
			$lastname = $data['lastname'];
			$gender = $genders[$data['gender']];
			$gender_id = $data['gender'];
			$telephone = $data['telephone'];
			$email = $data['email'];
			$aadhar_card_number = $data['aadhar_card_number'];
			$weight = '0';
			$dob = $data['year'].'-'.$data['month'].'-'.$data['day'];
			$age = $data['age'];
			$sport_id = $data['sport'];
			$sport = $sports[$data['sport']];
			$partner_required = $data['partner_required'];
			if($data['partner_required'] == '1'){
				$partner_dob = $data['partner_year'].'-'.$data['partner_month'].'-'.$data['partner_day'];
				$partner_age = $data['partner_age'];
				$partner_firstname = $data['partner_firstname'];
				$partner_middlename = $data['partner_middlename'];
				$partner_lastname = $data['partner_lastname'];
			}
		}
		// if ($data['dob'] == '' || $data['dob'] == '00-00-0000' || $data == '01-01-1970') {
		// 	$dob = '0000-00-00';
		// } else {
		// 	$dob = date('Y-m-d', strtotime($data['dob']));
		// }
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET 
						school_id = '" . $this->db->escape($data['school']) . "', 
						school = '" . $this->db->escape($school_names[$data['school']]) . "', 
						firstname = '" . $this->db->escape($firstname) . "', 
						middlename = '" . $this->db->escape($middlename) . "', 
						lastname = '" . $this->db->escape($lastname) . "',  
						fullname = '" . $this->db->escape($firstname.' '.$middlename.' '.$lastname) . "',  
						dob = '" . $this->db->escape($dob) . "',  
						age = '" . $this->db->escape($age) . "',  
						gender = '" . $this->db->escape($gender) . "',  
						gender_id = '" . $this->db->escape($gender_id) . "',
						sport_type = '" . $this->db->escape($sport_types[$data['sport_type']]) . "',  
						sport_type_id = '" . $this->db->escape($data['sport_type']) . "',  
						sport = '" . $this->db->escape($sport) . "',  
						sport_id = '" . $this->db->escape($sport_id) . "',
						group_name = '" . $this->db->escape($data['group_name']) . "',  
						group_id = '" . $this->db->escape($data['group_id']) . "',
						participant_type = '" . $this->db->escape($participant_types[$data['participant_type']]) . "',  
						participant_type_id = '" . $this->db->escape($data['participant_type']) . "',
						partner_dob = '" . $this->db->escape($partner_dob) . "', 
						partner_age = '" . $this->db->escape($partner_age) . "', 
						partner_firstname = '" . $this->db->escape($partner_firstname) . "', 
						partner_middlename = '" . $this->db->escape($partner_middlename) . "', 
						partner_lastname = '" . $this->db->escape($partner_lastname) . "', 
						partner_fullname = '" . $this->db->escape($partner_firstname.' '.$partner_middlename.' '.$partner_lastname) . "', 
						telephone = '".$this->db->escape($telephone)."',
						email = '".$this->db->escape($email)."',
						aadhar_card_number = '".$this->db->escape($aadhar_card_number)."',
						partner_required = '".$this->db->escape($partner_required)."',
						weight = '".$this->db->escape($weight)."',
						status = '".$this->db->escape($data['status'])."',
						customer_group_id = '1',
						approved = '1',
						safe  = '1',
						date_added = NOW()");
		//$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', newsletter = '" . (int)$data['newsletter'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', status = '" . (int)$data['status'] . "', approved = '" . (int)$data['approved'] . "', safe = '" . (int)$data['safe'] . "', date_added = NOW()");
		$customer_id = $this->db->getLastId();
		if($data['participant_type'] == '2'){
			if(isset($data['po_datas'])){
				foreach($data['po_datas'] as $pkey => $pvalue){
					if($pvalue['firstname'] != ''){
						if(isset($pvalue['year']) && isset($pvalue['month']) && isset($pvalue['day'])){
							$dob = $pvalue['year'].'-'.$pvalue['month'].'-'.$pvalue['day'];
						} else {
							$dob = '0000-00-00';
						}
						$sql = $this->db->query("INSERT INTO oc_customer_participant SET 
												customer_id = '".$customer_id."',
												firstname = '".$this->db->escape($pvalue['firstname'])."',
												middlename = '".$this->db->escape($pvalue['middlename'])."',
												lastname = '".$this->db->escape($pvalue['lastname'])."',
												fullname = '".$this->db->escape($pvalue['firstname'].' '.$pvalue['middlename'].' '.$pvalue['lastname'])."',
												aadhar_card_number = '".$this->db->escape($pvalue['aadhar_card_number'])."',
												dob = '".$this->db->escape($dob)."',
												age = '".$this->db->escape($pvalue['age'])."',
												inc_id = '".$this->db->escape($pkey)."',
												weight = '".$this->db->escape($pvalue['weight'])."' ");
					}
				}
			}
		}
		return $customer_id;
	}

	public function editCustomer($customer_id, $data) {
		$sql = "SELECT * FROM `oc_school` WHERE 1=1 ";
		$schoolss = $this->db->query($sql)->rows;
		$school_names['7777'] = 'Open';
		foreach($schoolss as $skey => $svalue){
			$school_names[$svalue['school_id']] = $svalue['name'];
		}
		$standards = array(
			'1' => 'I',
			'2' => 'II',
			'3' => 'III',
			'4' => 'IV',
			'5' => 'V',
			'6' => 'VI',
			'7' => 'VII',
			'8' => 'VIII',
			'9' => 'IX',
			'10' => 'X',
		);
		$sport_types = array(
			'1' => 'कला',
			'2' => 'क्रिडा',
		);
		$participant_types = array(
			'1' => 'वैयक्तिक स्पर्धा',
			'2' => 'सांघिक स्पर्धा',
		);
		$genders = array(
			'1' => 'पुरुष',
			'2' => 'स्त्री',
		);
		$sql = "SELECT * FROM `oc_sport` WHERE 1=1 ";
		$sportss = $this->db->query($sql)->rows;
		foreach($sportss as $skey => $svalue){
			$sports[$svalue['sport_id']] = $svalue['name'];
		}
		$firstname = '';
		$middlename = '';
		$lastname = '';
		$gender = '';
		$gender_id = '';
		$telephone = '';
		$email = '';
		$aadhar_card_number = '';
		$weight = '';
		$dob = '';
		$age = '';
		$sport_id = '';
		$sport = '';
		$partner_required = '';
		$partner_dob = '';
		$partner_age = '';
		$partner_firstname = '';
		$partner_middlename = '';
		$partner_lastname = '';
		if($data['participant_type'] == '2'){
			$firstname = $data['po_datas'][0]['firstname'];
			$middlename = $data['po_datas'][0]['middlename'];
			$lastname = $data['po_datas'][0]['lastname'];
			if(isset($genders[$data['gender']])){
				$gender = $genders[$data['gender']];
				$gender_id = $data['gender'];
			} else {
				$gender = '';
				$gender_id = 0;
			}
			$telephone = $data['telephone'];
			$email = $data['email'];
			$aadhar_card_number = $data['aadhar_card_number'];
			$weight = $data['weight'];
			if(isset($data['po_datas'][0]['year']) && isset($data['po_datas'][0]['month']) && isset($data['po_datas'][0]['day'])){
				$dob = $data['po_datas'][0]['year'].'-'.$data['po_datas'][0]['month'].'-'.$data['po_datas'][0]['day'];
			} else {
				$dob = '0000-00-00';
			}
			$age = $data['po_datas'][0]['age'];
			$sport_id = $data['sport'];
			$sport = $sports[$data['sport']];
			$partner_required = '';
			$partner_dob = '';
			$partner_age = '';
			$partner_firstname = '';
			$partner_middlename = '';
			$partner_lastname = '';
		} else {
			$firstname = $data['firstname'];
			$middlename = $data['middlename'];
			$lastname = $data['lastname'];
			$gender = $genders[$data['gender']];
			$gender_id = $data['gender'];
			$telephone = $data['telephone'];
			$email = $data['email'];
			$aadhar_card_number = $data['aadhar_card_number'];
			$weight = '0';
			$dob = $data['year'].'-'.$data['month'].'-'.$data['day'];
			$age = $data['age'];
			$sport_id = $data['sport'];
			$sport = $sports[$data['sport']];
			$partner_required = $data['partner_required'];
			if($data['partner_required'] == '1'){
				$partner_dob = $data['partner_year'].'-'.$data['partner_month'].'-'.$data['partner_day'];
				$partner_age = $data['partner_age'];
				$partner_firstname = $data['partner_firstname'];
				$partner_middlename = $data['partner_middlename'];
				$partner_lastname = $data['partner_lastname'];
			}
		}
		// if ($data['dob'] == '' || $data['dob'] == '00-00-0000' || $data == '01-01-1970') {
		// 	$dob = '0000-00-00';
		// } else {
		// 	$dob = date('Y-m-d', strtotime($data['dob']));
		// }
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET 
						school_id = '" . $this->db->escape($data['school']) . "', 
						school = '" . $this->db->escape($school_names[$data['school']]) . "', 
						firstname = '" . $this->db->escape($firstname) . "', 
						middlename = '" . $this->db->escape($middlename) . "', 
						lastname = '" . $this->db->escape($lastname) . "',  
						fullname = '" . $this->db->escape($firstname.' '.$middlename.' '.$lastname) . "',  
						dob = '" . $this->db->escape($dob) . "',  
						age = '" . $this->db->escape($age) . "',  
						gender = '" . $this->db->escape($gender) . "',  
						gender_id = '" . $this->db->escape($gender_id) . "',
						sport_type = '" . $this->db->escape($sport_types[$data['sport_type']]) . "',  
						sport_type_id = '" . $this->db->escape($data['sport_type']) . "',  
						sport = '" . $this->db->escape($sport) . "',  
						sport_id = '" . $this->db->escape($sport_id) . "',
						group_name = '" . $this->db->escape($data['group_name']) . "',
						group_id = '" . $this->db->escape($data['group_id']) . "',
						participant_type = '" . $this->db->escape($participant_types[$data['participant_type']]) . "',  
						participant_type_id = '" . $this->db->escape($data['participant_type']) . "',
						partner_dob = '" . $this->db->escape($partner_dob) . "', 
						partner_age = '" . $this->db->escape($partner_age) . "', 
						partner_firstname = '" . $this->db->escape($partner_firstname) . "', 
						partner_middlename = '" . $this->db->escape($partner_middlename) . "', 
						partner_lastname = '" . $this->db->escape($partner_lastname) . "', 
						partner_fullname = '" . $this->db->escape($partner_firstname.' '.$partner_middlename.' '.$partner_lastname) . "', 
						telephone = '".$this->db->escape($telephone)."',
						email = '".$this->db->escape($email)."',
						aadhar_card_number = '".$this->db->escape($aadhar_card_number)."',
						partner_required = '".$this->db->escape($partner_required)."',
						weight = '".$this->db->escape($weight)."',
						status = '".$this->db->escape($data['status'])."',
						customer_group_id = '1',
						approved = '1',
						safe  = '1',
						date_modified = NOW()
						WHERE customer_id = '".$customer_id."' ");
		
		$this->db->query("DELETE FROM `oc_customer_participant` WHERE `customer_id` = '".$customer_id."' ");
		if($data['participant_type'] == '2'){
			if(isset($data['po_datas'])){
				foreach($data['po_datas'] as $pkey => $pvalue){
					if($pvalue['firstname'] != ''){
						if(isset($pvalue['year']) && isset($pvalue['month']) && isset($pvalue['day'])){
							$dob = $pvalue['year'].'-'.$pvalue['month'].'-'.$pvalue['day'];
						} else {
							$dob = '0000-00-00';
						}
						$sql = $this->db->query("INSERT INTO oc_customer_participant SET 
												customer_id = '".$customer_id."',
												firstname = '".$this->db->escape($pvalue['firstname'])."',
												middlename = '".$this->db->escape($pvalue['middlename'])."',
												lastname = '".$this->db->escape($pvalue['lastname'])."',
												fullname = '".$this->db->escape($pvalue['firstname'].' '.$pvalue['middlename'].' '.$pvalue['lastname'])."',
												aadhar_card_number = '".$this->db->escape($pvalue['aadhar_card_number'])."',
												dob = '".$this->db->escape($dob)."',
												age = '".$this->db->escape($pvalue['age'])."',
												inc_id = '".$this->db->escape($pkey)."',
												weight = '".$this->db->escape($pvalue['weight'])."' ");
					}
				}
			}
		}
	}

	public function editToken($customer_id, $token) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function deleteCustomer($customer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_participant WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer c WHERE 1=1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			//$implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$implode[] = "c.fullname LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_school'])) {
			$implode[] = "c.school_id = '" . $this->db->escape($data['filter_school']) . "' ";
		}

		if (!empty($data['filter_standard'])) {
			$implode[] = "c.standard_id = '" . $this->db->escape($data['filter_standard']) . "' ";
		}

		if (!empty($data['filter_sport_type'])) {
			$implode[] = "c.sport_type_id = '" . $this->db->escape($data['filter_sport_type']) . "' ";
		}

		if (!empty($data['filter_sport'])) {
			$implode[] = "c.sport_id = '" . $this->db->escape($data['filter_sport']) . "' ";
		}

		if (!empty($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date_added']))) . "')";
		}

		if (!empty($data['filter_participant_type'])) {
			$implode[] = "c.participant_type_id = '" . $this->db->escape($data['filter_participant_type']) . "' ";
		}

		$implode[] = "c.active_status = '1' ";

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'c.name',
			'c.school',
			'c.standard',
			'c.sport_type',
			'c.sport',
			'c.status',
			'c.participant_type',
			'c.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if(isset($data['filter_export'])){
				$sql .= " AND batch_id <> '0' GROUP BY `batch_id` ORDER BY school, firstname, sport_type_id";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			if(isset($data['filter_export'])){
				$sql .= " AND batch_id <> '0' GROUP BY `batch_id` ORDER BY school, firstname, sport_type_id";
			} else {
				$sql .= " ORDER BY school, fullname";
			}
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalCustomers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer c";

		$implode = array();

		if (!empty($data['filter_name'])) {
			//$implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$implode[] = "c.fullname LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_school'])) {
			$implode[] = "c.school_id = '" . $this->db->escape($data['filter_school']) . "' ";
		}

		if (!empty($data['filter_standard'])) {
			$implode[] = "c.standard_id = '" . $this->db->escape($data['filter_standard']) . "' ";
		}

		if (!empty($data['filter_sport_type'])) {
			$implode[] = "c.sport_type_id = '" . $this->db->escape($data['filter_sport_type']) . "' ";
		}

		if (!empty($data['filter_sport'])) {
			$implode[] = "c.sport_id = '" . $this->db->escape($data['filter_sport']) . "' ";
		}

		if (!empty($data['filter_participant_type'])) {
			$implode[] = "c.participant_type_id = '" . $this->db->escape($data['filter_participant_type']) . "' ";
		}

		if (!empty($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape(date('Y-m-d', strtotime($data['filter_date_added']))) . "')";
		}

		$implode[] = "c.active_status = '1' ";

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalCustomersAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE status = '0' OR approved = '0'");

		return $query->row['total'];
	}

	public function getTotalAddressesByCustomerId($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTotalAddressesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE country_id = '" . (int)$country_id . "'");

		return $query->row['total'];
	}

	public function getTotalAddressesByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE zone_id = '" . (int)$zone_id . "'");

		return $query->row['total'];
	}

	public function getTotalCustomersByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		return $query->row['total'];
	}

	public function addHistory($customer_id, $comment) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_history SET customer_id = '" . (int)$customer_id . "', comment = '" . $this->db->escape(strip_tags($comment)) . "', date_added = NOW()");
	}

	public function getHistories($customer_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT comment, date_added FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalHistories($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);

		if ($customer_info) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$this->load->language('mail/customer');

			$this->load->model('setting/store');

			$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

			if ($store_info) {
				$store_name = $store_info['name'];
			} else {
				$store_name = $this->config->get('config_name');
			}

			$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id), $this->session->data['currency']));

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();
		}
	}

	public function deleteTransaction($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getTransactions($customer_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalTransactions($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTotalTransactionsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function addReward($customer_id, $description = '', $points = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);

		if ($customer_info) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");

			$this->load->language('mail/customer');

			$this->load->model('setting/store');

			$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

			if ($store_info) {
				$store_name = $store_info['name'];
			} else {
				$store_name = $this->config->get('config_name');
			}

			$message  = sprintf($this->language->get('text_reward_received'), $points) . "\n\n";
			$message .= sprintf($this->language->get('text_reward_total'), $this->getRewardTotal($customer_id));

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_reward_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();
		}
	}

	public function deleteReward($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");
	}

	public function getRewards($customer_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalRewards($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTotalCustomerRewardsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");

		return $query->row['total'];
	}

	public function getIps($customer_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}
		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
		
		return $query->rows;
	}

	public function getTotalIps($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTotalCustomersByIp($ip) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($ip) . "'");

		return $query->row['total'];
	}

	public function getTotalLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");
	}
}
