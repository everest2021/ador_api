ALTER TABLE `oc_order_info` ADD `report_status` INT(11) NOT NULL DEFAULT '0' AFTER `shift_user_id`;

ALTER TABLE `oc_item` ADD `item_onoff_status` INT(11) NOT NULL DEFAULT '0' AFTER `normal_point`;

ALTER TABLE `oc_order_info` ADD `wera_order_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `report_status`;

ALTER TABLE `oc_order_items_report` ADD `packaging_amt` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;


ALTER TABLE `oc_item` ADD `packaging_amt` INT(11) NOT NULL DEFAULT '0' AFTER `item_onoff_status`;


ALTER TABLE `oc_order_info_report`  ADD `shiftclose_status` INT(11) NOT NULL  AFTER `new_total_payment`,  ADD `shift_id` INT(11) NOT NULL  AFTER `shiftclose_status`,  ADD `shift_date` DATE NOT NULL  AFTER `shift_id`,  ADD `shift_time` VARCHAR(255) NOT NULL  AFTER `shift_date`,  ADD `payment_type` VARCHAR(255) NOT NULL  AFTER `shift_time`,  ADD `shift_username` VARCHAR(255) NOT NULL  AFTER `payment_type`,  ADD `shift_user_id` VARCHAR(255) NOT NULL  AFTER `shift_username`,  ADD `report_status` INT(11) NOT NULL  AFTER `shift_user_id`,  ADD `wera_order_id` VARCHAR(255) NOT NULL  AFTER `report_status`,  ADD `order_from` VARCHAR(255) NOT NULL  AFTER `wera_order_id`,  ADD `order_packaging` FLOAT(10,2) NOT NULL  AFTER `order_from`,  ADD `packaging_cgst` FLOAT(10,2) NOT NULL  AFTER `order_packaging`,  ADD `packaging_sgst` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst`,  ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst`,  ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst_percent`,  ADD `packaging` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst_percent`;

ALTER TABLE `oc_order_info` ADD `order_from` VARCHAR(255) NOT NULL DEFAULT '' AFTER `wera_order_id`;
ALTER TABLE `oc_order_info`  ADD `order_packaging` FLOAT(10,2) NOT NULL  AFTER `order_from`,  ADD `packaging_cgst` FLOAT(10,2) NOT NULL  AFTER `order_packaging`,  ADD `packaging_sgst` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst`,  ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst`,  ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL  AFTER `packaging_cgst_percent`,  ADD `packaging` FLOAT(10,2) NOT NULL  AFTER `packaging_sgst_percent`




ALTER TABLE `oc_order_items` ADD `packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;

ALTER TABLE `oc_order_info_report` ADD `report_status` INT(11) NOT NULL DEFAULT '0' AFTER `shift_user_id`, ADD `wera_order_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `report_status`, ADD `order_from` VARCHAR(255) NOT NULL DEFAULT '' AFTER `wera_order_id`, ADD `order_packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `order_from`, ADD `packaging_cgst` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `order_packaging`, ADD `packaging_sgst` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_cgst`, ADD `packaging_cgst_percent` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_sgst`, ADD `packaging_sgst_percent` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_cgst_percent`, ADD `packaging` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `packaging_sgst_percent`;
ALTER TABLE `oc_order_items_report` ADD `packaging_amt` FLOAT(10,2) NOT NULL DEFAULT '0' AFTER `kitchen_display`;
ALTER TABLE `oc_order_info` ADD `packaging_cgst` FLOAT(10,2) NOT NULL DEFAULT '0.00' AFTER `order_from` ;  
ALTER TABLE `oc_order_info` ADD `packaging_sgst` FLOAT(10,2) NOT NULL DEFAULT '0.00' AFTER `packaging_cgst` ;
