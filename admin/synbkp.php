<?php 
date_default_timezone_set('Asia/Kolkata');
//echo date('d-m-Y H:i');
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
//require_once('config.php');
$Controllercatalogsyncto = new Controllercatalogsyncto();
$Controllercatalogsyncto->putorder();

class Controllercatalogsyncto {
  	public $conn;
  	public $log;

  	public function __construct() {
  // 		$hostname = 'localhost';
  // 		$username = 'root';
  // 		$password= '';
  // 		$database = 'db_ador';
		// $this->conn = new mysqli($hostname, $username, $password, $database);
	
		// if ($this->conn->connect_error) {
		// 	fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($this->conn->connect_error, true)  . "\n");
		// 	die("Connection failed: " . $this->conn->connect_error);
		// }
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder(){
  		// $hostname = 'localhost';
  		// $username = 'root';
  		// $password= '';
  		// $database = 'db_ador';
		$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($conn->connect_error) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($conn->connect_error, true)  . "\n");
			die("Connection failed: " . $conn->connect_error);
		}
  		$final_data= array();
  		$final_data['order_items'] = $this->query("SELECT * FROM oc_order_items_report WHERE bk_status = '0' LIMIT 0, 100 ",$conn)->rows;

	  	$final_data['order_info'] = $this->query("SELECT * FROM oc_order_info_report WHERE bk_status = '0' LIMIT 0, 100",$conn)->rows;


		$url = SYNC_TO_SERVER;

		$data_json = json_encode($final_data);
		// echo'<pre>';
		// print_r($url);
		// exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		$datas = json_decode($response,true);
		// echo '<pre>';
		// print_r($response);
		// exit;
		
		if($datas['success_item'] == '1'){
  			$order_items_datas = $this->query("SELECT * FROM oc_order_items_report WHERE bk_status = '0'  LIMIT 0, 100 ",$conn)->rows;

  			foreach ($order_items_datas as $ikey => $ivalue) {
				$this->query("UPDATE oc_order_items_report SET bk_status = '1' WHERE bk_status = '0' AND id = '".$ivalue['id']."'",$conn);
  			}
  			echo " Data Send Successfully!!!!!!!!!!";
  		} else {
			echo " Data Not Send Please Check Error";
		}

  		if($datas['success_info'] == '1'){

  			$order_info_datas = $this->query("SELECT * FROM oc_order_info_report WHERE bk_status = '0'  LIMIT 0, 100 ",$conn)->rows;
  			foreach ($order_info_datas as $okey => $ovalue) {
				$this->query("UPDATE oc_order_info_report SET bk_status = '1' WHERE bk_status = '0' AND order_id = '".$ovalue['order_id']."'",$conn);
			}
			echo "Second Data Send Successfully";
			
			//$this->response->setOutput($this->load->view('catalog/databasebackup'));
		} else {
			echo " Data Not Send Please Check Error !!!!!!!!!!";
			
		}
		exit;
		//echo '<pre>'; print_r( $value) ;exit;
		curl_close($ch);
	  }
}
?>