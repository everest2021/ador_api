<?php
namespace Cart;
class User {
	private $user_id;
	private $username;
	private $userstore;
	private $cancelkot;
	private $printbill;
	private $user_printername;
	private $user_printertype;
	private $permission = array();

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->userstore = $user_query->row['store'];
				$this->cancelkot = $user_query->row['cancelkot'];
				$this->printbill = $user_query->row['bill_print'];
				$this->user_group_id = $user_query->row['user_group_id'];
				$this->user_printername = $user_query->row['printer_name'];
				$this->user_printertype = $user_query->row['printer_type'];
				$this->user_cancel_bill = $user_query->row['cancel_bill'];
				$this->user_edit_bill = $user_query->row['edit_bill'];
				$this->user_duplicate_bill = $user_query->row['duplicate_bill'];

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = json_decode($user_group_query->row['permission'], true);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($username, $password, $override = false) {
		if ($override) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "'");
		}else{
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
		}
		//$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = 1");
		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];

			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];
			$this->userstore = $user_query->row['store'];
			$this->cancelkot = $user_query->row['cancelkot'];
			$this->printbill = $user_query->row['bill_print'];
			$this->user_group_id = $user_query->row['user_group_id'];
			$this->user_printername = $user_query->row['printer_name'];
			$this->user_printertype = $user_query->row['printer_type'];
			$this->user_cancel_bill = $user_query->row['cancel_bill'];
			$this->user_edit_bill = $user_query->row['edit_bill'];
			$this->user_duplicate_bill = $user_query->row['duplicate_bill'];

			$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			$permissions = json_decode($user_group_query->row['permission'], true);

			if (is_array($permissions)) {
				foreach ($permissions as $key => $value) {
					$this->permission[$key] = $value;
				}
			}

			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		
		unset($this->session->data['user_id']);
		unset($this->session->data['token']);
		$this->user_id = '';
		$this->username = '';
	}

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getUserName() {
		return $this->username;
	}

	public function getUserStore() {
		return $this->userstore;
	}

	public function getCancelKot() {
		return $this->cancelkot;
	}

	public function getPrintBill() {
		return $this->printbill;
	}


	public function getGroupId() {
		return $this->user_group_id;
	}

	public function getPrinterName() {
		return $this->user_printername;
	}

	public function getPrinterType() {
		return $this->user_printertype;
	}
	public function getCanceBill() {
		return $this->user_cancel_bill;
	}

	public function getEditBill() {
		return $this->user_edit_bill;
	}
	public function getDuplicateBill() {
		return $this->user_duplicate_bill;
	}
}