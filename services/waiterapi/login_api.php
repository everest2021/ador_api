<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
//$file = '/var/www/html/ador/services/waiterapi/service.txt';
//$file = 'C:\xampp\htdocs\ador\services\waiterapi\service.txt';

$file = FILE_PATH;
$handle = fopen($file, 'a+'); 
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemapi = new Itemapi();

fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r('innn', true)  . "\n");
$value = $Itemapi->getitem($datas, $handle);
fclose($handle); 
exit(json_encode($value));


class Itemapi {
	public $conn;
	public function __construct() {
		// Create connection
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem($data = array(),$handle){ 
		if(!isset($data['username'])){
			//$data['username'] = 'college';
			$data['username'] = '';
		}
		if(!isset($data['password'])){
			//$data['password'] = 'admin';
			$data['password'] = '';

		} 
		$result = array();
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($data, true)  . "\n");
		if($data['username'] != '') {
			
			$user_query = $this->query("SELECT * FROM oc_user WHERE username = '" . $data['username'] . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->escape(htmlspecialchars($data['password'], ENT_QUOTES),$this->conn) . "'))))) OR password = '" . $this->escape(md5($data['password']),$this->conn) . "') ",$this->conn);

			$type = '';
			if($user_query->row['user_group_id'] == 13){
				$type = 'Captain';
			} else {
				$type = 'Waiter';
			}

			
			$employee = $this->query("SELECT waiter_id, code, name FROM oc_waiter WHERE waiter_id = '" . $user_query->row['employee_id'] . "' ",$this->conn);

			if ($employee->num_rows > 0) {
				$result['waiter_code'] = $employee->row['code'];
				$result['waiter_id'] = $employee->row['waiter_id'];
				$result['waiter_name'] = $employee->row['name'];
				$result['type'] = $type;
			} else{
				$result['waiter_code'] = 0;
				$result['waiter_id'] = 0;
				$result['waiter_name'] = '';
				$result['type'] = '';
			}
			
			if ($user_query->num_rows > 0) {
				$result['username'] = $user_query->row['username'];
				$result['user_id'] = $user_query->row['user_id'];
				$result['success'] = 1;
			} else{
				$result['success'] = 0;
			}
		}
		fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($result, true)  . "\n");

		
		return $result;
	}

	public function token($length = 32) {
		// Create random token
		$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$max = strlen($string) - 1;
		
		$token = '';
		
		for ($i = 0; $i < $length; $i++) {
			$token .= $string[mt_rand(0, $max)];
		}	
		
		return $token;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}

?>