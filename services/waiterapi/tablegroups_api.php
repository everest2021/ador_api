<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
//$file = '/var/www/html/ador/services/waiterapi/service.txt';
//$file = 'C:\xampp\htdocs\ador\services\waiterapi\service.txt';
$file = FILE_PATH;
$handle = fopen($file, 'a+');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$tablegroups_api = new tablegroups_api();
$value = $tablegroups_api->gettablegroups($datas, $handle);
fclose($handle);

exit(json_encode($value));

class tablegroups_api {
	public $conn;
	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function gettablegroups($data = array(),$handle){
		// echo'<pre>';
		// print_r($data);
		// exit;
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data, true)  . "\n");

		$result = array();
		$location_datas= $this->query("SELECT * FROM  `oc_location` ",$this->conn)->rows;
		$INCLUSIVES=$this->query("SELECT `value` FROM settings_ador WHERE `key` = 'INCLUSIVE'",$this->conn)->row['value'];
		if($INCLUSIVES == 1){
			$INCLUSIVE ='Yes';
		}else{
			$INCLUSIVE ='No';
		}
		foreach ($location_datas as $key => $location_data) {
			$result['table_query'][]=array(
					'location_id' => $location_data['location_id'],
                    'location' => $location_data['location'],
                    'rate_id' => $location_data['rate_id'],
                    'table_to' => $location_data['table_to'],
                    'table_from' => $location_data['table_from'],
                    'bill_copy' => $location_data['bill_copy'],
                    'kot_copy' => $location_data['kot_copy'],
                    'direct_bill' => $location_data['direct_bill'],
                    'parcel_detail' => $location_data['parcel_detail'],
                    'dboy_detail' => $location_data['dboy_detail'],
                    'a_to_z' => $location_data['a_to_z'],
                    'is_app' => $location_data['is_app'],
                    'kot_different' => $location_data['kot_different'],
                    'bill_printer_name' => $location_data['bill_printer_name'],
                    'bill_printer_type' => $location_data['bill_printer_type'],
                    'bill_different' => $location_data['bill_different'],
                    'is_advance' => $location_data['is_advance'],
                    'store_name' => $location_data['store_name'],
                    'service_charge' => $location_data['service_charge'],
                    'INCLUSIVE' => $INCLUSIVE
                  );  
		}

		if($data['table_idd'] != '' && $data['table_idd'] != 'null' && $data['table_idd'] != null && $data['table_idd'] !=  NULL){
			// echo '<pre>';
			// print_r($this->session->data);
			// exit;
			$table_id = $data['table_idd'];
			$this->query("DELETE FROM `oc_running_table` WHERE `table_id` = '".$table_id."' ",$this->conn);
		}
		/*echo '<pre>';
		print_r($result);
		exit;*/
		return $result;
	}
	
}

?>