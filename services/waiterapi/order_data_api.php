<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$orderdatas_api = new orderdatas_api();
$value = $orderdatas_api->getorderdatas($datas);
exit(json_encode($value));

class orderdatas_api {
	public $conn;
	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getorderdatas($data = array()){
		/*echo '<pre>';
		print_r($data);
		exit;*/
		$result = array();
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->query($last_open_date_sql,$this->conn);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}
		if(isset($data['status']) && isset($data['table_id']) && isset($data['location_id']) && $data['table_id'] !='' && $data['status']== 0 && $data['location_id'] !=''){
			$order_info_datass = $this->query("SELECT * FROM oc_order_info WHERE `bill_date` = '".$last_open_date."' AND location_id='".$data['location_id']."' AND `table_id` = '".$data['table_id']."' AND `pay_method` = '0' AND `cancel_status` = '0' ORDER BY `order_id` DESC LIMIT 1",$this->conn); 
			// echo '<pre>';
			// print_r($order_info_datass );
			// exit;
			if($order_info_datass->num_rows > 0){
				$order_info_datas =$order_info_datass->rows;
				$order_item_datas = $this->query("SELECT * FROM oc_order_items WHERE `order_id` = '".$order_info_datas[0]['order_id']."' AND cancelstatus = '0' ORDER BY `id` ASC ",$this->conn)->rows;
				$result['order_info_data'] = $order_item_datas;
			}
		$result['rate_change'] = $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'RATE_CHANGE'",$this->conn)->row['value']; 

		}else{
			$result['order_info_data'] = $this->query("SELECT * FROM  `oc_order_info` Where `location_id`='".$data['location_id']."' AND `day_close_status`='0' AND `cancel_status`='0' AND `bill_date`='".$last_open_date."' ",$this->conn)->rows;
		$result['rate_change'] = $this->query("SELECT `value` FROM settings_ador WHERE `key` = 'RATE_CHANGE'",$this->conn)->row['value']; 
			
		}
		
		//INSERT INTO `settings_ador` (`id`, `key`, `value`) VALUES (NULL, 'RATE_CHANGE', '1');
		return $result;
	}
	
}

?>