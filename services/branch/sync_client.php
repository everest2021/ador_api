<?php
session_start();
include '../../admin/config.php';
$backup = new Backup();
$backup->backupDatabaseTables();
$backup->importDatabaseTables();
// if(isset($_SESSION['default']['token'])){
//     $token = $_SESSION['default']['token'];
// } elseif (isset($_SESSION['token'])) {
//      $token = $_SESSION['token'];
// }
exit;
header('Location: '.HTTP_SERVER.'index.php?route=catalog/order&token='.$token);
class Backup
{
    public $conn;
    public $scpconnection;
    
    function __construct()
    {
        $dbname = DB_DATABASE;
        $host = DB_HOSTNAME;
        $user = DB_USERNAME;
        $pass = DB_PASSWORD;

        $this->conn = new mysqli($host,$user,$pass,$dbname);

        if($this->conn->connect_error){
            die("connection failed :".$this->conn->connect_error);
        }

        $this->scpconnection = ssh2_connect('166.62.28.102', 22);
        ssh2_auth_password($this->scpconnection, 'teybm7upyg9z', 'Nirav@111');
    }

    function backupDatabaseTables(){
        $this->conn->query("SET NAMES 'utf8'");
        $queryTables    = $this->conn->query('SHOW TABLES'); 
        while($row = $queryTables->fetch_row()) { 
            if($row[0] == 'oc_order_info' || $row[0] == 'oc_order_items' || $row[0] == 'oc_customerinfo'){
                $target_tables[] = $row[0]; 
            }
            $target_tables_all[] = $row[0]; 
        }

        foreach($target_tables as $table) {
            $result         =   $this->conn->query("SELECT * FROM $table WHERE bk_status = '0'");  
            $fields_amount  =   $result->field_count;  
            $rows_num       =   $this->conn->affected_rows;    
            $res            =   $this->conn->query('SHOW CREATE TABLE '.$table); 
            $TableMLine     =   $res->fetch_row();
            $content        = (!isset($content) ?  '' : $content);

            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
                while($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )  {
                            $content .= "\nINSERT INTO ".$table." VALUES";
                    }
                    $content .= "\n(";
                    for($j=0; $j<$fields_amount; $j++)  { 
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
                        if (isset($row[$j])){
                            $content .= '"'.$row[$j].'"' ; 
                        } else {   
                            $content .= '""';
                        }     
                        if ($j<($fields_amount-1)) {
                            $content.= ',';
                        }      
                    }
                    $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {   
                        $content .= ";";
                    } else {
                        $content .= ",";
                    } 
                    $st_counter=$st_counter+1;
                }
            } $content .="\n\n\n";
        }
        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
        $backup_name = '../server/'.'db_branch_bk.sql';
        file_put_contents($backup_name, $content);

        // $filename = "db_branch_bk.sql";
        // $file_path = '../server/'. $filename;
        // $html = file_get_contents($file_path);

        /*$zipFile = '../server/'.'db_branch_bk.zip';
        $zipArchive = new ZipArchive();

        if (!$zipArchive->open($zipFile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE))
            die("Failed to create archive\n");

        $zipArchive->addGlob('../server/'.'db_branch_bk.sql');
        if (!$zipArchive->status == ZIPARCHIVE::ER_OK)
            echo "Failed to write local files to zip\n";

        $zipArchive->close();*/

        ssh2_scp_send($this->scpconnection, '../server/db_branch_bk.sql', '/home/teybm7upyg9z/public_html/hotel/services/server/db_branch_bk.sql', 0644);


        // Header('Content-type: application/octet-stream');
        // header('Content-Disposition: attachment; filename='.$filename);
        // echo $html;
        //echo 'Done';exit;

        $this->conn->query("UPDATE oc_order_info SET bk_status = '1'"); 
        $this->conn->query("UPDATE oc_order_items SET bk_status = '1'");
        $this->conn->query("UPDATE oc_customerinfo SET bk_status = '1'");  
    }

    function importDatabaseTables(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'funshalla.in/hotel/services/branch/db_server_bk.sql');
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            $file = '1';
        }
        else
        {
            $file = '2';
        }

        if($file == '1'){
            ssh2_scp_recv($this->scpconnection, '/home/teybm7upyg9z/public_html/hotel/services/branch/db_server_bk.sql', 'db_server_bk.sql');
            $queryTables = $this->conn->query('SHOW TABLES');
            while($row = $queryTables->fetch_row()) { 
                if(
                    $row[0] == 'oc_category' || 
                    $row[0] == 'oc_department' ||
                    $row[0] == 'oc_subcategory' || 
                    $row[0] == 'oc_item' ||  
                    $row[0] == 'oc_tax' ||
                    $row[0] == 'oc_waiter' ||
            $row[0] == 'oc_user'
                    ){
                    $target_tables[] = $row[0];
                }
            }
            foreach($target_tables as $table) {
                $this->conn->query("TRUNCATE TABLE $table");
            }

            $this->conn->set_charset("utf8");
            $this->conn->multi_query(file_get_contents('db_server_bk.sql'));
        }
    }

}

?>