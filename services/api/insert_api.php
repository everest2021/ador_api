<?php 
date_default_timezone_set('Asia/Kolkata');
//echo date('d-m-Y H:i');
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$hourlysms = new Hourlysms();
$hourlysms->putorder();

class Hourlysms {
  	public $conn;
  	public $log;

  	public function __construct() {
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	
		if ($this->conn->connect_error) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($this->conn->connect_error, true)  . "\n");
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function get_settings($key,$conn) {
		$setting_status = $this->query("SELECT * FROM settings_ador WHERE `key` = '".$key."'",$conn);
		if($setting_status->num_rows > 0){
			return $setting_status->row['value'];
		} else {
			return '';
		}
	}

  	public function putorder(){
  		$sub_total_sale = 0;
 		$sub_total_cash = 0;;
 		$sub_total_card = 0;
 		$sub_total_online = 0;
 		$sub_total_onac = 0;

 		$sub_total_c_kot_amt = 0;
 		$sub_total_c_kot_count = 0;

 		$sub_total_c_bill_amt = 0;
 		$sub_total_c_bill_count = 0;
 		$sub_total_fdiscount = 0;
	 	$sub_total_ldiscount = 0;
  		
		$child_dblist = $this->query("SELECT device FROM oc_device WHERE 1=1",$this->conn)->rows;
		
		foreach ($child_dblist as $dkey => $dvalue) {
			
			$db_pce = 'db_'.$dvalue['device'];
			$datas['db_name'] = $db_pce;

	  		$conn_m = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $datas['db_name']);
			if ($conn_m->connect_error) {
				die("Connection failed: " . $this->conn_m->connect_error);
			}

	  		$finaldata = array();
	  		$date = date("d/m/Y");
			$time =  date("h:i:sa");
	  		
	  		$total_sale = $this->query("SELECT SUM(grand_total) as t_sale, SUM(pay_cash) as t_cash, SUM(pay_card) as t_card, SUM(pay_online) as t_online, SUM(onac) as t_onac FROM oc_order_info WHERE cancel_status = '0'",$conn_m)->row;
	  		$cancel_kot = $this->query("SELECT count(*) as kot_count, SUM(amt) as c_kot_amt FROM oc_order_items WHERE cancelstatus ='1' ",$conn_m)->row;
	  		$cancel_bill = $this->query("SELECT count(*) as bill_count, SUM(grand_total) as c_bill_amt FROM oc_order_info WHERE cancel_status = '1' ",$conn_m)->row;
	  		$discount = $this->query("SELECT SUM(ftotalvalue) as total_fdiscount, SUM(ltotalvalue) as total_ldiscount FROM oc_order_info WHERE cancel_status = '0' ",$conn_m)->row;
	  		
	 		$sub_total_sale = $sub_total_sale + $total_sale['t_sale'];
	 		$sub_total_cash = $sub_total_cash + $total_sale['t_cash'];
	 		$sub_total_card = $sub_total_card + $total_sale['t_card'];
	 		$sub_total_online = $sub_total_online + $total_sale['t_online'];
	 		$sub_total_onac = $sub_total_onac + $total_sale['t_onac'];

	 		$sub_total_c_kot_amt = $sub_total_c_kot_amt + $cancel_kot['c_kot_amt'];
	 		$sub_total_c_kot_count = $sub_total_c_kot_count + $cancel_kot['kot_count'];

	 		$sub_total_c_bill_amt = $sub_total_c_bill_amt + $cancel_bill['c_bill_amt'];
	 		$sub_total_c_bill_count = $sub_total_c_bill_count + $cancel_bill['bill_count'];

	 		$sub_total_fdiscount = $sub_total_fdiscount + $discount['total_fdiscount'];
	 		$sub_total_ldiscount = $sub_total_ldiscount + $discount['total_ldiscount'];
	  		
	  		$conn_m->close();
	  	}

	  	$setting_value_link_1 = $this->get_settings('SMS_LINK_1',$this->conn);
	  	$setting_value_link_2 = $this->get_settings('SMS_LINK_2',$this->conn);
	  	$setting_value_link_3 = $this->get_settings('SMS_LINK_3',$this->conn);
	  	if($setting_value_link_1 == ''){
	  		$send_link = $setting_value_link_2;
	  	} elseif ($setting_value_link_2 == '') {
	  		$send_link = $setting_value_link_3;
	  	}elseif ($setting_value_link_3 == '') {
	  		$send_link = $setting_value_link_1;
	  	} else{
	  		$send_link = $setting_value_link_1;
	  	}
		$link_1 = html_entity_decode($send_link, ENT_QUOTES, 'UTF-8');
		$setting_value_number = $this->get_settings('CONTACT_NUMBER',$this->conn);
		//$setting_value_number = '9730941488';
		//echo $setting_value_number;exit;
		if ($send_link != '' && $setting_value_number != '') {
			$text = "Total%20Sale%20Amt%20:".$sub_total_sale.",%0aTotal%20Cash%20Amt%20:".$sub_total_cash.",%0aTotal%20Card%20Amt%20:".$sub_total_card.",%0aTotal%20Online%20Amt%20:".$sub_total_online.",%0aTotal%20OnAcc%20Amt%20:".$sub_total_onac.",%0aTotal%20Cancel%20KOT%20Count%20/%20Amt%20:".$sub_total_c_kot_count.'/'.$sub_total_c_kot_amt.",%0aTotal%20Cancel%20BILL%20Count%20/%20Amt%20:".$sub_total_c_bill_count.'/'.$sub_total_c_bill_amt.",%0aTotal%20Food%20Discount%20Amt%20:".$sub_total_fdiscount.",%0aTotal%20Liquor%20Discount%20Amt%20:".$sub_total_ldiscount.",%0aDate%20:".$date.",%0aTime%20:".$time;
			// $str = "Total Sale Amt :2600,Total Cash Amt :2600,Total Card Amt :0,Total Online Amt :0,Total OnAcc Amt :0,Total Cancel KOT Count / Amt :0/0,Total Cancel BILL Count / Amt :0/0,Total Food Discount Amt :0,Total Liquor Discount Amt :0,Time :28-03-2019";
			// echo strlen($str);exit;
			$link = $link_1."&phone=".$setting_value_number."&text=".$text;
			//$link = SMS_LINK_1."&phone=".CONTACT_NUMBER."&text=".$text;
			//echo $link;exit;
			$ret = file($link);
			echo'SMS Send Sucessfully.';
		} else {
			echo 'SMS LINK OR Owner Number Not Set Please Check Setting Page';
		}

		// echo "<pre>";
		// print_r($text);
		// exit;

		//return $finaldata;
  	}	
  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
	
}
?>