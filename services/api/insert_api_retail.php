<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');
$file = '/var/www/html/pce/services/api/service.txt';
$handle = fopen($file, 'a+'); 
// $message = 'tdcsfas';
// fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");
//fclose($handle); 
//echo 'aaaa';exit;
$data = file_get_contents('php://input');
$datas = json_decode($data,true);
$Itemdataapi = new Itemdataapi($datas, $handle);
//echo "<pre>";print_r($datas);exit;
$value = $Itemdataapi->putorder($datas, $handle);
fclose($handle); 
exit(json_encode($value));
class Itemdataapi {
  	public $conn;
  	public $log;

  	public function __construct($datas, $handle) {
		// Create connection
  		//fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r('in construct', true)  . "\n");
  		//fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($datas, true)  . "\n");
  		//fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($datas['db_name'], true)  . "\n");

		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, $datas['db_name']);
		// Check connection
		if ($this->conn->connect_error) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($this->conn->connect_error, true)  . "\n");
			die("Connection failed: " . $this->conn->connect_error);
		}
  	}

  	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

  	public function putorder($data = array(), $handle){
  		date_default_timezone_set("Asia/Kolkata");
  		fwrite($handle, date('Y-m-d H:i:s') . '-' . print_r('in putorder', true)  . "\n");
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($data, true)  . "\n");
	    $result = array();	
	    
	    date_default_timezone_set("Asia/Kolkata");
		$last_open_date_sql = "SELECT `bill_date` FROM `oc_order_info` WHERE `day_close_status` = '0' ORDER BY `date` DESC LIMIT 1";
		$last_open_dates = $this->query($last_open_date_sql, $this->conn);
		if($last_open_dates->num_rows > 0){
			$last_open_date = $last_open_dates->row['bill_date'];
		} else {
			$last_open_date = date('Y-m-d');
		}

	    if(isset($data['info_datas'][0]) && !empty($data['info_datas'])){
			foreach($data['info_datas'] as $bkey => $bvalue){
				//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($bvalue, true)  . "\n");
				$bill_date = $bvalue['bill_date']; 
				$is_exist_sql = "SELECT `order_id` FROM `oc_order_info` WHERE `order_id` = '".$bvalue['order_id']."' ";
				$is_exist = $this->query($is_exist_sql, $this->conn);
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_order_info` SET 
							`order_id` = '".$bvalue['order_id']."',
							`app_order_id` = '".$bvalue['app_order_id']."',
							`kot_no` = '".$bvalue['kot_no']."', 
							`order_no` = '".$bvalue['order_no']."', 
							`merge_number` = '".$bvalue['merge_number']."',
							`location` = '".$bvalue['location']."', 
							`location_id` = '".$bvalue['location_id']."', 
							`t_name` = '".$bvalue['t_name']."',
							`parent_table_id` = '".$bvalue['parent_table_id']."',
							`table_id` = '".$bvalue['table_id']."', 
							`waiter_code` = '".$bvalue['waiter_code']."',
							`waiter` = '".$bvalue['waiter']."', 
							`waiter_id` = '".$bvalue['waiter_id']."', 
							`captain_code` = '".$bvalue['captain_code']."',
							`captain` = '".$bvalue['captain']."', 
							`captain_id` = '".$bvalue['captain_id']."',
							`person` = '".$bvalue['person']."', 
							`ftotal` = '".$bvalue['ftotal']."', 
							`ftotal_discount` = '".$bvalue['ftotal_discount']."', 
							`gst` = '".$bvalue['gst']."', 
							`ltotal` = '".$bvalue['ltotal']."', 
							`ltotal_discount` = '".$bvalue['ltotal_discount']."', 
							`vat` = '".$bvalue['vat']."', 
							`cess` = '".$bvalue['cess']."', 
							`staxfood` = '".$bvalue['staxfood']."', 
							`staxliq` = '".$bvalue['staxliq']."', 
							`stax` = '".$bvalue['stax']."', 
							`ftotalvalue` = '".$bvalue['ftotalvalue']."', 
							`fdiscountper` = '".$bvalue['fdiscountper']."', 
							`discount` = '".$bvalue['discount']."',
							`ldiscountper` = '".$bvalue['ldiscountper']."',
							`ldiscount` = '".$bvalue['ldiscount']."', 
							`ltotalvalue` = '".$bvalue['ltotalvalue']."', 
							`date` = '".$bvalue['date']."', 
							`time` = '".$bvalue['time']."', 
							`bill_status` = '".$bvalue['bill_status']."',
							`payment_status` = '".$bvalue['payment_status']."', 
							`cust_id` = '".$bvalue['cust_id']."', 
							`cust_name` = '".$bvalue['cust_name']."', 
							`cust_contact` = '".$bvalue['cust_contact']."', 
							`cust_address` = '".$bvalue['cust_address']."', 
							`cust_address_1` = '".$bvalue['cust_address_1']."', 
							`cust_email` = '".$bvalue['cust_email']."', 
							`gst_no` = '".$bvalue['gst_no']."', 
							`parcel_status` = '".$bvalue['parcel_status']."', 
							`day_close_status` = '".$bvalue['day_close_status']."', 
							`bill_date` = '".$bvalue['bill_date']."', 
							`rate_id` = '".$bvalue['rate_id']."', 
							`delivery_charges` = '".$bvalue['delivery_charges']."', 
							`grand_total` = '".$bvalue['grand_total']."', 
							`advance_billno` = '".$bvalue['advance_billno']."', 
							`advance_amount` = '".$bvalue['advance_amount']."', 
							`roundtotal` = '".$bvalue['roundtotal']."', 
							`total_items` = '".$bvalue['total_items']."', 
							`item_quantity` = '".$bvalue['item_quantity']."', 
							`date_added` = '".$bvalue['date_added']."', 
							`time_added` = '".$bvalue['time_added']."', 
							`out_time` = '".$bvalue['out_time']."', 
							`nc_kot_status` = '".$bvalue['nc_kot_status']."', 
							`year_close_status`  = '".$bvalue['year_close_status']."',  
							`cancel_status`  = '".$bvalue['cancel_status']."', 
							`pay_cash`  = '".$bvalue['pay_cash']."', 
							`pay_card`  = '".$bvalue['pay_card']."', 
							`pay_online`  = '".$bvalue['pay_online']."', 
							`card_no` = '".$bvalue['card_no']."', 
							`creditcustname`  = '".$bvalue['creditcustname']."', 
							`mealpass`  = '".$bvalue['mealpass']."', 
							`pass`  = '".$bvalue['pass']."', 
							`roomservice`  = '".$bvalue['room_service']."', 
							`roomno`  = '".$bvalue['room_no']."', 
							`msrcard` = '".$bvalue['msrcard']."',  
							`msrcardno` = '".$bvalue['msrcardno']."',
							`msrcardname` = '".$bvalue['msrcardname']."',
							`onac` = '".$bvalue['onac']."',
							`onaccust` = '".$bvalue['onaccust']."',
							`onaccontact` = '".$bvalue['onaccontact']."',
							`onacname` = '".$bvalue['onacname']."',
							`total_payment` = '".$bvalue['total_payment']."',
							`pay_method` = '".$bvalue['pay_method']."',
							`bk_status` = '".$bvalue['bk_status']."',
							`login_name` = '".$bvalue['login_name']."',
							`login_id` = '".$bvalue['login_id']."',
							`food_cancel` = '".$bvalue['food_cancel']."',
							`liq_cancel` = '".$bvalue['liq_cancel']."',
							`bill_modify` = '".$bvalue['bill_modify']."',
							`modify_remark` = '".$bvalue['modify_remark']."',
							`duplicate` = '".$bvalue['duplicate']."',
							`duplicate_time` = '".$bvalue['duplicate_time']."',
							`sales_id` = '".$bvalue['sales_id']."',
							`sales_name` = '".$bvalue['sales_name']."',
							`sales_contact` = '".$bvalue['sales_contact']."',
							`sales_address` = '".$bvalue['sales_address']."',
							`sales_address_1` = '".$bvalue['sales_address_1']."',
							`gst_type` = '".$bvalue['gst_type']."',
							`tendering_amount` = '".$bvalue['tendering_amount']."',
							`change_amount` = '".$bvalue['change_amount']."',
							`credit_number` = '".$bvalue['credit_number']."',
							`printstatus` = '".$bvalue['printstatus']."' ";
							
							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($sql, true)  . "\n");
							$this->query($sql,$this->conn);

							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('before order info error', true)  . "\n");
							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");	
				} else {
					$sql = "UPDATE `oc_order_info` SET 
							`app_order_id` = '".$bvalue['app_order_id']."',
							`kot_no` = '".$bvalue['kot_no']."', 
							`order_no` = '".$bvalue['order_no']."', 
							`merge_number` = '".$bvalue['merge_number']."',
							`location` = '".$bvalue['location']."', 
							`location_id` = '".$bvalue['location_id']."', 
							`t_name` = '".$bvalue['t_name']."',
							`parent_table_id` = '".$bvalue['parent_table_id']."',
							`table_id` = '".$bvalue['table_id']."', 
							`waiter_code` = '".$bvalue['waiter_code']."',
							`waiter` = '".$bvalue['waiter']."', 
							`waiter_id` = '".$bvalue['waiter_id']."', 
							`captain_code` = '".$bvalue['captain_code']."',
							`captain` = '".$bvalue['captain']."', 
							`captain_id` = '".$bvalue['captain_id']."',
							`person` = '".$bvalue['person']."', 
							`ftotal` = '".$bvalue['ftotal']."', 
							`ftotal_discount` = '".$bvalue['ftotal_discount']."', 
							`gst` = '".$bvalue['gst']."', 
							`ltotal` = '".$bvalue['ltotal']."', 
							`ltotal_discount` = '".$bvalue['ltotal_discount']."', 
							`vat` = '".$bvalue['vat']."', 
							`cess` = '".$bvalue['cess']."', 
							`staxfood` = '".$bvalue['staxfood']."', 
							`staxliq` = '".$bvalue['staxliq']."', 
							`stax` = '".$bvalue['stax']."', 
							`ftotalvalue` = '".$bvalue['ftotalvalue']."', 
							`fdiscountper` = '".$bvalue['fdiscountper']."', 
							`discount` = '".$bvalue['discount']."',
							`ldiscountper` = '".$bvalue['ldiscountper']."',
							`ldiscount` = '".$bvalue['ldiscount']."', 
							`ltotalvalue` = '".$bvalue['ltotalvalue']."', 
							`date` = '".$bvalue['date']."', 
							`time` = '".$bvalue['time']."', 
							`bill_status` = '".$bvalue['bill_status']."',
							`payment_status` = '".$bvalue['payment_status']."', 
							`cust_id` = '".$bvalue['cust_id']."', 
							`cust_name` = '".$bvalue['cust_name']."', 
							`cust_contact` = '".$bvalue['cust_contact']."', 
							`cust_address` = '".$bvalue['cust_address']."', 
							`cust_address_1` = '".$bvalue['cust_address_1']."', 
							`cust_email` = '".$bvalue['cust_email']."', 
							`gst_no` = '".$bvalue['gst_no']."', 
							`parcel_status` = '".$bvalue['parcel_status']."', 
							`day_close_status` = '".$bvalue['day_close_status']."', 
							`bill_date` = '".$bvalue['bill_date']."', 
							`rate_id` = '".$bvalue['rate_id']."', 
							`delivery_charges` = '".$bvalue['delivery_charges']."', 
							`grand_total` = '".$bvalue['grand_total']."', 
							`advance_billno` = '".$bvalue['advance_billno']."', 
							`advance_amount` = '".$bvalue['advance_amount']."', 
							`roundtotal` = '".$bvalue['roundtotal']."', 
							`total_items` = '".$bvalue['total_items']."', 
							`item_quantity` = '".$bvalue['item_quantity']."', 
							`date_added` = '".$bvalue['date_added']."', 
							`time_added` = '".$bvalue['time_added']."', 
							`out_time` = '".$bvalue['out_time']."', 
							`nc_kot_status` = '".$bvalue['nc_kot_status']."', 
							`year_close_status`  = '".$bvalue['year_close_status']."',  
							`cancel_status`  = '".$bvalue['cancel_status']."', 
							`pay_cash`  = '".$bvalue['pay_cash']."', 
							`pay_card`  = '".$bvalue['pay_card']."', 
							`pay_online`  = '".$bvalue['pay_online']."', 
							`card_no` = '".$bvalue['card_no']."', 
							`creditcustname`  = '".$bvalue['creditcustname']."', 
							`mealpass`  = '".$bvalue['mealpass']."', 
							`pass`  = '".$bvalue['pass']."', 
							`roomservice`  = '".$bvalue['room_service']."', 
							`roomno`  = '".$bvalue['room_no']."', 
							`msrcard` = '".$bvalue['msrcard']."',  
							`msrcardno` = '".$bvalue['msrcardno']."',
							`msrcardname` = '".$bvalue['msrcardname']."',
							`onac` = '".$bvalue['onac']."',
							`onaccust` = '".$bvalue['onaccust']."',
							`onaccontact` = '".$bvalue['onaccontact']."',
							`onacname` = '".$bvalue['onacname']."',
							`total_payment` = '".$bvalue['total_payment']."',
							`pay_method` = '".$bvalue['pay_method']."',
							`bk_status` = '".$bvalue['bk_status']."',
							`login_name` = '".$bvalue['login_name']."',
							`login_id` = '".$bvalue['login_id']."',
							`food_cancel` = '".$bvalue['food_cancel']."',
							`liq_cancel` = '".$bvalue['liq_cancel']."',
							`bill_modify` = '".$bvalue['bill_modify']."',
							`modify_remark` = '".$bvalue['modify_remark']."',
							`duplicate` = '".$bvalue['duplicate']."',
							`duplicate_time` = '".$bvalue['duplicate_time']."',
							`sales_id` = '".$bvalue['sales_id']."',
							`sales_name` = '".$bvalue['sales_name']."',
							`sales_contact` = '".$bvalue['sales_contact']."',
							`sales_address` = '".$bvalue['sales_address']."',
							`sales_address_1` = '".$bvalue['sales_address_1']."',
							`gst_type` = '".$bvalue['gst_type']."',
							`tendering_amount` = '".$bvalue['tendering_amount']."',
							`change_amount` = '".$bvalue['change_amount']."',
							`credit_number` = '".$bvalue['credit_number']."',
							`printstatus` = '".$bvalue['printstatus']."'
							WHERE `order_id` = '".$bvalue['order_id']."' ";
							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($sql, true)  . "\n");
							$this->query($sql,$this->conn);
							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('before order info error', true)  . "\n");
							fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");	
				}
			}
			if(strtotime($bill_date) != strtotime($last_open_date)){
				$update_sql = "UPDATE `oc_order_info` SET `day_close_status` = '1' WHERE `bill_date` = '".$last_open_date."' ";
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($update_sql, true)  . "\n");
				$this->query($update_sql,$this->conn);
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('before update day close error', true)  . "\n");
				fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
			}
			$info = 1;
		}  else {
			$info = 1;
		}

	    if(isset($data['item_datas'][0]) && !empty($data['item_datas'])){
			foreach($data['item_datas'] as $akey => $avalue){
				//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($avalue, true)  . "\n");
				$is_exist_sql = "SELECT `id` FROM `oc_order_items` WHERE `id` = '".$avalue['id']."' ";
				$is_exist = $this->query($is_exist_sql, $this->conn);
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_order_items` SET 
						`id` = '".$avalue['id']."',
						`order_id` = '".$avalue['order_id']."',  
						`billno` = '".$avalue['billno']."', 
						`nc_kot_reason` = '".$avalue['nc_kot_reason']."', 
						`nc_kot_status` = '".$avalue['nc_kot_status']."', 
						`code` = '".$avalue['code']."' ,
						`name` = '".$avalue['name']."',
						`qty` = '".$avalue['qty']."',  
						`transfer_qty` = '".$avalue['transfer_quantity']."', 
						`mrp` = '".$avalue['mrp']."', 
						`you_save` = '".$avalue['you_save']."', 
						`hsn_code` = '".$avalue['hsn_code']."', 
						`rate` = '".$avalue['rate']."', 
						`rate_original` = '".$avalue['rate_original']."', 
						`extra_requirement` = '".$avalue['extra_requirement']."', 
						`packing_charges` = '".$avalue['packing_charges']."', 
						`ismodifier` = '".$avalue['ismodifier']."',
						`parent_id` = '".$avalue['parent_id']."',  
						`parent` = '".$avalue['parent']."', 
						`cancelmodifier` = '".$avalue['cancelmodifier']."', 
						`amt` = '".$avalue['amt']."' ,
						`amt_original` = '".$avalue['amt_original']."' ,
						`item_amount_after_discount` = '".$avalue['item_amount_after_discount']."',
						`subcategoryid` = '".$avalue['subcategoryid']."',
						`message` = '".$avalue['message']."',  
						`is_liq` = '".$avalue['is_liq']."', 
						`kot_status` = '".$avalue['kot_status']."',
						`pre_qty` = '".$avalue['pre_qty']."',
						`prefix` = '".$avalue['prefix']."', 
						`is_new` = '".$avalue['is_new']."', 
						`kot_no` = '".$avalue['kot_no']."',
						`reason` = '".$avalue['reason']."',
						`discount_per` = '".$avalue['discount_per']."',
						`discount_value` = '".$avalue['discount_value']."',
						`tax1` = '".$avalue['tax1']."' ,
						`tax1_value` = '".$avalue['tax1_value']."',
						`tax2` = '".$avalue['tax2']."',  
						`tax2_value` = '".$avalue['tax2_value']."', 
						`bk_status` = '".$avalue['bk_status']."', 
						`cancelstatus` = '".$avalue['cancelstatus']."', 
						`login_id` = '".$avalue['login_id']."',
						`login_name` = '".$avalue['login_name']."',  
						`cancel_bill` = '".$avalue['cancel_bill']."', 
						`bill_modify` = '".$avalue['bill_modify']."', 
						`time` = '".$avalue['time']."',
						`date` = '".$avalue['date']."',
						`printstatus` = '".$avalue['printstatus']."',
						`waiter_code` = '".$avalue['waiter_code']."',
						`waiter_id` = '".$avalue['waiter_id']."',
						`waiter_name` = '".$avalue['waiter_name']."',
						`steward_commission` = '".$avalue['steward_commission']."',
						`steward_commission_amount` = '".$avalue['steward_commission_amount']."',
						`steward_commission_type` = '".$avalue['steward_commission_type']."',
						`captain_code` = '".$avalue['captain_code']."',
						`captain_id` = '".$avalue['captain_id']."',
						`captain_name` = '".$avalue['captain_name']."',
						`captain_commission` = '".$avalue['captain_commission']."',
						`captain_commission_amount` = '".$avalue['captain_commission_amount']."',
						`captain_commission_type` = '".$avalue['captain_commission_type']."',
						`bill_date` = '".$avalue['bill_date']."' ";
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($sql, true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('before order item error', true)  . "\n");
					$this->query($sql, $this->conn);
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				} else {
					$sql = "UPDATE `oc_order_items` SET 
						`order_id` = '".$avalue['order_id']."',  
						`billno` = '".$avalue['billno']."', 
						`nc_kot_reason` = '".$avalue['nc_kot_reason']."', 
						`nc_kot_status` = '".$avalue['nc_kot_status']."', 
						`code` = '".$avalue['code']."' ,
						`name` = '".$avalue['name']."',
						`qty` = '".$avalue['qty']."',  
						`transfer_qty` = '".$avalue['transfer_quantity']."',
						`mrp` = '".$avalue['mrp']."', 
						`you_save` = '".$avalue['you_save']."', 
						`hsn_code` = '".$avalue['hsn_code']."', 
						`rate` = '".$avalue['rate']."',
						`rate_original` = '".$avalue['rate_original']."', 
						`extra_requirement` = '".$avalue['extra_requirement']."', 
						`packing_charges` = '".$avalue['packing_charges']."', 
						`ismodifier` = '".$avalue['ismodifier']."',
						`parent_id` = '".$avalue['parent_id']."',  
						`parent` = '".$avalue['parent']."', 
						`cancelmodifier` = '".$avalue['cancelmodifier']."', 
						`amt` = '".$avalue['amt']."',
						`amt_original` = '".$avalue['amt_original']."',
						`item_amount_after_discount` = '".$avalue['item_amount_after_discount']."',
						`subcategoryid` = '".$avalue['subcategoryid']."',
						`message` = '".$avalue['message']."',  
						`is_liq` = '".$avalue['is_liq']."', 
						`kot_status` = '".$avalue['kot_status']."',
						`pre_qty` = '".$avalue['pre_qty']."',
						`prefix` = '".$avalue['prefix']."', 
						`is_new` = '".$avalue['is_new']."', 
						`kot_no` = '".$avalue['kot_no']."',
						`reason` = '".$avalue['reason']."',
						`discount_per` = '".$avalue['discount_per']."',
						`discount_value` = '".$avalue['discount_value']."',
						`tax1` = '".$avalue['tax1']."' ,
						`tax1_value` = '".$avalue['tax1_value']."',
						`tax2` = '".$avalue['tax2']."',  
						`tax2_value` = '".$avalue['tax2_value']."', 
						`bk_status` = '".$avalue['bk_status']."', 
						`cancelstatus` = '".$avalue['cancelstatus']."', 
						`login_id` = '".$avalue['login_id']."',
						`login_name` = '".$avalue['login_name']."',  
						`cancel_bill` = '".$avalue['cancel_bill']."', 
						`bill_modify` = '".$avalue['bill_modify']."', 
						`time` = '".$avalue['time']."',
						`date` = '".$avalue['date']."',
						`printstatus` = '".$avalue['printstatus']."',
						`waiter_code` = '".$avalue['waiter_code']."',
						`waiter_id` = '".$avalue['waiter_id']."',
						`waiter_name` = '".$avalue['waiter_name']."',
						`steward_commission` = '".$avalue['steward_commission']."',
						`steward_commission_amount` = '".$avalue['steward_commission_amount']."',
						`steward_commission_type` = '".$avalue['steward_commission_type']."',
						`captain_code` = '".$avalue['captain_code']."',
						`captain_id` = '".$avalue['captain_id']."',
						`captain_name` = '".$avalue['captain_name']."',
						`captain_commission` = '".$avalue['captain_commission']."',
						`captain_commission_amount` = '".$avalue['captain_commission_amount']."',
						`captain_commission_type` = '".$avalue['captain_commission_type']."',
						`bill_date` = '".$avalue['bill_date']."' 
						WHERE `id` = '".$avalue['id']."' ";
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($sql, true)  . "\n");
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r('before order item error', true)  . "\n");
					$this->query($sql, $this->conn);
					fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r(mysqli_error($this->conn), true)  . "\n");
				}
			}
			$item = 1;
		} else {
			$item = 1;
		}
		if($item == '1' && $info == '1'){ 
			$result['success'] = 1;
		} else {
			$result['success'] = 0;
	    }
		fwrite($handle, date('Y-m-d H:i:s') . ' - ' . print_r($result, true)  . "\n");
	    return $result;
	}

  	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>